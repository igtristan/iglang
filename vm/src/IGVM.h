/*
Copyright (c) 2014-2017 Incubator Games Ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#ifndef IGVM_h
#define IGVM_h 1

//include <fcgi_stdio.h>

#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include <stdio.h>

#ifdef __MINGW32__
	#include <malloc.h>
#endif

// new form
#define THROW_IGVM_NATIVE_ERROR(format,...) { igvm::NativeError ex; \
   snprintf(ex.message, igvm::NativeError::MAX_LENGTH - 1, "%s:%d " format, __FILE__, __LINE__, ##__VA_ARGS__); \
   ex.message[igvm::NativeError::MAX_LENGTH - 1] = '\0'; \
   throw ex;  \
   }

// deprecated form
#define THROW_IGVM_NATIVE_EXCEPTION(format,...) { igvm::NativeError ex; \
   snprintf(ex.message, igvm::NativeError::MAX_LENGTH - 1, "%s:%d " format, __FILE__, __LINE__, ##__VA_ARGS__); \
   ex.message[igvm::NativeError::MAX_LENGTH - 1] = '\0'; \
   throw ex;  \
   }

namespace igvm
{
	const int MAX_STACK_TRACE = 2048*4;
	
#ifdef IGBUILD_MVC
	typedef __int64					ig_i64;
	typedef __int32					ig_i32;
	typedef __int16					ig_i16;
	typedef __int8					ig_i8;
	typedef unsigned __int64		ig_u64;
	typedef unsigned __int32		ig_u32;
	typedef unsigned __int16		ig_u16;
	typedef unsigned __int8			ig_u8;
#else
	typedef int8_t   ig_i8;
	typedef int16_t  ig_i16;
	typedef int32_t  ig_i32;
	typedef int64_t  ig_i64;
	typedef uint8_t  ig_u8;
	typedef uint16_t ig_u16;
	typedef uint32_t ig_u32;
	typedef uint64_t ig_u64;
#endif

	const size_t IGVM_MAX_PATH = 4096;
	const int    LOAD_BUILTINS       = 1;			// init flag to load built in functions
	const int    NO_DEFAULT_INSTANCE = 2;			// init flag to ask not to create a default VM
	
	struct NativeError
	{
		static const int MAX_LENGTH = 1024;
		char message[MAX_LENGTH];

		const char * getMessage() { return message; }
	};
		
	union Box;	
	struct ByteArray;
	struct Array;
	struct VMInstance;
	struct Root;
	
	//////////////////////////////////////////////
	// Config Structure to be configured by user
	// see IGVMConfig.cpp for defaults
	//////////////////////////////////////////////
	
	struct Config
	{
		void  (*_get_dir)(VMInstance * instance, char * out, size_t sz);
		// reading igvm compiled source code
		void * (* _open)(VMInstance * instance, const char * path);
		void   (* _read)(VMInstance * instance, void * ptr, void * dst, size_t);
		void   (*_close)(VMInstance * instance, void * ptr);

		// accessing memory
		void * (*_malloc)(VMInstance * instance, size_t sz);
		void   (*_free)  (VMInstance * instance, void * ptr);
		
		// tracing out messages from the VM (not from user code)
		void   (*_log)   (VMInstance * instance, const char * message);
		
		// dealing with reference counted enums
		void   (*_swap_ref_32)(VMInstance * instance, ig_i32 old_ref, ig_i32 new_ref, bool has_new);
		
		int   YOUNG_POOL_SIZE;
		int   GC_FIRST;
		int   GC_INCREMENTAL;
	
		inline void init() {
			_open = defaultOpen;
			_read = defaultRead;
			_close = defaultClose;
			_log = defaultLog;
			_malloc = defaultMalloc;
			_free = defaultFree;
			_swap_ref_32 = defaultSwapRef32;
			_get_dir = defaultGetCompiledSourceDirectory;
			
			// larger is better for performance
			YOUNG_POOL_SIZE = 32  * 1024 * 1024; // 32 MB
			GC_FIRST        = 128 * 1024 * 1024; // 128 MB
			GC_INCREMENTAL  = 32  * 1024 * 1024; // 32 MB
		}
	
	
	private:
		static void * defaultOpen(VMInstance * vm, const char * path);
		static void   defaultRead(VMInstance * vm, void * ptr, void * dst, size_t sz);
		static void   defaultClose(VMInstance * vm, void * ptr);
		static void *  defaultMalloc(VMInstance * vm, size_t sz);
		static void    defaultFree(VMInstance * m, void *);
		static void    defaultLog(VMInstance * vm, const char * message);
		static void    defaultSwapRef32(VMInstance * vm, ig_i32 old_ref, ig_i32 new_ref, bool has_new);
		static void    defaultGetCompiledSourceDirectory(VMInstance * vm, char * out, size_t sz);
		
	};

		
	//////////////////////////////////////////
	// Object Oriented Interface
	//////////////////////////////////////////	
		
	struct VMInstance 
	{
		void * m_user_data;


		Config * getConfig();
		/**
		 * Get the user data pointer associated with this VMInstance
		 */

		inline void * getUserData() { return m_user_data; }
		
		/**
		 * Associate a user data pointer with this VM instance
		 */
		
		inline void   setUserData(void * d) { m_user_data = d; }
		
		/**
		 * Initialize the instance.  
		 * If config is NULL then it uses the data specified via globalInit
		 * If flags contains LOAD_BUILTINS, then it loads the builtins
		 */ 
		int init(const Config * config = NULL, int flags = LOAD_BUILTINS);
		
		/**
		 * Clear all roots and memory used by this vm. 
		 */ 
		
		int reinit(int flags = LOAD_BUILTINS);
		
		/**
		 * Call a method by specifying a klass and a method
		 * If this is a member method params[0] should be the "this" object
		 */
		int    call(const char * klass, const char * method, const Box * params, int param_count, Box * ret = NULL);
		
		/**
		 * Call a function pointer
		 */
		 
		int    callFunctionPointer(const Box & fp, const Box * params, int param_count, Box * ret = NULL);
		
		/**
		 * Call a named method on an object.  
		 * params[0] should be the object to invoke the method on
		 * This method uses the vtable of the object to determine which method to call
		 */
		int    callNamed(const char * method, const Box * params, int param_count, Box * ret = NULL);
		
		/**
		 * Create a new object of type klass.  
		 * klass is of the form "iglang.String"
		 */
		 
		int    newObject          (const char * klass, const Box * params, int param_count, Box * result);
		
		
		/**
		 * Create an IG string from a '\0' terminated cstring with UTF-8 data
		 *
		 */
		int    stringFromUTF8Bytes          (const char * str, Box * result);

		/**
		 * Create an IG string from UTF-8 data
		 * The number of UTF-8 bytes is specified
		 */
		 
		int    stringFromUTF8BytesWithLength(const char * str, int byte_len, Box * result);
		
		/**
		 * Calculate the number of bytes needed to represent this string as UTF8 Bytes
		 */
		
		int    UTF8ByteLengthOfString(const Box & str, int * byte_len);
		
		 /**
		  * Return the number of unicode characters contained within the string
		  */

		int    stringLength(const Box & str, int * out_length);

		/**
		 * Read a VM string as UTF8 into a buffer with a specified capacity.
		 * If there isn't enough capacity for the string, as much of the string as possible will be written
		 * The buffer is guaranteed to end with a '\0'
		 */
		int    UTF8BytesFromString(const Box & str, char * string, int capacity);
		

		/**
		 * Read a string out as individual unicode characters that take up at most 4bytes
		 * Thee buffer is guaraneteed to end with a '\0'
		 */
		int    U32FromString(const Box & str, ig_u32 * string, int capacity);
		
		
		/**
		 * To be used by native code to retain references to objects, in case GC occurs.
		 * By default a scope will be created for all native invocations
		 */

		void beginScope();
		void endScope();
		
		/**
		 * Set a native function to be invoked in place of an ig function
		 */
		
		
		int    setNativeFunction(const char * module_name, const char * function_name,
				int (*fn)(VMInstance * vm, const Box * params, int param_count, Box * ret));
				
		int    getClassName(const Box & object, char * out_text, int out_capacity); 		
		int    setField (const char * klass, const char * field, const Box & obj_handle, const Box & data);
		int    getField    (const char * klass, const char * field, const Box & object, Box * out);
		int    getArray    (const Box & handle, Array * array);
		
		/**
		 * Get the backing data of a ByteArray and optionally set the length of it
		 */
		
		int    getByteArray(const Box & handle, ByteArray * data, int set_length = -1);
		
		/**
		 * Create a byte array with a specific length and capacity
		 * Position will be set to 0,
		 * Length and Capacity will be set to length
		 */
		
		int    createByteArray(const void * data, int length, Box * out);
		
		const char * getErrorMessage();
		
		
		/**
		 * Configuration for debugging the vm
		 */
		
		int    debugConfig(int v);
		
		void   gc(bool stop_the_world = false);
		
		/**
		 * Create a new root.  This is initially set to null
		 */
		Root   allocRoot();
		
		/**
		 * Dispose of an existing root
		 */
		
		void   freeRoot(Root * root);
		
		/**
		 * Set the contents of the root to NULL
		 */
		
		void   clearRoot(Root * root);
		
		/**
		 * Set the value of a root to that within a box
		 */
		
		void   setRoot(Root * root, const Box & value);
		
		/**
		 * Get the value contained in the root
		 */
		
		Box    getRoot(Root * root);
	};	

	



	struct FuncPtr
	{
		ig_u32 object;
		ig_u32 function;
	};

	union Box
	{
	    ig_u32  u32;
		ig_i32  i32;
		ig_i64  i64;
		double  f64;
		FuncPtr fp;
		ig_u32  lo_hi[2];
	};

	struct Array
	{
		ig_i32 length;
		void * data;
		
		inline igvm::Box getObjectAt(int idx) {
			ig_i32 * d = (ig_i32 *)data;
			Box box;
			box.i64 = 0;
			box.i32 = d[idx];
			return box;
		}
		
		inline igvm::Box getDelegateAt(int idx) {
			ig_i64 * d = (ig_i64 *)data;
			Box box;
			box.i64 = d[idx];
			return box;
		}
	};

	struct ByteArray
	{
		ig_i32 length;
		void * data;
	};
	
	
	struct Root
	{
		ig_u32 m_root;
	};
	
	////////////////////////////////////////////////////////////////
	// Static Interface
	////////////////////////////////////////////////////////////////

	class VM
	{
	public:

		/**
		 * global initializer for the VM
		 *
		 */
		static int globalInit(const Config * config, int flags = 0);


		static VMInstance * allocInstance();
		static void         setActiveInstance(VMInstance * instance);
		static VMInstance * getActiveInstance();
		static void         freeInstance(VMInstance *);   

		/*
		 * Initialize the vm.  Must be called before any other function
		 */
		 
		static int init(const Config * config, int flags = 0);
		
		
		/*
		 * Load builtin native functions.  (Object, Math, etc)
		 * Must be called after init.
		 */

		static int loadBuiltins();

		/*
		 * Re-initialize the vm.
		 * This'll load all files from disk again, and throw away any static variables.
		 * All objects will be disposed, and any handles retained by the calling environment
		 * will become invalid.  Their further use will cause undefined behaviour
		 */
		 
		static int reinit(int flags = 0);




		/**
		 * Explicitly load a named module
		 * @param module_name - the name of the module to load (ie iglang.String)
		 */
		static int  loadModule(const char * module_name);

		/*
		 * Set a native function to be called instead of igcode for a static or member function
		 * @param module_name - the module specified (ie iglang.String)
		 * @param function_name - the function specified (ie toString)
		 * @param fn - the native function to call that will be called in place of any existing ig code
		 */

		static int  setNativeFunction(const char * module_name, const char * function_name,
				int (*fn)(VMInstance * vm, const Box * params, int param_count, Box * ret));
				
		static void beginScope();
		static void endScope();		
				
		
		static int setNativeDestructor(const char * module_name, void (*fn)(const Box obj));

		/*
		 *  Check if an object is an instance of another kind of object
		 *   If the object is null it will have a result of false
		 */
		 
		static int    objectInstanceOf(const char * klass, const Box & object, bool * result);
		
		/**
		 * Get the class name of an object.
		 */
		
		static int getClassName(const Box & object, char * out_text, int out_capacity);
		
		/*
		 * Check if a specified type extends another type
		 * @param child_class  - eg.  iglang.String
		 * @param parent_class - eg. iglang.Object
		 * @param out_result   - returns whether the child_class extends the parent class
		 */
		 
		static int typeExtends(const char * child_class, const char * parent_class, bool * out_result);


	 	static int    call2(const char * klass, const char * function_name, const Box * parameters, int count, Box * out);


		static int    callFunctionPointer(const Box & fp, const Box * params, int param_count, Box * out);
		
		inline static int callFunctionPointer(const Box & fp, const Box * params, int param_count) {
			Box garbage;
			return callFunctionPointer(fp, params, param_count, &garbage);
		}	
		
		/*
		 * Invoke a static function, or an explicit member function on an object
		 */
		 
		inline static int    call(const char * klass, const char * function_name, const Box * parameters, int count, Box & out)
		{
			Box ret;
			return call2(klass,function_name,parameters,count, &ret);
			out = ret;
		}
					
		/*
		 * Same as above but ignores the return parameter
		 */			
		inline static int call(const char * klass, const char * function_name, 
					const Box * parameters, int count) {
			Box garbage;
			return call2(klass,function_name,parameters,count, &garbage);
		}

		/*
		 * Invoke a named member function on an object through its vtable
		 */
		static int    callNamed(const char * function_name, const Box * parameters, int count, Box * out = NULL);
		

		/*
		 * Create a new object.  This calls the constructor on it
		 * Currently limited to a max of 32 parameters to the constructor
		 * NOTE: If GC is called before the object is stored
		 * the box returned will be invalid and use will cause undefined behaviour
		 */
		static int    newObject          (const char * klass, const Box * parameters, int count, Box * result);

		/*
		 * Convert a utf8 byte C string to a VM String
		 */
		static int    stringFromUTF8Bytes(const char * string, Box * result);
		
		
		/*
		 *
		 */
		static int    stringFromUTF8BytesWithLength(const char * string, int byte_length, Box * result);

		/*
		 * Convert a VM String to a UTF8 byte C String
		 * 
		 */

		 static int  UTF8BytesFromString(const Box & str, char * string, int capacity);




		 static int  U32FromString(const Box & str, ig_u32 * string, int capacity);

		 /**
		  * Count the # of bytes, excluding the null terminator required to store
		  * the given string in a UTF8 representation
		  */

		 static int    UTF8ByteLengthOfString(const Box & str, int * byte_length);

		 /**
		  * Return the number of unicode characters contained within the string
		  */

		 static int    stringLength(const Box & str, int * out_length);
			
		/**
		   Get the error message from the last reported error.
		   Any calls to the vm following a getErrorMessage may change this text.
		 */

		static const char * getErrorMessage();


		// BRUTE FORCE METHODS TO ACCESS DATA

		/*
		 * Get the offset that data appears in an object
		 */

		static int  describeMemberVariable
			(const char * klass, const char * field, bool drill_down,
			 char * out_type, int out_type_length,
			 char * out_exact_type, int out_exact_type_length,  int * out_offset, int * out_size);
			 
		static int  setMemberVariable
			(const char * klass, const char * field, bool drill_down,
			 const Box & data);

		static int  getMemberVariableOffset(const char * klass, const char * field, int * offset);
		
		/*
		 * Get the number of bytes from the top of an object that the array data starts at
		 */
		 
		static int  getArrayDataOffset(int * offset); 
		
		/*
		 * Overwrite data in an object.  Use getMemberVariableOffset to determine the offset
		 * Note this is very unsafe
		 */

		static int  setMemberData          (const Box & object, int offset, int length, const void * data);

		/*
		 * Get the data at an offset from within an object..  Use getMemberVariableOffset to determine the offset 
		 * Note this is very unsafe
		 */

		static int  getMemberData          (const Box & object, int offset, int length, void * data);


		/*
		 * Force garbage collection
		 * This could be called each frame on a separate thread if no other thread is using the 
		 * scripting system
		 */
		static void forceGC(bool stop_the_world = false);

		/*
		 * Get the value of a field on an object.
		 */

		static int getField (const char * klass, const char * field, const Box & object, Box * out);


		/*
		 * Set the value of a field on an object
		 */
		 
		static int setField (const char * klass, const char * field, const Box & obj_handle, const Box & data);

		/*
		 * Get a fixed representation of an array for editing outside of the vm
		 * Array contains two fields:   length and data
		 */

		static int    getArray(const Box & handle, Array * array);
		
		/*
		 * Get a fixed representation of the backing data of a Byte Array for editing outside of teh vm
		 *
		 */
		
		static int    getByteArray(const Box & handle, ByteArray * data, int set_length = -1);
		
		static int    debugConfig(int param);
		
		static Root   allocRoot();
		static void   freeRoot(Root * root);
		static void   clearRoot(Root * root);
		static void   setRoot(Root * root, const Box & value);
		static Box    getRoot(Root * root);
		
		
		/*
		 * Create a box that contains a 32 bit integer
		 */

		inline static Box i32Box(int i) {
			Box b;
			b.i32 = i;
			return b;
		}

		inline static Box nullBox() {
			Box b;
			b.i64 = 0;
			return b;
		}
		
		inline static bool isNull(const Box & b) {
			return b.u32 == 0;
		}	

	};


	inline Box f64Box(double d) {
		Box b;
		b.f64 = d;
		return b;
	}

	inline  Box i32Box(int i) {
		Box b;
		b.i32 = i;
		return b;
	}

	inline  Box nullBox() {
		Box b;
		b.i64 = 0;
		return b;
	}


};

#endif

