/*
Copyright (c) 2014-2017 Incubator Games Ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/


#include "IGVM.h"
#include <math.h>
#include <time.h>

#include "IGVMInternal.h"

#define IGVM_LIKELY(x)       __builtin_expect((x),1)
#define IGVM_UNLIKELY(x)     __builtin_expect((x),0)

static igvm::ig_u8  s_invoke_native_code[1] = { igvm::InOp::INVOKE_NATIVE };



///////////////////////////////////////
// Stack Trace Code
////////////////////////////////////////

#ifdef OSX_BUILD

#include <execinfo.h>



static void printStackTrace()
{	
	FILE *out = stderr;
	unsigned int max_frames = 63 ;
	fprintf(out, "stack trace:\n");

	// storage array for stack trace address data
	void* addrlist[max_frames+1];

	// retrieve current stack addresses
	int addrlen = backtrace( addrlist, sizeof( addrlist ) / sizeof( void* ));

	if ( addrlen == 0 ) 
	{
	  fprintf( out, "  \n" );
	  return;
	}

	// create readable strings to each frame.
	char** symbollist = backtrace_symbols( addrlist, addrlen );

	// print the stack trace.
	for ( int i = 0; i < addrlen; i++ )
	  fprintf( out, "%s\n", symbollist[i]);

	free(symbollist);
}

#else

static void printStackTrace() {
}

#endif

/////////////////////////////////////////


// enable tracking of ops executed in each function
// this is controller via Error.__debugConfig from within user scripts
#ifndef IGVM_TRACK_FUNCTION_OPS
#define IGVM_TRACK_FUNCTION_OPS 0
#endif

// enable using the jump table or a switch for the core of the vm
// this seems to perform better on iOS
// NOTE the jump table is LIKELY broken currently.  TODO verify
// Doesn't seem to really offer a significant performance boost
#define IGVM_USE_SWITCH         1


#define IGVM_DEBUG_RUNTIME 0
#define IGVM_DEBUG 0
#define IGVM_GC_DEBUG 0
#define IGVM_DEBUG_processModuleCode 0



// add the line number before each malloc and free call
#define IGVM_MEMORY_DEBUG 0


// on forceGC print out what is being allocated/freed each frame
#define IGVM_GC_PROFILING 0

// no if statement in INVOKE_FUNCTION
#define NATIVE_PATH_2     1

#define MAX_ERROR_MESSAGE_LENGTH 1024


#define IGVM_RESIZE(ptr,old_size,new_size) m_allocator.RESIZE(ptr,old_size,new_size, __FILE__, __LINE__)
#define IGVM_DUP(ptr,old_size,new_size)    m_allocator.DUP(ptr,old_size,new_size, __FILE__, __LINE__)
#define IGVM_MALLOC(sz)                    m_allocator.MALLOC(sz, __FILE__, __LINE__)
#define IGVM_FREE(ptr)                     m_allocator.FREE(ptr)
#define IGVM_CALL(vm,x) { int retval; if((retval = (x)) != 0) { fprintf(stderr, "IGVM FAILURE: %d\n", __LINE__); return retval; } }





namespace igvm
{

class VMImp;
struct Function;
struct Stack;
struct Module;
struct Object;
struct ExceptionTableEntry;
struct DebugTableEntry;
struct JumpTableEntry;
struct Item;
struct StackFrame;
struct PairEntry;
struct StringEntry;

	
 void dieAPainfullDeath(const char * str) {
	fprintf(stderr, "%s\n", str);
	printStackTrace();

	int * die = 0;
	*die = 666;
}

struct ObjectHeader;

void printObjectName(ObjectHeader * obj);
//const char * getModuleName(ig_u32 member_pair);


// the current active instane


static VMImp  * s_imp = NULL;

//void * __IGVM_MALLOC(size_t x, const char * file, int line);
//void * __IGVM_RESIZE(void * old_ptr, size_t old_size, size_t new_size, const char * file, int line);
//void   __IGVM_FREE(void * ptr, const char * file, int line);

//include "IGVMUtil.hpp"
//include "IGVMHeap.hpp"

/////////////
// Error tables
//////////////

struct ErrorText {
	int          sig;
	const char * text;
}
error_text[] =
{
	{0x11, "VM::validatePair couldn't load module"},
	{0x12, "VM::validatePair module wasn't loaded"},
	{0x13, "VM::validatePair type mismatch"},

	{0x21, "VM::newObject failed to find module"},
	{0x22, "VM::newObject failed to find constructor"},
	{0x23, "VM::newObject too few arguments provided to constructor"},
	{0x24, "VM::newObject too many arguments provided to constructor"},
	{0x25, "VM::newObject call to constructor failed"},

	{0x31, "VM::callNamed Couldn't find string entry"},
	{0x32, "VM::callNamed Callee is null"},
	{0x33, "VM::callNamed Couldn't find function in vtable"},
	{0x34, "VM::callNamed Invoke failed"},


	{0x41, "VM::call Function doesn't exist"},
	{0x42, "VM::call Too few parameters"},
	{0x43, "VM::call Too many parameters"},
	{0x45, "VM::call Invoke failed"},
	{0x46, "VM::call Function pointer refers to null function"},
	{0x47, "VM::call Function pointer refers to null object"},
	
	
	{0x51, "VM:getField field does not exist"},
	{0x52, "VM:getField field does not exist"},
	{0x53, "VM:getField field does not exist"},
	{0x54, "VM:getField field does not exist"},

	{0x61, "VM:setNativeFunction failed"},
	{0x62, "VM:setNativeFunction failed"},
	{0x63, "VM:setNativeFunction failed"},
	{0x64, "VM:setNativeFunction failed"},
	
	{0x6A, "VM:setNativeDestructor failed"},
	{0x6B, "VM:setNativeDestructor failed"},
	{0x6C, "VM:setNativeDestructor failed"},
	{0x6D, "VM:setNativeDestructor failed"},

	{0x71, "VM::getMemberVariableOffset Failed to find member variable"},
	{0x81, "VM::setMemberData Null object passed."},
	{0x82, "VM::getMemberData Null object passed."},

	
	{0x91, "VM::describeMemberVariable Couldn't find variable"},
	{0x92, "VM::describeMemberVariable Couldn't find variable"},
	{0x93, "VM::describeMemberVariable Couldn't find module"  },
	{0x94, "VM::describeMemberVariable Couldn't find variable"},
	{0x95, "VM::describeMemberVariable Null or empty klass or field"},

	{0xC0, "VM::stringFromUTF8Bytes(WithLength) Core library issue"},
	{0xC1, "VM::stringFromUTF8Bytes(WithLength) Failed to initialize string backing"},
	{0xC2, "VM::stringFromUTF8Bytes(WithLength) Invalid input parameters"},
	
	
	{0xD0, "VM:loadModuleInternal Critical Error. Static function slot already occupied."},
	{0xD1, "VM:loadModuleInternal Critical Error. Static variable slot already occupied."},	
	
	
	{0xED,  "VMImp::invoke stack overflow."},
	{0xEE,  "VMImp::invoke uncaught exception."},
	{0,     NULL}
};

/////////////////////
// set up tables for op code processing
// these are common for all vm instances
////////////////////

static bool         s_global_init = false;
static Config       s_global_config;

static bool         s_in_ops_valid [256];
static int          s_in_ops       [256];  // modifiers
static const char * s_in_opm_string[256];  // string identifier
static ig_i16       s_in_ops_dir   [256];  // stack change
static ig_u8 		s_in_ops_sz     [256];  // total 


static void registerInOp(int op, int modifiers, const char * str, int dir = 0) {
	s_in_ops[op]        = modifiers;
	s_in_opm_string[op] = str;
	s_in_ops_dir[op]    = dir;
	s_in_ops_valid[op]  = true;
	s_in_ops_sz[op] = 1 + ((modifiers >> 0) & 0x0f) + 
						 ((modifiers >> 8)  & 0x0f) + 
						 ((modifiers >> 16) & 0x0f) +
						 ((modifiers >> 24) & 0x0f);
}

/***
 * Register all the ops that this VM supports.
 * This data also encodes what type of fields each item supports
 */

static int registerInOps()
{
	// check that our notion of ASR works correctly
	/////////////////////////////////////
	{
		ig_i32 x = 0xffffffff;
		ig_u32 y = 0xffffffff;

		if ((x >> 24) != -1 ||
			(y >> 24) != 255) {

			fprintf(stderr, "igvm: FATAL ERROR: Shifts do not make sense on this architecture or C/C++ implementation. \n");
			dieAPainfullDeath("Invalid architecture");
		}
	}


	// check that union alignment makes sense
	//////////////////////////////////////////
	{
		Box a;
		if (((void *)&a.f64) != ((void *)&a.i32)) {
			fprintf(stderr, "igvm: FATAL ERROR: Union alignment failed\n");
			dieAPainfullDeath("Invalid architecture");			
		}
	}

	for (int i = 0; i < 256; i++) {
		registerInOp(0,0,"Undefined", 0);
		s_in_ops_valid[i] = false;
	}


	/*
	use regex:
		// static const int ([A-Z_0-9]+) .*;
		// registerInOp(InOp::\1,0,"\1");
	*/
	 registerInOp(InOp::NOP,0,"NOP",           0);  // .. => ..
	 registerInOp(InOp::NOP_TERM,0,"NOP_TERM",           0);  // .. => ..
	 registerInOp(InOp::POP,0,"POP",          -1);  // VALUE => ..
	 registerInOp(InOp::DUP,0,"DUP",           1);  // VALUE => VALUE,VALUE
	 registerInOp(InOp::DUP1X,0,"DUP1X",       1);
	 registerInOp(InOp::DUP2X,0,"DUP2X",       1);
	 registerInOp(InOp::RET0,0,"RET0",         0);  // .. => ..
	 registerInOp(InOp::RET1,0,"RET1",        -1);  // VALUE => ..

	 registerInOp(InOp::DUP2,0,"DUP2",              2);	 // VALUE0, VALUE1 => VALUE0, VALUE1, VALUE0, VALUE1
	 registerInOp(InOp::ADD_I32,0,"ADD_I32",       -1);  // VALUE, VALUE => VALUE
	 registerInOp(InOp::SUB_I32,0,"SUB_I32",       -1);  // VALUE, VALUE => VALUE
	 registerInOp(InOp::MUL_I32,0,"MUL_I32",       -1);  // VALUE, VALUE => VALUE
	 registerInOp(InOp::MLA_I32,0,"MLA_I32",       -2);  // VALUE, VALUE, VALUE => VALUE
	 registerInOp(InOp::DIV_I32,0,"DIV_I32",       -1);  // VALUE, VALUE => VALUE
	 registerInOp(InOp::MOD_I32,0,"MOD_I32",       -1);  // VALUE, VALUE => VALUE
	 registerInOp(InOp::ADD_F64,0,"ADD_F64",       -1);  // VALUE, VALUE => VALUE
	 registerInOp(InOp::SUB_F64,0,"SUB_F64",       -1);  // VALUE, VALUE => VALUE
	 registerInOp(InOp::MUL_F64,0,"MUL_F64",       -1);  // VALUE, VALUE => VALUE
	 registerInOp(InOp::MLA_F64,0,"MLA_F64",       -2);  // VALUE, VALUE, VALUE => VALUE
	 registerInOp(InOp::DIV_F64,0,"DIV_F64",       -1);  // VALUE, VALUE => VALUE
	 registerInOp(InOp::MOD_F64,0,"MOD_F64",       -1);  // VALUE, VALUE => VALUE
	 registerInOp(InOp::AND_I32,0,"AND_I32",       -1);  // VALUE, VALUE => VALUE
	 registerInOp(InOp::OR_I32,0, "OR_I32",        -1);  // VALUE, VALUE => VALUE
	 registerInOp(InOp::XOR_I32,0,"XOR_I32",       -1);  // VALUE, VALUE => VALUE
	 registerInOp(InOp::ASR_I32,0,"ASR_I32",       -1);  // VALUE, VALUE => VALUE
	 registerInOp(InOp::LSR_I32,0,"LSR_I32",       -1);  // VALUE, VALUE => VALUE
	 registerInOp(InOp::LSL_I32,0,"LSL_I32",       -1);  // VALUE, VALUE => VALUE
	 registerInOp(InOp::CMP_EQ_I32,0,"CMP_EQ_I32", -1);  // VALUE, VALUE => BOOLEAN
	 registerInOp(InOp::CMP_LT_I32,0,"CMP_LT_I32", -1);  // VALUE, VALUE => BOOLEAN
	 registerInOp(InOp::CMP_LE_I32,0,"CMP_LE_I32", -1);  // VALUE, VALUE => BOOLEAN
	 registerInOp(InOp::CMP_EQ_F64,0,"CMP_EQ_F64", -1);  // VALUE, VALUE => BOOLEAN
	 registerInOp(InOp::CMP_LT_F64,0,"CMP_LT_F64", -1);  // VALUE, VALUE => BOOLEAN
	 registerInOp(InOp::CMP_LE_F64,0,"CMP_LE_F64", -1);  // VALUE, VALUE => BOOLEAN
	 registerInOp(InOp::CMP_EQ_I64,0,"CMP_EQ_I64", -1);  // VALUE, VALUE => BOOLEAN
	 registerInOp(InOp::BIT_NOT_I32,0,"BIT_NOT_I32",0);  // VALUE => VALUE
	 registerInOp(InOp::NOT, 0, "NOT",              0);  // VALUE => VALUE
	 registerInOp(InOp::I32_TO_F64,0,"I32_TO_F64",  0);  // VALUE => VALUE
	 registerInOp(InOp::F64_TO_I32,0,"F64_TO_I32",  0);  // VALUE => VALUE
	 registerInOp(InOp::PUSH_64,     InOp::I64,"PUSH_64",1);     // .. => VALUE
	 registerInOp(InOp::PUSH_32,     InOp::I32,"PUSH_32",1);     // .. => VALUE
	 registerInOp(InOp::SWITCH_I32_JMP, InOp::I32 | ( InOp::I16_JUMP << 8),
		"SWITCH_I32_JMP", 0);	// VALUE => VALUE
	 registerInOp(InOp::PUSH_I8,     InOp::I8, "PUSH_I8",1);	 // .. => VALUE
	 registerInOp(InOp::PUSH_I16,     InOp::I16, "PUSH_I16",1);	 // .. => VALUE
	 registerInOp(InOp::I32_0,     0, "PUSH_I32_0",1);	 // .. => VALUE
	 registerInOp(InOp::I32_1,     0, "PUSH_I32_1",1);	 // .. => VALUE		 

	 
	 registerInOp(InOp::LL0,   0, "LL0", 1); // .. => VALUE
	 registerInOp(InOp::LL1,   0, "LL1", 1); // .. => VALUE
	 registerInOp(InOp::LL2,   0, "LL2", 1); // .. => VALUE
	 registerInOp(InOp::LL3,   0, "LL3", 1); // .. => VALUE
	 registerInOp(InOp::LL4,   0, "LL4", 1); // .. => VALUE
	
	 registerInOp(InOp::JEQ_I32,InOp::I16_JUMP,"JEQ_I32", -2);  // VALUE, VALUE => BOOLEAN
			 
	 registerInOp(InOp::ASR_I32_CONST,     InOp::I8, "ASR_I32_CONST",0);	 
	 registerInOp(InOp::LSR_I32_CONST,     InOp::I8, "LSR_I32_CONST",0);
	 registerInOp(InOp::LSL_I32_CONST,     InOp::I8, "LSL_I32_CONST",0);
	 registerInOp(InOp::LSL_ASR_I32_CONST, InOp::I8 | (InOp::I8 << 8), "LSL_ASR_I32_CONST", 0);
	 
			 
			 
	 
	 registerInOp(InOp::LOCAL_LOAD_2,  InOp::I8_LOCAL | (InOp::I8_LOCAL << 8), "LOCAL_LOAD_2",2);   // .. => VALUE, VALUE

	 registerInOp(InOp::LOCAL_LOAD,     InOp::I16_LOCAL,"LOCAL_LOAD",   1);  // .. => VALUE		
	 registerInOp(InOp::LOCAL_STORE,    InOp::I16_LOCAL,"LOCAL_STORE",  -1);  // VALUE => VALUE

		registerInOp(InOp::I_LS0, 0, "I_LS0", -1);
		registerInOp(InOp::I_LS1, 0, "I_LS1", -1);
		registerInOp(InOp::I_LS2, 0, "I_LS2", -1);
		registerInOp(InOp::I_LS3, 0, "I_LS3", -1);  
		registerInOp(InOp::I_LS4, 0, "I_LS4", -1);
		registerInOp(InOp::I_LS5, 0, "I_LS5", -1);
		registerInOp(InOp::I_LS6, 0, "I_LS6", -1);
		registerInOp(InOp::I_LS7, 0, "I_LS7", -1);  
				
	 registerInOp(InOp::LOCAL_LOAD_LO,  InOp::I8_LOCAL,"LOCAL_LOAD_LO",1);  // .. => VALUE		
	 registerInOp(InOp::LOCAL_STORE_LO, InOp::I8_LOCAL,"LOCAL_STORE_LO",-1);  // VALUE => VALUE
	 
	 registerInOp(InOp::LOCAL_STORE_KEEP, InOp::I8_LOCAL,"LOCAL_STORE_KEEP", 0);  // VALUE => ..			
	 registerInOp(InOp::LOCAL_CLEAR,   InOp::I16_LOCAL,"LOCAL_CLEAR", 0);  // .. => ..
	 registerInOp(InOp::INTERNAL_LOCAL_CLEAR_LO,   InOp::I8_LOCAL,"INTERNAL_LOCAL_CLEAR_LO", 0);  // .. => ..
	 	  
	 registerInOp(InOp::MEMBER_LOAD,    InOp::I16_MEMBER_VARIABLE_OFFSET,"MEMBER_LOAD",   0);   // OBJECT => VALUE 
	 registerInOp(InOp::MEMBER_LOAD_32, InOp::I16_MEMBER_VARIABLE_OFFSET,"MEMBER_LOAD_32",   0);   // OBJECT => VALUE 
	 
	 registerInOp(InOp::INTERNAL_MEMBER_LOAD_LO,    InOp::I8_MEMBER_VARIABLE_OFFSET,"MEMBER_LOAD_LO",   0);   // OBJECT => VALUE 
	 registerInOp(InOp::INTERNAL_MEMBER_LOAD_32_LO, InOp::I8_MEMBER_VARIABLE_OFFSET,"MEMBER_LOAD_32_LO",   0);   // OBJECT => VALUE 
	 
	 
	 registerInOp(InOp::LOAD_OBJ_FIELD,    InOp::I8_LOCAL | (InOp::I16_MEMBER_VARIABLE_OFFSET << 8), "LOAD_OBJ_FIELD",    1);
	 registerInOp(InOp::LOAD_OBJ_FIELD_32, InOp::I8_LOCAL | (InOp::I16_MEMBER_VARIABLE_OFFSET << 8), "LOAD_OBJ_FIELD_32", 1);	 
	 
	 registerInOp(InOp::MEMBER_STORE,InOp::I16_MEMBER_VARIABLE_OFFSET,"MEMBER_STORE", -2);   // OBJECT,VALUE => VALUE
	 registerInOp(InOp::MEMBER_STORE_32_REF,InOp::I16_MEMBER_VARIABLE_OFFSET,"MEMBER_STORE_32_REF", -2);   // OBJECT,VALUE => VALUE
	 registerInOp(InOp::MEMBER_STORE_32,InOp::I16_MEMBER_VARIABLE_OFFSET,"MEMBER_STORE_32", -2);   // OBJECT,VALUE => VALUE
	 
	 registerInOp(InOp::THIS_LOAD,      InOp::I16_MEMBER_VARIABLE_OFFSET,"THIS_LOAD", 1);       // .. => VALUE
	 registerInOp(InOp::THIS_LOAD_32,   InOp::I16_MEMBER_VARIABLE_OFFSET,"THIS_LOAD_32", 1);       // .. => VALUE
	 registerInOp(InOp::INTERNAL_THIS_LOAD_LO,      InOp::I8_MEMBER_VARIABLE_OFFSET,"THIS_LOAD_LO", 1);       // .. => VALUE
	 registerInOp(InOp::INTERNAL_THIS_LOAD_32_LO,   InOp::I8_MEMBER_VARIABLE_OFFSET,"THIS_LOAD_32_LO", 1);       // .. => VALUE

	 
	 registerInOp(InOp::THIS_INC_I32,   InOp::I16_MEMBER_VARIABLE_OFFSET | (InOp::I8 << 8),"THIS_INC_I32", 0);       // .. => ..
	 registerInOp(InOp::THIS_STORE,     InOp::I16_MEMBER_VARIABLE_OFFSET,"THIS_STORE", -1);      // VALUE => VALUE
	 registerInOp(InOp::THIS_STORE_32_REF,  InOp::I16_MEMBER_VARIABLE_OFFSET,"THIS_STORE_32_REF", -1);      // VALUE => VALUE
	 registerInOp(InOp::THIS_STORE_32,  InOp::I16_MEMBER_VARIABLE_OFFSET,"THIS_STORE_32", -1);      // VALUE => VALUE
	 
	 
	 registerInOp(InOp::STATIC_LOAD, InOp::I16_STATIC_VARIABLE_OFFSET,"STATIC_LOAD", 1);     // .. => VALUE
	 registerInOp(InOp::STATIC_STORE,InOp::I16_STATIC_VARIABLE_OFFSET,"STATIC_STORE", -1);    // VALUE => VALUE
	
	
	registerInOp(InOp::I_STATIC_STORE_FUNCPTR, InOp::I16_STATIC_VARIABLE_OFFSET, "I_STATIC_STORE_FUNCPTR",-1);  // VALUE => VALUE
	registerInOp(InOp::I_STATIC_STORE_OBJ    , InOp::I16_STATIC_VARIABLE_OFFSET, "I_STATIC_STORE_OBJ",    -1);  // VALUE => VALUE
	registerInOp(InOp::STATIC_STORE_32_REF    , InOp::I16_STATIC_VARIABLE_OFFSET, "STATIC_STORE_32_REF",    -1);  // VALUE => VALUE
	
	registerInOp(InOp::I_THIS_STORE_FUNCPTR, InOp::I16_MEMBER_VARIABLE_OFFSET, "I_THIS_STORE_FUNCPTR",    -1);  // VALUE => VALUE
	registerInOp(InOp::I_THIS_STORE_OBJ    , InOp::I16_MEMBER_VARIABLE_OFFSET, "I_THIS_STORE_OBJ",        -1);  // VALUE => VALUE
	
	registerInOp(InOp::I_MEMBER_STORE_FUNCPTR, InOp::I16_MEMBER_VARIABLE_OFFSET, "I_MEMBER_STORE_FUNCPTR", -2); // OBJECT,VALUE => VALUE
	registerInOp(InOp::I_MEMBER_STORE_OBJ    , InOp::I16_MEMBER_VARIABLE_OFFSET, "I_MEMBER_STORE_OBJ",    -2);  // OBJECT,VALUE => VALUE
	
	registerInOp(InOp::STATIC_LOAD_FUNCPTR, InOp::I16_STATIC_VARIABLE_OFFSET,"STATIC_LOAD_FUNCPTR", 1);      // .. => VALUE
	registerInOp(InOp::STATIC_LOAD_OBJ,     InOp::I16_STATIC_VARIABLE_OFFSET,"STATIC_LOAD_OBJ",     1);      // .. => VALUE		
	
	
	
	
	
	 registerInOp(InOp::PUSH_STRING,   InOp::I16_STRING,"PUSH_STRING", 1);   // .. => VALUE

	 registerInOp(InOp::FUNCPTR_MEMBER,InOp::I16_PAIR,"FUNCPTR_MEMBER", 0);  // OBJECT => FUNCPTR
	 registerInOp(InOp::FUNCPTR_STATIC,InOp::I16_PAIR,"FUNCPTR_STATIC", 1);  // ..     => FUNCPTR
	 
	 // shit call functions can return or not return
	 registerInOp(InOp::CALL_INTERFACE,         InOp::I8_ARGS | (InOp::I16_STRING << 8),"CALL_INTERFACE",       0);
	 registerInOp(InOp::CALL_VTABLE,            InOp::I8_ARGS | (InOp::I16_FUNCTION_OFFSET << 8),"CALL_VTABLE", 0);
	 registerInOp(InOp::CALL_SUPER,             InOp::I8_ARGS | (InOp::I16_FUNCTION_OFFSET << 8),"CALL_SUPER",  0);
	 registerInOp(InOp::CALL_STATIC,            InOp::I8_ARGS | (InOp::I16_PAIR << 8),"CALL_STATIC",            0);
	 registerInOp(InOp::CALL_STATIC_VTABLE,     InOp::I8_ARGS | (InOp::I16_PAIR << 8),"CALL_STATIC_VTABLE",     0);
	 registerInOp(InOp::CALL_DYNAMIC,           InOp::I8_ARGS ,"CALL_DYNAMIC",                                  0 - 1);		// consumes the dynamic ptr
	 
	 registerInOp(InOp::CALL_RET1_INTERFACE,    InOp::I8_ARGS | (InOp::I16_STRING << 8),"CALL_RET1_INTERFACE",  1);
	 registerInOp(InOp::CALL_RET1_VTABLE,       InOp::I8_ARGS | (InOp::I16_FUNCTION_OFFSET << 8),"CALL_RET1_VTABLE",1);
	 registerInOp(InOp::CALL_RET1_SUPER,        InOp::I8_ARGS | (InOp::I16_FUNCTION_OFFSET << 8),"CALL_RET1_SUPER",1);
	 registerInOp(InOp::CALL_RET1_STATIC,       InOp::I8_ARGS | (InOp::I16_PAIR << 8),"CALL_RET1_STATIC",1);
	 registerInOp(InOp::CALL_RET1_STATIC_VTABLE,InOp::I8_ARGS | (InOp::I16_PAIR << 8),"CALL_RET1_STATIC_VTABLE",1);
	 registerInOp(InOp::CALL_RET1_DYNAMIC,      InOp::I8_ARGS ,"CALL_RET1_DYNAMIC",1 - 1);			// consumes the dynamic ptr
	 
	 registerInOp(InOp::CATCH_ENTRY,   0, "CATCH_ENTRY", 1);
	 registerInOp(InOp::NEW,           InOp::I16_PAIR,"NEW",      1);   // ..      =>  OBJECT
	 registerInOp(InOp::THROW,         0, "THROW",                -1);  // ERROR   =>  ..
	 registerInOp(InOp::JMP,           InOp::I16_JUMP,"JMP",      0);   // ..      =>  ..
	 registerInOp(InOp::INTERNAL_JMP_LO, InOp::I8_JUMP, "JMP_LO",  0);
	 registerInOp(InOp::INTERNAL_VECTOR_LENGTH,     0, "VECTOR_LENGTH", 0);		// no effect on height of the stack
	 registerInOp(InOp::INTERNAL_VECTOR_OBJECT_GET, 0, "VECTOR_GET_OBJECT", -1);		// 
	 registerInOp(InOp::INTERNAL_VECTOR_32_GET, 0, "VECTOR_32_GET", -1);		// 
	 registerInOp(InOp::INTERNAL_ARRAY_COMPARE, InOp::I8, "ARRAY_COMPARE", -1);		// compare the raw binary data
	 	 	 	 
	 registerInOp(InOp::J0_I32,        InOp::I16_JUMP,"J0_I32",  -1);   // BOOLEAN =>  ..
	 registerInOp(InOp::JN0_I32,       InOp::I16_JUMP,"JN0_I32", -1);   // BOOLEAN =>  ..
	 
	 registerInOp(InOp::AND,        InOp::I16_JUMP,"AND",  0);   // BOOLEAN =>  BOOLEAN
	 registerInOp(InOp::OR  ,       InOp::I16_JUMP,"OR",   0);   // BOOLEAN =>  BOOLEAN?
	 
	 registerInOp(InOp::ARRAY_LENGTH,0,"ARRAY_LENGTH",            0);   // ARRAY => COUNT
	 registerInOp(InOp::ARRAY_RESIZE,InOp::I8,"ARRAY_RESIZE",	-1);	// ARRAY, SIZE => ..
	 registerInOp(InOp::ARRAY_FILL,  InOp::I8,"ARRAY_FILL",	    -2);	// ARRAY, VALUE => ..
	 registerInOp(InOp::ARRAY_COPY,  InOp::I8,"ARRAY_COPY",	    -2);	
	 
	 registerInOp(InOp::ARRAY_LOAD_8,0,"ARRAY_LOAD_8",           -1);   // ARRAY,INDEX => VALUE
	 registerInOp(InOp::ARRAY_LOAD_16,0,"ARRAY_LOAD_16",          -1);  // ARRAY,INDEX => VALUE
	 registerInOp(InOp::ARRAY_LOAD_32,0,"ARRAY_LOAD_32",          -1);  // ARRAY,INDEX => VALUE
	 registerInOp(InOp::ARRAY_LOAD_64,0,"ARRAY_LOAD_64",          -1);  // ARRAY,INDEX => VALUE
	 registerInOp(InOp::ARRAY_LOAD_OBJ,0,"ARRAY_LOAD_OBJ",        -1);  // ARRAY,INDEX => VALUE
	 registerInOp(InOp::ARRAY_LOAD_FUNCPTR,0,"ARRAY_LOAD_FUNCPTR",-1);  // ARRAY,INDEX => VALUE
	 
	 registerInOp(InOp::ARRAY_STORE_8,0,"ARRAY_STORE_8",          -3);  // ARRAY,INDEX,VALUE => ..
	 registerInOp(InOp::ARRAY_STORE_16,0,"ARRAY_STORE_16",        -3);  // ARRAY,INDEX,VALUE => ..
	 registerInOp(InOp::ARRAY_STORE_32,0,"ARRAY_STORE_32",        -3);  // ARRAY,INDEX,VALUE => ..
	 registerInOp(InOp::ARRAY_STORE_32_REF,0,"ARRAY_STORE_32_REF",        -3);  // ARRAY,INDEX,VALUE => ..
	 registerInOp(InOp::ARRAY_STORE_64,0,"ARRAY_STORE_64",        -3);  // ARRAY,INDEX,VALUE => ..
	 registerInOp(InOp::ARRAY_STORE_OBJ,0,"ARRAY_STORE_OBJ",      -3);  // ARRAY,INDEX,VALUE => ..
	 registerInOp(InOp::ARRAY_STORE_FUNCPTR,0,"ARRAY_STORE_FUNCPTR",-3);// ARRAY,INDEX,VALUE => ..
	 
	 registerInOp(InOp::ARRAY_NEW,InOp::I16_PAIR | (InOp::I8 << 8),"ARRAY_NEW", 0); // COUNT => OBJECT
	 registerInOp(InOp::CAST_TO,InOp::I16_PAIR,"CAST_TO",            0);            // VALUE => VALUE
	 registerInOp(InOp::INSTANCE_OF, InOp::I16_PAIR, "INSTANCE_OF",  0);            // VALUE => BOOLEAN
	 registerInOp(InOp::STACK_TRACE, InOp::I16_PAIR,"STACK_TRACE",   1);            // ..    => STACK-TRACE-STRING
	 registerInOp(InOp::NEXT_IT,
		(InOp::I16_LOCAL                   << 0 ) |
		( InOp::I16_MEMBER_VARIABLE_OFFSET << 8 ) |
		( InOp::I16_FUNCTION_OFFSET        << 16) |
		( InOp::I16_JUMP                   << 24)
	 ,"NEXT_IT", 0);  
		 // .. => ..  (the height is increased by 1 if the jump is not executed)
	
	// need to tag locals on these next 2
	registerInOp(InOp::LOCAL_INC_I32, InOp::I8_LOCAL | (InOp::I8 << 8), "LOCAL_INC_I8",                    0);   // .. => ..
	registerInOp(InOp::STACK_INC_I32, InOp::I8 ,                  "STACK_INC_I8",                    0);   // .. => ..
	
	registerInOp(InOp::RET_FUNCPTR,  0, "RET_FUNCPTR", -1);   // VALUE => ..
	registerInOp(InOp::RET_OBJ,      0, "RET_OBJ",     -1);   // VALUE => ..
	registerInOp(InOp::JMP_IN_I32_0RANGE, InOp::I16_JUMP , "JMP_IN_I32_0RANGE", -2);
	registerInOp(InOp::DEBUG_BLOCK_START, 0, "DEBUG_BLOCK_START", 0);
	registerInOp(InOp::DEBUG_BLOCK_END, 0, "DEBUG_BLOCK_END", 0);		
	
	// Dec 14, 2015
	registerInOp(InOp::PUSH_F64_0,	0,	"PUSH_F64_0", 1);	 // .. => VALUE
	registerInOp(InOp::PUSH_F64_1,	0,	"PUSH_F64_1", 1);	 // .. => VALUE
	
	return 0;
}








struct StackFrame
{
	StackFrame          * m_prev;		// pointer to the previous stack frame
	Box                 * m_sp;			// sp should be minimal for try/catch
	Box                 * m_bp;			// bp should be minimal for try/catch

	Function            * m_function;	// the function currently being executed
	unsigned char       * m_pc;         // the pc to return to (try/catch initial block)

	static const char * name() { return "StackFrame"; }
};


struct Stack
{
	static const int CAPACITY            = (64 * 1024 - 32) / 8;
	static const int STACK_SIZE_IN_BYTES = (64 * 1024 - 32);

	// for tracking the current active stacks
	Stack      * m_prev;
	Stack      * m_next;
	
	StackFrame * m_frame;
	Box *        m_current_sp;

	// allocate room.  128k - 32bytes  // upped 23 sept 2015
	Box			 m_stack[CAPACITY];
	

	static const char * name() { return "Stack"; }
};


/**

 */

class VMImp: public VMInstance
{
public:
	IGVMHeap             m_heap;

	VMArraySet<ig_u32>   m_load_stack;
	VMArraySet<Module *> m_link_stack;
	VMArraySet<ig_u32>   m_init_stack;

	// scope stack
	ig_u32           * m_scope;
	ig_u32             m_scope_index;
	ig_u32             m_scope_top;
	ig_i32             m_scope_capacity;

	// last error message
	char			   m_error_message[MAX_ERROR_MESSAGE_LENGTH];
	int                m_error_root_idx;
	int                m_debug_config;

	// constant pool values for built in types
	ig_u32               m_cp_bool;
	ig_u32               m_cp_double;
	ig_u32               m_cp_int;

	// pairs
	PairEntry        * m_pair_hash[5167];
	ig_u32             m_pair_hash_size;// = 5167;
	HEAP<Item *>	   m_pairs;            // the value
	HEAP<PairEntry *>  m_pair_components;  // the components that make up the pair

	// strings
	StringEntry      *   m_string_hash[5167];
	ig_u32               m_string_hash_size;// = 5167;
	HEAP<char>			 m_strings;
	HEAP<ig_u32>		 m_string_offsets;

	// static
	HEAP<char>		          m_static_data;
	

	//
	CHUNK_POOL<Stack,2>      m_stack_pool;
	Stack *                  m_stack_active_head;
	Stack *                  m_stack_active_tail;

	// allocator to use for accessing memory
	VMAllocator              m_allocator;
	VMAllocator              m_allocator_heap;
	// configuration currently specified for this instance
	Config                   m_config;

	int s_reinit_count;
	bool s_reinit_occupied;
	
	
	///////////////////////////////////////////////////////////
	// Functions
	///////////////////////////////////////////////////////////
	
	// Allocate a stack
	Stack * allocStack();
	
	// Count the number of active stacks
	int countActiveStacks();
	
	// Deallocate a stack
	void deallocStack(Stack * stack );
	/*
	 * Create a new handle with a specified garbage collection storage class
	 */

	//static ig_u32 createHandle(Object * ptr, int gc_class);
	//static void   destroyHandle(ig_u32 handle);



	 int invoke(Stack * stack, Function * fn, ig_i8 argc, const Box * args, Box * ret);
	 int process(char * code);  // NYI
	 int _processModuleCode(Module & m, int mode);
	 int _loadModuleInternal(const char * path, int height);

	 static ig_u32 _hashString(const char * utf8_string, size_t byte_length);

	/*
	 * Find (and create if requested) a string id out of a cstring with a given length.
	 */

	 ig_u32 _constantString(const char * utf8_string, size_t byte_length, bool create);

	/*
	 * Clear the constant string hash, freeing all memory and setting everything to NULL
	 */
	 void   _constantStringReinit();

	/*
	 * Clear the constant pool hash, freeing all memory and setting everything to null
	 */
	 void   _constantPairReinit();

	/*
	 * Search for (and create if requested) a constant pair constructed out of two string ids
	 */
	 ig_u32 _constantPair(ig_u32 string_1_id, ig_u32 string_2_id, bool create);

	/*
	 * Extract the string ids from a constant pair
	 */
	 void  _explodeConstantPair(ig_u32 pair, ig_u32 string_ids[2]);

	/*
	 * Validate that a constantPair exists for the given class and function name.
	 */

	int    _validatePair(const char * klass, const char * fn, int t0, int t1, Item ** item_out);

	void _constantPairDebug(ig_u32 offset, int mode = 0);
	void _heapRequestedGC();
	void _forceGC(bool stop_the_world);

	/*
	 * Convert a string id back into its character representation
	 * @returns - a cstring
	 */

	 const char * _getStringForId(ig_u32 string_id);
	
	
	 /**
	  * API IMPLEMENTATIONS.  see .h for usage
	  */
	
	 int          _stringFromUTF8Bytes(const char * text, Box * result) ;
	 int          _stringFromUTF8BytesWithLength(const char * text, size_t length, Box * result) ;
	 int          _UTF8BytesFromString(const Box & str, char * utf8, int capacity);
	 int          _U32FromString(const Box & str, ig_u32 * dst, int capacity);
	 int          _UTF8ByteLengthOfString(const Box & str, int * length);
	 int          _stringLength(const Box & str, int * out_length);
	 int          _call(const char * klass, const char * fn_name, const Box * parameters,  int count, Box * ret);
	 int          _callFunctionPointer(const Box & fp, const Box * parameters,  int count, Box * ret);
	 int          _callNamed(const char * fn_name, const Box * parameters,  int argc, Box * ret);
	 int          _createByteArray(const void * data, int length, Box * out);
	 
	 
	 int          _newObject(const char * klass_name, const Box * parameters,  int argc, Box * ret);
	 const char * _getErrorMessage();
	 int          _setNativeFunction(const char * klass, const char * fn_name,  int (*native_fn)(VMInstance * vm, const Box * params, int count, Box * ret));	
	 void         _beginScope();
	 void         _endScope();	 
	 void         _pushObjectToScope(ig_u32 handle);
	 int          _loadModule(const char * path_in);		 
	 int          _moduleToPair(const char * klass, ig_u32 * pair);
	 int          _getClassName(const Box & object, char * out_text, int out_capacity);
	 void         _stackTrace2();
	 void         _stackTrace(StackFrame * frame,Box * bp,  char * data, int MAX_STACK_TRACE);
	 int          _debugConfig(int );		 
	 int          _getByteArray(const Box & handle, ByteArray * array, int set_length);
	 int          _getArray(const Box & handle, Array * array);
	 int          _getField (const char * klass, const char * field, const Box & obj_handle, Box * result) ;
	 int 		  _setField (const char * klass, const char * field, const Box & obj_handle, const Box & data);
	

		int          _getMemberVariableOffset(const char * klass, const char * field,  int * offset);
	 	int          _getMemberData          (const Box & object, int offset, int length, void * data);
		int          _setMemberData          (const Box & object, int offset, int length, const void * data);
	 	int          _getArrayDataOffset(int * offset) ;
	
	 int          _loadBuiltins();

	 int          _init(const Config * config);
	 int          _reinit(bool setup = true);
	 Config *     _getConfig();

	 int   setNativeDestructor(const char * klass, void (*native_fn)(Box obj));
	 
	 int   objectInstanceOf(const char * klass, const Box & object, bool * result);
	 
	 
	 int typeExtends(const char * child_klass, const char * parent_klass, bool * result);
	 //int    newObject(const char * klass,  const Box * parameters,  int argc, Box & ret);
	
	 int  describeMemberVariable
		(const char * klass, const char * field, bool drill_down,
		 char * out_type,  int out_type_length,
		 char * out_exact_type,  int out_exact_type_length, 
		   int * out_offset,  int * out_size);
	
	
	
	
	
	Root _allocRoot();
	void _freeRoot(Root * root);
	void _clearRoot(Root * root);
	void _setRoot(Root * root, const Box & value);
	Box  _getRoot(Root * root);
	
	int  errorOccurred(int code);
	
	void   _fillArray(ig_u32 obj_handle, Object * obj, int storage_class, Box  value);
	void   _copyArray(ig_u32 dst_handle, Object * dst, int storage_class, ig_u32 src_handle, Object * src);
	void   _compareArray(int storage_class, Object * src,  Object * dst, int * result);
	
	
	ig_u32 _reallocArrayInstance(ig_u32 handle, Object * obj,  int count, int category);
	ig_u32 _createModuleInstance(Module * module);
	ig_u32 _createModuleArrayInstance(Module * module, int count, int category);

	static void objectDestructor(void * user_p, ig_u32 handle);
	
	Function * _getErrorFunction(const char * klass, const char * func);
};



/**
 * UGH.  This is not thread safe.
 */

Stack * VMImp::allocStack() 
{
	Stack * tmp = m_stack_pool.alloc();
	
	// insert into the active linked list
	if (m_stack_active_head == NULL) 
	{
		m_stack_active_head = tmp;
		m_stack_active_tail = tmp;
		tmp->m_prev = NULL;
		tmp->m_next = NULL;
	}
	else {
		
		tmp->m_next = m_stack_active_head;
		m_stack_active_head->m_prev = tmp;
		
		m_stack_active_head = tmp;
	}
	return tmp;
}

int VMImp::countActiveStacks() {
	int count = 0;
	
	Stack * tmp = m_stack_active_head;
	while (NULL != tmp) {
		count++;
		tmp = tmp->m_next;
	}
	return count;
}

void VMImp::deallocStack(Stack * stack ) 
{
	if (stack->m_prev != NULL) {
		stack->m_prev->m_next = stack->m_next;
	}
	if (stack->m_next != NULL) {
		stack->m_next->m_prev = stack->m_prev;
	}
	if (m_stack_active_head == stack) {
		m_stack_active_head = stack->m_next;
	}
	if (m_stack_active_tail == stack) {
		m_stack_active_tail = stack->m_prev;
	}

	m_stack_pool.dealloc(stack);
}


////////////////////////////////////
// Object interface functions
///////////////////////////////////
 
int VMInstance::call(const char * klass, const char * method, const igvm::Box * params, int param_count, igvm::Box * ret) {
	return ((VMImp *)this)->_call(klass,method,params,param_count,ret);
}

int VMInstance::callNamed(const char * method, const igvm::Box * params, int param_count, igvm::Box * ret) {
	return ((VMImp *)this)->_callNamed(method,params,param_count,ret);
}

int VMInstance::callFunctionPointer(const igvm::Box & func_ptr, const igvm::Box * params, int param_count, igvm::Box * ret) {
	return ((VMImp *)this)->_callFunctionPointer(func_ptr,params,param_count,ret);
}

int VMInstance::newObject(const char * klass_name, const igvm::Box * params, int param_count, igvm::Box * ret) {
	return ((VMImp *)this)->_newObject(klass_name,params,param_count,ret);
}

int VMInstance::stringFromUTF8Bytes(const char * text, Box * r)  {
	return ((VMImp *)this)->_stringFromUTF8Bytes(text, r);
}

int VMInstance::stringFromUTF8BytesWithLength(const char * text, int byte_len, Box * r)  {
	return ((VMImp *)this)->_stringFromUTF8BytesWithLength(text, byte_len, r);
}

int VMInstance::UTF8BytesFromString(const Box & str, char * utf8, int capacity) {
	return ((VMImp *)this)->_UTF8BytesFromString(str, utf8, capacity);
}

int VMInstance::U32FromString(const Box & str, ig_u32 * dst, int capacity) {
 	return ((VMImp *)this)->_U32FromString(str, dst, capacity);
}

int VMInstance::UTF8ByteLengthOfString(const Box & str, int * len) {
	return ((VMImp *)this)->_UTF8ByteLengthOfString(str, len);
}

int VMInstance::stringLength(const Box & str, int * out_length) {
	return ((VMImp *)this)->_stringLength(str,out_length);
}

const char * VMInstance::getErrorMessage() {
	return ((VMImp *)this)->_getErrorMessage();
}

int VMInstance::debugConfig(int v) {
	return ((VMImp *)this)->_debugConfig( v);
}

void   VMInstance::gc(bool stop_the_world) {
	return ((VMImp *)this)->_forceGC(stop_the_world);
}

int  VMInstance::setNativeFunction(const char * module_name, const char * function_name,
			int (*fn)(VMInstance * vm, const Box * params, int param_count, Box * ret)) {
	return ((VMImp *)this)->_setNativeFunction(module_name, function_name, fn);			
}
			
void VMInstance::beginScope() {
	((VMImp *)this)->_beginScope();
}			

void VMInstance::endScope() {
	((VMImp *)this)->_endScope();
}			
			
int VMInstance::getClassName(const Box & object, char * out_text, int out_capacity) {
	return ((VMImp *)this)->_getClassName(object, out_text, out_capacity);
}	

int VMInstance::getByteArray(const Box & handle, ByteArray * data, int set_length) {
	return ((VMImp *)this)->_getByteArray(handle, data, set_length);
}

int VMInstance::createByteArray(const void * data, int len, Box * out) {
	return ((VMImp *)this)->_createByteArray(data, len, out);
}

int VMInstance::getArray(const Box & handle, Array * data) {
	return ((VMImp *)this)->_getArray(handle, data);
}

int VMInstance::getField(const char * klass, const char * fld, const Box & handle, Box * data) {
	return ((VMImp *)this)->_getField(klass, fld, handle, data);
}

int VMInstance::setField(const char * klass, const char * fld, const Box & handle, const Box & data) {
	return ((VMImp *)this)->_setField(klass, fld, handle, data);
}

Root VMInstance::allocRoot() {
	return ((VMImp *)this)->_allocRoot();
}
void VMInstance::freeRoot(Root * root) {
	((VMImp *)this)->_freeRoot(root);
}
void   VMInstance::clearRoot(Root * root) {
	((VMImp *)this)->_clearRoot(root);
}
void   VMInstance::setRoot(Root * root, const Box & value) {
	((VMImp *)this)->_setRoot(root, value);
}
Box   VMInstance::getRoot(Root * root) { 
	return ((VMImp *)this)->_getRoot(root);
}

/////////////////////////////////////
// Static interface functions
/////////////////////////////////////

int VM::getField (const char * klass, const char * field, const Box & obj_handle, Box * result) {
	return s_imp->_getField(klass, field, obj_handle, result); 
}
int VM::getMemberData          (const Box & object, int offset, int length, void * data) {	
	return s_imp->_getMemberData(object, offset, length, data);
}
int VM::setMemberData          (const Box & object, int offset, int length, const void * data) {	
	return s_imp->_setMemberData(object, offset, length, data);
}
int VM::getArrayDataOffset(int * offset) {
	return s_imp->_getArrayDataOffset(offset);
}
int VM::stringFromUTF8Bytes(const char * text, Box * result)  {
	return s_imp->_stringFromUTF8Bytes(text, result);
}
int VM::stringFromUTF8BytesWithLength(const char * text, int len, Box * result)  {
	return s_imp->_stringFromUTF8BytesWithLength(text, len, result);
}
int VM::UTF8BytesFromString(const Box & str, char * utf8, int capacity) {
	return s_imp->_UTF8BytesFromString(str, utf8, capacity);
}
int VM::U32FromString(const Box & str, ig_u32 * utf8, int capacity) {
	return s_imp->_U32FromString(str, utf8, capacity);
}
int VM::UTF8ByteLengthOfString(const Box & str, int * len) {
	return s_imp->_UTF8ByteLengthOfString(str, len);
}
int VM::stringLength(const Box & str, int * out_length) {
	return s_imp->_stringLength(str, out_length);
}
int VM::loadModule(const char * path_in) {	
	return s_imp->_loadModule(path_in);
}
int VM::getByteArray(const Box & handle, ByteArray * array, int set_length) {
	return s_imp->_getByteArray(handle, array, set_length);
}
int VM::debugConfig(int v) {
	return s_imp->_debugConfig(v);
}
int VM::getArray(const Box & handle, Array * array) {
	return s_imp->_getArray(handle, array);
}
int VM::setField (const char * klass, const char * field, const Box & obj_handle, const Box & data) {
	return s_imp->_setField(klass, field, obj_handle, data);
}
void VM::forceGC(bool stop_the_world) {
	s_imp->_forceGC(stop_the_world);
}
int VM::loadBuiltins() {
	return s_imp->_loadBuiltins();
}
int VM::setNativeFunction(const char * klass, const char * fn_name, int (*native_fn)(VMInstance * instance, const Box * params, int param_count, Box * ret)) {
	return s_imp->_setNativeFunction(klass, fn_name, native_fn);
}
void VM::beginScope() {
	s_imp->_beginScope();
}
void VM::endScope() {
	s_imp->_endScope();
}
int VM::setNativeDestructor(const char * klass,  void (*native_fn)(Box param)) {
	return s_imp->setNativeDestructor(klass, native_fn);
}
int VM::callNamed(const char * fn_name, const Box * parameters, int argc, Box * ret) {
	return s_imp->_callNamed(fn_name, parameters, argc, ret);
}
int VM::newObject(const char * klass_name, const Box * parameters, int argc, Box * ret) {
	return s_imp->_newObject(klass_name, parameters, argc, ret);
}
int VM::objectInstanceOf(const char * klass, const Box & object, bool * result) {
	return s_imp->objectInstanceOf(klass, object, result);
}
int VM::getClassName(const Box & object, char * out_text, int out_capacity) {
	return s_imp->_getClassName(object, out_text, out_capacity);
}
int VM::typeExtends(const char * child_klass, const char * parent_klass, bool * result) {
	return s_imp->typeExtends(child_klass, parent_klass, result);
}
int VM::call2(const char * klass, const char * fn_name, const Box * parameters, int count, Box * ret) {
	return s_imp->_call(klass, fn_name, parameters, count, ret);
}
int VM::callFunctionPointer(const Box & fp, const Box * parameters, int count, Box * ret) {
	return s_imp->_callFunctionPointer(fp, parameters, count, ret);
}
int VM::describeMemberVariable
			(const char * klass, const char * field, bool drill_down,
			 char * out_type, int out_type_length,
			 char * out_exact_type, int out_exact_type_length, 
			  int * out_offset, int * out_size) {
	return s_imp->describeMemberVariable(klass, field, drill_down, out_type, out_type_length, out_exact_type, out_exact_type_length, out_offset, out_size);
}
int VM::getMemberVariableOffset(const char * klass, const char * field, int * offset) {
	return s_imp->_getMemberVariableOffset(klass, field, offset);
}
const char * VM::getErrorMessage() {
	return s_imp->_getErrorMessage();
}


Root VM::allocRoot() {
	return s_imp->_allocRoot();
}
void VM::freeRoot(Root * root) {
	s_imp->_freeRoot(root);
}
void   VM::clearRoot(Root * root) {
	s_imp->_clearRoot(root);
}
void   VM::setRoot(Root * root, const Box & value) {
	s_imp->_setRoot(root, value);
}
Box   VM::getRoot(Root * root) { 
	return s_imp->_getRoot(root);
}

// Initializing/allocation of state
///////////////////////////////////////////////////////////

int VM::globalInit(const Config * config, int flags) {

	// no configuration specified
	if (config == NULL) {
		return 1;
	}
	
	s_global_config = *config;
	
	int code = registerInOps();
	if (code != 0) {
		return code;
	}
	
	srand(time(NULL));
	
	s_global_init = true;
	
	if (0 == (flags & NO_DEFAULT_INSTANCE)) {
		s_imp = (VMImp *) VM::allocInstance();
	}	
	
	return 0;
}


int VMInstance::init(const Config * config, int flags) 
{
	//printf("VMInstance init A\n");
	// call the global initializer if it hasn't been previously invoked
	if (!s_global_init) {
		int code = VM::globalInit(config, 0);
		if (code != 0) {
			return code;
		}
	}

	VMImp * imp = (VMImp *) this;

	//printf("VMInstance init B\n");
	
	// use the global init if none has been specified
	int code = imp->_init(config == NULL ? &s_global_config : config);
	if (code != 0) { return code; }
	
	//printf("VMInstance init C\n");	
	
	
	if (flags & LOAD_BUILTINS) {
		//printf("VMInstance init D\n");
		return imp->_loadBuiltins();
	}
	
	return 0;
}

Config * VMInstance::getConfig()
{
	VMImp * imp = (VMImp *) this;
	return imp->_getConfig();
}

Config * VMImp::_getConfig()
{
	return &m_config;
}

int VMInstance::reinit(int flags) {

	VMImp * imp = (VMImp *) this;

	int code = imp->_reinit(true);
	if (code != 0) { return code; }
	if (flags & LOAD_BUILTINS) {
		return imp->_loadBuiltins();
	}
	return 0;
}


int VM::init(const Config * config, int flags) 
{
	// call the global initializer if it hasn't been previously invoked
	if (!s_global_init) {
		int code = VM::globalInit(config, 0);
		if (code != 0) {
			return code;
		}
	}

	// use the global init if none has been specified
	int code = s_imp->init(config == NULL ? &s_global_config : config);
	if (code != 0) { return code; }
	if (flags & LOAD_BUILTINS) {
		return s_imp->_loadBuiltins();
	}
	
	return 0;
}

int VM::reinit(int flags) {
	int code = s_imp->_reinit(true);
	if (code != 0) { return code; }
	if (flags & LOAD_BUILTINS) {
		return s_imp->_loadBuiltins();
	}
	return 0;
}

VMInstance * VM::allocInstance()
{
	VMImp * imp = (VMImp *) s_global_config._malloc(NULL, sizeof(VMImp));
	memset(imp, 0, sizeof(VMImp));
	
	// should these just be constants somewhere?
	imp->m_pair_hash_size = 5167;
	imp->m_string_hash_size = 5167;
	return imp;	
}

void         VM::setActiveInstance(VMInstance * instance)
{
	s_imp = (VMImp *) instance;
}

VMInstance * VM::getActiveInstance() 
{
	return s_imp;
}

void         VM::freeInstance(VMInstance * i) {
	
	
	//	m_heap.init(0,0,0);
	//m_error_root_idx = m_heap.allocAndClearStaticRoot();


	{
		VMImp * i2 = (VMImp *) i;
		
		// do a reinit, but do not set up for a load
		i2->reinit(false);

		i2->m_static_data      .destroy();
		i2->m_strings          .destroy();
		i2->m_string_offsets   .destroy();
		i2->m_pairs            .destroy();
		i2->m_pair_components  .destroy();
	
		i2->m_stack_pool       .destroy();
		i2->m_load_stack       .destroy();
		i2->m_link_stack       .destroy();
		i2->m_init_stack       .destroy();

		i2->m_heap.destroy();
	}
	
	
	// free the VMInstance struct
	s_global_config._free(NULL, i);
	
	//dieAPainfullDeath("Haven't properly implemented freeInstance");
	
	// if the instance being freed is active.  Set the active instance to null
	if (s_imp == i) {
		s_imp = NULL;
	}
	
	//free(i);
}
	


///////
// Implementations

Root   VMImp::_allocRoot()
{
	Root ret = { m_heap.allocDynamicRoot() };
	
	if (ret.m_root == 0) {
		fprintf(stderr, "FAILED VMImp::allocRoot\n");
	}
	return ret;
}

void   VMImp::_freeRoot(Root * root)
{
	if (root->m_root == 0) {
		fprintf(stderr, "FAILED VMImp::freeRoot\n");
	}

	m_heap.freeDynamicRoot(root -> m_root);
	root->m_root = 0;
}

void   VMImp::_clearRoot(Root * root)
{
	if (root->m_root == 0) {
		fprintf(stderr, "FAILED VMImp::clearRoot\n");
		printStackTrace();
	}

	//printf(" VMImp::clearRoot clear root: %d\n", root->m_root);
	Box * r = m_heap.getStaticRoot(root->m_root);
	memset(r, 0, sizeof(Box));
	//root->m_root = 0;
}

void   VMImp::_setRoot(Root * root, const Box & value)
{
	if (root->m_root == 0) {
		fprintf(stderr, "FAILED VMImp::setRoot\n");
	}
	
	Box * r = m_heap.getStaticRoot(root->m_root);
	*r = value;
//	memcpy(r, &value, sizeof(Box));

	if (value.u32 != 0) {
		m_heap.whiteToGrey(value.u32);
	}
}

Box   VMImp::_getRoot(Root * root)
{
	if (root->m_root == 0) {
		fprintf(stderr, "FAILED VMImp::getRoot\n");
	}
	
	return *m_heap.getStaticRoot(root->m_root);
}



#if IGVM_GC_PROFILING == 1		
static VMTracker s_tracker_allocs;
static VMTracker s_tracker_old_frees;
static VMTracker s_tracker_young_frees;
#endif	


const char * VMImp::_getErrorMessage()
{
	Box error = *m_heap.getStaticRoot(m_error_root_idx);
	if (error.u32 != 0)
	{
		// convert the error object to a string
		Box str;

		// get the error message
		this->_call("iglang.Error", "toString", &error, 1, &str);
		this->_UTF8BytesFromString(str, m_error_message, MAX_ERROR_MESSAGE_LENGTH);

		//VM::decRef(m_error_object);
		//m_error_object.u32 = 0;
	}
	else {
		m_error_message[0] = '\0';
	}
	return m_error_message;
}


struct StringEntry
{
	ig_u32           m_offset;
	ig_u32           m_hash;
	ig_i32           m_length;
	StringEntry    * m_next;		// for chaining in the hashtable
};

struct PairEntry
{
	ig_u32           m_p0;			// StringEntry offset
	ig_u32           m_p1;			// StringEntry offset
	ig_u32           m_offset;		// the pair index
	PairEntry      * m_next;		// for chaining in the hashtable
};



struct Item
{
	static const ig_i8 MEMBER_VARIABLE = 1;
	static const ig_i8 MEMBER_FUNCTION = 2;
	static const ig_i8 STATIC_VARIABLE = 3;
	static const ig_i8 STATIC_FUNCTION = 4;
	static const ig_i8 MODULE   		= 5;
	static const ig_i8 STRING           = 6;
	ig_i8       m_type;
};

struct String: public Item
{
	ig_u32 m_handle;
};


// class for managing groups of data
// data is never deleted







struct VTable
{
	VTable   * m_super;
	Module   * m_module;
	Function * m_functions[1];
};





struct Object: public ObjectHeader
{
		// force alignment

	// actually this probably isn't correct
	
	
	inline ig_i64 & i64(int offset) {
		char * p0 = (char *)this;
		return *(ig_i64 *)(p0 + offset);
	}

	inline ig_i32 & i32(int offset) {
		char * p0 = (char *)this;
		return *(ig_i32 *)(p0 + offset);
	}
	
};




//////////////////////////////////////
// start igc file format
//////////////////////////////////////

struct ModuleHeader
{
	ig_i32 m_header;
	ig_i32 m_version;


	ig_i32 m_name;
	ig_i32 m_type;
	ig_i32 m_extends;
	
	ig_i32 m_implements_count;
	ig_i32 m_implements[16];

	ig_u8 m_storage_class;			// NOT USED
	ig_u8 m_array_storage_class;	// set on an instance of ArrayNew
	ig_u8 m_padding2;				// NOT USED	
	ig_u8 m_padding3;				// NOT USED

	ig_i32 m_static_variable_count;
	ig_i32 m_member_variable_count;
	ig_i32 m_static_function_count;
	ig_i32 m_member_function_count;

	ig_i32 m_pair_pool_entries;			// # of entries
	ig_i32 m_string_entries;			// # of entries
	ig_i32 m_string_pool_size;
	ig_i32 m_data_pool_size;

	// static variables
	// member variables
	// static methods
	// member methods

	// pair pool
	// string lengths
	// string data
	// raw data
};


struct VariableHeader
{
	ig_i32 m_name;				// string pool index to the name
	ig_i32 m_type;				// string pool index to the type
	ig_i32 m_exact_type;		// string pool index to the exact type
	ig_i16 m_protection;		// protection to apply to this variable
	ig_i16 m_storage_class;		// 16 = object, 17 = function pointer, 18 = 32 bit reference count
								// 1 = byte, 2 = short, 4 = int, 8 = double
};

struct ExceptionTableEntry
{
	ig_u16	m_pc_start;		// this is the start of the try block
	ig_u16  m_pc_end;			// this is where the handlers are

	bool isWithin(int offset) {
		if (offset < m_pc_start || m_pc_end <= offset) {
			return false;
		}
		return true;
	}
};

struct DebugTableEntry
{
	ig_u16  m_offset;
	ig_u16  m_line_no;
	ig_u16  m_temp0;
	ig_u16  m_temp1;
};

struct JumpTableEntry
{
	ig_i16 m_ref_stack_height;	
	ig_i16 m_stack_height;
	ig_i32 m_offset;			// <- what offset this jump table points to	
};

struct FunctionTableCompactor
{
	ExceptionTableEntry * m_exceptions;
	int m_exception_count;
	
	DebugTableEntry     * m_debug;
	int m_debug_count;
	
	JumpTableEntry      * m_jump;
	int m_jump_count;
	
	int                   m_pc_curr;
	
public:
	FunctionTableCompactor(ExceptionTableEntry * e, int ec,
						   DebugTableEntry     * d, int dc,
						   JumpTableEntry      * j, int jc) 
	{
		m_exceptions = e;
		m_exception_count = ec;
		
		m_debug = d;
		m_debug_count = dc;
		
		m_jump = j;
		m_jump_count = jc;		   

		m_pc_curr = 0;						   
	}
	
	void inc(int count) {
		m_pc_curr += count;
	}
	
	void skip(int count) {
		for (int i = m_jump_count - 1; i >= 0; i--) {
			if (m_jump[i].m_offset > m_pc_curr) {
				m_jump[i].m_offset -= count;
			}
		}
		
		for (int i = m_debug_count - 1; i >= 0; i--) {
			if (m_debug[i].m_offset > m_pc_curr) {
				m_debug[i].m_offset -= count;
			}
		}
		
		
		
		
		for (int i = m_exception_count - 1; i >= 0; i--) 
		{
			// pc start is inclusive
			// pc end is exclusive
			if (m_exceptions[i].m_pc_start > m_pc_curr) {
				m_exceptions[i].m_pc_start -= count;
			}
			
			// [0,10)
			// @9  want to deduct 1.  pc_end should not change
			if (m_exceptions[i].m_pc_end - 1 > m_pc_curr) {
				m_exceptions[i].m_pc_end -= count;
			}
		}
	}
};	

struct FunctionHeader
{
	/*0*/
	ig_i32 m_name;

	ig_i16 m_arg_min;
	ig_i16 m_arg_max;
	/*8*/
	ig_i16 m_returns;
	ig_i16 m_padding;

	ig_i32 m_data_offset;
	/*16*/
	ig_i32 m_data_length;

	ig_i32 m_parameter_count;
	/*32*/
	ig_i32 m_local_count;			// the number of locals including arguments that this function has
	ig_i32 m_code_count;
	/*40*/
	ig_i32 m_exception_count;		// the # of exception entries
	ig_i32 m_debug_count;			// the # of debug entries
	/*48*/
	ig_i32 m_jump_table_length;		// the # of jump table entries
	
	/**
		Order of function data:
			LOCAL DEFAULTS
			CODE
			EXCEPTION TABLE
			DEBUG TABLE
			JUMP TABLE
	
	
	 **/

	Box * getLocalPointer(unsigned char * base) {
		return (Box *) &base[m_data_offset];
	}

	unsigned char * getCodePointer(unsigned char * base) {
		return (unsigned char *) &base[m_data_offset + m_local_count*sizeof(ig_i64)];
	}

	ExceptionTableEntry * getExceptionPointer(unsigned char * base) {
		return (ExceptionTableEntry *) &base[m_data_offset + m_local_count*sizeof(ig_i64) + m_code_count];
	}

	DebugTableEntry * getDebugPointer(unsigned char * base) {
		return (DebugTableEntry *) &base[m_data_offset + 
										 m_local_count*sizeof(ig_i64) + 
										 m_code_count + 
										 m_exception_count*sizeof(ig_i32)];
	}

	JumpTableEntry * getJumpTablePointer(unsigned char * base) {
		return (JumpTableEntry *) &base[m_data_offset + 
										 m_local_count*sizeof(ig_i64) + 
										 m_code_count + 
										 m_exception_count*sizeof(ig_i32) +
										 m_debug_count * sizeof(DebugTableEntry)];
	}

};


///////////////////////////////////
// end igc file format
///////////////////////////////////


// cache lines are technically 64 bytes on i7
struct Function: Item
{
	/*1*/
	// likely 3 bytes missing here due to the item Header
	ig_u8 	         m_local_count;
	ig_u8            m_param_count;
	ig_u16		     m_max_stack_size;					// maximum stack entry count required for this function (includes locals)
	
	/*8*/   unsigned char *  m_code;							// pointer to the code to be executed
	/*16*/  Box           *  m_local_defaults;					// default values for locals and optional paramters
	/*32 */ int (*m_native)(VMInstance * vm, const Box * params, int param_count, Box * ret);		// native function to execute instead



	FunctionHeader m_header;			// header as read from disk
	

	
	ig_u32         m_name;				// string const denoting the name of the function (excludes scope)
	ig_u32 		   m_pair;  			// the pair that identifies this function
	ig_u32		   m_offset;			// the slot in the vtable, or nothing if its static
										// m_type can be used to differentiate between function types
	
	Module *       m_module;			// what module the function is a member of

	ig_i32         m_called_ops;
	ig_i32         m_called_allocs;
	ig_i32         m_called_invocations;
	
	// should this just be separate?
	ExceptionTableEntry * m_exception_table;
	DebugTableEntry     * m_debug_table;
	JumpTableEntry      * m_jump_table;
	
	inline ig_i32 getLocalCount() { return m_header.m_local_count; }
	inline ig_i32 getParameterCount() { return m_header.m_parameter_count; }
};


static int s_storage_class_size[]  = {0,
							sizeof(ig_i8), 	// 1 = byte
							sizeof(ig_i16),	// 2 = short
							0,
							sizeof(ig_i32),	// 4 = int
							0,0,0,
							sizeof(ig_i64),	// 8 = double
							0,0,0,0,0,0,0,
							sizeof(ig_i32), // 16 = object pointer
							sizeof(ig_i64),	// 17 = function pointer
							sizeof(ig_i32)  // 18 = 32 bit int ref
							};
	

struct Variable: Item
{
	VariableHeader m_header;
	ig_u32         m_pair;
	ig_u32         m_name;
	ig_i32         m_type_pair;
	ig_i32		   m_exact_type_name;
	ig_i32         m_offset;   		// offset into the module

	static const int STORAGE_CLASS_8       = 1;
	static const int STORAGE_CLASS_16      = 2;
	static const int STORAGE_CLASS_32      = 4;
	static const int STORAGE_CLASS_64      = 8;
	static const int STORAGE_CLASS_OBJECT  = 16;
	static const int STORAGE_CLASS_FUNCPTR = 17;
	static const int STORAGE_CLASS_32_REFCNT = 18;
	/*
	 * Returns the number of bytes required to store a variable of this type
	 */

	int getStorageSize() {
	
		if (m_header.m_storage_class == STORAGE_CLASS_FUNCPTR || m_header.m_storage_class == STORAGE_CLASS_64) {
			return 8;
		}
		else {
			return 4;
		}
		//return 8;		// for now everything is 8 bytes... deal with it
	}
	
	int getStorageClass() { return m_header.m_storage_class; }

	/*
	 * Returns the offset, if and only if the data stored has a reference to a pointer
	 */

	int getHandleOffset() {
		if (m_header.m_storage_class == STORAGE_CLASS_OBJECT || m_header.m_storage_class == STORAGE_CLASS_FUNCPTR) {
			return m_offset;
		}
		else
		{
			return -1;
		}
	}

};



/*
 * This is organized so that the common info is at the top
 */
struct Module: Item
{
	// START GC INFO
	////////////////////////////////////////
	ig_i32   m_gc_object_handle_count;
	ig_i16 * m_gc_object_offsets;
	
	ig_i32   m_ref_count_field_count;
	ig_i16 * m_ref_count_fields;
	
	void (*m_native_destructor)( Box params);  // native function to call upon gc
	Module   * m_extends_module;		       // what module does this module extend
	//END GC INFO

	// START INSTANTIATING INFO
	////////////////////////////////////////
	// how big an object of this type is
	// the first bit of data contains a pointer to the vtable
	// vtable
	ig_i32 m_instance_size;
	char * m_instance_defaults;
	// also uses: m_gc_object_handle_count

	ig_i32 m_allocations;



	// END INSTANTIATING INFO







	ModuleHeader   m_header;
	ig_u32 		   m_pair;  // the pair that identifies this function

	bool		   m_statics_initialized;

	// the first entry in the vtable points to the vtable of the parent
	ig_i32     m_vtable_size;
	VTable   * m_vtable;

	

	
	

	Function * m_all_functions;




	Variable * m_static_variables;
	Variable * m_member_variables;

	// arrays that remap the internal code table to the global code table
	ig_u32  * m_string_remap;
	ig_u32  * m_pair_remap;
	unsigned char * m_data_pool;
	unsigned char * m_final_data_pool;

	// convenience pointers
	Function * m_static_functions;   // DO NOT FREE
	Function * m_member_functions; 	 // DO NOT FREE


	bool isInstanceOf(ig_u32 other_pair) {
		if (m_pair == other_pair) {
			return true;
		}
		if (m_extends_module == NULL) {
			return false;
		}
		return m_extends_module->isInstanceOf(other_pair);
	}
};




ig_u32 VMImp::_createModuleInstance(Module * module) 
{
	int gc_type = (module->m_gc_object_handle_count == 0) ? GC_LEAF : GC_OBJECT;
	return m_heap.allocHandle( module->m_instance_size, module->m_instance_defaults, gc_type);
}

ig_u32 VMImp::_createModuleArrayInstance(Module * module, int count, int category) 
{
	int gc_type = GC_LEAF;
	if      (category == Variable::STORAGE_CLASS_OBJECT)      { gc_type = GC_OBJECT_ARRAY_STRIDE_4; }
	else if (category == Variable::STORAGE_CLASS_FUNCPTR)  { gc_type = GC_OBJECT_ARRAY_STRIDE_8; }

	return m_heap.allocArrayHandle(module->m_instance_size, module->m_instance_defaults, count, s_storage_class_size[category], gc_type);
}

ig_u32 VMImp::_reallocArrayInstance(ig_u32 handle, Object * obj,  int count, int storage_class)
{
	// deref references that are no longer accessibile
	if (storage_class == Variable::STORAGE_CLASS_32_REFCNT)
	{
		ig_i32  old_count   = *(ig_i32 *)(obj->m_data);
		if (old_count > count) 
		{
			ig_i32 * data_ptr = (ig_i32 *) &obj->m_data[1];

			for (int i = count; i < old_count; i++) {
				m_config._swap_ref_32(this, data_ptr[i], 0, false);
				data_ptr[i] = 0;
			}
		}
	}

	return m_heap.reallocArray(handle, s_storage_class_size[storage_class], count);
}

/**
 * Fill an array with a value
 */


void VMImp::_fillArray(ig_u32 obj_handle, Object * obj, int storage_class, Box  value) 
{
	ig_i32  count   = *(ig_i32 *)(obj->m_data);
	ig_u8 * data_ptr = (ig_u8 *) &obj->m_data[1];
	int stride = s_storage_class_size[storage_class];
	
	if (storage_class == Variable::STORAGE_CLASS_32_REFCNT)
	{
		for (int i = 0; i < count; i++) 
		{
			m_config._swap_ref_32(this, *(ig_i32 *)data_ptr, value.i32, true);
		
			memcpy(data_ptr, &value, stride);
			data_ptr += stride;
		}
	}
	else if ((storage_class == Variable::STORAGE_CLASS_OBJECT || 
			  storage_class == Variable::STORAGE_CLASS_32  || 
			  storage_class == Variable::STORAGE_CLASS_16  || 
			  storage_class == Variable::STORAGE_CLASS_8  ) && (value.i32 == 0)) 
	{
		memset(data_ptr, 0, stride * count);
	}
	else if (storage_class == Variable::STORAGE_CLASS_OBJECT || 
			 storage_class == Variable::STORAGE_CLASS_FUNCPTR)
	{
		// add to the grey list if the array is grey or black
		
		m_heap.assigned(obj_handle, value.u32);
	
		// copy over the buffer
		for (int i = 0; i < count; i++) {
			memcpy(data_ptr, &value, stride);
			data_ptr += stride;
		}
	}

	else
	{
		// copy over the buffer
		for (int i = 0; i < count; i++) {
			memcpy(data_ptr, &value, stride);
			data_ptr += stride;
		}
	}
}


void VMImp::_compareArray(int storage_class,  Object * obj, Object * src, int * res)
{
	*res = 0;
	
	ig_i32  count   = *(ig_i32 *)(obj->m_data);
	ig_u8 * data_ptr = (ig_u8 *) &obj->m_data[1];
	
	ig_i32  src_count = *(ig_i32 *)(src->m_data);
	ig_u8 *  src_data_ptr = (ig_u8 *) &src->m_data[1];
	
	if (count != src_count) { return; }
	
	int stride = s_storage_class_size[storage_class];
	
	int result = memcmp(data_ptr, src_data_ptr, count * stride);
	if (result == 0) { *res = 1; }
	return;
}

void VMImp::_copyArray(ig_u32 obj_handle, Object * obj, int storage_class, 
								ig_u32 src_obj_handle, Object * src) 
{

	ig_i32  count   = *(ig_i32 *)(obj->m_data);
	ig_u8 * data_ptr = (ig_u8 *) &obj->m_data[1];
	
	ig_i32  src_count = *(ig_i32 *)(src->m_data);
	ig_u8 *  src_data_ptr = (ig_u8 *) &src->m_data[1];
	
	
	if (count > src_count ) {
		count = src_count;
	}
	
	int stride = s_storage_class_size[storage_class];
	
	if (storage_class == 18)
	{
		for (int i = 0; i < count; i++) 
		{
			ig_u32 value = *(ig_i32 *)src_data_ptr;
			m_config._swap_ref_32(this, *(ig_i32 *)data_ptr, value, true);
		
			memcpy(data_ptr, &value, stride);
			
			data_ptr += stride;
			src_data_ptr += stride;
		}
	}
	else if (storage_class == 16 || storage_class == 17)
	{
		// add to the grey list if the array is grey or black
		bool to_mark = false;
		if ((m_heap.m_handles_meta[obj_handle] & 0x3) != 0) {
			to_mark = true;
		}
	
		// copy over the buffer
		for (int i = 0; i < count; i++) {
		
			ig_i32 value = *(ig_i32 *)src_data_ptr;
			if (value != 0 && to_mark) {
				m_heap.whiteToGrey(value);
			}
		
			memcpy(data_ptr, src_data_ptr, stride);
			
			data_ptr += stride;
			src_data_ptr += stride;
		}
	}
	else
	{
		memcpy(data_ptr, src_data_ptr, stride * count);
	}
}



/**
 * Function to retrieve the list of objects
 * This allows us to keep the heap more in the dark about what the vm implementation is
 */


static ig_i16 * objectDiscovery(void * user_p, void * ptr)
{
	Object * obj    = (Object *) ptr;
	VTable * vtable = obj->m_vtable;
	return vtable->m_module->m_gc_object_offsets;
}

void VMImp::objectDestructor(void * user_p, ig_u32 handle)
{
	VMImp * imp = (VMImp *)user_p;
	
	//fprintf(stderr, "objectDestructor %x  => %p ", handle, m_heap.m_handles[handle]);
	Object * obj = (Object *) imp->m_heap.m_handles[handle];
	VTable * vtable      = obj->m_vtable;
	//fprintf(stderr, "\tvtable-ptr: %p", vtable);
	
	Module * module      = vtable->m_module;
	
	//fprintf(stderr, "\tmodule-ptr: %p", module);
	
	Box box;
	box.u32 = handle;
	
	//fprintf(stderr, "\tsc: %d", module->m_header.m_array_storage_class);
	
	if (module->m_header.m_array_storage_class == 18)
	{
		ig_i32   arr_count = *(ig_i32 *)(obj->m_data);
		ig_i32 * data_ptr   = (ig_i32 *) &obj->m_data[1];

		for (int i = 0; i < arr_count; i++) {
			imp->m_config._swap_ref_32(imp, data_ptr[i], 0, false);
		}
	}
	else
	{
		ig_i16 * ref_fields = module->m_ref_count_fields;
		while (*ref_fields != -1)
		{
			ig_i32 handle = obj->i32(*ref_fields);
			imp->m_config._swap_ref_32(imp, handle, 0, false);
		
			ref_fields ++;
		}
	}
	
	while (NULL != module) {
	
		if (module->m_native_destructor != NULL) {
			module->m_native_destructor(box);
		}
		module = module->m_extends_module;
	}
	
	//fprintf(stderr, "END\n");
}


void VMImp::_heapRequestedGC()
{
	fprintf(stderr, "HANDLE STRESS - forcing gc\n");
	
	
		// drop down a scope frame
	// [last top,  V0, V1, V2, V3] [last top, V0 ... VN] 
	
	
	// process the active scopes
	{
		int st = m_scope_top;
		int si = m_scope_index;
	
		while (st != 0)
		{
			for (int i = st + 1; i < si; i++) {
				m_heap.whiteToGreyIfValid(m_scope[i]);
			}

			si = st;
			st = m_scope[si];
		}	
	}
	
	
	// mark everything on the stack as grey
	Stack * curr = m_stack_active_head;
	while (curr != NULL) 
	{
	
		Box * s0 = curr->m_stack;
		Box * s1 = curr->m_current_sp;		// inclusive
	
		while (  ((size_t)s0)  <=  ((size_t)s1)     ) 
		{
			//printf("\tattempting to mark: %x\n", s0->u32);
			m_heap.whiteToGreyIfValid(s0->u32);
			
			s0++;
		}
		curr = curr->m_next;
	}		
	
	
	//_forceGC(true);
	
	bool stop_the_world = true;
	m_heap.gc(stop_the_world, this, objectDiscovery, objectDestructor);
}

void VMImp::_forceGC(bool stop_the_world)
{
	// scope stack is always blown away on a manual GC
	m_scope_index = 1;
	m_scope_top   = 0;
	m_scope[0]    = 0;
	
	// we also don't consider anything on any stack to be alive so
	// NO WORK HERE
	
	// tell the heap to perform its gc
	m_heap.gc(stop_the_world, this, objectDiscovery, objectDestructor);
		

#if IGVM_GC_PROFILING == 1		
	ig_u32 max = s_tracker_allocs.m_count;
	if (s_tracker_old_frees.m_count > max) {
		max = s_tracker_old_frees.m_count;
	}
	if (s_tracker_young_frees.m_count > max) {
		max = s_tracker_old_frees.m_count;
	}
	
	fprintf(stderr, "GC Profiling... allocated: %d\n", m_heap.s_young_allocator.m_memory_allocated);
	for (ig_u32 i = 0; i < max; i++) {
		int a = s_tracker_allocs[i];
		int o = s_tracker_old_frees[i];
		int y = s_tracker_young_frees[i];
		
		if (a != 0 || o != 0 || y != 0) {
			this->_constantPairDebug(i, 30);
			fprintf(stderr, " allocs: %d old-frees: %d young-frees: %d\n", a, o, y);
		}
	}

	s_tracker_allocs.clear();
	s_tracker_old_frees.clear();
	s_tracker_young_frees.clear();
#endif

}






int VMImp::_reinit(bool setup)
{
	//bool setup = false;

	//fprintf(stderr, "VM::reinit: %d\n", s_reinit_count);
	s_reinit_count ++;
	//return;

	//fprintf(stderr, "MARK2\n");

	if (s_reinit_occupied == true) {
		fprintf(stderr, "VM::reinit() Called while a reinit is already being called");
	}
	s_reinit_occupied = true;


	
	//fprintf(stderr, "MARK 3\n");
	// Clear out the handles
	m_heap.reinit(this, objectDestructor);

	//fprintf(stderr, "MARK 3a\n");
	// clear out all of the static data, and also unref any static constants
	this->_constantPairReinit();
	
	//fprintf(stderr, "MARK 3b\n");
	this->_constantStringReinit();

	m_stack_active_head = NULL;
	m_stack_active_tail = NULL;
	
	m_scope_top = 0;
	m_scope_index = 1;
	m_scope[0] = 0;

	// initialize the static data pool
	m_static_data.clear();
	
	//fprintf(stderr, "MARK 4\n");	
	// Clear out the string pool
	// and hash
	{
		memset(m_string_hash, 0, sizeof(StringEntry *) * m_string_hash_size);
		m_strings.clear();
		m_string_offsets.clear();
	}
	//fprintf(stderr, "MARK 5\n");
	// Clear out the pair pool
	// and hash
	{
		// THESE CONTAIN ACTUAL POINTERS
		memset(m_pair_hash, 0, sizeof(PairEntry *) * m_pair_hash_size);
		m_pairs.clear();
		m_pair_components.clear();
	}
	//fprintf(stderr, "MARK 6\n");
	// DOESN'T NEED TO BE CLEARED
	//m_stack_frame_pool.init(32);
	//m_stack_pool.init(4);

	// initialize the load stack
	VMImp::m_load_stack.clear();
	VMImp::m_link_stack.clear();
	// initialize the stack for init
	VMImp::m_init_stack.clear();


	if (setup)
	{
		this->m_error_root_idx = m_heap.allocAndClearStaticRoot();
	
		// set up entry 0 to be a null string
		this->_constantString("", 0, true);

		int cs_bool   = this->_constantString("bool",   strlen("bool"),   true);
		int cs_int    = this->_constantString("int",    strlen("int"),    true);
		int cs_double = this->_constantString("double", strlen("double"), true);

		// set up pair "","" to be null
		this->_constantPair  (0,  0, true);
		m_cp_bool   = this->_constantPair(cs_bool, 0, true);
		m_cp_int    = this->_constantPair(cs_int, 0, true);
		m_cp_double = this->_constantPair(cs_double, 0, true);

		// insert the array of shorts so that we can create string constants further on
		VMImp::m_load_stack.push(this->_constantString("iglang.Array<short>", strlen("iglang.Array<short>"), true));
	}

	s_reinit_occupied = false;
	
	
	return 0;
}


static void request_gc(VMInstance * ins) {	
	((VMImp *)ins)->_heapRequestedGC();
}


int VMImp::_init(const Config * config) {
	//printf("VM::init\n");
	m_config = *config;
	
	memset(&m_allocator, 0, sizeof(VMAllocator));
	m_allocator.m_config = &m_config;
	m_allocator.m_instance= this;
	m_allocator.m_gc_fn = request_gc;

	memset(&m_allocator_heap, 0, sizeof(VMAllocator));
	m_allocator_heap.m_config = &m_config;
	m_allocator_heap.m_instance= this;
	m_allocator_heap.m_gc_fn = request_gc;

	// create the scope stack
	m_scope_top = 0;
	m_scope_index = 1;
	m_scope_capacity = 32;
	m_scope = (ig_u32 *) IGVM_MALLOC(sizeof(ig_u32) * m_scope_capacity);
	m_scope[0] = 0;
	

	m_heap.init(&m_allocator_heap, 0,0,0);
	m_error_root_idx = m_heap.allocAndClearStaticRoot();

	// initialize the static data pool
	// this is where all static variables are stored
	// by default this is set to 64k
	m_static_data.init    (&m_allocator, 64 * 1024);
	
	
	// initialize the string pool to 128k
	memset(m_string_hash, 0, sizeof(StringEntry *) * m_string_hash_size);
	m_strings.init(&m_allocator, 128*1024);
	m_string_offsets.init(&m_allocator, 8192, (ig_u32)0xffffffff);


	memset(m_pair_hash, 0, sizeof(PairEntry *) * m_pair_hash_size);
	m_pairs.init(&m_allocator, 1024);
	m_pair_components.init(&m_allocator, 1024);

	m_stack_pool.init(&m_allocator);           //   4 entries per page
	m_stack_active_head = NULL;
	m_stack_active_tail = NULL;
	
	// initialize the load stack
	VMImp::m_load_stack.init(&m_allocator, 256);
	VMImp::m_link_stack.init(&m_allocator, 256);
	VMImp::m_init_stack.init(&m_allocator, 256);


	// set up entry 0 to be a null string
	this->_constantString("", 0, true);

	int cs_bool   = this->_constantString("bool",   strlen("bool"),   true);
	int cs_int    = this->_constantString("int",    strlen("int"),    true);
	int cs_double = this->_constantString("double", strlen("double"), true);

	// set up pair "","" to be null
	this->_constantPair  (0,  0, true);
	m_cp_bool   = this->_constantPair(cs_bool, 0, true);
	m_cp_int    = this->_constantPair(cs_int, 0, true);
	m_cp_double = this->_constantPair(cs_double, 0, true);

	// queue the import of iglang.Array<iglang.Short> for use by iglang.String
	VMImp::m_load_stack.push(this->_constantString("iglang.Array<short>", strlen("iglang.Array<short>"), true));
	
	
	return 0;
}


static int iglang_Object_hashCode(VMInstance * instance, const igvm::Box * params, int param_count, igvm::Box * ret) {
	ret->i32 = params[0].i32;
	return 0;
}


static int iglang_Object_toString(VMInstance * instance, const igvm::Box * params, int param_count, igvm::Box * ret) {
	igvm::Box b = params[0];
	
	char name[1024];
	char buffer[2048];
	
	
	if (0 != instance->getClassName(params[0], name, 1024)) {
		return 1;
	}
	
	snprintf(buffer, 2048, "%s@%x", name, b.u32);
	buffer[2048-1] = 0;
	
	if (0 != instance->stringFromUTF8Bytes(buffer, ret)) {
		return 1;
	}

	return 0;
}
static int iglang_Object_trace(VMInstance * instance, const igvm::Box * params, int param_count, igvm::Box * ret) {
	char buffer[4096];

	int ec;
	if (0 != (ec = instance->UTF8BytesFromString(params[0], buffer, 4096))) {
		return 1;
	}

	fprintf(stderr, "igvm: %s\n", buffer);

	return 0;
}

static int iglang_Error___ig_debugObject(VMInstance * instance, const igvm::Box * params, int param_count, igvm::Box * ret)  {
	//igvm::Box b = params[0];
	// TODO reimplement
	//printf("igvm: debugObject handle: 0x%x ptr: %p\n", b.u32, m_heap.m_handles[b.u32]);
	return 0;
}

static int iglang_Error___ig_debugConfig(VMInstance * instance, const igvm::Box * params, int param_count, igvm::Box * ret)  {
	igvm::Box b = params[0];
	ret->i32 = instance->debugConfig(b.i32);
	//printf("igvm: debugObject handle: 0x%x ptr: %p\n", b.u32, m_heap.m_handles[b.u32]);
	return 0;
}


static int iglang_Math_pow(VMInstance * instance, const igvm::Box * params, int param_count, igvm::Box * ret) {
	ret->f64 = pow(params[0].f64, params[1].f64);
	return 0;
}

static int iglang_Math_random(VMInstance * instance, const igvm::Box * params, int param_count, igvm::Box * ret) {
	ret->f64 = ((double) rand() / ((double)RAND_MAX+1.0));  // must be [0,1)
	return 0;
}



static int iglang_Math_sqrt(VMInstance * instance, const igvm::Box * params, int param_count, igvm::Box * ret) {
	ret->f64 = sqrt(params[0].f64);
	return 0;
}

static int iglang_Math_sin(VMInstance * instance, const igvm::Box * params, int param_count, igvm::Box * ret) {
	ret->f64 = sin(params[0].f64);
	return 0;
}


static int iglang_Math_abs(VMInstance * instance, const igvm::Box * params, int param_count, igvm::Box * ret) {
	ret->f64 = fabs(params[0].f64);
	return 0;
}

static int iglang_Math_cos(VMInstance * instance, const igvm::Box * params, int param_count, igvm::Box * ret) {
	ret->f64 = cos(params[0].f64);
	return 0;
}

static int iglang_Math_tan(VMInstance * instance, const igvm::Box * params, int param_count, igvm::Box * ret) {
	ret->f64 = tan(params[0].f64);
	return 0;
}

static int iglang_Math_atan2(VMInstance * instance, const igvm::Box * params, int param_count, igvm::Box * ret) {
	ret->f64 = atan2(params[0].f64, params[1].f64);
	return 0;
}

static int iglang_Math_atan(VMInstance * instance, const igvm::Box * params, int param_count, igvm::Box * ret) {
	ret->f64 = atan(params[0].f64);
	return 0;
}


static int iglang_Math_asin(VMInstance * instance, const igvm::Box * params, int param_count, igvm::Box * ret) {
	ret->f64 = asin(params[0].f64);
	return 0;
}

static int iglang_Math_acos(VMInstance * instance, const igvm::Box * params, int param_count, igvm::Box * ret) {
	ret->f64 = acos(params[0].f64);
	return 0;
}

static int iglang_Math_floor(VMInstance * instance, const igvm::Box * params, int param_count, igvm::Box * ret) {
	ret->f64 = floor(params[0].f64);
	return 0;
}

static int iglang_Math_ceil(VMInstance * instance, const igvm::Box * params, int param_count, igvm::Box * ret) {
	ret->f64 = ceil(params[0].f64);
	return 0;
}

static int iglang_Math_round(VMInstance * instance, const igvm::Box * params, int param_count, igvm::Box * ret) {
	ret->f64 = round(params[0].f64);
	return 0;
}


static int iglang_Math_min(VMInstance * instance, const igvm::Box * params, int param_count, igvm::Box * ret)
{
	double a = params[0].f64;
	double b = params[1].f64;
	ret->f64 = a < b ? a : b;
	return 0;
}

static int iglang_Math_max(VMInstance * instance, const igvm::Box * params, int param_count, igvm::Box * ret) {
	double a = params[0].f64;
	double b = params[1].f64;
	ret->f64 = a > b ? a : b;
	return 0;
}

static int iglang_Double_doubleToIntBits(VMInstance * instance, const igvm::Box * params, int param_count, igvm::Box * ret) {
	float value = params[0].f64;
	ret->i32 = *((igvm::ig_i32 *)&value);
	return 0;
}

static int iglang_Double_doubleToIntBitsHi(VMInstance * instance, const igvm::Box * params, int param_count, igvm::Box * ret) {
	double value = params[0].f64;
	ret->i32 = (((igvm::ig_i32 *)&value)[1]);
	return 0;
}

static int iglang_Double_doubleToIntBitsLo(VMInstance * instance, const igvm::Box * params, int param_count, igvm::Box * ret) {
	double value = params[0].f64;
	ret->i32 = (((igvm::ig_i32 *)&value)[0]);
	return 0;
}


static int iglang_DateTime_nativeGetTime(VMInstance * instance, const igvm::Box * params, int param_count, igvm::Box * ret) {
	ret->f64  = time(0);
	return 0;
}
	
static int iglang_DateTime_nativeExtractTime(VMInstance * instance, const igvm::Box * params, int param_count, igvm::Box * ret) {

	
	//fprintf(stderr, "[iglang_DateTime_nativeExtractTime] START\n");
	char tz[32];
	if (params[1].i32 == 0 || 0 != instance->UTF8BytesFromString(params[1], tz, 32)) {
		return 1;
	}
	
	igvm::Array fields;
	if (params[2].i32 == 0 || 0 != instance->getArray(params[2], &fields)) {
		return 1;
	}
	
	bool is_utc = false;
	time_t tt = params[0].f64;
	struct tm * t;
	if (strcmp(tz, "UTC") == 0) {
		t = gmtime(&tt);
		is_utc = true;
	}
	else if (strcmp(tz, "LOCAL") == 0) {
		t = localtime(&tt);
	}
	else {
		return 1;
	}

	((ig_i32 *)fields.data)[0] = t->tm_sec;
	((ig_i32 *)fields.data)[1] = t->tm_min;
	((ig_i32 *)fields.data)[2] = t->tm_hour;
	((ig_i32 *)fields.data)[3] = t->tm_mday;
	((ig_i32 *)fields.data)[4] = t->tm_mon + 1;
	((ig_i32 *)fields.data)[5] = t->tm_year + 1900;
	((ig_i32 *)fields.data)[6] = t->tm_wday;
	((ig_i32 *)fields.data)[7] = t->tm_yday;
	
    //printf("%d\n", t->tm_isdst);			/// <- should we use these
    //printf("%d\n", t->tm_gmtoff);			/// <- should we use these
    
    
    char zone[64];
    char zone_offset[64];
    
    strftime(zone_offset, 64, "%z", t);
    strftime(zone,        64, "%Z", t);
        
    //printf("%s %s\n", zone, zone_offset);   
        
  	int zo = 0;
  	if (!is_utc)
  	{
  		int multipliers [] = { 1, 10, 60, 600, 0 } ;
  		size_t len = strlen(zone_offset);
  		for (int i = 0; i < len; i++) {
  			int c = zone_offset[len - 1 - i];
  			if (i < 4) {
  				zo += (c - '0') * multipliers[i];
  			}
  			else {
  				if (c == '-') {
  					zo = 0 - zo;
  				}
  				break;
  			}
  		}	
  	}      
        
        
    ((ig_i32 *)fields.data)[8] = zo;    
        
    ret->u32 = 0;		// null;
    
    /*
    if (0 != instance->stringFromUTF8Bytes(zone, ret)) {
    	return 1;
    }
    */

	//fprintf(stderr, "[iglang_DateTime_nativeExtractTime] END\n");

	return 0;
}


static int read0Padded(char ** ptr, int digits, int * ec) 
{
	*ec = 0;
	int value = 0;
	while (digits > 0) {
		if (**ptr == '\0') {
			*ec = 1;
			return 0;
		}
		value = (value * 10) + (**ptr - '0');
		ptr[0] ++;
		digits --;
	}
	
	//printf("read0Padded: %d\n", value);
	return value;
}


/**
 * This implements a subset of strptime.  
 */
 
time_t my_timegm (struct tm *tm) 
{
#ifdef __MINGW32__
    return _mkgmtime(tm);
#else
	return timegm(tm);
	/*
    time_t ret;
    char *tz;

    tz = getenv("TZ");
    setenv("TZ", "", 1);
    tzset();
    ret = mktime(tm);
    if (tz)
    	setenv("TZ", tz, 1);
    else
   	 	unsetenv("TZ");
    tzset();
    return ret;
    */
#endif
}

static int iglang_DateTime_nativeParseTime(VMInstance * instance, const igvm::Box * params, int param_count, igvm::Box * ret)
{
	char format_string[1024];
	char time_string[1024];
	
	if (0 != instance->UTF8BytesFromString(params[0], format_string, 1024)) {
		return 1;
	}
	
	if (0 != instance->UTF8BytesFromString(params[1], time_string, 1024)) {
		return 1;
	}



	struct tm time_split;
	
	// First clear the result structure.  
	memset (&time_split, '\0', sizeof (struct tm));


	//int format_len = strlen(format_string);
	//int time_string = strlen(time_string);
	
	
	char * format_ptr = format_string;
	char * time_ptr   = time_string;
	
	int timezone_offset_sec = 0;
	
	int ec = 0;
	while (*format_ptr != '\0' && ec == 0)
	{
		char c = *format_ptr;
		format_ptr ++;
		
		if (c == '%') {
			c = *format_ptr;
			format_ptr++;
			switch (c) {
				case 'Y': {
					time_split.tm_year = read0Padded(&time_ptr, 4, &ec) - 1900;
					break;
				}
				case 'm': {
					time_split.tm_mon = read0Padded(&time_ptr, 2, &ec) - 1;
					break;
				}		
				case 'd': {
					time_split.tm_mday = read0Padded(&time_ptr, 2, &ec);				
					break;
				}	
				case 'H': {
					time_split.tm_hour = read0Padded(&time_ptr, 2, &ec);								
					break;
				}	
				case 'M': {
					time_split.tm_min = read0Padded(&time_ptr, 2, &ec);												
					break;
				}
				case 'S': {
					time_split.tm_sec = read0Padded(&time_ptr, 2, &ec);								
					break;
				}
				case '%': {
					if (c != *time_ptr) {
						return 1;
					}
					time_ptr++;
					break;
				}
				case 'z': {
					if (*time_ptr == 'Z') {
						time_ptr++;
					}
					else {
						char direction = *time_ptr; time_ptr++;
						if (direction != '-' || direction != '+') {
							return 1;
						}
						
						int time_pre = read0Padded(&time_ptr, 4, &ec);
						timezone_offset_sec = ((time_pre / 100) * 60 + (time_pre % 100)) * 60;
						if (direction == '+') { 
							timezone_offset_sec = 0 - timezone_offset_sec;
						}
					}
					break;
				}
				default: {
					return 1;
				}
			}
			
		}
		else 
		{
			// invalid format
			if (c != *time_ptr) {
				return 1;
			}
			time_ptr++;
		}	
	}
	
	//printf("fmt: %s tm: %s\n", format_string, time_string);
	
	// %Y-%m-%dT%H:%M:%SZ
	

	
	
	//const char *cp;

	// Try the ISO format first.  
	//cp = strptime (time_string,format_string, &time_split);
	//if (cp == NULL)
	//{
	//	//t//hrow new Error("Invalid Time Input");
	//	return 1;
	//}

	ret->f64 = my_timegm(&time_split) + timezone_offset_sec;
	return 0;
}

static int iglang_DateTime_nativeCreateTime(VMInstance * instance, const igvm::Box * params, int param_count, igvm::Box * ret)
{
	int timezone_offset_sec = 0;
	
	struct tm time_split;
	
	// First clear the result structure.  
	memset (&time_split, 0, sizeof (struct tm));
	time_split.tm_year = params[0].i32 - 1900;
	time_split.tm_mon  = params[1].i32 - 1;	
	time_split.tm_mday = params[2].i32;	
	time_split.tm_hour = params[3].i32;	
	time_split.tm_min  = params[4].i32;	
	time_split.tm_sec  = params[5].i32;	
			
	ret->f64 = my_timegm(&time_split) + timezone_offset_sec;
	return 0;
}


static int iglang_DateTime_nativeFormatTime(VMInstance * instance, const igvm::Box * params, int param_count, igvm::Box * ret) 
{
	//fprintf(stderr, "[iglang_DateTime_nativeFormatTime] START\n");
	char tz[32];
	if (params[1].i32 == 0 || 0 != instance->UTF8BytesFromString(params[1], tz, 32)) {
		return 1;
	}
	
	char result_string[1024];
	char format_string[1024];
	if (params[2].i32 == 0 || 0 != instance->UTF8BytesFromString(params[2], format_string, 1024)) {
		return 1;
	}
	
	time_t tt = params[0].f64;
	struct tm * t;
	if (strcmp(tz, "UTC") == 0) {
		t = gmtime(&tt);
	}
	else if (strcmp(tz, "LOCAL") == 0) {
		t = localtime(&tt);
	}
	else {
		return 1;
	}

	strftime(result_string, 1024, format_string, t);
    
    if (0 != instance->stringFromUTF8Bytes(result_string, ret)) {
    	return 1;
    }

	//fprintf(stderr, "[iglang_DateTime_nativeFormatTime] END\n");
	return 0;
}





static int iglang_Double_intPairToDouble(VMInstance * instance, const igvm::Box * params, int param_count, igvm::Box * ret) {
	
	union
	{
		igvm::ig_i32 v[2];
		double       f64;
	} conv;
	
	conv.v[0] = params[0].i32;
	conv.v[1] = params[1].i32;
	
	ret->f64 = conv.f64;
	
	return 0;
}

static int iglang_Double_intBitsToDouble(VMInstance * instance, const igvm::Box * params, int param_count, igvm::Box * ret) {
	igvm::ig_i32 value = params[0].i32;
	ret->f64 = *((float *)&value);
	return 0;
}


static int iglang_StringBuilder_intToByteArray(VMInstance * instance, const igvm::Box * params, int param_count, igvm::Box * ret) {
	int value = params[0].i32;
	igvm::Box array = params[1];
	int max_length = params[2].i32;



	Array native_array;
	if (instance->getArray(array, &native_array) == 0) {
		//int len     = native_array.length;
		char * data = (char *) native_array.data;

		snprintf(data, max_length, "%d", value);
		data[max_length-1] = '\0';
		ret->i32 = (ig_i32) strlen(data);
	}
	else {
		return 1;
	}

	return 0;
}

static int iglang_StringBuilder_doubleToByteArray(VMInstance * instance, const igvm::Box * params, int param_count, igvm::Box * ret) {
	double value = params[0].f64;
	igvm::Box array = params[1];
	int max_length = params[2].i32;

	//printf("iglang_StringBuilder_doubleToByteArray %f %d\n", value, max_length);

	Array native_array;
	if (instance->getArray(array, &native_array) == 0) {
		//int len     = native_array.length;
		char * data = (char *) native_array.data;

		snprintf(data, max_length, "%f", value);
		data[max_length-1] = '\0';
		ret->i32 = (ig_i32) strlen(data);
	}
	else {
		//printf("iglang_StringBuilder_doubleToByteArray get array failed\n");
		return 1;
	}

	return 0;
}



int VMImp::_loadBuiltins()
{
	IGVM_CALL(this, this->_loadModule("iglang.Object") );
	
	IGVM_CALL(this, this->_setNativeFunction("iglang.Object", "toString",  iglang_Object_toString));
	IGVM_CALL(this, this->_setNativeFunction("iglang.Object", "trace",    iglang_Object_trace) );
	IGVM_CALL(this, this->_setNativeFunction("iglang.Object", "hashCode", iglang_Object_hashCode) );
	IGVM_CALL(this, this->_setNativeFunction("iglang.Error", "__ig_debugObject", iglang_Error___ig_debugObject) );
	IGVM_CALL(this, this->_setNativeFunction("iglang.Math", "sqrt", iglang_Math_sqrt) );
	IGVM_CALL(this, this->_setNativeFunction("iglang.Math", "tan", iglang_Math_tan) );
	IGVM_CALL(this, this->_setNativeFunction("iglang.Math", "sin", iglang_Math_sin) );
	IGVM_CALL(this, this->_setNativeFunction("iglang.Math", "cos", iglang_Math_cos) );
	IGVM_CALL(this, this->_setNativeFunction("iglang.Math", "abs", iglang_Math_abs) );
	IGVM_CALL(this, this->_setNativeFunction("iglang.Math", "asin", iglang_Math_asin) );
	IGVM_CALL(this, this->_setNativeFunction("iglang.Math", "acos", iglang_Math_acos) );
	IGVM_CALL(this, this->_setNativeFunction("iglang.Math", "atan2", iglang_Math_atan2) );
	IGVM_CALL(this, this->_setNativeFunction("iglang.Math", "atan", iglang_Math_atan) );
	
	IGVM_CALL(this, this->_setNativeFunction("iglang.Math", "floor", iglang_Math_floor) );
	IGVM_CALL(this, this->_setNativeFunction("iglang.Math", "ceil",  iglang_Math_ceil) );
	IGVM_CALL(this, this->_setNativeFunction("iglang.Math", "round", iglang_Math_round) );
	IGVM_CALL(this, this->_setNativeFunction("iglang.Math", "min",   iglang_Math_min) );
	IGVM_CALL(this, this->_setNativeFunction("iglang.Math", "max",   iglang_Math_max) );

	IGVM_CALL(this, this->_setNativeFunction("iglang.Math", "pow", iglang_Math_pow) );
	IGVM_CALL(this, this->_setNativeFunction("iglang.Math", "random", iglang_Math_random) );

	IGVM_CALL(this, this->_setNativeFunction("iglang.Double", "doubleToIntBits", iglang_Double_doubleToIntBits) );
	IGVM_CALL(this, this->_setNativeFunction("iglang.Double", "doubleToIntBitsLo", iglang_Double_doubleToIntBitsLo) );
	IGVM_CALL(this, this->_setNativeFunction("iglang.Double", "doubleToIntBitsHi", iglang_Double_doubleToIntBitsHi) );
	IGVM_CALL(this, this->_setNativeFunction("iglang.Double", "intBitsToDouble", iglang_Double_intBitsToDouble) );
	IGVM_CALL(this, this->_setNativeFunction("iglang.Double", "intPairToDouble", iglang_Double_intPairToDouble) );

	IGVM_CALL(this, this->_setNativeFunction("iglang.StringBuilder", "doubleToByteArray", iglang_StringBuilder_doubleToByteArray) );
	IGVM_CALL(this, this->_setNativeFunction("iglang.StringBuilder", "intToByteArray", iglang_StringBuilder_intToByteArray) );
	
	IGVM_CALL(this, this->_setNativeFunction("iglang.Error", "__ig_debugConfig", iglang_Error___ig_debugConfig));


	IGVM_CALL(this, this->_setNativeFunction("iglang.DateTime", "nativeGetTime",     iglang_DateTime_nativeGetTime) );
	IGVM_CALL(this, this->_setNativeFunction("iglang.DateTime", "nativeExtractTime", iglang_DateTime_nativeExtractTime) );
	IGVM_CALL(this, this->_setNativeFunction("iglang.DateTime", "nativeFormatTime",  iglang_DateTime_nativeFormatTime) );
	IGVM_CALL(this, this->_setNativeFunction("iglang.DateTime", "nativeParseTime",   iglang_DateTime_nativeParseTime));
	IGVM_CALL(this, this->_setNativeFunction("iglang.DateTime", "nativeCreateTime",  iglang_DateTime_nativeCreateTime) );
	
	return 0;
}






int VMImp::errorOccurred(int code)
{
	if ((code & 0xffffff00) == 0) {
		m_error_message[0] = '\0';
	}
	ErrorText * et = error_text;
	while (et->sig != 0)
	{
		if (et->sig== (code & 0xff )) 
		{
			size_t len = strlen(m_error_message);
			snprintf(&m_error_message[len], MAX_ERROR_MESSAGE_LENGTH - 1 - len, "%s\n", et->text);

			m_error_message[ MAX_ERROR_MESSAGE_LENGTH - 1] = '\0';
			break;
		}

		et++;
	}

	return code;
}

void VMImp::_pushObjectToScope(ig_u32 handle)
{
	if (handle == 0) { return; }

	if (m_scope_index == m_scope_capacity) {
		int new_cap = (m_scope_capacity * 3)/2;
		m_scope     = (ig_u32 *) IGVM_RESIZE(m_scope, m_scope_capacity * sizeof(ig_u32), sizeof(ig_u32) * new_cap);
		m_scope_capacity = new_cap;
	}
	
	m_scope[m_scope_index] = handle;
	m_scope_index++;
		
}

void VMImp::_beginScope()
{
	if (m_scope_index == m_scope_capacity) {
		int new_cap = (m_scope_capacity * 3)/2;
		m_scope     = (ig_u32 *) IGVM_RESIZE(m_scope, m_scope_capacity * sizeof(ig_u32), sizeof(ig_u32) * new_cap);
		m_scope_capacity = new_cap;
	}
	
	m_scope[m_scope_index] = m_scope_top;
	m_scope_top = m_scope_index;
	m_scope_index++;
}

void VMImp::_endScope()
{
	// drop down a scope frame
	// [last top,  V0, V1, V2, V3] [last top, V0 ... VN] 
	
	// by default there is always a null scope top defined
	// scope_index always points to the next available slot
	
	// [ 0 ] [0, V0, V1, V2]  ?
	//        ^st             ^si
	         
		
	m_scope_index = m_scope_top;
	m_scope_top   = m_scope[m_scope_index];
}

int   VMImp::_setNativeFunction(const char * klass, const char * fn_name, int (*native_fn)(VMInstance *, const Box * params, int param_count, Box * ret))
{
	if (0 != this->_loadModule(klass)) {
		return errorOccurred(0x61);
	}

	// at this point we're guaranteed that the module exists,
	// but not necessarily that there a function with that name in the module
	ig_u32 k = this->_constantString(klass,    strlen(klass), false);
	ig_u32 f = this->_constantString(fn_name,  strlen(fn_name),  false);

	// if either of these cases exist, there's no way a function exists to set its native counterparts
	if (0 == k || 0 == f) {
		return errorOccurred(0x62);
	}

	// resolve the pair for the function
	ig_u32 p = this->_constantPair(k,f, false);

	Function * item = (Function *) m_pairs[p];
	// check that something exists
	if (NULL == item) {
		return errorOccurred(0x63);
	}
	// and that its actually a function
	else if ((item->m_type != Item::MEMBER_FUNCTION &&
			  item->m_type != Item::STATIC_FUNCTION)) {
		return errorOccurred(0x64);
	}

	// assign the native function
	item->m_native = native_fn;
	
	// this was done this way so that each CALL didn't have to check if there was native code attached to it
#if     NATIVE_PATH_2	 == 1	
	item->m_code   = s_invoke_native_code;	// overrite the existing code block
#endif
	return 0;
}

int   VMImp::setNativeDestructor(const char * klass, void (*native_fn)( Box params))
{
	if (0 != this->_loadModule(klass)) {
		return errorOccurred(0x6A);
	}

	ig_u32 k = this->_constantString(klass,    strlen(klass), false);
	if (0 == k) {
		return errorOccurred(0x6B);
	}
	
	ig_u32 p = this->_constantPair(k,0, false);
	
	Module * item = (Module *) m_pairs[p];
	if (NULL == item) {
		return errorOccurred(0x6C);
	}
	
	item->m_native_destructor = native_fn;
	
	return 0;
}


int VMImp::_callNamed(const char * fn_name, const Box * parameters, int argc, Box * ret)
{
	ig_u32 f = this->_constantString(fn_name,  strlen(fn_name),  false);

	if (f == 0) {
		return errorOccurred(0x31);
	}

	const Box * sp              = parameters;
	Object * callee       = m_heap.m_handles[sp->u32];
	if (NULL == callee) {
		return errorOccurred(0x32);
	}

	VTable * callee_vtable = callee->m_vtable;

	int iface_index = 0;
	while (NULL != callee_vtable->m_functions[iface_index]) {
		if (callee_vtable->m_functions[iface_index]->m_name == f) {
			break;
		}
		iface_index ++;
	}

	if (NULL == callee_vtable->m_functions[iface_index]) {
		return errorOccurred(0x33);
	}
	else
	{
		Function * fn = callee_vtable->m_functions[iface_index];
		int ec = VMImp::invoke(NULL, fn, argc, parameters,  ret);
		if (ec == 0) {
			return ec;
		}

		return errorOccurred(0x34 | (ec << 8));
	}
}


int  VMImp::_moduleToPair(const char * klass, ig_u32 * pair)
{
	*pair = 0;

	const ig_u32 k = this->_constantString(klass, strlen(klass), false);
	if (k == 0) {
		return 1;
	}

	const ig_u32 p = this->_constantPair(k,0, false);
	if (p == 0) {
		return 1;
	}
	
	*pair = p;
	
	return 0;
}

int VMImp::_getClassName(const Box & object, char * out_text, int out_capacity) 
{
	if (out_capacity <= 0) {
		return 1;
	}
	
	if (0 == object.u32) {
		snprintf(out_text, out_capacity, "null");
		out_text[out_capacity - 1] = '\0';
		return 0;
	}
	
	// extract the raw object pointer
	const Object * ptr = m_heap.m_handles[object.u32];
	if (NULL == ptr) { return 1; }
	
	// determine which module it is an instance of
	const Module * module   = ptr->m_vtable->m_module;
	if (NULL == module) { return 1; }
	
	// evaluate the name of the module
	ig_u32 pair = module->m_pair;
	
	ig_u32 pairs[2];
	this->_explodeConstantPair(pair, pairs);
	
	const char * data = _getStringForId(pairs[0]);
	strncpy(out_text, data, out_capacity);
	out_text[out_capacity-1] = '\0';
	
	return 0;
}

int   VMImp::objectInstanceOf(const char * klass, const Box & object, bool * result)
{
	// verify that the class has been loaded that we're checking against
	// if it isn't loaded then there really is no way that there could be
	// an object that is an instance of this type

	*result = false;

	ig_u32 p;
	if (0 != this->_moduleToPair(klass, &p)) {
		return 1;
	}

	const Object * ptr = m_heap.m_handles[object.u32];
	// ALLOW null pointers to not cause exceptions
	if (ptr != NULL)
	{
		//ig_u32 member_pair = ptr->m_vtable->m_member_pair;
		Module * member   = ptr->m_vtable->m_module;	//(Module *) m_pairs[member_pair];
		// how could this happen?

		if (NULL != member && member->isInstanceOf(p)) {
			*result = true;
		}
	}
	else {
		return 1;
	}

	return 0;
}

int VMImp::typeExtends(const char * child_klass, const char * parent_klass, bool * result)
{
	*result = false;		
	
	ig_u32 c, p;
	if (0 != this->_moduleToPair(child_klass, &c)) {
		return 1;
	}

	if (0 != this->_moduleToPair(parent_klass, &p)) {
		return 1;
	}
	
	Module * member = (Module *) m_pairs[c];
	// how could this happen?

	if (NULL != member && member->isInstanceOf(p)) {
		*result = true;
	}
	
	return 0;
}

int VMImp::_validatePair(const char * klass, const char * fn_name, int t0, int t1, Item ** item_out)
{
	*item_out = NULL;
	
	int ec;

	// load the klass if it isn't already done
	if (0 != (ec = this->_loadModule(klass))) {
		return errorOccurred(0x11 | (ec << 8));
	}

	ig_u32 k = this->_constantString(klass,    strlen(klass), false);
	ig_u32 f = this->_constantString(fn_name,  strlen(fn_name),  false);

	if (k == 0 || (f == 0 && t0 != Item::MODULE)) {
		return errorOccurred(0x12);
	}

	// resolve the pair for the function
	ig_u32 p = this->_constantPair(k,f, false);

	// validate that the function exists, and that it is a Static Function
	Item * item = m_pairs[p];
	if (NULL == item || (item->m_type != t0 && item->m_type != t1)) {
		return errorOccurred(0x13);
	}

	*item_out = item;
	return 0;
}

int    VMImp::_newObject(const char * klass,  const Box * parameters, int argc, Box * ret)
{
	Module   * module = NULL;
	Function * fn     = NULL;

	int ec;

	// validate that the module exists
	if (0 != (ec = this->_validatePair(klass, "", Item::MODULE, 0,  (Item **) &module)) ||
		NULL == module) {
		return errorOccurred(0x21 | (ec << 8));
	}

	// validate that a member function named <constructor> exists
	if (0 != (ec = this->_validatePair(klass, "<constructor>", Item::MEMBER_FUNCTION, 0,  (Item **) &fn)) ||
		NULL == fn) {
		return errorOccurred(0x22 | (ec << 8));
	}

	// validate that the correct number of arguments has been provided
	if (fn->m_header.m_arg_min > argc + 1) {
		return errorOccurred(0x23);
	}
	// also check that there are no more than 32 parameters
	else if (fn->m_header.m_arg_max < argc + 1 || (argc + 1) > 33) {
		return errorOccurred(0x24);
	}

	// create the instance of the object
	ig_u32 object_handle = this->_createModuleInstance(module);		// VMImp::createHandle(ptr, (module->m_gc_object_handle_count == 0) ? GC_LEAF : GC_OBJECT);

	// store the return value
	ret->u32 = object_handle;
	
	this->_pushObjectToScope(object_handle);
	
	// todo.. just do the scope trickey
	//m_heap.whiteToGrey(object_handle);

	// create the set of arguments to invoke the constructor with
	Box tmp[33];
	tmp[0] = *ret;
	memcpy(&tmp[1], parameters, sizeof(Box) * argc);

	// invoke the constructor
	Box garbage;
	ec = this->invoke(NULL, fn, argc + 1, tmp,  &garbage);
	if (ec == 0) {
		return 0;
	}

	return errorOccurred(0x25 | (ec << 8));
}


int    VMImp::_call(const char * klass, const char * fn_name, const Box * parameters, int count, Box * ret)
{
	Function * fn = NULL;
	
	// see if we can find an internal record of the function to be called
	int ec;
	if (0 != (ec = this->_validatePair(klass, fn_name, Item::MEMBER_FUNCTION, Item::STATIC_FUNCTION, (Item **) &fn)) ||
		NULL == fn) {
		return errorOccurred(0x41 | (ec << 8));
	}

	// validate that the correct number of arguments has been provided
	if (fn->m_header.m_arg_min > count) {
		return errorOccurred(0x42 | (ec << 8));
	}

	if (fn->m_header.m_arg_max < count) {
		return errorOccurred(0x43 | (ec << 8));
	}

	// invoke the function with the passed parameters
	ec = this->invoke(NULL, fn, count, parameters,  ret);
	if (ec != 0) {
		return errorOccurred(0x45 | (ec << 8));
	}

	return 0;
}

int   VMImp::_callFunctionPointer(const Box & function_pointer, const Box * parameters, int count, Box * ret)
{
	// TODO populate new error codes
	Box params2[16];


	
	// establish that this is not a null function?
	// TODO also do stricter tests on the pairs passed in
	if (function_pointer.fp.function == 0 || count > 15) {
		return errorOccurred(0x41 );
	}

	Function * fn = (Function *)m_pairs[function_pointer.fp.function];
	if (fn == NULL)
	{
		return errorOccurred(0x46 );
	}
	else if (fn->m_type == Item::STATIC_FUNCTION) {

	
		for (int shuffle = 0; shuffle < count; shuffle ++) {
			params2[shuffle] = parameters[shuffle];
		}
	}
	else if (fn->m_type == Item::MEMBER_FUNCTION)
	{
		// stop them from calling against a null object
		if (function_pointer.fp.object == 0) {
			return errorOccurred(0x41 );
		}
		
		// find the real function to call
		Object * callee        = m_heap.m_handles[function_pointer.fp.object];
		if (callee == NULL) {
			return errorOccurred(0x47 );
		}
		fn = callee->m_vtable->m_functions[fn->m_offset];
		
	
		params2[0] = function_pointer;
		params2[0].fp.function = 0;
		
		for (int shuffle = 0; shuffle < count; shuffle ++) {
			params2[shuffle + 1] = parameters[shuffle];
		}

		count ++;
	}
	else {
		return errorOccurred(0x41);
	}

	// validate that the correct number of arguments has been provided
	if (fn->m_header.m_arg_min > count) {
		return errorOccurred(0x42);
	}

	if (fn->m_header.m_arg_max < count) {
		return errorOccurred(0x43);
	}

	int ec = this->invoke(NULL, fn, count, params2,  ret);
	if (ec != 0) {
		return errorOccurred(0x45 | (ec << 8));
	}

	return 0;
}

/*
int  VM::setMemberVariable
		(const char * klass, const char * field, bool drill_down,
		 const Box & data)
{

}
*/

int  VMImp::describeMemberVariable
		(const char * klass, const char * field, bool drill_down,
		 char * out_type, int out_type_length,
		 char * out_exact_type, int out_exact_type_length, 
		  int * out_offset, int * out_size)
{
	// now we don't know if this is in this class

	int ec;

	if (klass == NULL || field == NULL ||
		klass[0] == '\0' || field[0] == '\0') {
		return errorOccurred(0x95);
	}


	ig_u32 fc = this->_constantString(field, strlen(field), false);
	if (fc == 0) {
		return errorOccurred(0x91);
	}


	Variable * v = NULL;
	if (!drill_down)
	{
		if (0 != (ec = this->_validatePair(klass, field, Item::MEMBER_VARIABLE, 0, (Item **) &v))) {
			return errorOccurred(0x92 | (ec << 8));
		}
	}
	else
	{
		// establish that the module in question exists

		Module * module = NULL;
		
		// check if a module exists with the given names
		if (0 != (ec = this->_validatePair(klass, "", Item::MODULE, 0, (Item **) &module))) {
			return errorOccurred(0x93 | (ec << 8));
		}


		// recursively look through parent modules to find the one with the named field
		bool found = false;
		while (module != NULL)
		{
			Variable * member_variables = module->m_member_variables;
			int        member_variable_count = module->m_header.m_member_variable_count;

			for (int i = 0; i < member_variable_count; i++) {
				if (member_variables[i].m_name == fc) {
					v = &member_variables[i];
					found = true;
					break;
				}
			}

			if (found) {
				break;
			}
			module = module->m_extends_module;
		}

		if (!found) {
			return errorOccurred(0x94);
		}
	}

	// do a final sanity check that we actually found it
	if (NULL == v) {
		return errorOccurred(0x94);
	}

	*out_offset = v->m_offset;
	*out_size   = v->getStorageSize();

	// the type doesn't necessarily match that specified upon compilation
	// eg.  a Map<String,String> will likely be output as a iglang.Map<iglang.Object,iglang.Object>
	// This is done in order to decrease code size.
	if (out_type != NULL && out_type_length > 0)
	{
		ig_u32 pairs[2];
		this->_explodeConstantPair(v->m_type_pair, pairs);
	
		const char * data = _getStringForId(pairs[0]);	//(char *) m_strings.getPointer(pairs[0]);
		strncpy(out_type, data, out_type_length-1);
		out_type[out_type_length - 1] = '\0';
	}
	
	// the exact type will correspond to exactly what was compiled.
	// eg. a Map<String,String> will l output as a iglang.Map<iglang.String,iglang.String> 
	if (out_exact_type != NULL && out_exact_type_length > 0)
	{
		const char * data = _getStringForId(v->m_exact_type_name);
		strncpy(out_exact_type, data, out_exact_type_length-1);
		out_exact_type[out_exact_type_length - 1] = '\0';
	}
	return 0;
}

int  VMImp::_getMemberVariableOffset(const char * klass, const char * field, int * offset)
{
	Variable * fn = NULL;
	int ec;
	if (0 != (ec = this->_validatePair(klass, field, Item::MEMBER_VARIABLE, 0, (Item **) &fn)) || 
		NULL == fn) {
		return errorOccurred(0x71 | (ec << 8));
	}

	*offset = fn->m_offset;
	return 0;
}

int  VMImp::_getArrayDataOffset(int * offset)
{
	*offset = sizeof(ObjectHeader);	// the first entry is used for the count
	return 0;
}

int  VMImp::_setMemberData          (const Box & object, int offset, int length, const void * data)
{
	ig_u32 handle = object.u32;
	unsigned char * object_data = (unsigned char *) m_heap.m_handles[handle];
	if (NULL == object_data) {
		return errorOccurred(0x81);
	}

	memcpy(&object_data[offset], data, length);
	return 0;
}

int  VMImp::_getMemberData          (const Box & object, int offset, int length, void * data)
{
	ig_u32 handle = object.u32;
	unsigned char * object_data = (unsigned char *) m_heap.m_handles[handle];
	if (NULL == object_data) {
		return errorOccurred(0x82);
	}

	memcpy(data, &object_data[offset], length);
	return 0;
}


int    VMImp::_getField (const char * klass, const char * field, const Box & obj_handle, Box * result)
{
	ig_u32 h = obj_handle.u32;
	Object * ptr = (Object *)m_heap.m_handles[h];

	result->i64 = 0;

	if (NULL == ptr) {
		return errorOccurred(0x51);
	}

	ig_u32 p0 = this->_constantString(klass, strlen(klass), false);
	ig_u32 p1 = this->_constantString(field, strlen(field), false);

	// couldn't look up either of the string
	if (p0 == 0 || p1 == 0) {
		return errorOccurred(0x52);
	}

	ig_u32 pair_id = this->_constantPair(p0, p1, false);
	if (0 == pair_id) {
		return errorOccurred(0x53);
	}

	Variable * item = (Variable *)m_pairs[pair_id];
	if (NULL == item || item->m_type != Item::MEMBER_VARIABLE) {
		return errorOccurred(0x54);
	}

	// this isn't 100% correct .. but it won't cause any harm either
	// unless certain OS's barf on alignment
	////D//printf("\tfield offset: %d\n", item->m_offset);
	result->i64 = ptr->i64(item->m_offset);

	////D//printf("\tresult: %d\n", result.i32);

	return 0;
}

/**
 * Note this does not fail if the object doesn't require the specified field
 */



int    VMImp::_setField (const char * klass, const char * field, const Box & obj_handle, const Box & data)
{
	ig_u32 h = obj_handle.u32;
	Object * ptr = (Object *)m_heap.m_handles[h];


	if (NULL == ptr) {
		return errorOccurred(0x51);
	}

	ig_u32 p0 = this->_constantString(klass, strlen(klass), false);
	ig_u32 p1 = this->_constantString(field, strlen(field), false);

	if (p0 == 0 && p1 == 0) {
		return errorOccurred(0x52);
	}

	ig_u32 pair_id = this->_constantPair(p0, p1, false);
	if (0 == pair_id) {
		return errorOccurred(0x53);
	}

	Variable * item = (Variable *)m_pairs[pair_id];
	if (NULL == item || item->m_type != Item::MEMBER_VARIABLE) {
		return errorOccurred(0x54);
	}


	size_t sz = item->getStorageSize();
	if (sz == 8) {
		 ptr->i64(item->m_offset) = data.i64;
	}
	else {
		ptr->i32(item->m_offset) = data.i32;
	}

	return 0;
}




int    VMImp::_getArray(const Box & handle, Array * array)
{
	ig_u32 h = handle.u32;
	
	

	Object * ptr = (Object *)m_heap.m_handles[h];
	if (ptr == NULL) {
		return 1;
	}
	
	// mark that we've exposed this pointer to the client
	// if its in the scope then it should be be considered PINNED
	this->_pushObjectToScope(handle.u32);

	array->length = *((ig_i32 *)ptr->m_data);
	array->data   =   (void *)(ptr->m_data + 1);

	return 0;
}

/**
 * Debug config is the only one allowed to not output to stderr
 */

int    VMImp::_debugConfig(int v) 
{
	// tracking off
	if (v == 0) {
		m_debug_config = 0;
	}
	// tracking on
	else if (v == 1) {
		m_debug_config = 1;
	}
	// clear out counts and stop tracking
	else if (v == 3)
    {
        for (int p = 0; p < m_pairs.getCount(); p++)
        {
            Item * item = m_pairs[p];
            
            if (NULL != item && (item->m_type == Item::STATIC_FUNCTION || item->m_type == Item::MEMBER_FUNCTION)) {
                Function * fn = (Function *)item;
                
                //if (fn->m_called_ops != 0 || fn->m_called_allocs != 0|| fn->m_called_invocations != 0)
                {
                    fn->m_called_ops = 0;
                    fn->m_called_allocs = 0;
                    fn->m_called_invocations = 0;
                }
            }
            else if (NULL != item && item->m_type == Item::MODULE) {
                Module * m = (Module *)item;
                
                //if (m->m_allocations != 0)
                {
                    m->m_allocations = 0;					
                }
            }
        }
        m_debug_config = 0;
    }
    // verbose description of execution
	else if (v == 2) {
		for (int p = 0; p < m_pairs.getCount(); p++)
		{
			Item * item = m_pairs[p];
			
			if (NULL != item && (item->m_type == Item::STATIC_FUNCTION || item->m_type == Item::MEMBER_FUNCTION)) {
				Function * fn = (Function *)item;
				
				if (fn->m_called_ops != 0 || fn->m_called_allocs != 0|| fn->m_called_invocations != 0)
				{
					ig_u32 pairs[2];
					this->_explodeConstantPair(fn->m_pair, pairs);
	
					printf("\"%s\",\"%s\",%d,%d,%d\n",_getStringForId(pairs[0]), _getStringForId(pairs[1]),	fn->m_called_ops,fn->m_called_allocs,
						fn->m_called_invocations);
					fn->m_called_ops = 0;
					fn->m_called_allocs = 0;
					fn->m_called_invocations = 0;
				}
			}
			else if (NULL != item && item->m_type == Item::MODULE) {
				Module * m = (Module *)item;
				
				if (m->m_allocations != 0)
				{
					ig_u32 pairs[2];
					this->_explodeConstantPair(m->m_pair, pairs);
	
					printf("\"%s\",\"%s\",0,%d,0\n",_getStringForId(pairs[0]), _getStringForId(pairs[1]),	m->m_allocations);
					m->m_allocations = 0;					
				}
			}
		}
		m_debug_config = 0;
	}
	// executive summary of executions
	else  if (v == 4) 
	{
		int ops = 0;
		int allocs = 0;
		int invocations = 0;
		for (int p = 0; p < m_pairs.getCount(); p++)
		{
			Item * item = m_pairs[p];
			
			if (NULL != item && (item->m_type == Item::STATIC_FUNCTION || item->m_type == Item::MEMBER_FUNCTION)) {
				Function * fn = (Function *)item;
				
				if (fn->m_called_ops != 0 || fn->m_called_allocs != 0|| fn->m_called_invocations != 0)
				{
					ops += fn->m_called_ops;
					allocs += fn->m_called_allocs;
					invocations += fn->m_called_invocations;
				}
			}
		}
		m_debug_config = 0;
		
		printf("IGVM ops: %d allocs: %d invocations: %d vm: %dKiB heap: %dKiB\n", ops, allocs, invocations,
				(int)(m_allocator.m_allocated / 1024), (int)(m_allocator_heap.m_allocated / 1024));
	}	
	return 0;
}

 int VMImp::_createByteArray(const void * data, int length, Box * out)
 {	
    igvm::Box ba_box;
    {
        igvm::Box params[1];
        params[0].i32 = (ig_i32)length; // <- we support at most 4GB. Which is MORE than enough

        if (0 != _call("iglang.ByteArray", "__ig_createWithCapacity", params, 1, &ba_box)) {
            return 1;
        }
    }
    
    *out = ba_box;
    
    if (data != NULL && length > 0)
    {
        igvm::ByteArray ba;
        if (0 != _getByteArray(ba_box, &ba, -1)) {
            return 1;
        }
        
        memcpy(ba.data, data, length);
    }
    
	return 0;
 }

int    VMImp::_getByteArray(const Box & handle, ByteArray * array, int set_length)
{
	if (handle.u32 == 0) {
		return 1;
	}

	// make sure the object we're referencing can't be GC'd
	this->_pushObjectToScope(handle.u32);
	
	Box ba_len;
	Box ba_array;
	
	if (set_length > 0) {
		Box ret;
		Box params[2];
		params[0]     = handle;
		params[1].i32 = set_length;
		if (0 != this->_call("iglang.ByteArray", "ensureCapacity", params, 2, &ret)) {
			goto getByteArray_FAIL;
		}
		
		ba_len.i32 = 0;
		if (0 != this->_setField("iglang.ByteArray", "m_position", handle, ba_len)) {
			goto getByteArray_FAIL;
		}
		
		
		ba_len.i32 = set_length;
		if (0 != this->_setField("iglang.ByteArray", "m_length", handle, ba_len)) {
			goto getByteArray_FAIL;
		}
		
		// should this also set the position?
		
	}
	else
	{
		// access the length field
		if (0 != this->_getField("iglang.ByteArray", "m_length", handle, &ba_len)) {
			goto getByteArray_FAIL;
		}
	}
	
	// access the backing array
	if (0 != this->_getField("iglang.ByteArray", "m_data", handle, &ba_array)) {
		goto getByteArray_FAIL;
	}
	
	Array data;
	if (0 != this->_getArray(ba_array, &data)) {
		goto getByteArray_FAIL;
	}
	
	array->length = ba_len.i32;
	array->data   = data.data;

	return 0;
	
getByteArray_FAIL:
	return 1;	
}


ig_u32 VMImp::_hashString(const char * utf8_string, size_t byte_length)
{
	const int FNV_32_PRIME = ((ig_u32)0x01000193);

	unsigned char *s = (unsigned char *)utf8_string;	/* unsigned string */
	ig_u32 hval = 0;
	/*
	 * FNV-1a hash each octet in the buffer
	 */
	for (int i = 0; i < byte_length; i++)
	{
		/* xor the bottom with the current octet */
		hval ^= (ig_u32)*s;
		s++;
		hval *= FNV_32_PRIME;
	}

	/* return our new hash value */
	return hval;
}


void VMImp::_constantStringReinit()
{
	for (int i = 0; i < m_string_hash_size; i++)
	{
		StringEntry * entry = m_string_hash[i];
		m_string_hash[i] = NULL;
		while (entry != NULL)
		{
			StringEntry * to_delete = entry;
			entry = entry->m_next;
			IGVM_FREE(to_delete);
		}
	}
}

ig_u32 VMImp::_constantString(const char * utf8_string, size_t unclamped_byte_length, bool create)
{
	// clamp the maximum string length
	ig_i32 byte_length = 0x7FFFFFFF;
	if (unclamped_byte_length < 0x7FFFFFFF) {
		byte_length = (ig_i32) unclamped_byte_length;
	}
	
	ig_u32 hash = _hashString(utf8_string, byte_length);


	unsigned index = hash % m_string_hash_size;

	StringEntry * entry = m_string_hash[index];
	while (entry != NULL)
	{
		if (entry->m_hash == hash &&
			entry->m_length == byte_length &&
			strncmp( _getStringForId(entry->m_offset), utf8_string, byte_length) == 0) {
			return entry->m_offset;
		}

		entry = entry->m_next;
	}

	if (!create) {
		return 0;
	}


	int offset_ref = m_string_offsets.alloc(1);
	int idx = m_strings.alloc(byte_length + 1);
	
	m_string_offsets[offset_ref] = idx;

	// no record found, thus create a new string entry
	entry = (StringEntry *)IGVM_MALLOC(sizeof(StringEntry));
	entry->m_next   = m_string_hash[index];
	entry->m_offset = offset_ref;		// was idx
	entry->m_hash   = hash;
	entry->m_length = byte_length;

	char * ptr = (char *) m_strings.getPointer(idx);
	// copy the string and append a '\0' after it (to make a proper c string)
	memcpy(ptr, utf8_string, byte_length);
	ptr[byte_length] = '\0';

	m_string_hash[index] = entry;
	
	//printf("Constant string added: %s (offset: %d)\n", ptr, entry->m_offset);

	return entry->m_offset;
}

/**
 * Convert a string id into a const char * cstring pointer
 */ 
const char * VMImp::_getStringForId(ig_u32 u)
{
	return (const char * ) m_strings.getPointer(m_string_offsets[u]);
}

void  VMImp::_constantPairDebug(ig_u32 offset, int mode) {
	for (int i = 0; i < m_pair_hash_size; i++)
	{
		PairEntry * entry = m_pair_hash[i];
		while (entry != NULL)
		{
			if (entry->m_offset == offset) {
				int p0 = entry->m_p0;
				int p1 = entry->m_p1;

				if (mode == 0) {
					fprintf(stderr, "\tconstantPairDebug '%s' '%s'\n", VMImp::_getStringForId(p0), VMImp::_getStringForId(p1));
				}
				else if (mode == 2) {
					fprintf(stderr, "'%s' '%s'", VMImp::_getStringForId(p0), VMImp::_getStringForId(p1));
				}
				else if (mode == 20) {
					fprintf(stderr, "'%s' '%s'", VMImp::_getStringForId(p0), VMImp::_getStringForId(p1));
				}
				else if (mode == 30) {
					fprintf(stderr, "%s", VMImp::_getStringForId(p0));
				}
				return;
			}

			entry = entry->m_next;
		}
	}
}

void VMImp::_explodeConstantPair(ig_u32 pair,  ig_u32 pairs[2])
{
	pairs[0] = m_pair_components[pair]->m_p0;
	pairs[1] = m_pair_components[pair]->m_p1;
}


void VMImp::_constantPairReinit()
{
	for (int i = 0; i < m_pair_hash_size; i++)
	{
		PairEntry * entry = m_pair_hash[i];
		m_pair_hash[i] = NULL;

		while (entry != NULL)
		{

			PairEntry * to_delete = entry;
			int idx = to_delete->m_offset;

			entry = entry->m_next;

			// TODO!!!!
			// figure out best way to reclaim handles
			// ie we're going to have constant strings referenced
			// that have a > 0 ref count... or should we just manually set the ref count

			if (m_pairs[idx] != NULL)
			{
				if (m_pairs[idx]->m_type == Item::MODULE ||
					m_pairs[idx]->m_type == Item::STRING)
				{
					// DO NOTHING. DELETIONS HAVE TO OCCUR LATER
				}
				else if (m_pairs[idx]->m_type == Item::STATIC_VARIABLE) {
					Variable * v = (Variable *) m_pairs[idx];
					
					// swap out all of the static variables
					if (v->getStorageClass() == 18) {
						ig_i32 * ptr = (ig_i32 *) m_static_data.getPointer(v->m_offset);
						m_config._swap_ref_32(this, *ptr, 0, false);
						
						*ptr = 0;
					}
					
					
					m_pairs[idx] = NULL;
				}
				else
				{
					// THESE ARE NOT OBJECTS THAT CAN BE DELETED
					// MUST NULL OUT NOW TO AVOID BECOMING INVALID
					// IN THE CASE THAT A MODULE DEALLOCS ITS FUNCTIONS
					// WHICH WOULD BE QUERIED IN FURTHER CHECKS
					m_pairs[idx] = NULL;
				}
			}

			IGVM_FREE(to_delete);
			m_pair_components[idx] = NULL;		// points back to the item just deleted
		}
	}

	for (int idx = m_pairs.getCount() - 1; idx >= 0; idx--)
	{
		if (m_pairs[idx] != NULL && m_pairs[idx]->m_type == Item::MODULE)
		{
			Module * m = (Module *)m_pairs[idx];

			IGVM_FREE(m->m_static_variables);
			IGVM_FREE(m->m_member_variables);
			IGVM_FREE(m->m_all_functions);
			IGVM_FREE(m->m_string_remap);
			IGVM_FREE(m->m_pair_remap);
			IGVM_FREE(m->m_gc_object_offsets);
			IGVM_FREE(m->m_ref_count_fields);
			IGVM_FREE(m->m_instance_defaults);
			IGVM_FREE(m->m_vtable);
			IGVM_FREE(m->m_data_pool);
			IGVM_FREE(m->m_final_data_pool );
			
			
			memset(m, 0, sizeof(Module));

			IGVM_FREE(m);
		}
		else if (m_pairs[idx] != NULL && m_pairs[idx]->m_type == Item::STRING)
		{
			String * s = (String *)m_pairs[idx];
			memset(s, 0, sizeof(String));
			IGVM_FREE(s);
		}

		m_pairs[idx] = NULL;
	}
}

/**
 * Create a constant pair out of two input constant strings
 */

ig_u32 VMImp::_constantPair(ig_u32 string0, ig_u32 string1, bool create)
{
	// create a hash based on combining the two string keys.
	// clearly we want to avoid situations where both strings are equivalent
	unsigned index = (string0 ^ string1) % m_pair_hash_size;

	// check to see if the constant pair already exists
	PairEntry * entry = m_pair_hash[index];
	while (entry != NULL)
	{
		if (entry->m_p0 == string0 && entry->m_p1 == string1) {
			return entry->m_offset;
		}
		entry = entry->m_next;
	}

	// 0 is considered the null constant pair
	if (!create) {
		return 0;
	}

	int idx = m_pairs.alloc(1);
	if (idx != m_pair_components.alloc(1)) {
		dieAPainfullDeath("constantPair critical failure");
	}

	// no record found, thus create a new entry
	entry = (PairEntry *)IGVM_MALLOC(sizeof(PairEntry));
	entry->m_next   = m_pair_hash[index];
	entry->m_offset = idx;
	entry->m_p0     = string0;
	entry->m_p1     = string1;

	m_pair_hash[index] = entry;

	m_pairs[idx]           = NULL;
	m_pair_components[idx] = entry;

	return entry->m_offset;
}



/**
 * Load a module given by the input path
 * path_in will be of the format
 * 	"iglang.Array<iglang.String>"
 */

int VMImp::_loadModule(const char * path_in)
{
	if (path_in == NULL) {
		return 1;
	}
	
	//printf("loadModule: %s\n", path_in);
	int ec = _loadModuleInternal(path_in, 0);
	if (ec != 0) {
		return ec;
	}


	{
		ig_u32 string_idx;
		while (m_load_stack.pop(&string_idx))
		{
			 //= m_load_stack.next();
			ig_u32 pair = this->_constantPair(string_idx, 0, false);


			const char * module_path = _getStringForId(string_idx);

			// its very possible that a pair # was not assigned at this point
			// this is particular possible for enums where an instance is never actually created
			// thus there would be no (NAME,0) pair.
			if (pair == 0 || m_pairs[pair] == NULL) {


				//TAB(0); printf(" -- load stack processing -- \n");
				if (0 != _loadModuleInternal(module_path, 0)) {
					fprintf(stderr, "\t called from: %s\n", path_in);
					return 1;
				}
			}
		}
	}

	{
		Module * m;
		while (m_link_stack.pop(&m))
		{
			int ec1 = this->_processModuleCode(*m, 1);
			if (ec1 != 0) {
				return ec1;
			}
		}
	}
	
	{
		ig_u32 string_idx;		
		while (m_init_stack.dequeue(&string_idx))
		{
			ig_u32 static_constructor_pair = this->_constantString("<static>", strlen("<static>"), true);
			ig_u32 pair                    = this->_constantPair(string_idx, static_constructor_pair, false);

			if (m_pairs[pair] != NULL)
			{
				Function * fn = (Function *)m_pairs[pair];
				Module   * m  = fn->m_module;

				Box param;
				Box ret;

				ec = this->invoke(NULL, fn, 0, &param, &ret);
				if (ec != 0) {
					//printf("Failed to load statics for module: %s\n", _getStringForId(string_idx));
					return ec;
				}

				m->m_statics_initialized = true;
			}
		}
	}

	return 0;
}


/**
 * Load a module
 * @param path_in - is guaranteed to not be null, and be a cstring
 */




int VMImp::_loadModuleInternal(const char * path_in /*not null*/, int height)
{
	size_t path_len = strlen(path_in);
	
	///////////////////////////////
	// check to see if the module has already been loaded
	///////////////////////////
	ig_u32 path_const = this->_constantString(path_in, path_len, false);
	if (0 != path_const) {
		ig_u32 path_pair_const = this->_constantPair(path_const, 0, false);
		if (0 != path_pair_const)
		{
			Item * existing = m_pairs[path_pair_const];

			// this isn't a module (error)
			if (NULL != existing && Item::MODULE != existing->m_type) {
				return 1;
			}
			// this is the module, and its already loaded
			else if (NULL != existing)
			{
				return 0;
			}
		}
	}

	// attempt to open the file in question
	void * fp = m_config._open(this, path_in);
	if (NULL == fp) {
		fprintf(stderr, "couldn't open: %s\n", path_in);
		
		// is this m_error_message even useful anymore?
		snprintf(m_error_message, MAX_ERROR_MESSAGE_LENGTH - 1, "error: could not open module: %s", path_in);
		m_error_message[ MAX_ERROR_MESSAGE_LENGTH - 1] = '\0';

		return 2;
	}


	Module & m = *(Module *)IGVM_MALLOC( sizeof(Module));
	memset(&m, 0, sizeof(Module));

	m.m_type = Item::MODULE;


	// read the header of the module
	ModuleHeader & header = m.m_header;
	m_config._read(this, fp, &header, sizeof(ModuleHeader));

	const int VM_VERSION = 0x0006;
	if (header.m_version != VM_VERSION) {
		fprintf(stderr, "igvm: FATAL ERROR: Improper version found: %d expected: %d \n", header.m_version, VM_VERSION);

		IGVM_FREE(&m);
		m_config._close(this, fp);
		
		return 3;
	}

	// read the list of static variables
	Variable * static_variables = (Variable *) IGVM_MALLOC( sizeof(Variable) * header.m_static_variable_count);
	for (int i = 0; i < header.m_static_variable_count; i++) {
		static_variables[i].m_type = Item::STATIC_VARIABLE;
		m_config._read(this, fp, &( static_variables[i].m_header), sizeof(VariableHeader));
	}

	// read the list of member variables
	Variable * member_variables = (Variable *) IGVM_MALLOC( sizeof(Variable) * header.m_member_variable_count);
	for (int i = 0; i < header.m_member_variable_count; i++) {
		member_variables[i].m_type = Item::MEMBER_VARIABLE;
		m_config._read(this, fp, &(member_variables[i].m_header), sizeof(VariableHeader));
	}

	// create a giant buffer for all of the functions
	int all_function_count = header.m_member_function_count + header.m_static_function_count;
	Function * all_functions = (Function *) IGVM_MALLOC( sizeof(Function) * all_function_count);

	// read the list of static functions
	Function * static_functions = (Function *) &all_functions[0];
	for (int i = 0; i < header.m_static_function_count; i++) {
		static_functions[i].m_type = Item::STATIC_FUNCTION;
		static_functions[i].m_offset             = 0;
		static_functions[i].m_native             = NULL;
		static_functions[i].m_called_ops         = 0;
		static_functions[i].m_called_allocs      = 0;
		static_functions[i].m_called_invocations = 0;
		m_config._read(this, fp, &(static_functions[i].m_header), sizeof(FunctionHeader));

		static_functions[i].m_param_count = static_functions[i].m_header.m_parameter_count;			
		static_functions[i].m_local_count = static_functions[i].m_header.m_local_count;

	}

	// read the list of member functions
	Function * member_functions = (Function *) &all_functions[header.m_static_function_count];
	for (int i = 0; i < header.m_member_function_count; i++) {
		member_functions[i].m_type = Item::MEMBER_FUNCTION;
		member_functions[i].m_offset             = 0;
		member_functions[i].m_native             = NULL;
		member_functions[i].m_called_ops         = 0;
		member_functions[i].m_called_allocs      = 0;
		member_functions[i].m_called_invocations = 0;
		m_config._read(this, fp, &(member_functions[i].m_header), sizeof(FunctionHeader));
		
		member_functions[i].m_param_count = member_functions[i].m_header.m_parameter_count;			
		member_functions[i].m_local_count = member_functions[i].m_header.m_local_count;			
	}

	// store the pointers to all of the functions and variables contained in this module
	m.m_static_variables = static_variables;	// TO FREE
	m.m_member_variables = member_variables;	// TO FREE
	m.m_all_functions    = all_functions;		// TO FREE

	// convenience pointers
	m.m_static_functions = static_functions;
	m.m_member_functions = member_functions;

	// remap our internal strings to global strings
	ig_u32 * string_remap = (ig_u32 *) IGVM_MALLOC( sizeof(ig_u32) * header.m_string_entries);
	{
		// create a table of all of the string lengths
		ig_i32 * string_lengths = (ig_i32 *)IGVM_MALLOC( sizeof(ig_i32) * header.m_string_entries);
		char * string_data      = (char *) IGVM_MALLOC( sizeof(char) * header.m_string_pool_size);

		// read in an array of string lengths
		m_config._read(this, fp, string_lengths, sizeof(ig_i32) * header.m_string_entries);

		// read in all of the UTF-8 String data
		m_config._read(this, fp, string_data, sizeof(char) * header.m_string_pool_size);

		// iterate through each string and add it to the global constant pool
		int offset = 0;
		for (int i = 0; i < header.m_string_entries; i++)
		{
			string_remap[i] = this->_constantString(&string_data[offset], string_lengths[i], true);
			offset += string_lengths[i];
		}

		// free the temporary structures
		IGVM_FREE(string_data);
		IGVM_FREE(string_lengths);
	}

	// remap our internal pairs to global pairs
	// we need all of these to be resolved before we can do final code processing
	ig_u32 * pair_remap = (ig_u32 *)IGVM_MALLOC( sizeof(ig_u32) * header.m_pair_pool_entries);
	{
		ig_u32 * pair_pool = (ig_u32 *) IGVM_MALLOC( sizeof(ig_u32) * 2 * header.m_pair_pool_entries);
		m_config._read(this, fp, pair_pool, sizeof(ig_u32) * 2 * header.m_pair_pool_entries);

		for (int i = 0; i < header.m_pair_pool_entries; i++)
		{
			ig_u32 string0 = string_remap[pair_pool[i * 2 + 0]];
			ig_u32 string1 = string_remap[pair_pool[i * 2 + 1]];

			// all pairs refer to exact file names
			// ie.  "iglang.String"
			this->m_load_stack.push(string0);

			pair_remap[i] = this->_constantPair(string0, string1, true);
		}
		IGVM_FREE(pair_pool);
	}

	m.m_string_remap = string_remap;	// TO FREE
	m.m_pair_remap   = pair_remap;		// TO FREE

	/// read in all of the unlinked code
	char * data_pool = (char *)IGVM_MALLOC( header.m_data_pool_size);
	m_config._read(this, fp, data_pool, header.m_data_pool_size);		
	
	// create an additional buffer for the linked code
	char * final_data_pool = (char *)IGVM_MALLOC( header.m_data_pool_size);




	//////////////////////////////
	// All data is read at this point
	////////////////////////////////
	m_config._close(this, fp);



	// get the pair for the object this extends
	Module * extends_module = NULL;
	ig_u32 extendm_pair = this->_constantPair(string_remap[m.m_header.m_extends], 0, false);
	////D//printf("extends-pair: %d %s\n", extendm_pair, &m_strings[string_remap[m.m_header.m_extends]]);

	// verify that this pair isn't 0, which means the null pair and
	// that the pair hasn't already been loaded
	if (0 == extendm_pair || m_pairs[extendm_pair] == NULL) 
	{
		//  load the file in question

		if (m.m_header.m_extends != 0)
		{
			if (0 != this->_loadModuleInternal(VMImp::_getStringForId(string_remap[m.m_header.m_extends]), height + 1)) {
				//printf("failed to load extends module\n");
				fprintf(stderr, "\t called fom: %s\n", path_in);
				return 1;
			}

			extendm_pair   = this->_constantPair(string_remap[m.m_header.m_extends], 0, false);
			extends_module = (Module *)m_pairs[extendm_pair];

			if (extendm_pair == 0 || extends_module == NULL) {
				return 1;
				//dieAPainfullDeath("Couldn't find extended module");
			}
		}
		else
		{
			extendm_pair = 0;
		}
	}
	else
	{
		extends_module = (Module *)m_pairs[extendm_pair];
	}

	// TODO validate that this is actually correct


	// link all necessary functions
	{
		ig_u32 module_name = string_remap[m.m_header.m_name];

		// TODO SAFEGUARD
		m.m_pair = this->_constantPair(module_name, 0, true);
		m_pairs[m.m_pair] = &m;

		//D//printf("Module pair: %d for %p\n", m.m_pair, &m);


		/////////////////
		// Process Member Variables
		////////////////
		{
			int member_variable_size = 0;		// <- how big the object will be in memory
			int gc_object_handle_count = 0;		// <- number of handles (Object,Function Pointers) contained within this object
			int ref_count_field_count = 0;		// <- number of references contained within this object
			Module * extends_mod = NULL;

			// calculate the size required by the parent
			if (extendm_pair != 0) 
			{
				extends_mod = (Module *)m_pairs[extendm_pair];
				member_variable_size   = extends_mod->m_instance_size;
				
				gc_object_handle_count = extends_mod->m_gc_object_handle_count;
				ref_count_field_count  = extends_mod->m_ref_count_field_count;
			}
			else
			{
				// vtable sits in the first 8 bytes of the object
				// a ig_i64 data[1] follows.  We're eliminating that count
				member_variable_size = sizeof(ObjectHeader) - sizeof(ig_i64);
			}



			// figure out the # of bytes needed in each object instance
			// and assign the correct offset to each member variable
			{
				for (int i = 0; i < header.m_member_variable_count; i++) {
					Variable & v = member_variables[i];

					const int size_of_variable = v.getStorageSize();
					
					// place each variable on the correct stride
					while ((member_variable_size % size_of_variable) != 0) {
						member_variable_size++;
					}

					v.m_offset = member_variable_size;
					if (v.getHandleOffset() >= 0 ) { gc_object_handle_count ++;  }
					if (v.getStorageClass() == Variable::STORAGE_CLASS_32_REFCNT) { ref_count_field_count ++;   }
					member_variable_size += size_of_variable;
				}
			}

			
			// create a list of all the ref counted fields
			m.m_ref_count_fields       = (ig_i16 *) IGVM_MALLOC( sizeof(ig_i16) * (ref_count_field_count + 1)); 
			m.m_ref_count_field_count  = ref_count_field_count;
			
			
			// create a list of all of the offsets that point to handles
			// this will be used for gc purposes later
			
			m.m_gc_object_offsets      = (ig_i16 *) IGVM_MALLOC( sizeof(ig_i16) * (gc_object_handle_count + 1));
			m.m_gc_object_handle_count = gc_object_handle_count;
			{
				// grab the list from its parent and merge it in
				
				int rc_offset = 0;
				int gc_offset = 0;
				if (NULL != extends_mod) 
				{
					// copy over the parent's gc table
					memcpy(m.m_gc_object_offsets, extends_mod->m_gc_object_offsets, sizeof(ig_i16) * extends_mod->m_gc_object_handle_count);
					gc_offset = extends_mod->m_gc_object_handle_count;
					
					// copy over the parent's ref count table
					memcpy(m.m_ref_count_fields, extends_mod->m_ref_count_fields, sizeof(ig_i16) * extends_mod->m_ref_count_field_count);
					rc_offset = extends_mod->m_ref_count_field_count;
				}

				// add in all of the member variables contained by this module
				for (int i = 0; i < header.m_member_variable_count; i++) {
					Variable & v = member_variables[i];
					int gco = v.getHandleOffset();
					if (gco != -1) {
						m.m_gc_object_offsets[gc_offset] = v.m_offset;
						gc_offset++;
					}
					if (v.getStorageClass() == 18) {
						m.m_ref_count_fields[rc_offset] = v.m_offset;
						rc_offset++;
					}
				}
				
				// terminate the list with the -1 sentinel
				m.m_gc_object_offsets[gc_offset] = -1;
				m.m_ref_count_fields[rc_offset] = -1; 
			}

			// get the default values for an instance (note a vtable reference will be created later)
			{
				char * module_defaults = (char *)IGVM_MALLOC( member_variable_size);
				memset(module_defaults, 0, member_variable_size);

				// copy the default values of the parent
				if (NULL != extends_module) {
					memcpy(module_defaults, extends_module->m_instance_defaults, extends_module->m_instance_size);
				}

				// insert our member variables into the pairs table
				for (int i = 0; i < header.m_member_variable_count; i++) {
					Variable & v = member_variables[i];

					v.m_pair            = this->_constantPair(module_name, string_remap[v.m_header.m_name], true);
					v.m_type_pair       = this->_constantPair(string_remap[v.m_header.m_type], 0, true);
					v.m_name            = string_remap[v.m_header.m_name];
					v.m_exact_type_name = string_remap[v.m_header.m_exact_type];	// EXACT TYPE
					m_pairs[v.m_pair]   = &v;
				}

				m.m_instance_defaults = module_defaults;	// TO FREE
				m.m_instance_size = member_variable_size;
			}

		}


		// allocate storage for static variables
		// If these are garbage collected (Objects, Function Pointers) they are place in the
		// static roots of the heap;
		////////////////////////
		for (int i = 0; i < header.m_static_variable_count; i++) {
			Variable & v = static_variables[i];

			v.m_pair      = this->_constantPair(module_name, string_remap[v.m_header.m_name], true);
			v.m_type_pair = this->_constantPair(string_remap[v.m_header.m_type], 0, true);
			v.m_name      = string_remap[v.m_header.m_name];
			v.m_exact_type_name = string_remap[v.m_header.m_exact_type];	// EXACT TYPE

			if (m_pairs[v.m_pair] != NULL) {  
				// SHOULD likely shove VM into critical error state
				return 0xD1;   // Static Variable Slot already initialized
			}

			m_pairs[v.m_pair] = &v;


			int storage_class = v.getStorageClass();
			if (storage_class == 16 || storage_class == 17)
			{
				int static_data_offset = m_heap.allocAndClearStaticRoot();
				v.m_offset             = static_data_offset;
			}
			else
			{
				// alloc storage space and assign default values
				int sz                 = 8;//v.getStorageSize();
				int static_data_offset = m_static_data.allocWithStride(sz, sz);

				v.m_offset = static_data_offset;

				// clear out the space reserved for the static variable
				void * ptr = m_static_data.getPointer(v.m_offset);
				memset(ptr, 0, sz);
			}
		}

		// register static functions
		////////////////////////////
		for (int i = 0; i < header.m_static_function_count; i++) {
			Function & func = static_functions[i];

			func.m_name   = string_remap[func.m_header.m_name];
			func.m_pair   = this->_constantPair(module_name, func.m_name, true);
			func.m_module = &m;

			if (m_pairs[func.m_pair] != NULL) {
				return 0xD0;		// Static Function Slot Already Initialized
			}

			m_pairs[func.m_pair] = &func;
		}

		//////////////////////////////////
		// Process member functions and create the vtables
		/////////////////////////////////
		{
			// evaluate the names for each function
			for (int i = 0; i < header.m_member_function_count; i++)
			{
				Function & func = member_functions[i];
				func.m_name = string_remap[func.m_header.m_name];
			}



			VTable * vtable = NULL;
			int      vtable_size = 0;

			VTable * parent_vtable = NULL;
			int      parent_vtable_size = 0;

			if (NULL != extends_module)
			{
				Module & e = *extends_module;
				parent_vtable      = e.m_vtable;
				parent_vtable_size = e.m_vtable_size;

				// out vtable size is identical to that of our parents to start withs
				vtable_size = parent_vtable_size;
				Function ** ef = e.m_vtable->m_functions;

				for (int i = 0; i < header.m_member_function_count; i++)
				{
					Function & func = member_functions[i];

					bool found = false;
					for (int j = 0; j < e.m_vtable_size; j++)
					{
						// this should actually be impossible for
						// ef[j] to equal NULL
						if (ef[j] != NULL &&
							ef[j]->m_name == func.m_name)
						{
							found = true;
							func.m_offset = j;
							break;
						}
					}

					// our entry wasn't found thus tack on an additional entry
					if (!found) {
						func.m_offset = vtable_size;
						vtable_size ++;
					}

				}
			}
			else
			{
				// no parent vtable is available
				for (int i = 0; i < header.m_member_function_count; i++)
				{
					Function & func = member_functions[i];
					func.m_offset = vtable_size;
					vtable_size ++;
				}
			}

			vtable = (VTable *) IGVM_MALLOC( sizeof(VTable) + (vtable_size + 1) * sizeof(Function *));
			if (NULL != parent_vtable) {
				// copy over the parents vtable
				memcpy(vtable, parent_vtable,
					sizeof(VTable) + parent_vtable_size * sizeof(Function *));
				vtable->m_super = parent_vtable;
			}
			else
			{
				// mark that there is no super vtable
				vtable->m_super = NULL;
			}

			// store the current pair for this member on the vtable
			// so that we can verify that objects are of a particular type
			// originally this was just the pair id, so that we could facilitate re-loading
			// but for the sake of native destructors and optimization it was changed to a raw pointer
			//vtable->m_member_pair = m.m_pair;
			vtable->m_module = &m;

			// apply the sentinel so that interface calls know where the vtable list ends
			// this means that no count needs to be stored
			vtable->m_functions[vtable_size] = NULL;


			// update our vtable with our functions, overwriting parent ones if they exist
			for (int i = 0; i < header.m_member_function_count; i++) {
				Function & func = member_functions[i];

				//D//printf("member function %d (%d)\n", func.m_header.m_data_offset, header.m_data_pool_size);
				func.m_pair = this->_constantPair(module_name, string_remap[func.m_header.m_name], true);
				m_pairs[func.m_pair] = &func;
				func.m_module = &m;
				vtable->m_functions[func.m_offset] = &func;

				// update the pair information
				// TODO SAFEGUARD
			}

			// the vtable could have been stored in a parallel array for all purposes
			// set up the defaults so that the vtable is always in the first entry
			((VTable **)m.m_instance_defaults)[0] = vtable;

			m.m_vtable         = vtable;			// TO FREE
			m.m_vtable_size    = vtable_size;
			m.m_extends_module = extends_module;
		}

	}

	m.m_data_pool = (unsigned char *) data_pool;				// TO FREE
	m.m_final_data_pool = (unsigned char *) final_data_pool;	// TO FREE
	// mode 0 is used to build up the list of items that must also be included
	int ec = this->_processModuleCode(m, 0);
	if (ec != 0) {
		return ec;
	}


	VMImp::m_link_stack.push(&m);

	// TODO process the other code blocks that need to be integrated.
	// these should have been found in processModuleCode pass 0


	// TODO need to run static initializers, and what not




	if (!m.m_statics_initialized) {
		if (string_remap[m.m_header.m_name] == 0)
		{
			fprintf(stderr, "fatal error.. null path const\n");
			dieAPainfullDeath("null path const");
		}
		VMImp::m_init_stack.push(string_remap[m.m_header.m_name]);
	}
	return 0;
}


int VMImp::_U32FromString(const Box & str, ig_u32 * dst, int capacity)
{
	dst[0] = 0;

	// is it a null pointer ?
	if (str.i32 == 0) {
		return 1;
	}
	
	this->_pushObjectToScope(str.u32);


	Box params[1];
	params[0] = str;

	int ec;

	// get the data field from the string object
	igvm::Box arg;
	if (0 != (ec = this->_getField("iglang.String", "m_data", params[0], &arg))) {
		return 1;
	}

	// convert the array handle into something that we can actually use
	igvm::Array sa;
	if (0 != this->_getArray(arg, &sa)) {
		// failed to access the short array
		return 1;
	}
	else
	{
		int len = (sa.length < (capacity-1)) ? sa.length : (capacity - 1);
		for (int i = 0; i < len; i++)
		{
			dst[i] = ((ig_u16 *)sa.data)[i];
		}
		dst[len] = 0;
	}

	return 0;
}

/**
 * Convert a String object into a C-String with a given capacity
 */

int VMImp::_UTF8BytesFromString(const Box & str, char * utf8, int capacity)
{
	utf8[0] = '\0';

	// is it a null pointer ?
	if (str.i32 == 0) {
		return 1;
	}
	
	this->_pushObjectToScope(str.u32);


	Box params[1];
	params[0] = str;

	int ec;

	// get the data field from the string object
	igvm::Box arg;
	if (0 != (ec = this->_getField("iglang.String", "m_data", params[0], &arg))) {
		return 1;
	}

	// convert the array handle into something that we can actually use
	igvm::Array sa;
	if (0 != this->_getArray(arg, &sa)) {
		// failed to access the short array
		return 1;
	}
	else
	{
		// process the data in the short array
		bool append_null = true;
		utf8[capacity-1] = '\0';

		if (append_null) {
			capacity --;
		}

		int dst_index = 0;


		for (int i = 0; i < sa.length; i++)
		{
			unsigned int c = ((ig_u16 *)sa.data)[i];

			// http://en.wikipedia.org/wiki/UTF-8

			// Just an ordinary ascii character
			////////////////////////////////////
			if      (c < 0x0000080)
			{
				// 0xxxxxxx
				if (capacity - dst_index < 1) break;

				utf8[dst_index] = (char)c;
				dst_index ++;
			}
			// Multibyte sequence
			/////////////////////////////////////
			else
			{
				int count = 0;
				int tag   = 0;

				if (c < 0x0000800)
				{
					count = 2;
					tag   = 0xC0;
					// 110xxxxx 	10xxxxxx
				}
				else if (c < 0x0010000)
				{
					// 1110xxxx 	10xxxxxx 	10xxxxxx
					count = 3;
					tag   = 0xE0;
				}
				else if (c < 0x0200000)
				{
					// 11110xxx 	10xxxxxx 	10xxxxxx 	10xxxxxx
					count = 4;
					tag = 0xF0;
				}
				else if (c < 0x4000000)
				{
					// 111110xx 	10xxxxxx 	10xxxxxx 	10xxxxxx 	10xxxxxx
					count = 5;
					tag = 0xF8;
				}
				else
				{
					// 1111110x 	10xxxxxx 	10xxxxxx 	10xxxxxx 	10xxxxxx 	10xxxxxx
					count = 6;
					tag = 0xFC;
				}

				// exit if we don't have enough room in the buffer
				if (capacity - dst_index < count)
				{
					break;
				}

				for (int j = count - 1; j > 0 ; j--)
				{
					utf8[dst_index + j] = 0x80 + (c & 0x3F);
					c = c >> 6;
				}
				utf8[dst_index] = tag + c;

				dst_index += count;

			}

		}

		if (append_null) {
			utf8[dst_index] = '\0';
		}

		return 0;
	}

}

int VMImp::_stringLength(const Box & str, int * length)
{
	*length = 0;
	// is it a null pointer ?
	if (str.i32 == 0) {
		return 1;
	}

	this->_pushObjectToScope(str.u32);

	Box params[1];
	params[0] = str;

	int ec;

	// get the data field from the string object
	igvm::Box arg;
	if (0 != (ec = this->_getField("iglang.String", "m_data", params[0], &arg))) {
		return 1;
	}

	// convert the array handle into something that we can actually use
	igvm::Array sa;
	if (0 != this->_getArray(arg, &sa)) {
		fprintf(stderr, "handleToShortArray failed: %d\n", arg.u32);
		return 1;
	}
	
	*length = sa.length;
	return 0;
}


/**
 * Calculate the number of bytes needed to represent the given string in UTF8
 * This does not include a terminating '\0' character
 */

int VMImp::_UTF8ByteLengthOfString(const Box & str, int * length)
{
	*length = 0;
	// is it a null pointer ?
	if (str.i32 == 0) {
		return 1;
	}

	this->_pushObjectToScope(str.u32);

	Box params[1];
	params[0] = str;

	int ec;

	// get the data field from the string object
	igvm::Box arg;
	if (0 != (ec = this->_getField("iglang.String", "m_data", params[0], &arg))) {
		return 1;
	}

	// convert the array handle into something that we can actually use
	igvm::Array sa;
	if (0 != this->_getArray(arg, &sa)) {
		fprintf(stderr, "handleToShortArray failed: %d\n", arg.u32);
		return 1;
	}
	else
	{

		int dst_index = 0;

		for (int i = 0; i < sa.length; i++)
		{
			unsigned int c = ((ig_u16 *)sa.data)[i];

			// http://en.wikipedia.org/wiki/UTF-8

			// Just an ordinary ascii character
			if      (c < 0x0000080) {
				// 0xxxxxxx
				dst_index ++;
			}
			// Multibyte sequence
			else if (c < 0x0000800) {
				dst_index += 2;
				// 110xxxxx 	10xxxxxx
			}
			else if (c < 0x0010000) {
				// 1110xxxx 	10xxxxxx 	10xxxxxx
				dst_index += 3;
			}
			else if (c < 0x0200000) {
				// 11110xxx 	10xxxxxx 	10xxxxxx 	10xxxxxx
				dst_index += 4;
			}
			else if (c < 0x4000000) {
				// 111110xx 	10xxxxxx 	10xxxxxx 	10xxxxxx 	10xxxxxx
				dst_index += 5;
			}
			else {
				// 1111110x 	10xxxxxx 	10xxxxxx 	10xxxxxx 	10xxxxxx 	10xxxxxx
				dst_index += 6;
			}
		}

	   *length = dst_index;

		return 0;
	}

}


/**
 * Convert a utf8 C string into a String object
 */


int VMImp::_stringFromUTF8BytesWithLength(const char * text, size_t length, Box * result)
{
	if (text == NULL || result == NULL) {
		return 0xC2;
	}
	
	// calculate the number of characters in the string
	// this is based on skipping utf8 characters that aren't start characters
	int character_count = 0;
	for (int i = 0; i < length; i++) {
		character_count += ((text[i] & 0xC0) != 0x80) ? 1 : 0;
	}

	 // find a reference to the pair for the short array that we're going to be using to back the data
	int name_id = this->_constantString("iglang.Array<short>", strlen("iglang.Array<short>"), false);
	int tmp16   = this->_constantPair(name_id, 0, false);
	if (tmp16 == 0) {
		return 0xC0;
	}

	// create our requested short array
	Module * module       = (Module *)m_pairs[tmp16];
	ig_u32   array_handle = this->_createModuleArrayInstance(module, character_count, 2);
	this->_pushObjectToScope(array_handle);
	
	// get the backing data for the short array
	Object * ptr          = m_heap.m_handles[array_handle];

	// cheap hack to get the head of the array
	ig_i16 * string = (ig_i16 *)(ptr->m_data + 1);

	Box parameters[1];
	parameters[0].u32 = array_handle;	//VMImp::createHandle(ptr, GC_LEAF);


	int string_idx  = 0;
	int i = 0;
	while (i < length)
	{
		int b0  = ((int)text[i]) & 0xff; i++;

		// this is a non ASCII sequence
		if ((b0 & 0x80) != 0)
		{
			// 2 bytes =   110xxxxx  10xxxxxx
			// 3 bytes =   1110xxxx  10xxxxxx  10xxxxxx

			int n  = (~b0) & 0xff;		// eg.     001XXXXX  (nlz tells us the number of 0's, negating gives us the # of 1s)
			int nlz  = 0;

			// this'll have at least 2 leading zeroes
			if ((n & 0xF0) == 0) { nlz += 4; n = n << 4; }
			if ((n & 0xC0) == 0) { nlz += 2; n = n << 2; }
			if ((n & 0x80) == 0) { nlz += 1;             }

			// mask out number of leading 1's + 1
			// +1 isn't necessary because the following binary digit is always a 0
			int b0_mask = 0x00ff >> nlz;

			b0             = b0 & b0_mask;
			int following  = nlz - 1;					// number of bytes following is leading 1's - 1

			while (following > 0) {
				int bN  = text[i] & 0x3f;  i++;
				b0 = (b0 << 6) | bN;
				following --;
			}
		}
	
		// make sure we never go over our calculated counts
		if (string_idx > character_count) {
			break;
		}
		
		string[string_idx] = b0;
		string_idx ++;
	}


	
	
	int ec;
	// create a string using the passed short array
	if (0 != (ec = this->_call("iglang.String", "__ig_shortArrayToStringNoCopy", parameters, 1, result))) {
		return 0xC1;
	}
	
	
	return 0;
}


int VMImp::_stringFromUTF8Bytes(const char * text, Box * result)	//, bool make_root)
{
	size_t length = strlen(text);

	int ec = _stringFromUTF8BytesWithLength(text, length, result);
	if (ec != 0) { return ec; }

	

	return 0;
}


//////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////
// Code Pre-validation and Linking
//////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////
/*
 * Process the code in the module discovering dependencies that need to be loaded
 * could this be done at a prior step... more than likely
 *
 * @param mode - 0 - VALIDATION (gathers required modules to load), stack height calculation
 *               1 - LINKING    (overrides existing ops with more compact ones, and finalizes pairs and jump offsets)
 */

const int PROCESS_MODE_VALIDATION = 0;
const int PROCESS_MODE_LINKING = 1;


//static int s_trace_count = 0;

int VMImp::_processModuleCode(Module & module, int mode)
{
	Function * all_functions = module.m_all_functions;
	int all_function_count =
		module.m_header.m_member_function_count +
		module.m_header.m_static_function_count;

	ig_u32 * string_remap           = module.m_string_remap;
	ig_u32 * pair_remap             = module.m_pair_remap;
	unsigned char * data_pool       = module.m_data_pool;			// data stored in files, can only be mutated in a non finalized fashion
	unsigned char * final_data_pool = module.m_final_data_pool;		// data processed by us



	ig_u32 CONST_length__get = this->_constantString("length__get", strlen("length__get"), true);
	ig_u32 CONST_array_get   = this->_constantString("<get[]>", strlen("<get[]>"), true);

	// TODO move to some other caching place?
	ig_u32 PAIR_Object_stack_trace = this->_constantPair(
		this->_constantString("iglang.Object", strlen("iglang.Object"), true),
		this->_constantString("stackTrace", strlen("stackTrace"), true),
		true);
		
	ig_u32 PAIR_Vector_of_Object_length__get = this->_constantPair(
			this->_constantString("iglang.Vector<iglang.Object>", strlen("iglang.Vector<iglang.Object>"), true),
			CONST_length__get, 
			true
		);
		
	ig_u32 PAIR_Vector_of_Object__get = this->_constantPair(
			this->_constantString("iglang.Vector<iglang.Object>", strlen("iglang.Vector<iglang.Object>"), true),
			CONST_array_get, 
			true
		);
	ig_u32 PAIR_Vector_of_int__get = this->_constantPair(
			this->_constantString("iglang.Vector<int>", strlen("iglang.Vector<int>"), true),
			CONST_array_get, 
			true
		);
		
	ig_u32 PAIR_Array_of_short_contentsEqual = this->_constantPair(
			this->_constantString("iglang.Array<short>", strlen("iglang.Array<short>"), true),
			this->_constantString("contentsEqual", strlen("contentsEqual"), true),
			true);


	for (int i = 0; i < all_function_count; i++)
	{
		Function & func = all_functions[i];

		//int local_count = func.m_header.m_local_count;
		int data_offset = func.m_header.m_data_offset;
		int data_length = func.m_header.m_data_length;
		int code_length = func.m_header.m_code_count;
		const int jump_table_length = func.m_header.m_jump_table_length;

		//unsigned char * locals = (unsigned char *) func.m_header.getLocalPointer(data_pool);
		unsigned char  * code       = func.m_header.getCodePointer(data_pool);
		JumpTableEntry * jump_table = func.m_header.getJumpTablePointer(data_pool);
		
		if (PROCESS_MODE_LINKING == mode) {

			memcpy(&final_data_pool[data_offset], &data_pool[data_offset], data_length);

			// update the functions code pointers
			func.m_local_defaults  = func.m_header.getLocalPointer    (final_data_pool);
			func.m_code            = func.m_header.getCodePointer     (final_data_pool);
			func.m_exception_table = func.m_header.getExceptionPointer(final_data_pool);
			func.m_debug_table     = func.m_header.getDebugPointer    (final_data_pool);
			func.m_jump_table      = func.m_header.getJumpTablePointer(final_data_pool);
		}


		// TODO this needs to account for stack height at various jump points...
		// ie.. make sure they are consistent across all paths
		int stack_height = 0;
		int stack_height_max = 0;
		//int local_count_max = 0;
		
		bool bad_assembly = false;
		
		int prev_op = InOp::NOP;
		
		for (int read_head = 0; read_head < code_length; read_head++) 
		{
REPROCESS_OP:		
			const int curr_stack_height = stack_height;
			const int j0 = read_head;
			int op = code[read_head];
		
			
			//if (op == InOp::NOP) {
			//	printf("NOP ");
			//}

			// Verify that the op is valid
			///////////////////////////////
			if (!s_in_ops_valid[op]) 
			{
				fprintf(stderr, "IGVM Invalid op found in bytecode: %d\n", op);
				ig_u32 p[2];
				this->_explodeConstantPair(func.m_pair, p);
				fprintf(stderr, "\tBad code in. '%s' '%s'\n", _getStringForId(p[0]), _getStringForId(p[1]));

				// this should just make the VM unusable
				dieAPainfullDeath("IGVM Invalid op found in bytecode");
			}
			
			

			// remap all CALL_SUPER's to CALL_STATIC's
			if (op == InOp::CALL_SUPER && mode == 1) {
				func.m_code[read_head] = InOp::CALL_STATIC_VTABLE;
			}
			else if (op == InOp::CALL_RET1_SUPER && mode == 1) {
				func.m_code[read_head] = InOp::CALL_RET1_STATIC_VTABLE;
			}
			int modifiers = s_in_ops[op];

			if (PROCESS_MODE_VALIDATION == mode)
			{
			
				bool prev_is_flow_terminator = (
								prev_op == InOp::JMP || 
								prev_op == InOp::INTERNAL_JMP_LO ||
								prev_op == InOp::THROW || 
								prev_op == InOp::RET1 ||
								prev_op == InOp::RET0 ||
								prev_op == InOp::RET_FUNCPTR ||
								prev_op == InOp::RET_OBJ);
			
				bool found = false;
				for (int k = 0; k < jump_table_length; k++) 
				{
					if (jump_table[k].m_offset == read_head) 
					{
						JumpTableEntry * ent = &jump_table[k];
						
						if (op == InOp::CATCH_ENTRY)
						{
							// force the stack height down to 0
							// sho8uld do this outside of the loop
							//stack_height = 0;
							
							if (stack_height != 0) 
							{
								ig_u32 p[2];
								this->_explodeConstantPair(func.m_pair, p);
								fprintf(stderr, "\t'%s' '%s'\n", _getStringForId(p[0]), _getStringForId(p[1]));
								fprintf(stderr, "IGVM Stack bleeding into start of catch: %d\n", op);
								
								bad_assembly = true;
							}
						}
						else if (op == InOp::NOP_TERM)
						{
							// force the stack height down to 0
							// sho8uld do this outside of the loop
							//stack_height = 0;
							
							if (stack_height != 0) 
							{
								ig_u32 p[2];
								this->_explodeConstantPair(func.m_pair, p);
								fprintf(stderr, "\t'%s' '%s'\n", _getStringForId(p[0]), _getStringForId(p[1]));
								fprintf(stderr, "IGVM Stack bleeding function termination: %d\n", op);
								
								bad_assembly = true;
							}
						}
						else if (prev_is_flow_terminator) 
						{
							if (ent -> m_ref_stack_height == 0x0000) 
							{
								{
									ig_u32 p[2];
									this->_explodeConstantPair(func.m_pair, p);
									fprintf(stderr, "\t'%s' '%s'\n", _getStringForId(p[0]), _getStringForId(p[1]));
									fprintf(stderr, "A. IGVM expected a jump table entry following a flow terminator: %d\n", op);
								
									bad_assembly = true;
								}
							}
							else
							{
								stack_height = ent -> m_stack_height;
								found = true;
							}
						}
						else
						{
							if (ent -> m_ref_stack_height == 0x0000) {
								ent -> m_ref_stack_height = 0xffff;
								ent -> m_stack_height = stack_height;
							}
							else if (ent -> m_stack_height != stack_height) {
							
								ig_u32 p[2];
								this->_explodeConstantPair(func.m_pair, p);
								fprintf(stderr, "\t'%s' '%s'\n", _getStringForId(p[0]), _getStringForId(p[1]));
					
								fprintf(stderr, "1. (offset: %d) IGVM conflicting stack heights found at jump target %d vs %d\n", read_head, ent->m_stack_height, stack_height);
							
								bad_assembly = true;
							}
						
						}
						
						break;
					}
				}
				
				if (!found && prev_is_flow_terminator) {
				
					if (op != InOp::CATCH_ENTRY && 
						op != InOp::NOP_TERM    && 
						!(op == InOp::RET0 &&  ((read_head + 1) == code_length || code[read_head + 1] == InOp::NOP_TERM)))
					{
						ig_u32 p[2];
						this->_explodeConstantPair(func.m_pair, p);
						fprintf(stderr, "\t'%s' '%s'\n", _getStringForId(p[0]), _getStringForId(p[1]));
				
						fprintf(stderr, "\tB. IGVM expected a jump table entry a flow terminator: %d posn: %d\n", op, read_head);
						if (op == InOp::RET0 && read_head < code_length) {
							fprintf(stderr, "\t\tnext-op: %d\n", code[read_head+1]);
						}
						
						bad_assembly = true;
					}
				}
			
			
				stack_height += s_in_ops_dir[op];
				if (stack_height > stack_height_max) {
					stack_height_max = stack_height;
				}
				
				
				//prev_op = op;
			}

			read_head++;




			//modifiers is split into 8 bit chunks
			//the first modifier is the lowest order 8 bytes
			// int mi = (modifier >> (8*i)) & 0xff;
			// bytes-to-read = mi & 0xf;

			while (modifiers > 0) {

				int m = modifiers & 0xff;
				int bytes_to_read = m & 0x0f;  

				// this abuses the fact that the system is little endian
				ig_i64 data = 0;
				memcpy(&data, &code[read_head], bytes_to_read);


				// TODO for locals ACTUALLY VERIFY EARLY THAT THE INDICIES EXIST (so we don't have to do it..
				// seriously just ad a different I16 type

				// TODO add in code to actually verify the stack height (this'd probably be here bob)

				if (m == InOp::I16_LOCAL || m == InOp::I8_LOCAL) {
					if (PROCESS_MODE_VALIDATION == mode)
					{
						if (data >= func.m_header.m_local_count) {
							fprintf(stderr, "exceeded declared local count: %d vs %d\n", (int)data, func.m_header.m_local_count);
							dieAPainfullDeath("exceeded declared local count");
							
							bad_assembly = true;
						}
						
						/*
						if (op == InOp::LOCAL_CLEAR) {
							s_trace_count ++;
							printf("LOCAL_CLEAR: %d (%d)\n", s_trace_count, data);
						}
						*/
						
						
						if (data < 128 && m == InOp::I16_LOCAL) 
						{
							if (InOp::LOCAL_LOAD == op) 
							{
								// rewrite LOCAL_LOAD_0 .. 5 => LL0 .. 5
								if (data < 5) {
									code[j0]            = InOp::LL0 + data;
									code[read_head + 0] = InOp::NOP;
									code[read_head + 1] = InOp::NOP;
								}
								else
								{
									//printf("remap test 1: %d\n", data);
									code[j0]            = InOp::LOCAL_LOAD_LO;
									code[read_head + 0] = data;
									code[read_head + 1] = InOp::NOP;	
								}
								
								// reprocess this generated code
								read_head = j0;
								stack_height = curr_stack_height;
								
								goto REPROCESS_OP;
							}
							else if (InOp::LOCAL_STORE == op) 
							{
								if (data < 8) {
									code[j0]            = InOp::I_LS0 + data;
									code[read_head + 0] = InOp::NOP;
									code[read_head + 1] = InOp::NOP;
								}
								else 
								{
									//printf("remap test 2: %d\n", data);
									code[j0]            = InOp::LOCAL_STORE_LO;
									code[read_head + 0] = data;
									code[read_head + 1] = InOp::NOP;
								}
								// reprocess this generated code
								read_head = j0;
								stack_height = curr_stack_height;
								goto REPROCESS_OP;
							}
							else if (InOp::LOCAL_CLEAR == op) {
								code[j0]            = InOp::INTERNAL_LOCAL_CLEAR_LO;
								code[read_head + 0] = data;
								code[read_head + 1] = InOp::NOP;

								// reprocess this generated code
								read_head = j0;
								stack_height = curr_stack_height;
								goto REPROCESS_OP;
							
							}
						}
					}
					

				}
				else if (m == InOp::I8_ARGS) {
					if (PROCESS_MODE_VALIDATION == mode)
					{
						stack_height -= data;
					}
				}
				else if (m == InOp::I16_PAIR) 
				{

					ig_u32 pair = pair_remap[data];

					if (PROCESS_MODE_LINKING == mode)
					{
						if (pair == PAIR_Object_stack_trace && (op == InOp::CALL_RET1_STATIC || op == InOp::CALL_RET1_STATIC_VTABLE))
						{
							func.m_code[j0] = InOp::STACK_TRACE;
							ig_u16 corrected_value = 0;				// overwrite with nops
							memcpy(&func.m_code[read_head], &corrected_value, sizeof(ig_u16));
						}
						else if (pair == PAIR_Vector_of_Object_length__get && op == InOp::CALL_RET1_STATIC_VTABLE)
						{
							func.m_code[j0] = InOp::INTERNAL_VECTOR_LENGTH;
							ig_u16 corrected_value = 0;				
							
							// overwrite with 3 nops   (arg count, and index)
							memcpy(&func.m_code[read_head-1], &corrected_value, sizeof(ig_u8));
							memcpy(&func.m_code[read_head],   &corrected_value, sizeof(ig_u16));
						}
						else if (pair == PAIR_Vector_of_Object__get && op == InOp::CALL_RET1_STATIC_VTABLE)
						{
							func.m_code[j0] = InOp::INTERNAL_VECTOR_OBJECT_GET;
							ig_u16 corrected_value = 0;				
							
							// overwrite with 3 nops   (arg count, and index)
							memcpy(&func.m_code[read_head-1], &corrected_value, sizeof(ig_u8));
							memcpy(&func.m_code[read_head],   &corrected_value, sizeof(ig_u16));
						}
						else if (pair == PAIR_Vector_of_int__get && op == InOp::CALL_RET1_STATIC_VTABLE)
						{
							func.m_code[j0] = InOp::INTERNAL_VECTOR_32_GET;
							ig_u16 corrected_value = 0;				
							
							// overwrite with 3 nops   (arg count, and index)
							memcpy(&func.m_code[read_head-1], &corrected_value, sizeof(ig_u8));
							memcpy(&func.m_code[read_head],   &corrected_value, sizeof(ig_u16));
						}
						else if (pair == PAIR_Array_of_short_contentsEqual && op == InOp::CALL_RET1_STATIC_VTABLE)
						{
							func.m_code[j0] = InOp::INTERNAL_ARRAY_COMPARE;
							ig_u8 storage_class = 2;	// <-- short
							ig_u16 corrected_value = 0;				
							
							// overwrite with storage class and 2 nops   (arg count, and index)
							memcpy(&func.m_code[read_head-1], &storage_class, sizeof(ig_u8));
							memcpy(&func.m_code[read_head],   &corrected_value, sizeof(ig_u16));
						
						}
						else {
							ig_u16 corrected_value = pair;
							memcpy(&func.m_code[read_head], &corrected_value, sizeof(ig_u16));
						}
					}
				}
				else if (m == InOp::I16_STRING) 
				{
					ig_u32 string_idx = string_remap[data];
					
					if ((string_idx & 0xffff0000) != 0) 
					{
						fprintf(stderr, "\t(%d) %s\n", string_idx, _getStringForId(string_idx));
						fprintf(stderr, "processModuleCode(mode=%d) Exceeded string capacity. String idx: 0x%x\n", mode, string_idx);
						dieAPainfullDeath("Exceeded string capacity");
					}

					if (PROCESS_MODE_LINKING == mode) 
					{
						ig_u16 corrected_value = string_idx;
						memcpy(&func.m_code[read_head], &corrected_value, sizeof(ig_u16));
					}
				}

				else if (m == InOp::I16_MEMBER_VARIABLE_OFFSET) 
				{
					if (data > module.m_header.m_pair_pool_entries) {
						//D//printf("\t\texceeded pair pool\n");
					}
					ig_u32 pair = pair_remap[data];

					Variable * item = (Variable *) m_pairs[pair];

					if (PROCESS_MODE_LINKING == mode)
					{
						bool do_standard_remap = true;
						// not a direct reference to a field
						// field most likely exists in a super class
						if (item == NULL)
						{
							ig_u32 exploded_pair[2];
							this->_explodeConstantPair(pair, exploded_pair);

							ig_u32 module_pair = this->_constantPair(exploded_pair[0], 0, false);
							Module * m = (Module *)m_pairs[module_pair];

							while (NULL != m) {
								int vc         = m->m_header.m_member_variable_count;
								Variable * mvs = m->m_member_variables;

								for (int k = 0; k < vc;  k++) {
									Variable & v = mvs[k];
									if (v.m_name == exploded_pair[1]) {
										item = &v;
										break;
									}
								}

								if (item != NULL || m->m_extends_module == NULL) {
									m = NULL;
								}
								else
								{
									m = m->m_extends_module;
								}
							}
						}

						if (item == NULL) {
							this->_constantPairDebug(pair);
							dieAPainfullDeath("Failed to identify pair");
						}


						//D//printf("\titem exists: %p %d => %d", item, pair, item->m_offset);
						//constantPairDebug(pair);
						
						const int item_storage_class = item->m_header.m_storage_class;
						if (op == InOp::MEMBER_STORE)
						{
							if (item_storage_class == 16) {
								func.m_code[j0] = InOp::I_MEMBER_STORE_OBJ;
							}
							else if (item_storage_class == 17) {
								func.m_code[j0] = InOp::I_MEMBER_STORE_FUNCPTR;
							}
							else if (item_storage_class == 18) {
								func.m_code[j0] = InOp::MEMBER_STORE_32_REF;
							}
							else if (item_storage_class <= 4) {
								func.m_code[j0] = InOp::MEMBER_STORE_32;
							}
						}
						else if (op == InOp::THIS_STORE)
						{
							//printf("caught this store\n");
							if (item_storage_class == 16) {
								func.m_code[j0] = InOp::I_THIS_STORE_OBJ;
							}
							else if (item_storage_class == 17) {
								func.m_code[j0] = InOp::I_THIS_STORE_FUNCPTR;
							}
							else if (item_storage_class == 18) {
								func.m_code[j0] = InOp::THIS_STORE_32_REF;
							}
							else if (item_storage_class <= 4) {
								func.m_code[j0] = InOp::THIS_STORE_32;
							}
						}
						//
						// re-map member loads base on
						// is it a 32 bit quantity?
						// is it addressable by an offset between [0,256)
						else if (op == InOp::MEMBER_LOAD)
						{
							if (item_storage_class <= 4 || 
								item_storage_class == 16 || 
								item_storage_class == 18) {
								func.m_code[j0] = InOp::MEMBER_LOAD_32;
							}
							
							if ( 0 <= item->m_offset && item->m_offset < 256) 
							{
								if (func.m_code[j0] == InOp::MEMBER_LOAD) {
									func.m_code[j0] = InOp::INTERNAL_MEMBER_LOAD_LO;
									do_standard_remap = false;
								}
								else if (func.m_code[j0] == InOp::MEMBER_LOAD_32) {
									func.m_code[j0] = InOp::INTERNAL_MEMBER_LOAD_32_LO;
									do_standard_remap = false;								
								}
							}
						}
						else if (op == InOp::LOAD_OBJ_FIELD)
						{
							if (item_storage_class <= 4 || 
								item_storage_class == 16 || 
								item_storage_class == 18) {
								func.m_code[j0] = InOp::LOAD_OBJ_FIELD_32;
							}
							
							//printf("LOAD OBJ FIELD\n");
						}
						else if (op == InOp::THIS_LOAD) 
						{
							if (item_storage_class <= 4 || 
								item_storage_class == 16 || 
								item_storage_class == 18) {
								func.m_code[j0] = InOp::THIS_LOAD_32;
							}
							
							if ( 0 <= item->m_offset && item->m_offset < 256) 
							{
								if (func.m_code[j0] == InOp::THIS_LOAD) {
									func.m_code[j0] = InOp::INTERNAL_THIS_LOAD_LO;
									do_standard_remap = false;
								}
								else if (func.m_code[j0] == InOp::THIS_LOAD_32) {
									func.m_code[j0] = InOp::INTERNAL_THIS_LOAD_32_LO;
									do_standard_remap = false;								
								}
							}
						}

						if (do_standard_remap) 
						{
							// remap the offset
							ig_u16 corrected_value = item->m_offset;
							memcpy(&func.m_code[read_head], &corrected_value, sizeof(ig_u16));
						}
						// 8 bit remap
						else 
						{
							//s_trace_count ++;
							//printf("8 bit remap: %d (%d)\n", s_trace_count, item->m_offset);
							func.m_code[read_head + 0] = item->m_offset;
							func.m_code[read_head + 1] = InOp::NOP;
						}
					}
				}
				else if (m == InOp::I16_STATIC_VARIABLE_OFFSET) {
					if (data > module.m_header.m_pair_pool_entries) {
						fprintf(stderr, "\t\texceeded pair pool\n");
					}
					ig_u32 pair = pair_remap[data];

					Variable * item = (Variable *) m_pairs[pair];

					if (PROCESS_MODE_LINKING == mode)
					{
						// not a direct reference to a field
						// field most likely exists in a super class
						if (item == NULL)
						{
							ig_u32 exploded_pair[2];
							this->_explodeConstantPair(pair, exploded_pair);

							ig_u32 module_pair = this->_constantPair(exploded_pair[0], 0, false);
							Module * m = (Module *)m_pairs[module_pair];

							while (NULL != m) {
								int vc         = m->m_header.m_static_variable_count;
								Variable * mvs = m->m_static_variables;

								for (int k = 0; k < vc;  k++) {
									Variable & v = mvs[k];
									if (v.m_name == exploded_pair[1]) {
										item = &v;
										break;
									}
								}

								if (item != NULL || m->m_extends_module == NULL) {
									m = NULL;
								}
								else
								{
									m = m->m_extends_module;
								}
							}
						}

						if (item == NULL) {
							this->_constantPairDebug(pair);
							dieAPainfullDeath("Failed to identify pair");
						}
						
						// rename the store to a store barrier if
						// the type being stored is an object or a function pointer
						if (op == InOp::STATIC_STORE)
						{
							if (item->m_header.m_storage_class == 16) {
								func.m_code[j0] = InOp::I_STATIC_STORE_OBJ;
							}
							else if (item->m_header.m_storage_class == 17) {
								func.m_code[j0] = InOp::I_STATIC_STORE_FUNCPTR;
							}
							else if (item->m_header.m_storage_class == 18) {
								func.m_code[j0] = InOp::STATIC_STORE_32_REF;
							}
						}
						else if (op == InOp::STATIC_LOAD)
						{
							if (item->m_header.m_storage_class == 16) {
								func.m_code[j0] = InOp::STATIC_LOAD_OBJ;
							}
							else if (item->m_header.m_storage_class == 17) {
								func.m_code[j0] = InOp::STATIC_LOAD_FUNCPTR;
							}
						}
						
						// remap the offset
						ig_u16 corrected_value = item->m_offset;
						memcpy(&func.m_code[read_head], &corrected_value, sizeof(ig_u16));
					}
				}
				else if (m == InOp::I16_FUNCTION_OFFSET) 
				{
					ig_u32 pair = pair_remap[data];

					Function * item = (Function *) m_pairs[pair];
					// find the function that the super call is pointing to
					if (op == InOp::CALL_SUPER || op == InOp::CALL_RET1_SUPER)
					{
						if (PROCESS_MODE_LINKING == mode) {
							// This is the case that the class calling super doesn't have itself
							// a function with the same name.
							if (NULL == item) 
							{
								ig_u32 items[2];
								this->_explodeConstantPair(pair, items);
								fprintf(stderr, "'%s' '%s'\n", _getStringForId(items[0]), _getStringForId(items[1])); 
							
							
							
								VTable * extends_vtable = module.m_extends_module->m_vtable;
								int fidx = 0;
								while (extends_vtable->m_functions[fidx] != NULL)
								{
									if (extends_vtable->m_functions[fidx]->m_name == items[1]) {
										break;
									}
									fidx ++;
								}
							
								if (NULL == extends_vtable->m_functions[fidx]) {
									fprintf(stderr, "'%s' '%s'\n", _getStringForId(items[0]), _getStringForId(items[1])); 
									dieAPainfullDeath("Failed to find super function to call");
								}
							
								item = extends_vtable->m_functions[fidx];
							}
							else
							{							
								if (item->m_module->m_extends_module == NULL) {
									dieAPainfullDeath("Invalid parent");
								}

								if (item->m_module->m_extends_module != module.m_extends_module) {
									dieAPainfullDeath("Invalid super call");
								}
							
								Function * old_item = item;

								// look into the extended classes vtable to figure out what
								// the pointer previously was
								VTable * extends_vtable = item->m_module->m_extends_module->m_vtable;
								item = extends_vtable->m_functions[old_item->m_offset];

								if (NULL == item || item->m_name != old_item->m_name) {
									dieAPainfullDeath("Failed to find item in extends vtable");
								}
							}

							// we actually need to remap this pair to that of the super class
							// so we actually need to have a reference to the vtable for this to happen
							//

						
							ig_u16 corrected_value = item->m_pair;
							memcpy(&func.m_code[read_head], &corrected_value, sizeof(ig_u16));
						}

					}
					// find the first function in the class heirarchy that matches the given name
					///////////////////////////
					else
					{


						if (PROCESS_MODE_LINKING == mode)
						{
							if (item == NULL)
							{
								ig_u32 exploded_pair[2];	// <--   klass, name
								this->_explodeConstantPair(pair, exploded_pair);

								ig_u32 module_pair = this->_constantPair(exploded_pair[0], 0, false);
								Module * m = (Module *)m_pairs[module_pair];

								int k = 0;
								Function ** v = m->m_vtable->m_functions;
								while (v[k] != NULL) {
									if (v[k]->m_name == exploded_pair[1]) {
										item = v[k];
										break;
									}
									k++;
								}
							}

							if (item == NULL) {
								this->_constantPairDebug(pair);
								dieAPainfullDeath("Failed to identity pair.");
							}


							//D//printf("\titem exists: %p %d => %d", item, pair, item->m_offset);
							//constantPairDebug(pair);

							ig_u16 corrected_value = item->m_offset;
							memcpy(&func.m_code[read_head], &corrected_value, sizeof(ig_u16));
						}
					}





				}
				else if (m == InOp::I16_JUMP || 
						 m == InOp::I8_JUMP) {
					//D//printf("\tjump %lld\n", data);

					ig_u16 jmp = (ig_u16) data;
					if (m == InOp::I8_JUMP) {
						jmp = (ig_u8)data;
					}
					//if (jmp == -1) {
					//	printf("Huh wtf infinite loop?\n");
					//	dieAPainfullDeath("Jump would cause infinite loop");
					//}

					// TODO verify that all jumps are within a valid range
					// these are relative jump indicies


					if (PROCESS_MODE_VALIDATION == mode)
					{
						// verify that the jump is within the current table
						if (jmp < 0 || jmp >= jump_table_length) 
						{
							fprintf(stderr, "Invalid Jump Table Position: %d expected [%d..%d) \n", jmp, 0, jump_table_length );
							bad_assembly = true;
						}
						else
						{
							JumpTableEntry * ent = &jump_table[jmp];
						
							// promote short range jumps to a more compact format
							if (op == InOp::JMP && m == InOp::I16_JUMP)
							{
								int anticipated_jump = ent->m_offset - (read_head + 1);
								if (-128 <= anticipated_jump && anticipated_jump < 128 && jmp < 255) 
								{
									// this needs to be this way in order to obey termination error checks
									code[read_head - 1] = InOp::NOP;
									code[read_head + 0] = InOp::INTERNAL_JMP_LO;
									code[read_head + 1] = data;


									//printf("replacing with jump lo: %d\n", data);

									// reprocess this generated code
									read_head    = j0;
									stack_height = curr_stack_height;
									goto REPROCESS_OP;
								}
							}
						
							// do standard jump processing
							if (ent -> m_ref_stack_height == 0x0000) {
								ent -> m_ref_stack_height = 0xffff;
								ent -> m_stack_height = stack_height;
							}
							else if (ent -> m_stack_height != stack_height) {
								ig_u32 p[2];
								this->_explodeConstantPair(func.m_pair, p);
								fprintf(stderr, "\t'%s' '%s'\n", _getStringForId(p[0]), _getStringForId(p[1]));
								fprintf(stderr, "2. IGVM conflicting stack heights found at jump target %d vs %d\n", ent->m_stack_height, stack_height);
							
								bad_assembly = true;
							}
						}
						
						
					}		
					/*		
					else if (PROCESS_MODE_LINKING == mode)
					{
						// convert table indicies into relative offsets
						ig_i16 corrected_value = jump_table[jmp].m_offset - (read_head);
						memcpy(&func.m_code[read_head], &corrected_value, sizeof(ig_u16));
					}
					*/
				}
				else
				{
					//D//printf("\tbytes: %d = %lld\n", bytes_to_read, data);
				}


				read_head += bytes_to_read;


				modifiers >>= 8;
			}
			
			if (op == InOp:: NEXT_IT) {
				stack_height ++;
			}
			if (op == InOp:: AND || op == InOp:: OR) {
				stack_height --;
			}

			read_head--;
			prev_op = op;
		}

		
		if (PROCESS_MODE_LINKING == mode)
		{
			/*
			// 131.5k of RAM used so far for compiled bytecode
			//NOT needed quite yet.
			//	TODO:  create 8bit jump maybe?  DONE
			//	TODO:  create succinct way of invoking common function syntaxes
			//  TODO:  fix jump tables
			//  TODO: fix exception tables
			{
				int wasted = 0;
				int total = 0;
			
				ig_u32 p[2];
				this->_explodeConstantPair(func.m_pair, p);
				printf("FN. '%s' '%s'\n", _getStringForId(p[0]), _getStringForId(p[1]));
			
				for (int read_head = 0; read_head < code_length; ) 
				{
					int op = func.m_code[read_head];
					read_head += s_in_ops_sz[op];
					
					if (op == InOp::NOP_TERM) {
						continue;
					}
			
					
					total += s_in_ops_sz[op];
					if (op == InOp::NOP) {
						wasted ++;
					}
				}
			
				s_trace_count += total;
				printf("\tnop waste: %d of %d (running-total: %d)\n", wasted, total, s_trace_count);
			}
			*/
			
			// compact all of the NOPS we may have introduced to the code stream
			// this will compact all of the tables passed to it
			FunctionTableCompactor compactor(func.m_exception_table, func.m_header.m_exception_count,
											 func.m_debug_table, 	 func.m_header.m_debug_count,
											 func.m_jump_table,      func.m_header.m_jump_table_length);
			
			// fix up the jump tables
			int write_head = 0;
			int read_head = 0;
			while (read_head < code_length) 
			{
				int op = func.m_code[read_head];
				
				if (op == InOp::NOP) {
					compactor.skip(1);
					read_head ++;
					continue;
				}
				
				int op_length = 1;
				int modifiers = s_in_ops[op];
				

				while (modifiers > 0) 
				{
					int m             = modifiers & 0xff;
					int bytes_to_read = m & 0x0f;  
					
					op_length += bytes_to_read;
					modifiers >>= 8;
				}

				memcpy(&func.m_code[write_head], &func.m_code[read_head], op_length);
				read_head  += op_length;
				write_head += op_length;
								
				compactor.inc(op_length);
			}
			
			while (write_head < read_head) {
				func.m_code[write_head] = InOp::NOP;
				write_head++;
			}	
				
			// fix up the jump tables
			for (int read_head = 0; read_head < code_length; ) 
			{
				int op = func.m_code[read_head]; read_head ++;
				int modifiers = s_in_ops[op];
				

				while (modifiers > 0) 
				{
					int m             = modifiers & 0xff;
					int bytes_to_read = m & 0x0f;  
					
					if (m == InOp::I16_JUMP) 
					{
						ig_i64 data = 0;
						memcpy(&data, &func.m_code[read_head], bytes_to_read);
					
						ig_u16 jmp = (ig_u16) data;
						ig_i16 corrected_value = func.m_jump_table[jmp].m_offset - (read_head);
						memcpy(&func.m_code[read_head], &corrected_value, sizeof(ig_i16));
					}
					else if (m == InOp::I8_JUMP)
					{
						ig_i64 data = 0;
						memcpy(&data, &func.m_code[read_head], bytes_to_read);
					
						ig_u8 jmp = (ig_u8) data;
						ig_i8 corrected_value = func.m_jump_table[jmp].m_offset - (read_head);
						memcpy(&func.m_code[read_head], &corrected_value, sizeof(ig_i8));
					}
					
					
					read_head += bytes_to_read;
					modifiers >>= 8;
				}
			}
			
			////////
			// Debugging
			
#if 0			
			{
				ig_u32 p[2];
				this->_explodeConstantPair(func.m_pair, p);
				
				if (strcmp(_getStringForId(p[0]), "hype.ui.UIWidget") == 0 &&
					strcmp(_getStringForId(p[1]), "render") == 0) 
				{
			
					for (int j = 0; j < code_length; j++) 
					{
						int op = func.m_code[j];
						int modifiers = s_in_ops[op];


						fprintf(stderr, "\t%d %s ", j, s_in_opm_string[op]);

						j++;
				
						while (modifiers > 0) 
						{

							int m = modifiers & 0xff;
							int bytes_to_read = m & 0x0f;  

							// this abuses the fact that the system is little endian
							ig_i64 data = 0;
							memcpy(&data, &func.m_code[j], bytes_to_read);

							if (m == InOp::I16_JUMP) {
						
								fprintf(stderr, " <jump:%d> ", (ig_i16)data);
							}
							else if (m == InOp::I8_JUMP) {
								fprintf(stderr, " <jump-lo:%d> ", (ig_i8)data);
							}
							else if (m == InOp::I16_LOCAL) {
								fprintf(stderr, " <local:%d> ", (int)data);
							}
							else if (m == InOp::I8_LOCAL) {
								fprintf(stderr, " <local8:%d> ", (int)data);
							}
							else if (m == InOp::I16_PAIR) {
								this->_explodeConstantPair((int)data, p);
								fprintf(stderr, " <%x %s:%s> " , (int)data, _getStringForId(p[0]), _getStringForId(p[1]));
							}
							else {
								fprintf(stderr, " <?:%d> ", (int)data);
							}

							j += bytes_to_read;

							modifiers >>= 8;
						}
				
						fprintf(stderr, "\n");

						j--;
					}
				}
			}
#endif			
		}
		else if (PROCESS_MODE_VALIDATION == mode)
		{
			func.m_max_stack_size = stack_height_max * 8 + func.getLocalCount() * 8;
		
		
			if (stack_height != 0) {
				bad_assembly = true;
			}
		
			if (bad_assembly) 
			{
				if (stack_height != 0) {
					fprintf(stderr, "final stack height != 0. failed to rudamentarily calculate stack height\n");
				}

				ig_u32 p[2];
				this->_explodeConstantPair(func.m_pair, p);

				// config should likely have an error function... this'd clean up a lot of this trash
				fprintf(stderr, "\t'%s' '%s'\n", _getStringForId(p[0]), _getStringForId(p[1]));
				
				fprintf(stderr, "stack_height: %d\n", stack_height);
				fprintf(stderr, "stack_height_max: %d\n", stack_height_max);
				
				fprintf(stderr, "CODE-DUMP:\n");
				
				
				for (int j = 0; j < code_length; j++) 
				{
					int op = code[j];
					int modifiers = s_in_ops[op];


					fprintf(stderr, "\t%d %s ", j, s_in_opm_string[op]);

					j++;
					
					while (modifiers > 0) 
					{

						int m = modifiers & 0xff;
						int bytes_to_read = m & 0x0f;  

						// this abuses the fact that the system is little endian
						ig_i64 data = 0;
						memcpy(&data, &code[j], bytes_to_read);

						if (m == InOp::I16_JUMP) {
							
							fprintf(stderr, " <jump:%d> ", jump_table[data].m_offset);
						}
						else if (m == InOp::I8_JUMP) {
							fprintf(stderr, " <jump-lo:%d> ", jump_table[data].m_offset);
						}
						else if (m == InOp::I16_LOCAL) {
							fprintf(stderr, " <local:%d> ", (int)data);
						}
						else if (m == InOp::I8_LOCAL) {
							fprintf(stderr, " <local8:%d> ", (int)data);
						}
						else {
							fprintf(stderr, " <?:%d> ", (int)data);
						}

						j += bytes_to_read;

						modifiers >>= 8;
					}
					
					fprintf(stderr, "\n");

					j--;
				}
			}
		
			// this wont be complete until the compiler can spit out how many values a fn returns

		}
	}

	return 0;
}



///////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////
// MAIN VM LOOP
///////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////

//define IGVM_NULL_CHECK(obj) { if (!obj) { goto NULL_POINTER_ERROR; }  }

// seems to be more performant
#define IGVM_NULL_CHECK(obj) { if (IGVM_UNLIKELY(!obj)) { goto NULL_POINTER_ERROR; }  }

// macros to read various sized chunks of data from the program
// these do generally produce optimized code
#define READ_I8(x)	{ memcpy(&x, pc, sizeof(ig_i8));  pc += 1; }
#define READ_I16(x)	{ memcpy(&x, pc, sizeof(ig_i16)); pc += 2; }
#define READ_U16(x)	{ memcpy(&x, pc, sizeof(ig_u16)); pc += 2; }
#define READ_I32(x)	{ memcpy(&x, pc, sizeof(ig_i32)); pc += 4; }
#define READ_I64(x)	{ memcpy(&x, pc, sizeof(ig_i64)); pc += 8; }



// code to allow us to revert back to using a switch if desired
#define GOTO_IGVM_SWITCH_TOP		goto IGVM_SWITCH_TOP

#if IGVM_USE_SWITCH == 0
#define IGVM_SWITCH_CASE(x)			L_IGVM_ ## x
#define IGVM_SWITCH_DEFAULT			L_IGVM_DEFAULT
#else
#define IGVM_SWITCH_CASE(x)			case InOp::x
#define IGVM_SWITCH_DEFAULT			default
#endif	

#define TRACK_INFINITE_LOOPS  0

int VMImp::invoke(Stack * stack, Function * fn, ig_i8 argc, const Box * args, Box * ret)
{
	//bool printed_allocation = false;
	Box default_ret;
	
	if (ret == NULL) {
		ret = &default_ret;
	}
	
	#if TRACK_INFINITE_LOOPS == 1
	time_t start_time = time(NULL);
	#endif
	//ig_i16   j16;
	//
	
#if IGVM_DEBUG == 1
	int indent = 0;
#endif
	StackFrame * frame = NULL;

	// this should really never be a pointer
	Function * curr_fn = NULL;
	ig_u8 * pc = NULL;		// the program counter
	Box * bp = NULL;		// the base pointer (all local variables are relative to this)
	Box * sp = NULL;		// the stack pointer (always points to the top most element)
							// if there are no elements on the stack it points to the address before
	Box * block_sp = NULL;
	
	//int   line_no = 0;
	bool destroy_stack = false;

	size_t stack_min = 0;
	size_t stack_max = 0;

	// the case where we're resuming a frame of executing
	if (NULL == fn)
	{
		bp = stack->m_frame->m_bp;
		sp = stack->m_frame->m_sp;
		pc = stack->m_frame->m_pc;
		curr_fn = stack->m_frame->m_function;
		frame = stack->m_frame;
		stack->m_current_sp = sp;

		stack_min = (size_t) stack->m_stack;
		stack_max = stack_min + Stack::STACK_SIZE_IN_BYTES;
	}
	else
	{
		if (stack == NULL) 
		{
			destroy_stack = true;
			
			// allocate a new stack and set up the current stack pointer
			stack = allocStack();
			stack->m_current_sp = stack->m_stack - 1;
			
			memcpy(stack->m_stack, args, argc * sizeof(Box));
			bp      = NULL;
			sp      = stack->m_current_sp;	// points before the first argument
			pc      = NULL;
			curr_fn = NULL;
		}

		stack_min = (size_t) stack->m_stack;
		
		// added a little bit of padding at the top
		stack_max = stack_min + Stack::STACK_SIZE_IN_BYTES - (sizeof(ig_i64) * 16);

///////////////////////////////////////////////////////////////////
// goto target for Invoking a function
// it is the callers responsibility to set
//   fn   - the function to be executed
//   argc - the number of arguments that exist on the stack (this includes 'this' handles)
////////////////////////////////////////////////////////////////////
INVOKE_FUNCTION:


#if IGVM_DEBUG == 1
		for (int ii = 0; ii < indent; ii++) { //D//printf("\t"); } //D//printf("INVOKE_FUNCTION argc: %d ", argc);
			indent++;
			constantPairDebug(fn->m_pair);
		}
#endif

#if IGVM_TRACK_FUNCTION_OPS == 1
		fn->m_called_invocations += m_debug_config;
#endif


// testing native invocation
#if NATIVE_PATH_2 == 0
		// ideally I'd like to get rid of this if statement altogether
		if (NULL != fn->m_native)
		{
			Box result;

			// fill out all required default values
			int lc = fn->getLocalCount();
			memcpy(&sp[1 + argc], &fn->m_local_defaults[argc],  (lc - argc) * sizeof(Box));


			int native_ec = 0;

			Box  error_message;
			bool error_message_exists = false;

			this->_beginScope();
			try
			{
				// native code can return an error code, or throw an error message
				// THROW_IGVM_NATIVE_ERROR("NULL Ptr found");
				// should probably do away with all error codes
				
				stack->m_current_sp = &sp[1 + argc];
				native_ec = fn->m_native(this, &sp[1], argc, &result);
			}
			catch (NativeError ex) 
			{
				native_ec = 1;
				if (0 == this->_stringFromUTF8Bytes(ex.message, &error_message)) 
				{
					error_message_exists = true;
				}
			}
			this->_endScope();

			// if native code failed
			// construct a fake error and attempt to execute it
			if (0 != native_ec)
			{

				fprintf(stderr, "NATIVE EC: %d\n", native_ec);
				int cp = this->_constantPair(this->_constantString("iglang.Error", strlen("iglang.Error"), false),
											 this->_constantString("__ig_nativeError", strlen("__ig_nativeError"), false), false);

				fn = (Function *)m_pairs[cp];
				if (fn == 0) {
					/////////////////////
					// Horrible horrible failure
					int * die = 0;
					*die = 666;
				}

				argc = 0;
				if (error_message_exists) {
					sp[1] = error_message;
					argc = 1;
				}


				goto INVOKE_FUNCTION;
			}

			if (fn->m_header.m_returns) {
				sp++;
				*sp = result;
			}
		}
		else
#endif			
		{
			StackFrame * prev_frame = frame;

			// check if a potential stack overflow COULD occur
			// this needs to take into account the size of the frame as well
			if (IGVM_UNLIKELY(((size_t)sp) + fn->m_max_stack_size >= stack_max)) 
			{
				fprintf(stderr, "Stack Overflow\n");
				
				
				char data[MAX_STACK_TRACE];
				snprintf(data, MAX_STACK_TRACE, "\n");
				this->_stackTrace(frame, bp, data, MAX_STACK_TRACE);
				fprintf(stderr, "igvm-stack-trace\n%s", data);
				
				// !!! bad
				return errorOccurred(0xED);
			}

			frame = (StackFrame *) &sp[1 + fn->m_local_count];
			frame->m_prev     = prev_frame;
			frame->m_pc       = pc;  // on frame exit restore these values
			frame->m_bp       = bp;  // on frame exit restore these values
			frame->m_sp       = sp;  // on frame exit restore these values
			frame->m_function = curr_fn;
			
			// ..(sp) a0     a1 a2
			// ..     a0(bp) a1 a2 l0 l1 .. ln STACK-FRAME(sp)

			// adjust our current base pointer
			bp      = &sp[1]; // will this actually be correct? and aligned to 64 bit?
			sp      = (Box *)&frame[1];	//&bp[fn->m_local_count];	//getLocalCount()];
			pc      = fn->m_code;
			curr_fn = fn;

			// but wouldn't this leave all variables in an indeterminant state?
			// yes but do I actually care
			const int param_count_diff = fn->m_param_count - argc;
			if (0 != param_count_diff) {
				memcpy(&bp[argc], &fn->m_local_defaults[argc],  (param_count_diff) * sizeof(Box));
			}
		}
	}

	ig_i8  tmp8;
	ig_i8  tmp8b;
	ig_u8  tmpu8;
	ig_i16 tmp16;
	//ig_u16 tmpU16;
	ig_i32 tmp32;
	ig_i64 tmp64;

#if IGVM_USE_SWITCH == 0
	static void * jump_table[256] = {
&&L_IGVM_NOP,
&&L_IGVM_POP,
&&L_IGVM_DUP,
&&L_IGVM_DUP1X,
&&L_IGVM_DUP2X,
&&L_IGVM_RET0,
&&L_IGVM_RET1,
&&L_IGVM_DUP2,
&&L_IGVM_ADD_I32,
&&L_IGVM_SUB_I32,
&&L_IGVM_MUL_I32,
&&L_IGVM_DIV_I32,
&&L_IGVM_MOD_I32,
&&L_IGVM_ADD_F64,
&&L_IGVM_SUB_F64,
&&L_IGVM_MUL_F64,
&&L_IGVM_DIV_F64,
&&L_IGVM_MOD_F64,
&&L_IGVM_AND_I32,
&&L_IGVM_OR_I32,
&&L_IGVM_XOR_I32,
&&L_IGVM_ASR_I32,
&&L_IGVM_LSR_I32,
&&L_IGVM_LSL_I32,
&&L_IGVM_CMP_EQ_I32,
&&L_IGVM_CMP_LT_I32,
&&L_IGVM_CMP_LE_I32,
&&L_IGVM_CMP_EQ_F64,
&&L_IGVM_CMP_LT_F64,
&&L_IGVM_CMP_LE_F64,
&&L_IGVM_CMP_EQ_I64,
&&L_IGVM_BIT_NOT_I32,
&&L_IGVM_NOT,
&&L_IGVM_I32_TO_F64,
&&L_IGVM_F64_TO_I32,
&&L_IGVM_PUSH_64,
&&L_IGVM_PUSH_32,
&&L_IGVM_LOCAL_LOAD,
&&L_IGVM_LOCAL_STORE,
&&L_IGVM_MEMBER_LOAD,
&&L_IGVM_MEMBER_STORE,
&&L_IGVM_STATIC_LOAD,
&&L_IGVM_STATIC_STORE,
&&L_IGVM_THIS_LOAD,
&&L_IGVM_THIS_STORE,
&&L_IGVM_LOCAL_LOAD_2,
&&L_IGVM_LOCAL_CLEAR,
&&L_IGVM_PUSH_STRING,
&&L_IGVM_LOCAL_STORE_KEEP,
&&L_IGVM_CALL_INTERFACE,
&&L_IGVM_FUNCPTR_MEMBER,
&&L_IGVM_FUNCPTR_STATIC,
&&L_IGVM_CALL_VTABLE,
&&L_IGVM_CALL_STATIC,
&&L_IGVM_CALL_DYNAMIC,
&&L_IGVM_NEW,
&&L_IGVM_THROW,
&&L_IGVM_JMP,
&&L_IGVM_J0_I32,
&&L_IGVM_JN0_I32,
&&L_IGVM_CAST_TO,
&&L_IGVM_NEXT_IT,
&&L_IGVM_CALL_STATIC_VTABLE,
&&L_IGVM_RET_FUNCPTR,
&&L_IGVM_RET_OBJ,
&&L_IGVM_I_MEMBER_STORE_FUNCPTR,
&&L_IGVM_I_MEMBER_STORE_OBJ,
&&L_IGVM_I_THIS_STORE_FUNCPTR,
&&L_IGVM_I_THIS_STORE_OBJ,
&&L_IGVM_I_STATIC_STORE_FUNCPTR,
&&L_IGVM_I_STATIC_STORE_OBJ,
&&L_IGVM_STATIC_LOAD_FUNCPTR,
&&L_IGVM_STATIC_LOAD_OBJ,
&&L_IGVM_ARRAY_LOAD_8,
&&L_IGVM_ARRAY_LOAD_16,
&&L_IGVM_ARRAY_LOAD_32,
&&L_IGVM_ARRAY_LOAD_64,
&&L_IGVM_ARRAY_LOAD_OBJ,
&&L_IGVM_ARRAY_LOAD_FUNCPTR,
&&L_IGVM_ARRAY_STORE_8,
&&L_IGVM_ARRAY_STORE_16,
&&L_IGVM_ARRAY_STORE_32,
&&L_IGVM_ARRAY_STORE_64,
&&L_IGVM_ARRAY_STORE_OBJ,
&&L_IGVM_ARRAY_STORE_FUNCPTR,
&&L_IGVM_ARRAY_NEW,
&&L_IGVM_ARRAY_LENGTH,
&&L_IGVM_INSTANCE_OF,
&&L_IGVM_STACK_TRACE,
&&L_IGVM_LOCAL_INC_I32,
&&L_IGVM_STACK_INC_I32,
&&L_IGVM_CALL_RET1_INTERFACE,
&&L_IGVM_CALL_RET1_VTABLE,
&&L_IGVM_CALL_RET1_STATIC,
&&L_IGVM_CALL_RET1_STATIC_VTABLE,
&&L_IGVM_CALL_RET1_DYNAMIC,
&&L_IGVM_PUSH_I8,
&&L_IGVM_CATCH_ENTRY,
&&L_IGVM_ARRAY_RESIZE,
&&L_IGVM_ARRAY_FILL,
&&L_IGVM_LL0,
&&L_IGVM_LL1,
&&L_IGVM_LL2,
&&L_IGVM_LL3,
&&L_IGVM_LL4,
&&L_IGVM_JEQ_I32,
&&L_IGVM_ASR_I32_CONST,
&&L_IGVM_LSR_I32_CONST,
&&L_IGVM_LSL_I32_CONST,		
&&L_IGVM_JMP_IN_I32_0RANGE,
&&L_IGVM_AND,
&&L_IGVM_OR,
&&L_IGVM_THIS_STORE_32_REF,
&&L_IGVM_MEMBER_STORE_32_REF,
&&L_IGVM_ARRAY_STORE_32_REF,
&&L_IGVM_STATIC_STORE_32_REF,
&&L_IGVM_THIS_LOAD_32,
&&L_IGVM_MEMBER_LOAD_32,
&&L_IGVM_THIS_STORE_32,
&&L_IGVM_MEMBER_STORE_32,
&&L_IGVM_MLA_I32,
&&L_IGVM_MLA_F64,
&&L_IGVM_LSL_ASR_I32_CONST,
&&L_IGVM_SWITCH_I32_JMP,
&&L_IGVM_I32_0,
&&L_IGVM_I32_1,
&&L_IGVM_ARRAY_COPY,
&&L_IGVM_DEBUG_BLOCK_START,
&&L_IGVM_DEBUG_BLOCK_END,
&&L_IGVM_PUSH_I16,
&&L_IGVM_LOCAL_LOAD_LO,
&&L_IGVM_LOCAL_STORE_LO,
&&L_IGVM_PUSH_F64_0,
&&L_IGVM_PUSH_F64_1,
&&L_IGVM_THIS_INC_I32,
&&L_IGVM_LOAD_OBJ_FIELD,
&&L_IGVM_LOAD_OBJ_FIELD_32,
&&L_IGVM_DEFAULT,
&&L_IGVM_DEFAULT,
&&L_IGVM_DEFAULT,
&&L_IGVM_DEFAULT,
&&L_IGVM_DEFAULT,
&&L_IGVM_DEFAULT,
&&L_IGVM_DEFAULT,
&&L_IGVM_DEFAULT,
&&L_IGVM_DEFAULT,
&&L_IGVM_DEFAULT,
&&L_IGVM_DEFAULT,
&&L_IGVM_DEFAULT,
&&L_IGVM_DEFAULT,
&&L_IGVM_DEFAULT,
&&L_IGVM_DEFAULT,
&&L_IGVM_DEFAULT,
&&L_IGVM_DEFAULT,
&&L_IGVM_DEFAULT,
&&L_IGVM_DEFAULT,
&&L_IGVM_DEFAULT,
&&L_IGVM_DEFAULT,
&&L_IGVM_DEFAULT,
&&L_IGVM_DEFAULT,
&&L_IGVM_DEFAULT,
&&L_IGVM_DEFAULT,
&&L_IGVM_DEFAULT,
&&L_IGVM_DEFAULT,
&&L_IGVM_DEFAULT,
&&L_IGVM_DEFAULT,
&&L_IGVM_DEFAULT,
&&L_IGVM_DEFAULT,
&&L_IGVM_DEFAULT,
&&L_IGVM_DEFAULT,
&&L_IGVM_DEFAULT,
&&L_IGVM_DEFAULT,
&&L_IGVM_DEFAULT,
&&L_IGVM_DEFAULT,
&&L_IGVM_DEFAULT,
&&L_IGVM_DEFAULT,
&&L_IGVM_DEFAULT,
&&L_IGVM_DEFAULT,
&&L_IGVM_DEFAULT,
&&L_IGVM_DEFAULT,
&&L_IGVM_DEFAULT,
&&L_IGVM_DEFAULT,
&&L_IGVM_DEFAULT,
&&L_IGVM_DEFAULT,
&&L_IGVM_DEFAULT,
&&L_IGVM_DEFAULT,
&&L_IGVM_DEFAULT,
&&L_IGVM_DEFAULT,
&&L_IGVM_DEFAULT,
&&L_IGVM_DEFAULT,
&&L_IGVM_DEFAULT,
&&L_IGVM_DEFAULT,
&&L_IGVM_DEFAULT,
&&L_IGVM_DEFAULT,
&&L_IGVM_DEFAULT,
&&L_IGVM_DEFAULT,
&&L_IGVM_DEFAULT,
&&L_IGVM_DEFAULT,
&&L_IGVM_DEFAULT,
&&L_IGVM_DEFAULT,
&&L_IGVM_DEFAULT,
&&L_IGVM_DEFAULT,
&&L_IGVM_DEFAULT,
&&L_IGVM_DEFAULT,
&&L_IGVM_I_LS0,  //200
&&L_IGVM_I_LS1,
&&L_IGVM_I_LS2,
&&L_IGVM_I_LS3,
&&L_IGVM_I_LS4,
&&L_IGVM_I_LS5,
&&L_IGVM_I_LS6,
&&L_IGVM_I_LS7,
&&L_IGVM_DEFAULT,
&&L_IGVM_DEFAULT,
&&L_IGVM_DEFAULT,
&&L_IGVM_DEFAULT,
&&L_IGVM_DEFAULT,
&&L_IGVM_DEFAULT,
&&L_IGVM_DEFAULT,
&&L_IGVM_DEFAULT,
&&L_IGVM_DEFAULT,
&&L_IGVM_DEFAULT,
&&L_IGVM_DEFAULT,
&&L_IGVM_DEFAULT,
&&L_IGVM_DEFAULT,
&&L_IGVM_DEFAULT,
&&L_IGVM_DEFAULT,
&&L_IGVM_DEFAULT,
&&L_IGVM_DEFAULT,
&&L_IGVM_DEFAULT,
&&L_IGVM_DEFAULT,
&&L_IGVM_DEFAULT,
&&L_IGVM_DEFAULT,
&&L_IGVM_DEFAULT,
&&L_IGVM_DEFAULT,
&&L_IGVM_DEFAULT,
&&L_IGVM_DEFAULT,
&&L_IGVM_DEFAULT,
&&L_IGVM_DEFAULT,
&&L_IGVM_DEFAULT,
&&L_IGVM_DEFAULT,
&&L_IGVM_DEFAULT,
&&L_IGVM_INTERNAL_VECTOR_32_GET,
&&L_IGVM_INTERNAL_VECTOR_OBJECT_GET,
&&L_IGVM_INTERNAL_VECTOR_LENGTH,
&&L_IGVM_INTERNAL_JMP_LO,
&&L_IGVM_INTERNAL_THIS_LOAD_LO,
&&L_IGVM_INTERNAL_THIS_LOAD_32_LO,
&&L_IGVM_INTERNAL_MEMBER_LOAD_LO,
&&L_IGVM_INTERNAL_MEMBER_LOAD_32_LO,
&&L_IGVM_INTERNAL_LOCAL_CLEAR_LO,
&&L_IGVM_INVOKE_NATIVE,			// this might be incorrect?
&&L_IGVM_DEFAULT,
&&L_IGVM_DEFAULT,
&&L_IGVM_DEFAULT,
&&L_IGVM_DEFAULT};
#endif








	for (;;)
	//while(true)
	{
	
	
// __asm__  volatile( "#TRISTAN ABOVE PRIMARY LOOP\n" );		
	
IGVM_SWITCH_TOP:		
	
	/*
		//unsigned char op = *pc;
		//pc ++;

#if IGVM_DEBUG_RUNTIME == 1
		for (int ii = 0; ii < indent; ii++) { printf("\t"); }
		printf("%p sp:%p %s\n", pc, sp, s_in_opm_string[op]);
#endif
	*/



	/*

		
#if TRACK_INFINITE_LOOPS == 1
		if (time(NULL) - start_time > 5) 
		{
			printf("Code timed out after 5 seconds\n");
			printf("##############################\n");
			
			const int MAX_STACK_TRACE = 2048*4;

			char data[MAX_STACK_TRACE];
			snprintf(data, MAX_STACK_TRACE, "Error: \n");


			this->_stackTrace(frame, bp, data, MAX_STACK_TRACE);
		 

			printf("%s\n", data);
			return errorOccurred(0xED);		
		}
#endif
	*/

///////////////////////////////
// Jump table based implementation
#if IGVM_USE_SWITCH == 0
		void * dst = jump_table[*pc];
		pc++;
	
#if IGVM_TRACK_FUNCTION_OPS == 1
		curr_fn->m_called_ops += m_debug_config;
#endif		
	
		goto *dst;
//////////////////////////////
// Switch based implementation		
#else	

#if IGVM_TRACK_FUNCTION_OPS == 1
		curr_fn->m_called_ops += m_debug_config;
#endif

		switch (*pc++)
#endif
		{
			 // do absolutely nothing
			 IGVM_SWITCH_CASE(NOP):    { GOTO_IGVM_SWITCH_TOP; }
							 
			 // pop an element off of the stack
			 // .. A B
			 // .. A
			 IGVM_SWITCH_CASE(POP):    {  sp--;  GOTO_IGVM_SWITCH_TOP; }

			 // another nop for the most part
			 IGVM_SWITCH_CASE(CATCH_ENTRY): { GOTO_IGVM_SWITCH_TOP; }
			 
			 // duplicate the top element
			 // .. A   => .. A A
			 IGVM_SWITCH_CASE(DUP):    { sp[1] = sp[0]; sp++; GOTO_IGVM_SWITCH_TOP; }

			 // duplicate the top 2 elements on the stack
			 // .. A B => .. A B A B
			 IGVM_SWITCH_CASE(DUP2) : { sp--; sp[2] = sp[0]; sp[3] = sp[1];  sp +=3; GOTO_IGVM_SWITCH_TOP; }

			 // take the topmost value and make a copy, shifting it back 1 place
			 //  .. A B  => .. B A B
			 IGVM_SWITCH_CASE(DUP1X): {
				Box top = *sp;

				sp --;
				sp[2] = top;
				sp[1] = sp[0];
				sp[0] = top;
				sp += 2;

				GOTO_IGVM_SWITCH_TOP;
			 }

			 // take the topmost value and make a copy, shifting it back 2 places
			 //  .. A B C  => .. C A B C
			 IGVM_SWITCH_CASE(DUP2X): {
				Box top = *sp;

				sp -= 2;
				sp[3] = top;
				sp[2] = sp[1];
				sp[1] = sp[0];
				sp[0] = top;
				sp += 3;

				GOTO_IGVM_SWITCH_TOP;
			 }

			 // return to previous stack frame
			 IGVM_SWITCH_CASE(RET0): 
			 {
				pc      = frame->m_pc;             // restore program-counter
				sp      = frame->m_sp;             // restore stack-pointer
				bp      = frame->m_bp;             // restore base-pointer
				curr_fn = frame->m_function;  // restore the current function

				frame = frame->m_prev;


				// if there is no "current" frame
				// then we've ended our execution unit
				if (IGVM_UNLIKELY(!frame)) {
					if (ret != NULL) {
						ret->i64 = 0;
					}

					//printf("pre dealloc count: %d (destroy: %d) \n", countActiveStacks(), destroy_stack);
					if (destroy_stack) {
						deallocStack(stack);
					}
					
					//printf("post dealloc count: %d\n", countActiveStacks());
					return 0;
				}

#if IGVM_DEBUG_RUNTIME == 1
				indent --;
#endif

				GOTO_IGVM_SWITCH_TOP;
			 }

			 // return a single value to the previous stack frame
			 // why is it even necessary to disambiguate these here?
			 // could just look at the function syntax or something for the termination case
			 IGVM_SWITCH_CASE(RET_FUNCPTR):
			 IGVM_SWITCH_CASE(RET_OBJ):
			 IGVM_SWITCH_CASE(RET1):
			 {				 
				ig_u8 op = pc[-1];
				
				Box top = *sp;
				pc      = frame->m_pc;	      // restore program-counter
				sp      = frame->m_sp;	      // restore stack-pointer
				bp      = frame->m_bp;	      // restore base-pointer
				curr_fn = frame->m_function;  // restore the current function
				
				// push the returned value onto the stack
				sp++;
				*sp = top;


				frame = frame->m_prev;


				// if any object is returned to the user
				// then mark it as grey so that it will not be
				// collected until the next cycle
				if (IGVM_UNLIKELY(!frame)) 
				{
					//printf("RET1 pre dealloc count: %d (destroy: %d) \n", countActiveStacks(), destroy_stack);
					
					if (ret != NULL)
					{
						if (op == InOp::RET_FUNCPTR && top.u32 != 0) {
							//m_heap.whiteToGrey(top.u32);			// ?? is this needed
							this->_pushObjectToScope(top.u32);
						}
						else if (op == InOp::RET_OBJ && top.u32 != 0) {
							//m_heap.whiteToGrey(top.u32);   		// ?? not needed
							this->_pushObjectToScope(top.u32);
						}
						// what NOW?
						*ret = top;
					}
					
					if (destroy_stack) {
						deallocStack(stack);
					}
					//printf("RET1 post dealloc count: %d\n", countActiveStacks());
					return 0;
				}

#if IGVM_DEBUG_RUNTIME == 1
				indent--;
#endif

				////D//printf("RET1 pc: %p sp: %p bp: %p top: %d\n", pc, sp, bp, sp->u32);

				GOTO_IGVM_SWITCH_TOP;
			 }


			 IGVM_SWITCH_CASE(ADD_I32) : { sp--;   sp[0].i32 += sp[1].i32;  GOTO_IGVM_SWITCH_TOP; }
			 IGVM_SWITCH_CASE(SUB_I32):  { sp--;   sp[0].i32 -= sp[1].i32;  GOTO_IGVM_SWITCH_TOP; }
			 IGVM_SWITCH_CASE(MUL_I32) : { sp--;   sp[0].i32 *= sp[1].i32;  GOTO_IGVM_SWITCH_TOP; }
			 IGVM_SWITCH_CASE(MLA_I32) : { sp-=2;   sp[0].i32 += sp[1].i32 * sp[2].i32;  GOTO_IGVM_SWITCH_TOP; }
			 IGVM_SWITCH_CASE(DIV_I32) : { sp--;   
						if (IGVM_UNLIKELY(sp[1].i32 == 0)) {
							goto DIVIDE_BY_ZERO_ERROR;
						}
						sp[0].i32 /= sp[1].i32;  
						GOTO_IGVM_SWITCH_TOP; }
						
			 IGVM_SWITCH_CASE(MOD_I32):  { sp--;   
						if (IGVM_UNLIKELY(sp[1].i32 == 0)) {
							goto DIVIDE_BY_ZERO_ERROR;
						}
						sp[0].i32 %= sp[1].i32;  GOTO_IGVM_SWITCH_TOP; }


			 IGVM_SWITCH_CASE(ASR_I32_CONST): {
				READ_I8(tmp8);
				sp[0].i32 = sp[0].i32 >> tmp8;
				GOTO_IGVM_SWITCH_TOP;
			 }

			IGVM_SWITCH_CASE(LSR_I32_CONST): {
				READ_I8(tmp8);
				sp[0].u32 = sp[0].u32 >> tmp8;
				GOTO_IGVM_SWITCH_TOP;
			 }
			 
			 IGVM_SWITCH_CASE(LSL_I32_CONST): {
				READ_I8(tmp8);
				sp[0].i32 = sp[0].i32 << tmp8;
				GOTO_IGVM_SWITCH_TOP;
			 }
			 
			 IGVM_SWITCH_CASE(LSL_ASR_I32_CONST): {
				READ_I8(tmp8);
				READ_I8(tmp8b);
				
				sp[0].i32 = (sp[0].i32 << tmp8) >> tmp8b;
				GOTO_IGVM_SWITCH_TOP;
			 }

			 IGVM_SWITCH_CASE(LOCAL_INC_I32): {
				READ_I8(tmpu8);
				READ_I8(tmp8);
				
				//printf("local-inc: %d\n", tmp8);
				bp[tmpu8].i32 += (int)tmp8;
				GOTO_IGVM_SWITCH_TOP;
			 }

			// I don't think this opcode is currently in use
			 IGVM_SWITCH_CASE(STACK_INC_I32): {
				READ_I8(tmp8)
				sp[0].i32 += (int)tmp8;
				GOTO_IGVM_SWITCH_TOP;
			 }

			 IGVM_SWITCH_CASE(ADD_F64) : { sp--;   sp[0].f64 += sp[1].f64; GOTO_IGVM_SWITCH_TOP; }
			 IGVM_SWITCH_CASE(SUB_F64):  { sp--;   sp[0].f64 -= sp[1].f64; GOTO_IGVM_SWITCH_TOP; }
			 IGVM_SWITCH_CASE(MUL_F64) : { sp--;   sp[0].f64 *= sp[1].f64; GOTO_IGVM_SWITCH_TOP; }
			 IGVM_SWITCH_CASE(MLA_F64) : { sp-=2;   sp[0].f64 += sp[1].f64*sp[2].f64; GOTO_IGVM_SWITCH_TOP; }
			 IGVM_SWITCH_CASE(DIV_F64):  { 
				sp--;   
				// divide by 0 check is not needed since.  0/0 = NAN,  +X/0 = +Inf,  -X/0 = -Inf
				sp[0].f64 /= sp[1].f64; 
				GOTO_IGVM_SWITCH_TOP; 
			 }
			 IGVM_SWITCH_CASE(MOD_F64) : { 
				sp--;   
				double pre = sp[0].f64;  
				if (IGVM_UNLIKELY(sp[1].f64 == 0.0)) {
					goto DIVIDE_BY_ZERO_ERROR;
				}
				sp[0].f64 =  pre - sp[1].f64 * ((int)(pre / sp[1].f64));
				GOTO_IGVM_SWITCH_TOP; 
			 }

			 IGVM_SWITCH_CASE(AND_I32)      : { sp--;  sp[0].i32 = sp[0].i32 & sp[1].i32; GOTO_IGVM_SWITCH_TOP; }
			 IGVM_SWITCH_CASE(OR_I32)       : { sp--;  sp[0].i32 = sp[0].i32 | sp[1].i32;  GOTO_IGVM_SWITCH_TOP; }
			 IGVM_SWITCH_CASE(XOR_I32)      : { sp--;  sp[0].i32 = sp[0].i32 ^ sp[1].i32;  GOTO_IGVM_SWITCH_TOP; }
			 IGVM_SWITCH_CASE(ASR_I32)      : { sp--;  sp[0].i32 >>= sp[1].i32; GOTO_IGVM_SWITCH_TOP; }
			 IGVM_SWITCH_CASE(LSR_I32)      : { sp--;  sp[0].u32 >>= sp[1].i32; GOTO_IGVM_SWITCH_TOP; }
			 IGVM_SWITCH_CASE(LSL_I32)      : { sp--;  sp[0].i32 <<= sp[1].i32; GOTO_IGVM_SWITCH_TOP; }
			 IGVM_SWITCH_CASE(CMP_EQ_I32)   : { sp--;  sp[0].i32 = ((sp[0].i32 == sp[1].i32) ? 1 : 0); GOTO_IGVM_SWITCH_TOP; }
			 IGVM_SWITCH_CASE(CMP_LT_I32)   : { sp--;  sp[0].i32 = ((sp[0].i32 < sp[1].i32) ? 1 : 0); GOTO_IGVM_SWITCH_TOP; }
			 IGVM_SWITCH_CASE(CMP_LE_I32)   : { sp--;  sp[0].i32 = ((sp[0].i32 <= sp[1].i32) ? 1 : 0); GOTO_IGVM_SWITCH_TOP; }
			 IGVM_SWITCH_CASE(CMP_EQ_F64)   : { sp--;  sp[0].i32 = ((sp[0].f64 == sp[1].f64) ? 1 : 0); GOTO_IGVM_SWITCH_TOP; }
			 IGVM_SWITCH_CASE(CMP_LT_F64)   : { sp--;  sp[0].i32 = ((sp[0].f64 < sp[1].f64) ? 1 : 0); GOTO_IGVM_SWITCH_TOP; }
			 IGVM_SWITCH_CASE(CMP_LE_F64)   : { sp--;  sp[0].i32 = ((sp[0].f64 <= sp[1].f64) ? 1 : 0); GOTO_IGVM_SWITCH_TOP; }
			 IGVM_SWITCH_CASE(CMP_EQ_I64)   : { sp--;  sp[0].i32 = ((sp[0].i64 == sp[1].i64) ? 1 : 0); GOTO_IGVM_SWITCH_TOP; }
			 IGVM_SWITCH_CASE(BIT_NOT_I32)  : { sp[0].i32 = ~sp[0].i32; GOTO_IGVM_SWITCH_TOP; }
			 IGVM_SWITCH_CASE(NOT)	        : { sp[0].i32 ^= 0x00000001; GOTO_IGVM_SWITCH_TOP; }
			 IGVM_SWITCH_CASE(F64_TO_I32)   : { sp[0].i32 = sp[0].f64; GOTO_IGVM_SWITCH_TOP; }
			 IGVM_SWITCH_CASE(I32_TO_F64)   : { sp[0].f64 = sp[0].i32; GOTO_IGVM_SWITCH_TOP ; }
			 IGVM_SWITCH_CASE(PUSH_64)      : { sp++; READ_I64(tmp64); sp->i64 = tmp64; GOTO_IGVM_SWITCH_TOP; }
			 IGVM_SWITCH_CASE(PUSH_32)      : { sp++; READ_I32(tmp32); sp->i32 = tmp32; GOTO_IGVM_SWITCH_TOP; }
			 IGVM_SWITCH_CASE(PUSH_I8)      : { sp++; READ_I8(tmp8);   sp->i32 = tmp8;	GOTO_IGVM_SWITCH_TOP; }
			 IGVM_SWITCH_CASE(PUSH_I16)     : { sp++; READ_I16(tmp16); sp->i32 = tmp16;	GOTO_IGVM_SWITCH_TOP; }				 
			 IGVM_SWITCH_CASE(I32_0)        : { sp++;  sp->i32 = 0;	GOTO_IGVM_SWITCH_TOP; }
			 IGVM_SWITCH_CASE(I32_1)        : { sp++;  sp->i32 = 1;	GOTO_IGVM_SWITCH_TOP; }
			 IGVM_SWITCH_CASE(PUSH_F64_0)   : { sp++;  sp->f64 = 0;	GOTO_IGVM_SWITCH_TOP; }
			 IGVM_SWITCH_CASE(PUSH_F64_1)   : { sp++;  sp->f64 = 1;	GOTO_IGVM_SWITCH_TOP; }
			 IGVM_SWITCH_CASE(DEBUG_BLOCK_START)      : { block_sp = sp;	GOTO_IGVM_SWITCH_TOP; }
			 IGVM_SWITCH_CASE(DEBUG_BLOCK_END)        : { 				
				if (sp != block_sp) 
				{
					printf("Block failure\n");
				
					StackFrame * prev_frame = frame;
					StackFrame tmp_frame;
					tmp_frame.m_prev = prev_frame;
					tmp_frame.m_pc   = pc;  // on frame exit restore these values
					tmp_frame.m_bp   = bp;  // on frame exit restore these values
					tmp_frame.m_sp   = sp;  // on frame exit restore these values
					tmp_frame.m_function = curr_fn;
				
					const int MAX_STACK_TRACE = 2048*4;

					char data[MAX_STACK_TRACE];
					snprintf(data, MAX_STACK_TRACE, "\n");


					this->_stackTrace(&tmp_frame, bp, data, MAX_STACK_TRACE);
			 

					fprintf(stderr, "igvm-stack-trace\n%s", data);
				
					// !!! bad
					return errorOccurred(0xED);
				}	 
				GOTO_IGVM_SWITCH_TOP; 
			 }
						
						
			 IGVM_SWITCH_CASE(SWITCH_I32_JMP)      :  { 
				
				
				pc --;
				
				ig_i32 value = sp[0].i32;
				while (pc[0] == InOp::SWITCH_I32_JMP)
				{
					pc++;
					READ_I32(tmp32); 

					if (tmp32 == value) { 
					
						unsigned char * curr_pc = pc;
						
						ig_i16 jmp;
						READ_I16(jmp);
					
						pc = curr_pc + jmp; 
						GOTO_IGVM_SWITCH_TOP; 
					}
					else {
						pc += 2;
					}
				}
				
				GOTO_IGVM_SWITCH_TOP; 
			 }

			 IGVM_SWITCH_CASE(LL0): {
				sp++;  *sp = bp[0];
				GOTO_IGVM_SWITCH_TOP;
			 }
			 
			IGVM_SWITCH_CASE(LL1): {
				sp++;  *sp = bp[1];
				GOTO_IGVM_SWITCH_TOP;
			 }
			 
			 IGVM_SWITCH_CASE(LL2): {
				sp++;  *sp = bp[2];
				GOTO_IGVM_SWITCH_TOP;
			 }
			 
			IGVM_SWITCH_CASE(LL3): {
				sp++;  *sp = bp[3];
				GOTO_IGVM_SWITCH_TOP;
			 }

			IGVM_SWITCH_CASE(LL4): {
				sp++;  *sp = bp[4];
				GOTO_IGVM_SWITCH_TOP;
			 }				 
			 
			 IGVM_SWITCH_CASE(LOCAL_LOAD) : {
				 READ_I16(tmp16);
				 sp++; *sp = bp[tmp16];
				 GOTO_IGVM_SWITCH_TOP;
			 }
			 
			 IGVM_SWITCH_CASE(LOCAL_LOAD_LO) : {
				 sp++; *sp = bp[pc[0]];  pc++;
				 GOTO_IGVM_SWITCH_TOP;
			 }
			 
			IGVM_SWITCH_CASE(LOCAL_LOAD_2)   : 
			 {
				 sp[1] = bp[pc[0]];
				 sp[2] = bp[pc[1]];
				 sp += 2;
				 pc += 2;
				 GOTO_IGVM_SWITCH_TOP;
			 }
			 
			 IGVM_SWITCH_CASE(LOCAL_STORE)  :     { READ_I16(tmp16); bp[tmp16] = *sp; sp--; GOTO_IGVM_SWITCH_TOP; }
			 
			 IGVM_SWITCH_CASE(LOCAL_STORE_LO)  :  { bp[pc[0]] = *sp; sp--; pc++; GOTO_IGVM_SWITCH_TOP; }
			 
			 
			 IGVM_SWITCH_CASE(LOCAL_STORE_KEEP)        : { bp[pc[0]] = *sp; pc++;      GOTO_IGVM_SWITCH_TOP; }
			 IGVM_SWITCH_CASE(LOCAL_CLEAR)             : { READ_I16(tmp16); bp[tmp16].i64 = 0; GOTO_IGVM_SWITCH_TOP; }
			 IGVM_SWITCH_CASE(INTERNAL_LOCAL_CLEAR_LO) : { bp[pc[0]].i64 = 0; pc ++; GOTO_IGVM_SWITCH_TOP; }

			 IGVM_SWITCH_CASE(MEMBER_LOAD)  : {
				IGVM_NULL_CHECK(m_heap.m_handles[sp->u32]);
				READ_I16(tmp16); sp->i64 = m_heap.m_handles[sp->u32]->i64(tmp16);  
				GOTO_IGVM_SWITCH_TOP; }
				
			 IGVM_SWITCH_CASE(MEMBER_LOAD_32)  : {
				IGVM_NULL_CHECK(m_heap.m_handles[sp->u32]);
				READ_I16(tmp16); 
				sp->i32 = m_heap.m_handles[sp->u32]->i32(tmp16);  
				sp->lo_hi[1] = 0;
				GOTO_IGVM_SWITCH_TOP; }	
				
			IGVM_SWITCH_CASE(INTERNAL_MEMBER_LOAD_LO)  : {
				IGVM_NULL_CHECK(m_heap.m_handles[sp->u32]);
				sp->i64 = m_heap.m_handles[sp->u32]->i64(pc[0]);  pc++;
				GOTO_IGVM_SWITCH_TOP; }
				
			 IGVM_SWITCH_CASE(INTERNAL_MEMBER_LOAD_32_LO)  : {
				IGVM_NULL_CHECK(m_heap.m_handles[sp->u32]);
				sp->i32 = m_heap.m_handles[sp->u32]->i32(pc[0]);  pc ++;
				sp->lo_hi[1] = 0;
				GOTO_IGVM_SWITCH_TOP; }	
					
				
			IGVM_SWITCH_CASE(LOAD_OBJ_FIELD) : {
				Object * obj = m_heap.m_handles[bp[pc[0]].u32];  pc++;
				IGVM_NULL_CHECK(obj);
				READ_I16(tmp16); sp++; sp->i64 = obj->i64(tmp16);  
				GOTO_IGVM_SWITCH_TOP;
			}	
				
			IGVM_SWITCH_CASE(LOAD_OBJ_FIELD_32) : {
				Object * obj = m_heap.m_handles[bp[pc[0]].u32];  pc++;
				IGVM_NULL_CHECK(obj);
				READ_I16(tmp16);
				sp++;
				sp->i32 = obj->i32(tmp16);  
				sp->lo_hi[1] = 0;
				GOTO_IGVM_SWITCH_TOP;
			}	
			
				
			IGVM_SWITCH_CASE(I_MEMBER_STORE_FUNCPTR):
			{
				READ_I16(tmp16); sp--;

				Object * obj = m_heap.m_handles[sp->u32];
				IGVM_NULL_CHECK(obj);

				m_heap.assigned(sp[0].u32, sp[1].u32);

				obj->i64(tmp16) = sp[1].i64; /*sp[0] = sp[1]; */ sp--;
				GOTO_IGVM_SWITCH_TOP;
			 }
			 
			 IGVM_SWITCH_CASE(I_MEMBER_STORE_OBJ):
			 {
				READ_I16(tmp16); sp--;

				Object * obj = m_heap.m_handles[sp->u32];
				IGVM_NULL_CHECK(obj);
				
				m_heap.assigned(sp[0].u32, sp[1].u32);

				obj->i32(tmp16) = sp[1].i32;  sp--;
				GOTO_IGVM_SWITCH_TOP;
			 }
			 
			 IGVM_SWITCH_CASE(MEMBER_STORE_32_REF) :
			 {
				READ_I16(tmp16); sp--;

				Object * obj = m_heap.m_handles[sp->u32];
				IGVM_NULL_CHECK(obj);
				
				m_config._swap_ref_32(this, obj->i32(tmp16), sp[1].i32, true);

				// swap refs
				obj->i32(tmp16) = sp[1].i32;  sp--;
				GOTO_IGVM_SWITCH_TOP;
			 }
			 
			 IGVM_SWITCH_CASE(MEMBER_STORE) : 
			 {
				READ_I16(tmp16); sp--;

				Object * obj = m_heap.m_handles[sp->u32];
				IGVM_NULL_CHECK(obj);

				obj->i64(tmp16) = sp[1].i64;  sp--;
				GOTO_IGVM_SWITCH_TOP;
			 }
			 
			 IGVM_SWITCH_CASE(MEMBER_STORE_32) : 
			 {
				READ_I16(tmp16); sp--;

				Object * obj = m_heap.m_handles[sp->u32];
				IGVM_NULL_CHECK(obj);

				obj->i32(tmp16) = sp[1].i32;  sp--;
				GOTO_IGVM_SWITCH_TOP;
			 }
			 
			 IGVM_SWITCH_CASE(INTERNAL_THIS_LOAD_LO)  : {
				sp++;
				sp->i64 = m_heap.m_handles[bp->u32]->i64(pc[0]); pc++;
				GOTO_IGVM_SWITCH_TOP;
			 }
			 
			 IGVM_SWITCH_CASE(INTERNAL_THIS_LOAD_32_LO)  : {
				sp++;
				sp->i32 = m_heap.m_handles[bp->u32]->i32(pc[0]); pc++;
				sp->lo_hi[1] = 0;
				GOTO_IGVM_SWITCH_TOP;
			 }
			 
			 IGVM_SWITCH_CASE(THIS_LOAD)  : {
				READ_I16(tmp16);
				sp++;
				sp->i64 = m_heap.m_handles[bp->u32]->i64(tmp16);
				GOTO_IGVM_SWITCH_TOP;
			 }
			 
			 IGVM_SWITCH_CASE(THIS_LOAD_32)  : {
				READ_I16(tmp16);
				sp++;
				sp->i32 = m_heap.m_handles[bp->u32]->i32(tmp16);
				sp->lo_hi[1] = 0;
				GOTO_IGVM_SWITCH_TOP;
			 }
			 
			 IGVM_SWITCH_CASE(THIS_INC_I32)  : {
				READ_I16(tmp16);
				READ_I8(tmp8);
				
				m_heap.m_handles[bp->u32]->i32(tmp16) += tmp8;
				GOTO_IGVM_SWITCH_TOP;
			 }
			 
			 
			 IGVM_SWITCH_CASE(THIS_STORE) : { 
				READ_I16(tmp16); 
				m_heap.m_handles[bp->u32]->i64(tmp16) = sp[0].i64;      sp--;
				GOTO_IGVM_SWITCH_TOP; 
			 }
			 
			 IGVM_SWITCH_CASE(THIS_STORE_32) : { 
				READ_I16(tmp16); 
				m_heap.m_handles[bp->u32]->i32(tmp16) = sp[0].i32;      sp--;
				GOTO_IGVM_SWITCH_TOP; 
			 }

			IGVM_SWITCH_CASE(THIS_STORE_32_REF) : { 
				READ_I16(tmp16); 
				
				m_config._swap_ref_32(this, m_heap.m_handles[bp->u32]->i32(tmp16), sp[0].i32, true);
				
				// SWAP REFS
				m_heap.m_handles[bp->u32]->i32(tmp16) = sp[0].i32;      sp--;
				GOTO_IGVM_SWITCH_TOP; 
			 }				 
			 
			 IGVM_SWITCH_CASE(I_THIS_STORE_OBJ): {
				READ_I16(tmp16); 
				
				m_heap.assigned(bp[0].u32, sp[0].u32);
				m_heap.m_handles[bp->u32]->i32(tmp16) = sp[0].i32;      sp--;
				GOTO_IGVM_SWITCH_TOP; 				 
			 }
			 
			 
			 IGVM_SWITCH_CASE(I_THIS_STORE_FUNCPTR):{
				READ_I16(tmp16); 
				
				m_heap.assigned(bp[0].u32, sp[0].u32);
				m_heap.m_handles[bp->u32]->i64(tmp16) = sp[0].i64;     sp--; 
				GOTO_IGVM_SWITCH_TOP; 				 
			 }

			 IGVM_SWITCH_CASE(STATIC_LOAD)  : 
			 {
				READ_I16(tmp16); sp++;  sp->i64 = m_static_data.i64(tmp16);
				GOTO_IGVM_SWITCH_TOP;
			 }
			 
			 IGVM_SWITCH_CASE(STATIC_STORE_32_REF) :
			 {
				READ_I16(tmp16); 
			 
				// swap refs
				//printf("STATIC_STORE_32_REF %d %lld %d\n", tmp16, m_static_data.i64(tmp16), sp[0].i32);
				m_config._swap_ref_32(this, m_static_data.i32(tmp16), sp[0].i32, true);
				//printf("\tEND\n");
				
				m_static_data.i64(tmp16) = sp->i64;   sp--;        
				GOTO_IGVM_SWITCH_TOP;     
			 }
			 IGVM_SWITCH_CASE(STATIC_STORE) : { 
				READ_I16(tmp16); m_static_data.i64(tmp16) = sp->i64;   sp--;        
				GOTO_IGVM_SWITCH_TOP;     
			 }
			 
			 
			 IGVM_SWITCH_CASE(STATIC_LOAD_OBJ) :
			 IGVM_SWITCH_CASE(STATIC_LOAD_FUNCPTR):
			 {
				READ_I16(tmp16); sp++;  
				*sp = *m_heap.getStaticRoot(tmp16);	
				GOTO_IGVM_SWITCH_TOP;
			 }
			 	 
			 IGVM_SWITCH_CASE(I_STATIC_STORE_OBJ) : 
			 IGVM_SWITCH_CASE(I_STATIC_STORE_FUNCPTR) :
			 {
				READ_I16(tmp16);
				m_heap.setStaticRoot(tmp16, *sp); sp--;
				GOTO_IGVM_SWITCH_TOP;
			 }

			 IGVM_SWITCH_CASE(FUNCPTR_MEMBER) : {
				READ_I16(tmp16);
				sp->fp.function = tmp16;
				GOTO_IGVM_SWITCH_TOP;
			 }
			 IGVM_SWITCH_CASE(FUNCPTR_STATIC) : {
				sp++; READ_I16(tmp16);
				sp->fp.object   = 0;
				sp->fp.function = tmp16;
				GOTO_IGVM_SWITCH_TOP;
			 }

			 IGVM_SWITCH_CASE(CALL_RET1_VTABLE):
			 IGVM_SWITCH_CASE(CALL_VTABLE)    :
			 {
				READ_I8(argc);			// load the argument count
				READ_I16(tmp16);		// load the vtable offset

				sp = sp - argc;	  // point prior to the argument list
				Object *   callee = m_heap.m_handles[sp[1].u32];
				IGVM_NULL_CHECK(callee);

				Function * callee_func = callee->m_vtable->m_functions[tmp16];

				fn = callee_func;

				goto INVOKE_FUNCTION;
			 }

			 IGVM_SWITCH_CASE(CALL_RET1_STATIC):
			 IGVM_SWITCH_CASE(CALL_STATIC)   : {


				READ_I8(argc);			// load the argument count
				READ_I16(tmp16);		// load the pair index

				sp = sp - argc;	// point prior to the argument list
				fn = (Function *)m_pairs[tmp16];

				goto INVOKE_FUNCTION;
			 }

			 // same as CALL_STATIC but checks that the first parameter
			 // is not null
			 IGVM_SWITCH_CASE(CALL_RET1_STATIC_VTABLE):
			 IGVM_SWITCH_CASE(CALL_STATIC_VTABLE)   : {
				READ_I8(argc);			// load the argument count
				READ_I16(tmp16);		// load the pair index



				sp = sp - argc;	// point prior to the argument list

				Object *   callee = m_heap.m_handles[sp[1].u32];
				IGVM_NULL_CHECK(callee);



				fn = (Function *)m_pairs[tmp16];
				goto INVOKE_FUNCTION;
			 }

			 IGVM_SWITCH_CASE(CALL_RET1_DYNAMIC):
			 IGVM_SWITCH_CASE(CALL_DYNAMIC)   :
			 {
				READ_I8(argc);			// load the argument count
				sp = sp - argc - 1;		// make the stack pointer point prior to the function pointer


				FuncPtr fp = sp[1].fp;

				// TODO check that this crashes in a reliable manner
				if (fp.function == 0) {
					printf("InOp:: CALL_DYNAMIC Null Pointer func: %d object: %d \n", fp.function, fp.object);
					goto NULL_POINTER_ERROR;
				}

				fn = (Function *)m_pairs[fp.function];
				if (fn->m_type == Item::STATIC_FUNCTION) 
				{

					// we need to shuffle down parameters
					// so that return values are in the right place
					for (int shuffle = 0; shuffle < argc; shuffle ++) {
						sp[shuffle + 1] = sp[shuffle + 2];
					}
				}
				else
				{
					Object * callee        = m_heap.m_handles[sp[1].u32];
					IGVM_NULL_CHECK(callee);
					
					fn = callee->m_vtable->m_functions[fn->m_offset];
					
					sp[1].fp.function = 0;
					argc++;					// include the this parameter in the arg count
				}

				goto INVOKE_FUNCTION;
			 }
			 
			 IGVM_SWITCH_CASE(CALL_RET1_INTERFACE):
			 IGVM_SWITCH_CASE(CALL_INTERFACE) : {
				READ_I8(argc);			// load the argument count
				READ_I16(tmp16);		// load the string id

				sp = sp - argc;			// make sp point prior to the argument list
				Object * callee        = m_heap.m_handles[sp[1].u32];
				IGVM_NULL_CHECK(callee);

				// find the function in the vtable with the matching name
				Function ** callee_functions = callee->m_vtable->m_functions;
				while (*callee_functions != NULL) {
					if ((*callee_functions)->m_name == (ig_u16)tmp16)
					{
						break;
					}
					callee_functions ++;
				}

				if (*callee_functions == NULL) {
					// couldn't find the function in the table
					dieAPainfullDeath("Couldn't find function in VTable");
				}
				else
				{
					fn = *callee_functions;
					goto INVOKE_FUNCTION;
				}
				GOTO_IGVM_SWITCH_TOP;
			 }

			 // Create a new instance of an object or leaf (no internal pointers)
			 IGVM_SWITCH_CASE(NEW)           :
			 {
				READ_I16(tmp16);

				Module * module = (Module *)m_pairs[tmp16];

#if IGVM_TRACK_FUNCTION_OPS == 1
				module->m_allocations    += m_debug_config;
				curr_fn->m_called_allocs += m_debug_config;
#endif				

#if 0
				// usefull for tracking down misc allocations
                 if (!printed_allocation)
                 {
                     printed_allocation = true;
                     const int MAX_STACK_TRACE = 2048*4;
                     char data[MAX_STACK_TRACE];
                     snprintf(data, MAX_STACK_TRACE, "\n");
                     this->_stackTrace(frame, bp, data, MAX_STACK_TRACE);
                     fprintf(stderr, "fn: %s allocation: \n%s", _getStringForId(curr_fn->m_name),   data);
                 }
 #endif
                 
				//IGVMStackRef(frame->m_stack, sp), 
				// really should just push pop this?
				
				// umm...
				stack->m_current_sp = sp;
				sp[1].u32   = this->_createModuleInstance(module);
				sp++;
									
				GOTO_IGVM_SWITCH_TOP;
			 }


			// check if we can upcast a particular object
			 IGVM_SWITCH_CASE(CAST_TO)    	 : 
			 {
				READ_I16(tmp16);

				// TODO make this work for interfaces as well?.. YAY!
				// TODO make this work for simplified templates as well

				Object * ptr = m_heap.m_handles[sp->u32];
				// ALLOW null pointers to not cause exceptions
				if (ptr != NULL)
				{
					IGVM_NULL_CHECK(ptr);


					//ig_u32 member_pair = ptr->m_vtable->m_member_pair;

					Module * member = ptr->m_vtable->m_module;	//(Module *) m_pairs[member_pair];
					if (NULL == member) {
						goto TYPE_CAST_ERROR;
					}
					if (!member->isInstanceOf(tmp16)) {
						goto TYPE_CAST_ERROR;
						//int * die = 0;
						//*die = 666;
					}
				}

				GOTO_IGVM_SWITCH_TOP;
			 }

			 IGVM_SWITCH_CASE(INSTANCE_OF): {
				READ_I16(tmp16);

				Object * ptr = m_heap.m_handles[sp->u32];
				IGVM_NULL_CHECK(ptr);

				Module * member = ptr->m_vtable->m_module;	
				if (IGVM_UNLIKELY(NULL == member)) {
					goto TYPE_CAST_ERROR;
				}
				sp->i32 = (member->isInstanceOf(tmp16)) ? 1 : 0;

				GOTO_IGVM_SWITCH_TOP;
			 }


			IGVM_SWITCH_CASE(STACK_TRACE): {

				stack->m_current_sp = sp;

				const int MAX_STACK_TRACE = 2048*4;

				char data[MAX_STACK_TRACE];
				snprintf(data, MAX_STACK_TRACE, "Error: \n");


				this->_stackTrace(frame, bp, data, MAX_STACK_TRACE);
			 

				printf("igvm-stack-trace\n%s", data);

				this->_beginScope();
				int ec;
				Box str;
				if (0 != (ec = this->_stringFromUTF8Bytes(data, &str))) {
					this->_endScope();
					return 1;
				}
				
				// push the string with the stack trace in it to the stack
				sp++;
				*sp = str;

				this->_endScope();
				GOTO_IGVM_SWITCH_TOP;
			 }

			 // might need a rethrow maybe?
			 IGVM_SWITCH_CASE(THROW)		 :
			 {
				// grab the exception to throw off of the top of the stack
				Box ex = *sp;
				sp --;

				ExceptionTableEntry * found    = NULL;
				while (found == NULL)
				{
					unsigned char * code_root  = curr_fn->m_code;
					unsigned char * curr_pos   = pc;

					// figure out how many bytes into the code segment we currently are
					ig_i32 offset = (ig_i32) (((size_t)curr_pos) - ((size_t)code_root));

					int                   et_count = curr_fn->m_header.m_exception_count;
					ExceptionTableEntry * et       = curr_fn->m_exception_table;

					// look within the exception table to find an entry that matches the
					// current code offset
					for (int ei = et_count - 1; ei >= 0; ei --) {
						if (et[ei].isWithin(offset)) {
							// found the thing to handle our issue
							found = &et[ei];
							break;
						}
					}

					// drop down to a previous stack frame
					if (NULL == found)
					{
						pc      = frame->m_pc;
						bp      = frame->m_bp;
						curr_fn = frame->m_function;
						frame   = frame->m_prev;


						//////////
						// Okay so this exception just wasn't handled
						if (frame == NULL)
						{	
							m_heap.setStaticRoot(m_error_root_idx, ex);
							
							
							// store a reference to the exception for later
							//if (m_error_object.u32 != 0) {
							//	VM::decRef(m_error_object);
							//}
							//m_error_object = ex;
							//VM::incRef(m_error_object);
							if (ret != NULL) {
								ret->i64 = 0;
							}
							if (destroy_stack) {
								m_stack_pool.dealloc(stack);
							}

							// error this should be tagged as an unhandled exception
							// ?? bad
							return errorOccurred(0xEE);
						}
						else
						{
							sp = (Box *)&frame[1];
						}
					}
				}

				// adjust the program counter so that it sits within the exception handler
				// had to add in the minus 1, since somewhere I was calculating the offset incorrectly
				pc = &curr_fn->m_code[found->m_pc_end ];
				sp ++;

				// store the exception so that it can be handled
				*sp = ex;

				GOTO_IGVM_SWITCH_TOP;
			 }

			 IGVM_SWITCH_CASE(NEXT_IT)      : {

				// next_it idx PAIR LABEL

				// should just read this in as a giant block
				ig_i16 local_idx;
				ig_i16 it_var;
				ig_i16 it_func;
				ig_i16 jmp;

				READ_I16(local_idx);
				READ_I16(it_var);
				READ_I16(it_func);

				unsigned char * curr_pc = pc;
				READ_I16(jmp);

				Box & iterator      = bp[local_idx];
				Object * obj = m_heap.m_handles[iterator.u32];
				IGVM_NULL_CHECK(obj);

				int has_next = obj->i32(it_var);
				fn           = obj->m_vtable->m_functions[it_func];

				if (!has_next) {
					pc = curr_pc + jmp;
				}
				else
				{
					argc = 1;

					sp[1] = iterator;
					goto INVOKE_FUNCTION;

				}

				GOTO_IGVM_SWITCH_TOP;
			 }

			 IGVM_SWITCH_CASE(PUSH_STRING)  :
			 {
				ig_u16 string_idx;
				READ_U16(string_idx);		// okay so we've loaded the string id

				//printf("PUSH_STRING: %d\n", (int)string_idx);

				ig_u32 pair_id = this->_constantPair(0, string_idx, true);
				
				
				
				if (m_pairs[pair_id] == NULL) 
				{
					
					//printf("PUSH_STRING... bp=%p sp=%p\n", bp, sp);
					stack->m_current_sp = sp;
					
					
					////D//printf("PUSH_STRING creation: %d\n", tmp16);
					// essentially create a pair that points to a string object

					const char * text = _getStringForId(string_idx);
					////D//printf("'%s'\n", text);
					int ec;

					this->_beginScope();
					// likely add a begin/end scope here
					Box str;
					if (0 != (ec = this->_stringFromUTF8Bytes(text, &str))) {
						// BAD FORM
						
						this->_endScope();
						return 1;
					}
					
					int idx = m_heap.allocAndClearStaticRoot();
					m_heap.setStaticRoot(idx, str);
					
					this->_endScope();

					// create a new string item
					String * snew = (String *)IGVM_MALLOC( sizeof(String));
					{
						snew->m_type = Item::STRING;
						snew->m_handle = str.u32;
					}

					m_pairs[pair_id] = snew;
				}


				String * s = (String *)m_pairs[pair_id];

				sp++;
				sp->u32 = s->m_handle;
				GOTO_IGVM_SWITCH_TOP;
			}

			 IGVM_SWITCH_CASE(INTERNAL_JMP_LO) : {
				pc += ((ig_i8 *)pc)[0];
				GOTO_IGVM_SWITCH_TOP;
			 }

			 IGVM_SWITCH_CASE(JMP)           : {
				unsigned char * curr_pc = pc;	// grab the PC before reading the arg
				ig_i16 jmp;
				READ_I16(jmp);

				pc = curr_pc + jmp;
				GOTO_IGVM_SWITCH_TOP;
			 }

			 IGVM_SWITCH_CASE(J0_I32): {
				sp--;
				unsigned char * curr_pc = pc;
				ig_i16 jmp;
				READ_I16(jmp);

				if (sp[1].i32 == 0) { pc = curr_pc + jmp; }
				GOTO_IGVM_SWITCH_TOP;
			 }

			 IGVM_SWITCH_CASE(JN0_I32): {
				sp--;
				unsigned char * curr_pc = pc;
				ig_i16 jmp;
				READ_I16(jmp);

				if (sp[1].i32 != 0) { pc = curr_pc + jmp; }
				GOTO_IGVM_SWITCH_TOP;
			 }
			 
			  IGVM_SWITCH_CASE(AND): {
				unsigned char * curr_pc = pc;
				ig_i16 jmp;
				READ_I16(jmp);

				if (sp[0].i32 == 0) { pc = curr_pc + jmp; } else {sp--; }
				GOTO_IGVM_SWITCH_TOP;
			 }

			 IGVM_SWITCH_CASE(OR): {
				unsigned char * curr_pc = pc;
				ig_i16 jmp;
				READ_I16(jmp);

				if (sp[0].i32 != 0) { pc = curr_pc + jmp;  } else {sp--; }
				GOTO_IGVM_SWITCH_TOP;
			 }
			 
			 
			 IGVM_SWITCH_CASE(JMP_IN_I32_0RANGE): {
				sp -= 2;
				
				unsigned char * curr_pc = pc;
				ig_i16 jmp;
				READ_I16(jmp);

				int v0 = sp[1].i32;
				if (0 <= v0 && v0 < sp[2].i32) { pc = curr_pc + jmp; }				 
				GOTO_IGVM_SWITCH_TOP;
			 }

			 IGVM_SWITCH_CASE(JEQ_I32): {
				sp -= 2;
				unsigned char * curr_pc = pc;
				ig_i16 jmp;
				READ_I16(jmp);

				if (sp[1].i32 == sp[2].i32) { pc = curr_pc + jmp; }
				GOTO_IGVM_SWITCH_TOP;
			 }

			 IGVM_SWITCH_CASE(ARRAY_NEW): {
				ig_i8 storage_class;

				READ_I16(tmp16);
				READ_I8(storage_class);

				Module * module = (Module *)m_pairs[tmp16];
				
#if IGVM_TRACK_FUNCTION_OPS == 1
				module->m_allocations += m_debug_config;
				curr_fn->m_called_allocs += m_debug_config;
#endif					
				
				// should do this somewhere else
				module->m_header.m_array_storage_class = storage_class;

				const int len = sp->i32;
				if (IGVM_UNLIKELY(len < 0)) {
					goto NEG_ARRAY_SIZE_ERROR;
				}
				
				stack->m_current_sp = sp;
				sp->u32 = this->_createModuleArrayInstance(module, len, storage_class);

				GOTO_IGVM_SWITCH_TOP;
			 }

			IGVM_SWITCH_CASE(INTERNAL_VECTOR_32_GET): 
			IGVM_SWITCH_CASE(INTERNAL_VECTOR_OBJECT_GET): {
				//fprintf(stderr, "VG A %x %x", sp[-1].u32, sp[0].i32);
				sp--;
				Object * obj = m_heap.m_handles[sp->u32];
				IGVM_NULL_CHECK(obj);

				//fprintf(stderr, " B");
				int      idx = sp[1].i32;

				// 0: handle to data,  4 : length
				// 8: capacity

				ig_u32 arr_handle = ((ig_u32 *) (obj->m_data))[0];
				ig_u32 len        = ((ig_u32 *) (obj->m_data))[1];
				if (IGVM_UNLIKELY((unsigned)idx >= len)) {
					sp[1].i32 = idx;
					sp[2].i32 = len;
					goto ARRAY_BOUNDS_ERROR;
				}
	
				//fprintf(stderr, " C");	
				
				// this assumes that the underlying array is configured correctly
				// ie. it is not null and it has a length greater than the index 
				
				Object * arr = m_heap.m_handles[arr_handle];
				ig_i32 * data = (ig_i32 *) (arr->m_data + 1);
				sp->i32 = data[idx];
				
				//fprintf(stderr, " E\n");
				GOTO_IGVM_SWITCH_TOP;
			 }

			IGVM_SWITCH_CASE(INTERNAL_VECTOR_LENGTH): {
				Object * obj = m_heap.m_handles[sp->u32];
				IGVM_NULL_CHECK(obj);

				// 0: handle to data,  4 : length
				// 8: capacity

				ig_i32 data = ((ig_i32 *) (obj->m_data))[1];
				sp->i32 = data;
				GOTO_IGVM_SWITCH_TOP;
			 }

			 IGVM_SWITCH_CASE(ARRAY_LENGTH): {
				Object * obj = m_heap.m_handles[sp->u32];
				IGVM_NULL_CHECK(obj);

				ig_i32 * data = (ig_i32 *) (obj->m_data + 0);
				sp->i32 = *data;
				GOTO_IGVM_SWITCH_TOP;
			 }

			IGVM_SWITCH_CASE(ARRAY_RESIZE): {
				ig_i8 storage_class;
				READ_I8(storage_class);
				
				sp--;
			
				Object * obj = m_heap.m_handles[sp->u32];
				IGVM_NULL_CHECK(obj);
				
				ig_i32 new_length  = sp[1].i32;
				
				sp->u32 = this->_reallocArrayInstance(sp->u32, obj, new_length, storage_class);
				GOTO_IGVM_SWITCH_TOP;
			}
			
			IGVM_SWITCH_CASE(ARRAY_FILL): {
			
				ig_i8 storage_class;
				READ_I8(storage_class);
				
				sp--;
			
				Object * obj = m_heap.m_handles[sp->u32];
				IGVM_NULL_CHECK(obj);
				
				
				this->_fillArray(sp->u32, obj, storage_class, sp[1]);
				
				sp--;
				GOTO_IGVM_SWITCH_TOP;
			}
			
			
			IGVM_SWITCH_CASE(INTERNAL_ARRAY_COMPARE): {
			
				ig_i8 storage_class;
				READ_I8(storage_class);
				
				sp--;
			
				Object * obj = m_heap.m_handles[sp->u32];
				IGVM_NULL_CHECK(obj);
				
				Object * src = m_heap.m_handles[sp[1].u32];
				IGVM_NULL_CHECK(src);
				
				int bool_result;
				this->_compareArray(storage_class, obj, src,  &bool_result);
				sp->i32 = bool_result;
				
				
				GOTO_IGVM_SWITCH_TOP;
			}
			
			IGVM_SWITCH_CASE(ARRAY_COPY): {
			
				ig_i8 storage_class;
				READ_I8(storage_class);
				
				sp--;
			
				Object * obj = m_heap.m_handles[sp->u32];
				IGVM_NULL_CHECK(obj);
				
				Object * src = m_heap.m_handles[sp[1].u32];
				IGVM_NULL_CHECK(src);
				
				
				this->_copyArray(sp->u32, obj, storage_class, sp[1].u32, src);
				
				sp--;
				GOTO_IGVM_SWITCH_TOP;
			}

			// for some reason these are disambiguated by the assembler?
			 IGVM_SWITCH_CASE(ARRAY_LOAD_8): {
				sp--;

				Object * obj = m_heap.m_handles[sp->u32];
				IGVM_NULL_CHECK(obj);

				int      idx = sp[1].i32;
				unsigned int      len = *(ig_u32 *) (obj->m_data + 0);
				if (IGVM_UNLIKELY((unsigned)idx >= len)) {
					sp[1].i32 = idx;
					sp[2].i32 = len;
					goto ARRAY_BOUNDS_ERROR;
				}

				ig_i8 * data = (ig_i8 *) (obj->m_data + 1);
				sp->i32 = data[idx];
				GOTO_IGVM_SWITCH_TOP;
			 }

			 IGVM_SWITCH_CASE(ARRAY_LOAD_16): {
				sp--;

				Object * obj = m_heap.m_handles[sp->u32];
				IGVM_NULL_CHECK(obj);

				int      idx = sp[1].i32;
				unsigned int      len = *(ig_u32 *) (obj->m_data + 0);
				if (IGVM_UNLIKELY((unsigned)idx >= len)) {
					sp[1].i32 = idx;
					sp[2].i32 = len;
					goto ARRAY_BOUNDS_ERROR;
				}

				ig_i16 * data = (ig_i16 *) (obj->m_data + 1);
				sp->i32 = data[idx];
				GOTO_IGVM_SWITCH_TOP;
			 }

			 IGVM_SWITCH_CASE(ARRAY_LOAD_OBJ):
			 IGVM_SWITCH_CASE(ARRAY_LOAD_32): {
				sp--;

				Object * obj = m_heap.m_handles[sp->u32];
				IGVM_NULL_CHECK(obj);

				int      idx = sp[1].i32;
				unsigned int      len = *(ig_u32 *) (obj->m_data + 0);
				if (IGVM_UNLIKELY((unsigned)idx >= len)) {
					sp[1].i32 = idx;
					sp[2].i32 = len;
					goto ARRAY_BOUNDS_ERROR;
				}

				ig_i32 * data = (ig_i32 *) (obj->m_data + 1);
				sp->i32 = data[idx];
				GOTO_IGVM_SWITCH_TOP;
			 }

			 IGVM_SWITCH_CASE(ARRAY_LOAD_FUNCPTR):
			 IGVM_SWITCH_CASE(ARRAY_LOAD_64): {
				sp--;

				Object * obj = m_heap.m_handles[sp->u32];
				IGVM_NULL_CHECK(obj);

				int      idx = sp[1].i32;
				unsigned int      len = *(ig_u32 *) (obj->m_data + 0);
				if (IGVM_UNLIKELY((unsigned)idx >= len)) {
					sp[1].i32 = idx;
					sp[2].i32 = len;
					goto ARRAY_BOUNDS_ERROR;
				}

				ig_i64 * data = (ig_i64 *) (obj->m_data + 1);
				sp->i64 = data[idx];
				
				GOTO_IGVM_SWITCH_TOP;
			 }


			IGVM_SWITCH_CASE(ARRAY_STORE_8): {
				sp -= 2;

				Object * obj   = m_heap.m_handles[sp->u32];
				IGVM_NULL_CHECK(obj);

				int      idx   = sp[1].i32;
				Box      value = sp[2];

				//reference for bounds check
				//http://stackoverflow.com/questions/17095324/fastest-way-in-c-to-determine-if-an-integer-is-between-two-integers-inclusive/17095534#17095534

				unsigned int      len = *(ig_u32 *) (obj->m_data + 0);
				if (IGVM_UNLIKELY((unsigned)idx >= len)) {
					sp[1].i32 = idx;
					sp[2].i32 = len;
					goto ARRAY_BOUNDS_ERROR;
				}

				ig_i8 * data = (ig_i8 *) (obj->m_data + 1);
				data[idx] = value.i32;

				//*sp = value;
				sp--;
				GOTO_IGVM_SWITCH_TOP;
			 }

			 IGVM_SWITCH_CASE(ARRAY_STORE_16): {
				sp -= 2;

				Object * obj   = m_heap.m_handles[sp->u32];
				IGVM_NULL_CHECK(obj);

				int      idx   = sp[1].i32;
				Box      value = sp[2];

				unsigned int      len = *(ig_u32 *) (obj->m_data + 0);
				if (IGVM_UNLIKELY((unsigned)idx >= len)) {
					sp[1].i32 = idx;
					sp[2].i32 = len;
					goto ARRAY_BOUNDS_ERROR;;
				}

				ig_i16 * data = (ig_i16 *) (obj->m_data + 1);
				data[idx] = value.i32;

				//*sp = value;
				sp--;
				GOTO_IGVM_SWITCH_TOP;
			 }


			 IGVM_SWITCH_CASE(ARRAY_STORE_32_REF):
			 IGVM_SWITCH_CASE(ARRAY_STORE_OBJ):
			 {
			 
				int op = pc[-1];
				
				sp -= 2;

				Object * obj   = m_heap.m_handles[sp->u32];
				IGVM_NULL_CHECK(obj);

				int      idx   = sp[1].i32;
				Box      value = sp[2];

				unsigned int      len = *(ig_u32 *) (obj->m_data + 0);
				if (IGVM_UNLIKELY((unsigned)idx >= len)) {
					sp[1].i32 = idx;
					sp[2].i32 = len;
					goto ARRAY_BOUNDS_ERROR;
				}

				ig_u32 * data = (ig_u32 *) (obj->m_data + 1);
				
				if (op == InOp::ARRAY_STORE_32_REF) {
					// SWAP REFERENCES
					m_config._swap_ref_32(this, data[idx], value.i32, true);
				}
				
				data[idx] = value.u32;
				
				if (op == InOp::ARRAY_STORE_OBJ) {
					m_heap.assigned(sp->u32, value.u32);
				}
				
				/* && (m_heap.m_handles_meta[sp->u32] & 0x3) != 0) {
					if (value.u32 != 0) {
						m_heap.whiteToGrey(value.u32);
					}
				}*/

				//*sp = value;
				sp--;

				GOTO_IGVM_SWITCH_TOP;
			 }


			 IGVM_SWITCH_CASE(ARRAY_STORE_32): 
			 {
				sp -= 2;

				Object * obj   = m_heap.m_handles[sp->u32];
				IGVM_NULL_CHECK(obj);

				int      idx   = sp[1].i32;
				Box      value = sp[2];

				unsigned int      len = *(ig_u32 *) (obj->m_data + 0);
				if (IGVM_UNLIKELY((unsigned)idx >= len)) {
					sp[1].i32 = idx;
					sp[2].i32 = len;
					goto ARRAY_BOUNDS_ERROR;
				}

				ig_u32 * data = (ig_u32 *) (obj->m_data + 1);
				data[idx] = value.u32;
				
				//*sp = value;
				sp--;

				GOTO_IGVM_SWITCH_TOP;
			 }

			 IGVM_SWITCH_CASE(ARRAY_STORE_FUNCPTR):
			 IGVM_SWITCH_CASE(ARRAY_STORE_64): {
				int op = pc[-1];
			 
				sp -= 2;

				Object * obj   = m_heap.m_handles[sp->u32];
				IGVM_NULL_CHECK(obj);

				int      idx   = sp[1].i32;
				Box      value = sp[2];
				
				//printf("InOp::ARRAY_STORE_64 function: %d object : %d\n",
				//	value.fp.function, value.fp.object);

				unsigned int      len = *(ig_u32 *) (obj->m_data + 0);
				if (IGVM_UNLIKELY((unsigned)idx >= len)) {
					sp[1].i32 = idx;
					sp[2].i32 = len;
					goto ARRAY_BOUNDS_ERROR;
				}

				ig_i64 * data = (ig_i64 *) (obj->m_data + 1);
				data[idx] = value.i64;

				if (op == InOp::ARRAY_STORE_FUNCPTR) {
					// && (m_heap.m_handles_meta[sp->u32] & 0x3) != 0) {
					//if (value.u32 != 0) {
					//	m_heap.whiteToGrey(value.u32);
					//}
					
					m_heap.assigned(sp->u32, value.u32);
				}

				//*sp = value;		// ? is this necessary anymore?
				sp--;

				GOTO_IGVM_SWITCH_TOP;
			 }
			 
			 IGVM_SWITCH_CASE(I_LS0) : 
			 IGVM_SWITCH_CASE(I_LS1) : 
			 IGVM_SWITCH_CASE(I_LS2) : 
			 IGVM_SWITCH_CASE(I_LS3) : 
			 IGVM_SWITCH_CASE(I_LS4) : 
			 IGVM_SWITCH_CASE(I_LS5) : 
			 IGVM_SWITCH_CASE(I_LS6) : 
			 IGVM_SWITCH_CASE(I_LS7) : 
			 {
			 	 bp[pc[-1] & 7] = sp[0]; 
				 sp--;
				 GOTO_IGVM_SWITCH_TOP;
			 }
			 

			IGVM_SWITCH_CASE(INVOKE_NATIVE): {
				
				// verify that a native function was defined
				if (IGVM_UNLIKELY(NULL == curr_fn->m_native)) {
					// ERROR
					return 1;
				}
				
				Box result;

				// fill out all required default values
				//int lc = fn->getLocalCount();
				//memcpy(&sp[1 + argc], &curr_fn->m_local_defaults[argc],  (lc - argc) * sizeof(Box));


				int native_ec = 0;

				Box  error_message;
				bool error_message_exists = false;
				bool returns = false;
				
				if (curr_fn->m_header.m_returns) {
					returns = true;	
				}
				
				
				this->_beginScope();
				try
				{
					// native code can return an error code, or throw an error message
					native_ec = curr_fn->m_native(this, bp, curr_fn->m_param_count, &result);
				}
				catch (NativeError ex) 
				{
					native_ec = 1;
					if (0 == this->_stringFromUTF8Bytes(ex.message, &error_message)) 
					{
						error_message_exists = true;
					}
				}
				this->_endScope();
				

				pc      = frame->m_pc;             // restore program-counter
				sp      = frame->m_sp;             // restore stack-pointer
				bp      = frame->m_bp;             // restore base-pointer
				curr_fn = frame->m_function;  // restore the current function

				// it would be nice if the frame position was implicit  :(
				// A0 A1 L2 L3 L4 .... Ln STACK-FRAME STACK
				// ^                      ^           ^
				// bp                     frame       sp
				
				
				frame = frame->m_prev;
				
				// if native code failed
				// construct a fake error and attempt to execute it
				if (IGVM_UNLIKELY(0 != native_ec))
				{
					//if (returns) {
					//	sp--;
					//}
					
					fprintf(stderr, "NATIVE EC: %d\n", native_ec);
					int cp = this->_constantPair(this->_constantString("iglang.Error", strlen("iglang.Error"), false),
												 this->_constantString("__ig_nativeError", strlen("__ig_nativeError"), false), false);

					fn = (Function *)m_pairs[cp];
					if (fn == 0) {
						return 1;	// nothing we can do except terminate execution
					}

					argc = 0;
					if (error_message_exists) {
						sp[1] = error_message;
						argc = 1;
					}


					goto INVOKE_FUNCTION;
				}
				else if (returns) {
					sp++;
					*sp = result;
				}

				GOTO_IGVM_SWITCH_TOP;
			}
			
			
			
			
			 IGVM_SWITCH_DEFAULT: {

				int op = pc[-1];

				fprintf(stderr, "Unknown opcode in stream: %d\n", op);
				dieAPainfullDeath("Unknown opcode");
				
				GOTO_IGVM_SWITCH_TOP;
			 }
			 
		}

		//char * ptr = (char * ) 0x1853bf010;	//0x181c44010;
		//if (*ptr != 0x55) {
		//	printf("Broke here baby %s\n", s_in_opm_string[op]);
		//}
	}


	// THIS WILL NEVER GET CALLED

	//if (destroy_stack) {
	//	m_stack_pool.dealloc(stack);
	//	// TODO this should probably clean up frames if there are any
	//}


	// ?? bad
	return 0;

	///////////////////////////////////////////////////////////////////////
	// Error Endpoints
	///////////////////////////////////////////////////////////////////////

	TYPE_CAST_ERROR:
	{
		fn = this->_getErrorFunction("iglang.Error", "__ig_typeCastError");
		if (fn == 0) { fprintf(stderr, "Missing Builtin"); return 1; }
		argc = 0;
		goto INVOKE_FUNCTION;
	}

	ARRAY_BOUNDS_ERROR:
	{
		fn = this->_getErrorFunction("iglang.Error", "__ig_arrayBoundsError");
		if (fn == 0) { fprintf(stderr, "Missing Builtin"); return 1; }
		argc = 2;
		goto INVOKE_FUNCTION;
	}
	
	NEG_ARRAY_SIZE_ERROR:
	NULL_POINTER_ERROR: 
	{
		fn = this->_getErrorFunction("iglang.Error", "__ig_nullPointerError");
		if (fn == 0) { fprintf(stderr, "Missing Builtin"); return 1; }
		argc = 0;
		goto INVOKE_FUNCTION;
	}

	DIVIDE_BY_ZERO_ERROR: 
	{
		fn = this->_getErrorFunction("iglang.Error", "__ig_divideByZeroError");
		if (fn == 0) { fprintf(stderr, "Missing Builtin"); return 1; }
		argc = 0;
		goto INVOKE_FUNCTION;
	}
}

Function * VMImp::_getErrorFunction(const char * klass, const char * func) {
	int cp = this->_constantPair(this->_constantString(klass, strlen(klass), false),
								 this->_constantString(func,   strlen(func), false), false);

	return (Function *)m_pairs[cp];
}


void VMImp::_stackTrace2()
{
}


void VMImp::_stackTrace(StackFrame * frame,Box * bp,  char * data, int MAX_STACK_TRACE)
{
	//Box * trace_bp = bp;
	StackFrame * trace_frame = frame;
	while (trace_frame != NULL && trace_frame->m_function != NULL) {

		// pc is currently pointing to the NEXT instruction to process
		size_t offset = (trace_frame->m_pc -
						trace_frame->m_function->m_code);
		offset --;


		ig_u32 fn_pair[2];
		this->_explodeConstantPair(trace_frame->m_function->m_pair, fn_pair);

		// calculate the line number that the trace will start at
		int line_no = 0;
		DebugTableEntry * dte = trace_frame->m_function->m_debug_table;
		for (int i = 0; i < trace_frame->m_function->m_header.m_debug_count; i++) {
			if (dte[i].m_offset >= offset) {
				break;
			}

			line_no = dte[i].m_line_no;
		}



		{
			// this should likely go through the log interface
			size_t len = strlen(data);
			snprintf(&data[len], MAX_STACK_TRACE-len, "\tat %s (%s:%d)\n",	// lc: %d bp: %p space: %d\n",
				VMImp::_getStringForId(fn_pair[0]),
				VMImp::_getStringForId(fn_pair[1]),
				line_no
				//,
				//trace_frame->m_function->m_header.m_local_count,
				//trace_frame->m_bp,
				//(int)((size_t)trace_frame->m_sp - (size_t)trace_frame->m_bp)
			);
		}

		/*
		TURN OFF VARIABLE DISPLAY FOR NOW
		for (int i = 0; i < trace_frame->m_function->m_header.m_local_count; i++) {
			int len = strlen(data);
			snprintf(&data[len], MAX_STACK_TRACE-len, "\t\t%d) %d %d\n",
				i,
				trace_bp[i].lo_hi[0],
				trace_bp[i].lo_hi[1]);
		}
		*/

		//trace_bp = trace_frame->m_bp;
		trace_frame = trace_frame->m_prev;
	}
}

};
