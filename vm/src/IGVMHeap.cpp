/*
Copyright (c) 2014-2017 Incubator Games Ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/


#include "IGVMInternal.h"


/**
 * TODO.  Have the heap keep a record of how much memory it's using
 *
 *
 * REFERENCES: http://www.mono-project.com/docs/advanced/garbage-collector/sgen/
 *
 *
 * okay... generations
 *    TRI-COLOR for YOUNG   (is referenced from roots or old generation)
 *    TRI-COLOR for OLD     (is referenced from roots)
 */

////////////////////////////////////////
// Linear allocator for the young generation
//////////////////////////////////////////////

namespace igvm
{	


void VMLinearAllocator::init(VMAllocator * alloc, size_t cap)
{

	m_allocator = alloc;
	m_count = 0;
	m_capacity = 0;
	m_data = NULL;
	m_memory_allocated = 0;
	m_last_ptr = NULL;
	m_capacity = cap;
	m_data     = (char *) m_allocator->MALLOC(cap, __FILE__, __LINE__);
	m_count    = 0;
	m_memory_allocated = 0;
	m_last_ptr = NULL;
}

void VMLinearAllocator::clear() {
	m_count = 0;
	m_memory_allocated = 0;
	m_last_ptr = NULL;
}

void VMLinearAllocator::destroy() {
	m_allocator->FREE(m_data, m_capacity);
	memset(this, 0, sizeof(VMLinearAllocator));
}



void * VMLinearAllocator::alloc(size_t sz) 
{
	// round sz to be divisible by 8, and add on header size
	size_t total = sizeof(VMLinearAllocatorEntry) + (((sz + 7) >> 3) << 3);
	
	// only put smallish objects into the linear allocator
	if (sz > 256 || total + m_count > m_capacity) 
	{
		return NULL;
	}
	
	VMLinearAllocatorEntry * ret = (VMLinearAllocatorEntry *) &m_data[m_count];
	ret->sz = sz;
	m_count += total;
	
	m_last_ptr = &ret[1];
	
	
	return &ret[1];
}


size_t VMLinearAllocator::getSize(void * ptr)
{
	VMLinearAllocatorEntry * ent = (VMLinearAllocatorEntry *) ptr;
	size_t sz = ent[-1].sz;
	return sz;
}


void * VMLinearAllocator::realloc(void * ptr, size_t new_sz)
{
	// figure out the total number of bytes the new allocation would require
	// round the number of bytes up to a count divisible by 8
	size_t total = (((new_sz + 7) >> 3) << 3);

	VMLinearAllocatorEntry * ent = (VMLinearAllocatorEntry *) ptr;
	size_t sz = ent[-1].sz;
	
	// round the number of bytes up to a count divisible by 8
	size_t old_total =  (((sz + 7) >> 3) << 3);
	
	
	if (m_last_ptr == ptr && (total - sz) + m_count <= m_capacity ) {
		ent[-1].sz = new_sz;			// was total
		m_count   += total - old_total;
	}
	else 
	{	
		void * new_ptr = alloc(new_sz);
		if (new_ptr == NULL) {
			return NULL;
		}
		memcpy(new_ptr, ptr, (new_sz < sz) ? new_sz : sz);
		
		return new_ptr; 
	}
	
	return ptr;
}

////////////////////////////////////////
// The primary heap
//////////////////////////////////////////////


void IGVMHeap::destroy() 
{
	// this assumes that reinit was called prior
	m_grey_list.destroy();
	m_static_roots.destroy();		// <-- list of active static roots
	m_dynamic_roots.destroy();		// <-- list of available static roots
	m_young_allocator.destroy();
	
	memset(this, 0, sizeof(IGVMHeap));
}


ig_u32 IGVMHeap::allocDynamicRoot()
{
	ig_u32 root;
	if (m_dynamic_roots.pop(&root)) {
		// hrm.. might be because we weren't clearing these?
		memset(&m_static_roots.m_handles[root], 0, sizeof(Box));
		return root;
	}

	return allocAndClearStaticRoot();
}

void   IGVMHeap::freeDynamicRoot(ig_u32 root)
{
	// todo should verify that this is a dynamic root somehow?
	m_dynamic_roots.push(root);
	memset(&m_static_roots.m_handles[root], 0, sizeof(Box));
}



ig_u32 IGVMHeap::allocAndClearStaticRoot()
{
	ig_u32 idx = (ig_u32) m_static_roots.m_count;
	Box box;
	memset(&box, 0, sizeof(Box));
	m_static_roots.push(box);
	return idx;
}


/**
 * Initialize the heap.
 */


int IGVMHeap::init(VMAllocator * alloc, int max_handles, int young_pool_size, int grey_list_size)
{
	m_allocator = alloc;
	
	s_handle_count = 0;	    // # of active handles
	s_handle_max_entry = 0; // the maximum handle entry occupied
	s_handle_free  = 0;	    // current free handle
	
	// make sure these are zeroed out
	memset(m_handles,      0, sizeof(Object *) * IGVM_MAX_HANDLES);
	memset(m_handles_meta, 0, sizeof(ig_u8)    * IGVM_MAX_HANDLES);

	// initialize the young allocator
	// this is where all new allocations are stored before being promoted
	// to the old generation
	m_young_allocator.init(alloc,  alloc->m_config->YOUNG_POOL_SIZE);
	
	// initialize the grey list
	// this is used for tri-color marking during gc
	m_grey_list.init		(alloc, 64 * 1024);
	m_static_roots.init		(alloc, 4 * 1024);
	m_dynamic_roots.init	(alloc, 1024);
	
	ig_u32 h;
	createHandle(NULL, GC_LEAF, &h);
	m_handles_meta[h] |= GC_MARK_BLACK;  // black
	setHandleAllocator(h, ALLOC_MALLOC);
	
	return 0;
}

/**
 * Free all memory used by the heap, then reinitialize it
 */

void IGVMHeap::reinit(void * user_p,
		void     ( * destructor)(void *, ig_u32 handle))
{
	//printf("IGVMHeap::reinit\n");
	
	// ignore entry '0' since that's reserved for NULL
	const int count = s_handle_max_entry;
	for (unsigned int i = 1; i < count; i--)
	{
		if ((m_handles_meta[i] & ALIVE_META_BIT) != 0		// alive bit is set
			   && m_handles[i] != NULL) {					// pointer is not null
			   
			destructor(user_p, i); 
			   
			// its using the MALLOC allocator   
			if (getHandleAllocator(i) == ALLOC_MALLOC) { 
				m_allocator->FREE(m_handles[i]);
			}  
		}	
	}

	s_handle_count = 0;	// # of active handles
	s_handle_free  = 0;	// current free handle
	
	s_handle_max_entry = 0;
	// make sure these are zeroed out

	//memset(m_handles_ref, 0,  sizeof(ig_i8) *IGVM_MAX_HANDLES);
	memset(m_handles_meta, 0, sizeof(ig_u8) *IGVM_MAX_HANDLES);
	memset(m_handles, 0,      sizeof(Object *) * IGVM_MAX_HANDLES);
	
	// clear out the young allocator
	m_young_allocator.clear();

	// clear out the grey list
	m_grey_list.clear();
	
	// clear out all the static roots (in program static variables)
	m_static_roots.clear();
	
	// clear out all the dynamic roots (user code roots)
	m_dynamic_roots.clear();
	
	// recreate the NULL handle (always slot 0)
	ig_u32 h;
	createHandle(NULL, GC_LEAF, &h);
	m_handles_meta[h] |= GC_MARK_BLACK;  // black
}


/**
 * Create a handle with a specific garbage collection type
 * 
 */

int IGVMHeap::createHandle(void * ptr, int gc_type, ig_u32 * out_handle)
{
	*out_handle = 0;
	if (s_handle_count >= IGVM_MAX_HANDLES) 
	{
		//fprintf(stderr, "Ran out of handles %d of %d\n", s_handle_count, IGVM_MAX_HANDLES);
		//dieAPainfullDeath("Ran out of handles");
		return 1;
	}

	// grab it out of the free list
	ig_u32 handle = s_handle_free;
	ig_i32 * next = (ig_i32 *)&m_handles[s_handle_free];

	// if a slot is 0 it points to the next slot (thus the +1)
	s_handle_free = handle + 1 + *next;
	s_handle_count ++;

	m_handles    [handle] = (Object * ) ptr;
	//m_handles_ref[handle] = 0;

	if (handle >= s_handle_max_entry) {
		s_handle_max_entry = handle + 1;
	}

	// A0P0TTMM
	// A  = alive bit
	// P = passed first gc  (default 0, set to 1 after first gc)
	//
	// MM = tri color mark bits
	//    00 = white
	//    01 = grey
	//    10 = undefined
	//    11 = black
	//
	// R = ref bits
	//     0 = has a ref count of 0,  1 = has a ref count > 0
	//
	// TT = type bits
	//    00 = leaf
	//    01 = object
	//    10 = array stride 4
	//    11 = array stride 8
	//
	m_handles_meta[handle] =
		0x00 |    				// white
		(gc_type << GC_TYPE_SHIFT) | 		// gc_type bits
		ALIVE_META_BIT;			// alive bit

	if (s_handle_count >= IGVM_MAX_HANDLES - 2) {
		fprintf(stderr, "potentially do a collection, because we're almost out of handles");
	}

	*out_handle = handle;
	return 0;
}

ig_u32 IGVMHeap::allocHandle(
			size_t instance_size, void * instance_defaults, int gc_type)
{
	// check to see if there is any pressure on the number of handles
	if (s_handle_count >= IGVM_MAX_HANDLES - 8) 
	{
		// SHOULD REQUEST A GC PASS
		//requestGC();
		
		m_allocator->GC();
		
		if (s_handle_count >= IGVM_MAX_HANDLES - 2)  {
			fprintf(stderr, "FATALITY\n");
			return 0;
		}
	}


	int  allocator = ALLOC_MALLOC;
	
	void * data = m_young_allocator.alloc(instance_size);
	if (data == NULL) {
		data = m_allocator->MALLOC(instance_size, __FILE__, __LINE__);
	}
	else {
		allocator = ALLOC_LINEAR;
	}
	
	
	if (instance_defaults != NULL) {
		memcpy(data, instance_defaults, instance_size);
	}
	
	ig_u32 handle;
	if (0 != this->createHandle(data, gc_type, &handle))
	{
		if (allocator == ALLOC_MALLOC) {
			m_allocator->FREE(data, instance_size);
		}
		
		fprintf(stderr, "FATALITY failed to create handle");
	}
	
	setHandleAllocator(handle, allocator);
	
	
	return handle;
}

ig_u32 IGVMHeap::allocArrayHandle(size_t instance_size, void * instance_defaults,  int count, int sz_per_entry, int gc_type)
{
	if (s_handle_count >= IGVM_MAX_HANDLES - 8) 
	{
		m_allocator->GC();
		// SHOULD REQUEST A GC PASS
		
		if (s_handle_count >= IGVM_MAX_HANDLES - 2)  {
			fprintf(stderr, "FATALITY\n");
			return 0;
		}
	}
	// note... this might be allocating objects slightly larger than we need
	//printf("allocating array sz: %d count: %d sz-per-entry: %d\n", instance_size, count, sz_per_entry);
	
	
	size_t array_total_size = instance_size  +
				sizeof(ig_i64) + 		// index size for alignment
				sz_per_entry * count;
			
	int allocator = ALLOC_MALLOC;			
	void * data = m_young_allocator.alloc(array_total_size);
	if (data == NULL) {
		data = m_allocator->MALLOC(array_total_size, __FILE__, __LINE__);
	}	
	else {
		allocator = ALLOC_LINEAR;
	}
		
	if (instance_defaults != NULL) {
		memcpy(data, instance_defaults, instance_size);
	}
	
	ObjectHeader * obj = (ObjectHeader *) data;
	
	// set the count on the array
	*((ig_i32*)obj->m_data) = count;
	// zero out all of the entries
	memset(&obj->m_data[1], 0, sz_per_entry * count);

	ig_u32 handle;
	if (0 != this->createHandle(obj, gc_type, &handle)) {
		if (allocator == ALLOC_MALLOC) {
			m_allocator->FREE(data, array_total_size);
		}
		
		fprintf(stderr, "FATALITY failed to create handle");
	}
	
	setHandleAllocator(handle, allocator);
	
	return handle;
}
			
ig_u32 IGVMHeap::reallocArray(ig_u32 handle, int sz_per_entry, int count)
{
	ObjectHeader * obj = (ObjectHeader *) m_handles[handle];
	
	int old_count = *((ig_i32*)obj->m_data);
	if (count < old_count) { old_count = count; }
	
	size_t new_size = sizeof(ObjectHeader) + count * sz_per_entry;
	size_t old_size = sizeof(ObjectHeader) + old_count * sz_per_entry;
	
	int allocator = getHandleAllocator(handle);
	
	
	ObjectHeader * new_obj = NULL;
	if (allocator == ALLOC_LINEAR) 
	{
		new_obj = (ObjectHeader *)m_young_allocator.realloc(obj, new_size);
		
		if (NULL != new_obj) 
		{
			// 0 out any new data at the end
			char * clear_ptr = ((char *)new_obj) + old_size ;
			memset(clear_ptr, 0,  new_size - old_size);
		}
	}
	
	if (new_obj == NULL)
	{
		
		
		new_obj = (ObjectHeader *) m_allocator->MALLOC(new_size, __FILE__, __LINE__);
		memcpy(new_obj, obj, sizeof(ObjectHeader) + old_count * sz_per_entry);
		
		// 0 out any new data at the end
		char * clear_ptr = ((char *)new_obj) + old_size ;
		memset(clear_ptr, 0,  new_size - old_size);
		
		if (allocator == ALLOC_MALLOC) {
			m_allocator->FREE(obj, old_size);
		}
		
		allocator = ALLOC_MALLOC;
	}
	
	*((ig_i32*)new_obj->m_data) = count;
	m_handles[handle] = (Object *) new_obj;
	
	setHandleAllocator(handle, allocator);
	

	return handle;
}

void IGVMHeap::freeHandle(ig_u32 handle)
{
	if (m_handles_meta[handle] == 0 ||
		m_handles[handle] == NULL) {
		dieAPainfullDeath("freeing a dead handle. What gives?");	
	}
	
	// the data at the specified handle SHOULD NEVER BE NULL
	// except in the case its handle 0, which SHOULD NEVER BE FREED
	if (getHandleAllocator(handle) == ALLOC_MALLOC) {
		m_allocator->FREE(m_handles[handle]);
	}

	// clear out all of the data pertaining to the handle
	//m_handles_ref[handle]  = 0;     // number of referrers  (likely overkill since very few objects will be rots)
	m_handles_meta[handle] = 0;	    // tags regarding the characteristics of the memory
	m_handles[handle]      = NULL;	// a raw pointer

	// Insert back into the free list
	{
		ig_i32 * next = (ig_i32 *)&m_handles[handle];
		*next = s_handle_free - handle - 1;

		s_handle_count --;
		s_handle_free = handle;
	}
}


/**
 * This is used for greedily marking the active stack.
 * No error is thrown if the handle isn't valid.
 */

void IGVMHeap::whiteToGreyIfValid(ig_u32 handle)
{
	// not withing range [0..IGVM_MAX_HANDLES)
	if (0 == handle || handle >= IGVM_MAX_HANDLES) {
		return;
	}

	// handle isn't marked as being active
	if ((m_handles_meta[handle] & ALIVE_META_BIT) == 0) {
		return;
	}	

	whiteToGrey(handle);
}


static int s_gc_white_to_grey_lut [16] = {
		0x03, 0x00, 0x00, 0x00,		// LEAF
		0x01, 0x00, 0x00, 0x00,		// OBJ
		0x01, 0x00, 0x00, 0x00,		// OBJ
		0x01, 0x00, 0x00, 0x00,		// OBJ
	};

void IGVMHeap::whiteToGrey(ig_u32 handle) 
{
	// grab the type and mark bits
	// 0000TTMM	
	int low   = m_handles_meta[handle] & 0xf;
	int to_or = s_gc_white_to_grey_lut[low];
	// yields a value to or to it for 000000MM
	if (to_or == 0x01) {
		m_grey_list.push(handle);
	}
	m_handles_meta[handle] |= to_or;
	
	/*
	// verify that the handle is currently white = 0x00
	if ((m_handles_meta[handle] & GC_MARK_MASK) != 0) {
		return;
	}
	
	// if the handle represents a leaf, auto promote to black
	// as it has no internal pointers to scan
	int meta = m_handles_meta[handle];
	if ((meta & GC_TYPE_MASK) == 0)
	{
		m_handles_meta[handle] = meta | GC_MARK_BLACK;
	}
	// its not a leaf make grey
	else
	{
		m_handles_meta[handle] = meta | GC_MARK_GREY;
		m_grey_list.push(handle);
	}
	*/
}


void IGVMHeap::assigned(ig_u32 obj_handle, ig_u32 handle)
{
	if (handle == 0) {
		return;
	}
	
	// only mark the handle if the obj_handle is BLACK or GREY
	if (0 == (m_handles_meta[obj_handle] & 0x0f)) {
		return;
	}
	
	int low   = m_handles_meta[handle] & 0xf;
	int to_or = s_gc_white_to_grey_lut[low];
	// yields a value to or to it for 000000MM
	if (to_or == 0x01) {
		m_grey_list.push(handle);
	}
	m_handles_meta[handle] |= to_or;
	
	
	
	//if ((m_heap.m_handles_meta[sp[1].u32] & 0x3) == 0 && 
	//	(m_heap.m_handles_meta[sp[0].u32] & 0x3) != 0) {
	//	m_heap.whiteToGrey(sp[1].u32);
	//}

}

 void IGVMHeap::toGreyOrBlack(ig_u32 handle) 
 {
 	// verify that this isnt the NULL handle
	if (handle == 0) { return; }
	
	int low   = m_handles_meta[handle] & 0xf;
	int to_or = s_gc_white_to_grey_lut[low];
	// yields a value to or to it for 000000MM
	if (to_or == 0x01) {
		m_grey_list.push(handle);
	}
	m_handles_meta[handle] |= to_or;
	
	/*
	
	// verify that its currently white 0b00 
	if ((m_handles_meta[handle] & GC_MARK_MASK) != 0) {
		return;
	}
	
	// it's a leaf auto promote to black, since it does not have any children to mark
	if ((m_handles_meta[handle] & GC_TYPE_MASK) == 0)
	{
		// mark as black
		m_handles_meta[handle] |= GC_MARK_BLACK;
	}
	// otherwise mark as grey and place on the grey list
	else
	{
		// mark as grey
		m_handles_meta[handle] |= GC_MARK_GREY;
		m_grey_list.push(handle);
	}
	*/	
}


/**
 *  Garbage collection for the Heap
 *  The heap currently maintains a 
 *  1) young pool (linear allocator)
 *  2) object pool
 *
 *  Tri-color marking is used for standard garbage collection
 *  We need to come up with a good tradeoff for 
 *  memory vs CPU vs 
 *  CPU/Power ideal is never GC
 *  Memory ideal is always GC
 *  Rules
 *  1) perform a FULL(ish) GC every X seconds
 *  2) do not start GC until over a specific limit.
 */



void IGVMHeap::gc(bool     stop_the_world, 
				  void * user_p, 
				  ig_i16 * ( * object_handle_offsets)(void *, void * ptr),
				  void     ( * destructor)(void *, ig_u32 handle))
{

	int handles_dealloced = 0;
	
	//fprintf(stderr, ">>>> gc head  grey-size: %d handles: %d max-handles: %d ", (int) m_grey_list.m_count, s_handle_count, IGVM_MAX_HANDLES);
	
	
	// woah now... gotta watch out
	if (s_handle_count >= IGVM_MAX_HANDLES / 2) {
		stop_the_world = true;
		fprintf(stderr, ">>>>> GC STOP THE WORLD\n");
	}

	
	
	int MAX_TICKS_TILL_FULL_GC = (60 * 30);
	int MIN_GC_ITERATIONS      = 10;
	int MEMORY_THRESHOLD_0	   = 16 * 1024 * 1024;	
	
	// desire a FULL gc every 30 seconds or so
	int    ideal_iterations = s_handle_count / MAX_TICKS_TILL_FULL_GC;
	
	if (ideal_iterations < MIN_GC_ITERATIONS) {
		ideal_iterations = MIN_GC_ITERATIONS;
	}
	
	
	//ideal_iterations = 1000;
	
	//stop_the_world = true;
	
	// allow the user to do a full gc at will
	int    iterations = stop_the_world ? IGVM_MAX_HANDLES  : ideal_iterations;
	ig_u32 handle = 0;
	while ((stop_the_world || iterations > 0) && m_grey_list.pop(&handle))
	{
		// blacken the object
		m_handles_meta[handle] |= GC_MARK_BLACK;
		iterations --;
		
		switch (GC_TYPE_BITS(m_handles_meta[handle]))
		{
			case GC_OBJECT:
			{
				ObjectHeader * obj = (ObjectHeader *) m_handles[handle];
				
				// retrieve the list of offsets that have handles
				// each offset is in a # of bytes
				ig_i16 * gc_offsets = object_handle_offsets(user_p, obj);
				
				const char * obj_data = (char *)obj;
				while (*gc_offsets != -1)
				{
					int byte_offset = *gc_offsets;  
	
					ig_u32 obj_handle = *(ig_u32 *)(obj_data + byte_offset);
					
					toGreyOrBlack(obj_handle);
					
					gc_offsets++;
				}
			
				break;
			}
			/*
			 * An Array of handles
			 *
			 * i32: count, i32: padding,
			 * u32: handles x count
			 */
			case GC_OBJECT_ARRAY_STRIDE_4:
			{
				// arrays of straight up objects
				ObjectHeader * obj = (ObjectHeader *) m_handles[handle];
				const int count    = *((ig_i32 *)(obj->m_data + 0));
				ig_u32 * ptr       =  ((ig_u32 *)(obj->m_data + 1));

				for (int k = 0; k < count; k++) {
					toGreyOrBlack(ptr[k]);
				}
				break;
			}
			/*
			 * An Array of stridedhandles
			 *
			 * i32: count, i32: padding,
			 * (u32: handles, u32 : data) x count
			 */
			case GC_OBJECT_ARRAY_STRIDE_8:
			{
				// this will rarely ever occur.  The only time this occurs is with
				// arrays of function pointers. The first u32 of each u64 COULD contain a pointer or 0 (NULL)
				
				ObjectHeader * obj = (ObjectHeader *) m_handles[handle];
				const int count    = *((ig_i32 *)(obj->m_data + 0)) * 2;
				ig_u32 * ptr       =  ((ig_u32 *)(obj->m_data + 1));

				//for (int k = (count - 1) * 2; k >= 0; k -= 2) {
				for (int k = 0; k < count; k += 2) {
					toGreyOrBlack(ptr[k]);
				}
				break;
			}
		}
		
		
	}

	//fprintf(stderr, "\tgc phase 2\n");

	//bool gc_completed = false;
	if (m_grey_list.m_count == 0) 
	{
		//gc_completed = true;
		for (ig_u32 handle = s_handle_max_entry - 1; handle > 0; handle--) 
		{		
			// if the handle isn't alive skip it
			if ((m_handles_meta[handle] & ALIVE_META_BIT) == 0) {
				continue;
			}
			
			// if the handle is still marked as white delete it
			if ((m_handles_meta[handle] & GC_MARK_MASK) == 0)
			{
				handles_dealloced ++;
				
				// invoke the destructor if there is one
				destructor(user_p, handle);
				
				
				
				// !!!!!!!!
				// likely need to encode a table of reference offsets or something?
				
				// this is the only spot that freeHandle is called
				freeHandle(handle);
			}
			// handle is black
			else 
			{
				// promote objects from young generation to old generation
				// we'll be clearing out the young allocator once this is complete
				if (getHandleAllocator(handle) == ALLOC_LINEAR)
				{	
					if (m_handles[handle] != NULL) 
					{
						size_t sz = m_young_allocator.getSize(m_handles[handle]);
						m_handles[handle] = (Object *) m_allocator->DUP(m_handles[handle], sz, __FILE__, __LINE__);
					
						//if (m_handles[handle] != NULL) {
						//m_handles[handle] = (Object *)  m_young_allocator.instance(m_handles[handle]);
					}
					//m_handles_meta[handle] |= PASSED_GC_BIT;
					setHandleAllocator(handle, ALLOC_MALLOC);
				}
				
				
				// reset this handle to white
				m_handles_meta[handle] &= ~GC_MARK_MASK;
			}
		}
		
		
		// clear the contents of the young allocator
		m_young_allocator.clear();
		
		// initialize the grey list by  scan through all static roots
		for (unsigned int i = 0; i < m_static_roots.m_count; i++) 
		{
			ig_u32 handle = m_static_roots.m_handles[i].u32;
			
			// if the handle isn't alive skip it
			if ((m_handles_meta[handle] & ALIVE_META_BIT) == 0 || handle == 0) {
				continue;
			}
			
			// if the handle is still marked as white transition
			// it to grey or black
			if ((m_handles_meta[handle] & GC_MARK_MASK) == 0)
			{
				// if its a leaf automatically mark as black
				if (0 == (GC_TYPE_MASK & m_handles_meta[handle]))
				{
					// mark as black
					m_handles_meta[handle] |= GC_MARK_BLACK;
				}
				// otherwise mark as grey to be examined
				else
				{
					// mark as grey
					m_handles_meta[handle] |= GC_MARK_GREY;
					m_grey_list.push(handle);
				}
			}
		}
		
		
	}
	
	//printf(">>>GC COMPLETE (grey-size: %d, dealloced %d, static-roots: %d)\n",m_grey_list.m_count, handles_dealloced, m_static_roots.m_count);
}
}


