/*
Copyright (c) 2014-2017 Incubator Games Ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#ifndef IGVMInternal_H
#define IGVMInternal_H 1

#include "IGVM.h"




// ig_u8 = A0P0TTMM
//
// MM = tri color mark bits
//    00 = white
//    01 = grey
//    10 = UNDEFINED (this was done so that coloring could be achieved by a simple bitwise OR)
//    11 = black

// TT = type bits
//    00 = leaf           (object contains no internal handles)
//    01 = object         (object contains a table specifying locations of handles)
//    10 = array stride 4 (array with handles every 4 bytes starting after the length field)
//    11 = array stride 8 (array with handles every 8 bytes starting after the length field)
//
// P = passed first gc  (default 0, set to 1 after first gc)
//     THIS SIGNIFIES THAT IT WAS PROMOTED FROM THE YOUNG POOL
//
// A  = alive bit (is this handle considered alive)


// maximum number of handles supported at a given time
// on a 64bit system this'll take up (8+1) MB of memory JUST FOR THESE TABLES
#define IGVM_MAX_HANDLES  (1024 * 1024)

#define PASSED_GC_BIT   (0x20)
#define ALIVE_META_BIT  (0x80)



// get the ref bit (1 = this object has external references)
//define GC_REF_BITS(x)   (((x) >> 2) & 1)

////////////////////////////////////
// TYPE BITS
///////////////////////////////////

#define GC_TYPE_SHIFT   2

// get the type bits ... this describes the makeup of the object.. leaf, object, array8, array16
#define GC_TYPE_BITS(x)  (((x) >> GC_TYPE_SHIFT) & 3)

// mask out the gc type bits
#define GC_TYPE_MASK     (3 << GC_TYPE_SHIFT)


// GC_LEAF - doesn't contain any handles within
#define GC_LEAF                  (0)

// GC_OBJECT - does contain handles within
#define GC_OBJECT                (1)

// GC_OBJECT_ARRAY_STRIDE_4 - is an array with a 32 bit handle every 4 bytes
#define GC_OBJECT_ARRAY_STRIDE_4 (2)

// GC_OBJECT_ARRAY_STRIDE_8 - is an array with a 32 bit handle every 8 bytes
#define GC_OBJECT_ARRAY_STRIDE_8 (3)

//////////////////////////////////
// MARK BITS
/////////////////////////////////

// white,grey,black bits
#define GC_MARK_BITS(x)  ((x) & 3)
#define GC_MARK_SHIFT     0
#define GC_MARK_MASK     (3 << GC_MARK_SHIFT)
#define GC_MARK_WHITE     0
#define GC_MARK_GREY	  1
#define GC_MARK_BLACK     3



namespace igvm
{

 void dieAPainfullDeath(const char * str);

/**
 * Compact allocator objects for use 
 */	
struct VMAllocator	
{
public:
	Config     * m_config;
	VMInstance * m_instance;
	void        (* m_gc_fn) (VMInstance *);

	// keep track of roughly how much memory has been allocated
	size_t       m_allocated;

	inline void * MALLOC(size_t sz, const char * file, int line) {
		m_allocated += sz;
		return m_config->_malloc(m_instance, sz);
	}
	
	inline void * DUP(void * ptr, size_t sz, const char * file, int line) {
		m_allocated += sz;
		void * tmp =  m_config->_malloc(m_instance, sz);
		memcpy(tmp, ptr, sz);
		return tmp;
	}

	inline void   FREE(void * ptr, size_t sz = 0) {
		m_allocated -= sz;
		return m_config->_free(m_instance, ptr);
	}

	inline void *   RESIZE(void * old_ptr, size_t old_sz, size_t new_sz, const char * file, int line) 
	{
		m_allocated -= old_sz;
		m_allocated += new_sz;
		
		void * new_ptr = m_config->_malloc(m_instance, new_sz);
		memcpy(new_ptr, old_ptr, old_sz);
		m_config->_free(m_instance, old_ptr);
		return new_ptr;
	}
	
	inline void GC() { 
		//fprintf(stderr, "GC Called\n");
		m_gc_fn(m_instance); 
	}
};
		

struct InOp
{
	// right now there are 130 valid ops that are output by igc

	// tags
	static const int I8			= 1;
	static const int I16        = 2;
	static const int I32        = 4;
	static const int I64        = 8;
	static const int I8_ARGS    = 0x11;
	static const int I8_JUMP    = 0x31;


	static const int I16_PAIR   = 0x12;
	static const int I16_STRING = 0x22;
	static const int I16_JUMP   = 0x32;
	static const int I16_FUNCTION_OFFSET = 0x42;
	
	static const int I8_MEMBER_VARIABLE_OFFSET = 0x51;
	static const int I16_MEMBER_VARIABLE_OFFSET = 0x52;
	static const int I16_STATIC_VARIABLE_OFFSET = 0x62;
	static const int I8_LOCAL  = 0x71;
	static const int I16_LOCAL = 0x72;
	
	// opcodes
	static const int NOP     = 0;
	static const int POP     = 1;
	static const int DUP     = 2;
	static const int DUP1X   = 3;
	static const int DUP2X   = 4;
	static const int RET0    = 5;
	static const int RET1    = 6;
	static const int DUP2    = 7;
	static const int ADD_I32 = 8;
	static const int SUB_I32 = 9;
	static const int MUL_I32 = 10;
	static const int DIV_I32 = 11;
	static const int MOD_I32 = 12;
	static const int ADD_F64 = 13;
	static const int SUB_F64 = 14;
	static const int MUL_F64 = 15;
	static const int DIV_F64 = 16;
	static const int MOD_F64 = 17;
	static const int AND_I32 = 18;
	static const int OR_I32  = 19;
	static const int XOR_I32 = 20;
	static const int ASR_I32 = 21;
	static const int LSR_I32 = 22;
	static const int LSL_I32 = 23;
	static const int CMP_EQ_I32 = 24;
	static const int CMP_LT_I32 = 25;
	static const int CMP_LE_I32 = 26;
	static const int CMP_EQ_F64 = 27;
	static const int CMP_LT_F64 = 28;
	static const int CMP_LE_F64 = 29;
	static const int CMP_EQ_I64 = 30;
	static const int BIT_NOT_I32 = 31;
	static const int NOT = 32;
	static const int I32_TO_F64 = 33;
	static const int F64_TO_I32 = 34;
	static const int PUSH_64 = 35;
	static const int PUSH_32 = 36;
	static const int LOCAL_LOAD = 37;
	static const int LOCAL_STORE = 38;
	static const int MEMBER_LOAD = 39;
	static const int MEMBER_STORE = 40;
	static const int STATIC_LOAD = 41;
	static const int STATIC_STORE = 42;
	static const int THIS_LOAD = 43;
	static const int THIS_STORE = 44;
	static const int LOCAL_LOAD_2 = 45;
	static const int LOCAL_CLEAR = 46;
	static const int PUSH_STRING = 47;
	static const int LOCAL_STORE_KEEP = 48;
	static const int CALL_INTERFACE = 49;
	static const int FUNCPTR_MEMBER = 50;
	static const int FUNCPTR_STATIC = 51;
	static const int CALL_VTABLE = 52;
	static const int CALL_STATIC = 53;
	static const int CALL_DYNAMIC = 54;
	static const int NEW = 55;
	static const int THROW = 56;
	static const int JMP = 57;
	static const int J0_I32 = 58;
	static const int JN0_I32 = 59;
	static const int CAST_TO = 60;
	static const int NEXT_IT = 61;
	static const int CALL_STATIC_VTABLE = 62;
	static const int RET_FUNCPTR = 63;
	static const int RET_OBJ = 64;
	static const int I_MEMBER_STORE_FUNCPTR = 65;		// NOT OUTPUT (aug 31 2016)
	static const int I_MEMBER_STORE_OBJ = 66;			// NOT OUTPUT (aug 31 2016)
	static const int I_THIS_STORE_FUNCPTR = 67;		// NOT OUTPUT (aug 31 2016)
	static const int I_THIS_STORE_OBJ = 68;			// NOT OUTPUT (aug 31 2016)
	static const int I_STATIC_STORE_FUNCPTR = 69;		// NOT OUTPUT (aug 31 2016)
	static const int I_STATIC_STORE_OBJ = 70;			// NOT OUTPUT (aug 31 2016)
	static const int STATIC_LOAD_FUNCPTR = 71;		// NOT OUTPUT (aug 31 2016)
	static const int STATIC_LOAD_OBJ = 72;			// NOT OUTPUT (aug 31 2016)
	static const int ARRAY_LOAD_8 = 73;
	static const int ARRAY_LOAD_16 = 74;
	static const int ARRAY_LOAD_32 = 75;
	static const int ARRAY_LOAD_64 = 76;
	static const int ARRAY_LOAD_OBJ = 77;
	static const int ARRAY_LOAD_FUNCPTR = 78;
	static const int ARRAY_STORE_8 = 79;
	static const int ARRAY_STORE_16 = 80;
	static const int ARRAY_STORE_32 = 81;
	static const int ARRAY_STORE_64 = 82;
	static const int ARRAY_STORE_OBJ = 83;
	static const int ARRAY_STORE_FUNCPTR = 84;
	static const int ARRAY_NEW = 85;
	static const int ARRAY_LENGTH = 86;
	static const int INSTANCE_OF = 87;
	static const int STACK_TRACE = 88;				// NOT OUTPUT (aug 31 2016)
	static const int LOCAL_INC_I32 = 89;
	static const int STACK_INC_I32 = 90;
	static const int CALL_RET1_INTERFACE     = 91;
	static const int CALL_RET1_VTABLE        = 92;
	static const int CALL_RET1_STATIC        = 93;
	static const int CALL_RET1_STATIC_VTABLE = 94;
	static const int CALL_RET1_DYNAMIC       = 95;
	static const int PUSH_I8 = 96;
	static const int CATCH_ENTRY = 97;
	static const int ARRAY_RESIZE = 98;
	static const int ARRAY_FILL = 99;
	// these should no longer be emitted (this should just be a vm optimization)
	static const int LL0 = 100;
	static const int LL1 = 101;
	static const int LL2 = 102;
	static const int LL3 = 103;
	static const int LL4 = 104;
	// end of note
	static const int JEQ_I32 = 105;
	static const int ASR_I32_CONST = 106;
	static const int LSR_I32_CONST = 107;
	static const int LSL_I32_CONST = 108;		
	static const int JMP_IN_I32_0RANGE = 109;		// does this ever exist
	static const int AND = 110;
	static const int OR  = 111;

	static const int THIS_STORE_32_REF   = 112;	// NOT OUTPUT (aug 31 2016)
	static const int MEMBER_STORE_32_REF = 113; // NOT OUTPUT (aug 31 2016)
	static const int ARRAY_STORE_32_REF  = 114;
	static const int STATIC_STORE_32_REF = 115; // NOT OUTPUT (aug 31 2016)

	static const int THIS_LOAD_32		 = 116;// NOT OUTPUT (aug 31 2016)
	static const int MEMBER_LOAD_32		 = 117;// NOT OUTPUT (aug 31 2016)
	static const int THIS_STORE_32		 = 118;// NOT OUTPUT (aug 31 2016)
	static const int MEMBER_STORE_32     = 119;// NOT OUTPUT (aug 31 2016)

	static const int MLA_I32             = 120;
	static const int MLA_F64             = 121;
	static const int LSL_ASR_I32_CONST   = 122;
	static const int SWITCH_I32_JMP      = 123;
	static const int I32_0				 = 124;
	static const int I32_1               = 125;
	static const int ARRAY_COPY          = 126;
	static const int DEBUG_BLOCK_START   = 127;
	static const int DEBUG_BLOCK_END     = 128;
	static const int PUSH_I16            = 129;
	static const int LOCAL_LOAD_LO       = 130;	    // should be VM optimization
	static const int LOCAL_STORE_LO       = 131;	// should be VM optimization
	static const int PUSH_F64_0			  = 132;
	static const int PUSH_F64_1			  = 133;
	static const int THIS_INC_I32		  = 134;
	static const int LOAD_OBJ_FIELD       = 135;
	static const int LOAD_OBJ_FIELD_32    = 136;
	
	
	// 192 and on will be forever reserved for internal cohesive use
	//
	// [192,200) will me the remap of LL
	// [200,208) will be the map of LS0 => 7
	
	
	static const int I_LL0  = 192;
	static const int I_LL1  = 193;
	static const int I_LL2  = 194;
	static const int I_LL3  = 195;
	static const int I_LL4  = 196;
	static const int I_LL5  = 197;
	static const int I_LL6  = 198;
	static const int I_LL7  = 199;
		
	static const int I_LS0	= 200;
	static const int I_LS1	= 201;
	static const int I_LS2	= 202;
	static const int I_LS3	= 203;
	static const int I_LS4	= 204;
	static const int I_LS5	= 205;
	static const int I_LS6	= 206;
	static const int I_LS7	= 207;
	
	// 208
	// 216
	// 224
	// 232
	// 240

	
	static const int INTERNAL_ARRAY_COMPARE       = 241;
	static const int INTERNAL_VECTOR_32_GET       = 242;
	static const int INTERNAL_VECTOR_OBJECT_GET   = 243;
	static const int INTERNAL_VECTOR_LENGTH       = 244;
	static const int INTERNAL_JMP_LO			  = 245;
	static const int INTERNAL_THIS_LOAD_LO	      = 246;
	static const int INTERNAL_THIS_LOAD_32_LO	  = 247;	
	static const int INTERNAL_MEMBER_LOAD_LO	  = 248;
	static const int INTERNAL_MEMBER_LOAD_32_LO	  = 249;	
	static const int INTERNAL_LOCAL_CLEAR_LO	  = 250;
	
	static const int INVOKE_NATIVE        = 251;	// NOT OUTPUT (aug 31 2016)
	static const int CALL_RET1_SUPER = 252;
	//static const int CREATE_IT = 253;
	static const int CALL_SUPER = 254;
	static const int NOP_TERM = 255;
};


struct Object;
struct VTable;

struct ObjectHeader
{
	union {
		VTable * m_vtable;
		ig_i64   padding;
	};
	
	// used purely to force algnment, and as a minium size
	// internal code relies on this being at least 8 bytes 
	ig_i64   m_data[1];
};



/**
 * Implementation of a resizeable stack
 */

template <class T>
struct VMStack
{
	T * m_handles;
	size_t        m_count;
	size_t        m_capacity;
	VMAllocator * m_allocator;
	
	
	/**
	 * Initialize the stack with an allocator and an initial capacity
	 */
	
	void init(VMAllocator * alloc, int capacity) {
		m_allocator = alloc;
		m_handles  = (T *) m_allocator->MALLOC(sizeof(T) * capacity, __FILE__, __LINE__);
		m_capacity = capacity;
		m_count    = 0;
	}
	
	
	/**
	 * Clear out all entries used by the stack
	 */
	
	inline void clear() {
		m_count = 0;
	}
	
	/**
	 * Free all the memory used by the stack
	 */
	
	inline void destroy() {
		m_allocator->FREE(m_handles, sizeof(T) * m_capacity);
		memset(this, 0, sizeof(VMStack<T>));
	}
	
	/**
	 * Add an item to the stack
	 */

	inline void push(const T & entry) 
	{
		if (m_count == m_capacity) {
			size_t   new_cap  = (m_capacity * 3) / 2;
			
			m_handles  = (T *) m_allocator->RESIZE(m_handles, sizeof(T) * m_count, sizeof(T) * new_cap, __FILE__, __LINE__);
			m_capacity = new_cap;
		}
		
		m_handles[m_count] = entry;
		m_count++;
	}
	
	/**
	 * Remove an item from the stack
	 * @returns true if an item existed to be popped
	 */
	
	inline bool pop(T * entry) {
		if (m_count <= 0) { return false; }
		m_count--;
		*entry = m_handles[m_count];
		return true;
	}
};


////////////////////////////////////////////////////////////
// VMArraySet is a stack that only allows a single entry to
// be present once.  It uses s_config for memory management

template<class T>
class VMArraySet
{
	T * m_items;
	int m_count;
	int m_capacity;
	VMAllocator * m_allocator;
	
public:
	inline void init(VMAllocator * alloc, int initial_capacity) {
		m_allocator = alloc;
		m_items = (T*)m_allocator->MALLOC(sizeof(T) * initial_capacity, __FILE__, __LINE__);
		m_count = 0;
		m_capacity = initial_capacity;
	}

	inline void destroy() {
		m_allocator->FREE(m_items, sizeof(T) * m_capacity);
		m_items = NULL;
		m_count = 0;
		m_capacity = 0;
	}

	inline void resize(int new_capacity)
	{
	
		m_items    = (T*) m_allocator->RESIZE(m_items, 
					m_count * sizeof(T), sizeof(T) * new_capacity, __FILE__, __LINE__);
		m_capacity = new_capacity;
		
		
		/*
		T* to_delete = m_items;
		T* tmp = (T*) IGVM_MALLOC(sizeof(T) * new_capacity);

		memcpy(tmp, m_items, m_count * sizeof(T));
		m_capacity = new_capacity;
		m_items = tmp;

		IGVM_FREE(to_delete);
		*/
	}

	inline void clear() {
		m_count = 0;
	}

	inline bool dequeue(T * ret) {
		if (m_count == 0) { return false; }
		m_count --;
		*ret = m_items[0];
		// shuffle over all of the items
		for (int i = 0; i < m_count; i++) {
			m_items[i] = m_items[i+1];
		}
		return true;
	}


	inline bool pop(T * ret) {
		if (m_count == 0) { return false; }
		m_count --;
		*ret = m_items[m_count];
		return true;
	}
	
	/**
	 * Insert an item at the end of the list as long as it
	 * isn't already a member
	 */

	inline void push(T item) {
		for (int i = 0; i < m_count; i++) {
			if (m_items[i] == item) {
				return;
			}
		}

		if (m_count == m_capacity) {
			resize(m_capacity * 1.5);
		}

		m_items[m_count] = item;
		m_count++;
	}
};

// HEAP


template <class T>
class HEAP
{
	// currently this'll have a degenerate case if allocations
	// go as follows  4,8,4,8,4,8,4,8 .. this'll was 25% of our memory
public:
	T * m_data;
	int m_count;
	int m_capacity;
	int m_capacity_max;		// TODO implement
	VMAllocator * m_allocator;

	inline void init(VMAllocator * alloc, int cap, int max_cap = 0x7FFFFFFF) {
		m_allocator = alloc;
		m_data = (T *) m_allocator->MALLOC(sizeof(T) * cap, __FILE__, __LINE__);
		m_capacity = cap;
		m_count = 0;
		m_capacity_max = max_cap;
	}
	
	inline void destroy() {
		m_allocator->FREE(m_data, sizeof(T) * m_capacity);
		m_data = NULL;
		m_count = 0;
		m_capacity = 0;
		m_capacity_max = 0;
	}

	/**
	 * remove all elements of the heap.  Doesn't delete backing store
	 */
	inline void clear() {
		m_count = 0;
	}

	/**
	 * access the Nth element of the heap
	 */
	inline T & operator[](int idx) { 
		return m_data[idx]; 
	}

	/**
	 * get a pointer to the Nth element of the heap
	 */
	 
	inline T * getPointer(int offset) {
		return &m_data[offset];
	}

	/**
	 * get the number of elements in the heap
	 */
	inline int getCount() { return m_count; }

	/*
	 *
	 */

	inline void resize(int new_capacity)
	{
		m_data = (T *) m_allocator->RESIZE(m_data, sizeof(T) *  m_count, sizeof(T) * new_capacity, __FILE__, __LINE__);
		m_capacity = new_capacity;
		//printf("HEAP resize\n");

		//T* to_delete = m_data;
		//T* tmp = (T *) IGVM_MALLOC(sizeof(T) * new_capacity);

		//memcpy(tmp, m_data, sizeof(T) *  m_count);
		//m_capacity = new_capacity;
		//m_data = tmp;

		//IGVM_FREE(to_delete);
	}

	/* Allocate a chunk of memory with a specified stride
	 */
	 


	inline int alloc(int byte_count)
	{
		int idx = m_count;
		m_count += byte_count;
		if (m_count >= m_capacity) {
			// what if m_count > capacity
			int new_cap = m_capacity * 1.5;
			if (new_cap < m_count) {
				new_cap = m_count;
			}
			resize(new_cap);
		}
		return idx;
	}

	inline int allocWithStride(int byte_count, int stride)
	{
		if (stride > 1)
		{
			while ((m_count % stride) != 0) {
				m_count ++;
			}
		}
		
		return alloc(byte_count);
	}

	inline ig_i64 & i64(int offset) {
		char * ptr = (char *) &m_data[offset];
		return *(ig_i64 *)ptr;
	}

	inline ig_i32 & i32(int offset) {
		char * ptr = (char *) &m_data[offset];
		return *(ig_i32 *)ptr;
	}
	
};



/*
 * This is essentially an object pool, that allocates blocks of records
 * instead of individual records.
 * the following condition must be held: sizeof(T) >= sizeof(void *)
 */
 
template <class T, int N>
struct CHUNK_POOL_PAGE
{
	CHUNK_POOL_PAGE<T,N> * m_next;
	T                      m_data[N];
}; 
 
template <class T, int N>
class CHUNK_POOL
{
public:
	CHUNK_POOL_PAGE<T,N> * m_allocations;
	T * m_free;			// the free list
	//int m_alloc_chunk;	// the number of items in each chunk
	VMAllocator * m_allocator;
	
	
	inline void init(VMAllocator * alloc) {
		m_allocator = alloc;
		m_free        = NULL;
		//m_alloc_chunk = N;
	}
	
	inline void destroy() {
		CHUNK_POOL_PAGE<T,N> * head = m_allocations;
		while (head != NULL) {
			CHUNK_POOL_PAGE<T,N> * to_delete = head;
			head = head->m_next;
			
			m_allocator->FREE(to_delete, sizeof(CHUNK_POOL_PAGE<T,N>));
		}
		
		m_free = NULL;
		//m_alloc_chunk = 0;
	}

	/**
	 *Allocate a chunk of size sizeof(T)
	 **/

	inline T * alloc() 
	{
		if (m_free == NULL)
		{
			CHUNK_POOL_PAGE<T,N> * a = (CHUNK_POOL_PAGE<T,N> *) m_allocator->MALLOC( sizeof(CHUNK_POOL_PAGE<T,N>), __FILE__, __LINE__);
			a->m_next = m_allocations;
			m_allocations = a;
			
			
			T * new_block = (T *) &a->m_data;  // IGVM_MALLOC(sizeof(T) * m_alloc_chunk);
			for (int i = 0; i < N; i++) {
				T ** next = (T **) &new_block[i];
				*next = m_free;
				m_free = &new_block[i];
			}
		}

		T   * ret = m_free;
		T ** next = (T**) m_free;
		m_free    = *next;

		return ret;
	}
	
	/**
	 * Release a chunk of sizeof(T)
	 **/

	inline void dealloc(T * obj) {
		T ** next = (T **) obj;
		*next = m_free;
		m_free = obj;
	}
};


//////////////////////////////////////////////
// Tracker
// - as 0..N slots which can be incremented

struct VMTracker
{
	ig_u32  m_count;
	int *   m_data;
	VMAllocator * m_allocator;
	//VMTracker() {
		//m_count    = 0;
	//	m_data = NULL;
	//}
	
	inline void init(VMAllocator * alloc) {
		m_allocator = alloc;
		m_data = (int *) m_allocator->MALLOC(sizeof(int) * 10, __FILE__, __LINE__ );
		memset(m_data, 0, sizeof(int) * 10);
		m_count = 10;
	}
	
	inline void inc(ig_u32 idx) 
	{

		if (m_count <= idx) 
		{
			//int   old_count = m_count;
			int * old = m_data;
			m_data = (int *)  m_allocator->MALLOC(sizeof(int) * (idx + 1), __FILE__, __LINE__);
			memset(m_data, 0, sizeof(int) * (idx + 1));
			memcpy(m_data, old, sizeof(int) * m_count);
			m_count = idx + 1;
			
			m_allocator->FREE(old, sizeof(int) * m_count);
		}
		
		m_data[idx] ++;
	}
	
	inline int operator[](ig_u32 idx) {
		if (idx >= m_count) { return 0; }
		return m_data[idx];
	}
	
	inline void clear()
	{
		memset(m_data, 0, sizeof(int) * m_count);
	}
};



/////////////////////////
// Linear allocator for use by the Heap
///////////////////////////

struct VMLinearAllocatorEntry
{
	union {
		size_t sz;
		ig_i64 padding;	
	};
};

struct VMLinearAllocator
{
	VMAllocator * m_allocator;  
	size_t        m_memory_allocated;
	
	size_t m_count;
	size_t m_capacity;
	char * m_data;
	void * m_last_ptr;
	
	//VMLinearAllocator();
	
	
	void init(VMAllocator * alloc, size_t cap);
	
	/**
	 * Clear out all data stored in this allocator
	 */
	
	
	void clear();
	
	/**
	 * 0 out the allocator and free all memory owned by it
	 * This object will no longer be usable at this point
	 */
	
	void destroy();
	
	/**
	 * Allocate a block of memory of size 'sz'  
	 * If the chunk of memory cannot fit within the linear allocator NULL is returned
	 */
	void * alloc(size_t sz);
	
	/**
	 * Attempt to realloc the block.  err resize.
	 * If it can't be resized NULL is returned
	 */
	void * realloc(void * ptr, size_t new_sz);
	
	/**
	 *
	 * Get the size of a block that's contained in this allocator
	 */
	
	size_t getSize(void * ptr);
};



struct IGVMHeap
{
	static const int ALLOC_MALLOC = 0;
	static const int ALLOC_LINEAR = 1;


	Object *           m_handles		[IGVM_MAX_HANDLES];
	ig_u8              m_handles_meta[IGVM_MAX_HANDLES]; // meta information for the handles

	VMStack<ig_u32>    m_grey_list;
	VMStack<Box>       m_static_roots;
	VMStack<ig_u32>    m_dynamic_roots;
	VMLinearAllocator  m_young_allocator;
	
	VMAllocator *      m_allocator;  
	
	int                s_handle_count;				// # of active handles
	int                s_handle_free;				// current free handle
	unsigned int       s_handle_max_entry;

	void destroy();
	int  init(VMAllocator * alloc, int max_handles, int young_pool_size, int grey_list_size);
	void reinit(void * user_p, void     ( * destructor)(void *, ig_u32));
	
	
	inline void  setHandleAllocator(ig_u32 handle, int allocator) {
		if (allocator == ALLOC_MALLOC) {
			m_handles_meta[handle] |= PASSED_GC_BIT;
		}
		else {
			m_handles_meta[handle] &= ~PASSED_GC_BIT;
		}
	}
	
	inline int   getHandleAllocator(ig_u32 handle) {
		if (0 != (m_handles_meta[handle] & PASSED_GC_BIT)) {
			return ALLOC_MALLOC;
		}
		return ALLOC_LINEAR;
	}
	
	
	ig_u32 allocHandle     ( size_t instance_size, void * instance_defaults, int gc_type);
	ig_u32 allocArrayHandle(size_t instance_size, void * instance_defaults,  int count, int sz_per_entry, int gc_type);

	ig_u32 reallocArray(ig_u32 handle, int sz_per_entry, int new_count);

	
	void   freeHandle(ig_u32 handle);
	
	void assigned(ig_u32 obj_handle, ig_u32 value_handle);
	
	// mark a handle as grey or black if it is currently white
	// leafs are automatically turned black
	// this does not check if the handle is null
	void whiteToGrey(ig_u32 handle);
	
	
	void whiteToGreyIfValid(ig_u32 handle);
	

	//void * alloc(size_t mem);
	
	void gc(bool stop_the_world, 
		void * user_p,
		ig_i16 * (* discovery )(void *, void * ptr), 
		void     (* destructor)(void *, ig_u32 handle));
	
	
	
	
	ig_u32          allocAndClearStaticRoot();

	/**
	 * Get the contents of a static root.  This is primarily for the internals
	 * of the VM
	 */

	inline Box * getStaticRoot(ig_u32 idx) {
		return &m_static_roots.m_handles[idx];
	}
	
	/**
	 * Set the contents of a static root.  This is primarily for the internals
	 * of the VM
	 */
	
	inline void setStaticRoot(ig_u32 idx, const Box & box) {
		m_static_roots.m_handles[idx] = box;
		if (box.u32 != 0) {
			whiteToGrey(box.u32);
		}
	}
	
	/**
	 * Allocate a new dynamic root
	 */
	
	ig_u32    allocDynamicRoot();
	
	/**
	 * Free an existing dynamic root
	 */ 
	
	void   freeDynamicRoot(ig_u32 root);
	
	
private:
	/**
	 * Create a new handle
	 * returns 0 if successfull
	 */
	int createHandle(void * ptr, int gc_type, ig_u32 * out_handle);
	
	// mark a handle as grey or black if it is currently white
	// leafs are automatically turned black
	// this checks if the handle is null
	void toGreyOrBlack(ig_u32 handle);
};



}

#endif