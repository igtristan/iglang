"use strict";




var igvm = require("./dst_js/igvm_node.js");
igvm.init();

function processArguments(method, fn, start_index, args_in) {
	let args = [method, fn];
	for (let i = start_index; i < args_in.length; i++) {
		const a = args_in[i];
		if (typeof a === 'string') {
			args.push(igvm.nativeToString(a));
		}
		else {
			args.push(a);
		}
	}

	return args;
}

function runExactTest(message, expected_result, method, fn)
{
	let args = processArguments(method, fn, 4, arguments);
	let result = igvm.call.apply(igvm, args);

	if (result === expected_result) {
		console.log("SUCCESS " + message);
	}
	else {
		console.log("ERROR " + message + ". expected: " + expected_result + " found: " + result);
	}
}

function runStringTest(message, expected_result, method, fn) 
{
	let args = processArguments(method, fn, 4, arguments);
	

	let result = igvm.call.apply(igvm, args);
	if (igvm.isNull(result)) {
		result = null;
	}
	else {
		result = igvm.stringToNative(result);
	}

	if (result == expected_result) {
		console.log("SUCCESS " + message);
	}
	else {
		console.log("ERROR " + message + ". expected: " + expected_result + " found: " + result);
	}
}


console.log('############################################');
console.log('NodeJS Test Suite for iglang JS target');
console.log('############################################');

runExactTest("Math imla",      7,        "MathTest", "imla", 1, 2, 3);
runExactTest("Math sin(0)",  0.0,     "iglang.Math", "sin",  0.0);
runExactTest("Math cos(0)",  1.0,     "iglang.Math", "cos",  0.0);
runStringTest("String concat null", "nullnull", "StringTest", "concat", null, null);
runStringTest("String concat", "abc123", "StringTest", "concat", "abc", "123");
runStringTest("String charAt", "d", "iglang.String",   "charAt", "abcdef", 3);
runExactTest("Vector idx", 4, "VectorTest", "getIndex", 2,   0,2,4,8);
runExactTest("Switch pow of 3", 27, "SwitchTest", "powerOf3", 3);

//console.log("sin 90 = " + igvm.call("iglang.Math", "sin", 3.14159265/2));


/*
8
var fs = require("fs");
var vm = require("vm");

function include(path, context) {
    var code = fs.readFileSync(path, 'utf-8');
    vm.runInContext(code, vm.createContext(context));
}

include()

*/
