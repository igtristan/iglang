/*
Copyright (c) 2014-2017 Incubator Games Ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

// TODO need to error if dst directory does not exist



import java.util.*;
import java.io.*;
import java.security.*;
import java.net.*;
import java.util.zip.*;

import pass1.*;
import pass2.*;


public class igc extends IGConsts
{
	public igc()
	{

	}

	public static void TODO()
	{
		throw new RuntimeException("TODO");
	}
	
	
	
	public static void processTemplate(File src, String klass_name, ArrayList<String> pack, File dst)
	{
		try
		{
		IGTemplateParser tp = new IGTemplateParser();
		tp.init(src.toString(), new FileInputStream(src));
		
	
		PrintWriter pw = new PrintWriter(new FileWriter(dst));
		pw.print("package ");
		for (int i = 0; i < pack.size(); i++) {
			if (i != 0) { pw.print("."); }
			pw.print(pack.get(i));
		}
		pw.println(";");
		pw.println("class " + klass_name + " { ");
		
		pw.println("public static function writeToByteArray(params: Map<String,String>, ba : ByteArray): void {");
		tp.writeTo(pw, /*mode0*/ 0, "ba");
		pw.println("}");
		pw.println("}");
		
		pw.close();
		}
		catch (Exception ex) {
			System.out.println("Template failed: " + src);
			ex.printStackTrace();
			System.exit(1);
		}
	
	}


	

	public static void gather(Vector<IGSource> sources, ArrayList<String> pack, boolean first, 
		File node, String dst_root, String dst_path)
	{
		String node_name = node.getName();
		if(node.isDirectory())
		{
			File[] subNode = node.listFiles();

			for(File filename : subNode){
				//System.out.print("files... " + filename.getPath());

				if (first) {
					gather(sources, pack, false, filename, dst_root, dst_path);
				}
				else
				{
					pack.add(node_name);
					gather(sources, pack, false, filename, dst_root, dst_path + node_name + "/");
					pack.remove(pack.size() - 1);
				}
			}
		}
		else if (node_name.endsWith(".ig"))
		{
			// verify that the ig file is not the result of a template
			if (!new File(node.toString() + "t").exists())
			{
				String filename = node.getName();
				int extensionIndex = filename.lastIndexOf(".");
				if (extensionIndex != -1)
					filename = filename.substring(0, extensionIndex);

				IGSource source = new IGSource(new ArrayList<String>(pack), 
					node.getPath(), dst_root, dst_path  + filename, filename);
				sources.add(source);
			}
		}
		else if (node_name.endsWith(".igt"))
		{
			File template_src = node;
		
			String node_path = node.toString();
			node = new File(node_path.substring(0, node_path.length() - 1));
			
			//
			{
				String src_name = template_src.getName();
				src_name = src_name.substring(0, src_name.length() - 4);
				processTemplate(template_src, src_name, pack,  node);
			}
			
			String filename = node.getName();
		    int extensionIndex = filename.lastIndexOf(".");
		    if (extensionIndex != -1)
				filename = filename.substring(0, extensionIndex);

			IGSource source = new IGSource(new ArrayList<String>(pack), node.getPath(), 
				dst_root, dst_path  + filename, filename);
			sources.add(source);
		}
	}

	public static void usage(int ret) {
	
		new RuntimeException().printStackTrace();
	
		System.out.println("Usage: java -jar igc.jar <options> {TARGET} src-dir0/ ... src-dirN/ dst-dir/");
		System.out.println("\twhere {TARGET} = -igvm, -as3, -cpp, -php, -java, -doc");
		System.out.println("use -help for a list of possible options");
		System.exit(ret);
	}
	
	public static void gatherJarInternals(Vector<IGSource> sources, String arg_dst_type, String arg_dst_directory)
	{
		try
		{
			CodeSource src = igc.class.getProtectionDomain().getCodeSource();
			if (src != null) 
			{
				URL jar = src.getLocation();
				ZipInputStream zip = new ZipInputStream(jar.openStream());
				while(true) {
				ZipEntry e = zip.getNextEntry();
				if (e == null)
				{ 
					break;
				}
				String name = e.getName();
				
				String lib_prefix = "data/" + arg_dst_type + "_lib/";
				String prefix     = "data/" + arg_dst_type + "_src/";
				if (arg_dst_type.equals("doc")) {
					prefix = "data/asm_src/";
				}

				if (name.startsWith(lib_prefix)) {
					String src_path =  name;
					if (!e.isDirectory()) 
					{
						String src_filename =  name.substring(lib_prefix.length());
						File dst_file = new File(arg_dst_directory + src_filename);
					
						try
						{
							File dst_file_parent = dst_file.getParentFile();
							dst_file_parent.mkdirs();
						}
						catch (Exception ex) {
							// do nothing not really an error
						}

						pass2.Util.copyFile("jar:" + src_path, dst_file);
					}
				}	
				else if (name.startsWith(prefix) && name.endsWith(".ig")) 
				{
					String src_path = name;
					
					
					ArrayList<String> pack = new ArrayList<String>();
					
					// remove the .ig extension
		   			int extensionIndex = src_path.lastIndexOf(".");
					String filename    = src_path.substring(prefix.length(), extensionIndex);
					String n           = filename.substring(filename.lastIndexOf("/") + 1);
					
					String [] toks = filename.split("/");
					for (String t : toks) {
						pack.add(t);
					}
					
					pack.remove(pack.size() - 1);

					//System.out.println("arg dst directory: " + arg_dst_directory);
					sources.add(new IGSource(pack, "jar:" + src_path, arg_dst_directory, filename, n));	
				}
			}
		} 
		else {
		  /* Fail... */
		}
		}
		catch (IOException ex) {
			ex.printStackTrace();
		}
	}
	
	public void init(String [] x)
	{
		util.Log.startTimer();
		
		String arg_dst_type = "null";
		String variant = "standard";
		int asm_writer_mode = pass2.asm.IGAsmDocument.MODE_WRITE_DISABLED;
		
		boolean compilation_failed = false;
		//boolean new_features = false;
		boolean verbose = false;
		boolean json_enabled = false;
		boolean json_sentinel = false;
		int xi = 0;

		//for (String ssss : x ) {
		//	System.out.println("'" + ssss + "'");
		//}

		while (x[xi].startsWith("-"))
		{
			if (x[xi].equals("-v")) {
				verbose = true;
				xi ++;

				pass1.IGSettings.s_verbose = verbose;
			}
			else if (x[xi].equals("-json")) {
				util.Log.setEnabled(true);
				xi++;
			}
			else if (x[xi].startsWith("-json-sentinel=")) 
			{
				String rest = x[xi].substring("-json-sentinel=".length());
				util.Log.setEnabled(true);
				util.Log.setSentinel(rest);
				xi++;
				
			}
			else if (x[xi].equals("-igvm")) {
				//System.out.println("igvm tag");
				arg_dst_type = "asm";
				asm_writer_mode = pass2.asm.IGAsmDocument.MODE_WRITE_HEADERS;
				xi++;
			}
			else if (x[xi].equals("-S:headers")) {
				//System.out.println("-S:headers tag");
				arg_dst_type = "asm";
				asm_writer_mode = pass2.asm.IGAsmDocument.MODE_WRITE_HEADERS;
				xi++;
			}
			else if (x[xi].equals("-S")) {
				//System.out.println("-S tag");
				arg_dst_type = "asm";
				asm_writer_mode = pass2.asm.IGAsmDocument.MODE_WRITE_BODY;
				xi++;
			}
			else if (x[xi].equals("-js")) {
				arg_dst_type = "js";
				variant = "standard";
				xi++;
			}
			else if (x[xi].equals("-js-node")) {
				arg_dst_type = "js";
				variant = "node";
				xi++;
			}

			else if (x[xi].equals("-cpp")) {
				arg_dst_type = "cpp";
				xi++;
			}
			else if (x[xi].equals("-as3")) {
				arg_dst_type = "as3";
				xi++;
			}
			else if (x[xi].equals("-java")) {
				arg_dst_type = "java";
				xi++;
			}
			else if (x[xi].equals("-php")) {
				arg_dst_type = "php";
				xi++;
			}
			else if (x[xi].equals("-doc")) {
				arg_dst_type = "doc";
				xi++;
			}
			else if (x[xi].equals("-o")) {
				xi ++;

				// believe this option broke some builds.  Need to re-evaluate
				pass1.IGSettings.s_optimization_level = 1;
				pass1.IGSettings.s_optimize = false;
			}
			else if (x[xi].startsWith("-debug-timers=")) {
				boolean value = Boolean.parseBoolean(x[xi].split("=")[1]);
				xi ++;
	
				util.Log.setPrintTimers(value);
			}
			else if (x[xi].equals("--help") || x[xi].equals("-help")) 
			{
				System.out.println("iglang version 0.3 © Incubator Games Ltd. 2013-2017");
				System.out.println();
				System.out.println("Usage: java -jar igc.jar <options> TARGET src-dir-1/ .. src-dir-N/ dst-dir/");
				System.out.println("where possible options include:");
				//System.out.println("  -n Run with newest features");
				System.out.println("  -json                Print errors as a json payload");
				System.out.println("  -json:sentinel=XXXXX Print XXXXX before the json payload");
				System.out.println("  -debug-timers=true   Display time required for compilation steps");
				
				//System.out.println("  -o Optimize. Replace constants inline");
				//System.out.println("  -v Verbose.");
				System.out.println("  -help                Display this message");
				System.out.println("  -version             Display the version of the igc compiler");
				System.out.println();
				System.out.println("where TARGET is:");
				System.out.println("  -as3                 Flash AS3 output");
				System.out.println("  -igvm                IGVM bytecode");
				System.out.println("  -doc                 HTML Documentation");
				
				
				//System.out.println("  cpp - Garbage collected c++ output");
				
				//System.out.println("  java - Java output");
				//System.out.println("  php - PHP output (experimental and incomplete)");
				System.exit(0);
			}
			else if (x[xi].equals("--version") || x[xi].equals("-version")) {
				System.out.println("iglang version 0.3");
				System.out.println("© Incubator Games Ltd. 2013-2017");
				System.exit(0);
			}
			else
			{
				System.out.println("igc: invalid flag: " + x[xi]);
				
				usage(1);

			}
		}
		
		
		//System.out.println("xi: " + xi + " " + x.length);
		
		// if no destination type was specified or no feasible way an arg and src were specified
		if (arg_dst_type == null || (xi + 2) > x.length) {
			usage(1);
		}

		// yeah.. this fails for setting endian
		// and /src/game/widgets/IGTileFadeWidget.as(80)
		pass1.IGSettings.s_optimize = false;



		String arg_dst_directory = x[x.length - 1];

		//if (x.length != (xi + 3) || (!arg_dst_type.equals("as3")
		//			&& !arg_dst_type.equals("cpp")
		//			&& !arg_dst_type.equals("vm")
		//			&& !arg_dst_type.equals("php")
		//			&& !arg_dst_type.equals("java")
		//			&& !arg_dst_type.equals("doc")
		//			&& !arg_dst_type.equals("asm")
		//			)
		//	)
		//{
		//	usage(1);
		//}
	
		/////////////////////////////////
		// Verify and validate the destination directory
		
		File dst_dir = new File(arg_dst_directory);

		// if destination path exists, it must be a directory
		if (dst_dir.exists() && !dst_dir.isDirectory()) {
			usage(1);
		}
		
		
		if (!arg_dst_directory.endsWith("/") && !arg_dst_directory.endsWith(File.separator)) {
			arg_dst_directory += File.separator;
		}
		
		
		


		String compilation_target = arg_dst_type;
		Vector<IGSource> sources = new Vector<IGSource>();
		IGPackage           root    = new IGPackage(arg_dst_directory);


		gatherJarInternals(sources, arg_dst_type, arg_dst_directory);
		
		for (int i = xi; i < x.length - 1; i++)
		{		
			String arg_src_directory = x[i];
			File src_dir = new File(arg_src_directory);
		
			// source directory must exist and be a directory
			if (!src_dir.exists() || !src_dir.isDirectory() ) {
				System.err.println("bad source directory: " + src_dir);
				usage(1);
			}

			// verify that appending a path will generate a valid sub file
			if (!arg_src_directory.endsWith("/") && !arg_src_directory.endsWith(File.separator)) {
				arg_src_directory += File.separator;
			}

		
			gather(sources, new ArrayList<String>(), true, new File(arg_src_directory), arg_dst_directory, "");
		}
		
		util.Log.stopTimer("Initial gather ! (target " + arg_dst_type + ") " );

		// for stdlib stuff
		//gather(source, true, new File(aux directory for skew), arg_dst_directory);


		util.Log.startTimer();
		String current_path = "";
		try
		{
			IGLexer lexer = new IGLexer();
			lexer.clearSymbols();
			
			lexer.addSymbol("package", TOK_PACKAGE);
			lexer.addSymbol("import", TOK_IMPORT);
			lexer.addSymbol("class", TOK_CLASS);
			lexer.addSymbol("extends", TOK_EXTENDS);

			lexer.addSymbol("function", TOK_FUNCTION);
			lexer.addSymbol("var", TOK_VAR);
			lexer.addSymbol("const", TOK_CONST);
			lexer.addSymbol("public", TOK_PUBLIC);
			lexer.addSymbol("internal", TOK_INTERNAL);
			lexer.addSymbol("private", TOK_PRIVATE);
			lexer.addSymbol("protected", TOK_PROTECTED);
			lexer.addSymbol("static", TOK_STATIC);
			lexer.addSymbol("native", TOK_NATIVE);
			lexer.addSymbol("#refcount", TOK_REF_COUNT); 

			lexer.addSymbol("int",        TOK_INT);
			lexer.addSymbol("uint",       TOK_UINT);
			lexer.addSymbol("double",     TOK_NUMBER);
			lexer.addSymbol("bool",       TOK_BOOLEAN);
			lexer.addSymbol("byte",       TOK_BYTE);
			lexer.addSymbol("float",	  TOK_FLOAT);
			lexer.addSymbol("short",      TOK_SHORT);



			lexer.addSymbol("Class",      TOK_CLASS_TYPE);

			lexer.addSymbol("#>>>", TOK_LSR);
			lexer.addSymbol("#>>", 	TOK_ASR);
			lexer.addSymbol("#<<", 	TOK_LSL);
			lexer.addSymbol("#",  TOK_SPECIAL);

			lexer.addSymbol("if", TOK_IF);
			lexer.addSymbol("else", TOK_ELSE);
			lexer.addSymbol("for", TOK_FOR);
			lexer.addSymbol("while", TOK_WHILE);
			lexer.addSymbol("do", TOK_DO);
			lexer.addSymbol("void", TOK_VOID);
			//lexer.addSymbol("./", TOK_IDIV);
			lexer.addSymbol(".", TOK_DOT);
			lexer.addSymbol("?", TOK_QUESTIONMARK);
			lexer.addSymbol(":", TOK_COLON);
			lexer.addSymbol(";", TOK_SEMI);
			lexer.addSymbol(",", TOK_COMMA);

			lexer.addSymbol("+=", TOK_ADD_ASSIGN);
			lexer.addSymbol("-=", TOK_SUB_ASSIGN);
			lexer.addSymbol("*=", TOK_MUL_ASSIGN);
			lexer.addSymbol("/=", TOK_DIV_ASSIGN);

			// disable these for now
			lexer.addSymbol("%=", TOK_MOD_ASSIGN);
			lexer.addSymbol("&=", TOK_AND_ASSIGN);
			lexer.addSymbol("|=", TOK_OR_ASSIGN);
			lexer.addSymbol("^=", TOK_XOR_ASSIGN);

			lexer.addSymbol("==", TOK_EQ);
			lexer.addSymbol("!=", TOK_NE);
			lexer.addSymbol("<=", TOK_LE);
			lexer.addSymbol(">=", TOK_GE);


			lexer.addSymbol("++", TOK_INC);
			lexer.addSymbol("--", TOK_DEC);

			lexer.addSymbol("+", TOK_ADD);
			lexer.addSymbol("-", TOK_SUB);
			lexer.addSymbol("*", TOK_MUL);
			lexer.addSymbol("/", TOK_DIV);
			lexer.addSymbol("%", TOK_MOD);
			lexer.addSymbol("{", TOK_LBRACE);
			lexer.addSymbol("}", TOK_RBRACE);
			lexer.addSymbol("[", TOK_LARRAY);
			lexer.addSymbol("]", TOK_RARRAY);
			lexer.addSymbol("(", TOK_LBRACKET);
			lexer.addSymbol(")", TOK_RBRACKET);
			lexer.addSymbol("<", TOK_LT);
			lexer.addSymbol(">", TOK_GT);
			lexer.addSymbol("=", TOK_ASSIGN);
			lexer.addSymbol("get", TOK_GET);
			lexer.addSymbol("set", TOK_SET);
			//lexer.addSymbol("this", TOK_THIS);

			lexer.addSymbol("!", TOK_NOT);
			lexer.addSymbol("&&", TOK_AND);
			lexer.addSymbol("||", TOK_OR);

			lexer.addSymbol("&", TOK_BIT_AND);
			lexer.addSymbol("|", TOK_BIT_OR);
			lexer.addSymbol("~", TOK_BIT_NOT);
			lexer.addSymbol("^", TOK_BIT_XOR);

			lexer.addSymbol("return", TOK_RETURN);
			lexer.addSymbol("new", TOK_NEW);
			lexer.addSymbol("case", TOK_CASE);
			lexer.addSymbol("switch", TOK_SWITCH);
			lexer.addSymbol("default", TOK_DEFAULT);
			lexer.addSymbol("break", TOK_BREAK);
			lexer.addSymbol("null", TOK_NULL);
			lexer.addSymbol("continue", TOK_CONTINUE);
			lexer.addSymbol("as", TOK_AS);
			lexer.addSymbol("throw", TOK_THROW);

			lexer.addSymbol("implements", TOK_IMPLEMENTS);
			lexer.addSymbol("interface", TOK_INTERFACE);
			lexer.addSymbol("inline", TOK_INLINE);
			lexer.addSymbol("override", TOK_OVERRIDE);
			lexer.addSymbol("virtual", TOK_VIRTUAL);
			lexer.addSymbol("final",  TOK_FINAL);
			lexer.addSymbol("enum",   TOK_ENUM);
			lexer.addSymbol("Type",   TOK_TYPE);

			// special types
			lexer.addSymbol("Function",  		TOK_FUNCTION_TYPE);
			lexer.addSymbol("StaticFunction", 	TOK_STATIC_FUNCTION_TYPE);
			lexer.addSymbol("try", 				TOK_TRY);
			lexer.addSymbol("catch", 			TOK_CATCH);
			lexer.addSymbol("in",               TOK_IN);
			lexer.addSymbol("true",				TOK_TRUE);
			lexer.addSymbol("false",            TOK_FALSE);
			lexer.addSymbol("=>",			    TOK_RANGE);
			lexer.addSymbol("->",				TOK_ANON_FN);
			lexer.buildTable(compilation_target);
			
			util.Log.stopTimer("Lexer built");
		
			util.Log.startTimer();
		
			for (int i = 0; i < sources.size(); i++)
			{
				IGSource source = sources.get(i);
				String src_path = source.m_src_path;
				current_path = src_path;
				
				if (pass1.IGSettings.s_verbose) {
					System.out.println("Processing: " + src_path);
				}

				
				try
				{
					if (src_path.startsWith("jar:")) {
						InputStream is = this.getClass().getClassLoader().getResourceAsStream(src_path.substring(4));	
						lexer.initWithFile(src_path, is);	
					}
					else
					{
						lexer.initWithFile(src_path, new FileInputStream(src_path));
					}
				}
				catch (java.util.NoSuchElementException ex) 
				{
					if (pass1.IGSettings.s_verbose) {
						ex.printStackTrace();
					}
					util.Log.logErrorEcho(src_path, "Failed to process file");
					System.exit(1);
				}
				catch (java.lang.NullPointerException ex) 
				{
					if (pass1.IGSettings.s_verbose) {
						ex.printStackTrace();
					}
					util.Log.logErrorEcho(src_path, "Failed to process file");
					System.exit(1);
				}
				catch (FileNotFoundException ex) 
				{
					if (pass1.IGSettings.s_verbose) {
						ex.printStackTrace();
					}
					util.Log.logErrorEcho(src_path, "Failed to process file");
					System.exit(1);
				}
			
				IGTokenList tok = IGTokenList.alloc(new Token());
				IGTokenList prev = null;
				IGTokenList head = tok;

				//int token_index = 0;
				while (lexer.read(tok.m_token) == 1)
				{
					switch (tok.m_token.m_type)
					{
						case IGLexer.LEXER_STRING_MULTILINE:
							tok.m_token.printError("Invalid multiline string");
							System.exit(1);
							break;
						case IGLexer.LEXER_STRING_INVALID_ESCAPE:
							tok.m_token.printError("Invalid escape character");
							System.exit(1);
							break;
						case IGLexer.LEXER_CHAR_MULTILINE:
							tok.m_token.printError("Invalid multiline char");
							System.exit(1);						
							break;
						case IGLexer.LEXER_INVALID:
							tok.m_token.printError("Invalid token");
							System.exit(1);
							break;
					
					}
					
					prev = tok;
					tok = IGTokenList.alloc(new Token());
					prev.m_next = tok;
				}


				// Create an EOF token
				//tok.m_next = tok;		// loop the EOF token back on itself
				
				tok.m_token.m_type = IGLexer.LEXER_EOF;
				tok.m_token.m_token_source = lexer.m_token_source;

				// perform the initial parsing pass on the input data
				IGFileReader.parseFile(source, head);	//list);
				IGTokenList.reclaim(head);

				// inject the source file into our internal package heirarchy
				root.inject(source);
			}
		}
		catch (RuntimeException ex) {
			ex.printStackTrace();
			util.Log.logErrorEcho(current_path, "Failed to process file");
			System.exit(1);
		}
		util.Log.stopTimer("Initial parse");

		util.Log.startTimer();
		{

			final int source_len = sources.size();
			for (int i = 0; i < source_len; i++) {
				IGSource s = sources.get(i);
				if (s.m_failed_pass0) 
				{
					System.err.println("COMPILATION FAILED");
					System.exit(1);
				}
			}
		
			// create seperate AST's of each file
			IGValidator.run(root, sources);
		}
		util.Log.stopTimer("Validator");


		Vector<IGSource> output_sources = new Vector<IGSource>();

		util.Log.startTimer();
		{


			

			// check to see if the validation pass was successfull
			// and cull any source files that should not be output
			// ie standard library husks
			///////////////////////////////////////////////////////

			final int source_len = sources.size();
			for (int i = 0; i < source_len; i++) {
				IGSource s = sources.get(i);
				if (s.m_failed_pass0) {
					System.err.println("COMPILATION FAILED");
					System.exit(1);
				}

				if (compilation_target.equals("doc") ||
					s.shouldOutput(compilation_target)) {
					output_sources.add(s);
				}
			}

			//System.out.println("pass1.IGValueRange.run START");
			if (0 != pass1.IGValueRange.run(root, output_sources)) {
				System.err.println("COMPILATION FAILED");
				System.exit(1);
			}
		}
		util.Log.stopTimer("Value Ranges");
		
		util.Log.startTimer();
		{
		
			//System.out.println("pass1.IGValueRange.run END");			
			// TODO validate enums at this point
			
			
			//pass1.IGOptimizer.run(root, output_sources);


			// ???
			if (!compilation_target.equals("asm") &&
				!compilation_target.equals("js") ) {
				for (IGSource s2 : output_sources) {
					s2.purgeStubs();
				}
			}


//			if (compilation_target.equals("cpp")) {
//				(new pass2.Cpp()).run(root, output_sources);
//			}
//			else 

			if (compilation_target.equals("php")) {
				(new pass2.Php()).run(root, output_sources);
			}
			else if (compilation_target.equals("java")) {
				(new pass2.Java()).run(root, output_sources);
			}
			else if (compilation_target.equals("as3")) {
				pass2.As3New.run(root, output_sources);
			}
			// I don't believe that this target is even functional
			else if (compilation_target.equals("vm")) {
				pass2.VM.run(root, output_sources);
			}
			else if (compilation_target.equals("doc")) {
				(new pass2.Doc()).run(root, output_sources);
			}
			else if (compilation_target.equals("js")) 
			{
				// the javascript target uses the same back end as the assembly versiokn.
				// it transcodes the final output into non numan readable text
				pass2.Asm executor = new pass2.Asm();

				pass2.Asm.Config config = new pass2.Asm.Config();
				config.dst_extension = ".js";

				Vector<pass2.asm.IGAsmTarget> to_run = executor.run(root, output_sources, asm_writer_mode, config);
				
				
				util.Log.stopTimer("js1");
				
				util.Log.startTimer();
				//executor.run2(to_run);		// <-- this will need to be replaced

				// perform the final optimization step and write the asm out to disk
				new pass2.asm_to_js.IGAsmToJs().run(arg_dst_directory, to_run, variant);

			}
			else if (compilation_target.equals("asm")) {


				pass2.Asm.Config config = new pass2.Asm.Config();
				config.dst_extension = ".igclass";


				pass2.Asm executor = new pass2.Asm();
				Vector<pass2.asm.IGAsmTarget> to_run = executor.run(root, output_sources, asm_writer_mode, config);
				
				
				util.Log.stopTimer("Asm1");
				
				util.Log.startTimer();
				executor.run2(to_run);
				
				// 
				//
				
				//pass2.asm.IGAsmOptimizer.complete();
			}
			else
			{
				util.Log.logErrorEcho("?", "Unknown compilation target: " + compilation_target);
				System.exit(1);
			}
		}
		util.Log.stopTimer("Output to target");
		util.Log.printTimers();
		
		util.Log.s_status = "OK";
	}







	public static void main(String [] args)
	{
		util.Log.addShutdownHook();
		
		try
		{
			igc p = new igc();
			p.init(args);
		}
		catch (Exception ex) 
		{
			System.err.println("Internal Error");
			ex.printStackTrace();
			
			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw);
			ex.printStackTrace(pw);

			
			util.Log.logError("?", sw.toString(), 0, 0);

			System.exit(1);
		}

	}
}
