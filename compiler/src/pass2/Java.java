/*
Copyright (c) 2014-2017 Incubator Games Ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

package pass2;

import java.util.*;
import util.*;
import pass1.*;

// we're going to have to treat casting correctly 
// http://php.net/manual/en/function.intval.php

public class Java extends Processor
{

		
	IGTemplateUtil m_template_util = new IGTemplateUtil();
	IGContext      m_ctx = null;


	/*
	 * Depth first traversal of all nodes
	 */
	
	private void markup(TokenWriter pw, IGSource s, IGNode node)
	{
		final IGNodeList children = node.m_children;
		int children_count = children.size();
		//for (IGNode child : node.m_children) {
		for (int i = 0; i < children_count; i++) {
			markup(pw, s, children.get(i));
		}
		
		//System.err.println("markup2: " + node.m_token + " " + node.m_node_type);
		markup2(pw, s, node);
	}
	

	private String getDefaultForType(Type type)
	{
			
		if (type == Type.INT||
			type == Type.UINT ||
			type.isEnum()) {
		
			return "0";
		}
		else if (type == Type.DOUBLE) {
			return "0.0";
		}
		else if (type== Type.BOOL) {
		
			return "false";
		}
		else
		{
			return "null";
		}	
	
	}
	
	private String R(Type t)
	{
		t = m_ctx.resolveType(t);
		
		if (t == Type.BOOL) {
			return "boolean";	
		}
		else if (t == Type.STRING) {
			return "java.lang.String";
		}
		else if (t.m_primary.equals("iglang.Array")) 
		{
			return t.m_left + " [] ";
		}
		else if (t.isEnum()) {
				
			Type enum_primitive = t.getEnumPrimitive();
			if (Type.INT == enum_primitive || Type.UINT == enum_primitive) {
				return "int";
			}
			else if (Type.BOOL == enum_primitive) {
				return "boolean";
			}
			else if (Type.DOUBLE == enum_primitive) {
				return "double";
			}
			else {
				throw new RuntimeException("Can't find Java type for: " + t);
			}
		
		}
		else 
		{
			return t.toString();
		}	
	
	}
	
	private void markup2(TokenWriter pw, IGSource s, IGNode node)
	{
		Token token = node.m_token;
	
	
		
		 if (node.m_node_type == IGNode.NODE_BINARY && token.m_type == TOK_LSR) {
			pw.replace(token, ">>>");
		}
		else if (node.m_node_type == IGNode.NODE_BINARY && token.m_type == TOK_ASR) {
			pw.replace(token, ">>");
		}
		else if (node.m_node_type == IGNode.NODE_BINARY && token.m_type == TOK_LSL) {
			pw.replace(token, "<<");
		}
		else if (token.m_type == IGLexer.LEXER_NATIVE_BLOCK) 
		{
			if (token.m_value.startsWith("@java")) {
				pw.append(token, token.m_value.substring(5));
			}
			token.m_output = false;
		}
		else if (node.m_node_type == IGNode.NODE_VALUE) 
		{
			if (token.m_type == IGLexer.LEXER_STRING) 
			{
				pw.prepend(token, "\"");
				pw.append(token, "\"");
			}
			else if (token.m_type == IGLexer.LEXER_CHAR)
			{
				pw.prepend(token, "(int)\'");
				pw.append(token, "\'");
			}
		}
		else if (node.m_node_type == IGNode.NODE_TYPE_LOCAL_VARIABLE_DEFINITION)
		{	
			token.m_output = false;
		}
		else if (node.m_node_type == IGNode.LOCAL_VARIABLE_ACCESS) {
			if (s.isEnum() && node.m_token.m_value.equals("this")) {
				pw.replace(node.m_token, "__ig_enum_this");
			}
		}
		else if (node.m_node_type == IGNode.ID_TYPE_PAIR) 
		{
			IGNode id   = node.getChild(0);
			IGNode type = node.getChild(1);
			
			token.m_output = false;
			
			pw.prepend(id, R(type.m_type) + " ");
			

			pw.disableOutput(type);
			
		}
		else if (node.m_node_type == IGNode.FUNCTION_RETURN_TYPE)
		{
			pw.disableOutput(node);
		}
		else if (node.m_node_type == IGNode.STATIC_FIELD_ACCESS)
		{
			//pw.prepend(token, "\\" + s.getScopePath().toStringWithSeperator("\\","\\", false) + "::");
		}
		else if (node.m_node_type == IGNode.NODE_INDEX)
		{
			IGNode left = node.getChild(0);
			IGNode right = node.getChild(1);
			if (!left.m_type.m_primary.equals("iglang.Array")) {
				//  (xxxxx : left)([ value ] : right)
					
				pw.replace(node.m_token, ".__ig_arrget(");
				
				pw.replace(s.m_token_list[node.m_end_index - 1], ")");
			}			
		}
		else if (node.m_node_type  == IGNode.NODE_NEW)
		{
			IGNode left = node.getChild(0);
			IGNode right = node.getChild(1);
			
			if (left.m_type.m_primary.equals("iglang.Error"))
			{
				pw.disableOutput(left);
				//token.m_output = false;
				pw.append(token, " RuntimeException");
			}
			else
			{
				pw.disableOutput(left);
				pw.append(token, R(left.m_type));
				//disableOutput(left);
				
				//IGSource s2 = (IGSource) left.m_type.m_scope;
				//pw.append(left, "\\" + s2.getScopePath().toStringWithSeperator("\\","\\", false));
			
			}
		}
		// so I made no distinction between a call and a dot call... greatx
		else if (node.m_node_type == IGNode.NODE_CALL)
		{
			IGNode pre 		= node.getChild(0);
			IGNode params   = node.getChild(1);
			
			if (pre.getChildCount() > 1)
			{
				IGNode left 	= pre.getChild(0);
				IGNode right 	= pre.getChild(1);
				
				if (left.m_type.isEnum()) {
					pw.prepend(node, R(left.m_type) + "." + right.m_token.m_value + "(");
					
					
					
					
					if (params.getChildCount() > 0) {
						pw.replace(pre.m_token, ",");
					}
					else {
						pw.replace(pre.m_token, "");
					}
					
					pw.replace(right, "");
					pw.replace(params.m_token, "");
					
					
//					node.debug();
				}
			}
			else {
				//System.err.println("bar pre");
				//node.debug();
			}
				/*
				if (left.m_type == Type.get("iglang.String") &&
					right.m_token.m_value.equals("indexOf")) {
			
					pw.replace(pre.m_token, ",");
			
					// this isn't technically correct
					pw.prepend(node.m_start_index, "mb_strpos(");
					token.m_output = false;
					right.m_token.m_output = false;	
				
					//pw.append(node.m_end_index - 1, ")");
				}
				else if (left.m_type == Type.get("iglang.String") &&
					right.m_token.m_value.equals("substr")) {
			
					pw.replace(pre.m_token, ",");
			
					// this isn't technically correct
					pw.prepend(node.m_start_index, "mb_substr(");
					token.m_output = false;
					right.m_token.m_output = false;	
				
					//pw.append(node.m_end_index - 1, ")");
				}
				else if (left.m_type.m_primary.equals("iglang.Vector") &&
						 right.m_token.m_value.equals("push")) 
				{
				
					pw.replace(pre.m_token, ",");
					pw.prepend(node.m_start_index, "array_push(");
					token.m_output = false;
					right.m_token.m_output = false;	
				}
				else if (left.m_type.m_primary.equals("iglang.Vector") &&
						 right.m_token.m_value.equals("pop")) 
				{
					// just get rid of the "." altogether
					pw.replace(pre.m_token, "");
					
					pw.prepend(node.m_start_index, "array_pop(");
					token.m_output = false;
					right.m_token.m_output = false;	
				}
				else if (left.m_type.m_primary.equals("iglang.Map") &&
						 right.m_token.m_value.equals("containsKey")) 
				{
					pw.replace(pre.m_token, ",");
					pw.prepend(node.m_start_index, "__ig_containsKey(");
					token.m_output = false;
					right.m_token.m_output = false;	
				}
				else if (left.m_type.m_primary.equals("iglang.Map") &&
						 right.m_token.m_value.equals("deleteKey")) 
				{
					pw.replace(pre.m_token, ",");
					pw.prepend(node.m_start_index, "__ig_deleteKey(");
					token.m_output = false;
					right.m_token.m_output = false;	
				}
				*/
			

		}
		else if (node.m_node_type == IGNode.FUNCTION_PARAMETERS)
		{
		
		}
		else if (node.m_node_type == IGNode.CLASS_FUNCTION) 
		{
			IGMemberNode member_node = (IGMemberNode) node;
			
			int i = node.m_start_index;
			while (peekAt(i).m_type != TOK_FUNCTION) {
				//pw.disableOutput(peekAt(i));
				i++;
			}
			
			Token function_token = peekAt(i+0);
			
			
			
			Token id_token = peekAt(i+1);
			
			if (id_token.m_value.equals("<constructor>")) 
			{
				pw.replace(id_token, s.m_expected_component);
				pw.replace(function_token, "");
			}
			else if (id_token.m_value.equals("<get[]>")) {
				pw.replace(id_token, "__ig_getarr");
				pw.replace(function_token, "");
			}
			else if (id_token.m_value.equals("<set[]>")) {
				pw.replace(id_token, "__ig_setarr");
				pw.replace(function_token, "");
			}
			else if (s.isEnum()) {
				pw.replace(function_token, "static " + R( member_node.m_member.m_return_type));
			}
			else
			{
				pw.replace(function_token, R( member_node.m_member.m_return_type));
			}
			
			
			if (id_token.m_type == TOK_GET ||
				id_token.m_type == TOK_SET) {
			
				pw.disableOutput(id_token);	
			}
			
			if (s.isEnum()) {
				// node[0] preamble
				// node[1] member name
				// node[2] SIG
				IGNode n = node.getChild(2).findLeftMostWithType(IGNode.NODE_FUNCTION_SIGNATURE);
				
				// ?? TODO correctly detect enum type
				if (member_node.m_member.getParameterCount() > 0) {
					pw.replace(n.m_token, "(int __ig_enum_this, ");
				}
				else {
					pw.replace(n.m_token, "(int __ig_enum_this ");
				}
			}
			//pw.prepend(id_token, "$");
		}
		else if (node.m_node_type == IGNode.CLASS_STATIC_FUNCTION) 
		{
			IGMemberNode member_node = (IGMemberNode) node;
			
			int i = node.m_start_index;
			while (peekAt(i).m_type != TOK_FUNCTION) {
				//pw.disableOutput(peekAt(i));
				i++;
			}
			
			Token function_token = peekAt(i+0);
			pw.replace(function_token, R(member_node.m_member.m_return_type));
			
			Token id_token = peekAt(i+1);
			if (id_token.m_value.equals("<constructor>")) {
				pw.replace(id_token, s.m_expected_component);
			}
			
			if (id_token.m_type == TOK_GET ||
				id_token.m_type == TOK_SET) {
			
				pw.disableOutput(id_token);	
			}
			//pw.prepend(id_token, "$");
		}
		else if ((node.m_node_type == IGNode.DOT_ACCESSOR || node.m_node_type == IGNode.STATIC_DOT_ACCESSOR) && !node.isLValue())
		{
			IGNode left = node.getChild(0);
			IGNode right = node.getChild(1);
			
			
			/////////////////////
			// String length
			/////////////////////
			
			
			if ( left.m_type == Type.get("iglang.String") &&
				right.m_token.m_value.equals("length")) {
				
				pw.replace(right.m_token, "length()");
			}
			
			/////////////////////
			// Array and vector length
			///////////////////////
			
			else if (left.m_type.m_primary.equals("iglang.Array") &&	
					right.m_token.m_value.equals("length")) {
			
				pw.replace(right.m_token, "length");		
			}
			else if (left.m_type.m_primary.equals("iglang.Vector")  &&
					 right.m_token.m_value.equals("length")
			)
			{
				pw.replace(right.m_token, "size()");
			}
			else
			{
				pw.append(node, "__get()");
				
			}
		}
		else if (node.m_node_type == IGNode.SOURCE_PREAMBLE)
		{
			IGNode modifiers = node.getChild(0);
			IGNode type_node = node.getChild(1);
			if (type_node.m_token.m_value.equals("enum")) {
				pw.replace(type_node.m_token, "class");
			}
		}
		else if (node.m_node_type == IGNode.DOT_ACCESSOR || node.m_node_type == IGNode.STATIC_DOT_ACCESSOR)
		{
			IGNode left = node.getChild(0);
			IGNode right = node.getChild(1);
			
			/*
			IGNode parent = node.m_parent;
			
			if ((left.m_type.m_primary.equals("iglang.Vector") ||
					 left.m_type.m_primary.equals("iglang.Array"))  &&
					 right.m_token.m_value.equals("length")
			)
			{
				pw.prepend(node.m_start_index, "__ig_setArrayLength(");
				token.m_output = false;
				right.m_token.m_output = false;	
				
				pw.replace(parent.m_token, ",");
				
				
				String defaux = getDefaultForType(left.m_type.m_left);
				
				pw.append(parent.m_end_index - 1, "," + defaux + ")");
			}
			else
			{
				
			
				if (left.m_type.isNamespace()) {
					pw.replace(token, "::");
				}
				else {
					pw.replace(token, "->");
				}
				
				pw.append(right, "__set(");
				pw.disableOutput(parent.m_token);
				pw.append(parent, ")");
			}
			*/
		}
		//else if (node.m_node_type == IGNode.NODE_TYPE_MEMBER_VARIABLE_NAME)
		//{
		//
		//}
	}


	private void process(IGSource s) throws java.io.IOException
	{
		TokenWriter pw = Util.createOutputPrintWriter(s, generateExtension(s) + ".java");
		setSource(s.m_token_list, 0, s.m_token_list.length);
	
		if (s.m_type == IGScopeItem.SCOPE_ITEM_ENUM)
		{
			markup(pw, s, s.m_root_node);
		}
		else if (s.m_type == IGScopeItem.SCOPE_ITEM_INTERFACE )
		{
			markup(pw, s, s.m_root_node);
		}
		else if (s.m_type == IGScopeItem.SCOPE_ITEM_CLASS)
		{
			markup(pw, s, s.m_root_node);
		}
		else
		{
			throw new RuntimeException("Unknown source type.");
		}
	
		pw.printRange(s.m_token_list,  0, s.m_token_list.length);
		pw.close();
	}
	
	public String generateExtension(IGSource s) 
	{
		Type simple_type = s.getInstanceDataType();
		Type full_type = m_ctx.getType(s);
		
		String a = full_type.toString().substring(simple_type.toString().length(), full_type.toString().length());
		a = a.replace("<", "{");
		a = a.replace(">", "}");
		return a;
	}

	public  void run(IGPackage root, Vector<IGSource> sources)
	{	
		// create the header
		
		try
		{

			// process non templated classes
			for (IGSource s: sources)
			{
				if (s.isTemplatedClass()) {
					continue;
				}
		
				m_ctx = new IGContext();
				m_ctx.clearTypeReplace();
				m_ctx.setCollapseObjectTemplateTypes(true);
				
				
				process(s);	
			}
				
			// process templated classes
			while (m_template_util.augmentTemplateVariants())
			{
				for (IGSource s: sources)
				{
					if (!s.isTemplatedClass()) {
						continue;
					}
					
					Type idt = s.getInstanceDataType();
				
					IGTemplateUtil.IGTemplateVariant tv = m_template_util.getVariant(idt.m_primary);
					if (tv == null) { continue; }
					
					int template_count = tv.m_lefts.size();
			
					for (int template_index = 0; template_index < template_count; template_index++)
					{
						m_ctx = new IGContext();
						m_ctx.clearTypeReplace();
						m_ctx.setCollapseObjectTemplateTypes(true);
					
						Type l = tv.m_lefts.get(template_index);
						Type r = tv.m_rights.get(template_index);
				
						for (int k = 0; k < s.m_template_types.size(); k++) {
							if (k == 0) {
								m_ctx.addTypeReplace(s.m_template_types.get(0), l);
							}
							else if (k == 1)
							{
								m_ctx.addTypeReplace(s.m_template_types.get(1), r);
							}
						}
						
						process(s);	
					}
					
					// clear out the processed list of lefts and rights
					tv.mark();
				}
			}
		}
		catch (Exception ex)
		{
			System.err.print("java export failed");
			ex.printStackTrace();
			System.exit(1);
		}
	}
}