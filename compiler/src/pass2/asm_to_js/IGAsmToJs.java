package pass2.asm_to_js;

/**
This doesn't correctly handle the following cases

class A
{
	public static var X :int = 0;
	public static function a():void {
	
	}
}

class B extends A {
	public static function b():void {
		a();
		trace ("A": " + X);
	}
}
*/

import java.util.*;
import pass2.asm.*;

import java.io.*;
public class IGAsmToJs
{
	int m_sp = 0;
	int m_arg_count = 0;
	StringBuilder m_out = new StringBuilder();
	int m_indent = 0;
	HashMap<String,Label> m_labels;
	int m_label_count;

	static class Variable
	{
		String m_name;
		boolean m_is_param;

		public Variable(String n, boolean p) {
			m_name = n;
			m_is_param = p;
		}
	}

	private ArrayList<Variable> m_active_variables_list = new ArrayList<Variable>();
	private HashMap<String,Variable> m_active_variables = new HashMap<String,Variable>();

	public void clearVariables() {
		m_active_variables = new HashMap<String,Variable>();
		m_active_variables_list = new ArrayList<Variable>();
	}

	public void registerVariable(String name, boolean is_param) {
		if (!m_active_variables.containsKey(name)) {
			Variable v = new Variable(name,is_param);
			m_active_variables.put(name, v);
			m_active_variables_list.add(v);
		}
	}
	public void push(int idx)
	{
		m_sp ++;
		writes(lstack(0) + rvar(idx));
	}

	public void indent(int amount) {
		m_indent+= amount;
	}

	public void write(String s) {
		for (int i = m_indent; i > 0; i--) { m_out.append("\t"); }
		m_out.append(s);
		m_out.append("\n");

	//	System.out.println("OUTPUT: " + s + " ## sp=" + m_sp);
	}
	public void writes(String s) {
		for (int i = m_indent; i > 0; i--) { m_out.append("\t"); }
		m_out.append(s);
		m_out.append(";\n");

	//	System.out.println("OUTPUT: " + s + "; ## sp=" + m_sp);
	}


	public String stack(int offset) {

		int o =  (m_sp - 1 + offset);
		if (o < 0) 
		{ 
			System.out.println("CURRENT DATA");
			System.out.println(m_out);
			System.out.println("SADNESS");
			throw new RuntimeException("Invalid Stack Offset: " + m_current_module + " " + m_current_member); 
		}
		registerVariable("s" + o, false);
		return "s" + o;
	}

	public String lstack(int offset) {
		return stack(offset) + "=";
	}



	public String lvar(String offset) {
		return rvar(Integer.parseInt(offset)) + "=";
	}

	public String lvar(int offset) {
		return rvar(offset) + "=";
	}

	public String rvar(int offset) {
		//m_active_variables[offset] = true;
		if (offset < 0) { throw new RuntimeException("Invalid Variable Offset"); }
		registerVariable("v" + offset, false);
		return "v" +  offset;

	}

	public String pvar(int offset) {
		//m_active_variables[offset] = true;
		if (offset < 0) { throw new RuntimeException("Invalid Variable Offset"); }
		registerVariable("v" + offset, true);
		return "v" +  offset;

	}


	public String var(int offset) {
		return rvar(offset);
	}

	public String var(String offset) {
		return rvar(Integer.parseInt(offset));
	}




	public String binary(String fn) {

		m_sp--;
		String s = stack(0) + "=" + stack(0) + fn + stack(1) +";";
		return s;
	}

	public String compare(String fn) {
		m_sp--;
		String s = stack(0) + "=(" + stack(0) + fn + stack(1) + ") ? 1 : 0";
		return s;
	}

	public String ibinary(String fn) {

		m_sp--;
		String s = stack(0) + "=(" + stack(0) + fn + stack(1) + ") | 0"+";";
		return s;
	}


	public String binary_vec(String fn, int count) {

		
		StringBuilder sb = new StringBuilder();

		//System.out.println("sp: " + m_sp + " count: " + count);
		m_sp -= count;
		for (int i = 0; i < count; i++)
		{
			int dst_slot = i - count + 1;
			sb.append(stack(dst_slot) + "=(" + stack(dst_slot) + fn + stack(dst_slot + count) + ")"+"; ");
		}

		return sb.toString();
	}

	public String binary_vec_val(String fn, int count) {

		
		StringBuilder sb = new StringBuilder();

		//System.out.println("sp: " + m_sp + " count: " + count);
		m_sp -= 1;
		for (int i = 0; i < count; i++)
		{
			int dst_slot = i - count + 1;
			sb.append(stack(dst_slot) + "=(" + stack(dst_slot) + fn + stack(1) + ")"+"; ");
		}

		return sb.toString();
	}

	public String iunary(String fn) {

		String s = stack(0) + "=" + fn + stack(0)+";";
		return s;
	}

	static final class Label {
		public int m_id;
		public int m_sp;

		public Label(int i,int p) { m_id = i; m_sp = p; }

	}


	public Label generateLabel(String label, int sp)
	{
		Label existance = m_labels.get(label);
		if (existance == null) {
			m_label_count++;
			existance = new Label(m_label_count, sp);
			m_labels.put(label, existance);
		}
		return existance;
	}

	public String GOTO(String label)
	{
		Label existance = generateLabel(label, m_sp);
		return "pc=" + existance.m_id + ";continue;";
	}

	public String LABEL(String label) {
		Label existance = generateLabel(label, -1);
		//System.out.println("LABEL: " + label + " sp: " + m_sp + " spec'd: " + existance.m_sp);
		return "case " + existance.m_id + ":";
	}	

	public int getSPForLabel(String label)
	{
		Label lbl = m_labels.get(label);
		if (lbl == null) {
			System.out.println("WARNING This label was completely inaccessible: " + label);
			return 0;
		}
		return lbl.m_sp;
	}

	public int getIndexForLabel(String label)
	{
		Label lbl = m_labels.get(label);
		if (lbl == null) {
			System.out.println("SPMissing " + label);
		}
		return lbl.m_id;
	}


	/**

		Okay anyways ,... we dont properly handle all the remaining array ops that
		were never directly coded into ops
	*/
	public String dumpParameters(IGAsmOp op, int uniform_op, int ret_count)
	{
		StringBuilder tmp = new StringBuilder();

		int argc = op.getArgCount();

		// include the function pointer into the argument count
		if (uniform_op == IGAsm.call_dynamic) {
			argc++;
		}

		//System.out.println("args: " + argc + " ret: " + ret_count);

		m_sp -= argc;


		if (ret_count != 0) {
			tmp.append(lstack(1));
		}

		// all the ret1 varients have been changed into their NON ret1 versions
		switch(uniform_op) {
			case IGAsm.call_interface     :{ 
				tmp.append(stack(1) + ".$vtable." + getSafeMemberName(op.m_p1)); break;  }
			case IGAsm.call_vtable        :{
				tmp.append(stack(1) + ".$vtable." + getSafeMemberName(op.m_p2)); break;  }
			case IGAsm.call_static:
			{ 
				String module_name = getSafeModuleName(op.m_p1);
				String member_name = getSafeMemberName(op.m_p2);

				// an early out case
				if (module_name.equals("iglang.Math") && member_name.equals("square")) {
					tmp.append(stack(1) + "*" + stack(1));
					m_sp += ret_count;	
					return tmp.toString();
				}

				tmp.append("IGVM.MODULE." + module_name + "." + member_name); 
				break;  
			}
			case IGAsm.call_static_vtable :{ 
				tmp.append(stack(1) + ".$vtable." + getSafeMemberName(op.m_p2)); break;  }
			case IGAsm.call_dynamic       :{ 
				tmp.append("IGVM._invokeDynamic");
				break; 
			}
			case IGAsm.call_super         :{ 
				tmp.append("IGVM.MODULE." + getSafeModuleName(op.m_p1) + ".$vtable.$parent."  + getSafeMemberName(op.m_p2)); ; break; }
				//		tmp.append(stack(1) + ".$vtable.$parent." + getSafeMemberName(op.m_p2)); ; break; }
	
		}


			
		

		tmp.append('(');
		for (int i = 0; i < argc; i++) {
			if (i != 0) tmp.append(',');
			tmp.append(stack(i + 1));
		}
		tmp.append(')');

		m_sp += ret_count;	
		return tmp.toString();
	}

	public HashMap<String,String> m_safe_names = new HashMap<String,String>();

	public static String getSafeMemberName(String member_name) {
		// TODO
		if ("<constructor>".equals(member_name)) {
			return "$constructor";
		}
		else if ("<static>".equals(member_name)) {
			return "$static";
		}
		else if ("<get[]>".equals(member_name)) {
			return "$aget";
		}
		else if ("<set[]>".equals(member_name)) {
			return "$aset";
		}
		return member_name;
	}

	public static String getSafeModulePath(String module_name) {
		return "IGVM.MODULE." + getSafeModuleName(module_name);
	}

	public static String getSafeModuleName(String module_name) {
		if (module_name == null) {
			throw new RuntimeException("Null module_name");
		}
		// TODO
		StringBuilder tmp = new StringBuilder();
		int len = module_name.length();
		for (int i = 0; i < len; i++) {
			final char c = module_name.charAt(i);
			switch(c) {
				case '.': { tmp.append("$$"); break; }
				case '<': { tmp.append("$B"); break; }
				case '>': { tmp.append("$E"); break; }
				case ',': { tmp.append("$_"); break; }
				
				default:
					tmp.append(c);
			}
		}
		return tmp.toString();
	}

	public String lstatic(String module, String field) 
	{
		if (m_active_vtable.pathMatches(module)) {
			JsVtable mod = m_active_vtable;
			while (!mod.containsStaticVariable(field)) {
				mod = mod.m_parent;
			}
			module = mod.m_path;
		}
		return getSafeModulePath(module) + "." + getSafeMemberName(field) + "=";
	}

	public String rstatic(String module, String field) {
		if (m_active_vtable.pathMatches(module)) {
			JsVtable mod = m_active_vtable;
			while (!mod.containsStaticVariable(field)) {
				mod = mod.m_parent;
			}
			module = mod.m_path;
		}
		return getSafeModulePath(module) + "." + getSafeMemberName(field);
	}

	public static void run(String arg_dst_directory, Vector<IGAsmTarget> to_run, String variant)
	{
		Hashtable<String, IGAsmTarget> loaded = new Hashtable<String, IGAsmTarget>();

		Hashtable<String, IGAsmTarget> targets = new Hashtable<String, IGAsmTarget>();
		for (IGAsmTarget target : to_run) {
			targets.put(target.m_full_path, target);
		}

		// remove excessive labels
		for (IGAsmTarget target : to_run) {
			target.optimize(targets);
		}

		boolean skipped = true;
		boolean added   = false;


		HashMap<String,JsVtable> vtables = new HashMap<String,JsVtable>();
		ArrayList<IGAsmTarget> ordered_targets = new ArrayList<IGAsmTarget>();


		while (skipped) {
			skipped = false;
			added   = false;
			// everything that is extended is now in the correct order
			for (IGAsmTarget target : to_run) {
				//target.optimize(targets);
				IGAsmDocument w = target.getDocument();
				String module_name    = w.m_name_fully_qualified;

				// has this file been already processed?
				if ( loaded.containsKey(module_name)) {
					continue;
				}


				String module_extends = w.m_extends_fully_qualified;

				// has extended class  been loaded?
				if (module_extends != null && !loaded.containsKey(module_extends)) {
					skipped = true;
					continue;
				}

				// this really needs to dig into the code being executed
				// this is in no way complete or fullproof
				// TODO greatly improve this algo
				IGSet dependencies = w.getStaticDependencies();
				List<String> keys  = dependencies.getKeys();
				for (int i = keys.size() - 1; i >= 0; i--) {
					String key = keys.get(i);
					if (module_name.equals(key)) {
						continue;
					}
					if (!loaded.containsKey(key)) {
						skipped = true;
						continue;
					}
				}

				

				
				added = true;
				ordered_targets.add(target);

				try
				{
					StringBuilder data = new StringBuilder();
					//TextReader src = Util.getDestFileAsTextReader(target.m_source, target.m_src_path);
					File dst = pass2.Util.getDestFile(target.m_source, 
							target.m_dst_path);	
		
					PrintWriter pw = new PrintWriter(dst);

					new IGAsmToJs().processTarget(vtables, target, data);
				
					pw.print(data.toString());
					pw.close();
				}
				catch (IOException ex) {
					ex.printStackTrace();
					System.out.println("Failed in thread");
				}


				

				loaded.put(w.m_name_fully_qualified, target);
			}

			if (!added) {
				throw new RuntimeException("Recursive includes :(");
			}
		}


		try
		{
			// node js suitable output	
			{
				PrintWriter pw = new PrintWriter(
					arg_dst_directory + "index.js");

				pw.println("var fs = require(\"fs\");");
				pw.println("var vm = require(\"vm\");");
				pw.println("");
				pw.println("function include(path) {");
				//pw.println("    console.log('loading: ' + path );");
				pw.println("    var code = fs.readFileSync(__dirname + '/' + path, 'utf-8');");
				pw.println("    vm.runInThisContext(code);");
				pw.println("}");
				pw.println("");
				pw.println("include(\"igvm_core.js\");");
				pw.println("module.exports = IGVM;");

				for (IGAsmTarget target : ordered_targets) {
					pw.println("include(\"" + target.m_source.getPartialDestinationPath() + target.m_dst_path + "\");");
				}

				pw.println("console.log('Initializing...');");
				pw.println("IGVM.init();");
				pw.println();

				for (IGAsmTarget target : ordered_targets) 
				{
					IGAsmDocument w = target.getWriter();
					String module_extends = w.m_name_fully_qualified;

					//pw.println("console.log('" + module_extends + "');");
					pw.println(getSafeModulePath(module_extends) + ".$static();");
				}


				pw.close();
			}

			// see https://www.html5rocks.com/en/tutorials/speed/script-loading/
			// standard js suitable output
			// we'll also add a mode that outputs the whole codebase in a single
			// file
			{

				PrintWriter pw2 = new PrintWriter(
					arg_dst_directory + "igvm_static_init.js");


				//pw.println("function igvm_static_init(){");
				for (IGAsmTarget target : ordered_targets) 
				{
					IGAsmDocument w = target.getWriter();
					String module_extends = w.m_name_fully_qualified;

					//pw2.println("console.log('" + module_extends + "');");
					pw2.println(getSafeModulePath(module_extends) + ".$static();");
				}

				pw2.close();

				PrintWriter pw = new PrintWriter(
					arg_dst_directory + "igvm.js");


			///	pw.println("}");


				/**
					PHILOSOPHY
					1) include igvm.js - this should by default load everything
					2) have any native call actually trigger the static initialization process
				**/


				pw.println("var igvm = (function(){");
				{
					pw.println("'use strict';");

					// scripts that are loaded dynamically are considered to be async
					// this is clearly not our case.  Order does verymuch matter
					// for initializinng parent classes
					pw.println("function include(url) {");
					pw.println("\tvar script = document.createElement( 'script' );");
					pw.println("\tscript.type = 'text/javascript';");
					pw.println("\tscript.async = false;");
					pw.println("\tscript.src = url;");
					pw.println("\tdocument.body.appendChild( script );");
					pw.println("\treturn script;");
					pw.println("}");

					// AFAIK includingText this way runs the code immediately
					//pw.println("function includeText(text) {");
					//pw.println("\tvar script = document.createElement( 'script' );");
					//pw.println("\tscript.type = 'text/javascript';");
					//pw.println("\tscript.async = false;");
					//pw.println("\tscript.text = text;");
					//pw.println("\tdocument.body.appendChild( script );");
					//pw.println("}");

					pw.println("return {");
					pw.println("m_callback:null,");
					pw.println("load:function(path_to_scripts, cbk){");
					pw.println("\tthis.m_callback = cbk; ");
					pw.println("\tvar last = include(path_to_scripts+\"igvm_core.js\");");
					for (IGAsmTarget target : ordered_targets) {
						pw.println("\tlast = include(path_to_scripts+\"" + target.m_source.getPartialDestinationPath() + target.m_dst_path + "\");");
					}

					// user libraries also need to know when the code base has been loaded
					// but do I want to do this reactive the way the IGVM code is set up
					pw.println("\tlast.onload = function() { IGVM.init(); };");
					pw.println();

					pw.println("\tlast = include(path_to_scripts+\"igvm_static_init.js\");");
					//StringBuilder instructions = new StringBuilder();
					pw.println("\tlast.onload = function() { cbk(); };");

					//pw.println("\tincludeText(\"igvm.m_callback();\");");
					pw.println("}}");
				}
				pw.println("})();");

				pw.close();
			}
		}
		catch (IOException ex) {
			ex.printStackTrace();
			System.out.println("messed up");
		}

	}


	static class JsVtable
	{
		JsVtable               m_parent;
		String                 m_path;
		IGAsmDocument          m_doc;
		HashMap<String,String> items;
		//public JsVtable() {
		//	items = new HashMap<String,String>();
		//}


		public boolean pathMatches(String p) {
			return p.equals(m_path);
		}

		public boolean containsStaticVariable(String field) {
			return m_doc.containsStaticVariable(field);
		}

		public JsVtable(JsVtable parent, String safe_name, IGAsmDocument doc) {
			m_doc       = doc;
			m_path      = safe_name;
			m_parent    = parent;
			if (parent == null) {
				items = new HashMap<String,String>();
			}
			else
			{
				items = new HashMap<String,String>(parent.items);
			}
		}

		public void add(String key, String value) {
			items.put(key, value);
		}

		public HashMap<String,String> getMap() { return items; }
	}

	private String   m_current_member = null;
	private String   m_current_module = null;
	private JsVtable m_active_vtable = null;
	// this is neet
	public void processTarget(HashMap<String,JsVtable> m_vtables, IGAsmTarget target, StringBuilder _out) {

		m_out = _out;

		String module = target.m_full_path;
		String parent_module = target.getWriter().m_extends_fully_qualified;

		m_current_module = module;

		//System.out.println("vs " + module + " ex " + parent_module);
		//if (!module.equals("iglang.Object")) {
		////	parent_module = "iglang.Object";
		//}

		// TODO need to exttract the parent module


		IGAsmDocument writer = target.getWriter();


		final JsVtable parent_vtable = (parent_module == null) ? null : m_vtables.get(parent_module);
		JsVtable my_vtable = new JsVtable(parent_vtable, (module), writer);
		m_vtables.put(module, my_vtable);	


		m_active_vtable = my_vtable;


		write("(function(IGVM){ \"use strict\"; ");
		// gather all the static variables and static functions for the module
		// should this also pull in functions from extended scopes (LIKELY)
		write(getSafeModulePath(module) + " = {");
		{
			indent(1);
			// define all the functions and statics here
			for (IGAsmHeader h : writer.getHeaders()) {

				m_current_member = h.getName();
				// if is constructor bail early
				if (h.isFunction())
				{

					write("\"" + getSafeMemberName(h.getName()) + "\" :");

					// dump out the function bodies
					processTargetMember(target, (IGAsmHeader.Function) h );
					write(",");

					if (!h.isStatic()) {
						my_vtable.add(
							getSafeMemberName(h.getName()),		// function name
							getSafeModulePath(module) + "." + getSafeMemberName(h.getName()));
					}
				}
				else if (h.isVariable() && h.isStatic()) 
				{
					IGAsmHeader.Variable v = (IGAsmHeader.Variable)h;
					
					write(getSafeMemberName(v.getName()) + " : " + getDefaultForStorageClass(v.getStorageClass()) + ",");
				}
			}


			write("'$vtable': null,");
			write("'$name': '" + module + "',");

			
			{
				// this is only technically valid for a class
				// will need to do more legwork for interfaces
				write("'$instanceof': function (other) {");
				indent(1);
				write("if (null == other) { return true; }");
				writes("var other_vtable = other.$vtable");
				writes("var my_vtable    = " + getSafeModulePath(module) + ".$vtable");
				write("while(null != other_vtable) {" );
				indent(1);
					write("if(other_vtable == my_vtable){return true;}");
					write("other_vtable = other_vtable.$parent;");
				indent(-1);
				write("}");
				writes("return false");
				write("}");
				indent(-1);
			}
			indent(-1);
		}
		write("};");

		if (writer.m_type.equals("class"))
		{
			write("IGVM.MODULE." + getSafeModuleName(module) + ".$vtable = {");
			{
				indent(1);
				if (parent_module == null) {
					write("'$parent':\tnull");
				}
				else {
					write("'$parent':\t" + getSafeModulePath(parent_module) + ".$vtable");
				}

				// output the vtable
				for (Map.Entry<String, String> entry : my_vtable.getMap().entrySet()) {
	    			String key   = entry.getKey();
	    			String value = entry.getValue();
	    			write("," + key + ":\t" + value);
				}
				indent(-1);
			}
			write("};");
			
			// gather all the initial state for an instantiated module
			write("IGVM.OBJECT." + getSafeModuleName(module) + " = function() {");
			{
				indent(1);
				writes("this.$vtable = " + getSafeModulePath(module) + ".$vtable");

				// ummm... i should probably be doing this for all parents as well
				for (IGAsmHeader h : writer.getHeaders()) {


					//??? what about the values defined in the super class
					//??? how  do these effect prototypes?

					// if is constructor bail early
					if (h.isVariable() && !h.isStatic()) {
						IGAsmHeader.Variable v = (IGAsmHeader.Variable)h;
						writes("\tthis." + getSafeMemberName(v.getName()) + " = " + getDefaultForStorageClass(v.getStorageClass()));
					}
					
				}
				indent(-1);
			}
			write("};");
		}



		write("})(IGVM);");

		// SHOULD THROW PARENT IF IT EXISTS INTO THE PROTOTYPE CHAIN?
		// or inlude all its members in the fn

		// should all of the previous just be in the prototype?
		// probably
		//writes("IGVM.OBJECT." + getSafeModuleName(module) + ".prototype.$vtable = " +
		//		getSafeModulePath(module) + ".$vtable");

		

		// are prototypes mutable?
		// should it 

	}



	public String createArray(int storage_class) {
		// sp[0] contains the number of elements
		if (IGAsmStorageClass.INT_8 == storage_class) {
			return "IGVM.newInt8Array(" + stack(0) + "|0)";
		}
		else if (IGAsmStorageClass.INT_16 == storage_class) {
			return "IGVM.newInt16Array(" + stack(0) + "|0)";
		} 
		else if (IGAsmStorageClass.INT_32 == storage_class) {
			return "IGVM.newInt32Array(" + stack(0) + "|0)";
		} 
		else if (IGAsmStorageClass.FLOAT_64 == storage_class) {
			return "IGVM.newFloat64Array(" + stack(0) + "|0)";
		}
		else if (IGAsmStorageClass.FLOAT_32 == storage_class) {
			return "IGVM.newFloat32Array(" + stack(0) + "|0)";
		}
		else if (IGAsmStorageClass.OBJECT == storage_class) {
			return "IGVM.newObjectArray(" + stack(0) +"|0)"; // ES2015
		}
		else if (IGAsmStorageClass.FUNC_PTR == storage_class) {
			return "IGVM.newFuncPtrArray(" + stack(0) +"|0)"; // ES2015
		}
		else {
			throw new RuntimeException("Unsupported storage_class");
		}
	}

	public String copyArray(int storage_class) 
	{		
		// sp[0] contains an existing array of the appropriate type
		if (IGAsmStorageClass.INT_8 == storage_class) {
			return "IGVM.dupInt8Array(" + stack(0) + ")";
		}
		else if (IGAsmStorageClass.INT_16 == storage_class) {
			return "IGVM.dupInt16Array(" + stack(0) + ")";
		} 
		else if (IGAsmStorageClass.INT_32 == storage_class) {
			return "IGVM.dupInt32Array(" + stack(0) + ")";
		} 
		else if (IGAsmStorageClass.FLOAT_64 == storage_class) {
			return "IGVM.dupFloat64Array(" + stack(0) + ")";
		}
		else if (IGAsmStorageClass.FLOAT_32 == storage_class) {
			return "IGVM.dupFloat64Array(" + stack(0) + ")";
		}
		else if (IGAsmStorageClass.OBJECT == storage_class) {
			return "IGVM.dupObjectArray(" + stack(0) + ")"; // slice 
		}
		else if (IGAsmStorageClass.FUNC_PTR == storage_class) {
			return "IGVM.dupFuncPtrArray(" + stack(0) + ")"; // slice}
		}
		else {
			throw new RuntimeException("Unsupported storage_class");
		}
	}

	public String resizeArray(int storage_class) 
	{
		String def = "0";

// sp[0] contains an existing array of the appropriate type
		if (IGAsmStorageClass.INT_8 == storage_class) {
			return lstack(0) + "IGVM._resizzle(IGVM.newInt8Array(" + stack(1)    + "|0), " + stack(0) + ");";
		}
		else if (IGAsmStorageClass.INT_16 == storage_class) {
			return lstack(0) + "IGVM._resizzle(IGVM.newInt16Array(" + stack(1)   + "|0), " + stack(0) + ");";
		} 
		else if (IGAsmStorageClass.INT_32 == storage_class) {
			return lstack(0) + "IGVM._resizzle(IGVM.newInt32Array(" + stack(1)   + "|0), " + stack(0) + ");";
		} 
		else if (IGAsmStorageClass.FLOAT_64 == storage_class) {
			return lstack(0) + "IGVM._resizzle(IGVM.newFloat64Array(" + stack(1) + "|0), " + stack(0) + ");";
		}
			else if (IGAsmStorageClass.FLOAT_32 == storage_class) {
			return lstack(0) + "IGVM._resizzle(IGVM.newFloat32Array(" + stack(1) + "|0), " + stack(0) + ");";
		}
		else if (IGAsmStorageClass.OBJECT == storage_class) {
			def = "null";
		}
		else if (IGAsmStorageClass.FUNC_PTR == storage_class) {
			def = "null";
		}
		else {
			throw new RuntimeException("Unsupported storage_class");
		}


		return 
			"while (" + stack(0)+".length<" + stack(1) + "){" + stack(0) + ".push("+def+"); }" +
			stack(0) + ".length = " + stack(1)  + ";";

		
	}


	public String escapeString(String s) {
		StringBuilder sb = new StringBuilder();
		final int len = s.length();
		for (int i = 0; i < len; i++) {
			char c = s.charAt(i);
			if (c == '"') {
				sb.append("\\");
				sb.append("\"");
			}
			else if (c == '\\') {
				sb.append("\\");
				sb.append("\\");
			}
			else if (c == '\n') {
				sb.append("\\");
				sb.append("n");
			}
			else if (c == '\t') {
				sb.append("\\");
				sb.append("t");
			}
			else {
				sb.append(c);
			}
		}

		return sb.toString();
	}

	public String getDefaultForStorageClass(int storage_class) {
		if (IGAsmStorageClass.INT_8 == storage_class ||
			IGAsmStorageClass.INT_16 == storage_class ||
			IGAsmStorageClass.INT_32 == storage_class) {
			return "0";
		} 
		else if (IGAsmStorageClass.FLOAT_64 == storage_class ||
				 IGAsmStorageClass.FLOAT_32 == storage_class) {
			return "0.0";
		}
		else if (IGAsmStorageClass.OBJECT == storage_class ||
			IGAsmStorageClass.FUNC_PTR == storage_class) {
			return "null";
		}
		else {
			throw new RuntimeException("Unsupported storage_class");
		}
	}

	public String createStaticPointer(IGAsmOp op) {
		return "{'o':null," +
			    "'m':'" + getSafeModuleName(op.m_p0) + "'," +
			    "'f':'" + getSafeMemberName(op.m_p1) + "'}";
	}

	public String createMemberPointer(IGAsmOp op){
		return "{'o':"  + stack(0) + "," +
			    "'m':'" + getSafeModuleName(op.m_p0) + "'," +
			    "'f':'" + getSafeMemberName(op.m_p1) + "'}";
	}

	/**
	 * Process a function within a member
	 */


	public void processTargetMember(IGAsmTarget target, IGAsmHeader.Function header)
	{

		StringBuilder primary = m_out;

		clearVariables();

		m_sp = 0;
		m_out          = new StringBuilder();
		m_labels    = new HashMap<String,Label>();
		m_label_count  = 0;
	//	m_active_variables = new boolean[256];

		//String fn_name = header.getName();
		int argc = header.getArgMax();


		

		ArrayList<IGAsmOp> ops = header.m_ops;
		
		int prev_op = -1;
		int op_count = ops.size();

		// check if there are any labels defined
		boolean has_labels = false;
		for (int i = op_count - 1; i >= 0; i--) {
			if (ops.get(i).m_label != null) {
				has_labels = true;
				break;
			}
		}

		// mark the variables that are parameters
		for (int i = 0; i < argc; i++) {
			registerVariable("v" + i, true);
		}



		indent(1);

		//System.out.println("################################ " + target.m_full_path + " " + header.getName());
		
		if (has_labels)
		{
			write("var pc = 0; while(true) { switch(pc) {");
			write("case 0: ");
		}
/*
	
		for (int i = 0; i < op_count; i++)
		{
			System.out.println(ops.get(i));
		}
*/
		for (int i = 0; i < op_count; i++)
		{
			final IGAsmOp op = ops.get(i);

			if (prev_op == IGAsm.jmp         || 
				prev_op == IGAsm.__throw     || 
				prev_op == IGAsm.ret1        ||
				prev_op == IGAsm.ret0        ||
				prev_op == IGAsm.ret_funcptr ||
				prev_op == IGAsm.ret_obj)
			{
				if (op.m_op != IGAsm.END)
				{
					// need to pull pc
					if (op.m_label != null) {
						
						int sp = getSPForLabel(op.m_label);
						if (-1 != sp) {
							m_sp = sp;
						}

					}
					else {
						throw new RuntimeException("Unacessible code: " + op + " " + op.m_op);
					}
				}
			}
			

			
			prev_op = op.m_op;


			if (op.m_label != null) {
				indent(-1);
				write(LABEL(op.m_label));
				indent(1);
			}

			switch (op.m_op)
			{
				case IGAsm.nop :{ break; }
				case IGAsm.nop_term :{ break; }
				case IGAsm.pop :{ m_sp --; write("//pop"); break; }  // ..X => ..
				case IGAsm.ll0 :{ push(0);	break; } // .. => ..bp[0]
				case IGAsm.ll1 :{ push(1);	break; } // .. => ..bp[1]
				case IGAsm.ll2 :{ push(2);	break; } // .. => ..bp[2]
				case IGAsm.ll3 :{ push(3);	break; } // .. => ..bp[3]
				case IGAsm.ll4 :{ push(4);	break; } // .. => ..bp[4]
				case IGAsm.dup  : { 
					m_sp++;
					writes(lstack(0) + stack(-1));  
					break; } // ..A => ..AA
				case IGAsm.dup2 : { 
					m_sp += 2;
					writes(lstack(-1) + stack(-3));
					writes(lstack(-0) + stack(-2));  
					break; } // ..AB => ..ABAB
				case IGAsm.dup1x :{ // AB=>BAB
					m_sp ++;
					writes(lstack(0)  + stack(-1)); //AB?* => ABB*;
					writes(lstack(-1) + stack(-2)); //     => AAB*
					writes(lstack(-2) + stack(0));  //     => BAB*
					break; } 
				case IGAsm.dup2x :{ 
					m_sp ++;
					writes(lstack(0)  + stack(-1)); //ABC?* => ABCC*;
					writes(lstack(-1) + stack(-2)); //      => ABBC*
					writes(lstack(-2) + stack(-3));  //     => AABC*
					writes(lstack(-3) + stack(0));  //      => CABC*
					break; } 
				case IGAsm.ret0         :{ write("return; ");	break; }
				case IGAsm.ret1         :{ write("return " + stack(0) + ";");	break; }
				case IGAsm.ret_obj      :{ write("return " + stack(0) + ";");	break; }
				case IGAsm.ret_funcptr  :{ write("return " + stack(0) + ";");	break; }
				case IGAsm.add_i32      :{ write(ibinary("+"));	   break; }
				case IGAsm.sub_i32      :{ write(ibinary("-"));	   break; }
				case IGAsm.mul_i32      :{ write(ibinary("*"));	   break; }
				case IGAsm.div_i32      :{ write(ibinary("/"));	   break; }
				case IGAsm.mod_i32      :{ write(ibinary("%"));	   break; }
				case IGAsm.add_f64      :{ write(binary("+"));	   break; }
				case IGAsm.sub_f64      :{ write(binary("-"));	   break; }
				case IGAsm.mul_f64      :{ write(binary("*"));	   break; }
				case IGAsm.div_f64      :{ write(binary("/"));	   break; }
				case IGAsm.mod_f64      :{ write(binary("%"));	   break; }
				case IGAsm.add_f64_vec  :{ write(binary_vec("+", Integer.parseInt(op.m_p0))); break; }
				case IGAsm.sub_f64_vec  :{ write(binary_vec("-", Integer.parseInt(op.m_p0))); break; }
				case IGAsm.mul_f64_vec  :{ write(binary_vec("*", Integer.parseInt(op.m_p0))); break; }
				case IGAsm.mul_f64_vec_val  :{ write(binary_vec_val("*", Integer.parseInt(op.m_p0))); break; }
				
				case IGAsm.bit_and_i32  :{ write(ibinary("&"));	   break; }
				case IGAsm.bit_or_i32   :{ write(ibinary("|"));	   break; }
				case IGAsm.bit_xor_i32  :{ write(ibinary("^"));	   break; }
				case IGAsm.bit_not_i32  :{ write(iunary("~"));	   break; }
				case IGAsm.not          :{ write(iunary("1^"));    break; } 
				case IGAsm.lsl_i32      :{ write(ibinary("<<"));   break; }
				case IGAsm.lsr_i32      :{ write(ibinary(">>>"));  break; }
				case IGAsm.asr_i32      :{ write(ibinary(">>"));   break; }
				case IGAsm.cmp_eq_i32   :{ writes(compare("=="));  break; }
				case IGAsm.cmp_lt_i32   :{ writes(compare("<"));   break; }
				case IGAsm.cmp_le_i32   :{ writes(compare("<="));  break; }
				case IGAsm.cmp_eq_f64   :{ writes(compare("=="));  break; }
				case IGAsm.cmp_lt_f64   :{ writes(compare("<"));   break; }
				case IGAsm.cmp_le_f64   :{ writes(compare("<="));  break; }
				case IGAsm.cmp_eq_obj   :{ writes(compare("===")); break; }
				case IGAsm.cmp_eq_fp    :{ m_sp--; writes("TODO"); break; } // TODO
				case IGAsm.i32_to_f64   :{ writes(lstack(0) + "1.0*" + stack(0) ); break; }
				case IGAsm.f64_to_i32   :{ writes(lstack(0) + "Math.floor(" + stack(0) + ")"); break; }
				case IGAsm.catch_entry  :{
					m_sp++; 
					writes("pc=" + getIndexForLabel(op.m_label));
					write("continue; // terminate try catch");
					indent(-1);
					write("}}}");
					write("catch(_tmp) { " + lstack(0) + "_tmp; }");	
					break; }
				case IGAsm.array_length :{ writes(lstack(0) + stack(0) + ".length"); break; }
				
				
				case IGAsm.param_undef :{ /** do nothing **/ break; }
				case IGAsm.param_i32   :{ write("if (typeof " + var(op.m_p0) + " === 'undefined') {");
										  write("\t" + lvar(op.m_p0) + op.m_p1 + ";} /*param i32*/"); break; }
				case IGAsm.param_i64   :{ write("if (typeof " + var(op.m_p0) + " === 'undefined') {");
										  write("\t" + lvar(op.m_p0) + op.m_p1 + ";}/*param i64*/"); break; }
				case IGAsm.param_f64   :{ write("if (typeof " + var(op.m_p0) + " === 'undefined') {");
										  write("\t" + lvar(op.m_p0) + op.m_p1 + ";}"); break; }

				// these can't really have any unique values can they?  just null for the most part
				case IGAsm.param_object   :{ write("if (typeof " + var(op.m_p0) + " === 'undefined') {");
										  write("\t" + lvar(op.m_p0) + "null;}"); break; }
				case IGAsm.param_func_ptr   :{ write("if (typeof " + var(op.m_p0) + " === 'undefined') {");
										  write("\t" + lvar(op.m_p0) + "null;}"); break; }

				case IGAsm.push_f64 :{ m_sp++; writes(lstack(0) + op.m_p0); break; }
				// these need to be differentiated
				case IGAsm.push_i64 :{ m_sp++; writes(lstack(0) + op.m_p0); break; }
				// these need to be differentiated
				case IGAsm.push_i32 :{ m_sp++; writes(lstack(0) + op.m_p0); break; }
				case IGAsm.push_h32         :{ m_sp++; writes(lstack(0) + op.m_p0); break; }	// TODO
				case IGAsm.push_string      :{ m_sp++; writes(lstack(0) + 
					"IGVM.nativeToString(\"" + escapeString(op.m_p0) + "\")"); break; } // TODO
				
				case IGAsm.local_load       :{ push(Integer.parseInt(op.m_p0)); break; }
				
				case IGAsm.local_store      :{ writes(lvar(op.m_p0) + stack(0)); m_sp--; break; }
				case IGAsm.local_store_keep :{ writes(lvar(op.m_p0) + stack(0)); break; }

				case IGAsm.local_load_2 :{ 
					push(Integer.parseInt(op.m_p0));
					push(Integer.parseInt(op.m_p1));
					break; }

				// this needs to be disambiguated
				case IGAsm.local_clear  :{ 
					int storage_class = Integer.parseInt(op.m_p1);
					if (storage_class == IGAsmStorageClass.INT_8 ||
						storage_class == IGAsmStorageClass.INT_16 ||
						storage_class == IGAsmStorageClass.INT_32) {
						writes(lvar(Integer.parseInt(op.m_p0)) + " 0");
					} 
					else if (storage_class == IGAsmStorageClass.FLOAT_64 ||
							 storage_class == IGAsmStorageClass.FLOAT_32) {
						writes(lvar(Integer.parseInt(op.m_p0)) + " 0.0");
					}
					else {
						writes(lvar(Integer.parseInt(op.m_p0)) + " null");
					}
					break; 
				}	

//				case IGAsm.create_it    :{ break; }
				case IGAsm.member_store :{ m_sp -= 2; writes(stack(1) + "." + getSafeMemberName(op.m_p1) + "=" + stack(2)); break; }
				case IGAsm.member_load  :{ writes(lstack(0) + stack(0) + "." + getSafeMemberName(op.m_p1));  break; }
				case IGAsm.static_store :{ m_sp --; writes(lstatic(op.m_p0, op.m_p1) + stack(1)); break; }
				case IGAsm.static_load  :{ m_sp ++; writes(lstack(0) + rstatic(op.m_p0,op.m_p1) + "; //rstatic"); break; }
				case IGAsm.this_store   :{ m_sp --; writes(var(0) + "." + op.m_p1 + "=" + stack(1)); }
				case IGAsm.this_load    :{ m_sp ++; writes(lstack(0) + var(0) + "." + op.m_p1); break; }
				
				// load a specific field of an object
				case IGAsm.load_obj_field :{ 
					// p0 = class, p1 = name of member 
					m_sp++;
					writes(lstack(0) + var(op.m_p0) + "." + op.m_p2);
					break; }
				
				//
				case IGAsm.call_interface     :{  writes(dumpParameters(op, op.m_op, 0)); break; }
				case IGAsm.call_vtable        :{  writes(dumpParameters(op, op.m_op, 0)); break;  }
				case IGAsm.call_static        :{  writes(dumpParameters(op, op.m_op, 0)); break;  }
				case IGAsm.call_static_vtable :{  writes(dumpParameters(op, op.m_op, 0)); break;  }
				case IGAsm.call_dynamic       :{  writes(dumpParameters(op, op.m_op, 0)); break; }
				case IGAsm.call_super         :{  writes(dumpParameters(op, op.m_op, 0)); break; }
				
				case IGAsm.call_ret1_interface     :{ writes(dumpParameters(op,IGAsm.call_interface, 1)); break; }
				case IGAsm.call_ret1_vtable        :{ writes(dumpParameters(op,IGAsm.call_vtable, 1)); break; }
				case IGAsm.call_ret1_static        :{ writes(dumpParameters(op, IGAsm.call_static,1)); break; }
				case IGAsm.call_ret1_static_vtable :{ writes(dumpParameters(op, IGAsm.call_static_vtable, 1)); break; }
				case IGAsm.call_ret1_dynamic       :{ writes(dumpParameters(op, IGAsm.call_dynamic, 1)); break; }
				case IGAsm.call_ret1_super         :{ writes(dumpParameters(op, IGAsm.call_super, 1)); break; }

				
				case IGAsm.funcptr_member :{ writes(lstack(0) + createMemberPointer(op)); break; }
				case IGAsm.funcptr_static :{ m_sp++; writes(lstack(0) + createStaticPointer(op)); break; }
				
				// start of a try block.  The label on this denotes where the catch block begins
				// TODO
				case IGAsm.try_push       :{ 
											 Label l =generateLabel(op.m_p0, m_sp);
											 write("pc = 0; try { "); // reset the pc for the try catch entry);
											 write("while(pc != " + l.m_id +") { switch(pc) { case 0: //" + op.m_p0);	
											 
											 indent(1);
											 break; }


				case IGAsm.instance_of    :{ writes(lstack(0) + 
									getSafeModulePath(op.m_p0) + ".$instanceof(" + stack(0) + ") ? 1 : 0");     break; }
				
				case IGAsm.cast_to      :{ 
					write("if(!" + getSafeModulePath(op.m_p0) + ".$instanceof(" + stack(0) + ")) {");
					write("throw 'Cast Failed';");		// should generte a built in exception?
					// also our standard catch
					write("}");	// this needs to fail if the underlying object is of the wrong type
					break;
				 }

				case IGAsm.__new          :{ 
					m_sp++; 
					writes(lstack(0) + "new IGVM.OBJECT." +getSafeModuleName(op.m_p0) + "()");
					break;
				}

				case IGAsm.__throw        :{ m_sp--; writes("throw " + stack(1)); break; }
				
				case IGAsm.jmp            :{  writes(GOTO(op.m_p0));	break; }
				case IGAsm.j0_i32         :{ m_sp--;  write("if(0===" + stack(1) + "){" + GOTO(op.m_p0) + "}"); break; }
				case IGAsm.jn0_i32        :{ m_sp--;  write("if(0!==" + stack(1) + "){" + GOTO(op.m_p0) + "}"); break;}
				case IGAsm.jeq_i32        :{ m_sp-=2; write("if(" + stack(1) + "==="+stack(2) + "){" + GOTO(op.m_p0) + "}");break; }
				
				case IGAsm.jr_i32 :{ 
					m_sp-=2;
					write("if(0<="+stack(1)+"&&"+stack(1)+"<"+stack(2)+"){" + GOTO(op.m_p0)+ "}");
					break; }		// still not 100% sure (jump in range?)

				case IGAsm.and            :{ 	write("if(0===" + stack(0) + "){" + GOTO(op.m_p0) + "}");
												m_sp--;
												break; }
				

				case IGAsm.or             :{ 	write("if(0!==" + stack(0) + "){" + GOTO(op.m_p0) + "}");
												m_sp--;
												break; }
				// next_it 3 iglang.util.VectorIterator<iglang.Object> has_next iglang.util.VectorIterator<iglang.Object> next L5
				case IGAsm.next_it:
				{ 
						// has this been cleared?
						int    it_local       = Integer.parseInt(op.m_p0);
						String has_next_class = op.m_p1;
						String has_next_fn    = op.m_p2;
						String next_class     = op.m_p3;
						String next_fn        = op.m_p4;
						String exit_label     = op.m_p5;

						// check if more items are available
						writes("if (!"  + var(it_local) + "." + getSafeMemberName(has_next_fn) + ") { " + GOTO(exit_label) + "}");
						m_sp++;
						writes(lstack(0) + var(it_local) + ".$vtable." + getSafeMemberName(next_fn) + "(" + var(it_local) + ")");
						break; 
				}
				
				case IGAsm.array_load     :{ 
					m_sp --;  
					writes(lstack(0) + stack(0)+"["+stack(1)+"]");  
					break; }
				
				case IGAsm.array_store    :{ 
					m_sp -= 3; 
					writes(stack(1) + "["+stack(2)+"] = " + stack(3)); 
					break; }
				
				// array_new ACTUAL-CLASS STORAGE-CLASS # length => new-array
				case IGAsm.array_new      :{ 
					//m_sp++;
					writes(lstack(0) + createArray(Integer.parseInt(op.m_p1)));	
					break; 
				}
				case IGAsm.array_resize   :{ 
					m_sp --;
					write(resizeArray(Integer.parseInt(op.m_p0)));
					break; }

				//  array_fill storge-class # array, item => nil # check that fill exists
				case IGAsm.array_fill     :{ 
					m_sp -= 2;	writes(stack(1) + ".fill("+stack(2)+")"); break; }
				case IGAsm.array_copy     :{ 
					writes(lstack(0) + copyArray(Integer.parseInt(op.m_p0)));
					break; }	
				case IGAsm.local_inc_i8   :{ writes(var(op.m_p0) + " += " + op.m_p1); break; }
				case IGAsm.stack_inc_i8   :{ writes(stack(0) + "+=" + op.m_p0); break; }
				// might need to constrain scope
				case IGAsm.this_inc_i32   :{ writes(var(0) + "." + op.m_p1 + " += " + op.m_p2 ); break;}
				case IGAsm.asr_i32_const  :{ writes(lstack(0) + stack(0) + ">>"  + op.m_p0); break; }
				case IGAsm.lsr_i32_const  :{ writes(lstack(0) + stack(0) + ">>>" + op.m_p0); break; }
				case IGAsm.lsl_i32_const  :{ writes(lstack(0) + stack(0) + "<<"  + op.m_p0);  break; }
				case IGAsm.mla_i32           :{ m_sp -=2; writes(stack(0) + "+="+ stack(1)+"*"+stack(2)); break; }
				case IGAsm.mla_f64           :{ m_sp -=2; writes(stack(0) + "+="+ stack(1)+"*"+stack(2)); break; }
				// this is a singned extraction
				//  (x << N) >> M
				case IGAsm.lsl_asr_i32_const :{ 
						writes(lstack(0) + "(" + stack(0) + " << " + op.m_p0 + ")" + 
							" >> " + op.m_p1); 
							 break; }
				case IGAsm.switch_i32_jmp    :{ write("if(" + op.m_p0 + "===" + stack(0) + "){ "
				 								+ GOTO(op.m_p1) + "}");
												break; }
				
				// these can only be created by the optimizer
				case IGAsm.push_i32_0 :{ m_sp++; writes(lstack(0) + "0");     break; }
				case IGAsm.push_i32_1 :{ m_sp++; writes(lstack(0) + "1");     break; }
				case IGAsm.push_f64_0 :{ m_sp++; writes(lstack(0) + "0.0");   break; }
				case IGAsm.push_f64_1 :{ m_sp++; writes(lstack(0) + "1.0");   break; }
				case IGAsm.push_i8    :{ m_sp++; writes(lstack(0) + op.m_p0); break; }
				case IGAsm.push_i16   :{ m_sp++; writes(lstack(0) + op.m_p0); break; }
				case IGAsm.push_obj_null: { m_sp++; writes(lstack(0) + "null"); break; }
				case IGAsm.push_fp_null: { m_sp++; writes(lstack(0) + "null"); break; }
				case IGAsm.debug_block_start :{ break; }   // DO NOTHING
				case IGAsm.debug_block_end :  { break; }   // DO NOTHING
					
				//case IGAsm.local_store_lo :{ break; }
				case IGAsm.local_load_lo :{ push(Integer.parseInt(op.m_p0));	break; }
				
				case IGAsm.END :{ break; }
				default: {
					write("// unknown op " + op.m_op);
				}
			}
		}
		if (has_labels) {
			write("}}");
		}

		indent(-1);

		// teminte the switch and the function
		

		StringBuilder body = m_out;
		m_out = new StringBuilder();

		StringBuilder fn_decl = new StringBuilder();
		fn_decl.append("function (");
		for (int i = 0; i < argc; i++) {
			if (i != 0) {
				fn_decl.append(",");
			}
			fn_decl.append(pvar(i));
		}
		fn_decl.append("){");

		write(fn_decl.toString());


		indent(1);
		for(int i = 0; i < m_active_variables_list.size(); i++) {
			Variable v = m_active_variables_list.get(i);
			if (v.m_is_param) {
				continue;
			}

			writes ("var " + v.m_name);
		}
		indent(-1);
		m_out.append(body);

		write("}");

		// add to the primary StringBuilder for the whole project
		primary.append(m_out);
		m_out = primary;
	}
}