/*
Copyright (c) 2014-2017 Incubator Games Ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

package pass2;

import java.util.*;
import util.*;
import pass1.*;

// we're going to have to treat casting correctly 
// http://php.net/manual/en/function.intval.php

public class Php extends Processor
{

	private Hashtable<IGNode,IGNode> m_markup_skip = new Hashtable<IGNode,IGNode>();	
	
	
	/*
	 * Depth first traversal of all nodes
	 */
	 
	 
	private void markupSkip(IGNode node) {
		m_markup_skip.put(node, node);
	}
	
	private void markup(TokenWriter pw, IGSource s, IGNode node)
	{
		//System.err.println("markup: " + node.m_node_type + " " + node.m_token.m_value);
		if (markupPre(pw, s, node))
		{
			final IGNodeList children = node.m_children;
			int children_count = children.size();
			
			//for (IGNode child : node.m_children) {
			for (int i = 0; i < children_count; i++)
			{
				if (!m_markup_skip.containsKey(node)) {
					markup(pw, s, children.get(i));
				}
			}
		}
		
		//System.err.println("markup2: " + node.m_token + " " + node.m_node_type);
		markupPost(pw, s, node);
	}
	
	
	/*
	 * Marking up node
	 */
	 
	private boolean triggersReplace(IGNode pre)
	{
		if (pre.m_parent.m_node_type != IGNode.NODE_CALL) {
			return false;
		}
	
		IGNode left 	= pre.getChild(0);
		IGNode right 	= pre.getChild(1);
	
	
		if (left.m_type == Type.get("iglang.String") &&
			right.m_token.m_value.equals("indexOf")) {
	
			return true;
		}
		else if (left.m_type == Type.get("iglang.String") &&
			right.m_token.m_value.equals("substr")) {
	
			return true;
		}
		
		else if (left.m_type == Type.get("iglang.String") &&
			right.m_token.m_value.equals("split")) {
	
			return true;
		}
		else
		if (left.m_type.m_primary.equals("iglang.Vector") &&
				 right.m_token.m_value.equals("push")) 
		{
			return true;
		}
		else if (left.m_type.m_primary.equals("iglang.Vector") &&
				 right.m_token.m_value.equals("pop")) 
		{
			return true;
		}
		else if (left.m_type.m_primary.equals("iglang.Vector") &&
				 right.m_token.m_value.equals("indexOf")) 
		{
			return true;
		}
		else if (left.m_type.m_primary.equals("iglang.Vector") &&
				 right.m_token.m_value.equals("__delete")) 
		{
			return true;
		}
		else if (left.m_type.m_primary.equals("iglang.Map") &&
				 right.m_token.m_value.equals("containsKey")) 
		{
			return true;
		}
		else if (left.m_type.m_primary.equals("iglang.Map") &&
				 right.m_token.m_value.equals("deleteKey")) 
		{
			return true;	
		}
		
		return false;
	}
	
	private String getDefaultForType(Type type)
	{
			
		if (type == Type.get("int") ||
			type == Type.get("uint") ||
			type.isEnum()) {
		
			return "0";
		}
		else if (type == Type.get("double")) {
			return "0.0";
		}
		else if (type== Type.get("bool")) {
		
			return "false";
		}
		else
		{
			return "NULL";
		}	
	
	}
	
	
	private boolean markupPre(TokenWriter pw, IGSource s, IGNode node)
	{
		Token token = node.m_token;
	
		if (node.m_node_type == IGNode.NODE_ENUM_MEMBER) 
		{
			return false;
		}
		else if ((node.m_node_type ==  IGNode.NODE_TYPE_FOREACH))
		{
			IGNode value 	= node.getChild(0);
			
			markupSkip(value);
			pw.disableOutput(value);
		}
		else if (node.m_node_type == IGNode.VECTOR_INITIALIZER)
		{
			pw.disableOutput(node.getChild(0));
			markup(pw, s, node.getChild(1));

			return false;
		}
		else if (node.m_node_type == IGNode.ARRAY_INITIALIZER)
		{
			pw.disableOutput(node.getChild(0));
			markup(pw, s, node.getChild(1));
			
			
			return false;
		}
		else if (node.m_node_type == IGNode.MAP_INITIALIZER)
		{
			pw.disableOutput(node.getChild(0));
			markup(pw, s, node.getChild(1));

			return false;
		}
		
		
		
		return true;
	}
	
	private Object getAnnotation(Token t) {
		// TODO
		return null;
	}
	
	private void markupPost(TokenWriter pw, IGSource s, IGNode node)
	{
		Token token = node.m_token;
	
		if (node.m_node_type == IGNode.SCOPE_CHAIN_START) 
		{
			IGSource src = (IGSource)node.m_type.m_scope;
			pw.prepend(token, "\\" + src.m_package.toStringWithSeperator("\\","\\",false) + "\\");
		}
		else if (node.m_node_type == IGNode.NODE_ENUM_MEMBER) 
		{
			IGMember member = (IGMember)getAnnotation(token);	//token.m_annotation_object;

			pw.prepend(token, "const " + token.m_value + "  = " + member.m_return_value_range.toPHPString() + ";");
			pw.disableOutput(node);
			
			/*
			if (peek(0).m_value.equals("=")) {
				pw.disableOutput(peek(0));
				pw.disableOutput(peek(1));
				if (peek(2).m_value.equals(",")) {
					pw.disableOutput(peek(2));
				}
			}
			else if (peek(0).m_value.equals(",")) {
				pw.disableOutput(peek(0));
			}		
			*/
		}
		else if (node.m_node_type == IGNode.PACKAGE)
		{
			pw.replace(token, "namespace");
		}
		else if (node.m_node_type == IGNode.IMPORT)
		{
			pw.disableOutput(node);
		}
		else if (node.m_node_type == IGNode.NODE_CATCH) 
		{
			int i = node.m_start_index;
			pw.append(peekAt(i+1), "\\Exception ");
		}
		else if (node.m_node_type == IGNode.CLASS) {
		
			// classes in php can't be public/private/internal or protected
			int i = node.m_start_index;
			while (peekAt(i).m_type != TOK_CLASS) {
				if (peekAt(i).m_type == TOK_EOF) {
					throw new RuntimeException("Umm what? Couldn't find class tag");
				}
				pw.disableOutput(peekAt(i));
				i++;
			}
			
			if (peekAt(i + 1).m_type != TOK_EXTENDS) {
				pw.append(peekAt(i + 1), " extends \\iglang\\Object");
			}
		}
		else if (node.m_node_type == IGNode.NODE_INTERFACE) {
		
			// classes in php can't be public/private/internal or protected
			int i = node.m_start_index;
			while (peekAt(i).m_type != TOK_INTERFACE) {
				if (peekAt(i).m_type == TOK_EOF) {
					throw new RuntimeException("Umm what? Couldn't find class tag");
				}
				pw.disableOutput(peekAt(i));
				i++;
			}
			
			if (peekAt(i + 1).m_type != TOK_EXTENDS) {
				pw.append(peekAt(i + 1), " extends \\iglang\\Object");
			}
		}
		else if (node.m_node_type == IGNode.NODE_ENUM) {
		
			// classes in php can't be public/private/internal or protected
			int i = node.m_start_index;
			while (peekAt(i).m_type != TOK_ENUM) {
				if (peekAt(i).m_type == TOK_EOF) {
					throw new RuntimeException("Umm what? Couldn't find class tag");
				}
				pw.disableOutput(peekAt(i));
				i++;
			}

			// no such thing as an enum in php
			pw.replace(peekAt(i), "class");
		}
		else if (node.m_node_type ==  IGNode.NODE_TYPE_CAST)
		{
			IGNode dst_type = node;
			IGNode src_type = node.getChild(1);
			
			pw.disableOutput(node.getChild(0));
//			for (int i = token.m_token_index; i < src_type.m_start_index; i++) {
//				pw.disableOutput(peekAt(i));
//			}
			
			/*
			token.m_output = false;
			while (getTokenIndex() < value.m_start_index) {
				read().m_output = false;
			}
			*/
			
			// TODO catch this in the Validator
			//String dest_type = type(s, node.m_type);
			//if (dest_type.startsWith("source:")) {
			//	throw new RuntimeException("Borked the type on this cast");
			//}
			
			if (dst_type.m_type == Type.get("iglang.String") && 
					(
						src_type.m_type == Type.get("int") ||
					 	src_type.m_type == Type.get("uint"))
					) 
			{
				pw.append(token, "(string)");
			}
			else if (dst_type.m_type == Type.get("int") && 
					(
						src_type.m_type == Type.get("iglang.String") ||
						src_type.m_type == Type.get("int")  ||
						src_type.m_type == Type.get("uint") ||
						src_type.m_type == Type.get("double")  
					)) 
			{
				if (src_type.m_type != Type.get("iglang.String")) {
					pw.append(token, "/*WS*/(int)");
				}
				else
				{
					pw.append(token, "(int)");
				}
			}
			else
			{
				pw.append(token, "/*UNSAFE "  + dst_type.m_type + " */");
				//error(token, "Unimplemented cast");
			}
			
			//pw.append(token, "iglang::ig_cast< " + dest_type + "," + type(s, value.m_type) + " >");
		}
		else if (node.m_node_type == IGNode.CLASS_FUNCTION) 
		{
			int i = node.m_start_index;
			while (peekAt(i).m_type != TOK_FUNCTION) {
				//pw.disableOutput(peekAt(i));
				i++;
			}
			
			Token function_token = peekAt(i+0);
			//pw.disableOutput(function_token);
			
			Token id_token = peekAt(i+1);
			if (id_token.m_value.equals("<constructor>")) {
				pw.replace(id_token, "__construct");
			}
			
			if (id_token.m_type == TOK_GET ||
				id_token.m_type == TOK_SET) {
			
				pw.disableOutput(id_token);	
			}
			//pw.prepend(id_token, "$");
		}
		else if (node.m_node_type == IGNode.CLASS_STATIC_FUNCTION) 
		{
			int i = node.m_start_index;
			while (peekAt(i).m_type != TOK_FUNCTION) {
				//pw.disableOutput(peekAt(i));
				i++;
			}
			
			Token function_token = peekAt(i+0);
			//pw.disableOutput(function_token);
			
			Token id_token = peekAt(i+1);
	
			
			if (id_token.m_type == TOK_GET ||
				id_token.m_type == TOK_SET) {
			
				pw.disableOutput(id_token);	
			}
			//pw.prepend(id_token, "$");
		}		
		else if (node.m_node_type == IGNode.CLASS_VARIABLE) 
		{
			int i = node.m_start_index;
			while (peekAt(i).m_type != TOK_VAR) {
				//pw.disableOutput(peekAt(i));
				i++;
			}
			
			Token var_token = peekAt(i+0);
			pw.disableOutput(var_token);
			
			Token id_token = peekAt(i+1);
			pw.prepend(id_token, "$");
		}
		else if (node.m_node_type == IGNode.CLASS_STATIC_VARIABLE) 
		{
			IGMember  m = (IGMember)getAnnotation(node.m_token);	//.m_annotation_object;
		
			int i = node.m_start_index;
			while (peekAt(i).m_type != TOK_VAR) {
				//pw.disableOutput(peekAt(i));
				
				if (m.hasModifier(IGMember.FINAL)) {// && peekAt(i).m_type == TOK_STATIC) {
					pw.disableOutput(peekAt(i));
				}
				
				i++;
			}
			
			Token var_token = peekAt(i+0);
			pw.disableOutput(var_token);
			
			if (m.hasModifier(IGMember.FINAL)) {
				Token id_token = peekAt(i+1);
				pw.prepend(id_token, "const ");		
			}
			else
			{
				Token id_token = peekAt(i+1);
				pw.prepend(id_token, "$");
			}
		}
		else if (node.m_node_type == IGNode.NODE_BINARY && token.m_type == TOK_LSR) {
			pw.replace(token, ">>>");
		}
		else if (node.m_node_type == IGNode.NODE_BINARY && token.m_type == TOK_ASR) {
			pw.replace(token, ">>");
		}
		else if (node.m_node_type == IGNode.NODE_BINARY && token.m_type == TOK_LSL) {
			pw.replace(token, "<<");
		}
		else if (token.m_type == IGLexer.LEXER_NATIVE_BLOCK) 
		{
			if (token.m_value.startsWith("@php")) {
				pw.append(token, token.m_value.substring(4));
			}
			token.m_output = false;
		}
		else if (node.m_node_type == IGNode.NODE_VALUE) {
			if (token.m_type == IGLexer.LEXER_STRING) 
			{
				if (token.m_value.indexOf("\\") != -1 || token.m_value.indexOf("\'") != -1)
				{
					pw.prepend(token, "\"");
					pw.append(token, "\"");
				}
				else
				{
					pw.prepend(token, "\'");
					pw.append(token, "\'");
				}
			}
			else if (token.m_type == IGLexer.LEXER_CHAR)
			{
				pw.replace(token, "/*" + token.m_value + "*/" + (int)token.m_value.charAt(0));
			}
			else if (token.m_type == TOK_NULL) {
				pw.replace(token, "NULL");
			}
		}
		else if (node.m_node_type == IGNode.NODE_BINARY) {
			IGNode left = node.getChild(0);
			IGNode right = node.getChild(1);
			
			if (token.m_type == TOK_ADD) {
				if (left.m_type == Type.get("iglang.String") ||
					right.m_type == Type.get("iglang.String")) {
			
					token.m_output = false;
					pw.append(token, ".");	
				}
			}
			
			// UGH.. string concatenation is parsed RIGHT TO LEFT
			// instead of LEFT TO RIGHT
		}
		else if ((node.m_node_type ==  IGNode.NODE_TYPE_FOREACH))
		{
			IGNode value 	= node.getChild(0);
			IGNode data_set = node.getChild(1);
		
			IGVariable v = null;
		
			if (value.m_node_type == IGNode.NODE_TYPE_LOCAL_VARIABLE_DEFINITION) 
			{
				v = (IGVariable)value.m_scope;
			}
			else if (value.m_node_type == IGNode.LOCAL_VARIABLE_ACCESS)
			{
				v = (IGVariable)value.m_scope;					
			}
			else
			{
				error(token, "Invalid value variable: " + value.m_type);
			}
			
			
			
			
			String tmp_name = "$__ig_tmp" + token.m_token_index;
			String value_name = "$" + v.m_name.m_value;
			if (v.m_occurrance_count > 0)
			{
				value_name += "__" + v.m_occurrance_count;
			}
		
		
			pw.replace(token, "foreach");
			
			if (data_set.m_type.m_primary.equals("iglang.Map")) {
				pw.append(data_set, " as " + value_name + " => " + tmp_name);
			}
			else // Vector and Aarray
			{
				pw.append(data_set, " as " + tmp_name + " => " + value_name);
			}
		}	
		else if (node.m_node_type == IGNode.NODE_TYPE_FOREACH_IN)
		{
			pw.disableOutput(token);
			//pw.append(node, " as ");
		
		}
		
		else if (node.m_node_type == IGNode.NODE_TYPE_LOCAL_VARIABLE_DEFINITION)
		{	
			token.m_output = false;
		}
		else if (node.m_node_type == IGNode.ID_TYPE_PAIR) 
		{
			IGNode id   = node.getChild(0);
			IGNode type = node.getChild(1);
			
			token.m_output = false;
			pw.append(id, "/*" + type.m_type + "*/ ");
			pw.disableOutput(type);
			
		}
		else if (node.m_node_type == IGNode.FUNCTION_RETURN_TYPE)
		{
			pw.disableOutput(node);
		}
		else if (node.m_node_type == IGNode.STATIC_FIELD_ACCESS)
		{
		
			IGMember scope_item = (IGMember)node.m_scope;
			if (scope_item.getScopeType() == IGScopeItem.SCOPE_ITEM_STATIC_VARIABLE &&
				!scope_item.hasModifier(IGMember.FINAL)) {
				pw.prepend(token, "\\" + s.getScopePath().toStringWithSeperator("\\","\\", false) + "::$");
			}
			else
			{
				pw.prepend(token, "\\" + s.getScopePath().toStringWithSeperator("\\","\\", false) + "::");
			}
		}
		else if (node.m_node_type == IGNode.ARRAY_INITIALIZER_LIST ||
				 node.m_node_type == IGNode.VECTOR_INITIALIZER_LIST ||
				 node.m_node_type == IGNode.MAP_INITIALIZER_LIST) {
		
			pw.prepend(node.m_start_index, "array(");
			pw.disableOutput(node.m_end_index - 1);
			pw.append(node.m_end_index - 1, ")");
			token.m_output = false;		 
		}
		else if (node.m_node_type == IGNode.MAP_INITIALIZER_SEPARATOR) {
			pw.disableOutput(token);
			pw.append(token, " => ");
		}
		else if (node.m_node_type == IGNode.FIELD_ACCESS)
		{
			if (!node.m_type.isNamespace()) {
				pw.prepend(token, "$this->");
			}
			else
			{
				if (node.m_type.isNamespace()) {
					IGSource s2 = (IGSource)node.m_type.m_scope;
					pw.replace(token, "\\" + s2.getScopePath().toStringWithSeperator("\\","\\", false));
				}
			}
		}
		else if (node.m_node_type == IGNode.NODE_TYPE_LOCAL_VARIABLE_NAME) 
		{
			pw.prepend(token, "$");
			IGVariable v = (IGVariable) token.m_node.m_scope;

			if (v.m_occurrance_count > 0 && !token.m_value.equals("this") && !token.m_value.equals("super"))
			{
				pw.append(token,"__" + v.m_occurrance_count);
			}
		}
		else if (node.m_node_type == IGNode.LOCAL_VARIABLE_ACCESS) 
		{
			pw.prepend(token, "$");
			IGVariable v = (IGVariable) token.m_node.m_scope;

			if (v.m_occurrance_count > 0 && !token.m_value.equals("this") && !token.m_value.equals("super"))
			{
				pw.append(token,"__" + v.m_occurrance_count);
			}
		}
		else if (node.m_node_type  == IGNode.NODE_NEW)
		{
			IGNode left = node.getChild(0);
			IGNode right = node.getChild(1);
			
			if (left.m_type.m_primary.equals("iglang.Array")  ||
				left.m_type.m_primary.equals("iglang.Vector")) 
			{
				token.m_output = false;
				pw.disableOutput(left);
				
				
				// okay we need to handle vectors that don't have any elements
				
				
				pw.prepend(token, "/*" + right.getChildCount() + "*/");
				
				if (right.getChild(0).getChildCount() == 0) {
					pw.append(token, "array");
				}
				else
				{
					pw.append(token, "__ig_newArray(");
					
					String defaux = getDefaultForType(left.m_type.m_left);
					
					pw.append(node.m_end_index - 1, "," + defaux + ")");
	
				}
			}
			else if (left.m_type.m_primary.equals("iglang.Map")) 
			{
				token.m_output = false;
				pw.disableOutput(left);
				pw.append(token, "array");
			}
			else if (left.m_type.m_primary.equals("iglang.Error"))
			{
				pw.disableOutput(left);
				//token.m_output = false;
				pw.append(token, " \\Exception");
			}
			else
			{
				pw.disableOutput(left);
				
				IGSource s2 = (IGSource) left.m_type.m_scope;
				pw.append(left, "\\" + s2.getScopePath().toStringWithSeperator("\\","\\", false));
			
			}
		}
		else if (node.m_node_type ==  IGNode.NODE_DOT)
		{
			if (!triggersReplace(node)) {
		
				IGNode left  = node.getChild(0);
				IGNode right = node.getChild(1);
			
				Type   left_type = left.m_type;
				if (left_type.isNamespace()) {
					pw.append(token, "::");
				}
				else
				{
					pw.append(token, "->");
				}
				token.m_output = false;
			}
		}
		else if (node.m_node_type == IGNode.NODE_CALL)
		{
			IGNode pre 		= node.getChild(0);
			
			if (pre.getChildCount() > 1)
			{
				IGNode left 	= pre.getChild(0);
				IGNode right 	= pre.getChild(1);
			
			
				if (left.m_type == Type.get("iglang.String") &&
					right.m_token.m_value.equals("indexOf")) {
			
					pw.replace(pre.m_token, ",");
			
					// this isn't technically correct
					pw.prepend(node.m_start_index, "__ig_stringIndexOf(");
					token.m_output = false;
					right.m_token.m_output = false;	
				
					//pw.append(node.m_end_index - 1, ")");
				}
				else if (left.m_type == Type.get("iglang.String") &&
					right.m_token.m_value.equals("substr")) {
			
					pw.replace(pre.m_token, ",");
			
					// this isn't technically correct
					pw.prepend(node.m_start_index, "mb_substr(");
					token.m_output = false;
					right.m_token.m_output = false;	
				
					//pw.append(node.m_end_index - 1, ")");
				}
				else if (left.m_type == Type.get("iglang.String") &&
					right.m_token.m_value.equals("split")) {
			
					pw.replace(pre.m_token, ",");
			
					// this isn't technically correct
					pw.prepend(node.m_start_index, "__ig_split(");
					token.m_output = false;
					right.m_token.m_output = false;	
				
					//pw.append(node.m_end_index - 1, ")");
				}
				else if (left.m_type.m_primary.equals("iglang.Vector") &&
						 right.m_token.m_value.equals("push")) 
				{
				
					pw.replace(pre.m_token, ",");
					pw.prepend(node.m_start_index, "array_push(");
					token.m_output = false;
					right.m_token.m_output = false;	
				}
				else if (left.m_type.m_primary.equals("iglang.Vector") &&
						 right.m_token.m_value.equals("pop")) 
				{
					// just get rid of the "." altogether
					pw.replace(pre.m_token, "");
					
					pw.prepend(node.m_start_index, "array_pop(");
					token.m_output = false;
					right.m_token.m_output = false;	
				}
				else if (left.m_type.m_primary.equals("iglang.Vector") &&
						 right.m_token.m_value.equals("indexOf")) 
				{
					pw.replace(pre.m_token, ",");
					pw.prepend(node.m_start_index, "__ig_indexOf(");
					token.m_output = false;
					right.m_token.m_output = false;	
				}
				else if (left.m_type.m_primary.equals("iglang.Vector") &&
						 right.m_token.m_value.equals("__delete")) 
				{
					pw.replace(pre.m_token, ",");
					pw.prepend(node.m_start_index, "__ig_delete(");
					token.m_output = false;
					right.m_token.m_output = false;	
				}
				else if (left.m_type.m_primary.equals("iglang.Map") &&
						 right.m_token.m_value.equals("containsKey")) 
				{
					pw.replace(pre.m_token, ",");
					pw.prepend(node.m_start_index, "__ig_containsKey(");
					token.m_output = false;
					right.m_token.m_output = false;	
				}
				else if (left.m_type.m_primary.equals("iglang.Map") &&
						 right.m_token.m_value.equals("deleteKey")) 
				{
					pw.replace(pre.m_token, ",");
					pw.prepend(node.m_start_index, "__ig_deleteKey(");
					token.m_output = false;
					right.m_token.m_output = false;	
				}
			}

		}
		else if ((node.m_node_type == IGNode.DOT_ACCESSOR || node.m_node_type == IGNode.STATIC_DOT_ACCESSOR) && !node.isLValue())
		{
			IGNode left = node.getChild(0);
			IGNode right = node.getChild(1);
			
			
			/////////////////////
			// String length
			/////////////////////
			
			
			if ( left.m_type == Type.get("iglang.String") &&
				right.m_token.m_value.equals("length")) {
				
				pw.prepend(node.m_start_index, "mb_strlen(");
				token.m_output = false;
				right.m_token.m_output = false;	
				
				pw.append(node.m_end_index - 1, ")");
			}
			
			/////////////////////
			// Array and vector length
			///////////////////////
			
			else if ((left.m_type.m_primary.equals("iglang.Vector") ||
					 left.m_type.m_primary.equals("iglang.Array"))  &&
					 right.m_token.m_value.equals("length")
			)
			{
				pw.prepend(node.m_start_index, "count(");
				token.m_output = false;
				right.m_token.m_output = false;	
				
				pw.append(node.m_end_index - 1, ")");
			}
			else
			{
				if (left.m_type.isNamespace()) {
					pw.replace(token, "::");
				}
				else {
					pw.replace(token, "->");
				}
				pw.append(node, "__get()");
				
			}
		}
		else if ((node.m_node_type == IGNode.DOT_ACCESSOR || node.m_node_type == IGNode.STATIC_DOT_ACCESSOR ) && node.isLValue())
		{
			IGNode left = node.getChild(0);
			IGNode right = node.getChild(1);
			
			IGNode parent = node.m_parent;
			
			if ((left.m_type.m_primary.equals("iglang.Vector") ||
					 left.m_type.m_primary.equals("iglang.Array"))  &&
					 right.m_token.m_value.equals("length")
			)
			{
				pw.prepend(node.m_start_index, "__ig_setArrayLength(");
				token.m_output = false;
				right.m_token.m_output = false;	
				
				pw.replace(parent.m_token, ",");
				
				
				String defaux = getDefaultForType(left.m_type.m_left);
				
				pw.append(parent.m_end_index - 1, "," + defaux + ")");
			}
			else
			{
				
			
				if (left.m_type.isNamespace()) {
					pw.replace(token, "::");
				}
				else {
					pw.replace(token, "->");
				}
				
				pw.append(right, "__set(");
				pw.disableOutput(parent.m_token);
				pw.append(parent, ")");
			}
		}
		//else if (node.m_node_type == IGNode.NODE_TYPE_MEMBER_VARIABLE_NAME)
		//{
		//
		//}
	}


	public  void run(IGPackage root, Vector<IGSource> sources)
	{	
		// create the header
		
		try
		{
			for (IGSource s: sources)
			{
				if (pass1.IGSettings.s_verbose) {
					System.err.println("Php.run " + s.m_expected_component);
				}
				
				TokenWriter pw = Util.createOutputPrintWriter(s, ".php");
				setSource(s.m_token_list, 0, s.m_token_list.length);
				
				if (s.m_type == IGScopeItem.SCOPE_ITEM_ENUM)
				{
					markup(pw, s, s.m_root_node);
				}
				else if (s.m_type == IGScopeItem.SCOPE_ITEM_INTERFACE )
				{
					markup(pw, s, s.m_root_node);
				}
				else if (s.m_type == IGScopeItem.SCOPE_ITEM_CLASS)
				{
					markup(pw, s, s.m_root_node);
					
					/*
					for (IGMember m : s.m_members) 
					{
						IGNode body_node = m.m_node;
						
						int start_token_index = body_node.m_start_index;
						int end_token_index   = body_node.m_end_index;

						markup(pw, s, body_node);
					}
					*/
				}
				else
				{
					throw new RuntimeException("Unknown source type.");
				}
				
				//System.out.println("output " + s.m_dst_path + ".php");
				
				pw.print("<?php ");
				pw.printRange(s.m_token_list,  0, s.m_token_list.length);
				pw.close();
			}
		}
		catch (Exception ex)
		{
			System.err.print("php export failed");
			ex.printStackTrace();
			System.exit(1);
		}
	}
}