/*
Copyright (c) 2014-2017 Incubator Games Ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
package pass2;

import pass1.*;
import java.util.*;



/**
 	For java version could just, use a context remap of iglang.String to java.lang.String
 */

public final class IGTemplateUtil
{
		
	private HashMap<String, IGTemplateVariant> m_template_variants = new HashMap<String,IGTemplateVariant>();
	private boolean m_has_unprocessed_template_variants = true;
	
	
	/**
	 * Get a variant identified by the template class path.  e.g.  iglang.Vector
	 */
	
	public IGTemplateVariant getVariant(String primary_key) {
		if (!m_template_variants.containsKey(primary_key)) {
			return null;
		}
		return m_template_variants.get(primary_key);
	}
	
	/**
	 * Add to the list any new template variants newly discovered.
	 * This scans the entire contents of the Types database and checks if anything is missing
	 */
	
	public boolean augmentTemplateVariants()
	{
		m_has_unprocessed_template_variants = false;
	
		ArrayList<Type> templated_types = new ArrayList<Type>();
		Type.getTemplatedTypes(templated_types);
	
		// group the list of template variants by their primary type
		for (Type t: templated_types) {
			if (!m_template_variants.containsKey(t.m_primary)) {
				m_template_variants.put(t.m_primary, new IGTemplateVariant());
			}
			
			IGTemplateVariant tv = m_template_variants.get(t.m_primary);
			if (tv.add(t)) {
				m_has_unprocessed_template_variants = true;
			}
		}	
		
		return m_has_unprocessed_template_variants;
	}


	static class IGTemplateVariant
	{
		Vector<Type> m_lefts  = new Vector<Type>();
		Vector<Type> m_rights = new Vector<Type>();		
		
		
		HashMap<String,Type> m_existing = new HashMap<String,Type>();
		
		public void mark() 
		{
			// clear out all the processed variants
			m_lefts  = new Vector<Type>();
			m_rights = new Vector<Type>();			
		}
		
		public boolean add(Type t) 
		{
			Type left = t.m_left;
			Type right = t.m_right;
			
			// throw out templated stubs
			if (left == Type.get("iglang.T1") || 
				left == Type.get("iglang.T2") || 
				right == Type.get("iglang.T1") || 
			    right == Type.get("iglang.T2")) {
			 	return false;   
			}
			
			if (left.m_primary.equals("Function") || left.m_primary.equals("StaticFunction") || left.m_primary.equals("MemberFunction")) {
				left = Type.get("iglang.util.igDelegate");		// ?? should this even exist?
			}
			
			if (left.m_scope != null)
			{
				if (left.m_scope.getScopeType() == IGScopeItem.SCOPE_ITEM_CLASS ||
					left.m_scope.getScopeType() == IGScopeItem.SCOPE_ITEM_INTERFACE) {
					left = Type.OBJECT; 
				}
			}
			
			if (right != null)
			{
				if (right.m_primary.equals("Function") || right.m_primary.equals("StaticFunction") || left.m_primary.equals("MemberFunction")) {
					right = Type.get("iglang.util.igDelegate");	// ?? should this even exist
				}
				
				if (right.m_scope != null) {
					if (right.m_scope.getScopeType() == IGScopeItem.SCOPE_ITEM_CLASS ||
						right.m_scope.getScopeType() == IGScopeItem.SCOPE_ITEM_INTERFACE) {
						right = Type.OBJECT;
					}
				}
			}
			
			String key = left + "|" + right;
			if (!m_existing.containsKey(key)) {
			
				m_existing.put(key, t);
				m_lefts.add(left);
				m_rights.add(right);
				
				return true;
			}
			
			return false;
		}
		
		
	}

}