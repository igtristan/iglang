/*
Copyright (c) 2014-2017 Incubator Games Ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

package pass2.asm;

import java.util.*;

public abstract class IGAsmHeader
{
	public String [] m_header = null;
	public ArrayList<IGAsmOp> m_raw_ops = null;
	public ArrayList<IGAsmOp> m_ops = null;
	public IGSet m_dependencies = null;

	public int m_inlineable = 0;   // 0 == undefined, 1 == YES,  2 == NO
	
	public boolean isFunction() {
		return m_header[0].equals("function") || m_header[0].equals("static-function");
	}

	public boolean isVariable() {
		return !isFunction();
	}

	public boolean isStatic() {
		return m_header[0].equals("static-var") || m_header[0].equals("static-function");
	}

	public boolean isStaticVariable(String name) {
		return m_header[0].equals("static-var") && m_header[2].equals(name);
	}

	public String getName() {
		return m_header[2];
	}
	
	
	/**
	 * A Header defining that a function follows
	 * TYPE PERMISSIONS FUNCTION-NAME ARG-MIN..ARG-MAX CONSERVATIVE-LOCAL-COUNT RETURN-COUNT 0 FINAL
	 * TYPE = static-function, function
	 * PERMISSIONS = public, private, protected, internal

	 */
	public static class Function extends IGAsmHeader
	{
		int m_arg_min;
		int m_arg_max;

		public Function(String [] h, ArrayList<IGAsmOp> ops) {
			m_header = h;
			m_ops = ops;

			int dot_idx = h[3].indexOf(".");
			m_arg_min = Integer.parseInt(h[3].substring(0,dot_idx));
			m_arg_max = Integer.parseInt(h[3].substring(dot_idx + 2));
		}

		public int getArgMin() {
			//m_header[3]
			return m_arg_min;
		}

		public int getArgMax() {
			return m_arg_max;
		}

	}

	public static class Variable extends IGAsmHeader
	{
		/*
		sb[1] = (permissions);
					sb[2] = (m.m_name.m_value);
					sb[3] = (m_ctx.resolveTypeDoNotCollapse(m.m_return_type).toTypeString());
					sb[4] = (m_ctx.resolveType(m.m_return_type).toTypeString());
					sb[5] = (intToString(storage_class));
		*/
		public Variable(String [] h) {
			m_header = h;
		}

		public String getType() {
			return m_header[3];
		}

		public String getCollapsedType() {
			return m_header[4];
		}

		public int getStorageClass() {
			return Integer.parseInt(m_header[5]);
		}
	}



	

}