/*
Copyright (c) 2014-2017 Incubator Games Ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

/*
	TODO:
		Rename to IGAsmDocument
*/
package pass2.asm;

import pass2.*;
import java.util.*;

import java.io.PrintWriter;


public final class IGAsmDocument
{
	public static final int MODE_WRITE_DISABLED = 0;
	public static final int MODE_WRITE_HEADERS  = 1;
	public static final int MODE_WRITE_BODY     = 2;


	//private PrintWriter m_tw;

	// this is for building up the document

	private IGSet m_current_dependencies = new IGSet();
	private ArrayList<IGAsmOp> m_asm = null;
	private IGAsmHeader m_current_header = null;
	private int m_mode = MODE_WRITE_HEADERS;
	
	
	public IGSet m_dependencies = new IGSet();
	public String m_extends_fully_qualified = null;	// ie. iglang.Vector<int>
	public String m_name_fully_qualified = null;	// ie. iglang.Vector<int>?
	public String m_type;		// enum, class, interface

	// used currently by the AS3 target
	private IGSet              m_static_dependencies = new IGSet();


	// used by IGAssembler
	ArrayList<String []>   m_meta = new ArrayList<String [] >();
	ArrayList<IGAsmHeader> m_headers = new ArrayList<IGAsmHeader>();
	int m_line_number = 0;
	
	// temporary 16 entry mirrored circular buffer	
	//private static IGAsmOp [] s_C = new IGAsmOp[32];
	
	public IGAsmDocument(int mode) {
		m_mode = mode;
	}
	
	public boolean containsStaticVariable(String name) {
		for (int i = m_headers.size() - 1; i >= 0; i--) {
			if (m_headers.get(i).isStaticVariable(name)) {
				return true;
			}
		}
		return false;
	}
	public ArrayList<IGAsmHeader> getHeaders() {
		return m_headers;
	}

	public IGSet getDependencies() {
		return m_dependencies;
	}

	public IGSet getStaticDependencies() {
		return m_static_dependencies;
	}

	public static boolean DISABLE_OPTIMIZATIONS= false;
	
	
	private boolean inRange(String v, int min, int max) {
		long v2 = Long.parseLong(v);
		if (v2 < min || v2 > max) { return false; }
		return true;
	}
	
	public boolean numInRange(String n, int min, int max)
	{
		// TODO VERIFY THAT THIS IS NOT AN 32 REF FIELD
		long value = Long.parseLong(n);
		if (value < min) { return false; }
		if (value > max) { return false; }
		return true;
	}


	public String intToString(int i) {
		return Integer.toString(i);
	}
	
	public void processAsm(Hashtable<String, IGAsmTarget> targets, IGAsmHeader header)
	{
		ArrayList<IGAsmOp> out_asm = new ArrayList<IGAsmOp>();
	
		boolean is_member_function = header.m_header[0].equals("function");
		
		header.m_ops = out_asm;
	
		///////////////////
		// Optimization list
		
		ArrayList<IGAsmOp> in2_asm = new ArrayList<IGAsmOp>();
		ArrayList<IGAsmOp> in_asm = header.m_raw_ops;
		final int          in_asm_sz = in_asm.size();
		
		
		boolean found_terminator = false;
		
		for (int i = 0; i < in_asm_sz; i++)  
		{
			IGAsmOp op = in_asm.get(i);
			
			
			if ((IGAsm.call_ret1_static_vtable == op.m_op ||
				 IGAsm.call_ret1_static        == op.m_op ||
				 IGAsm.call_ret1_vtable        == op.m_op) && op.m_p0.equals("1")) {
				
				IGAsmTarget target = targets.get(op.m_p1);
				if (target == null) {
					System.out.println("WTF failed to find: " + op.m_p1);
					System.out.println("op: " + op);
					throw new Error("Fatal Exception");
				}
				if (target != null) 
				{
					//System.out.println("findInlineCandidateStaticFunction1to1 " + op.m_p1 + " " + op.m_p2);
					IGAsmHeader inline_header = target.findInlineCandidateStaticFunction1to1	(op.m_p2);
					if (inline_header != null) 
					{	
						
						in2_asm.add(IGAsmOp.make_op_comment(IGAsm.nop, "inline"));
						
						
						
						int rsz =  inline_header.m_raw_ops.size();
						
						//System.out.println("\tlabel: " + op.m_label + " rsz: " + rsz);
						
						if (op.m_label != null) {
							in2_asm.add(IGAsmOp.make_op("nop", op.m_label));
							//in2_asm.add(IGAsmOp.make_label(op.m_label));
						}
						
						if (rsz == 4)
						{
							
						}
						else
						{
						
							if (inline_header.m_raw_ops.get(1).m_op == IGAsm.this_load) {
								//IGAsmOp inline_op = inline_header.m_raw_ops.get(1).inlineClone();
								//inline_op.m_op = "member_load";
								//in2_asm.add(inline_op);
								
								throw new RuntimeException("Unsupported case");
							}
							
							for (int j = 2; j < rsz - 2; j++) 
							{
								IGAsmOp inline_op = inline_header.m_raw_ops.get(j);
								if (inline_op.m_label != null) {
									throw new RuntimeException("Invalid inline. Contents have label: " + inline_op.m_label);
								}
								
								in2_asm.add(inline_op.inlineClone());
							}
						}
					
						continue;
						
					}
				}
			}
			
			in2_asm.add(new IGAsmOp(op));
		}
		
		

		// create a circular buffer of ops
		final int REPT = 16;				// <- number of items in circular buffer
		final int M    = 0xf;				// <- MASK into circular buffer
		final int READ_FORWARD = 10;		// 

		IGAsmOp C [] = new IGAsmOp[REPT * 2];	
		//for (int i = 0; i < in2_asm_sz; i++)  {
		//	IGAsmOp op = in2_asm.get(i);
		//	C[i] = op;
		//}
		


		// pre populate the mirrored circular buffer
		int in2_asm_sz = in2_asm.size();	
		for (int i = 0; i < READ_FORWARD; i++) {
			C[i + 0]    = (i < in2_asm_sz) ? in2_asm.get(i) : null;
			C[i + REPT] = C[i];
		}
		

		boolean found_end = false;
		
		//IGAsmOp prev_op = null;
		
		for (int ii = 0; ii < in2_asm_sz; ii++) 
		{
			final int j = ii & M;
			//final int i = ii;
			
			// update the circular buffer of operands
			IGAsmOp forward = ((ii +READ_FORWARD) < in2_asm_sz) ? in2_asm.get(ii + READ_FORWARD) : null;
			C[((ii+READ_FORWARD)&M) + 0   ] = forward;
			C[((ii+READ_FORWARD)&M) + REPT] = forward;
			
			//System.out.println("flooding: " + ((ii+7)&M) + " " + (((ii+7)&M) + 8));
			
			IGAsmOp curr_op =  C[j+0];
			
			if (DISABLE_OPTIMIZATIONS) {
				out_asm.add(curr_op);
				continue;
			}
			
			
			IGAsmOp next_op  = C[j+1];	
			IGAsmOp next2_op = C[j+2];   
			IGAsmOp next3_op = C[j+3];  
	
			final int  op = curr_op.m_op;
			
			final int    iop = op; //-1;


			if (iop == IGAsm.catch_entry) {	//"catch_entry".equals(op)) {
				found_terminator = false;
			}
			
			if (found_terminator && curr_op.m_label == null && iop != IGAsm.END) { //!"END".equals(curr_op.m_op) ) { 
				continue;
			}
			else {
				found_terminator = false;
			}
			
			if (iop == IGAsm.END) {	//"END".equals(op)) {
			
				found_end = true;
			}
			else if (iop == IGAsm.nop)	{ //"nop".equals(curr_op.m_op)) {
				
				if (curr_op.m_label == null) {
					continue;
				}
				else
				if (curr_op.m_label != null && 
					next_op.m_label == null && next_op.m_op != -1 && next_op.m_op != IGAsm.END) {
					
					//System.out.println("nudged src  " + curr_op);
					//System.out.println("nudged dst: \t" + next_op + "\n\t" + next2_op + "\n\t" + next3_op);
					
					next_op.m_label = curr_op.m_label;
					curr_op.m_label = null;
					
					
					continue;
				}
				
			}
			
			
			
			// (not, j0_i32) => (jn0_i32)
			else if (iop == IGAsm.not) 
			{ 
				if (next_op != null && next_op.m_label == null && (IGAsm.j0_i32 == next_op.m_op)) 
				{
					next_op.m_label = curr_op.m_label;
					next_op.m_op    = IGAsm.jn0_i32;
					
					// TOSS the 'NOT'
					continue;
				}
			}
			// (cmp_eq_i32, not, j0_i32) =>  (jeq_i32)
			else if (iop == IGAsm.cmp_eq_i32 && 
					 (IGAsm.not    == next_op.m_op)     && next_op.m_label == null &&
					 (IGAsm.j0_i32 == next2_op.m_op)    && next2_op.m_label == null)
			{
				next_op.m_op     = IGAsm.nop;
				next2_op.m_op    = IGAsm.jeq_i32;
				next2_op.m_label = curr_op.m_label;
			
				// TOSS the current op
				continue;
			}
			// (cmp_eq_i32, jn0_i32) => (jeq_i32)
			else if (iop == IGAsm.cmp_eq_i32 && 
					 (IGAsm.jn0_i32 == next_op.m_op) && next_op.m_label == null)
			{
				next_op.m_op = IGAsm.jeq_i32;
				next_op.m_label = curr_op.m_label;
				next_op.m_comment += "p2";
				// toss away the cmp_eq_i32
				continue;
			}
			
			//
			else if (iop == IGAsm.cmp_eq_obj)		
			{
				if (IGAsm.jn0_i32 == next_op.m_op && next_op.m_label == null) 
				{
					next_op.m_op    = IGAsm.jeq_i32;
					next_op.m_label = curr_op.m_label;
					next_op.m_comment += "p0";
			
					// toss away the cmp_eq_i32
					continue;
				}
				else if (
					(IGAsm.not    == next_op.m_op)  && next_op.m_label == null &&
					(IGAsm.j0_i32 == next2_op.m_op) && next2_op.m_label == null) 
				{
					next_op.m_op     = IGAsm.nop;
					next2_op.m_op    = IGAsm.jeq_i32;
					next2_op.m_label = curr_op.m_label;
					next2_op.m_comment += "p1";			
			
					continue;
				}
				else {
					//curr_op.m_comment += " TERDS";
				}
			}
			/* @299		dup
@299		push_i32 64001
@299		jeq_i32 L64
            */
    
			else if (IGAsm.dup == op &&
					 (IGAsm.push_i32 == next_op.m_op || IGAsm.push_i8 == next_op.m_op) && next_op.m_label == null &&
					 IGAsm.cmp_eq_i32 == next2_op.m_op && next2_op.m_label == null &&
					 IGAsm.jn0_i32  == next3_op.m_op && next3_op.m_label == null) {
			
				next3_op.m_op    = IGAsm.switch_i32_jmp;
				next3_op.m_p1    = next3_op.m_p0;
				next3_op.m_p0    = next_op.m_p0;
				next3_op.m_label = curr_op.m_label;
				
				next_op.m_op = IGAsm.nop;
				next2_op.m_op = IGAsm.nop;
				
				continue;		 
			}

			else if ( IGAsm.dup == iop && // "dup".equals(curr_op.m_op)         && 
					  (IGAsm.local_store==next_op.m_op) && next_op.m_label == null &&
					  (IGAsm.pop==next2_op.m_op)        && next2_op.m_label == null)
			{
				if ((IGAsm.local_load==next3_op.m_op) && next3_op.m_label == null && next3_op.m_p0.equals(next_op.m_p0)
						&& Integer.parseInt(next_op.m_p0) <= 127) {
				
					next2_op.m_op = IGAsm.nop;
					next3_op.m_op = IGAsm.nop;   next3_op.m_p0 = null;
					//next_op.m_comment += " Eliminated excess load rel. 2";
					next_op.m_label = curr_op.m_label;
					next_op.m_op = (IGAsm.local_store_keep);
					
					// TOSS AWAY THE DUP
					continue;
				}
				
				
				else {
					next_op.m_label = curr_op.m_label;
					next2_op.m_op = IGAsm.nop;
					
					// no need to perform this opcode now
					//int idx = Integer.parseInt(next_op.m_p0);
					//if (idx <= 255) {
					//	next_op.m_op = IGAsm.local_store_lo;
					//}
					
					// toss away current DUP
					continue;
				}
				
				
			}
			else if ( IGAsm.dup == iop && // "dup".equals(curr_op.m_op) && 
					  (IGAsm.this_store==next_op.m_op) && next_op.m_label == null &&
					  (IGAsm.pop==next2_op.m_op) && next2_op.m_label == null)
			{
				
				next_op .m_label = curr_op.m_label;
				next2_op.m_op = IGAsm.nop;
				
				// toss away current DUP
				continue;
			}
			else if ( IGAsm.dup == iop && // "dup".equals(curr_op.m_op) && 
					  (IGAsm.static_store==next_op.m_op) && next_op.m_label == null &&
					  (IGAsm.pop==next2_op.m_op) && next2_op.m_label == null)
			{
				
				next_op.m_label = curr_op.m_label;
				next2_op.m_op = IGAsm.nop;
				// toss away current token
				continue;
			}
			else if ( IGAsm.dup1x == iop && // "dup1x".equals(curr_op.m_op) && 
					  (IGAsm.member_store == next_op.m_op) && next_op.m_label == null &&
					  (IGAsm.pop == next2_op.m_op)         && next2_op.m_label == null)
			{
				
				next_op.m_label = curr_op.m_label;
				next2_op.m_op = IGAsm.nop;
				// toss away current token
				continue;
			}
			// pop reduction (this needs further work)
			else if ( (IGAsm.dup1x == iop || IGAsm.dup2x == iop)  && // ("dup2x".equals(curr_op.m_op) || "dup1x".equals(curr_op.m_op))       &&
					 ((IGAsm.call_vtable == next_op.m_op) || (IGAsm.call_static_vtable==next_op.m_op) || (IGAsm.array_store==next_op.m_op)) &&
					 (IGAsm.pop==next2_op.m_op)         &&
					 next_op.m_label == null &&
					 next2_op.m_label == null) {
			
				String count = next_op.m_p0;
				if ( (IGAsm.array_store == next_op.m_op)) {
					count = "3";
				}
			
				if (((IGAsm.dup2x == curr_op.m_op) && count.equals("3")) ||
					((IGAsm.dup1x == curr_op.m_op) && count.equals("2"))) {

					next_op.m_label = curr_op.m_label;
					next2_op.m_op = IGAsm.nop;
			
					// toss away the current token
					continue;			 
				}
			}
			else if (IGAsm.mul_i32 == op &&
					 IGAsm.add_i32 == next_op.m_op && next_op.m_label == null) {
					 
				next_op.m_op = IGAsm.mla_i32;
				next_op.m_label = curr_op.m_label;
				
				// toss current token
				continue;		 
			}
			else if (IGAsm.mul_f64 == op &&
					 IGAsm.add_f64 == next_op.m_op && next_op.m_label == null) {
					 
				next_op.m_op = IGAsm.mla_f64;
				next_op.m_label = curr_op.m_label;
				
				// toss current token
				continue;		 
			}
			// local_load 0
			// local_load 1
			
			//push_i32 0
			//i32_to_f64
			else if (          iop == IGAsm.this_load &&
					  (C[j+1].m_op == IGAsm.push_i32 || C[j+1].m_op == IGAsm.push_i8) && C[j+1].m_label == null &&
					   C[j+2].m_op == IGAsm.add_i32    && C[j+2].m_label == null         && 
					   
					   C[j+3].m_op == IGAsm.dup        &&
					   C[j+4].m_op == IGAsm.this_store && C[j+4].m_label == null         &&
					   C[j+4].m_p0 == curr_op.m_p0     && C[j+4].m_p1    == curr_op.m_p1 &&
					   C[j+5].m_op == IGAsm.pop        &&
					   is_member_function              &&
					   numInRange(C[j+1].m_p0, -127, 127)
					   ) 
			{
					// TODO VERIFY THAT THIS IS NOT AN 32 REF FIELD
					long value = Long.parseLong(C[j+1].m_p0);
				
					//if (-127 <= value && value <= 127) 
					//{
					curr_op.m_op = IGAsm.this_inc_i32;
					curr_op.m_p2 = C[j+1].m_p0;
				
				
					C[j+1].m_op = IGAsm.nop;
					C[j+1].m_p0 = null;
					C[j+2].m_op = IGAsm.nop;
					C[j+3].m_op = IGAsm.nop;
					C[j+4].m_op = IGAsm.nop;
					C[j+4].m_p0 = null;
					C[j+4].m_p1 = null;
					C[j+5].m_op = IGAsm.nop;
					//}
				
			}
			// BRK
			else if (IGAsm.local_load == iop)	//"local_load".equals(curr_op.m_op))
			{
				
				if (is_member_function &&	
					curr_op.m_p0.equals("0") &&
					next_op.m_op == IGAsm.member_load && next_op.m_label == null)
				{
				
				
					next_op.m_op    = IGAsm.this_load;
					next_op.m_label = curr_op.m_label;
					//next_op.m_comment += " BRK BRK BKR";
					// discard curr op
					continue;
				}
				/*
				// DISABLED UNTIL WE CAN UPDATE THE SERVER
				else if (next_op.m_op == IGAsm.member_load && next_op.m_label == null
					&& Integer.parseInt(curr_op.m_p0) < 127)
				{
				
					next_op.m_op    = IGAsm.load_obj_field;
					
					next_op.m_p2    = next_op.m_p1;			// copy over object field
					next_op.m_p1    = next_op.m_p0;			// copy over object name
					next_op.m_p0    = curr_op.m_p0;			// copy of the local offset
					next_op.m_label = curr_op.m_label;
					//next_op.m_comment += "NEW";
					continue;
				}
				*/
				
				
				// (local_load $V, push_i32 $X, add_i32, dup, local_store $V, pop) 
				// where $V in [0,127] and $X in [-128,127]
				// => (local_inc_i32 $V $X)
				else
				if ((IGAsm.push_i8 == C[j+1].m_op || IGAsm.push_i32 == C[j+1].m_op) && C[j+1].m_label == null &&
					(IGAsm.add_i32 == C[j+2].m_op) && C[j+2].m_label == null &&
					(IGAsm.dup     == C[j+3].m_op) && C[j+3].m_label == null &&
				    (IGAsm.local_store == C[j+4].m_op) && C[j+4].m_label == null &&
					(IGAsm.pop ==C[j+5].m_op) && C[j+5].m_label == null &&
					C[j+4].m_p0.equals(curr_op.m_p0)  && 
					Integer.parseInt(curr_op.m_p0) < 127 &&	
					Integer.parseInt(C[j+1].m_p0) <=  127 &&
					Integer.parseInt(C[j+1].m_p0) >= -128)
				{
					curr_op.m_op = (IGAsm.local_inc_i8);
					curr_op.m_p1 = C[j+1].m_p0;

					C[j+1].m_op = IGAsm.nop;
					C[j+2].m_op = IGAsm.nop;
					C[j+3].m_op = IGAsm.nop;
					C[j+4].m_op = IGAsm.nop;
					C[j+5].m_op = IGAsm.nop;
				}
				// (local_load $A, local_load $B) => (local_load_2 $A $B)
				// where $A in [0,127] and $B in [0,127]
				else if (next_op.m_label == null && (IGAsm.local_load == next_op.m_op) &&
						 Integer.parseInt(curr_op.m_p0) <= 127 &&
						 Integer.parseInt(next_op.m_p0) <= 127) {
				
				
					curr_op.m_op = IGAsm.local_load_2;
					curr_op.m_p1 = next_op.m_p0;
					curr_op.m_comment += " " + next_op.m_comment;
					
					next_op.m_op = IGAsm.nop;
					next_op.m_p0 = null;
				}
				// range check optimization
				else if ((IGAsm.push_i8    == C[j+1].m_op || IGAsm.push_i32 == C[j+1].m_op) && C[j+1].m_label == null && C[j+1].m_p0.equals("0")  &&
						 (IGAsm.cmp_lt_i32 == C[j+2].m_op) && C[j+2].m_label == null &&
						 (IGAsm.or         == C[j+3].m_op) && C[j+3].m_label == null &&
						 (IGAsm.local_load == C[j+4].m_op) && C[j+4].m_label == null  && C[j+4].m_p0.equals(curr_op.m_p0) &&
						 (IGAsm.this_load  == C[j+5].m_op) && C[j+5].m_label == null &&
						 (IGAsm.cmp_lt_i32 == C[j+6].m_op) && C[j+6].m_label == null &&
						 (IGAsm.not        == C[j+7].m_op) && C[j+7].m_label == null &&
						 (IGAsm.nop        == C[j+8].m_op) && C[j+3].m_p0.equals(C[j+8].m_label) &&
						 (IGAsm.j0_i32     == C[j+9].m_op) && C[j+9].m_label == null && numInRange(C[j+1].m_p0, -128, 127)) 
				{
				
					int idx = Integer.parseInt(curr_op.m_p0);
					if (idx <= 4)
					{
						curr_op.m_op = IGAsm.ll0 + idx;	//"ll" + curr_op.m_p0;
						curr_op.m_p0 = null;
					}
				
					C[j+1].m_op = IGAsm.nop;
					C[j+2].m_op = IGAsm.nop;
					C[j+3].m_op = IGAsm.nop;
					C[j+4].m_op = IGAsm.nop;
					
					C[j+6].m_op = IGAsm.nop;
					C[j+7].m_op = IGAsm.nop;   
					C[j+8].m_label = null;
					C[j+9].m_op = IGAsm.jr_i32;  				
				}
				else 
				{
					int idx = Integer.parseInt(curr_op.m_p0);
					
					if (idx <= 4)
					{
						curr_op.m_op = IGAsm.ll0 + idx;
						curr_op.m_p0 = null;
					}
					else if (idx <= 255)
					{
						curr_op.m_op = IGAsm.local_load_lo;
					}
				}
			}
			else if (IGAsm.lsl_i32_const == op && 
					 (next_op.m_op == IGAsm.push_i32 || next_op.m_op == IGAsm.push_i8) && next_op.m_label == null &&
					 next2_op.m_op == IGAsm.asr_i32  && next2_op.m_label == null && inRange(next_op.m_p0, -127, 128))
			{

				next2_op.m_p1    = next_op.m_p0;
				next2_op.m_p0    = curr_op.m_p0;
				next2_op.m_label = curr_op.m_label;
				next2_op.m_op    = IGAsm.lsl_asr_i32_const;

				next_op.m_op     = IGAsm.nop;
				next_op.m_p0     = null;
				next_op.m_label  = null;
				
				continue;
			}
			
			else if (IGAsm.push_i32 == iop || IGAsm.push_i8 == iop)
			{
				int storage_class = -1;
				if (IGAsm.push_i32 == iop) { storage_class = IGAsmStorageClass.INT_32; }
				else if (IGAsm.push_i8 == iop) { storage_class = IGAsmStorageClass.INT_8; }

				// basically a bunch of null check code
				// check function public parent__set 2..2 4 0 FINAL
				if (iop          == IGAsm.push_i32 && curr_op.m_p0.equals("0"))
				{
					// TODO verify rigorously
					// check on 2049
					if (next_op.m_op  == IGAsm.cmp_eq_obj && next_op.m_label == null &&
						next2_op.m_op == IGAsm.jn0_i32    && next2_op.m_label == null) 
					{
						next_op.m_op = IGAsm.nop;
						next_op.m_p0 = null;
					
						next2_op.m_label  = curr_op.m_label;
						next2_op.m_op     = IGAsm.j0_i32;
					
					
					
		
						// toss away the cmp_eq_i32
						continue;
					
					}
					else if (
						next_op.m_op  == IGAsm.cmp_eq_obj && next_op.m_label == null &&
						next2_op.m_op == IGAsm.not        && next2_op.m_label == null &&
						next3_op.m_op == IGAsm.j0_i32     && next3_op.m_label == null) 
					{
						next_op.m_op = IGAsm.nop;
						next_op.m_p0 = null;
					
						next2_op.m_op = IGAsm.nop;
					
						next3_op.m_label  = curr_op.m_label;
						next3_op.m_op     = IGAsm.j0_i32;
					
					
					
		
						// toss away the cmp_eq_i32
						continue;
					
					}
				
					/*
					(IGAsm.not    == next_op.m_op)  && next_op.m_label == null &&
						(IGAsm.j0_i32 == next2_op.m_op) && next2_op.m_label == null) 
						*/
				
				
					// (push_i32 0, jmp_eq TARGET) => (j0_i32 TARGET)
				
					if (next_op.m_op == IGAsm.jeq_i32  && next_op.m_label == null) 
					{	 
						next_op.m_op    = IGAsm.j0_i32;
						next_op.m_label = curr_op.m_label;	
					
						// TOSS the current token
						continue; 
					}
				}
			
			
				//System.out.println("NUMERIC VALUE: " + curr_op.m_p0);
				long value = Long.parseLong(curr_op.m_p0);
				
				if (-127 <= value && value <= 127 &&
					next_op.m_label == null &&
					((IGAsm.asr_i32 == next_op.m_op) ||
					 (IGAsm.lsr_i32 == next_op.m_op) ||
					 (IGAsm.lsl_i32 == next_op.m_op) ||
					 (IGAsm.add_i32 == next_op.m_op) ||
					 (IGAsm.sub_i32 == next_op.m_op))
					) 
				{
				
					// an operation that does nothing					
					if      (IGAsm.add_i32 == next_op.m_op) {
						curr_op.m_op = IGAsm.stack_inc_i8;
					}	
					if      (IGAsm.sub_i32 == next_op.m_op) {
						curr_op.m_op = IGAsm.stack_inc_i8;
						curr_op.m_p0 = "" + (-value);
					}	
					else if (IGAsm.asr_i32 == next_op.m_op) {
						curr_op.m_op = IGAsm.asr_i32_const;
					}
					else if (IGAsm.lsr_i32 == next_op.m_op) {
						curr_op.m_op = IGAsm.lsr_i32_const;
					}
					else if (IGAsm.lsl_i32 == next_op.m_op) {
						curr_op.m_op = IGAsm.lsl_i32_const;
					}
		
					next_op.m_op = IGAsm.nop;
					next_op.m_label = curr_op.m_label;
					
					// toss away all adds, shifts etc by zero
					if (0 != value) {	
						next_op.m_op = curr_op.m_op;
						next_op.m_p0 = curr_op.m_p0;
					}
					
					// toss current op so that we can do further optimization
					continue;
				}
				// TOSS out operations that have no effect
				// add_i32 0, lsl_i32 0, lsr_i32 0, as3_i32 0, bit_or_i32 0, sub_i32 0 
				else if (0 == value &&
					next_op.m_label == null &&
					((IGAsm.asr_i32 == next_op.m_op) ||
					 (IGAsm.lsr_i32 == next_op.m_op) ||
					 (IGAsm.lsl_i32 == next_op.m_op) ||
					 (IGAsm.bit_or_i32 == next_op.m_op) ||
					 (IGAsm.add_i32 == next_op.m_op) ||
					 (IGAsm.sub_i32 == next_op.m_op))
					) 
				{
					// an operation that does nothing
					next_op.m_op = IGAsm.nop;
					next_op.m_label = curr_op.m_label;
					// toss the 0 constant
					continue;
				}
	/*			
				push_i8 0
	@243		cmp_eq_i32
	@243		j0_i32
	*/
	
				// THROW OUT DEAD LOADS
				// (local_load $X, pop) => (nop)
				else if (curr_op.m_label == null &&
						 next_op.m_label == null && 
						 next_op.m_op == IGAsm.pop)
				{
					next_op.m_op = IGAsm.nop;
					continue;
				}
				else if (0 == value &&
					next_op.m_op == IGAsm.cmp_eq_i32 && next_op.m_label == null &&
					next2_op.m_op == IGAsm.j0_i32    && next2_op.m_label == null) {
					
					next_op.m_op = IGAsm.nop;
					next2_op.m_op = IGAsm.jn0_i32;
					next2_op.m_label = curr_op.m_label;
					continue;	
				}	
				
					/*
	this_load trudy.game.model.objects.TRKoschei m_tick
@463		push_i8 30
@463		cmp_eq_i32
@463		j0_i32 NI159
*/
				else if (-127 <= value && value <= 127 &&
					next_op.m_op == IGAsm.cmp_eq_i32 && next_op.m_label == null &&
					next2_op.m_op == IGAsm.j0_i32 && next2_op.m_label == null   ) {
					
					curr_op.m_op = IGAsm.stack_inc_i8;
					curr_op.m_p0 = "" + (-value);
					next_op.m_op = IGAsm.nop;
					next2_op.m_op = IGAsm.jn0_i32;
				}		
				else if (0 == value && 
					next_op.m_op == (IGAsm.cmp_eq_i32) && next_op.m_label == null &&
					next2_op.m_op == (IGAsm.not)       && next2_op.m_label == null &&
					next3_op.m_op == (IGAsm.j0_i32)    && next3_op.m_label == null) {
					
					
					next3_op.m_label = curr_op.m_label;
					next_op.m_op     = IGAsm.nop;
					next2_op.m_op    = IGAsm.nop;
					
					// discard current op
					continue;
					
				}

				
				// replace with clears
				else if (
					0 == value &&
					(IGAsm.dup         == next_op.m_op)  && next_op.m_label  == null &&
					(IGAsm.local_store == next2_op.m_op) && next2_op.m_label == null &&
					(IGAsm.pop         == next3_op.m_op) && next3_op.m_label == null)
				{
					
					
					next_op.m_op     = IGAsm.nop;
					next2_op.m_op    = IGAsm.local_clear;
					next2_op.m_label = curr_op.m_label;
					next2_op.m_p1    = intToString(storage_class);
					next3_op.m_op    = IGAsm.nop;
					
					// toss current push_i32/push_i8 token
					continue;
				}
				// replace with clears
				else if (
					0 == value &&
					(IGAsm.i32_to_f64  == C[j+1].m_op) && C[j+1].m_label == null &&
					(IGAsm.dup         == C[j+2].m_op) && C[j+2].m_label  == null &&
					(IGAsm.local_store == C[j+3].m_op) && C[j+3].m_label == null &&
					(IGAsm.pop         == C[j+4].m_op) && C[j+4].m_label == null)
				{
					
					
					
					
					
					C[j+4].m_op     = IGAsm.local_clear;
					C[j+4].m_p0     = C[j+3].m_p0;
					C[j+4].m_p1     = intToString(storage_class);
					C[j+4].m_label  = curr_op.m_label;
					
					C[j+1].m_op     = IGAsm.nop;
					C[j+2].m_op     = IGAsm.nop;
					C[j+3].m_op     = IGAsm.nop;
					C[j+3].m_p0 = null;
					
					// toss current push_i32/push_i8 token
					continue;
				}
				// get rid of unnecessary coversions to floats
				else if (IGAsm.i32_to_f64 == next_op.m_op && next_op != null && next_op.m_label == null) {
				
					next_op.m_op = IGAsm.push_f64;
					next_op.m_p0 = curr_op.m_p0;
					next_op.m_label = curr_op.m_label;
					
					continue;
				}
				else if (-0x800 <= value && value <= 0x7fff)
				{
					if (value == 0) {
						curr_op.m_op = IGAsm.push_i32_0;
						curr_op.m_p0 = null;
					}
					else if (value == 1) {
						curr_op.m_op = IGAsm.push_i32_1;
						curr_op.m_p0 = null;
					}
					else if (-128 <= value && value <= 127)
					{
						curr_op.m_op = IGAsm.push_i8;
					}
					else {
						curr_op.m_op = IGAsm.push_i16;
					}
				}
			}
			else if (IGAsm.push_f64 == iop)
			{
				double v = Double.parseDouble(curr_op.m_p0);
				if (0 == v) {
					curr_op.m_op = IGAsm.push_f64_0;
					curr_op.m_p0 = null;
				}
				else if (1 == v) {
					curr_op.m_op = IGAsm.push_f64_1;
					curr_op.m_p0 = null;
				}
			}
			
			if (IGAsm.jmp         == iop || 
				IGAsm.__throw     == iop || 
				IGAsm.ret0        == iop || 
				IGAsm.ret1        == iop || 
				IGAsm.ret_obj     == iop ||
				IGAsm.ret_funcptr == iop) {
			
				found_terminator = true;	
			}

			out_asm.add(curr_op);
		}
		
		if (!found_end) // || "<get[]>".equals(header.m_header[2])) 
		{
			System.out.println("ISAsmWriter where is my end?");
			System.out.println();
			for (int k = 0; k < header.m_header.length; k++) {
				System.out.print(header.m_header[k] + " ");
			}
			
			System.out.println("PRE-OPTIMIZED:");
			for (int k = 0; k < in_asm.size(); k++) {
				System.out.println(in_asm.get(k).toString());
			}
			System.out.println();
			
			System.out.println("POST-OPTIMIZED:");				
			for (int k = 0; k < out_asm.size(); k++) {
				System.out.println(out_asm.get(k).toString());
			}
			
			
			
			throw new RuntimeException("Encoding failure");
		}		
	}
	
	/*
	private void encodeAsm() 
	{
		if (m_current_header != null && m_asm != null)
		{
			IGAsmHeader header = m_current_header;
			
			header.m_raw_ops = m_asm;
			
			
		}
	}
	*/
	
	
	/**
	 * Add an op and tag it with the required line number
	 */
	
	private void asmAdd(IGAsmOp op) {
		op.m_line_number = m_line_number;
		m_asm.add(op);
	}
	
	public IGAsmOp getLastOP() {
		return m_asm.get(m_asm.size() - 1);
	}
	
	/**
	 * specify the current line number
	 */
	
	public void line(int line_no) 
	{
		m_line_number = line_no;
	}
	
	/*
	 *
	 */
	
	public void asm(IGAsmOp op_chain)
	{
		if (op_chain ==  null) {
			throw new RuntimeException("Can't add null asm");
		}	
		
		if (op_chain.m_next != null) {
			asm(op_chain.m_next);
		}	
		
		asmAdd(new IGAsmOp(op_chain));
	}
	
	public void asmNoCopy(IGAsmOp op_chain)
	{
		if (op_chain ==  null) {
			throw new RuntimeException("Can't add null asm");
		}	

		if (op_chain.m_next != null) {
			asmNoCopy(op_chain.m_next);
		}	
		
		asmAdd(op_chain);
		op_chain.m_next = null;
	}
	
	

	
	/**
	 * Use this for meta information that appears at the top of the file
	 */
	
	public void metaType(String type, String name) {
		// specify a type (enum,class,interface) and a fully qualified name for
		// an object
		String [] a = new String[2];
		a[0] = type;
		a[1] = name;
		m_type = type;
		m_name_fully_qualified = name;
		meta(a);
	}

	public void metaExtends(String s0, String s1) {
		String [] a = new String[2];
		a[0] = s0;
		a[1] = s1;
		m_extends_fully_qualified = s1;
		meta(a);
	}

	public void meta(String s0, String s1) {
		String [] a = new String[2];
		a[0] = s0;
		a[1] = s1;
		meta(a);
	}
	
	public void meta(ArrayList<String> al) {
		String []dsf = new String[al.size()];
		al.toArray(dsf);
		meta(dsf);
	}
	
	public void meta(String [] s) {
		m_meta.add(s);
		
		
		//DELAYED PRINT printArray(s);
	}
	
	public void printArray(PrintWriter tw, String [] s) 
	{
		for (int i = 0; i < s.length; i++) {
			if (i != 0) {
				tw.print(" ");
			}
			tw.print(s[i]);
		}
		
		tw.println();
	}
	
	/**
	 * Use this for headers.  ie the meta information about a function before the code or 
	 * a variable definition
	 */
	
	public void header(ArrayList<String> al) {
		String []dsf = new String[al.size()];
		al.toArray(dsf);
		header(dsf);
	}

	public void addFunctionDependency(String module, String fn) {
		m_current_dependencies.add(module);
		m_dependencies.add(module);
	}

	public void addMemberDependency(String module, String member) {
		m_current_dependencies.add(module);
		m_dependencies.add(module);
	}

	public void addModuleDependency(String module) {
		m_current_dependencies.add(module);
		m_dependencies.add(module);
	}
	
	public void header(String [] s) {
		//encodeAsm();
		
		// DELAYED PRINT m_tw.println("");		// put out an additional blank line
		// DELAYED PRINT printArray(s);
	
		if (s[0].equals("static-function") || s[0].equals("function")) {
			m_asm                  = new ArrayList<IGAsmOp>();
			m_current_dependencies = new IGSet();



			IGAsmHeader header = new IGAsmHeader.Function(s, null);
			m_headers.add(header);

			header.m_raw_ops = m_asm;
			header.m_dependencies = m_current_dependencies;
			m_current_header = header;

			if (header.getName().equals("<static>")) {
				m_static_dependencies = m_current_dependencies;
			}
		}
		else if (s[0].equals("var") || s[0].equals("static-var")){
			m_asm = null;
			IGAsmHeader header = new IGAsmHeader.Variable(s);
			m_headers.add(header);
			header.m_raw_ops = null;
			header.m_dependencies = m_current_dependencies;
			m_current_header = header;
		}
		else {
			throw new RuntimeException("Unknown header type: " + s);
		}
	}
	
	public void finished() {
		//encodeAsm();
	}
	
	

	public void optimize(Hashtable<String,IGAsmTarget> targets) {
		ArrayList<IGAsmHeader> headers = m_headers;
		final int              header_count = headers.size();
			
		for (int k = 0; k < header_count; k++)
		{
			IGAsmHeader header = headers.get(k);
			
			if (header.m_raw_ops != null) {
				processAsm(targets, header);
			}
		}
	}

	public void optimizeAndWrite(Hashtable<String, IGAsmTarget> targets, PrintWriter tw) 
	{
		optimize(targets);


		ArrayList<IGAsmHeader> headers = m_headers;
		final int              header_count = headers.size();
		
		/*	
		for (int k = 0; k < header_count; k++)
		{
			IGAsmHeader header = headers.get(k);
			
			if (header.m_raw_ops != null) {
				processAsm(targets, header);
			}
		}
		*/

		// PRINT THE ACTUAL CONTENTS OF THE FILE
		{
			for (String [] m : m_meta) {
				printArray(tw, m);
			}
		
			
			for (int k = 0; k < header_count; k++)
			{
				IGAsmHeader header = headers.get(k);
			
				tw.println("");		// put out an additional blank line


				for (String k2 : header.m_dependencies.getKeys()) {
					tw.println("#DEP " + k2);
				}
				printArray(tw, header.m_header);
		
				if (m_mode == MODE_WRITE_BODY && header.isFunction()) 
				{	
					int i = 0;
					for (i = 0; i < header.m_ops.size(); i++)
					{
						IGAsmOp a = header.m_ops.get(i);
						a.output(tw);
					}
				}
			}
		}
		tw.close();
	}
	

	
	/** 
	 * Add assembly ops to the outgoing stream
	 */
	
	public void comment(String c)
	{
		asmAdd(IGAsmOp.make_comment(c));
	}
	
	public void label(String l)
	{
		asmAdd(IGAsmOp.make_label(l));	
	}
	

	public void op(int o)
	{
		asmAdd(IGAsmOp.make_op(o));	
	}
	
	public void op(int o, String p0)
	{
		if (o == IGAsm.push_i32 && p0 == null) {
			throw new RuntimeException("woah");
		}
		asmAdd(IGAsmOp.make_op(o, p0));		
	}
	
	public void op(int o, String p0, String p1)
	{
		asmAdd(IGAsmOp.make_op(o, p0, p1));	
	}
	
	public void op(int o, String p0, String p1, String p2)
	{
		asmAdd(IGAsmOp.make_op(o, p0, p1, p2));	
	}
	
	public void op(int o, String p0, String p1, String p2, String p3, String p4, String p5)
	{
		asmAdd(IGAsmOp.make_op(o, p0, p1, p2, p3, p4, p5));	
	}
	
		
	public void op_comment(int o, String c)
	{
		asmAdd(IGAsmOp.make_op_comment(o, c));	
	}
	
	public void op_comment(int o, String p0, String c)
	{
		asmAdd(IGAsmOp.make_op_comment(o, p0, c));	
	}
	
	public void op_comment(int o, String p0, String p1, String c)
	{
		asmAdd(IGAsmOp.make_op_comment(o, p0, p1, c));	
	}
	
	public void op_comment(int o, String p0, String p1, String p2, String c)
	{
		asmAdd(IGAsmOp.make_op_comment(o, p0, p1, p2, c));	
	}
	
	// START OLD
	public void op(String o)
	{
		asmAdd(IGAsmOp.make_op(o));	
	}
	
	public void op(String o, String p0)
	{
		asmAdd(IGAsmOp.make_op(o, p0));		
	}
	
	public void op(String o, String p0, String p1)
	{
		asmAdd(IGAsmOp.make_op(o, p0, p1));	
	}
	
	public void op(String o, String p0, String p1, String p2)
	{
		asmAdd(IGAsmOp.make_op(o, p0, p1, p2));	
	}
	
	public void op(String o, String p0, String p1, String p2, String p3, String p4, String p5)
	{
		asmAdd(IGAsmOp.make_op(o, p0, p1, p2, p3, p4, p5));	
	}
	
		
	public void op_comment(String o, String c)
	{
		asmAdd(IGAsmOp.make_op_comment(o, c));	
	}
	
	public void op_comment(String o, String p0, String c)
	{
		asmAdd(IGAsmOp.make_op_comment(o, p0, c));	
	}
	
	public void op_comment(String o, String p0, String p1, String c)
	{
		asmAdd(IGAsmOp.make_op_comment(o, p0, p1, c));	
	}
	
	public void op_comment(String o, String p0, String p1, String p2, String c)
	{
		asmAdd(IGAsmOp.make_op_comment(o, p0, p1, p2, c));	
	}
	// END OLD

}