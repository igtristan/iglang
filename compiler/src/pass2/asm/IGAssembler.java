/*
Copyright (c) 2014-2017 Incubator Games Ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

package pass2.asm;


/*

	array[index] += 5

	push_l	 array
	push_i32 index
	dup2x
	array_get
	push_i32   5
	add
	array_set



	WHY THE HELL DOESN'T THIS COMPLAIN ABOUT DUPLICATE LABELS?
*/

import java.io.*;
import java.util.*;
import util.ByteArray;
import util.*;
import javax.xml.bind.DatatypeConverter;
import java.util.regex.Pattern;


/*
INPUT FORMAT:

	class 		Object 
	extends 	OtherObject 
	implements 	SomeInterface

	var 	   x : int
	static-var j
	
	#function that takes 3..4 arguments, and uses a grand total of 8 local variables (includes parameters)
	static-function simpleAdd 1..2 3
		def 2   33		# this has a value of 33 for the default parameter 2
		push_local 0
		push_local 1
		add_i
		ret1
	END
		 
 

OUTPUT FORMAT:
struct ModuleHeader
{
	ig_i32 m_header;
	ig_i32 m_version;
	
	ig_i32 m_full_path
	ig_i32 m_extends;
	ig_i32 m_implements_count;
	ig_i32 m_implements[16]
		
	ig_i32 m_static_variable_count;
	ig_i32 m_member_variable_count;
	ig_i32 m_static_function_count;
	ig_i32 m_member_function_count;
	
	ig_i32 m_string_pool_size;
	ig_i32 m_string_entries;			// # of entries
	ig_i32 m_pair_pool_entries;			// # of entries
}

struct Variable
{
	ig_i32  m_name;
	ig_i32  m_type;
	ig_i32  m_flags;
}	
	
struct Method
{
	ig_i32  m_min_args;
	ig_i32  m_max_args;
	ig_i32  m_local_count;
	
	// need to point to code
	// need to point to defaults
}	
	


	

VM
	The vm will be organized that the stack contains 64 bit registers

		struct IGVMFunctionPointer
		{
			ig_i32 object;
			ig_i32 function;	// also has a tag for if an objet is expected
		}
		union
		{
			ig_i32 i32;	  // for ints and handles
			double f64;	  // for doubles
			struct IGVMFunctionPointer fp;
		} IGVMStackEntry;

		each object contains a vtable with the following entries
		
		Object
		{
			vtable => VTable
		
		}
		
		VTable
		{
			length
			idx0
			idx1
			idx2
			key id0
			key id1
			key id2
		}	
*/



public  class IGAssembler
{

static final class IGAssemblyUnitVariable implements Comparable
{
	public IGAssemblyUnit m_unit;
	public boolean 	m_static = true;

	public int  	m_name;
	public int      m_type          = 0;
	public int      m_exact_type    = 0;
	public int      m_protection    = 0;	
	
	public int      m_storage_class = 8;	// highest possible storage class
	// 16 = object, 17 = function pointer, 18 = ref count
	
	
	@Override
    public int compareTo(Object o) {
    
    	IGAssemblyUnitVariable other = (IGAssemblyUnitVariable)o;
    	
    	int this_class = (m_storage_class < 16) ? 0 : 1;
    	int other_class = (other.m_storage_class < 16) ? 0 : 1;
    	
    	return  other_class - this_class;
    }
	
	
	public IGAssemblyUnitVariable(IGAssemblyUnit u) {
		m_unit = u;
	}
	
	public void setName(int n) {
		m_name = n;
	}
	
	public void setStatic(boolean s) {
		m_static = s;
	}	
	
	public void setProtection(String p) {
		p = p.toLowerCase();
		if (p.equals("public")) {
			m_protection = 0;
		}
		else
		{
			m_protection = 1;
		}
	}
	public void output(LEDataOutputStream dos) throws IOException
	{
		dos.writeInt(m_name);			// string key for the name
		dos.writeInt(m_type);			// string key for the contents type.        iglang.Vector<iglang.Object>
		dos.writeInt(m_exact_type);		// string key for the exact contents type.  iglang.Vector<iglang.String>
		dos.writeShort(m_protection);	// int for the protection, and other such flags
		dos.writeShort(m_storage_class);
	}
	
	// ie iglang.ArrayList<iglang.Object>
	public void setStorageType(int type_name) {
		m_type = type_name;
	}

	// ie iglang.ArrayList<iglang.ArrayList<iglang.String>>>	
	public void setExactType(int type_name) {
		m_exact_type = type_name;
	}
	
	public void setStorageClass(int sc) {
		m_storage_class = sc;
	}
}

static final class IGLabelReference
{
	// where the reference should be written
	public ByteArray m_dst;
	public int m_offset;
	
	public String m_label;

	
	public IGLabelReference(int o, String l, ByteArray dst) {
		m_offset = o;
		m_label = l;
		m_dst = dst;
	}
}	

public static final class IGAssemblyUnitMethod
{
	
	public boolean m_static = true;
	public IGAssemblyUnit m_unit;
	
	// This hashtable might actually be more performant
	public StringIntHashtable m_labels = new StringIntHashtable();
	public ArrayList<IGLabelReference>  m_label_references = new ArrayList<IGLabelReference>();
	
	
	// data to output
	public int m_name;
	public int m_arg_min;
	public int m_arg_max;
	public int m_returns = 0;
	public int m_local_count;
	public int m_protection = 0;
	public int m_line_no = 0;
	public long [] m_locals;
	
	public ByteArray m_data = null;
	public ByteArray m_exception_table = null;
	public ByteArray m_debug_table = null;
	public ByteArray m_jump_table = null;
	
	public IGAssemblyUnitMethod(IGAssemblyUnit unit) {
		m_data = new ByteArray();
		m_data.setBigEndian(false);
		
		m_exception_table = new ByteArray();
		m_exception_table.setBigEndian(false);
		
		m_debug_table = new ByteArray();
		m_debug_table.setBigEndian(false);
		
		m_jump_table = new ByteArray();
		m_jump_table.setBigEndian(false);
		
		
		m_unit = unit;
	}	
		
	// get the number of bytes for the data segment
	// this will be on an 8 byte boundary with padding if necessary
	public int getLength()
	{
		int locals_length = 8 * m_locals.length;
		
		// the code has already been padded to 8 bytes
		int code_length = m_data.getLength();
		int exception_table_length = m_exception_table.getLength();
		int debug_table_length = m_debug_table.getLength();
		int jump_table_length = m_jump_table.getLength();
		
		
		return locals_length + code_length + exception_table_length + debug_table_length + jump_table_length;
	}
	
	public void output(LEDataOutputStream dos, int data_offset) throws IOException
	{
		dos.writeInt(m_name);
		dos.writeShort((short)m_arg_min);
		dos.writeShort((short)m_arg_max);
		dos.writeShort((short)m_returns);
		dos.writeShort(0);	// padding
		
		
		// TODO add in the data regarding the exception table
		dos.writeInt(data_offset);			// offset into the buffer for code
		dos.writeInt(getLength());			// the length of the locals and code
		
		dos.writeInt(m_arg_max);							// arg count
		dos.writeInt(m_local_count);						// local default
		dos.writeInt(m_data.getLength());					// # of code bytes
		dos.writeInt(m_exception_table.getLength() / 4);	// # of exception entries
		dos.writeInt(m_debug_table.getLength() / 8);		// debug symbols
		dos.writeInt(m_jump_table.getLength() / 8);			// jump table
	}
	
	public void outputData(LEDataOutputStream dos) throws IOException
	{
		for (int i = 0; i < m_locals.length; i++) {
			dos.writeLong(m_locals[i]);
		}
		
		
		m_data.writeTo(dos);
		m_exception_table.writeTo(dos);
		m_debug_table.writeTo(dos);
		m_jump_table.writeTo(dos);
	}
	
	

	
	public void setName(String n) {
		m_name = m_unit._stringPool(n);
	}
	
	public void setStatic(boolean s) {
		m_static = s;
	}
	
	public void setReturns(int r) {
		m_returns = r;
	}
	
	public void setProtection(String p) {
		m_protection = 0;
	}
	
	public void setCounts(int arg_min, int arg_max, int local_count) {
		m_arg_min = arg_min;
		m_arg_max = arg_max;
		m_local_count = local_count;
		m_locals = new long[local_count];
	}

	// this encodes an intermediate representation
	// which on load then genererates a specific representation (also one much more efficient)


	public static final int NOP = 0;
	public static final int POP = 1;
	public static final int DUP = 2;
	public static final int DUP1X = 3;
	public static final int DUP2X = 4;
	public static final int RET0 = 5;
	public static final int RET1 = 6;
	public static final int DUP2 = 7;
	public static final int ADD_I32 = 8;
	public static final int SUB_I32 = 9;
	public static final int MUL_I32 = 10;
	public static final int DIV_I32 = 11;
	public static final int MOD_I32 = 12;
	public static final int ADD_F64 = 13;
	public static final int SUB_F64 = 14;
	public static final int MUL_F64 = 15;
	public static final int DIV_F64 = 16;
	public static final int MOD_F64 = 17;
	public static final int AND_I32 = 18;
	public static final int OR_I32 = 19;
	public static final int XOR_I32 = 20;
	public static final int ASR_I32 = 21;
	public static final int LSR_I32 = 22;
	public static final int LSL_I32 = 23;
	public static final int CMP_EQ_I32 = 24;
	public static final int CMP_LT_I32 = 25;
	public static final int CMP_LE_I32 = 26;
	public static final int CMP_EQ_F64 = 27;
	public static final int CMP_LT_F64 = 28;
	public static final int CMP_LE_F64 = 29;
	public static final int CMP_EQ_I64 = 30;
	public static final int BIT_NOT_I32 = 31;
	public static final int NOT = 32;
	public static final int I32_TO_F64 = 33;
	public static final int F64_TO_I32 = 34;
	public static final int PUSH_64 = 35;
	public static final int PUSH_32 = 36;
	public static final int LOCAL_LOAD = 37;
	public static final int LOCAL_STORE = 38;
	public static final int MEMBER_LOAD = 39;
	public static final int MEMBER_STORE = 40;
	public static final int STATIC_LOAD = 41;
	public static final int STATIC_STORE = 42;
	public static final int THIS_LOAD = 43;
	public static final int THIS_STORE = 44;
	public static final int LOCAL_LOAD_2 = 45;
	public static final int LOCAL_CLEAR = 46;
	public static final int PUSH_STRING = 47;
	public static final int LOCAL_STORE_KEEP = 48;
	public static final int CALL_INTERFACE = 49;
	public static final int FUNCPTR_MEMBER = 50;
	public static final int FUNCPTR_STATIC = 51;
	public static final int CALL_VTABLE = 52;
	public static final int CALL_STATIC = 53;
	public static final int CALL_DYNAMIC = 54;
	public static final int NEW = 55;
	public static final int THROW = 56;
	public static final int JMP = 57;
	public static final int J0_I32 = 58;
	public static final int JN0_I32 = 59;
	public static final int CAST_TO = 60;
	public static final int NEXT_IT = 61;
	public static final int CALL_STATIC_VTABLE = 62;
	public static final int RET_FUNCPTR = 63;
	public static final int RET_OBJ = 64;
	
	/*
	public static final int MEMBER_STORE_FUNCPTR = 65;
	public static final int MEMBER_STORE_OBJ = 66;
	public static final int THIS_STORE_FUNCPTR = 67;
	public static final int THIS_STORE_OBJ = 68;
	public static final int STATIC_STORE_FUNCPTR = 69;
	public static final int STATIC_STORE_OBJ = 70;
	public static final int STATIC_LOAD_FUNCPTR = 71;
	public static final int STATIC_LOAD_OBJ = 72;
	*/
	public static final int ARRAY_LOAD_8 = 73;
	public static final int ARRAY_LOAD_16 = 74;
	public static final int ARRAY_LOAD_32 = 75;
	public static final int ARRAY_LOAD_64 = 76;
	public static final int ARRAY_LOAD_OBJ = 77;
	public static final int ARRAY_LOAD_FUNCPTR = 78;
	public static final int ARRAY_STORE_8 = 79;
	public static final int ARRAY_STORE_16 = 80;
	public static final int ARRAY_STORE_32 = 81;
	public static final int ARRAY_STORE_64 = 82;
	public static final int ARRAY_STORE_OBJ = 83;
	public static final int ARRAY_STORE_FUNCPTR = 84;
	public static final int ARRAY_NEW = 85;
	public static final int ARRAY_LENGTH = 86;
	public static final int INSTANCE_OF = 87; 
	/* public static final int STACK_TRACE = 88; */
	public static final int LOCAL_INC_I32 = 89;
	public static final int STACK_INC_I32 = 90;
	public static final int CALL_RET1_INTERFACE = 91;
	public static final int CALL_RET1_VTABLE = 92;
	public static final int CALL_RET1_STATIC = 93;
	public static final int CALL_RET1_STATIC_VTABLE = 94;
	public static final int CALL_RET1_DYNAMIC = 95;
	public static final int PUSH_I8 = 96;
	public static final int CATCH_ENTRY = 97;
	public static final int ARRAY_RESIZE = 98;
	public static final int ARRAY_FILL = 99;
	public static final int LL0 = 100;
	public static final int LL1 = 101;
	public static final int LL2 = 102;
	public static final int LL3 = 103;
	public static final int LL4 = 104;
	public static final int JEQ_I32 = 105;	
	public static final int ASR_I32_CONST = 106;
	public static final int LSR_I32_CONST = 107;
	public static final int LSL_I32_CONST = 108;
	public static final int JMP_IN_I32_0RANGE = 109;
	public static final int AND = 110;
	public static final int OR  = 111;

	/*
	public static final int THIS_STORE_32_REF   = 112;
	public static final int MEMBER_STORE_32_REF = 113;
	*/
	public static final int ARRAY_STORE_32_REF  = 114;	
	/*
	public static final int STATIC_STORE_32_REF = 115;
	*/


/*
	public static final int THIS_LOAD_32		 = 116;
	public static final int MEMBER_LOAD_32		 = 117;
	public static final int THIS_STORE_32		 = 118;
	public static final int  MEMBER_STORE_32     = 119;
*/

	public static final int  MLA_I32             = 120;
	public static final int  MLA_F64             = 121;
	public static final int LSL_ASR_I32_CONST    =  122;
	public static final int SWITCH_I32_JMP       = 123;
	public static final int PUSH_I32_0			 = 124;
	public static final int PUSH_I32_1           = 125;
	public static final int ARRAY_COPY           = 126;
	public static final int DEBUG_BLOCK_START   = 127;
    public static final int DEBUG_BLOCK_END     = 128;
    public static final int PUSH_I16     = 129;
    public static final int LOCAL_LOAD_LO       = 130;
//	public static final int LOCAL_STORE_LO       = 131;
    public static final int PUSH_F64_0			 = 132;
    public static final int PUSH_F64_1			 = 133;
    public static final int THIS_INC_I32         = 134;
    public static final int LOAD_OBJ_FIELD       = 135;
    public static final int LOAD_OBJ_FIELD_32       = 136;

    public static final int ADD_F64_VEC = 140;
    public static final int SUB_F64_VEC = 141;
    public static final int MUL_F64_VEC = 142;
    public static final int MUL_F64_VEC_VAL = 143;

	public static final int CALL_RET1_SUPER = 252;
	//public static final int CREATE_IT = 253;
	public static final int CALL_SUPER = 254;
	public static final int NOP_TERM = 255;

	
	/**
	 * Meta ops that may not correspond to actual ops
	 */
	 
	public static final int NON_ASM_OP = 0x200; 

	// instructions that do not result in an output op	 
	public static final int META_param_undef = NON_ASM_OP | 0;
	public static final int META_param_i32   = NON_ASM_OP| 1;
	public static final int META_param_i64   = NON_ASM_OP | 2;
	public static final int META_param_f64   = NON_ASM_OP | 3;
	public static final int META_param_object = NON_ASM_OP | 4;
	public static final int META_param_func_ptr = NON_ASM_OP | 5;
	
	// instructions that do result in an op but require additional functionality
	public static final int META_push_f64 = 0x100 | 5;
	public static final int META_push_i64 = 0x100 | 6;
	public static final int META_push_i32 = 0x100 | 7;
	public static final int META_push_h32 = 0x100 | 8;
	public static final int META_push_string = 0x100 | 9;
	public static final int META_local_store = 0x100 | 10;
	public static final int META_local_store_keep = 0x100 | 0xff;	//LOCAL_STORE_KEEP;  // why was this constant here?
	public static final int META_local_load = 0x100 | 11;
	public static final int META_local_load_2 = 0x100 | 0xfe;	//LOCAL_LOAD_2;		// why was this constant here?
	public static final int META_local_clear = 0x100 | 12;
//	public static final int META_create_it = 0x100 | 13;
	public static final int META_member_store = 0x100 | 14;
	public static final int META_member_load = 0x100 | 15;
	public static final int META_static_store = 0x100 | 16;
	public static final int META_static_load = 0x100 | 17;
	public static final int META_this_store = 0x100 | 18;
	public static final int META_this_load = 0x100 | 19;
	public static final int META_cast_to = 0x100 | 20;
	public static final int META_call_interface = 0x100 | 21;
	public static final int META_call_vtable = 0x100 | 22;
	public static final int META_call_static = 0x100 | 23;
	public static final int META_call_static_vtable = 0x100 | 24;
	public static final int META_call_dynamic = 0x100 | 25;
	public static final int META_call_super = 0x100 | 26;
	public static final int META_funcptr_member = 0x100 | 27;
	public static final int META_funcptr_static = 0x100 | 28;
	public static final int META_try_push = 0x100 | 29;
	public static final int META_instance_of = 0x100 | 30;
	public static final int META_new = 0x100 | 31;
	public static final int META_throw = 0x100 | 32;
	public static final int META_jmp = 0x100 | 33;
	public static final int META_j0_i32 = 0x100 | 34;
	public static final int META_jn0_i32 = 0x100 | 35;
	public static final int META_next_it = 0x100 | 36;
//	public static final int META_release = 0x100 | 37;
	public static final int META_array_load = 0x100 | 38;
	public static final int META_array_store = 0x100 | 39;
	public static final int META_array_new = 0x100 | 40;		
	
	public static final int META_call_ret1_interface = 0x100 | 41;
	public static final int META_call_ret1_vtable = 0x100 | 42;
	public static final int META_call_ret1_static = 0x100 | 43;
	public static final int META_call_ret1_static_vtable = 0x100 | 44;
	public static final int META_call_ret1_dynamic = 0x100 | 45;
	public static final int META_call_ret1_super = 0x100 | 46;
	
	public static final int META_push_i8      = 0x100 | 47;
	
	public static final int META_local_inc_i8 = 0x100 | 48;
	public static final int META_stack_inc_i8 = 0x100 | 49;
	public static final int META_array_resize = 0x100 | 50;
	public static final int META_array_fill   = 0x100 | 51;
	public static final int META_jeq_i32      = 0x100 | 52;
	
	public static final int META_ASR_I32_CONST = 0x100 | 53;
	public static final int META_LSR_I32_CONST = 0x100 | 54;
	public static final int META_LSL_I32_CONST = 0x100 | 56;	
	public static final int META_jr_i32        = 0x100 | 57;
	public static final int META_and           = 0x100 | 58;
	public static final int META_or            = 0x100 | 59;
	public static final int META_end           = 0x100 | 60;
	public static final int CMP_EQ_OBJ         = 0x100 | 61;
	public static final int META_lsl_asr_i32_const = 0x100 | 62;
	public static final int META_switch_i32_jmp = 0x100 | 63;
	public static final int META_array_copy = 0x100 | 64;
	public static final int META_push_i16   = 0x100 | 65;
	
	public static final int META_local_load_lo       =  0x100 | 66;
	
	//public static final int META_local_store_lo       =  0x100 | 67;
	public static final int META_this_inc_i32         =  0x100 | 68;
	
	public static final int META_load_obj_field       = 0x100 | 69;//       = 135;

	public static final int META_push_obj_null        = 0x100 | 70;
	public static final int META_push_fp_null         = 0x100 | 71;
		
	public static HashMap<String,Integer> s_simple = new HashMap<String,Integer>();
	public static HashMap<Integer,String> s_simple_back = new HashMap<Integer,String>();

	private static final void addOp(String key, int value) {
		if (s_simple.containsKey(key)) {
			System.out.println("duplicate mapping");
			throw new RuntimeException("BARF");
		}
		s_simple.put(key, value);
		s_simple_back.put(value, key);
	}
	
	static {
		addOp("nop", NOP);
		addOp("nop_term", NOP_TERM);
		addOp("pop", POP);
		addOp("ll0", LL0);
		addOp("ll1", LL1);
		addOp("ll2", LL2);
		addOp("ll3", LL3);	
		addOp("ll4", LL4);	
				
		addOp("dup", DUP);
		addOp("dup2", DUP2);
		addOp("dup1x", DUP1X);
		addOp("dup2x", DUP2X);
		addOp("ret0",  RET0);
		addOp("ret1",  RET1);
		addOp("ret_obj", RET_OBJ);
		addOp("ret_funcptr", RET_FUNCPTR);
		addOp("add_i32", ADD_I32);
		addOp("sub_i32", SUB_I32);
		addOp("mul_i32", MUL_I32);
		addOp("div_i32", DIV_I32);
		addOp("mod_i32", MOD_I32);
		
		addOp("add_f64", ADD_F64);
		addOp("sub_f64", SUB_F64);
		addOp("mul_f64", MUL_F64);
		addOp("div_f64", DIV_F64);
		addOp("mod_f64", MOD_F64);
		
		addOp("bit_and_i32", AND_I32);
		addOp("bit_or_i32",  OR_I32);
		addOp("bit_xor_i32", XOR_I32);
		addOp("bit_not_i32", BIT_NOT_I32);
		
		addOp("not",         NOT);
		addOp("lsl_i32", LSL_I32);
		addOp("lsr_i32", LSR_I32);
		addOp("asr_i32", ASR_I32);
		
		addOp("cmp_eq_obj", CMP_EQ_OBJ);
		addOp("cmp_eq_i32", CMP_EQ_I32);
		addOp("cmp_lt_i32", CMP_LT_I32);
		addOp("cmp_le_i32", CMP_LE_I32);
		
		addOp("cmp_eq_f64", CMP_EQ_F64);
		addOp("cmp_lt_f64", CMP_LT_F64);
		addOp("cmp_le_f64", CMP_LE_F64);
	
		//addOp("cmp_eq_obj", CMP_EQ_I32);
		addOp("cmp_eq_fp", CMP_EQ_I64);
		addOp("i32_to_f64", I32_TO_F64);
		addOp("f64_to_i32", F64_TO_I32);
			
		addOp("catch_entry", CATCH_ENTRY);	
		addOp("array_length", ARRAY_LENGTH);
		
		
		addOp("param_undef", META_param_undef);
		addOp("param_i32", META_param_i32);
		addOp("param_i64", META_param_i64);
		addOp("param_f64", META_param_f64);
		addOp("param_object", META_param_object);
		addOp("param_func_ptr", META_param_func_ptr);
		
		addOp("push_f64", META_push_f64);
		addOp("push_i64", META_push_i64);
		addOp("push_i16", META_push_i16);
		addOp("push_i8", META_push_i8);
		addOp("push_i32", META_push_i32);
		addOp("push_h32", META_push_h32);
		addOp("push_obj_null", META_push_obj_null);
		addOp("push_fp_null", META_push_fp_null);
		addOp("push_string", META_push_string);
		addOp("local_store", META_local_store);
		addOp("local_store_keep", META_local_store_keep);
		addOp("local_load", META_local_load);
		addOp("local_load_2", META_local_load_2);
		addOp("local_clear", META_local_clear);
		//addOp("create_it", META_create_it);
		addOp("member_store", META_member_store);
		addOp("member_load", META_member_load);
		addOp("static_store", META_static_store);
		addOp("static_load", META_static_load);
		addOp("this_store", META_this_store);
		addOp("this_load", META_this_load);
		addOp("cast_to", META_cast_to);
		
		addOp("call_interface", META_call_interface);
		addOp("call_vtable", META_call_vtable);
		addOp("call_static", META_call_static);
		addOp("call_static_vtable", META_call_static_vtable);
		addOp("call_dynamic", META_call_dynamic);
		addOp("call_super", META_call_super);
		
		addOp("call_ret1_interface",		META_call_ret1_interface);
		addOp("call_ret1_vtable", 		META_call_ret1_vtable);
		addOp("call_ret1_static", 		META_call_ret1_static);
		addOp("call_ret1_static_vtable", META_call_ret1_static_vtable);
		addOp("call_ret1_dynamic", 		META_call_ret1_dynamic);
		addOp("call_ret1_super", 		META_call_ret1_super);	

		
		addOp("funcptr_member", META_funcptr_member);
		addOp("funcptr_static", META_funcptr_static);
		addOp("try_push", META_try_push);
		addOp("instance_of", META_instance_of);
		addOp("new", META_new);
		addOp("throw", META_throw);
		addOp("jmp", META_jmp);
		addOp("j0_i32", META_j0_i32);
		addOp("jn0_i32", META_jn0_i32);
		addOp("jeq_i32", META_jeq_i32);
		addOp("and", META_and);
		addOp("or", META_or);
		addOp("next_it", META_next_it);
//		addOp("release", META_release);
		addOp("array_load", META_array_load);
		addOp("array_store", META_array_store);
		addOp("array_new", META_array_new);	
		addOp("array_resize", META_array_resize);	
		addOp("local_inc_i8", META_local_inc_i8);
		addOp("stack_inc_i8", META_stack_inc_i8);
		addOp("array_fill",   META_array_fill);
		addOp("array_copy",   META_array_copy);
		addOp("asr_i32_const", META_ASR_I32_CONST);
		addOp("lsr_i32_const", META_LSR_I32_CONST);
		addOp("lsl_i32_const", META_LSL_I32_CONST);
		addOp("lsl_asr_i32_const", META_lsl_asr_i32_const);
		addOp("switch_i32_jmp", META_switch_i32_jmp);
				
		addOp("jr_i32", META_jr_i32);
		addOp("mla_i32", MLA_I32);
		addOp("mla_f64", MLA_F64);
		
		addOp("push_i32_0", PUSH_I32_0);
		addOp("push_i32_1", PUSH_I32_1);
		addOp("push_f64_0", PUSH_F64_0);
		addOp("push_f64_1", PUSH_F64_1);
	

		addOp("debug_block_start", DEBUG_BLOCK_START);
		addOp("debug_block_end",    DEBUG_BLOCK_END);
		addOp("local_load_lo",  META_local_load_lo);
		//addOp("local_store_lo",  META_local_store_lo);
		addOp("this_inc_i32",     META_this_inc_i32);
		addOp("load_obj_field",   META_load_obj_field);
		addOp("END",    META_end);
	}
	
	/*
	public  boolean pushSimple(String [] args) {
		
		int arg = s_simple.get(args[0]);
		
		// collapse nops
		if (arg <= 255) {
			return arg;
		}
		
		m_data.writeByte((int)arg);
		// yes we processed the op
		return arg;
	}
	*/


	public void push(byte op) {
		m_data.writeByte(op);
	}
	
	public void push(byte op, byte a0) {
		m_data.writeByte(op);
		m_data.writeByte(a0);		
	}

	public void push(byte op, short a0) {
		m_data.writeByte(op);
		m_data.writeShort(a0);		
	}

	public void push(byte op, int a0) {
		m_data.writeByte(op);
		m_data.writeInt(a0);		
	}
	
	public void push(byte op, long a0) {
		m_data.writeByte(op);
		m_data.writeLong(a0);
	}
	
	
	public void pushFunc(byte op, int args, String object, String method)
	{
		if (args > 64) {
			throw new RuntimeException("Max 64 function arguments");
		}
	
		int pair_pool = m_unit.pairPool(m_unit._stringPool(object), m_unit._stringPool(method));
		push(op);
		push((byte)args, (short)pair_pool);
	}

	public void pushLoadStore(byte op, String object, String field)
	{
		int pair_pool = m_unit.pairPool(m_unit._stringPool(object), m_unit._stringPool(field));
		push(op, (short)pair_pool);
	}


	/**
	 * text is guaranteed to be trimmed
	 */

	//public void addCodeLine(String text) 
	//{
	//	String [] toks = IGAssembler.WHITESPACE_SPLIT.split(text);
	//	addCodeLine(toks);
	//}
	
	
	
	private  String [] s_toks = new String [8];
	
	public void addOp(IGAsmOp op)
	{
		String [] toks = s_toks;
		toks[0] = null;	//s_simple_back.get(op.m_op);
		toks[1] = op.m_p0;
		toks[2] = op.m_p1;
		toks[3] = op.m_p2;
		toks[4] = op.m_p3;
		toks[5] = op.m_p4;
		toks[6] = op.m_p5;
				
		addOp(op.m_line_number, op.m_label, op.m_op, toks);
	}
	
	public void addCodeLine(String [] toks) 
	{
		// TODO add back in line numbers
	
		if (toks[0].endsWith(":")) {
		
			// strip off the ':' from the label
			String label = toks[0].substring(0, 	toks[0].length() - 1);

			// shuffle down the rest of the tokens
			for (int i = 0; i < toks.length - 1; i++) {
				toks[i] = toks[i+1];
			}
			toks[toks.length - 1] = null;

			addOp(-1, label, s_simple.get(toks[0]), toks);
		}
		else
		{
			addOp(-1, null, s_simple.get(toks[0]),  toks);
		}
	}
	
	public void validateLocalIndex(int local_index) {
		if (local_index < 0 || local_index >= m_local_count) {
			throw new RuntimeException("Invalid local index specified: " + local_index);
		}
	}
	
	static final int [] s_array_load_ops = new int[]{
		0,
		ARRAY_LOAD_8,
		ARRAY_LOAD_16,
		0,
		ARRAY_LOAD_32,0,0,0,
		ARRAY_LOAD_64,0,0,0,0,0,0,0,
		ARRAY_LOAD_OBJ,
		ARRAY_LOAD_FUNCPTR,
		ARRAY_LOAD_32};
	
	static final int  [] s_array_store_ops = new int[]{
		0,
		ARRAY_STORE_8,
		ARRAY_STORE_16,
		0,
		ARRAY_STORE_32,0,0,0,
		ARRAY_STORE_64,0,0,0,0,0,0,0,
		ARRAY_STORE_OBJ,
		ARRAY_STORE_FUNCPTR,
		ARRAY_STORE_32_REF};
			
	
	
	public void addOp(int line_no, String label,  int op_code, String [] s) {
		String o = s[0];	//.toLowerCase();
		
		int position = m_data.getPosition();	//current; .. figure out the current position in the stream
		if (label != null) {
			m_labels.put(label, position);
		}


		ByteArray ba = m_data;

		// should put this in a try catch	
		
		//if (!s_simple.containsKey(o)) {
		//	throw new RuntimeException("Unknown op: " + o);
		//}	
		//int op_code = s_simple.get(o);
			
		if ((op_code & NON_ASM_OP) != NON_ASM_OP && line_no != -1) 
		{
			if (line_no != m_line_no) 
			{
				m_debug_table.writeShort(ba.getPosition());
				m_debug_table.writeShort(line_no);
				m_debug_table.writeShort(0);
				m_debug_table.writeShort(0);
				m_line_no = line_no;
			}
		}	
			
		// handle everything and anything that isn't basic
		if (op_code <= 255)
		{	
			// do nop removal at this point?
			if (op_code != NOP) {
				ba.writeByte((int)op_code);
			}
		}
		else
		{
			switch (op_code)
			{
				case CMP_EQ_OBJ:
					{
						ba.writeByte(CMP_EQ_I32);
					}	
					break;
				case META_param_undef:  
					{
						int local_index = Integer.parseInt(s[1]);
						validateLocalIndex(local_index);	
				
						m_locals[local_index] = 0;
					}		
					break;  
				case META_param_i32:  {
						int local_index = Integer.parseInt(s[1]);
						validateLocalIndex(local_index);	
							
						int value = Integer.parseInt(s[2]);
						m_locals[local_index] = value;
					}
					break;  
				case META_param_object:
				case META_param_func_ptr:
				case META_param_i64:  {
						int local_index = Integer.parseInt(s[1]);
						validateLocalIndex(local_index);
				
						long value = Long.parseLong(s[2]);
						m_locals[local_index] = value;
					}
					break;  
				case META_param_f64:  {
						int local_index = Integer.parseInt(s[1]);
						validateLocalIndex(local_index);
				
						double value = Double.parseDouble(s[2]);
						m_locals[local_index] = Double.doubleToLongBits(value);
					}			
					// need to add in hex equivilant
					break;  
					

				case ADD_F64_VEC: 
				case SUB_F64_VEC:
				case MUL_F64_VEC:
				case MUL_F64_VEC_VAL:
				{
					ba.writeByte((byte)op_code);
					ba.writeByte(Integer.parseInt(s[1]));
					break;
				}
				case META_push_f64:  {
						ba.writeByte(PUSH_64);
						ba.writeLong(Double.doubleToLongBits(Double.parseDouble(s[1])  ));
					}
					break;  
				case META_push_i64:  {
						ba.writeByte(PUSH_64);
						ba.writeLong(Long.parseLong(s[1]));
					}
					break;  
				case META_push_i32:  {
						ba.writeByte(PUSH_32);
						ba.writeInt((int)Long.parseLong(s[1]));
					}
					break;  
				case META_push_i8:  {
						ba.writeByte(PUSH_I8);
						ba.writeByte((byte)Integer.parseInt(s[1]));
					}
					break;  
				case META_push_obj_null: {
					ba.writeByte(PUSH_I32_0);
					break;
				}	
				case META_push_fp_null: {
					ba.writeByte(PUSH_64);
					ba.writeLong(0);
					break;
				}

				case META_push_i16:  {
						ba.writeByte(PUSH_I16);
						ba.writeShort((short)Integer.parseInt(s[1]));
					}
					break;  	
					
				case META_push_h32:  {
						ba.writeByte(PUSH_32);
				
						long long_value = Long.decode(s[1]);
						ba.writeInt((int)long_value);
						//throw new RuntimeException("Unhandled");
					}
					break;  
				case META_push_string:  {
						// this will probably have to change at some point
						ba.writeByte(PUSH_STRING);
				
				
						String ps = s[1];
						if (ps.startsWith("base64:")) {
							byte [] arr = DatatypeConverter.parseBase64Binary(ps.substring("base64:".length()));
							ps = Decoder.decodeString(new String(arr)).toString();
						}
				
						ba.writeShort(m_unit._stringPool(ps));
					}
					break;  
				case META_local_store:  {
						ba.writeByte(LOCAL_STORE);
				
						int local_index = Integer.parseInt(s[1]);
						validateLocalIndex(local_index);
						ba.writeShort(local_index);
					}
					break;  
					
				
					/*	
				case META_local_store_lo: {
					ba.writeByte(LOCAL_STORE_LO);

						int local_index = Integer.parseInt(s[1]);
						validateLocalIndex(local_index);
						
						if (local_index < 0 || local_index > 255) {
							throw new RuntimeException("Totally invalid local lo load");
						}
						
						ba.writeByte((byte)local_index);
					}
					break;
				*/
					
				case META_local_store_keep:  {
						ba.writeByte(LOCAL_STORE_KEEP);
				
						int local_index = Integer.parseInt(s[1]);
						validateLocalIndex(local_index);
						ba.writeByte((byte)local_index);
					}
					break;  
					
				case META_local_load_lo: {
					ba.writeByte(LOCAL_LOAD_LO);
						// perhaps wrap in a parseIntGE0
						int local_index = Integer.parseInt(s[1]);
						validateLocalIndex(local_index);
						
						if (local_index < 0 || local_index > 255) {
							throw new RuntimeException("Totally invalid local lo load");
						}
						
						ba.writeByte((byte)local_index);
					}
					break;
				
					
				case META_local_load:  {
						ba.writeByte(LOCAL_LOAD);
						// perhaps wrap in a parseIntGE0
						int local_index = Integer.parseInt(s[1]);
						validateLocalIndex(local_index);
						ba.writeShort(local_index);
					}
					break;  
				case META_local_load_2:  {
						ba.writeByte(LOCAL_LOAD_2);
						// perhaps wrap in a parseIntGE0
						int local_index = Integer.parseInt(s[1]);
						validateLocalIndex(local_index);
						ba.writeByte(local_index);
						
						local_index = Integer.parseInt(s[2]);
						validateLocalIndex(local_index);
						ba.writeByte(local_index);
					}
					break;  
				case META_local_clear:  {
						ba.writeByte(LOCAL_CLEAR);
				
						int local_index = Integer.parseInt(s[1]);
						validateLocalIndex(local_index);				
						ba.writeShort(local_index);
					}
					break;  
				//case META_create_it:  {
				//		ba.writeByte(CREATE_IT);
				//		ba.writeShort(Integer.parseInt(s[1]));
				//	}
				//	break;  
				case META_member_store:  {
						ba.writeByte(MEMBER_STORE);
						ba.writeShort(m_unit.pairPool(s[1], s[2]));
					}
					break;  
				case META_member_load:  {
						ba.writeByte(MEMBER_LOAD);
						ba.writeShort(m_unit.pairPool(s[1], s[2]));
					}			
					break;  
				
				
				case META_load_obj_field:  {
				
				
						
						ba.writeByte(LOAD_OBJ_FIELD);
						
						
						//System.out.println("LOAD OBJ FIELD: " + s[1] + " " + s[2] + " " + s[3]);
						ba.writeByte((byte)Integer.parseInt(s[1]));
						ba.writeShort(m_unit.pairPool(s[2], s[3]));
					}			
					break;  	
					
				case META_static_store:  {
						ba.writeByte(STATIC_STORE);
						ba.writeShort(m_unit.pairPool(s[1], s[2]));
					}
					break;  
				case META_static_load:  {
						ba.writeByte(STATIC_LOAD);
						ba.writeShort(m_unit.pairPool(s[1], s[2]));
					}			
					break;  
				case META_this_store:  {
						ba.writeByte(THIS_STORE);
						ba.writeShort(m_unit.pairPool(s[1], s[2]));
					}
					break;  
				case META_this_load:  {
						ba.writeByte(THIS_LOAD);
						ba.writeShort(m_unit.pairPool(s[1], s[2]));
					}
					break;  
				case META_cast_to:  {
						ba.writeByte(CAST_TO);
						ba.writeShort(m_unit.pairPool(s[1], ""));
					}	
					break;  
					
				
					
				// ret void variants
				case META_call_interface:  {
						ba.writeByte(CALL_INTERFACE);
						ba.writeByte(Integer.parseInt(s[1]));
						ba.writeShort(m_unit._stringPool(s[2]));
					}			
					break;  
				case META_call_vtable:  {
						ba.writeByte(CALL_VTABLE);
						ba.writeByte(Integer.parseInt(s[1]));
						ba.writeShort(m_unit.pairPool(s[2], s[3]));
					}
					break;  
				case META_call_static:  {
						ba.writeByte(CALL_STATIC);
						ba.writeByte(Integer.parseInt(s[1]));
						ba.writeShort(m_unit.pairPool(s[2], s[3]));
					}
					break;  
				case META_call_static_vtable:  {
						ba.writeByte(CALL_STATIC_VTABLE);
						ba.writeByte(Integer.parseInt(s[1]));
						ba.writeShort(m_unit.pairPool(s[2], s[3]));
					}
					break;  
				case META_call_dynamic:  {
						ba.writeByte(CALL_DYNAMIC);
						ba.writeByte(Integer.parseInt(s[1]));
					}
					break;  
				case META_call_super:  {
						ba.writeByte(CALL_SUPER);
						ba.writeByte(Integer.parseInt(s[1]));
						ba.writeShort(m_unit.pairPool(s[2], s[3]));
					}
					break;  
				// ret 1 variants
				case META_call_ret1_interface:  {
						ba.writeByte(CALL_RET1_INTERFACE);
						ba.writeByte(Integer.parseInt(s[1]));
						ba.writeShort(m_unit._stringPool(s[2]));
					}			
					break;  
				case META_call_ret1_vtable:  {
						ba.writeByte(CALL_RET1_VTABLE);
						ba.writeByte(Integer.parseInt(s[1]));
						ba.writeShort(m_unit.pairPool(s[2], s[3]));
					}
					break;  
				case META_call_ret1_static:  {
						ba.writeByte(CALL_RET1_STATIC);
						ba.writeByte(Integer.parseInt(s[1]));
						ba.writeShort(m_unit.pairPool(s[2], s[3]));
					}
					break;  
				case META_call_ret1_static_vtable:  {
						ba.writeByte(CALL_RET1_STATIC_VTABLE);
						ba.writeByte(Integer.parseInt(s[1]));
						ba.writeShort(m_unit.pairPool(s[2], s[3]));
					}
					break;  
				case META_call_ret1_dynamic:  {
						ba.writeByte(CALL_RET1_DYNAMIC);
						ba.writeByte(Integer.parseInt(s[1]));
					}
					break;  
				case META_call_ret1_super:  {
						ba.writeByte(CALL_RET1_SUPER);
						ba.writeByte(Integer.parseInt(s[1]));
						ba.writeShort(m_unit.pairPool(s[2], s[3]));
					}
					break;  	
					
				case META_funcptr_member:  {
						ba.writeByte(FUNCPTR_MEMBER);
						ba.writeShort(m_unit.pairPool(s[1], s[2]));
					}
					break;  
				case META_funcptr_static:  {
						ba.writeByte(FUNCPTR_STATIC);
						ba.writeShort(m_unit.pairPool(s[1], s[2]));
					}
					break;  
				case META_try_push:  {
						// this is a non output instruction
				
						// note the current position in the data stream
						m_exception_table.writeShort(ba.getPosition());
				
						// create a label reference to insert the field
						m_label_references.add(
							new IGLabelReference(m_exception_table.getPosition(), s[1], m_exception_table));
						m_exception_table.writeShort(0);
					}
					break;  
				case META_instance_of:  {
						ba.writeByte(INSTANCE_OF);
						ba.writeShort(m_unit.pairPool(s[1], ""));
					}
					break;  
				case META_new:  {
						ba.writeByte(NEW);
						ba.writeShort(m_unit.pairPool(s[1], ""));
					}
					break;  
				case META_throw:  {
						ba.writeByte(THROW);
					}
					break;  

//				case META_release:  {
//						ba.writeByte(NOP);
//					}
//					break;  
				case META_array_load:  {
						String pair = s[1];
						int    storage_class = Integer.parseInt(s[2]);
				
						ba.writeByte(s_array_load_ops[storage_class]);
					}
					break;  
				case META_array_store:  {
						String pair = s[1];
						int    storage_class = Integer.parseInt(s[2]);
				
						ba.writeByte(s_array_store_ops[storage_class]);
					}
					break;  
				case META_array_new:  {
						ba.writeByte(ARRAY_NEW);
						ba.writeShort(m_unit.pairPool(s[1], ""));	// object base
						ba.writeByte(Integer.parseInt(s[2]));		// storage class
						break;
					}
				case META_array_resize: {
					ba.writeByte(ARRAY_RESIZE);
					ba.writeByte(Integer.parseInt(s[1]));		// storage class
					break;
				}
					
				case META_array_fill: {
					ba.writeByte(ARRAY_FILL);
					ba.writeByte(Integer.parseInt(s[1]));		// storage class
					break;
				}
					
				case META_array_copy: {
					ba.writeByte(ARRAY_COPY);
					ba.writeByte(Integer.parseInt(s[1]));		// storage class
					break;
				}	
				////////////////////////////////////////////////
				// Ops that utilize the jump table	
				case META_switch_i32_jmp:  {
						ba.writeByte(SWITCH_I32_JMP);
						ba.writeInt((int)Long.parseLong(s[1]));
						writeJumpTableReference(s[2]);
					}
					break;  
				case META_jmp:  {
						ba.writeByte(JMP);
						writeJumpTableReference(s[1]);
					}
					break;  
				case META_j0_i32:  {
						ba.writeByte(J0_I32);
						writeJumpTableReference(s[1]);
					}
					break;  
				case META_jn0_i32:  {
						ba.writeByte(JN0_I32);
						writeJumpTableReference(s[1]);
					}
					break;  
				case META_jeq_i32:  {
						ba.writeByte(JEQ_I32);
						writeJumpTableReference(s[1]);
					}
					break;	
				case META_and:  {
						ba.writeByte(AND);
						writeJumpTableReference(s[1]);
					}
					break;  
				case META_or:  {
						ba.writeByte(OR);
						writeJumpTableReference(s[1]);
					}
					break;		
					
				case META_jr_i32: {
				
						ba.writeByte(JMP_IN_I32_0RANGE);
						writeJumpTableReference(s[1]);
						break;
					}
				case META_next_it:  {
						ba.writeByte(NEXT_IT);
						ba.writeShort(Integer.parseInt(s[1]));			// local variable #
						ba.writeShort(m_unit.pairPool(s[2], s[3]));		// has next variable
						ba.writeShort(m_unit.pairPool(s[4], s[5]));		// next() function
						writeJumpTableReference(s[6]);							// exit point
					}
					break;  					
					
				case META_local_inc_i8: {
				
					ba.writeByte(LOCAL_INC_I32);
					ba.writeByte((byte)Integer.parseInt(s[1]));
					ba.writeByte((byte)Integer.parseInt(s[2]));
				
					//		 static const int LOCAL_INC_I32			= 81;
					// static const int STACK_INC_I32			= 82;
					
					break;
				}	
				case META_stack_inc_i8: {
					ba.writeByte(STACK_INC_I32);
					ba.writeByte((byte)Integer.parseInt(s[1]));
					break;
				}	
				
				case META_this_inc_i32: {
					ba.writeByte (THIS_INC_I32);
					ba.writeShort(m_unit.pairPool(s[1], s[2]));
					ba.writeByte ((byte)Integer.parseInt(s[3]));
					break;
				}		
				
				case META_ASR_I32_CONST: {
					ba.writeByte(ASR_I32_CONST);
					ba.writeByte((byte)Integer.parseInt(s[1]));
					break;
				}
				case META_LSR_I32_CONST: {
					ba.writeByte(LSR_I32_CONST);
					ba.writeByte((byte)Integer.parseInt(s[1]));
					break;
				}
				case META_LSL_I32_CONST: {
					ba.writeByte(LSL_I32_CONST);
					ba.writeByte((byte)Integer.parseInt(s[1]));
					break;
				}
				case META_lsl_asr_i32_const: {
					ba.writeByte(LSL_ASR_I32_CONST);
					ba.writeByte((byte)Integer.parseInt(s[1]));
					ba.writeByte((byte)Integer.parseInt(s[2]));
					break;				
				}
				
				default:
				{
					throw new RuntimeException("Unhandled op: " + o);
				}
			}
		}
	
		
		//m_label_references.add(new IGLabelReference(position + 1, s[1]));
	}
	
	
	private ArrayList<String>        m_jump_table_labels = new ArrayList<String>();
	private Hashtable<String, Short> m_label_to_jump_table_sequence = new Hashtable<String, Short>();
	
	
	public void writeJumpTableReference(String lbl)
	{
		if (!m_label_to_jump_table_sequence.containsKey(lbl)) 
		{		
			m_label_to_jump_table_sequence.put(lbl, (short)m_jump_table_labels.size());	
			m_jump_table_labels.add(lbl);
		}
		
		m_data.writeShort(m_label_to_jump_table_sequence.get(lbl));
		
		
		//else {
		//
		//	
		//}
		//m_label_references.add(new IGLabelReference(m_data.getPosition(), lbl, m_data));
		//m_data.writeShort(0);
	}
	
	
	public void resolveLabels()
	{
		
		///////////
		// Jump Table
		///////////
		{
			for (int i = 0; i < m_jump_table_labels.size(); i++)
			{
				String label = m_jump_table_labels.get(i);
				
				if (!m_labels.containsKey(label)) {
					System.out.println("processing label: " + label);
					throw new RuntimeException("Missing label: " + label + " " + m_unit.m_string_pool.get(m_unit.m_name));	
				}
				
				int reference = m_labels.get(label);
				
				m_jump_table.writeInt(0);
				m_jump_table.writeInt(reference);
			}
		
		}
	
		//////////
		// Deal with exception table label references
		//////////
		
		//if (m_label_references != null)
		//{
			for (IGLabelReference r : m_label_references)
			{
			
				if (!m_labels.containsKey(r.m_label)) {
					System.out.println("processing label: " + r.m_label);
					throw new RuntimeException("Missing label");	
				}
			
				// this implementation will crash if it can't find the required key?
				

				int reference = m_labels.get(r.m_label);
				
				int offset    = r.m_offset;
			
				ByteArray dst = r.m_dst;
			
				dst.setPosition(offset);
			
				// special case for the exception table
				if (dst != m_data) {
					offset = 0;
				}
				dst.writeShort(reference - offset);	/// should this be relative to the PC?
			}
		//}
		
		int length = m_data.getLength();
		m_data.setPosition(length);
		
		
		// pad out the code length with nops
		while ((length & 0x7) != 0) {
			m_data.writeByte(NOP_TERM);
			length = m_data.getLength();
		}
		
		m_data.setPosition(0);
	}
	
}

static class IGAssemblyUnit
{
	public static final boolean SORT_MEMBER_VARIABLES = true;
/*

OUTPUT FORMAT:
struct ModuleHeader
{
	ig_i32 m_header;
	ig_i32 m_version;
	
	
	ig_i32 m_full_path
	ig_i32 m_type;
	ig_i32 m_extends;
	ig_i32 m_implements_count;
	
	ig_i32 m_implements[16]
	
	
	ig_u8 m_storage_class;
	ig_u8 padding0;
	ig_u8 padding1;
	ig_u8 padding2;
		
	ig_i32 m_static_variable_count;
	ig_i32 m_member_variable_count;
	ig_i32 m_static_function_count;
	ig_i32 m_member_function_count;
	
	ig_i32 m_pair_pool_entries;			// # of entries
	ig_i32 m_string_entries;			// # of entries
	ig_i32 m_string_pool_size;
	ig_i32 m_data_pool_size;
	
	// static variables
	// member variables
	// static methods
	// member methods
	
	// pair pool
	// string lengths
	// string data
	
};

struct MethodHeader
{
	ig_i32 m_name;
	ig_i32 m_arg_min;
	ig_i32 m_arg_max;
	ig_i32 m_local_count;
	ig_i32 m_data_offset;
	ig_i32 m_spacer;
};

struct VariableHeader
{
	ig_i32 m_name;
	ig_i32 m_type;
	ig_i32 m_parameters;
}

*/

	// 0.1 = optimizations
	// 0.2 = adding support for tracing gc
	// 0.3 = stack height tracing, w/ jump table
	// 0.4 = additional opcodes for optimization purposes
	// 0.5 = support for ref counted 32 bit handles
	// 0.6 = added support for mid execution gc, switches, mla's, bit extraction
	public static final int VERSION = 0x0006;

	public void output(LEDataOutputStream dos) throws IOException {
		dos.writeByte((byte)'I');
		dos.writeByte((byte)'G');
		dos.writeByte((byte)'V');
		dos.writeByte((byte)'M');		
		
		dos.writeInt(VERSION);
		
		dos.writeInt(m_name);
		dos.writeInt(m_type);
		dos.writeInt(m_extends);
		dos.writeInt(m_implements.size());
		
		for (int i = 0; i < 16; i++) {
			if (i < m_implements.size()) {
				dos.writeInt(m_implements.get(i));
			}
			else
			{
				dos.writeInt(0);
			}
		}	
		
		dos.writeByte(0);
		dos.writeByte(0);
		dos.writeByte(0);
		dos.writeByte(0);
		
		dos.writeInt(m_static_variables.size());
		dos.writeInt(m_member_variables.size());
		dos.writeInt(m_static_functions.size());
		dos.writeInt(m_member_functions.size());	
		dos.writeInt(m_pair_pool.size());
		dos.writeInt(m_string_pool.size());
		
		int string_pool_length = 0;
		{
			//m_string_pool_lengths = new int[m_string_pool.size()];
			{
				int sp_sz = m_string_pool_bytes.size();
				for (int i = 0; i < sp_sz; i++) 
				{
					byte [] b = (byte []) m_string_pool_bytes.get(i);
					int l =  b.length;
					
					//m_string_pool_lengths[i] = l;
					string_pool_length += l;
				}
			}
		}
		dos.writeInt(string_pool_length);
		
		int data_pool_length = 0;
		{
			int member_functions = m_member_functions.size();
			int static_functions = m_static_functions.size();

			m_data_pool_lengths = new int[member_functions + static_functions];
			
			int k = 0;
			
			for (int i = 0; i < static_functions; i++) {
				m_data_pool_lengths[k] = m_static_functions.get(i).getLength();
				data_pool_length += m_data_pool_lengths[k];
				k++;
			}		
			
			
			for (int i = 0; i < member_functions; i++) {
				m_data_pool_lengths[k] = m_member_functions.get(i).getLength();
				data_pool_length += m_data_pool_lengths[k];
				k++;
			}		
		}
		
		dos.writeInt(data_pool_length);
		
		
		
		for (int i = 0; i < m_static_variables.size(); i++) {
			m_static_variables.get(i).output(dos);
		}		

		for (int i = 0; i < m_member_variables.size(); i++) {
			m_member_variables.get(i).output(dos);
		}		
		
		int data_offset = 0;
		int k = 0;
		{
			for (int i = 0; i < m_static_functions.size(); i++) {
				m_static_functions.get(i).output(dos, data_offset);
				data_offset += m_data_pool_lengths[k]; k++;
			}		

			for (int i = 0; i < m_member_functions.size(); i++) {
				m_member_functions.get(i).output(dos, data_offset);
				data_offset += m_data_pool_lengths[k]; k++;
			}	
		}
		

		int sp_sz = m_string_pool_bytes.size();
		//for (int i = 0; i < sp_sz; i++) {
		//	dos.writeInt(m_string_pool_lengths[i]);
		//}
		
		
		for (int i = 0; i < sp_sz; i++) {
			byte [] b = (byte []) m_string_pool_bytes.get(i);
			dos.writeInt(b.length);
		}
		
		for (int i = 0; i < sp_sz; i++) {
			byte [] b = (byte []) m_string_pool_bytes.get(i);
			dos.write(b);
		}

		for (int i = 0; i < m_pair_pool.size(); i++) {
			dos.writeLong(m_pair_pool.get(i));
		}

		
		{
			for (int i = 0; i < m_static_functions.size(); i++) {
				m_static_functions.get(i).outputData(dos);
			}		

			for (int i = 0; i < m_member_functions.size(); i++) {
				m_member_functions.get(i).outputData(dos);
			}	
		}
		
	}



	public static final int TYPE_CLASS     = 1;
	public static final int TYPE_ENUM      = 2;
	public static final int TYPE_INTERFACE = 3;

	int    m_type = 0;
	int    m_name = 0;
	int    m_extends = 0;
	ArrayList<Integer> m_implements = new ArrayList<Integer>();
	
	//public Hashlist m_labels = new Hashlist();
	public ArrayList<IGAssemblyUnitVariable> m_static_variables = new ArrayList<IGAssemblyUnitVariable>();
	public ArrayList<IGAssemblyUnitVariable> m_member_variables = new ArrayList<IGAssemblyUnitVariable>();	
	public ArrayList<IGAssemblyUnitMethod>   m_static_functions = new ArrayList<IGAssemblyUnitMethod>();
	public ArrayList<IGAssemblyUnitMethod>   m_member_functions = new ArrayList<IGAssemblyUnitMethod>();
	
	
	public void sortMemberVariables()
	{
		if (SORT_MEMBER_VARIABLES) {
			Collections.sort(m_member_variables);
		}
	
		/*
		System.out.println("sortMemberVariables");
		
		for (IGAssemblyUnitVariable v : m_member_variables) {
			System.out.println("\t" + v.m_storage_class);
		}
		*/
	
	}
	
	
	// is this the right approach? (yeah I think it'll do)
	public ArrayList<String> m_string_pool       = new ArrayList<String>();
	public ArrayList<Object> m_string_pool_bytes = new ArrayList<Object>();
	//private StringIntHashtable m_string_pool_membership = new StringIntHashtable();
	
	public int [] m_string_pool_lengths;
	public int [] m_data_pool_lengths;
	public LongVector    m_pair_pool   = new LongVector();
	//ArrayList<Integer> m_i32_pool   = new ArrayList<Integer>();
	//LongVector    m_i64_pool   = new LongVector();
	
	public void clear() {
		//m_string_pool_membership.clear();
	}
			
	public void setClassName(String n) {
		//System.err.println("class: " + n);
		m_name = _stringPool(n);
		m_type = TYPE_CLASS;
	}
	
	public void setEnumName(String n) {
		m_name = _stringPool(n);
		m_type = TYPE_ENUM;
	}
	
	public void setInterfaceName(String n) {
		m_name = _stringPool(n);
		m_type = TYPE_INTERFACE;
	}
	
	public void setExtends(String n) {
		//System.err.println("extends: " + n);
		if (m_type != TYPE_CLASS) {
			throw new RuntimeException("Only classes may extend other classes");
		}
		
		m_extends = _stringPool(n);
	}
	
	public void addImplements(String n) {
		if (m_type != TYPE_CLASS) {
			throw new RuntimeException("Only classes may implement other classes");
		}
		
		m_implements.add(_stringPool(n));
	}
	

	
	
	
	public String stringPool(String s) {
		_stringPool(s);
		return s;
	}
	
	// hrm... wont th
	public int _stringPool(String s) 
	{
		/*
		int idx = m_string_pool.size();
		int off = m_string_pool_membership.getput(s, idx);
		if (off == idx) 
		{
			try
			{
				m_string_pool.add(s);
				m_string_pool_bytes.add(s.getBytes("UTF-8"));
			}
			catch (UnsupportedEncodingException ex) {
				throw new RuntimeException("barf..");
			}
		}
		
		return off;
		*/
		
		int idx = m_string_pool.indexOf(s);
		if (idx != -1) {
			return idx;
		}
		
		//for (int i = 0; i < m_string_pool.size(); i++) {
		//	if (m_string_pool.get(i).equals(s)) {
		//		return i;
		//	}
		//}
		
		try
		{
			m_string_pool.add(s);
			m_string_pool_bytes.add(s.getBytes("UTF-8"));
		}
		catch (UnsupportedEncodingException ex) {
			throw new RuntimeException("barf..");
		}
		
		return m_string_pool.size() - 1;
		
	}
	
	
	public int pairPool(String a, String b) {
		return pairPool(_stringPool(a), _stringPool(b));
	}
	
	public int pairPool(int a, int b) {
		long la = a;
		long lb = b;
		long s = ((la & 0x00000000FFFFFFFFL) | ((lb & 0x00000000FFFFFFFFL) << 32));
		
		int idx = m_pair_pool.indexOf(s);
		if (idx >= 0) {
			return idx;
		}
		//for (int i = 0; i < m_pair_pool.size(); i++) {
		//	if (m_pair_pool.get(i) == s) {
		//		return i;
		//	}
		//}
		m_pair_pool.add(s);
		return m_pair_pool.size() - 1;		
	}
}



	public static void process(TextReader br, String f, File dst) throws IOException
	{
		IGAssemblyUnit unit = new IGAssemblyUnit();
		
		String [] toks = new String[16];

//		boolean has_sorted_member_vars = false;
		
		try
		{
			
			while (br.readASMLineToks(toks) > 0) 
			{
				String tok0 = toks[0];
			
				if (tok0.equals("class")) {
					unit.setClassName(unit.stringPool(toks[1]));
				}
				else if (tok0.equals("enum")) {
					unit.setEnumName(unit.stringPool(toks[1]));
				}
				else if (tok0.equals("interface")) {
					unit.setInterfaceName(unit.stringPool(toks[1]));
				}
				else if (tok0.equals("extends")) {
					unit.setExtends(unit.stringPool(toks[1]));
				}
				else if (tok0.equals("implements")) {
					unit.addImplements(unit.stringPool(toks[1]));
				}
				else if (tok0.equals("full-extends"))
				{
				}
				else if (tok0.equals("full-implements"))
				{
				}
				else if (tok0.equals("var") || tok0.equals("static-var")) 
				{
				
					IGAssemblyUnitVariable var = new IGAssemblyUnitVariable(unit);
					var.setStatic(tok0.equals("static-var"));
					
					var.setProtection(toks[1]);
					var.setName   (unit._stringPool(toks[2]));
					var.setExactType(unit._stringPool(toks[3]));
					var.setStorageType(unit._stringPool(toks[4]));
				
					var.setStorageClass(Integer.parseInt(toks[5]));
				
					if (var.m_static) {	unit.m_static_variables.add(var); }
					else              {	unit.m_member_variables.add(var); }
				}
				else if (tok0.equals("function") || tok0.equals("static-function")) {
				
					//if (!has_sorted_member_vars) {
						//
					//	//
//
//						unit.sortMemberVariables();
//						has_sorted_member_vars = true;
//					}
				
					IGAssemblyUnitMethod method = new IGAssemblyUnitMethod(unit);
					method.setStatic(tok0.equals("static-function"));
					method.setProtection(toks[1]);
					method.setName(unit.stringPool(toks[2]));
					
				
					if (method.m_static) { unit.m_static_functions.add(method);   }
					else                 { unit.m_member_functions.add(method);   }
				
					String arg_count   = toks[3];
				
					int arg_min = -1;
					int arg_max = -1;
					int local_count = 0;
					
					int range_index = arg_count.indexOf("..");
					if (range_index == -1) {
						arg_min = arg_max = Integer.parseInt(arg_count);
					}
					else
					{
						//String [] arg_toks = IGAssembler.RANGE_SPLIT.split(arg_count);
						arg_min = Integer.parseInt(arg_count.substring(0, range_index));
						arg_max = Integer.parseInt(arg_count.substring(range_index + 2));
					}
					
					local_count = Integer.parseInt(toks[4]);
					method.setReturns(Integer.parseInt(toks[5]));
					
					method.setCounts(arg_min, arg_max, local_count + arg_max);
				
					//line = br.readASMLine();
					while (br.readASMLineToks(toks) > 0) 
					{
						// we're done for the time being
						if (toks[0].equals("END")) {
							break;
						}
					
						method.addCodeLine(toks);
						//line = br.readASMLine();
					}
					method.resolveLabels();
				}
				
				else
				{
					throw new RuntimeException("Invalid ig assembly code");
				}
			
		
				//line = br.readASMLine();
			}	
			
			System.out.println("You should be sorting!!!");
			unit.sortMemberVariables();
		
		}
		catch (Exception ex) {
			System.out.println("" + dst);
			util.Log.logErrorEcho(f, "Assembler failure. " + br.getLineNumber() + "> " + br.getCurrentLine());
			ex.printStackTrace();
			System.exit(1);
		}	
	
		try
		{
			LEDataOutputStream dos = new LEDataOutputStream(new BufferedOutputStream(new FileOutputStream(dst)));
			unit.output(dos);
			dos.close();
		
		}
		catch (IOException ex) 
		{
			util.Log.logErrorEcho("" + dst, "writing failed");
			ex.printStackTrace();
			System.exit(1);
		}
	
		unit.clear();
	}
	
	
	public static void process(IGAsmDocument br, String f, File dst) throws IOException
	{
		IGAssemblyUnit unit = new IGAssemblyUnit();
		

		try
		{
			for (String [] s : br.m_meta)
			{
				String [] toks = s;
				String tok0 = toks[0];
			
				if (tok0.equals("class")) {
					unit.setClassName(unit.stringPool(toks[1]));
				}
				else if (tok0.equals("enum")) {
					unit.setEnumName(unit.stringPool(toks[1]));
				}
				else if (tok0.equals("interface")) {
					unit.setInterfaceName(unit.stringPool(toks[1]));
				}
				else if (tok0.equals("extends")) {
					unit.setExtends(unit.stringPool(toks[1]));
				}
				else if (tok0.equals("implements")) {
					unit.addImplements(unit.stringPool(toks[1]));
				}
				else if (tok0.equals("full-extends"))
				{
				}
				else if (tok0.equals("full-implements"))
				{
				}
				else
				{
					throw new RuntimeException("Invalid ig assembly code");
				}
		
			}

			for (IGAsmHeader header : br. m_headers)
			{
				String [] toks = header.m_header;
				String tok0 = toks[0];
		
				if (tok0.equals("function") || tok0.equals("static-function")) 
				{
					IGAssemblyUnitMethod method = new IGAssemblyUnitMethod(unit);
					method.setStatic(tok0.equals("static-function"));
					method.setProtection(toks[1]);
					method.setName(unit.stringPool(toks[2]));
				
			
					if (method.m_static) { unit.m_static_functions.add(method);   }
					else                 { unit.m_member_functions.add(method);   }
			
					String arg_count   = toks[3];
			
					int arg_min = -1;
					int arg_max = -1;
					int local_count = 0;
				
					int range_index = arg_count.indexOf("..");
					if (range_index == -1) {
						arg_min = arg_max = Integer.parseInt(arg_count);
					}
					else
					{
						//String [] arg_toks = IGAssembler.RANGE_SPLIT.split(arg_count);
						arg_min = Integer.parseInt(arg_count.substring(0, range_index));
						arg_max = Integer.parseInt(arg_count.substring(range_index + 2));
					}
				
					local_count = Integer.parseInt(toks[4]);
					method.setReturns(Integer.parseInt(toks[5]));
					method.setCounts(arg_min, arg_max, local_count + arg_max);
			
					for (IGAsmOp op : header.m_ops)
					{
						if (op.m_op != -1) {
							if (op.m_op == IGAsm.END) {
								break;
							}	
							method.addOp(op);	
						}			
					}
				
					method.resolveLabels();
				}
				else if (tok0.equals("var") || tok0.equals("static-var")) 
				{
			
					IGAssemblyUnitVariable var = new IGAssemblyUnitVariable(unit);
					var.setStatic(tok0.equals("static-var"));
				
					var.setProtection(toks[1]);
					var.setName   (unit._stringPool(toks[2]));
					var.setExactType(unit._stringPool(toks[3]));
					var.setStorageType(unit._stringPool(toks[4]));
			
					var.setStorageClass(Integer.parseInt(toks[5]));
			
					if (var.m_static) {	unit.m_static_variables.add(var); }
					else              {	unit.m_member_variables.add(var); }
				}
				else
				{
					throw new RuntimeException("Invalid ig assembly code");
				}
			}
			
			
			unit.sortMemberVariables();
		
		}
		catch (Exception ex) {
		
		
			System.out.println("" + dst);
			util.Log.logErrorEcho(f, "Assembler failure. ");
			ex.printStackTrace();
			System.exit(1);
		}	
	
		try
		{
			LEDataOutputStream dos = new LEDataOutputStream(new BufferedOutputStream(new FileOutputStream(dst)));
			unit.output(dos);
			dos.close();
		
		}
		catch (IOException ex) 
		{
			util.Log.logErrorEcho("" + dst, "writing failed");
			ex.printStackTrace();
			System.exit(1);
		}
	
		unit.clear();
	}
}
