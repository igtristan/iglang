/*
Copyright (c) 2014-2017 Incubator Games Ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

package pass2.asm;

/**
 * This links and IGAsmDocument to the IGSource file that it was generated from
 */


import util.*;
import pass1.*;


import java.util.*;
import java.io.*;

public final class IGAsmTarget
{
	public IGAsmDocument m_writer;
	public IGSource    m_source;
	public String      m_src_path;
	public String      m_dst_path;
	public String      m_asm_file_dst;
	
	
	public String m_full_path;
	
	public Hashtable<String, IGAsmHeader> m_name_to_header = null;

	public IGAsmDocument getWriter() {
		return m_writer;
	}

	public IGAsmDocument getDocument() {
		return m_writer;
	}
	
	// is this even used?
	public IGAsmHeader findInlineCandidateStaticFunction1to1(String name) {
	
		if (null == m_name_to_header) {
			m_name_to_header = new Hashtable<String, IGAsmHeader>();
			
			for (IGAsmHeader h : m_writer.m_headers) 
			{	
				m_name_to_header.put(h.m_header[2], h);
			}
		}
		
		
		IGAsmHeader h2 =  m_name_to_header.get(name);
		if (h2 == null || h2.m_inlineable == 2) { return null; }
		if (h2.m_inlineable == 1) { return h2; }
		
		IGAsmHeader header = null;
		//for (IGAsmHeader h : m_writer.m_headers) 
		{
			IGAsmHeader h = h2;
			h.m_inlineable = 2;
			
			if (h != null && ("static-function".equals(h.m_header[0]) ||
			                   "function".equals(h.m_header[0])) && 
			   name.equals(h.m_header[2]) && 
			   h.m_header[3].equals("1..1") &&
			   "FINAL".equals(h.m_header[6])) {
				header = h;
			}
		}
		
		if (header == null) { return null; }

		
		final ArrayList<IGAsmOp> ops = header.m_raw_ops;
		final int                sz  = ops.size();
		
		if (sz < 4) { return null; }
		if (sz > 16) { return null; }
		
		
		if (! (IGAsm.param_undef == ops.get(0).m_op) ) {                               
			return null;
		}
		if (! ((IGAsm.ll0 == ops.get(1).m_op) || 
			  ((IGAsm.local_load == ops.get(1).m_op)  && "0".equals(ops.get(1).m_p0)  ) 
			 // || "this_load == ops.get(1).m_op)
			  
			  )
			  ) { 
			return null;
		}
		if (! (IGAsm.ret1 == ops.get(sz-2).m_op)  ) {               
			return null;
		}
		if (! (IGAsm.END == ops.get(sz-1).m_op)  ) {               
			return null;
		}
		
		for (int i = 2; i < sz - 2; i++)
		{
			final int op = ops.get(i).m_op;

			boolean valid = ( (IGAsm.mul_i32 == op)     ||
							  (IGAsm.add_i32 == op)     ||
							  (IGAsm.push_i32 == op)    ||
							  (IGAsm.push_i8 == op)     ||
							  (IGAsm.sub_i32 == op)     ||
							  (IGAsm.bit_and_i32 == op) ||
							  (IGAsm.bit_or_i32 == op)  ||
							  (IGAsm.div_i32 == op)     ||
							  (IGAsm.asr_i32 == op)     ||
							  (IGAsm.lsr_i32 == op)     ||
							  (IGAsm.lsl_i32 == op)     ||
							  (IGAsm.cmp_eq_i32 == op)  ||
							 // (IGAsm.cmp_ne_i32 == op)  ||
							  (IGAsm.not == op));
			
			if (!valid) { 
				return null;
			}
		}
		
		//if (name.equals("x__get")) {
		//System.out.println("Is VALID!");
		//}
		header.m_inlineable = 1;
		return header;
	}
	
	

	public void optimize(Hashtable<String,IGAsmTarget> targets)
	{
		m_writer.optimize(targets);
	}



	/**
	 * Used for writing out final block of assembl
	 */
	public void writeAsmFile(Hashtable<String, IGAsmTarget> targets)
	{
		try
		{
			m_writer.optimizeAndWrite(targets, pass2.Util.createOutputPrintWriter(m_source,m_asm_file_dst  + ".asm"));		
		}
		catch (IOException ex) {
			ex.printStackTrace();
		}
	}
	
	public IGAsmTarget(IGAsmDocument w, IGSource a, String b, String c, String asm_file_dst, String full_path) {
		m_writer = w;
		m_source = a;
		m_src_path = b;
		m_dst_path = c;
		m_asm_file_dst = asm_file_dst;
		m_full_path = full_path;
	}
}
