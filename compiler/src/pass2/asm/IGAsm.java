/*
Copyright (c) 2014-2017 Incubator Games Ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

package pass2.asm;

// table a direct encoding of the s_simple map
// these are all the meta ops.  Ie used to communicate to the assembler, but
// not as output to the vm
public class IGAsm
{
		public static final int nop = IGAssembler.IGAssemblyUnitMethod. NOP;
		public static final int nop_term = IGAssembler.IGAssemblyUnitMethod. NOP_TERM;
		public static final int pop = IGAssembler.IGAssemblyUnitMethod. POP;
		public static final int ll0 = IGAssembler.IGAssemblyUnitMethod. LL0;
		public static final int ll1 = IGAssembler.IGAssemblyUnitMethod. LL1;
		public static final int ll2 = IGAssembler.IGAssemblyUnitMethod. LL2;
		public static final int ll3 = IGAssembler.IGAssemblyUnitMethod. LL3;	
		public static final int ll4 = IGAssembler.IGAssemblyUnitMethod. LL4;	
				
		public static final int dup = IGAssembler.IGAssemblyUnitMethod. DUP;
		public static final int dup2 = IGAssembler.IGAssemblyUnitMethod. DUP2;
		public static final int dup1x = IGAssembler.IGAssemblyUnitMethod. DUP1X;
		public static final int dup2x = IGAssembler.IGAssemblyUnitMethod. DUP2X;
		public static final int ret0 = IGAssembler.IGAssemblyUnitMethod.  RET0;
		public static final int ret1 = IGAssembler.IGAssemblyUnitMethod.  RET1;
		public static final int ret_obj = IGAssembler.IGAssemblyUnitMethod. RET_OBJ;
		public static final int ret_funcptr = IGAssembler.IGAssemblyUnitMethod. RET_FUNCPTR;
		public static final int add_i32 = IGAssembler.IGAssemblyUnitMethod. ADD_I32;
		public static final int sub_i32 = IGAssembler.IGAssemblyUnitMethod. SUB_I32;
		public static final int mul_i32 = IGAssembler.IGAssemblyUnitMethod. MUL_I32;
		public static final int div_i32 = IGAssembler.IGAssemblyUnitMethod. DIV_I32;
		public static final int mod_i32 = IGAssembler.IGAssemblyUnitMethod. MOD_I32;
		


		public static final int add_f64_vec = IGAssembler.IGAssemblyUnitMethod. ADD_F64_VEC;
		public static final int sub_f64_vec = IGAssembler.IGAssemblyUnitMethod. SUB_F64_VEC;
		public static final int mul_f64_vec = IGAssembler.IGAssemblyUnitMethod. MUL_F64_VEC;
		public static final int mul_f64_vec_val = IGAssembler.IGAssemblyUnitMethod. MUL_F64_VEC_VAL;

		public static final int add_f64 = IGAssembler.IGAssemblyUnitMethod. ADD_F64;
		public static final int sub_f64 = IGAssembler.IGAssemblyUnitMethod. SUB_F64;
		public static final int mul_f64 = IGAssembler.IGAssemblyUnitMethod. MUL_F64;
		public static final int div_f64 = IGAssembler.IGAssemblyUnitMethod. DIV_F64;
		public static final int mod_f64 = IGAssembler.IGAssemblyUnitMethod. MOD_F64;
		
		public static final int bit_and_i32 = IGAssembler.IGAssemblyUnitMethod. AND_I32;
		public static final int bit_or_i32 = IGAssembler.IGAssemblyUnitMethod.  OR_I32;
		public static final int bit_xor_i32 = IGAssembler.IGAssemblyUnitMethod. XOR_I32;
		public static final int bit_not_i32 = IGAssembler.IGAssemblyUnitMethod. BIT_NOT_I32;
		
		public static final int not = IGAssembler.IGAssemblyUnitMethod.         NOT;
		public static final int lsl_i32 = IGAssembler.IGAssemblyUnitMethod. LSL_I32;
		public static final int lsr_i32 = IGAssembler.IGAssemblyUnitMethod. LSR_I32;
		public static final int asr_i32 = IGAssembler.IGAssemblyUnitMethod. ASR_I32;
		
		public static final int cmp_eq_i32 = IGAssembler.IGAssemblyUnitMethod. CMP_EQ_I32;
		public static final int cmp_lt_i32 = IGAssembler.IGAssemblyUnitMethod. CMP_LT_I32;
		public static final int cmp_le_i32 = IGAssembler.IGAssemblyUnitMethod. CMP_LE_I32;
		
		public static final int cmp_eq_f64 = IGAssembler.IGAssemblyUnitMethod. CMP_EQ_F64;
		public static final int cmp_lt_f64 = IGAssembler.IGAssemblyUnitMethod. CMP_LT_F64;
		public static final int cmp_le_f64 = IGAssembler.IGAssemblyUnitMethod. CMP_LE_F64;
	
		public static final int cmp_eq_obj = IGAssembler.IGAssemblyUnitMethod. CMP_EQ_OBJ;	// NOTE THIS WEIRD MAPPING
		public static final int cmp_eq_fp = IGAssembler.IGAssemblyUnitMethod. CMP_EQ_I64;	// ALSO NOTE THIS WEIRD MAPPING
		public static final int i32_to_f64 = IGAssembler.IGAssemblyUnitMethod. I32_TO_F64;
		public static final int f64_to_i32 = IGAssembler.IGAssemblyUnitMethod. F64_TO_I32;
			
		public static final int catch_entry = IGAssembler.IGAssemblyUnitMethod. CATCH_ENTRY;	
		public static final int array_length = IGAssembler.IGAssemblyUnitMethod. ARRAY_LENGTH;
		
		
		public static final int param_undef = IGAssembler.IGAssemblyUnitMethod. META_param_undef;
		public static final int param_i32 = IGAssembler.IGAssemblyUnitMethod. META_param_i32;
		public static final int param_i64 = IGAssembler.IGAssemblyUnitMethod. META_param_i64;
		public static final int param_f64 = IGAssembler.IGAssemblyUnitMethod. META_param_f64;
		public static final int param_object = IGAssembler.IGAssemblyUnitMethod.META_param_object;
		public static final int param_func_ptr = IGAssembler.IGAssemblyUnitMethod.META_param_func_ptr;
		
		public static final int push_f64 = IGAssembler.IGAssemblyUnitMethod. META_push_f64;
		public static final int push_i64 = IGAssembler.IGAssemblyUnitMethod. META_push_i64;
		public static final int push_i32 = IGAssembler.IGAssemblyUnitMethod. META_push_i32;
		

		public static final int push_h32 = IGAssembler.IGAssemblyUnitMethod. META_push_h32;
		public static final int push_string = IGAssembler.IGAssemblyUnitMethod. META_push_string;
		public static final int local_store = IGAssembler.IGAssemblyUnitMethod. META_local_store;
		public static final int local_load = IGAssembler.IGAssemblyUnitMethod. META_local_load;
		public static final int local_store_keep = IGAssembler.IGAssemblyUnitMethod. META_local_store_keep;

		public static final int local_load_2 = IGAssembler.IGAssemblyUnitMethod. META_local_load_2;
		public static final int local_clear = IGAssembler.IGAssemblyUnitMethod. META_local_clear;
//		public static final int create_it = IGAssembler.IGAssemblyUnitMethod. META_create_it;
		public static final int member_store = IGAssembler.IGAssemblyUnitMethod. META_member_store;
		public static final int member_load = IGAssembler.IGAssemblyUnitMethod. META_member_load;
		public static final int static_store = IGAssembler.IGAssemblyUnitMethod. META_static_store;
		public static final int static_load = IGAssembler.IGAssemblyUnitMethod. META_static_load;
		public static final int this_store = IGAssembler.IGAssemblyUnitMethod. META_this_store;
		public static final int this_load = IGAssembler.IGAssemblyUnitMethod. META_this_load;
		public static final int cast_to = IGAssembler.IGAssemblyUnitMethod. META_cast_to;
		
		public static final int call_interface = IGAssembler.IGAssemblyUnitMethod. META_call_interface;
		public static final int call_vtable = IGAssembler.IGAssemblyUnitMethod. META_call_vtable;
		public static final int call_static = IGAssembler.IGAssemblyUnitMethod. META_call_static;
		public static final int call_static_vtable = IGAssembler.IGAssemblyUnitMethod. META_call_static_vtable;
		public static final int call_dynamic = IGAssembler.IGAssemblyUnitMethod. META_call_dynamic;
		public static final int call_super = IGAssembler.IGAssemblyUnitMethod. META_call_super;
		
		public static final int call_ret1_interface = IGAssembler.IGAssemblyUnitMethod.		META_call_ret1_interface;
		public static final int call_ret1_vtable = IGAssembler.IGAssemblyUnitMethod. 		META_call_ret1_vtable;
		public static final int call_ret1_static = IGAssembler.IGAssemblyUnitMethod. 		META_call_ret1_static;
		public static final int call_ret1_static_vtable = IGAssembler.IGAssemblyUnitMethod. META_call_ret1_static_vtable;
		public static final int call_ret1_dynamic = IGAssembler.IGAssemblyUnitMethod. 		META_call_ret1_dynamic;
		public static final int call_ret1_super = IGAssembler.IGAssemblyUnitMethod. 		META_call_ret1_super;	

		
		public static final int funcptr_member = IGAssembler.IGAssemblyUnitMethod. META_funcptr_member;
		public static final int funcptr_static = IGAssembler.IGAssemblyUnitMethod. META_funcptr_static;
		public static final int try_push = IGAssembler.IGAssemblyUnitMethod. META_try_push;
		public static final int instance_of = IGAssembler.IGAssemblyUnitMethod. META_instance_of;
		public static final int __new = IGAssembler.IGAssemblyUnitMethod. META_new;
		public static final int __throw = IGAssembler.IGAssemblyUnitMethod. META_throw;
		public static final int jmp = IGAssembler.IGAssemblyUnitMethod. META_jmp;
		public static final int j0_i32 = IGAssembler.IGAssemblyUnitMethod. META_j0_i32;
		public static final int jn0_i32 = IGAssembler.IGAssemblyUnitMethod. META_jn0_i32;
		public static final int jeq_i32 = IGAssembler.IGAssemblyUnitMethod. META_jeq_i32;
		public static final int and = IGAssembler.IGAssemblyUnitMethod. META_and;
		public static final int or = IGAssembler.IGAssemblyUnitMethod. META_or;
		public static final int next_it = IGAssembler.IGAssemblyUnitMethod. META_next_it;
//		public static final int release = IGAssembler.IGAssemblyUnitMethod. META_release;
		public static final int array_load = IGAssembler.IGAssemblyUnitMethod. META_array_load;
		public static final int array_store = IGAssembler.IGAssemblyUnitMethod. META_array_store;
		public static final int array_new = IGAssembler.IGAssemblyUnitMethod. META_array_new;	
		public static final int array_resize = IGAssembler.IGAssemblyUnitMethod. META_array_resize;	
		public static final int local_inc_i8 = IGAssembler.IGAssemblyUnitMethod. META_local_inc_i8;
		public static final int stack_inc_i8 = IGAssembler.IGAssemblyUnitMethod. META_stack_inc_i8;
		public static final int this_inc_i32 = IGAssembler.IGAssemblyUnitMethod. META_this_inc_i32;
		
		public static final int array_fill = IGAssembler.IGAssemblyUnitMethod.   META_array_fill;
		public static final int array_copy = IGAssembler.IGAssemblyUnitMethod.   META_array_copy;
		
		public static final int asr_i32_const = IGAssembler.IGAssemblyUnitMethod. META_ASR_I32_CONST;
		public static final int lsr_i32_const = IGAssembler.IGAssemblyUnitMethod. META_LSR_I32_CONST;
		public static final int lsl_i32_const = IGAssembler.IGAssemblyUnitMethod. META_LSL_I32_CONST;
		
		public static final int mla_i32       = IGAssembler.IGAssemblyUnitMethod.MLA_I32;
		public static final int mla_f64       = IGAssembler.IGAssemblyUnitMethod.MLA_F64;		
		public static final int lsl_asr_i32_const = IGAssembler.IGAssemblyUnitMethod.META_lsl_asr_i32_const;
		public static final int switch_i32_jmp    = IGAssembler.IGAssemblyUnitMethod.META_switch_i32_jmp;
		
		
		// these can only be created by the optimizer
		public static final int push_i32_0 = IGAssembler.IGAssemblyUnitMethod.PUSH_I32_0;
		public static final int push_i32_1 = IGAssembler.IGAssemblyUnitMethod.PUSH_I32_1;
		public static final int push_f64_0 =  IGAssembler.IGAssemblyUnitMethod.PUSH_F64_0;
		public static final int push_f64_1 =  IGAssembler.IGAssemblyUnitMethod.PUSH_F64_1;
		public static final int push_i8 = IGAssembler.IGAssemblyUnitMethod. META_push_i8;
		public static final int push_i16 = IGAssembler.IGAssemblyUnitMethod. META_push_i16;		
			
		public static final int jr_i32 = IGAssembler.IGAssemblyUnitMethod. META_jr_i32;
		
		public static final int debug_block_start = IGAssembler.IGAssemblyUnitMethod.DEBUG_BLOCK_START;
		public static final int debug_block_end = IGAssembler.IGAssemblyUnitMethod.DEBUG_BLOCK_END;
		
		//public static final int local_store_lo = IGAssembler.IGAssemblyUnitMethod. META_local_store_lo;
		public static final int local_load_lo = IGAssembler.IGAssemblyUnitMethod. META_local_load_lo;
		
		
		
		
		public static final int load_obj_field = IGAssembler.IGAssemblyUnitMethod.META_load_obj_field;//       = 135;
				
		public static final int push_obj_null = IGAssembler.IGAssemblyUnitMethod.META_push_obj_null;
		public static final int push_fp_null  = IGAssembler.IGAssemblyUnitMethod.META_push_fp_null;			
		public static final int END = IGAssembler.IGAssemblyUnitMethod.META_end;
}