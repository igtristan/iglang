/*
Copyright (c) 2014-2017 Incubator Games Ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

package pass2.asm;


import javax.xml.bind.DatatypeConverter;
import java.io.*;

public final class IGAsmOp
{
	public int  m_line_number = -1;	// the line number from the file this was generated from
	
	public String m_label = null;	// the label for the op.  null if DNE.
	public int    m_op    = -1;		// the numeric op as defined in AGAsm
	
	public String m_p0 = null;
	public String m_p1 = null;
	public String m_p2 = null;
	public String m_p3 = null;
	public String m_p4 = null;
	public String m_p5 = null;
	
	public String m_comment = null;
	
	IGAsmOp m_next = null;
	
	public int getArgCount() {
		return Integer.parseInt(m_p0);
	}
	public boolean isLabeledNOP() {
		return m_label != null && m_op == IGAsm.nop;
	}
	
	public IGAsmOp inlineClone() {
		IGAsmOp ret =  new IGAsmOp(this);
		ret.m_line_number = -1;
		return ret;
	}

	public IGAsmOp inlineCloneWithLabel(String label) {
		IGAsmOp ret = new IGAsmOp(this);
		ret.m_label = label;
		ret.m_line_number = -1;
		return ret;
	}
	
	public IGAsmOp()
	{
	
	}
	
	public IGAsmOp(IGAsmOp other) {
		
		m_line_number = other.m_line_number;
		m_label = other.m_label;
		m_op = other.m_op;
		m_p0 = other.m_p0;
		m_p1 = other.m_p1;
		m_p2 = other.m_p2;
		m_p3 = other.m_p3;
		
		// for iterator
		m_p4 = other.m_p4;
		m_p5 = other.m_p5;
		m_comment = other.m_comment;
	}

	
	public @Override String toString()
	{
		StringBuilder sb = new StringBuilder();
		if (m_line_number != -1) {
			sb.append('@');
			sb.append(m_line_number);
			sb.append(' ');
		}
		
		if (m_label != null) {
			sb.append(m_label);
			sb.append(": ");
		}
		else {
			sb.append("\t");
		}
		
		if (m_op != -1)
		{
			if (m_op == IGAsm.push_string) 
			{
				String   bytesEncoded = DatatypeConverter.printBase64Binary(m_p0.getBytes());
						
				sb.append(convertBack(m_op));
				sb.append(" ");
				sb.append("base64:" + bytesEncoded);	// new p0
			}
			else
			{
				sb.append(convertBack(m_op));
				
				if (m_p0 != null) {
					sb.append(" ");
					sb.append(m_p0);
					
					if (m_p1 != null) {
						sb.append(" ");
						sb.append(m_p1);
						
						if (m_p2 != null) {
							sb.append(" ");
							sb.append(m_p2);
							
							if (m_p3 != null) {
								sb.append(" ");
								sb.append(m_p3);
							}
							if (m_p4 != null) {
								sb.append(" ");
								sb.append(m_p4);
							}
							if (m_p5 != null) {
								sb.append(" ");
								sb.append(m_p5);
							}
						}
					}
				}	
			}
		}
		
		
		if (m_comment != null) 
		{
			sb.append(" #");
			sb.append(m_comment);
			
		}
		
		return sb.toString();
	}
	
	public void output(PrintWriter pw) {
	
	
		if (m_line_number != -1) {
			pw.print('@');
			pw.print(m_line_number);
			pw.print('\t');
		}
			
	
		if (m_label != null) {
			pw.print(m_label);
			pw.print(": ");
		}
		else {
			pw.print("\t");
		}
		
		if (m_op != -1)
		{
			if (m_op == IGAsm.push_string) 
			{
				String   bytesEncoded = DatatypeConverter.printBase64Binary(m_p0.getBytes());
						
				pw.print(convertBack(m_op));
				pw.print(" ");
				pw.print("base64:" + bytesEncoded);	// new p0
			}
			else
			{
				pw.print(convertBack(m_op));
				if (m_p0 != null) 
				{
					pw.print(" ");
					pw.print(m_p0);
				
					if (m_p1 != null) {
						pw.print(" ");
						pw.print(m_p1);
					
						if (m_p2 != null) {
							pw.print(" ");
							pw.print(m_p2);
						
							if (m_p3 != null) {
								pw.print(" ");
								pw.print(m_p3);
							}
							if (m_p4 != null) {
								pw.print(" ");
								pw.print(m_p4);
							}
							if (m_p5 != null) {
								pw.print(" ");
								pw.print(m_p5);
							}
						}			
					}		
				}
			}
		}
		
		if (m_comment != null) 
		{
			pw.print(" #");
			pw.print(m_comment);	
		}
		

		
		pw.println();
	}
	

	public  IGAsmOp comment(String c)
	{ 
		IGAsmOp obj = new IGAsmOp();
		obj.m_comment = c;
		obj.m_next = this;
		return obj; 
	}
	
	public  IGAsmOp label(String l)
	{ 
		IGAsmOp obj = new IGAsmOp();
		obj.m_label = l;
		obj.m_op = convert("nop");
		obj.m_next = this;
		return obj; 
	}
	
	public  IGAsmOp op(String o)
	{ 
		IGAsmOp obj = new IGAsmOp();
		obj.m_op = convert(o);
		obj.m_next = this;
		return obj; 
	}
	
	public  IGAsmOp op(String o, String p0)
	{ 	
		IGAsmOp obj = new IGAsmOp();
		obj.m_op = convert(o);
		obj.m_p0 = p0;
		obj.m_next = this;
		return obj; 
	}
	
	public  IGAsmOp op(String o, String p0, String p1)
	{ 
		IGAsmOp obj = new IGAsmOp();
		obj.m_op = convert(o);
		obj.m_p0 = p0;
		obj.m_p1 = p1;
		obj.m_next = this;
		return obj; 
	}
	
	public  IGAsmOp op(String o, String p0, String p1, String p2)
	{ 	
		IGAsmOp obj = new IGAsmOp();
		obj.m_op = convert(o);
		obj.m_p0 = p0;
		obj.m_p1 = p1;
		obj.m_p2 = p2;
		obj.m_next = this;
		return obj; 
	}
	
	public  IGAsmOp op(String o, String p0, String p1, String p2, String p3, String p4, String p5)
	{ 
		IGAsmOp obj = new IGAsmOp();
		obj.m_op = convert(o);
		obj.m_p0 = p0;
		obj.m_p1 = p1;
		obj.m_p2 = p2;
		obj.m_p3 = p3;
		obj.m_p4 = p4;
		obj.m_p5 = p5;
		obj.m_next = this;
		return obj; 
	}
	
		
	public  IGAsmOp op(int o)
	{ 
		IGAsmOp obj = new IGAsmOp();
		obj.m_op = (o);
		obj.m_next = this;
		return obj; 
	}
	
	public  IGAsmOp op(int o, String p0)
	{ 	
		IGAsmOp obj = new IGAsmOp();
		obj.m_op = (o);
		obj.m_p0 = p0;
		obj.m_next = this;
		return obj; 
	}
	
	public  IGAsmOp op(int o, String p0, String p1)
	{ 
		IGAsmOp obj = new IGAsmOp();
		obj.m_op = (o);
		obj.m_p0 = p0;
		obj.m_p1 = p1;
		obj.m_next = this;
		return obj; 
	}
	
	public  IGAsmOp op(int o, String p0, String p1, String p2)
	{ 	
		IGAsmOp obj = new IGAsmOp();
		obj.m_op = (o);
		obj.m_p0 = p0;
		obj.m_p1 = p1;
		obj.m_p2 = p2;
		obj.m_next = this;
		return obj; 
	}
	
	public  IGAsmOp op(int o, String p0, String p1, String p2, String p3, String p4, String p5)
	{ 
		IGAsmOp obj = new IGAsmOp();
		obj.m_op = (o);
		obj.m_p0 = p0;
		obj.m_p1 = p1;
		obj.m_p2 = p2;
		obj.m_p3 = p3;
		obj.m_p4 = p4;
		obj.m_p5 = p5;
		obj.m_next = this;
		return obj; 
	}
	
	
	public  IGAsmOp op_comment(String o, String c)
	{ 	IGAsmOp obj = new IGAsmOp();
		obj.m_op = convert(o);
		obj.m_comment = c;
		obj.m_next = this;
		return obj; 
	}
	
	public  IGAsmOp op_comment(String o, String p0, String c)
	{ 	IGAsmOp obj = new IGAsmOp();
		obj.m_op = convert(o);
		obj.m_p0 = p0;
		obj.m_comment = c;
		obj.m_next = this;
		return obj; 
	}
	
	public  IGAsmOp op_comment(String o, String p0, String p1, String c)
	{
	 	IGAsmOp obj = new IGAsmOp();
		obj.m_op = convert(o);
		obj.m_p0 = p0;	
		obj.m_p1 = p1;
		obj.m_comment = c;
		obj.m_next = this;
		return obj; 
	}
	
	public  IGAsmOp op_comment(String o, String p0, String p1, String p2, String c)
	{ 
		IGAsmOp obj = new IGAsmOp();
		obj.m_op = convert(o);
		obj.m_p0 = p0;	
		obj.m_p1 = p1;
		obj.m_p2 = p2;
		obj.m_comment = c;
		obj.m_next = this;
		return obj; 
	}
	
	
	public  IGAsmOp op_comment(int o, String c)
	{ 	IGAsmOp obj = new IGAsmOp();
		obj.m_op = (o);
		obj.m_comment = c;
		obj.m_next = this;
		return obj; 
	}
	
	public  IGAsmOp op_comment(int o, String p0, String c)
	{ 	IGAsmOp obj = new IGAsmOp();
		obj.m_op = (o);
		obj.m_p0 = p0;
		obj.m_comment = c;
		obj.m_next = this;
		return obj; 
	}
	
	public  IGAsmOp op_comment(int o, String p0, String p1, String c)
	{
	 	IGAsmOp obj = new IGAsmOp();
		obj.m_op = (o);
		obj.m_p0 = p0;	
		obj.m_p1 = p1;
		obj.m_comment = c;
		obj.m_next = this;
		return obj; 
	}
	
	public  IGAsmOp op_comment(int o, String p0, String p1, String p2, String c)
	{ 
		IGAsmOp obj = new IGAsmOp();
		obj.m_op = (o);
		obj.m_p0 = p0;	
		obj.m_p1 = p1;
		obj.m_p2 = p2;
		obj.m_comment = c;
		obj.m_next = this;
		return obj; 
	}
	
	
	public static IGAsmOp make_comment(String c)
	{ 
		IGAsmOp obj = new IGAsmOp();
		obj.m_comment = c;
		return obj; 
	}
	
	public static IGAsmOp make_label(String l)
	{ IGAsmOp obj = new IGAsmOp();
		obj.m_label = l;
		obj.m_op = convert("nop");
	return obj; }
	
	public static IGAsmOp make_op(String o)
	{ IGAsmOp obj = new IGAsmOp();
		obj.m_op = convert(o);
	return obj; }
	
	public static IGAsmOp make_op(String o, String p0)
	{ IGAsmOp obj = new IGAsmOp();
		obj.m_op = convert(o);
		obj.m_p0 = p0;
	return obj; }
	
	public static IGAsmOp make_op(String o, String p0, String p1)
	{ IGAsmOp obj = new IGAsmOp();
		obj.m_op = convert(o);
		obj.m_p0 = p0;
		obj.m_p1 = p1;
	return obj; }
	
	public static IGAsmOp make_op(String o, String p0, String p1, String p2)
	{ IGAsmOp obj = new IGAsmOp();
		obj.m_op = convert(o);
		obj.m_p0 = p0;
		obj.m_p1 = p1;
		obj.m_p2 = p2;
	return obj; }
	
	
	public static IGAsmOp make_op(String o, String p0, String p1, String p2, String p3, String p4, String p5)
	{ IGAsmOp obj = new IGAsmOp();
		obj.m_op = convert(o);
		obj.m_p0 = p0;
		obj.m_p1 = p1;
		obj.m_p2 = p2;
		obj.m_p3 = p3;
		obj.m_p4 = p4;
		obj.m_p5 = p5;
	return obj; }
	
	
	public static IGAsmOp make_op(int o)
	{ IGAsmOp obj = new IGAsmOp();
		obj.m_op = (o);
	return obj; }
	
	
		public static IGAsmOp make_op(int o, String p0)
	{ IGAsmOp obj = new IGAsmOp();
		obj.m_op = (o);
		obj.m_p0 = p0;
	return obj; }
	
	public static IGAsmOp make_op(int o, String p0, String p1)
	{ IGAsmOp obj = new IGAsmOp();
		obj.m_op = (o);
		obj.m_p0 = p0;
		obj.m_p1 = p1;
	return obj; }
	
	public static IGAsmOp make_op(int o, String p0, String p1, String p2)
	{ IGAsmOp obj = new IGAsmOp();
		obj.m_op = (o);
		obj.m_p0 = p0;
		obj.m_p1 = p1;
		obj.m_p2 = p2;
	return obj; }
	
	
	public static IGAsmOp make_op(int o, String p0, String p1, String p2, String p3, String p4, String p5)
	{ IGAsmOp obj = new IGAsmOp();
		obj.m_op = (o);
		obj.m_p0 = p0;
		obj.m_p1 = p1;
		obj.m_p2 = p2;
		obj.m_p3 = p3;
		obj.m_p4 = p4;
		obj.m_p5 = p5;
	return obj; }
	
	public static IGAsmOp make_op_comment(int o, String c)
	{ IGAsmOp obj = new IGAsmOp();
		obj.m_op = (o);
		obj.m_comment = c;
	return obj; }
	
	public static IGAsmOp make_op_comment(int o, String p0, String c) { 
		IGAsmOp obj = new IGAsmOp();
		obj.m_op = (o);
		obj.m_p0 = p0;
		obj.m_comment = c;
		return obj; 
	}
	
	public static IGAsmOp make_op_comment(int o, String p0, String p1, String c)
	{
	 	IGAsmOp obj = new IGAsmOp();
		obj.m_op = (o);
		obj.m_p0 = p0;	
		obj.m_p1 = p1;
		obj.m_comment = c;
		return obj; 
	}
	
	public static IGAsmOp make_op_comment(int o, String p0, String p1, String p2, String c)
	{ 
		IGAsmOp obj = new IGAsmOp();
		obj.m_op = (o);
		obj.m_p0 = p0;	
		obj.m_p1 = p1;
		obj.m_p2 = p2;
		obj.m_comment = c;
		return obj; 
	}
		
	
	public static IGAsmOp make_op_comment(String o, String c)
	{ IGAsmOp obj = new IGAsmOp();
		obj.m_op = convert(o);
		obj.m_comment = c;
	return obj; }
	
	public static IGAsmOp make_op_comment(String o, String p0, String c) { 
		IGAsmOp obj = new IGAsmOp();
		obj.m_op = convert(o);
		obj.m_p0 = p0;
		obj.m_comment = c;
		return obj; 
	}
	
	public static IGAsmOp make_op_comment(String o, String p0, String p1, String c)
	{
	 	IGAsmOp obj = new IGAsmOp();
		obj.m_op = convert(o);
		obj.m_p0 = p0;	
		obj.m_p1 = p1;
		obj.m_comment = c;
		return obj; 
	}
	
	public static IGAsmOp make_op_comment(String o, String p0, String p1, String p2, String c)
	{ 
		IGAsmOp obj = new IGAsmOp();
		obj.m_op = convert(o);
		obj.m_p0 = p0;	
		obj.m_p1 = p1;
		obj.m_p2 = p2;
		obj.m_comment = c;
		return obj; 
	}
	
	public static int convert(String s) {
		return IGAssembler.IGAssemblyUnitMethod.s_simple.get(s); 
	}

	public static String convertBack(int s) {
		return IGAssembler.IGAssemblyUnitMethod.s_simple_back.get(s); 
	}
	
	// left 4 dead => cursed 
}