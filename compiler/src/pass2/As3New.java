/*
Copyright (c) 2014-2017 Incubator Games Ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

package pass2;

import pass1.*;
import java.util.Vector;
import java.io.*;
import java.util.Hashtable;
import java.util.ArrayList;
public class As3New extends IGConsts
{

	public static Token expect(Token [] token_list, int token_idx, int token_type, String text)
	{
		Token stok = token_list[token_idx];
		if (stok.m_type != token_type)
		{
//			System.err.println();
			//System.err.println(stok.m_file + ":" + stok.m_line + "> " + text);
			stok.printError(text);
			System.exit(1);
		}
		return stok;
	}


	private static int token_idx = 0;
	private static Token [] token_list = null;

	public static Token peek(int offset) {
		offset ++;

		if (token_idx + offset >= token_list.length) return null;
		return token_list[token_idx + offset];
	}

	//public static boolean hasNode(Token t, int node_type)
	//{
	//	return t.m_node != null && t.m_node.m_node_type == node_type;
	//}


	/*
	 * Depth first traversal of all nodes
	 */
	
	private static void markup(TokenWriter pw, IGSource s, IGNode node)
	{
		if (markupPre(pw, s, node))
		{
			final IGNodeList children = node.m_children;
			int children_count = children.size();
			
			
			//for (IGNode child : node.m_children) {
			for (int i = 0; i < children_count; i++) {
				markup(pw, s, children.get(i));
			}
		}
		
		markup2(pw, s, node);
	}
	
	
	
	private static boolean markupPre(TokenWriter pw, IGSource s, IGNode node)
	{
		if (node == null || pw == null) {
			throw new RuntimeException("What gives?");
		}
		
		if (pass1.IGSettings.s_optimization_level > 0)
		{
			if (node.m_node_type == IGNode.NODE_UNARY ||
				node.m_node_type == IGNode.NODE_BINARY ||
				node.m_node_type == IGNode.NODE_DOT ||
				(node.m_node_type == IGNode.STATIC_FIELD_ACCESS && 
					node.m_parent.m_node_type != IGNode.NODE_BINARY &&
					node.m_parent.m_node_type != IGNode.NODE_ENUM_MEMBER)
				)
			{
			
				IGValueRange vr = node.m_value_range;
				if (vr != null && vr.isConcrete()) {
					if (vr.m_type == IGValueRange.TYPE_INT) {
						return false;
					}
					else if (vr.m_type == IGValueRange.TYPE_DOUBLE) {
						return false;
					}
				}
			}
		}
		
		
		
		//if (node.m_value != null) 
		{
			/*
			if (pass1.IGSettings.s_optimize)
			{
				IGOptimizerValue ov = node.m_value;
				if (ov.m_type == IGOptimizerValue.OTYPE_INT ) {
			
					pw.disableOutput(node);
	//				pw.prepend(node, ""+ ov.m_int);
					pw.prepend(node, "" + ov.m_int);
					return false;	
				}
			}
			*/
		}
		
		
		
		// okay this says go ahead and replace the children
		return true;
	}
	
	/*
	 * Marking up node
	 */
	 
	private static  String igTypeToAS3(Type type)
	{

		if (type == null) {
			throw new RuntimeException("What gives? How can this type be null?");
		}
		else if (type == Type.INT)	{    //get("int"))
			return "int";
		}
		else if (type == Type.UINT){	//Type.get("uint")){
			return "uint";
		}
		else if (type == Type.DOUBLE) { //Type.get("double")){
			return "Number";
		}
		else if (type == Type.BYTE) {
			return "int";
		}
		else if (type == Type.SHORT) {
			return "int";
		}
		else if (type == Type.BOOL) {   //get("bool")){
			return "Boolean";
		}
		else if (type.isEnum()) {
			IGSource source = (IGSource)type.m_scope;
			return igTypeToAS3(source.m_enum_extends);
		}
		else if (type.isFunctionPointer()) {
			return "Function";
		}

		else if (type.m_primary.startsWith("iglang.Map") && type.m_left.equals("iglang.String")) {
			return "Object";
		}
		else if (type.m_primary.startsWith("iglang.Map")) {
			// pretty sure this looks for explicit equality
			// if this isn't the case we should likely use a precanned one of ours that
			// will look for these attributes   X
			return "Dictionary";
		}
		else if (type.m_primary.startsWith("iglang.Vector")) {
		
			//Type child = type.m_children.get(0);
			
			// down the road indicate further type info
			return "Vector.<" + igTypeToAS3(type.m_left) + ">";
		}
		else if (type.m_primary.startsWith("iglang.Array")) {
			return "Vector.<" + igTypeToAS3(type.m_left) + ">";
		}
		else {
			String p = type.m_primary;
			int k = p.lastIndexOf(".");
			if (k == -1) {
				return p;
			}
			return p.substring(k + 1);
		}
			
	}
	
	private  static ArrayList<String> s_enum_to_string = new ArrayList<String>();
	
	private static void markup2(TokenWriter pw, IGSource s, IGNode node)
	{
		Token token = node.m_token;
		
		
		if (pass1.IGSettings.s_optimization_level > 0)
		{
			if (node.m_node_type == IGNode.NODE_UNARY ||
				node.m_node_type == IGNode.NODE_BINARY ||
				node.m_node_type == IGNode.NODE_DOT ||
				(node.m_node_type == IGNode.STATIC_FIELD_ACCESS && 
					node.m_parent.m_node_type != IGNode.NODE_BINARY &&
					node.m_parent.m_node_type != IGNode.NODE_ENUM_MEMBER)
			)
			{
				IGValueRange vr = node.m_value_range;
				if (vr != null && vr.isConcrete()) {
					if (vr.m_type == IGValueRange.TYPE_INT) {
						pw.replace(node, "" + vr.intValue());
						//pw.append(node,node.m_node_type + ">>" + node.m_parent.m_node_type + " */");
						return;
					}
					else if (vr.m_type == IGValueRange.TYPE_DOUBLE) {
						pw.replace(node, "" + vr.doubleValue());
						//pw.prepend(node, "" + vr.doubleValue() + "/*");
						//pw.append(node,"*/");
						return;
					}
				}
			}
		}
		
		
		if (node.m_node_type == IGNode.PACKAGE)
		{
			// destroy the trailing ';'
			pw.replace(token_list[node.m_end_index - 1], "");
			
			pw.append(node, "{ import flash.utils.Dictionary; import flash.utils.ByteArray; import flash.utils.Endian; import platform.*; import iglang.*; ");
		}
		else if (node.m_node_type == IGNode.IMPORT)
		{
			////////////
			// Check for an handle heirarchical imports
			//////////////
		
			// the semi colon is the very last symbol
			int last1 = node.m_end_index - 3;
			int last0 = node.m_end_index - 2;
			
			
			if (last1 > node.m_start_index) {
				if (token_list[last0].m_value.equals("*") &&
					token_list[last1].m_value.equals("*")) {
	
					// overwrite all tokens but the final ';' 
					for (int i = node.m_start_index; i < node.m_end_index - 1; i++) {
						pw.replace(token_list[i], "");
					}
					
					ArrayList<String> keys = new ArrayList<String>();
					for (int i = node.m_start_index + 1; i < last1; i++) {
						if (!token_list[i].m_value.equals(".")) {
							keys.add(token_list[i].m_value);
						}
					}
					
					StringBuilder import_list = new StringBuilder();
					for (IGSource possible_import : s_sources) {
						if ( possible_import.m_package.startsWith(keys)) 
						{
							import_list.append("import "  +  possible_import.m_package + "." + possible_import.m_expected_component + "; ");
						}
					}

					// replace the ';' with the updated import list
					pw.replace(token_list[node.m_end_index - 1], import_list.toString());
				}
				
			}
			
		}
		/*
		 * Casting an ENUM to a STRING
		 */
		else if (node.m_node_type == IGNode.NODE_TYPE_CAST)
		{	
			Type dst_type = node.m_type;
			Type src_type = node.m_children.get(1).m_type;
		
			if (dst_type == Type.STRING) 
			{
				if (src_type == Type.INT   ||
					src_type == Type.SHORT ||
					src_type == Type.BYTE  ||
					src_type == Type.DOUBLE ||
					src_type == Type.BOOL) {
					
					// do nothing.  Go on as before
				}
				else if (src_type.isEnum()) 
				{
					pw.replace(node.m_children.get(0), src_type + ".__ig_enumToString" );
				}	
			}
		}
		else if (node.m_node_type == IGNode.LOCAL_ACCESSOR)
		{
			//System.out.println("This used to be for local get");
			IGMember member = (IGMember)node.m_scope;
			//if (!member.hasModifier(IGMember.STATIC) &&
			if	(member.getSource().isEnum()) {
				
				///*" + node.m_type + " " + node.m_scope + "*/
				pw.append(node, "(this__ig_enum)");
			}
		}
		else if (node.m_node_type == IGNode.MEMBER_PREAMBLE)
		{
			int node_scope_type = node.m_scope.getScopeType();
			IGMember member = (IGMember) node.m_scope;
			
			if (node_scope_type == IGScopeItem.SCOPE_ITEM_MEMBER_FUNCTION) 
			{
				// for interfaces?
				if (!member.hasModifier(IGMember.PUBLIC)) 
				{
					if (node.m_start_index == node.m_end_index) {
						pw.prepend(node, "private ");
					}
				}
			}
			else  if (node_scope_type == IGScopeItem.SCOPE_ITEM_STATIC_FUNCTION) 
			{
				// static token will be a part of the preamble
				if (!member.hasModifier(IGMember.PUBLIC)) 
				{
					if (node.m_start_index == node.m_end_index - 1) {
						pw.prepend(node, "private ");
					}
				}
			}
		}
		else if (node.m_node_type == IGNode.MEMBER_PREAMBLE_TOKEN)
		{
			
			
			int node_scope_type = node.m_scope.getScopeType();
			
			if (token.m_type == TOK_NATIVE) 
			{
				pw.replace(node.m_token, "");
			}
			else if (node.m_token.m_value.equals("protected")) {
				// our notation of protected differs from AS3s
				pw.replace(node.m_token, "public");
			}
			else if (node_scope_type == IGScopeItem.SCOPE_ITEM_MEMBER_FUNCTION)
			{
				//if (node.m_token.m_value.equals("get")){
				//	System.out.println("found get");
				//	// if it's a getter just eliminate the get keyword for enums
				//	IGSource parent_source = ((IGMember)node.m_scope).getSource();
				//	if (parent_source.m_type == IGScopeItem.SCOPE_ITEM_ENUM) {
				//		pw.replace(node.m_token, "");
				//	}	
				//}
				
				IGMember member = (IGMember) node.m_scope;
				
					
				if (member.m_name.m_value.equals("<constructor>")) {
				
					// as3 requires all constructors to be public
					// so whatevs
					if (node.m_token.m_value.equals("internal")) {
						pw.replace(node.m_token, "public");
					}
					else if (node.m_token.m_value.equals("private")) {
						pw.replace(node.m_token, "public");					
					}
				}
				else if (node.m_token.m_value.equals("override")) 
				{
					
					if (member.m_name.m_value.equals("toString")) {
					
					
						//System.out.println("found possible override");
					
					
					
						IGSource ext   = member.getSource().getExtendsSource();
						//System.out.println("" + ext + " " + member.getSourceNameAsString() );
						if (ext != null)
						{
							IGMember other_to_string = ext.getFirstToString();
							//System.out.println("" + other_to_string.getSource().toString());
							if (other_to_string.getSource().toString().equals("iglang.Object"))
							{
								// should we really do this in all cases?
								pw.replace(node.m_token, "");
							}
						}
						else {
							pw.replace(node.m_token, "");
						}
					}
				}	
				
			}
		}
		else if (node.m_node_type == IGNode.MEMBER_NAME)
		{
			int node_scope_type = node.m_scope.getScopeType();
		
		
			if (node_scope_type == IGScopeItem.SCOPE_ITEM_MEMBER_FUNCTION) 
			{
				IGSource parent_source = ((IGMember)node.m_scope).m_parent;
				if (parent_source.m_type == IGScopeItem.SCOPE_ITEM_ENUM) 
				{
					pw.prepend(node, " static ");
					
					
					
					
					Token t = token_list[node.m_start_index + 1];
					
					//System.out.println("HUH? " + node.m_token.m_value + " " + t);
					
					// get's just dissapear in enums
					if (t.m_value.equals("get")) {
						pw.replace(t, "");
					}
				}
			}
			
			// TODO blow away the override modifier on toString in AS3
			//if (node.m_scope.getScopeType() == IGScopeItem.SCOPE_ITEM_MEMBER_FUNCTION) {
				//Token t = token_list.get(node.m_end_index - 1);
			//	if (t.m_value.equals("toString")) {
				//	
				//}
			//}
			
			if (node_scope_type == IGScopeItem.SCOPE_ITEM_MEMBER_FUNCTION ||
			    node_scope_type == IGScopeItem.SCOPE_ITEM_STATIC_FUNCTION) {
			 
			 	Token t = token_list[node.m_end_index - 1];
			 	
			 	if (t.m_value.endsWith("__get") || 
					t.m_value.endsWith("__set")) 
				{
					pw.replace(t, t.m_value.substring(0, t.m_value.length() - 5)); 
				}   
				else if (t.m_value.equals("<constructor>")) {
					pw.replace(t, m_current_source.m_expected_component);
				}
			}
		}
		else if (node.m_node_type == IGNode.FUNCTION_PARAMETERS)
		{
			if (node.m_scope.getScopeType() == IGScopeItem.SCOPE_ITEM_MEMBER_FUNCTION) {
				IGSource parent_source = ((IGMember)node.m_scope).m_parent;
				if (parent_source.m_type == IGScopeItem.SCOPE_ITEM_ENUM) {
					String enum_type = igTypeToAS3(parent_source.m_enum_extends);
					
					
					pw.prepend(node, " this__ig_enum : " + enum_type + " " + (node.isEmpty() ? "" : ","));
				}
			}
		}
		else if (node.m_node_type == IGNode.NODE_ENUM_MEMBER) 
		{
			pw.prepend(token, "public static const ");
			
			IGMember   enum_member = ((IGMember)node.m_scope);
			IGSource parent_source = enum_member.m_parent;
			String enum_type = igTypeToAS3(parent_source.m_enum_extends);
			
			pw.append(token, " : " + enum_type);  // NOTE THIS SHOULD BE WHERE WE LOOK AT THE PRIMITIVE TYPE
			
			
			if (node.m_children.get(0).m_node_type == IGNode.STATIC_FIELD_ACCESS) 
			{
				pw.append(node.m_children.get(0), " = " + enum_member.m_return_value_range.toAS3String());
			}
	
			s_enum_to_string.add(enum_member.m_name.m_value);
	
			//__ig_enumToString
	
			/// HACK ALLERT
			if (token_list[node.m_end_index].m_type == TOK_COMMA) {
				pw.replace(token_list[node.m_end_index], ";");
			}
			else 
			{
				StringBuilder sb = new StringBuilder();
				sb.append("\tpublic static function __ig_enumToString(x : " + enum_type + ") : String { /* AUTO GENERATED */\n");
				sb.append("\t\tswitch(x) {\n");
				for (int i = 0; i < s_enum_to_string.size(); i++) {
					String key = s_enum_to_string.get(i);
					sb.append("\t\t\tcase (" + key + "): return \"" + key + "\";\n"); 
				}
				
				// this is the last enum member
				sb.append("\t\t}\n\t\treturn String(x);\n\t}\n");
			
				// this should really be placed at the end of the file
				pw.prepend(token_list[token_list.length-1], sb.toString());
			}
		}
		else if (node.m_node_type == IGNode.VECTOR_INITIALIZER)
		{
			// = new Vector<String>()[  dfdfsf ]
			IGNode left = node.getChild(0);
			pw.replace(left, igTypeToAS3(left.m_type) + "(");
			pw.append(node.getChild(1), ")");
		
		}
		else if (node.m_node_type == IGNode.ARRAY_INITIALIZER)
		{
			// = new Array<String>(3)[  dfdfsf ]
			IGNode left = node.getChild(0);
			pw.replace(left, igTypeToAS3(left.m_type) + "(");
			pw.append(node.getChild(1), ")");
		
		}
		else if ((node.m_node_type ==  IGNode.NODE_NEW))	//token.m_type == TOK_NEW)
		{
			// TODO revamp this ... see http://help.adobe.com/en_US/FlashPlatform/reference/actionscript/3/Vector.html
			// it has the crazy syntax of "new <int>[1,2,3,4]"  jesus these guys give a flying fuck about consistency
		
			if (node == null || node.m_children.size() != 2) {
				token.printError("New is incomplete: " + node);
			}

			IGNode type 	 = node.getChild(0);
			IGNode func_call = node.getChild(1);

			if (type.m_token.m_value.equals("ByteArray")) {
				pw.replace(type, "");
				pw.replace(node.m_token, "IGAS3.newByteArray");
			}
			/*
			else if (type.m_token.m_value.equals("Vector")) {
				pw.replace(type, "");
				pw.replace(node.m_token, "IGAS3.newVectorDouble");
			}
			else if (type.m_token.m_value.equals("Array")) {
				pw.replace(type, "");
				pw.replace(node.m_token, "IGAS3.newArrayDouble");
			}
			*/
			else if (node.m_parent.m_node_type == IGNode.MAP_INITIALIZER)
			{
				//func_call.m_token.m_whitespace += "/*C*/";
				pw.prepend(node, "/*");
				pw.append(node, "*/");
			}
			else if (node.m_parent.m_node_type == IGNode.ARRAY_INITIALIZER)
			{
				//System.out.println
				if (type.m_token.m_value.equals("Array")) {
					pw.append(func_call.m_end_index - 2, "/* this should have it's size fixed*/");
				}
			}
			else if (node.m_parent.m_node_type == IGNode.VECTOR_INITIALIZER)
			{
				
			}

			else {
				// TODO make fixed size
				// TODO if this is an array of doubles it NEEDS TO BE FILLED WITH 0s
				if (type.m_token.m_value.equals("Array")) {
					pw.append(func_call.m_end_index - 2, ",true");
				}
			}
		}
		else if (node.m_node_type == IGNode.MAP_INITIALIZER_LIST)
		{
			if (!node.m_type.m_primary.equals("iglang.String"))
			{
				pw.replaceFirstToken(node, "IGAS3.createMapDictionaryFromArray([");
				pw.replaceLastToken(node, "])");
				
				int len = node.m_children.size();
				for (int i = 0; i < len; i++) {
					IGNode n2 = node.m_children.get(i).m_children.get(1);
					pw.replace(n2.m_token, ",");
				}
			}
			else
			{
				pw.replaceFirstToken(node, "{");
				pw.replaceLastToken(node, "}");
			}
		}
		else if (node.m_node_type == IGNode.SWITCH_CASE)
		{
			//pw.replate(node.m_token, "case
			pw.prepend(node.getChild(1), ":");
			pw.append(node.getChild(1),  "break; ");
		}
		else if (node.m_node_type == IGNode.SWITCH_DEFAULT)
		{
			pw.prepend(node.getChild(0), ":");
			pw.append(node.getChild(0),  "break; ");
		}
		else if (node.m_node_type == IGNode.NODE_TYPE_TYPE)
		{
			//node.debug();
		
			// foreaches on Keys that aren't strings of dictionaries
			//ID_TYPE_PAIR              = 4 (first parent)
			//NODE_TYPE_LOCAL_VARIABLE_DEFINITION = 3 (second parent)
			// NODE_TYPE_FOREACH                  = 13 (third parent)
			
			String as3_type = igTypeToAS3(node.m_type);
			
			
			if (node.m_parent.m_parent.m_parent.m_node_type == IGNode.NODE_TYPE_FOREACH)
			{
				IGNode for_each_root      = node.m_parent.m_parent.m_parent;
				IGNode for_each_container = for_each_root.m_children.get(1);
				
				if ( for_each_container.m_type.m_primary.equals("iglang.Map") &&
					!for_each_container.m_type.m_left.equals("iglang.String"))
				{
					pw.replace(node, "* /*" + as3_type + "*/");
				}
				else
				{
					pw.replace(node, as3_type);
				}
			}
			else
			{
				pw.replace(node, as3_type);
			}

		}
		else if (node.m_node_type == IGNode.LOCAL_VARIABLE_ACCESS ||
				 node.m_node_type == IGNode.NODE_TYPE_LOCAL_VARIABLE_NAME)
		{

			if (!(token.m_node.m_scope instanceof IGVariable)) {
				token.printError("Why isn't this a variable? " + token.m_node.m_node_type);
			}

			IGVariable v = (IGVariable) token.m_node.m_scope;

			// just found out I had to fully guarantee against ANY
			// hoisting ever.  This can get messed up if a local in one instance overrides
			// a member variable.
			if (//v.m_occurrance_count > 0 && 
				!token.m_value.equals("this") && !token.m_value.equals("super"))
			{
				pw.append(token,//"__ig_" 
						"$" + v.m_occurrance_count);
			}
			
			if (m_current_source.m_type == IGScopeItem.SCOPE_ITEM_ENUM) {
				if (token.m_value.equals("this") || token.m_value.equals("super")) {
					pw.append(token, "__ig_enum");
				}
			}
		}
		else if (node.m_node_type == IGNode.NODE_TYPE_STATIC_VARIABLE_NAME ||
		         node.m_node_type == IGNode.NODE_TYPE_VARIABLE_NAME)
		{
			// check if this has been assigned  a value yet
			// THIS IS EQUALLY STUPID
			if (node.m_parent.m_parent.m_node_type != IGNode.NODE_BINARY) 
			{			
				Type type = node.m_type;
				if (type == Type.INT || type == Type.get("uint") || type == Type.DOUBLE)
				{
					pw.append(node.m_parent, " = 0");
				}
				else if (type == Type.get("bool")) {
					pw.append(node.m_parent, " = false");
				}
				else if (type.isEnum()) {
					pw.append(node.m_parent, " = 0");
				}
				else {
					pw.append(node.m_parent, " = null");
				}
			}
			
		}
		else if (node.m_node_type == IGNode.NODE_TYPE_LOCAL_VARIABLE_DEFINITION)
		{
			IGVariable variable = (IGVariable) token.m_node.m_scope;
			if (!variable.m_AS3_has_initial_value)
			{
				Type type = variable.m_type;
				if (type == Type.INT || type == Type.get("uint") || type == Type.DOUBLE)
				{
					pw.append(token.m_node, " = 0");
				}
				else if (type == Type.get("bool")) {
					pw.append(token.m_node,  " = false");
				}
				else if (type.isEnum()) {
					pw.append(token.m_node, " = 0");
				}
				else {
					pw.append(token.m_node,  " = null");
				}
			}
		}
		else if (node.m_node_type == IGNode.SOURCE_NAME)
		{
			if (token.m_type == TOK_ENUM) {
				pw.replace(token, "class");
			}
		}	
		else if (node.m_node_type == IGNode.EXTENDS) {
			// void out its extends type
			if (m_current_source.m_type == IGScopeItem.SCOPE_ITEM_ENUM) {
				pw.disableOutput(node);
			}
		}
		else if (node.m_node_type == IGNode.NODE_VALUE && token.m_type == IGLexer.LEXER_STRING) 
		{
			pw.append(token, "\"");
			pw.prepend(token, "\"");
		}
		else if (node.m_node_type == IGNode.NODE_BINARY && token.m_type == TOK_DIV) {
			if (node.m_type == Type.get("int") || node.m_type == Type.get("uint")) {
				pw.prepend(node, node.m_type.m_primary + "(");
				pw.append(node, ")");
			}
		}
		else if (node.m_node_type == IGNode.NODE_BINARY && token.m_type == TOK_EQ) {
			// convert all equality checks into strict
			pw.append(token, "=");
		}
		else if (node.m_node_type == IGNode.NODE_BINARY && token.m_type == TOK_NE) {
			// convert all equality checks into strict
			pw.append(token, "=");
		}
		else if (node.m_node_type == IGNode.NODE_BINARY && token.m_type == TOK_LSR) {
			pw.replace(token, ">>>");
		}
		else if (node.m_node_type == IGNode.NODE_BINARY && token.m_type == TOK_ASR) {
			pw.replace(token, ">>");
		}
		else if (node.m_node_type == IGNode.NODE_BINARY && token.m_type == TOK_LSL) {
			pw.replace(token, "<<");
		}
		else if (node.m_node_type == IGNode.NODE_VALUE && token.m_type == IGLexer.LEXER_CHAR) 
		{
			String value = token.m_value;
			
			// the value range should contain the exact ascii code
			pw.replace(token, "/*" + value + "*/" + node.m_value_range.getIntValue());	//((int)value.charAt(0)));
		}
		else if ((node.m_node_type == IGNode.NODE_TYPE_FOREACH))
		{
			IGNode left 	= token.m_node.getChild(0);
			IGNode right_in = token.m_node.getChild(1);

			if (right_in.m_type.m_primary.startsWith("iglang.Vector") ||
				right_in.m_type.m_primary.startsWith("iglang.Array"))
			{
				pw.append(token, " each");
			}
			else if (right_in.m_type.m_primary.startsWith("iglang.Map")) {
			
			}
			else {
			
				
			
				// 3  => NODE_TYPE_LOCAL_VARIABLE_DEFINITION
				// 14 => NODE_TYPE_FOREACH_IN
				//		163 => FOREACH_IN_RANGE
				
				
				if (right_in.m_node_type             == IGNode.NODE_TYPE_FOREACH_IN &&
				    right_in.getChild(1).m_node_type == IGNode.FOREACH_IN_RANGE) 
				{
				 
				 	String unique_id = token.getUniqueVariableId();
				 	
				 	// eg.
				 	// for ( XXX in YYY => ZZZ) {
				 
				 	// kill the 'for' and the first '('
					pw.replace(token, "");
					pw.replaceAtOffset(token, 1, "");
				 
				 	// replace the 'in' with an assignment
				 	pw.replace(right_in.m_token, "=");
				 	// replace the '=>' with  '; var MAX : int = ('
				 	
				 	// 'var' used to be 'const' then MXMLC flipped its shit.
				 	// This might have been that it just doesn't like constants in catch statements
				 	pw.replace(right_in.getChild(1).m_token, "; var " + unique_id + " : int = (");
				 	
				 	
				 	IGNode iterator_name = left;
				 	if (left.m_node_type == IGNode.NODE_TYPE_LOCAL_VARIABLE_DEFINITION) {
				 		iterator_name = left.getChild(0).getChild(0);
				 	}
				 	
				 	StringBuilder vname_prep = new StringBuilder();
				 	pw.printRangeNoWhitespace(token_list, iterator_name.m_start_index, iterator_name.m_end_index, vname_prep);
				 	String vname = vname_prep.toString();
				 	
				 	pw.prepend(node.getChild(2).m_token, "; for(;" + vname + "<" + unique_id + ";" +
				 		vname + "++)");
				}
				else
				{		
					throw new RuntimeException("Unsupported for each. " + left.m_node_type + " " + 
						right_in.m_node_type);
				}
			}
		}
	
		else if (node.m_node_type == IGNode.FOREACH_IN_RANGE)
		{
			// min '=>' max
			
			//IGNode min = node.getChild(0);
			IGNode max = node.getChild(0);
		
		}
		
		else if (node.m_node_type ==  IGNode.NATIVE_BLOCK) {
			if (token.m_value.startsWith("@as3")) {
				pw.replace(token, token.m_value.substring(4));
			}
			else
			{
				pw.disableOutput(token);
			}
		}
		else if (node.m_node_type == IGNode.NODE_DOT)
		{
			IGNode left = node.getChild(0);
			IGNode right = node.getChild(1);
		
			// apply the augmented fields to the math class
			if (left.m_type == Type.get("source:iglang.Math") && left.m_node_type == IGNode.SCOPE_CHAIN_START)
			{
				if (right.m_type == Type.DOUBLE && right.m_token.m_value.equals("DEG_TO_RAD")) {
				
					pw.replace(node, "(Math.PI/180.0)");
				}
				else if (right.m_type == Type.DOUBLE && right.m_token.m_value.equals("RAD_TO_DEG")) {
					pw.replace(node, "(180.0/Math.PI)");
				}
			}
			// remap
			else if (left.m_type == Type.get("source:iglang.Double") && left.m_node_type == IGNode.SCOPE_CHAIN_START)
			{
				if (right.m_type == Type.DOUBLE && right.m_token.m_value.equals("POSITIVE_INFINITY")) {
					pw.replace(node, "Number.POSITIVE_INFINITY");
				}
				else if (right.m_type == Type.DOUBLE && right.m_token.m_value.equals("NEGATIVE_INFINITY")) {
					pw.replace(node, "Number.NEGATIVE_INFINITY");
				}
			}
			
		}
		else if ((node.m_node_type == IGNode.DOT_ACCESSOR || node.m_node_type == IGNode.STATIC_DOT_ACCESSOR) && 
			!node.isLValue())
		{
			//  a.b
			IGNode a = node.getChild(0);
			IGNode b = node.getChild(1);
		
			IGNode left = a;
			IGNode right = b;
		
			if (a.m_type.isEnum())
			{
				//pw.append(a, "/*X2*/");
				
				pw.prepend(node, a.m_type + "." + b.m_token.m_value + "(");
				pw.replace(node.m_token, ")");
				pw.disableOutput(b);
				//pw.replace(
					
				// this needs to be addressed
				//pw.disableOutput(left.m_token.m_token_index);					// .
				//pw.disableOutput(left.m_token.m_token_index + 1);	// containsKey
				//pw.disableOutput(left.m_token.m_token_index + 2);	// (
				//pw.append(left.m_token.m_token_index, right.isEmpty() ? "" : ",");
			}
			else if (left.m_type.m_primary.equals("iglang.Map") && right.m_token.m_value.equals("length"))
			{
				pw.prepend(node, "IGAS3.objectLength(");
				pw.replace(node.m_token, "");
				pw.replace(right, ")");
			}
		}
		else if ((node.m_node_type ==  IGNode.NODE_CALL))
		{
			// the thing being called
			IGNode left = node.getChild(0);
			// the parameters
			IGNode right = node.getChild(1);

			if (left.m_node_type == IGNode.NODE_DOT)
			{
				/// left = a . b
				///  node  = left ( right

				IGNode a = left.getChild(0);
				IGNode b = left.getChild(1);


				if (a.m_type.isEnum() && 
					left.m_scope.getScopeType() == IGScopeItem.SCOPE_ITEM_MEMBER_FUNCTION) // was node
				{
					pw.prepend(node, a.m_type + "." + b.m_token.m_value + "(");
					
					// this needs to be addressed
					pw.disableOutput(left.m_token.m_token_index);					// .
					pw.disableOutput(left.m_token.m_token_index + 1);	// containsKey
					pw.disableOutput(left.m_token.m_token_index + 2);	// (
					pw.append(left.m_token.m_token_index, right.isEmpty() ? "" : ",");
				}
				else if (b.m_token.m_value.equals("<constructor>")) {
					pw.replace(left, "super");
				}
				else if (a.m_type == Type.STRING && b.m_token.m_value.equals("compareTo")) 
				{
					pw.replace(b.m_token, "localeCompare");
				}
		
				else if (a.m_type == Type.STRING && b.m_token.m_value.equals("startsWith"))
				{
					pw.prepend(node, "iglang.IGAS3.stringStartsWith(");
					
					// this needs to be addressed
					pw.disableOutputTokens(left.m_token.m_token_index, 3);	// [.] [startsWith]  [(]
					pw.append(left.m_token.m_token_index, ",");
				}
				else if (a.m_type == Type.STRING && b.m_token.m_value.equals("endsWith"))
				{
					pw.prepend(node, "iglang.IGAS3.stringEndsWith(");
					
					// this needs to be addressed
					pw.disableOutputTokens(left.m_token.m_token_index, 3);	// [.] [endsWith]  [(]
					pw.append(left.m_token.m_token_index, ",");
				}
				
				else if (b.m_token.m_value.equals("containsKey") &&
					     a.m_type.m_primary.equals("iglang.Map")
				)
				{
					pw.prepend(node, "iglang.IGAS3.containsKey(");
					
					// this needs to be addressed
					pw.disableOutputTokens(left.m_token.m_token_index, 3);	// [.] [containsKey]  [(]
					pw.append(left.m_token.m_token_index, ",");
				}
				else if (b.m_token.m_value.equals("getWithDefault") &&
					     a.m_type.m_primary.equals("iglang.Map")
				)
				{
					// July 25, 2016
					// think this checks out
					pw.prepend(node, "iglang.IGAS3.mapGetWithDefault(");
					
					// this needs to be addressed
					pw.disableOutputTokens(left.m_token.m_token_index, 3);	// [.] [getWithDefault]  [(]
					pw.append(left.m_token.m_token_index, ",");
				}

				else if (b.m_token.m_value.equals("fill") &&
					     ( a.m_type.m_primary.equals("iglang.Vector") ||
					       a.m_type.m_primary.equals("iglang.Array"))
				)
				{
					pw.prepend(node, "iglang.IGAS3.vectorFill(");
					
					// this needs to be addressed
					pw.disableOutput(left.m_token.m_token_index);					// .
					pw.disableOutput(left.m_token.m_token_index + 1);	// vectorFill
					pw.disableOutput(left.m_token.m_token_index + 2);	// (
					pw.append(left.m_token.m_token_index, ",");
				}
				else if (b.m_token.m_value.equals("copy") &&
					     ( a.m_type.m_primary.equals("iglang.Vector") ||
					       a.m_type.m_primary.equals("iglang.Array"))
				)
				{
					pw.prepend(node, "iglang.IGAS3.vectorCopy(");
					
					// this needs to be addressed
					pw.disableOutput(left.m_token.m_token_index);					// .
					pw.disableOutput(left.m_token.m_token_index + 1);	// vectorCopy
					pw.disableOutput(left.m_token.m_token_index + 2);	// (
					pw.append(left.m_token.m_token_index, ",");
				}
				else if (b.m_token.m_value.equals("deleteKey") &&
						 a.m_type.m_primary.equals("iglang.Map")
				)
				{
					pw.prepend(node, "iglang.IGAS3.deleteKey(");
					
	
					// this needs to be addressed
					pw.disableOutput(left.m_token.m_token_index);					// .
					pw.disableOutput(left.m_token.m_token_index + 1);	// deleteKey
					pw.disableOutput(left.m_token.m_token_index + 2);	// (
					pw.append(left.m_token.m_token_index, ",");
				}
				////////////////////////////////////////////////
				// Vector Additions
				////////////////////////////////////////////////
				else if (b.m_token.m_value.equals("__delete") &&
						 a.m_type.m_primary.equals("iglang.Vector")) {

					pw.disableOutput(left.m_token.m_token_index + 1);
					pw.append(left.m_token.m_token_index + 1, "splice");

					// give the delete a different default value
					if (right.m_children.size() == 1)
					{
						IGNode first_parameter = right.getChild(0);
						pw.append(first_parameter.m_end_index - 1, ",1");
					}
				}
				else if (b.m_token.m_value.equals("insert") &&
						 a.m_type.m_primary.equals("iglang.Vector")) {

					pw.disableOutput(left.m_token.m_token_index + 1);
					pw.append(left.m_token.m_token_index + 1, "splice");

					IGNode second_parameter = right.getChild(0);
					pw.append(second_parameter, ",0");
				}
				else if (b.m_token.m_value.equals("contains") &&
						 a.m_type.m_primary.equals("iglang.Vector")) {

					pw.prepend(node, "(");
					pw.disableOutput(left.m_token.m_token_index + 1);
					pw.append(left.m_token.m_token_index + 1, "indexOf");
					pw.append(node,  "!= -1)");
				}
				else if (b.m_token.m_value.equals("contains") &&
						 a.m_type.m_primary.equals("iglang.Array")) {

					pw.prepend(node, "(");
					pw.disableOutput(left.m_token.m_token_index + 1);
					pw.append(left.m_token.m_token_index + 1, "indexOf");
					pw.append(node,  "!= -1)");
				}
				///////////////////////////////////////////////////////
				// String additions
				////////////////////////////////////////////////
				else if (b.m_token.m_value.equals("split") &&
						 a.m_type.m_primary.equals("iglang.String")) {

					pw.prepend(node, "Vector.<String>(");
					pw.append(node, ")");
				}
				else if (b.m_token.m_value.equals("trim") &&
						 a.m_type.m_primary.equals("iglang.String"))
				{
					pw.prepend(node, "iglang.IGAS3.trim(");
					
					// TODO verify that this is correct
					// this needs to be addressed
					pw.disableOutput(left.m_token.m_token_index);					// .
					pw.disableOutput(left.m_token.m_token_index + 1);	// deleteKey
					pw.disableOutput(left.m_token.m_token_index + 2);	// (
					//pw.append(left.m_token.m_token_index, ",");
				}
			
			}
		}
	}
	
	private static Vector<IGSource> s_sources = null;
	private static IGSource m_current_source = null;

	public static void run(IGPackage pkg, Vector<IGSource> sources)
	{
		s_sources = sources;
		// create the header
		for (IGSource source : sources)
		{
			if (IGSettings.s_verbose) {
				System.out.println("as3-output: " + source.m_src_path);
			}
			
		 	//Hashtable<Integer,String> to_append = new Hashtable<Integer, String>();
			token_list = source.m_token_list;
			token_idx = 0;
			
			
			
			TokenWriter pw = null;
			try
			{
				m_current_source = source;
				
				
				
				s_enum_to_string = new ArrayList<String>();
				pw = Util.createOutputPrintWriter(source, ".as");
				
				markup(pw, source, source.m_root_node);

				pw.printRange(source.m_token_list, 
					source.m_root_node.m_start_index, 
					source.m_root_node.m_end_index);		
				
				pw.println("");
				pw.println("}");
				pw.close();
			}
			catch (Exception ex)
			{
				System.err.println("error in: " + source.m_expected_component);
				ex.printStackTrace();
				System.exit(1);
			}
			finally
			{
				pw.close();
			}			
		}
	}
}
