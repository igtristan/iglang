/*
Copyright (c) 2014-2017 Incubator Games Ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

package pass2;

import util.*;
import pass1.*;

import java.io.*;

import java.util.Vector;

public class VM
{

	public static void rundown(TokenWriter pw, IGNode node, IGSource s) throws IOException
	{
		if (node == null ) {
			pw.println("\t" +" null node");
			return;
		}
		pw.println("\t" + node.m_node_type + " " + ((node.m_token != null) ? node.m_token.m_value : "null"));
		
		
		final IGNodeList children = node.m_children;
		int children_count = children.size();
		
		//for (IGNode child : node.m_children) {
		for (int i = 0; i < children_count; i++) {
			rundown(pw, children.get(i), s);
		}
	}

	public static void run(IGPackage root, Vector<IGSource> sources)
	{	
		// create the header
		
		try
		{
			//createPackageHeaders(root);
			
			//////////////////////////////////////////////////
			// Create header files
			//////////////////////////////////////////////////
			
			for (IGSource s : sources)
			{
				TokenWriter pw = Util.createOutputPrintWriter(s, ".asm");
				pw.println(s.m_expected_component);
				
				for (IGMember m : s.m_members)
				{
					pw.println("IGMember: " + m.m_name.m_value);
					
					if (m.m_type == IGMember.TYPE_FUNCTION)
					{
						IGNode node = m.m_node;
						
						rundown(pw, node , s);
					
					
					}
				}
				
				pw.println("HUH?");
				pw.close();
			}
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
	}
}