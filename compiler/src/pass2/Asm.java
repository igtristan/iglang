/*
Copyright (c) 2014-2017 Incubator Games Ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

package pass2;

/**
 * Todo.  Figure out how we're going to handle null between function pointers and object
 */


import pass2.asm.*;

/*
	I'm probably injecting super somewhere

	TODO
		- calling super.method()
		- calling super in constructor
		- how exactly are we going to deal with generics
		- is it just something that gets executed on load .. ie we ask for
			* Vector<Map>  which gives us a Vector<Object>
			* Vector<bool> which gives us a Vector<int>
			* I'm guessing this is more a compile thing
				Vector<Object,int,double,bool,Function>
				Maps ... require most permutations
				Array<Object, ... etc
		- handling enums?
		
	Java temp directory
		System.out.println(System.getProperty("java.io.tmpdir"));

 */

import util.*;
import pass1.*;

import java.util.*;
import java.io.*;



public final class Asm extends Processor
{
	public static final boolean INCREMENT_OPTIMIZATION = true;
	private  int s_current_protection = 0;
	
	private  void newHeaderSource()
	{
		s_current_protection = 0;
	}
	

	private ArrayList<String> m_string_list            = new ArrayList<String>();
	private StringIntHashtable m_string_map = new StringIntHashtable();
	
	private int getStringIndex(String s) {
		if (!m_string_map.containsKey(s)) {
			m_string_map.put(s, m_string_list.size());
			m_string_list.add(s);
		}
		
		return m_string_map.get(s);
	}
	
	
	
	//////////////////////
	// Above this is most likely old relic code
	
	private int m_label_id = 0;
	
	
	private final void resetLabelCounter() {
		m_label_id = 0;
	}
	
	/**
	 * Allocate a label to use.  Is guaranteed to be unique within the file
	 */
	
	private final String getLabel() {
		String lbl =  "L" + m_label_id;
		m_label_id ++;
		return lbl;
	}
	
	private final String getLabel(String pre) {
		String lbl =  pre + m_label_id;
		m_label_id ++;
		return lbl;
	}
	
	private final String helperTrace(IGNode n) {
		String t = "";
		if (n.m_children.size() > 0) {
			t += helperTrace(n.m_children.get(0));
		}
		
		t += " " + n.m_node_type + "'" + n.m_token.m_value + "'";
		
		if (n.m_children.size() > 1) {
			t += helperTrace(n.m_children.get(1));
		}
		
		return t;
	}
	
	
	/*
	 * Cast a node to a particular dst_type.
	 */ 
	
	
	private final void outputFunctionWrapperCast(IGMember m, IGNode right, Type z_scope_type, IGAsmDocument pw) throws IOException
	{
		while (right.m_node_type == IGNode.NODE_TYPE_BRACKETED_EXPRESSION) {
			right = right.m_children.get(0);
		}

		// not sure how I confused this
		// scopes on types are only valid for IGSource references, not for IGMember references
		
		IGMember fn = (IGMember)right.m_scope;
		int scope_type = fn.getScopeType();
		
		if (IGScopeItem.SCOPE_ITEM_STATIC_FUNCTION == scope_type)
		{
			pw.addFunctionDependency(fn.getSourceNameAsString(), fn.getMemberNameAsString());
			pw.op(IGAsm.funcptr_static, fn.getSourceNameAsString(), fn.getMemberNameAsString());
		}
		else if (IGScopeItem.SCOPE_ITEM_MEMBER_FUNCTION == scope_type) 
		{
			if (right.m_node_type == IGNode.NODE_DOT)
			{
				outputFunctionBody(m, right.m_children.get(0), pw);
				
				pw.addFunctionDependency(fn.getSourceNameAsString(), fn.getMemberNameAsString());
				pw.op(IGAsm.funcptr_member,
					fn.getSourceNameAsString(), fn.getMemberNameAsString());			
			}
			else if (right.m_node_type == IGNode.FIELD_ACCESS)
			{
				pw.op(IGAsm.local_load,"0");

				pw.addFunctionDependency(fn.getSourceNameAsString(), fn.getMemberNameAsString());
				pw.op(IGAsm.funcptr_member, fn.getSourceNameAsString(), fn.getMemberNameAsString());	
			}
			else
			{
				right.m_token.printError("Unsupported case");
				throw new RuntimeException("Unsupported case!");
			}
			// case 1     fnptr = local_function
			// case 2     fnptr = (some object expression).local_function
			// could also be bracketed in both cases I suppose = ((((expression.local_function)))))
		
			//if (right.m_children > 1) {
			//	outputFunctionBody(m, right.m_children.get(0), pw);
			//}
		}
		else {
			throw new RuntimeException("How did this get compiled?");
		}	
	}
	
	
	private final void outputCast(IGMember m, IGNode right, Type dst_type, IGAsmDocument pw) throws IOException
	{
		Type right_type = right.m_type;
		
		if (right.m_type.isFunctionWrapper())
		{
			while (right.m_node_type == IGNode.NODE_TYPE_BRACKETED_EXPRESSION) {
				right = right.m_children.get(0);
			}
		
		
			// not sure how I confused this
			// scopes on types are only valid for IGSource references, not for IGMember references
			
			IGMember fn = (IGMember)right.m_scope;
			int scope_type = fn.getScopeType();
			
			if (IGScopeItem.SCOPE_ITEM_STATIC_FUNCTION == scope_type)
			{
				pw.addFunctionDependency(fn.getSourceNameAsString(), fn.getMemberNameAsString());
				pw.op(IGAsm.funcptr_static, fn.getSourceNameAsString(), fn.getMemberNameAsString());			
			}
			else if (IGScopeItem.SCOPE_ITEM_MEMBER_FUNCTION == scope_type) 
			{
				if (right.m_node_type == IGNode.NODE_DOT)
				{
					outputFunctionBody(m, right.m_children.get(0), pw);
					
					pw.addFunctionDependency(fn.getSourceNameAsString(), fn.getMemberNameAsString());
					pw.op(IGAsm.funcptr_member, fn.getSourceNameAsString(), fn.getMemberNameAsString());								
				}
				else if (right.m_node_type == IGNode.FIELD_ACCESS)
				{
					pw.op(IGAsm.local_load,"0");

					pw.addFunctionDependency(fn.getSourceNameAsString(), fn.getMemberNameAsString());
					pw.op(IGAsm.funcptr_member, fn.getSourceNameAsString(), fn.getMemberNameAsString());				
				}
				else
				{
					right.m_token.printError("Unsupported case");
					throw new RuntimeException("Unsupported case!");
				}
				// case 1     fnptr = local_function
				// case 2     fnptr = (some object expression).local_function
				// could also be bracketed in both cases I suppose = ((((expression.local_function)))))
			
				//if (right.m_children > 1) {
				//	outputFunctionBody(m, right.m_children.get(0), pw);
				//}
			}
			else {
				throw new RuntimeException("How did this get compiled?");
			}						
		}
		else
		{

			outputFunctionBody(m, right, pw);
			castTo(right_type, dst_type, pw);		
		}	
	}
	
	
	
	/**
	 * output the assembly necessary to cast one type to another
	 *  e.g. double to int
	 *  e.g. Object to String
	 */
	
	private final void castTo(Type src_type, Type dst_type, IGAsmDocument pw) throws IOException
	{
		castTo(src_type, dst_type, pw, false);
	}
	
	private final void castTo(Type src_type, Type dst_type, IGAsmDocument pw, boolean explicit) throws IOException
	{
		// save the original values for debugging purposes
		final Type ps = src_type;
		final Type pd = dst_type;
		
		
		src_type = m_ctx.resolveType(src_type);
		dst_type = m_ctx.resolveType(dst_type);
							 
		if (src_type == Type.SHORT || src_type == Type.BYTE) { src_type = Type.INT; }
		if (dst_type == Type.SHORT || dst_type == Type.BYTE) { dst_type = Type.INT; }							 
		 
		if (src_type == Type.FLOAT) { src_type = Type.DOUBLE; }
		if (dst_type == Type.FLOAT) { dst_type = Type.DOUBLE; }	

		if (dst_type == null) {
			return;
		}
		
		if (src_type == dst_type) {
			return;
		}
		
	
		Type type_bool   = Type.BOOL;
		Type type_int    = Type.INT;
		Type type_uint   = Type.UINT;
		Type type_double = Type.DOUBLE;
		Type type_string = Type.STRING;
		
		
		
		boolean src_intish = src_type == Type.INT  || src_type == Type.UINT;
		boolean dst_intish = dst_type == Type.INT  ||  dst_type == Type.UINT;	
	
		
		//boolean explicit = true;
		
		int work = src_type.workRequiredToCastTo(dst_type, explicit);

		
		
		
		if (work == Type.WORK_NONE) {
			return;
		}
		else if (work == Type.WORK_INCOMPATIBLE) 
		{
			System.err.println("attempted to cast: " + ps + " => " + pd);
			throw new RuntimeException("Invalid cast: " + src_type + " => " + dst_type + " explicit: " + explicit);
		}
		
		if (work == Type.WORK_ENUM_CAST) {
			// do nothing
		}
		else if (work == Type.WORK_NULL_TO_DELEGATE_NULL)
		{
			pw.op(IGAsm.pop);
			pw.op(IGAsm.push_fp_null);
		}
		else if (work == Type.WORK_OBJECT_CAST) {
			if (dst_type == Type.STRING) {
				pw.op(IGAsm.call_ret1_static,"1","iglang.String", "stringFromObject");
				pw.addFunctionDependency("iglang.String", "stringFromObject");
			}
			else
			{
				pw.addModuleDependency(dst_type.toString());
				pw.op(IGAsm.cast_to , dst_type.toString());
			}
		}
		else if (work == Type.WORK_PRIMITIVE_CAST) {
			if ((src_intish || src_type == type_bool) && dst_type == type_double) {
				pw.op(IGAsm.i32_to_f64);
			}
			else if (src_type == type_double && dst_intish) {
				pw.op(IGAsm.f64_to_i32);
			}
			else if (src_type == type_bool && dst_intish) {
				// do nothing
			}	
			else if (src_intish && dst_type == type_bool) {
				pw.op(IGAsm.cmp_eq_i32);
				pw.op(IGAsm.not);
			}
			else
			{
				throw new RuntimeException("Invalid primitive cast");
			}
			//i32_to_f64
			//f64_to_i32
		}
		else if (work == Type.WORK_PRIMITIVE_TO_STRING)
		{
			if (src_intish) {
				pw.addFunctionDependency("iglang.String", "stringFromInt");
				pw.op(IGAsm.call_ret1_static, "1", "iglang.String", "stringFromInt");
			}
			else if (src_type == type_bool) {
				pw.addFunctionDependency("iglang.String", "stringFromBool");
				pw.op(IGAsm.call_ret1_static, "1",  "iglang.String", "stringFromBool");
			}
			else if (src_type == type_double) {
				pw.addFunctionDependency("iglang.String", "stringFromDouble");
				pw.op(IGAsm.call_ret1_static, "1", "iglang.String", "stringFromDouble");
			}
			
			else
			{
				throw new RuntimeException("Unknown cast: '" + src_type + "' => '" + dst_type + "'");
			}
		}
		else if (work == Type.WORK_STRING_TO_PRIMITIVE)
		{
			if (dst_intish) {
				pw.addFunctionDependency("iglang.String", "intValue");
				pw.op(IGAsm.call_ret1_static,"1","iglang.String","intValue");
			}
			else if (dst_type == type_bool) {
				pw.addFunctionDependency("iglang.String", "boolValue");
				pw.op(IGAsm.call_ret1_static,"1","iglang.String","boolValue");
			}
			else if (dst_type == type_double) {
				pw.addFunctionDependency("iglang.String", "doubleValue");
				pw.op(IGAsm.call_ret1_static,"1","iglang.String","doubleValue");			
			}
		
			else
			{
				throw new RuntimeException("Unknown cast: " + src_type + " => " + dst_type);
			}
		}		
		else if (work == Type.WORK_ENUM_TO_STRING) {
			pw.addFunctionDependency(src_type.toString(), "toString");
			pw.op_comment(IGAsm.call_ret1_static, "1", src_type.toString(), "toString",  /*comment*/ "enum");
		}
		else
		{
			pw.addModuleDependency(dst_type.toString());
			pw.op(IGAsm.cast_to, src_type.toString(), dst_type.toString());
		}
	}
	
	
	private final IGAsmOp [] outputFunctionBody(IGMember m, IGNode node, IGAsmDocument pw) throws IOException
	{
		return outputFunctionBody(m, node, pw, 0);
	}
	
	private final int getLocalIndex(IGMember m, Token name, boolean setter_context)
	{

		int idx =  m.lookupLocalVariable(name, setter_context);
		if (idx == -1) {
			name.printError("what ?");
			throw new RuntimeException("Local not found with name: " + name.m_value);
		}
		
		// but what about things that "DON'T" have super declared
		if (!m.hasModifier(IGMember.STATIC) && m.m_parent_source.m_class_extends != null) 
		{ 
			// super and this are declared as the first parameters
			idx = idx - 1;
			if (idx < 0) idx = 0;
		}
		
		return idx;

	}
	
	private final int getLocalIndex(IGMember m, Token blame, String name, int index, boolean setter_context)
	{

		int idx =  m.lookupLocalVariable(name, index, setter_context);
		if (idx == -1) {	
			blame.printError("what ?");
			
			m.debugVariables();
			throw new RuntimeException("Variable not found with name: " + name);
		}
		
		// THIS IS SUPER HACKY
		// TODO REPLACE WITH A DIFFERENT SOLUTION
		// SHOULD REALLY CALCULATE THIS UP FRONT BASED OFF OF PARAMETERS
		// AND LIFECYCLES
		if (!m.hasModifier(IGMember.STATIC) && m.m_parent_source.m_class_extends != null) 
		{ 
			// super and this are declared as the first parameters
			idx = idx - 1;
			if (idx < 0) idx = 0;
		}
		
		return idx;

	}
	
	
	private static final int BEHAVIOUR_LOAD  = 0;
	private static final int BEHAVIOUR_STORE = 1;
	
	
	private ArrayList<String> m_continue_break = new ArrayList<String>();
	private final void pushContinueBreak(String c_label, String b_label) {
		m_continue_break.add(c_label);
		m_continue_break.add(b_label);
	}
	
	private final String getContinueLabel() {
		return m_continue_break.get(m_continue_break.size() - 2);
	}

	private final String getBreakLabel() {
		return m_continue_break.get(m_continue_break.size() - 1);
	}
	
	private final void popContinueBreak() {
		// pop the two records off
		m_continue_break.remove(m_continue_break.size() - 1);
		m_continue_break.remove(m_continue_break.size() - 1);
	}
	
	/**
	 * Return the op necessary to append a specific type to the string builder
	 */
	
	private final String stringBuilderAppend(Type type, IGAsmDocument pw) throws IOException {
	
		Type dst_type  = Type.STRING;
		
	
		Type src_type = m_ctx.resolveType(type);					 
		int work = src_type.workRequiredToCastTo(dst_type, true);

		
		if (work == Type.WORK_NONE) {
			return "appendObject";
		}
		else if (work == Type.WORK_INCOMPATIBLE) {

			throw new RuntimeException("stringBuilderAppend Invalid cast: " + src_type + " => " + dst_type);
		}
	
		if (work == Type.WORK_OBJECT_CAST) {
			return "appendObject";
		}
		else if (work == Type.WORK_PRIMITIVE_TO_STRING)
		{
			if (src_type == Type.INT || src_type == Type.UINT) {
				return "appendInt";
			}
			else if (src_type == Type.SHORT || src_type == Type.BYTE) {
				return "appendInt";
			}
			else if (src_type == Type.DOUBLE) {
				return "appendDouble";
			}
			else if (src_type == Type.BOOL) {
				return "appendBool";
			}
			else {
				throw new RuntimeException("Invalid PRIMITIVE TO STRING");
			}
		}	
		else if (work == Type.WORK_ENUM_TO_STRING) {
			pw.addFunctionDependency(src_type.toString(), "toString");
			pw.op_comment(IGAsm.call_ret1_static,"1", src_type.toString(), "toString",  /*comment*/ "enum");	
			return "appendObject";
		}
		else
		{
			throw new RuntimeException("stringBuilderAppend Invalid cast: " + src_type + " => " + dst_type);
		}
	}
	
	private final IGAsmOp[] processStringBuilder(IGMember m, IGNode node, IGAsmDocument pw, int behaviour) throws IOException
	{
		pw.addModuleDependency("iglang.StringBuilder");

		pw.op(IGAsm.__new,"iglang.StringBuilder");
		pw.op(IGAsm.dup);
		
		pw.addFunctionDependency("iglang.StringBuilder", "<constructor>");
		pw.op(IGAsm.call_static,"1","iglang.StringBuilder","<constructor>");
		
		ArrayList<IGNode> to_process = new ArrayList<IGNode>();
	
		while (node.m_node_type == IGNode.NODE_BINARY &&
		       node.m_token.m_type == TOK_ADD &&
		       node.m_type == Type.STRING) {
		       
		 	IGNode left = node.m_children.get(1);
		 	IGNode right = node.m_children.get(0);
		 	
		 	to_process.add(left);
		 	//outputFunctionBody(m, left, pw);

		 
		 	node = right;  
		}
	
		to_process.add(node);
		
		for (int i = to_process.size() - 1; i >= 0; i--) {
			node = to_process.get(i);
			outputFunctionBody(m, node, pw);
			
			String append_function = stringBuilderAppend(node.m_type, pw);

			pw.addFunctionDependency("iglang.StringBuilder", append_function);
			pw.op(IGAsm.call_ret1_static,"2","iglang.StringBuilder", append_function);
		}
			 		
		pw.addFunctionDependency("iglang.StringBuilder", "toString");
		pw.op(IGAsm.call_ret1_static,"1","iglang.StringBuilder","toString");

		return empty2();
	}
	
	private static IGAsmOp [] s_empty2 = null;
	private static IGAsmOp [] s_empty3 = null;
	
	private final IGAsmOp[] empty2() {
		if (s_empty2 == null) {
			s_empty2 = new IGAsmOp[2];
			s_empty2[0] = null;
			s_empty2[1] = null;
		}
		return s_empty2;
	}
	
	private final IGAsmOp[] empty3() {
		if (s_empty3 == null) {
			s_empty3 = new IGAsmOp[3];
			s_empty3[0] = null;
			s_empty3[1] = null;
			s_empty3[2] = null;
		}
		return s_empty3;
	}


	public static final int BEHAVIOUR_READ_WRITE = 1;
	
	
	private final IGAsmOp[] outputFunctionBody(IGMember m, IGNode node, IGAsmDocument pw, int behaviour) throws IOException
	{
		if (pass1.IGSettings.s_optimization_level > 0)
		{
			if (node.m_node_type == IGNode.NODE_UNARY ||
				node.m_node_type == IGNode.NODE_BINARY ||
				(behaviour == 0 &&
					(node.m_node_type == IGNode.NODE_DOT ||
					node.m_node_type == IGNode.STATIC_FIELD_ACCESS))
				)
			{
				IGValueRange vr = node.m_value_range;
				if (vr != null && vr.isConcrete()) {
					if (vr.m_type == IGValueRange.TYPE_INT) {
					
						int val = vr.intValue();
						pw.op(IGAsm.push_i32, Integer.toString(val));
				
						return empty2();
					}
					else if (vr.m_type == IGValueRange.TYPE_DOUBLE) {
						pw.op(IGAsm.push_f64, ""+vr.doubleValue());
				
						return empty2();
					}
				}
			}
		}
		switch (node.m_node_type)
		{
			case IGNode.NODE_RETURN: break;
			case IGNode.NODE_BLOCK: break;
			case IGNode.NODE_CALL: break;
			case IGNode.NODE_PARAMETER_LIST: break;
			case IGNode.NODE_TYPE_LOCAL_VARIABLE_DEFINITION: break;
			case IGNode.ID_TYPE_PAIR: break;
			case IGNode.LOCAL_VARIABLE_ACCESS: break;
			case IGNode.STATIC_FIELD_ACCESS: break;
			case IGNode.NODE_TRINARY: break;
			case IGNode.NODE_VALUE: break;
			case IGNode.NODE_TYPE_BRACKETED_EXPRESSION: break;
			case IGNode.NODE_TYPE_CAST:  break;
			case IGNode.NODE_BINARY: break;
			case IGNode.NODE_IF: break;
			case IGNode.NODE_DOT: break;
			//case IGNode.NODE_DOT_GET: break;
			//case IGNode.NODE_DOT_SET: break;	
			case IGNode.NODE_NEW: break;		
			case IGNode.DOT_ACCESSOR: break;
			case IGNode.STATIC_DOT_ACCESSOR: break;
			case IGNode.NATIVE_BLOCK: break;
			case IGNode.FIELD_ACCESS: break;
			case IGNode.INCREMENT: break;
			case IGNode.NODE_INDEX : break;
			//case IGNode.NODE_LOCAL_SET: break;
			//case IGNode.NODE_LOCAL_GET: break;
			case IGNode.LOCAL_ACCESSOR: break;
			case IGNode.STATIC_LOCAL_ACCESSOR: break;
			case IGNode.NODE_UNARY: break;
			case IGNode.STATEMENT: break;
			case IGNode.PARAMETER_CAST: break;
			case IGNode.THROW: break;
			case IGNode.TRY: break;
			case IGNode.NODE_FOR: break;
			case IGNode.NODE_TYPE_FOREACH: break;
			case IGNode.FLOW_CHANGE:	break;	// breaks, continues etc
			case IGNode.SWITCH:	break;
			case IGNode.SWITCH_CASE:	break;
			case IGNode. SWITCH_DEFAULT:	break;
			default:
				pw.comment("Unknown node: " + node.m_node_type + " " + node.m_children.size());
				if (node.m_node_type == 0 && node.m_token != null) {
					node.debug(1);
					node.m_token.printError("Non typed node: ");
				}
				
				break;
		}	
		switch (node.m_node_type)
		{
			case IGNode.NODE_TYPE_LOCAL_VARIABLE_NAME 		: { throw new RuntimeException("what?"); }

			 case IGNode.NODE_TYPE_LOCAL_VARIABLE_DEFINITION : { 
			 	if (node.m_children.size() != 1) {
			 		throw new RuntimeException("Invalid arg");
			 	}
			 	
			 	outputFunctionBody(m, node.m_children.get(0), pw);
			 	
			 	// pop the values off the stack following a store
			 	if (node.m_children.get(0).m_token.m_type == TOK_ASSIGN) {
			 		for (int i = node.m_type.getTupleCount(); i > 0; i--) {
			 			pw.op(IGAsm.pop);
			 		}
			 	}
			 	//if (node.m_children.get(0).m_token.m_type == TOK_ASSIGN) {
			 	//	pw.op(IGAsm.pop);
			 	//}
			 
			 	break;
			 }
			 
			 case IGNode.STATEMENT: 
			 {
			 	outputFunctionBody(m, node.m_children.get(0), pw);
			 	
			 	// if there's anything still left on the stack at this point pop it off
			 	if (node.m_type != Type.get("void")) {
			 		for (int i = node.m_type.getTupleCount(); i > 0; i--) {
			 			pw.op(IGAsm.pop);
			 		}
			 	}
			 	break;
			 }

			 //////////////////////////////////////////////////////////////
			 
			 case IGNode.LOCAL_VARIABLE_ACCESS     : 
			 { 
			 	if (behaviour == BEHAVIOUR_READ_WRITE)
			 	{
		 			IGAsmOp [] diffs =  new IGAsmOp[2];

		 			final int load_index  = getLocalIndex(m, node.m_token, false);
		 			final int store_index = getLocalIndex(m, node.m_token, true);
		 			
		 			final String local_variable_name = node.m_token.m_value;
		 			int vector_size = node.m_type.getTupleCount();
		 			if (vector_size == 1) {

						diffs[0] = IGAsmOp.make_op_comment(IGAsm.local_load,  intToString(load_index), local_variable_name);
						diffs[1] = IGAsmOp.make_op(IGAsm.dup).
									op_comment(IGAsm.local_store, intToString(store_index), local_variable_name);
					}
					else 
					{
						IGAsmOp load_op  = IGAsmOp.make_op_comment(IGAsm.local_load, intToString(load_index), local_variable_name);
						for (int vi = 1; vi < vector_size; vi++) {
							load_op = load_op.op(IGAsm.local_load, intToString(load_index + vi));
						}

						IGAsmOp store_op  = IGAsmOp.make_op(IGAsm.nop);
						// funny we don't have a dup that works in this context
						// so we must first store then write.. this can be optimized out later
						for (int vi = vector_size - 1; vi >= 0 ; vi--) {
							store_op = store_op.op(IGAsm.local_store, intToString(store_index + vi));
						}
						for (int vi = 0; vi < vector_size; vi++) {
							store_op = store_op.op(IGAsm.local_load, intToString(store_index + vi));
						}	

						diffs[0] = load_op;
						diffs[1] = store_op;
					}
					return diffs;
			 	}
			 	else
			 	{

			 		final int tuple_size =  node.m_type.getTupleCount();
			 		final int load_index  = getLocalIndex(m, node.m_token,false);
			 		for (int vi = 0; vi < tuple_size; vi++)
			 		{
				 		pw.op_comment(IGAsm.local_load, intToString(load_index + vi),     /*comment*/ node.m_token.m_value);
					}

				}
			 	break;
			 }

	
				// ID : TYPE
				case IGNode.ID_TYPE_PAIR              : { 
					if (node.m_children.size() != 2) {
						throw new RuntimeException("Invalid arg");
					}
					
					IGNode id_node = node.m_children.get(0);
					
					
					if (behaviour == BEHAVIOUR_READ_WRITE) {
						IGAsmOp [] diffs =  new IGAsmOp[2];

			 			final int index  = getLocalIndex(m, id_node.m_token, true);
	
			 			
			 			final String local_variable_name = id_node.m_token.m_value;
			 			final int    vector_size         = id_node.m_type.getTupleCount();
			 			if (vector_size == 1) {

							diffs[0] = IGAsmOp.make_op_comment(IGAsm.local_load,  intToString(index), local_variable_name);
							diffs[1] = IGAsmOp.make_op(IGAsm.dup).
										op_comment(IGAsm.local_store, intToString(index), local_variable_name);
						}
						else 
						{
							IGAsmOp load_op  = IGAsmOp.make_op_comment(IGAsm.local_load, intToString(index), local_variable_name);
							for (int vi = 1; vi < vector_size; vi++) {
								load_op = load_op.op(IGAsm.local_load, intToString(index + vi));
							}

							IGAsmOp store_op  = IGAsmOp.make_op(IGAsm.nop);
							// funny we don't have a dup that works in this context
							// so we must first store then write.. this can be optimized out later
							for (int vi = vector_size - 1; vi >= 0 ; vi--) {
								store_op = store_op.op(IGAsm.local_store, intToString(index + vi));
							}
							for (int vi = 0; vi < vector_size; vi++) {
								store_op = store_op.op(IGAsm.local_load, intToString(index + vi));
							}	

							diffs[0] = load_op;
							diffs[1] = store_op;
						}
						//return diffs;


						/*

						//IGAsmOp [] diffs =  new IGAsmOp[2];
						diffs[0] = //null;	//"local_load " + getLocalIndex(m, id_node.m_token, false)  + " #" + id_node.m_token.m_value;
								   IGAsmOp.make_op_comment(IGAsm.local_load, intToString(getLocalIndex(m, id_node.m_token, true)),  id_node.m_token.m_value);	
						
						diffs[1] = IGAsmOp.make_op(IGAsm.nop).
										op(IGAsm.dup).op_comment(IGAsm.local_store, intToString(getLocalIndex(m, id_node.m_token, true)), id_node.m_token.m_value);
						*/						
						
						return diffs;
					
					}
					else
					{
						final int local_index = getLocalIndex(m, id_node.m_token, true) ;
						if (id_node.m_type.isTuple()) {
							final Type [] tuple_types = id_node.m_type.getTupleTypes();

							pw.comment("defined " + id_node.m_token.m_value);
							for (int i = 0; i < tuple_types.length; i++) {
								final int storage_class = tuple_types[i].getStorageClass();
								pw.op_comment(IGAsm.local_clear, intToString(local_index + i), 
										intToString(storage_class), /*comment*/ id_node.m_token.m_value);
							}
						}
						else
						{
							final int storage_class = id_node.m_type.getStorageClass();
							// need to come up with variants
							pw.comment("defined " + id_node.m_token.m_value);
							pw.op_comment(IGAsm.local_clear, intToString(local_index), 
										intToString(storage_class), /*comment*/ id_node.m_token.m_value);
						}

					}
					
					break;
			 	}

	
				case IGNode.NODE_TYPE_TYPE: { throw new RuntimeException("what?"); }
				case IGNode.NODE_TYPE_VAR:  { throw new RuntimeException("what?"); }

	
			case IGNode.FIELD_ACCESS              : { 
				String my_data_type = m_ctx.getType(m.m_parent_source).toString();		
				if (behaviour == BEHAVIOUR_READ_WRITE)
				{
						pw.addMemberDependency(my_data_type.toString(), node.m_token.m_value);
						IGAsmOp [] diffs =  new IGAsmOp[2];
						diffs[0] = IGAsmOp.make_op(IGAsm.this_load,  my_data_type.toString(), node.m_token.m_value);
						diffs[1] = IGAsmOp.make_op(IGAsm.dup).op("this_store", my_data_type.toString(), node.m_token.m_value);
				
						return diffs;
				}
				else
				{
					pw.addMemberDependency(my_data_type.toString(), node.m_token.m_value);
					pw.op(IGAsm.this_load, my_data_type, node.m_token.m_value);
				}		
				break;
			}

	

				case IGNode.STATIC_FIELD_ACCESS: 
				{ 	
					String my_data_type = m_ctx.getType(m.m_parent_source).toString();		
					if (behaviour == BEHAVIOUR_READ_WRITE)
					{
							pw.addMemberDependency(my_data_type.toString(), node.m_token.m_value);

							IGAsmOp [] diffs =  new IGAsmOp[2];
							diffs[0] = IGAsmOp.make_op_comment(IGAsm.static_load,  my_data_type.toString(), node.m_token.m_value,   /*comment*/ "IGNode.STATIC_FIELD_ACCESS");
							diffs[1] = IGAsmOp.make_op(IGAsm.dup).op_comment(IGAsm.static_store, my_data_type.toString(),  node.m_token.m_value, /*comment*/ "IGNode.STATIC_FIELD_ACCESS");
					
							return diffs;
					}
					else
					{
						pw.addMemberDependency(my_data_type.toString(), node.m_token.m_value);
						pw.op_comment(IGAsm.static_load, my_data_type.toString(), node.m_token.m_value, /*comment*/ "IGNode.STATIC_FIELD_ACCESS");
					}		
					break;
				}

				// TYPE (<child0>)
				case IGNode.NODE_TYPE_CAST: 
				{ 
					Type dst_type = node.m_type;
					Type src_type = node.m_children.get(1).m_type;
					
					outputFunctionBody(m, node.m_children.get(1), pw);
					castTo(src_type, dst_type, pw, true);
				
			 		break;
			 	}

				// XXXXX<child0> . ID<child1>
				case IGNode.NODE_DOT: 
				{  
					Characterization ch = new Characterization(m_ctx, Characterization.FIELD, this, m, node);
					IGNode id_node = node.m_children.get(1);
					

					String characterization_type = ch.getType().toString();
					String characterization_name = ch.getName();

					pw.addMemberDependency(characterization_type, characterization_name);

					// this member load situtation isn't always 100% correct.
					// this also propagates to this load
					if (ch.isObject()) 
					{
						outputFunctionBody(m, node.m_children.get(0), pw);
					
						// dispatch the alternatives to be used for a setter/modifier context
						if (behaviour == BEHAVIOUR_READ_WRITE) {
							IGAsmOp [] diffs =  new IGAsmOp[3];
							diffs[0] = IGAsmOp.make_op(IGAsm.member_load, ch.getType().toString(), ch.getName());
							diffs[1] = IGAsmOp.make_op(IGAsm.dup1x).op("member_store", ch.getType().toString(), ch.getName());
							diffs[2] = IGAsmOp.make_op(IGAsm.dup);
							return diffs;
						}
						else {
							pw.op(IGAsm.member_load, ch.getType().toString(), ch.getName());
						}
					}
					else if (ch.isStatic()) {

							
						if (behaviour == BEHAVIOUR_READ_WRITE) {
							IGAsmOp [] diffs =  new IGAsmOp[2];
							diffs[0] = IGAsmOp.make_op_comment(IGAsm.static_load,  ch.getType().toString(), ch.getName(), /*comment*/ "IGNode.NODE_DOT");
							diffs[1] = IGAsmOp.make_op(IGAsm.dup).op_comment(IGAsm.static_store, ch.getType().toString(), ch.getName(), /*comment*/ "IGNode.NODE_DOT");
							
							return diffs;
						}
						else {
							pw.op_comment(IGAsm.static_load, ch.getType().toString(), ch.getName(), /*comment*/ "IGNode.NODE_DOT");
						}					
					}
					break;
				}
				
				//case IGNode.NODE_LOCAL_GET:
				//case IGNode.NODE_LOCAL_SET:
				case IGNode.LOCAL_ACCESSOR:
				case IGNode.STATIC_LOCAL_ACCESSOR:
				{
				
					
				
					Characterization ch = new Characterization(m_ctx, Characterization.FIELD, this, m, node);

					IGMember the_actual_function = (IGMember)node.m_scope;

					//pw.comment("NODE_LOCAL_GET or NODE_LOCAL_SET o:" + ch.isObject() + 
					//		" s: " + ch.isStatic() + " f:" + ch.isFinal() + " e:" + ch.isEnum() + 
					//		" the_actual_function: " + the_actual_function.m_name + " " + the_actual_function);

					if (ch.isInterface())
					{
						throw new RuntimeException("Impossible case");
					}
					// should actually be called is member?
					else if (ch.isObject()) 
					{
						pw.op_comment(IGAsm.local_load, "0",  /* comment */ "this (local set/get)");
					
					
						
						String call_type =  "vtable";				
						
						if (ch.isSuper()) {
							call_type =  "super";	// this shouldn't be possible in this context
							ch.setType(m_ctx.getType(m.m_parent_source));
						}
						else if (ch.isFinal())
						{
							call_type =  "static";
							if (!ch.isEnum()) {
								call_type =  "static_vtable";
							}
						}
							
						//pw.comment("Checking");
							
						String callee_type = ch.getType().toString();
						if (call_type.equals("static_vtable"))
						{
							if (ch.getFunction().m_parent_source != ch.getObjectType().m_scope) {
								callee_type = "" + ch.getFunction().m_parent_source.getInstanceDataType();
							}
						}
						//else if (call_type.equals("static"))
						//{
						//	if (ch.getFunction().m_parent_source != ch.getObjectType().m_scope) {
						//		callee_type = "" + ch.getFunction().m_parent_source.getInstanceDataType();
						//	}
						//}


						pw.addFunctionDependency(callee_type, ch.getName() + "__get");
						pw.addFunctionDependency(callee_type, ch.getName() + "__set");
							
						if (behaviour == 1) {
							IGAsmOp [] diffs =  new IGAsmOp[3];
							diffs[0] = IGAsmOp.make_op("call_ret1_" + call_type, "1", 	   callee_type, ch.getName() + "__get");
							diffs[1] = IGAsmOp.make_op(IGAsm.dup1x).op("call_" + call_type,"2",callee_type,ch.getName() + "__set");
							diffs[2] = IGAsmOp.make_op(IGAsm.dup);
							return diffs;
						}
						else {
							pw.op("call_ret1_" + call_type, "1", callee_type, ch.getName() + "__get");
						}
					}
					else if (ch.isStatic()) {
					
						String callee_type = ch.getType().toString();
						//if (call_type.equals("static_vtable"))
						//{
						if (ch.getFunction().m_parent_source != ch.getObjectType().m_scope) {
							callee_type = "" + ch.getFunction().m_parent_source.getInstanceDataType();
						}
						//}


						pw.addFunctionDependency(callee_type, ch.getName() + "__get");
						pw.addFunctionDependency(callee_type, ch.getName() + "__set");

						if (behaviour == 1) {
							IGAsmOp [] diffs =  new IGAsmOp[2];
							diffs[0] = IGAsmOp.make_op(IGAsm.call_ret1_static,"0",callee_type, ch.getName() + "__get");
							diffs[1] = IGAsmOp.make_op(IGAsm.dup).op("call_static", "1", callee_type, ch.getName() + "__set");
							return diffs;
						}
						else {
							pw.op(IGAsm.call_ret1_static,"0",callee_type,  ch.getName() + "__get");
						}					
					}
					break;
				}
				

				//case IGNode.NODE_DOT_GET:
				//case IGNode.NODE_DOT_SET: 
				
				case IGNode.STATIC_DOT_ACCESSOR:
				case IGNode.DOT_ACCESSOR:
				{ 
					//pw.comment("NODE_DOT_GET or NODE_DOT_SET");
					Characterization ch = new Characterization(m_ctx, Characterization.FIELD, this, m, node);
					IGNode id_node = node.m_children.get(1);
					
					if (ch.isInterface()) {
						outputFunctionBody(m, node.m_children.get(0), pw);
					

						pw.addFunctionDependency(ch.getType().toString(), ch.getName() + "__get");
						pw.addFunctionDependency(ch.getType().toString(), ch.getName() + "__set");


						if (behaviour == 1) {
							IGAsmOp [] diffs =  new IGAsmOp[3];
							diffs[0] = IGAsmOp.make_op(IGAsm.call_ret1_interface, "1", ch.getType().toString(), ch.getName() + "__get");
							diffs[1] = IGAsmOp.make_op(IGAsm.dup1x).op("call_interface","2",ch.getType().toString(),ch.getName() + "__set");
							diffs[2] = IGAsmOp.make_op(IGAsm.dup);
							return diffs;
						}
						else {
							pw.op(IGAsm.call_ret1_interface, "1", ch.getType().toString(), ch.getName()+ "__get");
						}
					}
					else if (ch.isObject()) {
						outputFunctionBody(m, node.m_children.get(0), pw);
						
						String object_type = ch.getType();
						
						if (object_type.startsWith("iglang.Array<") && ch.getName().equals("length")) {
						
							if (behaviour == 1) {
								throw new RuntimeException("error");
							}
							else
							{
								pw.op_comment(IGAsm.array_length, /*comment */ ch.getType().toString());
							}
						}
						else
						{	
							boolean can_inline = false;						

							String call_type =  "vtable";	
							if (ch.isSuper()) {
								call_type =  "super";
								ch.setType(m_ctx.getType(m.m_parent_source));	
							}
							else 			
							if (ch.isFinal())
							{
								call_type = "static";
								if (!ch.isEnum()) {
									call_type =  "static_vtable";
								}
								can_inline = true;
							}
							
							Type callee_type = ch.getObjectType();
							if (call_type.equals("static_vtable"))
							{
								if (ch.getFunction().m_parent_source != ch.getObjectType().m_scope) {
									callee_type = ch.getFunction().m_parent_source.getInstanceDataType();
								}
							}
							

							pw.addFunctionDependency(callee_type.toString(), ch.getName() + "__get");
							pw.addFunctionDependency(callee_type.toString(), ch.getName() + "__set");
	
							
							if (behaviour == 1) {
								IGAsmOp [] diffs =  new IGAsmOp[3];
								diffs[0] = IGAsmOp.make_op("call_ret1_" + call_type,        "1", callee_type.toString(), ch.getName() + "__get");
								diffs[1] = IGAsmOp.make_op(IGAsm.dup1x).op("call_" + call_type, "2", callee_type.toString(), ch.getName() + "__set");
								diffs[2] = IGAsmOp.make_op(IGAsm.dup);
								return diffs;
							}
							else {
							
								boolean was_inlined = false;
								
								/*
								if (can_inline && !callee_type.m_primary.equals("iglang.Array")) 
								{
									// we know that the member in question is either in the callee_type or in a sub type
									// really need to drill down for all member_load and this_load to make sure the thing we're accessing makes sense
									
									
									IGMember inline_member = ch.getFunction();
									
									// TODO finalize up the resolving steps
									// TODO forbid classses from extending templated classes
									//System.out.println("call-type: " + call_type + " " + inline_member.m_parent_source);
									//System.out.println("Inlineing ??? " + scope + " " + obj_type + " " + callee_type + " " + ch.getName() + "__get");
									//System.out.println("TYPE: " + inline_member.m_inline_candidate);
									
									if (inline_member.m_inline_candidate == 1)
									{
										IGSource  new_source = (IGSource)callee_type.m_scope;

										pw.op_comment("member_load", "" + callee_type, inline_member.m_inline_candidate_node.m_token.m_value, "Inlined " + callee_type.toString()+ " " + ch.getName() + "__get");
										was_inlined = true;
									}
									else if (inline_member.m_inline_candidate == 2)
									{
										throw new RuntimeException("Failure bro.");						
									}
									
									
								}
								*/
								
								if (!was_inlined)
								{
									pw.op("call_ret1_" + call_type, "1", callee_type.toString(), ch.getName() + "__get");
								}
							}
						}
					}
					else if (ch.isStatic()) {


						pw.addFunctionDependency(ch.getType().toString(), ch.getName() + "__get");
						pw.addFunctionDependency(ch.getType().toString(), ch.getName() + "__set");

						if (behaviour == 1) {
							IGAsmOp [] diffs =  new IGAsmOp[2];
							diffs[0] = IGAsmOp.make_op(IGAsm.call_ret1_static,"0", ch.getType().toString(), ch.getName() + "__get");
							diffs[1] = IGAsmOp.make_op(IGAsm.dup).op("call_static","1",ch.getType().toString(),ch.getName() + "__set");
							return diffs;
						}
						else {
							pw.op(IGAsm.call_ret1_static, "0", ch.getType().toString(), ch.getName() + "__get");
						}					
					}
					break;
				}

				// XXXXX '[' index ']'
				// an array or map will always be on the right side
				case IGNode.NODE_INDEX: 
				{ 
					outputFunctionBody(m, node.m_children.get(0), pw);
					
					outputFunctionBody(m, node.m_children.get(1), pw);
					//castTo(left_type,  cast_type, pw, explicit);
				
					Type t = m_ctx.resolveType(node.m_children.get(0).m_type);
				
					if (t.m_primary.equals("iglang.Array")) 
					{
						int storage_class = t.m_left.getStorageClass();
						
						//if (t.m_left == Type.get("iglang.Short") && storage_class != 2) {
						//	throw new RuntimeException("What gives?");
						//}
				
					
						if (behaviour == 1) {
							IGAsmOp [] toks = new IGAsmOp[3];
							toks[0] = IGAsmOp.make_op(IGAsm.array_load, t.toString(), intToString(storage_class));
							toks[1] = IGAsmOp.make_op(IGAsm.dup2x).op("array_store", t.toString(), intToString(storage_class));
							toks[2] = IGAsmOp.make_op(IGAsm.dup2);
							return toks;
						}
						else {
							pw.op(IGAsm.array_load, t.toString(), intToString(storage_class));
						}
					}
					else
					{
						IGSource source = (IGSource)t.m_scope;


						pw.addFunctionDependency(t.toString(), "<get[]>");
						pw.addFunctionDependency(t.toString(), "<set[]>");

						
						if (behaviour == 1) 
						{
							IGMember getter = source.getMember("<get[]>");
							IGMember setter = source.getMember("<set[]>");
						



							IGAsmOp [] toks = new IGAsmOp[3];
							if (getter.hasModifier(IGMember.FINAL)) {
								toks[0] = IGAsmOp.make_op(IGAsm.call_ret1_static_vtable,"2",t.toString(), "<get[]>");
							}
							else {
								toks[0] = IGAsmOp.make_op(IGAsm.call_ret1_vtable,"2",t.toString(), "<get[]>");
							}
							if (setter.hasModifier(IGMember.FINAL)) {
								toks[1] = IGAsmOp.make_op(IGAsm.dup2x).op(IGAsm.call_static_vtable,"3",t.toString(), "<set[]>");
							}
							else {
								toks[1] = IGAsmOp.make_op(IGAsm.dup2x).op(IGAsm.call_vtable,"3",t.toString(), "<set[]>");
							}
							toks[2] = IGAsmOp.make_op(IGAsm.dup2);
							return toks;
						}
						else 
						{
							IGMember getter = source.getMember("<get[]>");
							if (getter.hasModifier(IGMember.FINAL)) {
								pw.op(IGAsm.call_ret1_static_vtable, "2", t.toString(), "<get[]>");
							}
							else 
							{
								pw.op(IGAsm.call_ret1_vtable, "2", t.toString(), "<get[]>");
							}
						}
					}
				
				 	break;
				}

				// XXXXX '(' params ')'
				case IGNode.NODE_CALL: 
				{ 
					IGNode callee    = node.m_children.get(0);
					IGNode call_args = node.m_children.get(1);
					
					int num_args = 0;

					for (int ai = 0; ai < call_args.m_children.size(); ai++) {
						Type arg_type = call_args.m_children.get(ai).m_type;
						if (!arg_type.isTuple()) {
							num_args++;
							continue;
						}

						num_args += arg_type.getTupleTypes().length;
					}
					



				 	String call_prefix = "call_";
					if (node.m_type != Type.VOID) {
						call_prefix = "call_ret1_";
					}
	
	
					//node.m_token.printError("check: ");
			
					Characterization ch = new Characterization(m_ctx, Characterization.CALL, this, m, callee);
					
					IGNode to_process = ch.getProcessingNode();
					if (to_process != null) {
						outputFunctionBody(m, to_process, pw);
					}
					
					if (ch.needsThisPush()) {
						pw.op_comment(IGAsm.local_load,"0", /* comment */ "this (auto push)");
					}
					
					if (ch.isFunctionPointer()) 
					{
						outputFunctionBody(m, call_args, pw);	
						
						pw.op(call_prefix + "dynamic", intToString(num_args));
					}
					else if (ch.isStatic())
					{

						pw.addFunctionDependency(ch.getType().toString(), ch.getName());

						// umm.. wtf statics actually need to refer to their container.. not the class its being called on
						outputFunctionBody(m, call_args, pw);	
						pw.op(call_prefix + "static", intToString(num_args), ch.getType().toString(), ch.getName());	
					}
					else if (ch.isInterface())
					{
						outputFunctionBody(m, call_args, pw);
						
						pw.op(call_prefix + "interface",  intToString(num_args + 1), ch.getName());	
					}
					else if (ch.isObject())
					{
						outputFunctionBody(m, call_args, pw);	
						
						if (ch.isSuper()) {
							ch.setType(m_ctx.getType(m.m_parent_source));
							
							String fn_name = ch.getName();
							

							pw.addFunctionDependency(ch.getType().toString(), fn_name);
							
							//BAD CONSTRUCTOR SET UP
							if ("<constructor>".equals(fn_name)) {
								// constructors DO NOT RETURN VALUES
								pw.op(IGAsm.call_super, intToString(num_args + 1), ch.getType().toString(), fn_name);	
								// this is to deal with the pop that'll inevitably follow the super call
								pw.op(IGAsm.local_load, "0");
							}
							else {
								pw.op(call_prefix + "super", intToString(num_args + 1), ch.getType().toString(), fn_name);	
							}
						}
						else 
						if (ch.isFinal())
						{
							//String reasoning = " HRM ";
							//String call_type = "call_static";
							if (!ch.isEnum()) {
							
		
								String call_type = "static_vtable";
								String callee_type = ch.getType().toString();
								String callee_name = ch.getName();
								
								
								if (call_type.equals("static_vtable"))
								{
									if (ch.getFunction().m_parent_source != ch.getObjectType().m_scope) {
										callee_type = "" + ch.getFunction().m_parent_source.getInstanceDataType();
									}
								}
								
								//reasoning += 
								  //        ch.getFunction().m_name.m_value + 
								 //   " " + ch.getFunction().m_parent_source.m_expected_component + 
								//	" " + ch.getFunction().hasModifier(IGMember.STATIC) +
								//	" " + ch.getFunction().hasModifier(IGMember.FINAL);

								
								if ("resize".equals(callee_name) && callee_type.startsWith("iglang.Array<")) {
									pw.op(IGAsm.array_resize, intToString(ch.getObjectType().m_left.getStorageClass()));
								}
								else if ("fill".equals(callee_name) && callee_type.startsWith("iglang.Array<")) {
									pw.op(IGAsm.array_fill, intToString(ch.getObjectType().m_left.getStorageClass()));
								}
								else if ("copy".equals(callee_name) && callee_type.startsWith("iglang.Array<")) {
									pw.op(IGAsm.array_copy, intToString(ch.getObjectType().m_left.getStorageClass()));
								}
								else
								{		
									pw.addFunctionDependency(callee_type, callee_name);
									pw.op(call_prefix + "static_vtable", intToString(num_args + 1), callee_type, callee_name);				
								}
							}
							else
							{
							
								Type   callee_type = ch.getObjectType();
								String callee_name = ch.getName();
								
								/// TODO VERIFY THAT THE FUNCTIONS IN QUESTION ARE SUBS
								if ("equals".equals(callee_name) && callee_type.isIntishEnum() && ((IGSource)callee_type.m_scope).canOptimizeMemberFunctionOut("equals")) {
									pw.op(IGAsm.cmp_eq_i32);
								}
								else if ("hashCode".equals(callee_name) && callee_type.isIntishEnum() && ((IGSource)callee_type.m_scope).canOptimizeMemberFunctionOut("hashCode")) {
									// do nothing.. there's no work to be done here
								}
								else
								{

									pw.addFunctionDependency(ch.getType().toString(), callee_name);
									pw.op(call_prefix + "static", intToString(num_args + 1), ch.getType().toString(), callee_name);				
								}
							}
							
						}
						else
						{
							pw.addFunctionDependency(ch.getType().toString(), ch.getName());
							pw.op(call_prefix + "vtable", intToString(num_args + 1), ch.getType().toString(), ch.getName());
						}
					}
					
					
			 		break;
			 	}

				// '(' EXPRESSION ')'
				case IGNode.NODE_TYPE_BRACKETED_EXPRESSION		: {   
					IGNodeList children = node.m_children;
					for (int i = 0; i < children.size(); i++) {
						outputFunctionBody(m, children.get(i), pw);
					}	
					break;
				}

				case IGNode.NODE_NEW: 
				{ 
					IGNode new_type = node.m_children.get(0);
					IGNode new_args = node.m_children.get(1);
					int arg_count = 0;
					
					Type new_t = m_ctx.resolveType(new_type.m_type);
					
					if (new_t.m_primary.equals("iglang.Array")) {
						int storage_class = new_t.m_left.getStorageClass();
						
						
						// note sure exactly why I created a NEW_CALL node type
						if (new_args.m_node_type != IGNode.NEW_CALL) {
							throw new RuntimeException("ex");
						}	
						else
						{
							if (new_args.m_children.size() > 0) {
								IGNode sub = new_args.m_children.get(0);
								arg_count = sub.m_children.size();
								outputFunctionBody(m, sub, pw);
							}
						}
						
						// TODO verify that there is only a single argument
						
						pw.op(IGAsm.array_new, m_ctx.resolveType(new_type.m_type).toString(),  intToString(storage_class));
					}
					else
					{
					
						pw.op(IGAsm.__new, new_t.toString());
						pw.op(IGAsm.dup);
					
						// note sure exactly why I created a NEW_CALL node type
						if (new_args.m_node_type != IGNode.NEW_CALL) {
							throw new RuntimeException("ex");
						}	
						else
						{
							if (new_args.m_children.size() > 0) {
								IGNode sub = new_args.m_children.get(0);
								arg_count = sub.m_children.size();
								outputFunctionBody(m, sub, pw);
							}
						}
					
						// wouldn't this imply that all MUST have constructors
						pw.addFunctionDependency(new_t.toString(), "<constructor>");
						pw.op(IGAsm.call_static, intToString(arg_count + 1), new_t.toString(), "<constructor>");
					}
					break;
				}

	
	
				// The following 3 are used when there is no specific 
				// operator mentioned
				/////////////////////////////////////////////////////////////
	
				case IGNode.NODE_UNARY						: 
				{ 
					//pw.println("#unary " + node.m_token.m_value);
					Type type = node.m_type;
					boolean is_bool   = type == Type.BOOL;
					boolean is_int    = type == Type.INT || type == Type.UINT;
					boolean is_double = type == Type.DOUBLE;

					IGNode child0 = node.m_children.get(0);

					if (node.m_token.m_type == TOK_SUB) {
					
						if (child0.m_node_type == IGNode.NODE_VALUE)
						{
							Token t = child0.m_token;
							
							// this feels like an early optimization
							// but for int min values it really isn't
							if (t.m_type == IGLexer.LEXER_INT) {
								pw.op(IGAsm.push_i32, "-" + t.m_value);
							}
							else if (t.m_type == IGLexer.LEXER_FLOAT) {
								pw.op(IGAsm.push_f64,  "-" + t.m_value);
							}
							else if (t.m_type == IGLexer.LEXER_HEX) {
								pw.op(IGAsm.push_h32,  "-" + t.m_value);
							}
							else if (t.m_type == IGLexer.LEXER_CHAR) {
								pw.op_comment(IGAsm.push_i32,  "-" + util.Decoder.escapedCharToInt(t.m_value), /*comment*/ "'" + t.m_value + "'");
							}
							else
							{
								throw new RuntimeException("Unknown unary optimization");
							}
						}
						else
						{
							if (is_int) {
								pw.op(IGAsm.push_i32,"0");
								outputFunctionBody(m, child0, pw);
								pw.op(IGAsm.sub_i32);
							}
							else if (is_double) {
								pw.op(IGAsm.push_f64,"0");
								outputFunctionBody(m, child0, pw);
								pw.op(IGAsm.sub_f64);
							}
							else {
								node.m_token.printError("huh?");
								throw new RuntimeException("Unknown case: " + type);
							}
						}
					}
					else if (node.m_token.m_type == TOK_ADD) {
						// nothing needs be done
						outputFunctionBody(m, node.m_children.get(0), pw);
					}
					else if (node.m_token.m_type == TOK_NOT) {
						outputFunctionBody(m, node.m_children.get(0), pw);
						pw.op(IGAsm.not);
					}
					else if (node.m_token.m_type == TOK_BIT_NOT) {
						outputFunctionBody(m, node.m_children.get(0), pw);
						pw.op(IGAsm.bit_not_i32);
					}
					else {
						throw new RuntimeException("Unknown case");
					}
					// this isn't fully implemented either
					break;
				}


				case IGNode.INCREMENT:
				{
					//pw.comment("increment " + node.m_token.m_value);
					
					boolean optimized_increment = false;
					IGNode storage_node = node.m_children.get(0);
					Type t = node.m_type;
					
					
					
					if (INCREMENT_OPTIMIZATION)
					{
						if (t == Type.INT || t == Type.UINT) 
						{
							if (storage_node.m_node_type == IGNode.LOCAL_VARIABLE_ACCESS && 
								!"super".equals(storage_node.m_token.m_value))
							{
								int load_index = getLocalIndex(m, storage_node.m_token, false);
								int store_index = getLocalIndex(m, storage_node.m_token, true);
								if (store_index >= 0 && store_index < 128 && load_index == store_index)
								{
									pw.op_comment(IGAsm.local_inc_i8, "" + store_index, (node.m_token.m_type == TOK_INC) ? "1" : "-1"
										, " inc");
									optimized_increment = true;
								}
							}
						}
					}
					
					if (!optimized_increment)
					{
						IGAsmOp [] diffs = outputFunctionBody(m, node.m_children.get(0), pw, 1);
							

						if (diffs.length > 2) {
							pw.asm(diffs[2]); // this should also have the number of dup values
						}
					
						// load the current value
						pw.asm(diffs[0]); 
						
						
						
					
						if (t == Type.DOUBLE) {
						
							if (node.m_token.m_type == TOK_INC) {
								// stack inc instead?
								pw.op(IGAsm.push_f64,"1.0");
								pw.op(IGAsm.add_f64);
							}
							else if (node.m_token.m_type == TOK_DEC) {
								pw.op(IGAsm.push_f64,"-1.0");
								pw.op(IGAsm.add_f64);					
							}	
						}
						else if (t == Type.INT || t == Type.UINT) 
						{
							if (node.m_token.m_type == TOK_INC) {
								pw.op(IGAsm.push_i32,"1");
								pw.op(IGAsm.add_i32);
							}
							else if (node.m_token.m_type == TOK_DEC) {
								pw.op(IGAsm.push_i32,"-1");
								pw.op(IGAsm.add_i32);					
							}						
						}
						else {
							throw new RuntimeException("Invalid increment");
						}
				
						// write back the value	
						pw.asm(diffs[1]);
						pw.op(IGAsm.pop);
					}
					break;
				}

				case IGNode.NODE_TUPLE_BINARY:
				{
					Token  op = node.m_token;
					IGNode left  = node.m_children.get(0);
					IGNode right = node.m_children.get(1);

					int count = left.m_type.getTupleCount();
					
					outputFunctionBody(m, left, pw);
					outputFunctionBody(m, right, pw);

					IGAsmOp result = null;
					switch(op.m_type) 
					{	
						case TOK_ADD: {
							result = IGAsmOp.make_op(IGAsm.add_f64_vec, intToString(count)); 
							break;
						}
						case TOK_SUB: {
							result = IGAsmOp.make_op(IGAsm.sub_f64_vec, intToString(count)); 
							break;
						}
						case TOK_MUL: {
							result = IGAsmOp.make_op(IGAsm.mul_f64_vec, intToString(count)); 
							break;
						}
					}
					pw.asm(result);

					break;
				}

				case IGNode.NODE_TUPLE_BINARY_SINGLE:
				{
					Token  op = node.m_token;
					IGNode left  = node.m_children.get(0);
					IGNode right = node.m_children.get(1);

					int count = left.m_type.getTupleCount();
					
					switch(op.m_type) 
					{	
						case TOK_MUL:
						{
							outputFunctionBody(m, left, pw);
							outputFunctionBody(m, right, pw);

							IGAsmOp result = IGAsmOp.make_op(IGAsm.mul_f64_vec_val, intToString(count)); 
							
							pw.asm(result);
							break;
						}
					}

					break;
				}


				case IGNode.NODE_BINARY						: 
				{ 
					//pw.println("#binary " + node.m_token.m_value);
					
					Token  op = node.m_token;
					IGNode left  = node.m_children.get(0);
					IGNode right = node.m_children.get(1);
					
					IGAsmOp result = null;
					//String mid_result = null;
					
					
					Type node_type = node.m_type;
					boolean node_is_int    = (node_type == Type.INT) ||  (node_type == Type.UINT);
					boolean node_is_bool   = node_type == Type.BOOL;
					boolean node_is_double = node_type == Type.DOUBLE;
					
					if (node_type.isEnum()) {
						Type prim = node_type.getEnumPrimitive();
						
						node_is_int    = (prim == Type.INT) ||  (prim == Type.UINT);
						node_is_bool   = prim == Type.BOOL;
						node_is_double = prim == Type.DOUBLE;
					}
					
					boolean params_are_doubleish = false;
					boolean params_are_intish    = false;
					boolean string_comparison_possible = false;
					
					if (left.m_type == Type.STRING && right.m_type != Type.NULL) {
						string_comparison_possible = true;
					}
					else if (right.m_type == Type.STRING  && left.m_type != Type.NULL) {
						string_comparison_possible = true;
					}
					
					
					if (left.m_type.getEnumPrimitive() == Type.DOUBLE ||
						left.m_type == Type.DOUBLE || 
						right.m_type == Type.DOUBLE ||
						right.m_type.getEnumPrimitive() == Type.DOUBLE) 
					{
						params_are_doubleish = true;
					}
					else if (
						left.m_type.getEnumPrimitive() == Type.INT  ||
						left.m_type.getEnumPrimitive() == Type.BOOL  ||
						left.m_type == Type.INT || 
						left.m_type == Type.UINT || 
						left.m_type == Type.BOOL ||
						left.m_type == Type.SHORT ||
						left.m_type == Type.BYTE ||
						
						
						right.m_type.getEnumPrimitive() == Type.INT  ||
						right.m_type.getEnumPrimitive() == Type.BOOL  ||
						right.m_type == Type.INT ||
						right.m_type == Type.UINT || 
						right.m_type == Type.BOOL ||
						right.m_type == Type.SHORT ||
						right.m_type == Type.BYTE)
					{
						params_are_intish = true;
					}
					
					
					Type left_type = left.m_type;
					Type right_type = right.m_type;
					
					Type cast_type = null;
					
					
					
					if (op.m_type == TOK_ASSIGN)
					{
						IGAsmOp [] diffs = outputFunctionBody(m, left, pw, 1);
						
						outputCast(m, right, left_type, pw);
						
						pw.asm(diffs[1]);
					}
					else if (op.m_type == TOK_ADD_ASSIGN ||
							 op.m_type == TOK_SUB_ASSIGN ||
							 op.m_type == TOK_MUL_ASSIGN ||
							 op.m_type == TOK_DIV_ASSIGN ||
							 op.m_type == TOK_MOD_ASSIGN || 
							 op.m_type == TOK_XOR_ASSIGN || 
							 op.m_type == TOK_OR_ASSIGN || 
							 op.m_type == TOK_AND_ASSIGN)
					{
						//if (left_count 
						IGAsmOp [] diffs = outputFunctionBody(m, left, pw, 1);
						
						// to any load/store busy work
						if (diffs.length > 2) {
							pw.asm(diffs[2]);
						}
						
						// load the current value
						pw.asm(diffs[0]);
			
						// The type we need to cast both sides to before proceeding
						Type intermediate_type = null;
						
						// the result always has to be turned back into the same type as the left side
						cast_type = left_type;
						
						// String +=
						if (node_type == Type.STRING)
						{
							if (op.m_type == TOK_ADD_ASSIGN) {
								pw.addFunctionDependency("iglang.String", "append");
								intermediate_type = Type.STRING;
								result = IGAsmOp.make_op(IGAsm.call_ret1_static, "2",  "iglang.String",  "append");
								
								// is this even necessary?
								cast_type = Type.STRING;
							}
						}
						// left or right hand side is a double
						else if (params_are_doubleish) {
							intermediate_type = Type.DOUBLE;
							
							switch(op.m_type) {
								case TOK_ADD_ASSIGN: result = IGAsmOp.make_op(IGAsm.add_f64); break;
								case TOK_SUB_ASSIGN: result = IGAsmOp.make_op(IGAsm.sub_f64); break;
								case TOK_MUL_ASSIGN: result = IGAsmOp.make_op(IGAsm.mul_f64); break;
								case TOK_DIV_ASSIGN: result = IGAsmOp.make_op(IGAsm.div_f64); break;
								case TOK_MOD_ASSIGN: result = IGAsmOp.make_op(IGAsm.mod_f64); break;
							}

						}
						// left and right hand sides are either enums, ints or uints
						else if (params_are_intish) {
							intermediate_type = Type.INT;
							
							switch(op.m_type) {
								case TOK_ADD_ASSIGN: result = IGAsmOp.make_op(IGAsm.add_i32); break;
								case TOK_SUB_ASSIGN: result = IGAsmOp.make_op(IGAsm.sub_i32); break;
								case TOK_MUL_ASSIGN: result = IGAsmOp.make_op(IGAsm.mul_i32); break;
								case TOK_DIV_ASSIGN: 	
									// why am I maintaining the same division for floats as ints?
									result = IGAsmOp.make_op(IGAsm.div_f64); 
									intermediate_type = Type.DOUBLE; 
									break;
								case TOK_MOD_ASSIGN: result = IGAsmOp.make_op(IGAsm.mod_i32); break;
								case TOK_XOR_ASSIGN: result = IGAsmOp.make_op(IGAsm.bit_xor_i32); break;
								case TOK_OR_ASSIGN: result = IGAsmOp.make_op(IGAsm.bit_or_i32); break;
								case TOK_AND_ASSIGN: result = IGAsmOp.make_op(IGAsm.bit_and_i32); break;
							}
						}
						
						if (result == null || intermediate_type == null) {
							op.printError("Invalid assignment");
							throw new RuntimeException("Invalid assignment");
						}
			
						castTo(left_type, intermediate_type, pw);
			
						outputFunctionBody(m, right, pw);
						castTo(right_type, intermediate_type, pw);			
						
						pw.asmNoCopy(result);
						
						// cast back to the storage type (is this 100% correct?)
						castTo(intermediate_type, cast_type, pw);			
						
						pw.asm(diffs[1]);				
					}
					else if (op.m_type == TOK_AND) {
					
						// a && b
						//     eval(a)
						//     if FALSE goto B1:
						//     eval(b)
						// B1: nop
					
						String term = getLabel("A");
						outputFunctionBody(m, left, pw);

						pw.op(IGAsm.and, term);
						IGAsmOp and_op = pw.getLastOP();
						
						outputFunctionBody(m, right, pw);	
						
						IGAsmOp last_op = pw.getLastOP();
						if (last_op.isLabeledNOP()) {
							and_op.m_p0 = last_op.m_label;
						}
						else {
							pw.label(term);					
						}
					}
					// current grouping of A || B || C is (A || B) || C
					// what we really want is  A || (B || C)
					// see UIWidget hitTEst with point ~2291
					else if (op.m_type == TOK_OR) 
					{
						// a || b
						//     eval(a)
						//     if TRUE goto B1:
						//     eval(b)
						// B1: nop
						String term = getLabel("O");
						outputFunctionBody(m, left, pw);
		
						pw.op(IGAsm.or, term);
						IGAsmOp or_op = pw.getLastOP();
						
						outputFunctionBody(m, right, pw);	
						
						IGAsmOp last_op = pw.getLastOP();
						if (last_op.isLabeledNOP()) {
							or_op.m_p0 = last_op.m_label;
						}
						else {
							pw.label(term);					
						}
					}
					else
					{
						boolean explicit = false;

						if (op.m_type == TOK_LSL && node_is_int) {
							result = IGAsmOp.make_op(IGAsm.lsl_i32);
							cast_type = Type.INT;
						}
						else if (op.m_type == TOK_LSR && node_is_int) {
							result = IGAsmOp.make_op(IGAsm.lsr_i32);
							cast_type = Type.INT;
						}
						else if (op.m_type == TOK_ASR && node_is_int) {
							result = IGAsmOp.make_op(IGAsm.asr_i32);
							cast_type = Type.INT;
						}
						else if (op.m_type == TOK_ADD && node_is_int) {
							result = IGAsmOp.make_op(IGAsm.add_i32);
							cast_type = Type.INT;
						}
						else if (op.m_type == TOK_SUB && node_is_int) {
							result = IGAsmOp.make_op(IGAsm.sub_i32);
							cast_type = Type.INT;
						}
						else if (op.m_type == TOK_MUL && node_is_int) {
							result = IGAsmOp.make_op(IGAsm.mul_i32);
							cast_type = Type.INT;
						}
						else if (op.m_type == TOK_MOD && node_is_int) {
							result = IGAsmOp.make_op(IGAsm.mod_i32);
							cast_type = Type.INT;
						}
						else if (op.m_type == TOK_BIT_AND && node_is_int) {
							result = IGAsmOp.make_op(IGAsm.bit_and_i32);
							cast_type = Type.INT;
						}
						else if (op.m_type == TOK_BIT_OR && node_is_int) {
							result = IGAsmOp.make_op(IGAsm.bit_or_i32);
							cast_type = Type.INT;
						}
						else if (op.m_type == TOK_BIT_XOR && node_is_int) {
							result = IGAsmOp.make_op(IGAsm.bit_xor_i32);
							cast_type = Type.INT;
						}
						// not a boolean... what was I thinking
						//else if (op.m_type == TOK_BIT_NOT && node_is_int) {
						//	result = "bit_not_i32";
						//	cast_type = Type.INT;
						//}
						else if (op.m_type == TOK_EQ && params_are_intish) {
							result = IGAsmOp.make_op(IGAsm.cmp_eq_i32);
							//cast_type = Type.DOUBLE;
						}
						else if (op.m_type == TOK_NE && params_are_intish) {
							result = IGAsmOp.make_op(IGAsm.cmp_eq_i32).op(IGAsm.not);							
							//cast_type = Type.DOUBLE;
						}
						else if (op.m_type == TOK_LT && params_are_intish) {
							result = IGAsmOp.make_op(IGAsm.cmp_lt_i32);							
							//cast_type = Type.DOUBLE;
						}
						else if (op.m_type == TOK_LE && params_are_intish) {
							result = IGAsmOp.make_op(IGAsm.cmp_le_i32);							
							//cast_type = Type.DOUBLE;
						}
						else if (op.m_type == TOK_GT && params_are_intish) {
							result = IGAsmOp.make_op(IGAsm.cmp_le_i32).op(IGAsm.not);						
						}
						else if (op.m_type == TOK_GE && params_are_intish) {
							result = IGAsmOp.make_op(IGAsm.cmp_lt_i32).op(IGAsm.not);	
						}					
						
						// what about bit not
						
						else if (op.m_type == TOK_ADD && node_is_double) {
							result = IGAsmOp.make_op(IGAsm.add_f64);
							cast_type = Type.DOUBLE;
						}
						else if (op.m_type == TOK_SUB && node_is_double) {
							result = IGAsmOp.make_op(IGAsm.sub_f64);
							cast_type = Type.DOUBLE;
						}
						else if (op.m_type == TOK_MUL && node_is_double) {
							result = IGAsmOp.make_op(IGAsm.mul_f64);
							cast_type = Type.DOUBLE;
						}
						// all divisions are promoted to float division
						else if (op.m_type == TOK_DIV) {				// && node_is_double
							if (node.m_type == Type.DOUBLE) {
								result = IGAsmOp.make_op(IGAsm.div_f64);
								cast_type = Type.DOUBLE;
							}
							else {
								result = IGAsmOp.make_op(IGAsm.div_i32);
								cast_type = Type.INT;
							}
						}
						else if (op.m_type == TOK_MOD && node_is_double) {
							result = IGAsmOp.make_op(IGAsm.mod_f64);
							cast_type = Type.DOUBLE;
						}
						
						
						else if (op.m_type == TOK_EQ && params_are_doubleish) {
							result = IGAsmOp.make_op(IGAsm.cmp_eq_f64);
							cast_type = Type.DOUBLE;
						}
						else if (op.m_type == TOK_NE && params_are_doubleish) {
							result = IGAsmOp.make_op(IGAsm.cmp_eq_f64).op(IGAsm.not);								
							cast_type = Type.DOUBLE;
						}
						else if (op.m_type == TOK_LT && params_are_doubleish) {
							result = IGAsmOp.make_op(IGAsm.cmp_lt_f64);							
							cast_type = Type.DOUBLE;
						}
						else if (op.m_type == TOK_LE && params_are_doubleish) {
							result = IGAsmOp.make_op(IGAsm.cmp_le_f64);							
							cast_type = Type.DOUBLE;
						}
						else if (op.m_type == TOK_GT && params_are_doubleish) {
							result = IGAsmOp.make_op(IGAsm.cmp_le_f64).op(IGAsm.not);								
							cast_type = Type.DOUBLE;
						}
						else if (op.m_type == TOK_GE && params_are_doubleish) {
							result = IGAsmOp.make_op(IGAsm.cmp_lt_f64).op(IGAsm.not);								
							cast_type = Type.DOUBLE;
						}
						
						///////////////
						// String concatenation and equality
						///////////////
						else if (op.m_type == TOK_ADD && node_type == Type.STRING) {
						
							return processStringBuilder(m, node, pw, behaviour);
							
							//result = "call_static 2 iglang.String append";
							//cast_type = Type.STRING;
							
							//explicit = true;
						}
						else if (op.m_type == TOK_EQ && string_comparison_possible) {

							pw.addFunctionDependency("iglang.String", "__ig_equals");
							result = IGAsmOp.make_op(IGAsm.call_ret1_static, "2", "iglang.String", "__ig_equals");
							cast_type = Type.STRING;
							
							explicit = true;
						}
						else if (op.m_type == TOK_NE && string_comparison_possible) {
							pw.addFunctionDependency("iglang.String", "__ig_equals");
							result =      IGAsmOp.make_op(IGAsm.call_ret1_static, "2", "iglang.String", "__ig_equals").op(IGAsm.not);
							cast_type = Type.STRING;
							
							explicit = true;
						}
						// default catch all for object equality
						else if (op.m_type == TOK_EQ || op.m_type == TOK_NE) {
							// what about function pointers really?
							if (left_type.isFunctionPointer() || 
								right_type.isFunctionPointer()) {
								result = IGAsmOp.make_op(IGAsm.cmp_eq_fp);
								
								// ensure that the appropriate casting occurs
								if (left_type == Type.NULL) { cast_type = right_type; }
								if (right_type == Type.NULL) { cast_type = left_type; }
								
								
								//result.m_comment = ("" + left_type + " " + right_type + " " + cast_type);
							}
							else
							{
								result = IGAsmOp.make_op(IGAsm.cmp_eq_obj);
							}
							
							if (op.m_type == TOK_NE) {
								result = result.op(IGAsm.not);  // note the chains are on purpose in reverse order
							}
						}
						else 
						{
							throw new RuntimeException("missed case: " + node.m_token.m_value + " " +
								node.m_type + " <= " + left.m_type + " " + right.m_type);
						}
					
						try
						{
							outputFunctionBody(m, left, pw);
							castTo(left_type,  cast_type, pw, explicit);
							
							//if (mid_result != null) {
							//	pw.println("\t" + mid_result);
							//}
										
							outputFunctionBody(m, right, pw);
							castTo(right_type, cast_type, pw, explicit);					
						}
						catch (RuntimeException ex) {
							op.printError("Weird token");
							throw ex;
						}			
						
						if (result == null) {
						
							throw new RuntimeException("What gives?");
							//pw.println("#binary " + node.m_token.m_value);
						}
						else
						{
							//pw.comment("# " + node_type + " " + left.m_type + " " + 
							//	right.m_type + " cast-type: " + cast_type);
							pw.asm(result);
						}
					}

					
					
				
					break;
				}

				case IGNode.NODE_TRINARY					: 
				{
					String l1    = getLabel();
					String lexit = getLabel();
					
					// calculate condition
					outputFunctionBody(m, node.m_children.get(0), pw);
					pw.op(IGAsm.j0_i32,l1);

					//true case
					outputFunctionBody(m, node.m_children.get(1), pw);
					pw.op(IGAsm.jmp,lexit);
					pw.label(l1);
					
					//false case
					outputFunctionBody(m, node.m_children.get(2), pw);
					pw.label(lexit);
					
			 		break;
			 	}
			 	
			 	case IGNode.NATIVE_BLOCK: {
			 		// do nothing.. this might be used for something else at a later point

					if (node.m_token.m_value.startsWith("@asm-comment")) {
						pw.comment(node.m_token.m_value.substring("@asm-comment".length()));
					}
					else if (node.m_token.m_value.equals("@asm-debug-block-start")) {
						pw.op(IGAsm.debug_block_start);
					}
					else if (node.m_token.m_value.equals("@asm-debug-block-end")) {
						pw.op(IGAsm.debug_block_end);
					}
			 		break;
			 	}

				case IGNode.THROW: {
					// TODO fill this out
					
					outputFunctionBody(m, node.m_children.get(0), pw);
					Type t = node.m_children.get(0).m_type;
					
					pw.op(IGAsm.__throw, t.toString());			// throw always indicates the type of object being thrown
					
					
					break;
				}
				
				case IGNode.WHILE: {
				
					IGNode  c1 = node.m_children.get(0);
					IGNode block = node.m_children.get(1);
					
					
					String lblHead = getLabel();
					String lblTail = getLabel();
					String lblContinue = lblHead;
					
					pushContinueBreak(lblContinue, lblTail);
					{
						// initializer
						pw.label(lblHead);			// target for "continue"
					
						// check to see if the loop should progress
						outputFunctionBody(m, c1, pw);
						pw.op(IGAsm.j0_i32, lblTail);
					
						// perform the body of the loop
						{
							outputFunctionBody(m, block, pw);
						}	

						// if the block always breaks... then
						pw.op(IGAsm.jmp, lblHead);
						pw.label(lblTail);			// target for "break"
					}
					popContinueBreak();			
									
			 		break;
				}
				
				case IGNode.DO: {

					IGNode block = node.m_children.get(0);				
					IGNode  c1 = node.m_children.get(1);
					
					String lblHead = getLabel();
					String lblContinue = getLabel();
					String lblTail = getLabel();
					
					pushContinueBreak(lblContinue, lblTail);
					{
						// initializer
						pw.label(lblHead);
					
					
						// perform the body of the loop
						{
							outputFunctionBody(m, block, pw);
						}	

						// check to see if the loop should progress
						pw.label(lblContinue);	    // target for "continue"
						outputFunctionBody(m, c1, pw);
					
						pw.op(IGAsm.jn0_i32, lblHead);		// if true go back to the top
						pw.label(lblTail);			// target for "break"
					}	
					popContinueBreak();			
									
			 		break;				
				}	


				case IGNode.NODE_FOR : { 
				
					IGNode  c0 = node.m_children.get(0);
					
					IGNode  c1 = node.m_children.get(1);
					IGNode  c2 = node.m_children.get(2);
					IGNode block = node.m_children.get(3);
					
					
					String lblHead = getLabel();
					String lblTail = getLabel();
					String lblContinue = getLabel();
					
					pushContinueBreak(lblContinue, lblTail);
					
					// initializer
					outputFunctionBody(m, c0, pw);	
					pw.label(lblHead);
					
					// check to see if the loop should progress
					outputFunctionBody(m, c1, pw);
					pw.op(IGAsm.j0_i32, lblTail);
					{
						
						outputFunctionBody(m, block, pw);
					}
					
					
					// incrementer				
					pw.label(lblContinue);
					outputFunctionBody(m, c2, pw);
					
					//if (!block.m_returns) {
					
					//System.out.println("NODE FOR CONTROL FLOW: " + c2.m_control_flow);
					
					// this was c2 before
					//if (block.m_control_flow == 0)
					{
						pw.op(IGAsm.jmp, lblHead);
					}
					//}
					pw.label(lblTail);
								
					popContinueBreak();			
									
			 		break;
			 	}

	
	
				// for ( <child-0 VAR or EXPRESSION>  <child-1 NODE_TYPE_FOREACH_IN> )
				case IGNode.NODE_TYPE_FOREACH                   : { 
				
					/*
					
						#local_load container
						push_it					// creates an iterator from the container
						
					
					*/
					
					
					// this really needs a local variable to allocate it
				
				
					String iterator_variable_name = node.m_token.getUniqueVariableId();
					int    iterator_variable_index = getLocalIndex(m, node.m_token, iterator_variable_name,
									node.m_token.m_token_index , true);
				
				
					//pw.comment("foreach");
				
					// for a standard foreach, variable will be a definition or a statement
					// for a range    foreach, variable with be a definition or an expression	
					IGNode variable  = node.m_children.get(0);
					if (variable.m_node_type == IGNode.NODE_TYPE_LOCAL_VARIABLE_DEFINITION) {
						variable = variable.m_children.get(0);
					}
				
					
					IGNode container = node.m_children.get(1).m_children.get(0);
					IGNode iterator_fn = node.m_children.get(1).m_children.get(1);
					IGNode block     = node.m_children.get(2);
					
					
					
					
					
					if (iterator_fn.m_node_type == IGNode.FOREACH_IN_RANGE)
					{
					
						//node.debug(0);
						// NOTE in this block iterator_variable_index IS NOT THAT
						// its actually the contents of the max value
					
						String lblHead = getLabel();
						String lblTail = getLabel();
						String lblContinue = getLabel();
					
						// push the labels onto the stack for where to return execution for
						// continues or breaks
						pushContinueBreak(lblContinue, lblTail);
					
						// store the min value into the iterator
						outputFunctionBody(m, container, pw, 0);
						
						//System.out.println("VNT " + variable.m_node_type);
						IGAsmOp [] toks = outputFunctionBody(m, variable, pw, 1);
						pw.asm(toks[1]);  	// store the value into the iteration variable
						pw.op(IGAsm.pop);		// pop off the result of the store
							
						// store the max value
						outputFunctionBody(m, iterator_fn.m_children.get(0), pw, 0);
						pw.op(IGAsm.local_store, intToString(iterator_variable_index));
						//pw.op(IGAsm.pop);
					
						pw.label(lblHead);
						pw.asm(toks[0]);
						
						pw.op(IGAsm.local_load, intToString(iterator_variable_index));
						pw.op(IGAsm.cmp_lt_i32);
						pw.op(IGAsm.j0_i32, lblTail);
					
					
					
					
						// now what if there is a return within the function body... well hell I guess it'd just have
						// to push off the extra iterators in the tack
						outputFunctionBody(m, block, pw);
					
						//System.out.println("NODE FOR_EACH_IN CONTROL FLOW: " + block.m_control_flow);			
						pw.label(lblContinue);
					
						//if (block.m_control_flow == 0)
						{
							boolean optimized_increment = false;
						
							if (INCREMENT_OPTIMIZATION)
							{
								if (variable.m_node_type == IGNode.LOCAL_VARIABLE_ACCESS && 
									!"super".equals(variable.m_token.m_value))
								{
									final int load_index = getLocalIndex(m, variable.m_token, false);
									final int store_index = getLocalIndex(m, variable.m_token, true);
									if (store_index >= 0 && store_index < 128 && load_index == store_index)
									{
										pw.op_comment(IGAsm.local_inc_i8, "" + store_index, "1"
											, " inc");
										optimized_increment = true;
									}
								}
							}
							
					
							if (!optimized_increment) {
								pw.asm(toks[0]);
								pw.op(IGAsm.push_i32, "1");
								pw.op(IGAsm.add_i32);
								pw.asm(toks[1]);
								pw.op(IGAsm.pop);
							}
							pw.op(IGAsm.jmp, lblHead);
						
							// TODO verify if correct
						}
						
						pw.label(lblTail);	// pop off the iterator
					}
					else
					{
						String lblHead = getLabel();
						String lblTail = getLabel();
						String lblContinue = lblHead;
					
						// push the labels onto the stack for where to return execution for
						// continues or breaks
						pushContinueBreak(lblContinue, lblTail);
					
						Type container_type = m_ctx.resolveType(container.m_type);
						Type iterator_type  = m_ctx.resolveType(iterator_fn.m_type.unwrap().m_right);		// this is incorrect
					
						// okay so really how should this behave in terms of tries and catches
						//pushFinalizer("destroy_it " + iterator_variable_index + " #" + iterator_variable_name);
						{		
							outputFunctionBody(m, container, pw);

							pw.addFunctionDependency(container_type.toString(), "iterator");
							pw.addFunctionDependency(iterator_type.toString(),  "has_next");	
							pw.addFunctionDependency(iterator_type.toString(),  "next");

							pw.op(IGAsm.call_ret1_vtable,"1", container_type.toString(), "iterator");
							pw.op(IGAsm.local_store, intToString(iterator_variable_index));

							pw.label(lblHead);
					
							// this code will be executed for each and every frame (we'll need to eventually address this)
							IGAsmOp [] toks = outputFunctionBody(m, variable, pw, 1);
						
							pw.op(IGAsm.next_it, intToString(iterator_variable_index), 
										iterator_type.toString(), "has_next",
										iterator_type.toString() ,"next",
										lblTail);	
									
							pw.asm(toks[1]);  	// store the value into the iteration variable
							pw.op(IGAsm.pop);		// pop off the result of the store
											
							// now what if there is a return within the function body... well hell I guess it'd just have
							// to push off the extra iterators in the tack
							outputFunctionBody(m, block, pw);
					
							//if (block.m_control_flow == 0) {
								pw.op(IGAsm.jmp, lblHead);
							//}
							pw.label(lblTail);	// pop off the iterator
						}
					}
					// call release.. this'll hint to the garbage collector that the object doesn't
					// have any references at this point in time
					
					//pw.println("\trelease " + iterator_variable_index + " #" + iterator_variable_name);
					
					popContinueBreak();
					
			 		break;
			 	}

	
				// in <child-0 EXPRESSION> 
				case IGNode.NODE_TYPE_FOREACH_IN                : {  throw new RuntimeException("error"); }

		
				//case IGNode.NODE_TYPE_RETURN_ID                 : { 
			// break;}
	
	
				case IGNode.NODE_BLOCK: 
				{ 
					node.calcMinLineNumber();
					
					final IGNodeList children = node.m_children;
					int children_count = children.size();
					//for (IGNode child : node.m_children)
					
					for (int child_idx = 0; child_idx < children_count; child_idx ++)
					{
						IGNode child = children.get(child_idx);
						// this would probably be the ideal place to output line #'s etc
						pw.line(child.m_min_line_number);
						outputFunctionBody(m, child, pw);
					}
					break;
				}

				case IGNode.SWITCH:
				{
					String switch_term = getLabel();
					boolean switch_term_used = false;
					
					String switch_default_lbl = getLabel();
				
					String [] switch_labels = new String[node.m_children.size()];
				
					IGNode clause_block = node.m_children.get(0);
					outputFunctionBody(m, clause_block, pw);
					
					Type dst_type = node.m_children.get(0).m_type;
					Type cmp_type = dst_type;
					
					if (cmp_type.isEnum()) {
						cmp_type = cmp_type.getEnumPrimitive();
					}
					
					for (int i = 1; i < node.m_children.size(); i++) {
						IGNode n = node.m_children.get(i);
						if (n.m_node_type != IGNode.SWITCH_CASE) {
							continue;
						}	
						switch_labels[i] = getLabel();
						
						pw.op(IGAsm.dup);
						outputFunctionBody(m, n.m_children.get(0), pw);
						castTo(n.m_children.get(0).m_type, dst_type, pw);
						if (cmp_type == Type.INT || cmp_type == Type.BOOL) 	{ pw.op(IGAsm.cmp_eq_i32); }
						else if (cmp_type == Type.DOUBLE) 					{ pw.op(IGAsm.cmp_eq_f64); }
						else {
							throw new RuntimeException("Currently unsupported switch type: " + dst_type);
						}
						pw.op(IGAsm.jn0_i32, switch_labels[i]);
					}
					
					pw.op(IGAsm.pop);
					pw.op(IGAsm.jmp, switch_default_lbl);
				
					// print out each individual case block
					for (int i = 1; i < node.m_children.size(); i++) {
						IGNode n = node.m_children.get(i);
						if (n.m_node_type != IGNode.SWITCH_CASE) {
							continue;
						}					
						
						pw.label(switch_labels[i]);
						pw.op(IGAsm.pop);					// pop off the unwanted value
						
						IGNode case_node = n.m_children.get(1);
						outputFunctionBody(m, case_node, pw);
						
						if ((case_node.m_control_flow & IGNode.CF_RETURN) == 0) {
							switch_term_used = true;
							pw.op(IGAsm.jmp, switch_term);
						}
					}
					
					// print out each individual switch default block
					pw.label(switch_default_lbl);
					
					for (int i = 1; i < node.m_children.size(); i++) {
						IGNode n = node.m_children.get(i);
						if (n.m_node_type != IGNode.SWITCH_DEFAULT) {
							continue;
						}

						IGNode default_node = n.m_children.get(0);
						outputFunctionBody(m, default_node, pw);
						
						//if (!default_node.m_returns) {
						//	pw.op(IGAsm.jmp, switch_term);
						//}
					}					
					
					if (switch_term_used) 
					{
						pw.label(switch_term);	
					}
					
					break;
				}
	
				case IGNode.NODE_IF	: 
				{ 
					boolean terminate_if_label_used = false;
					String terminate_if_label = getLabel("TI");
					
					
					String next_if_label      = getLabel("MI");
					
					//pw.println("\tpos " + node.m_token);
					//pw.comment("#if");
					
					IGNode if_condition = node.m_children.get(0);
					outputFunctionBody(m, if_condition, pw);
					pw.op(IGAsm.j0_i32, next_if_label);
					
					IGNode if_block     = node.m_children.get(1);
					outputFunctionBody(m, if_block, pw);
					
					if (node.m_children.size() >= 3 && if_block.m_control_flow == 0) {
						terminate_if_label_used = true;
						pw.op(IGAsm.jmp, terminate_if_label);
					}
	
					for (int i = 2; i < node.m_children.size(); i++)
					{
						IGNode if_sub_node = node.m_children.get(i);
						if (if_sub_node.m_node_type == IGNode.NODE_ELSE_IF) {
							//pw.comment("#else if");
							
							pw.label(next_if_label);
							next_if_label = getLabel("NI");
							
							IGNode if_sub_condition = if_sub_node.m_children.get(0);
							outputFunctionBody(m, if_sub_condition, pw);
							pw.op(IGAsm.j0_i32, next_if_label);
							
							IGNode if_sub_block     = if_sub_node.m_children.get(1);
							outputFunctionBody(m, if_sub_block, pw);
							
							// see 2072 in UIWidget render
							// skip the terminate if we are the last element
							if (if_sub_block.m_control_flow == 0 && i != node.m_children.size() - 1) 
							{
								// this jump is also not necessary if not else clause is used
								terminate_if_label_used = true;
								pw.op(IGAsm.jmp, terminate_if_label);
							}
						}
						else
						{
							//pw.comment("#else");
							
							pw.label(next_if_label);
							next_if_label = null;
							
							IGNode if_sub_block     = if_sub_node.m_children.get(0);
							outputFunctionBody(m, if_sub_block, pw);
							
							/*
							// this shouldn't ever be needed
							if (if_sub_block.m_control_flow == 0) 
							{
								terminate_if_label_used = true;
								pw.op(IGAsm.jmp, terminate_if_label);
							}
							*/
						}
					}
					
					if (next_if_label != null) {
						pw.label(next_if_label);
					}
									
					if (terminate_if_label_used) { //node.m_children.size() >= 3) {
						pw.label(terminate_if_label);
					}
					//else {
					//	
					//}
					//pw.comment("endif");
					
				
			 		break;
				 }

				case IGNode.NODE_ELSE_IF:                   { throw new RuntimeException("fail");  }
				case IGNode.NODE_ELSE:                      { throw new RuntimeException("fail");   }
				case IGNode.NODE_TYPE_STATIC_VARIABLE_NAME: { throw new RuntimeException("fail");  }
				case IGNode.NODE_TYPE_VARIABLE_NAME:        { throw new RuntimeException("fail");  }
			 
			 
	
	
	
				// wraps inline ints, Strings, booleans, nulls
				case IGNode.NODE_VALUE							: { 
					Token t = node.m_token;
					
					if (t.m_type == TOK_TRUE) {		
						pw.op(IGAsm.push_i32,"1");
					}
					else if (t.m_type == TOK_FALSE) {
						pw.op(IGAsm.push_i32,"0");
					}
					else if (t.m_type == TOK_NULL) {
						pw.op(IGAsm.push_obj_null);
					}					
					else if (t.m_type == IGLexer.LEXER_INT) {
						pw.op(IGAsm.push_i32, t.m_value);
					}
					else if (t.m_type == IGLexer.LEXER_FLOAT) {
						pw.op(IGAsm.push_f64,t.m_value);
					}
					else if (t.m_type == IGLexer.LEXER_STRING) {
					
						pw.addModuleDependency("iglang.String");

						String s     = t.m_value;
						int    s_len = s.length();
						
						int found_escape = -1;
						for (int ci = 0; ci < s_len; ci++) {
							char c = s.charAt(ci);
							if (c == '\\') {
								found_escape = ci;
								break;
							}
						}
						// i guess before the escaping procedure took care of this?
						if (-1 == found_escape) {
							pw.op(IGAsm.push_string, t.m_value);
						}
						else 
						{
							//System.out.println("processing: '" + s + "'");
							StringBuilder result = new StringBuilder(s_len);	// initial cap?

							int s0 = 0;
								// CharSequence
							for (int ci = found_escape; ci < s_len; ci++) {
								char c0 = s.charAt(ci);
								if (c0 != '\\') {
									continue;
									
								}

								result.append(s, s0, ci);
								char c1 = s.charAt(ci + 1);
								boolean is_unicode_escape = false;
								
								switch (c1) {
									case 'n' :  result.append('\n'); break;
									case 'r' :  result.append('\r'); break;
									case 't' :  result.append('\t'); break;
									case '\\' : result.append('\\'); break;
									case '"'  : result.append('"'); break;
									case '\'' : result.append('\''); break;
									case 'u'  : 	
										is_unicode_escape = true;
										break;
										
									default: {
										throw new RuntimeException("Unsupported escape char: " + c1);
									}
								}
								
								if (is_unicode_escape)
								{
									char u0 = s.charAt(ci + 2);
									char u1 = s.charAt(ci + 3);
									char u2 = s.charAt(ci + 4);
									char u3 = s.charAt(ci + 5);
									
									
									int v = (fromHex(u0) << 12) | (fromHex(u1) << 8) | (fromHex(u2) << 4) | (fromHex(u3) << 0);
									
									result.append((char)v);
									
									
									s0 = ci + 2 + 4;
									ci = ci + 1 + 4;
								}
								else
								{
									s0 = ci + 2;
									ci++;			// skip the next character
								}
							}
							
							if (s0 < s_len) {
								result.append(s, s0, s_len);
							}
							pw.op(IGAsm.push_string, result.toString());
						}
					}
					else if (t.m_type == IGLexer.LEXER_HEX) {
						long value = Long.decode(t.m_value);
						pw.op(IGAsm.push_i32, "" + value);
					}
					else if (t.m_type == IGLexer.LEXER_CHAR) {
						pw.op_comment(IGAsm.push_i32, "" + util.Decoder.escapedCharToInt(t.m_value), /*comment*/ "'" + t.m_value + "'");
					}
					else
					{
						t.printError("What?");
						throw new RuntimeException(" IGNode.NODE_VALUE fail: " + t.m_type + " " + t.m_value);
					}
			 		break;
			 	}

	
				case IGNode.NODE_PARAMETER_LIST					: { 
				
					final IGNodeList children = node.m_children;
					int children_count = children.size();
					
					for (int child_idx = 0;  child_idx < children_count; child_idx++) {
			 		//for (IGNode child : node.m_children)
					//{
							
						outputFunctionBody(m, children.get(child_idx), pw);
					}
					break;
				}

				case IGNode.PARAMETER_CAST: {
					outputCast(m, node.m_children.get(0), node.m_type, pw);
					break;
				}
				
				case IGNode.ARRAY_INITIALIZER : 
				{
					Type t = m_ctx.resolveType(node.m_children.get(0).m_type);
					int storage_class = t.m_left.getStorageClass();
						
					outputFunctionBody(m, node.getChild(0), pw);
						
					final IGNodeList children = node.getChild(1).m_children;
					int children_count = children.size();	
						
					//for (IGNode child : node.getChild(1).m_children)
					for (int child_idx = 0; child_idx < children_count; child_idx++)
					{	
						IGNode child = children.get(child_idx);
						if (child.m_node_type == IGNode.ARRAY_INITIALIZER_ITEM) 
						{
							int initializer_index = child.m_user_index;
							
							pw.op(IGAsm.dup);		// duplicate the pointer
							pw.op(IGAsm.push_i32, Integer.toString(initializer_index));
							outputFunctionBody(m, child.m_children.get(0), pw);
							pw.op(IGAsm.array_store, t.toString(), intToString(storage_class));
						}
					}
					break;
				}

				case IGNode.VECTOR_INITIALIZER : 
				{
					Type t = m_ctx.resolveType(node.m_children.get(0).m_type);
					pw.addFunctionDependency(t.toString(), "push");

					int storage_class = t.m_left.getStorageClass();
						
					outputFunctionBody(m, node.getChild(0), pw);	
						
					final IGNodeList children = node.getChild(1).m_children;
					int children_count = children.size();	
						
					//for (IGNode child : node.getChild(1).m_children)
					for (int child_idx = 0; child_idx < children_count; child_idx++)
					{	
						IGNode child = children.get(child_idx);
						if (child.m_node_type == IGNode.VECTOR_INITIALIZER_ITEM) 
						{
							int initializer_index = child.m_user_index;
							
							pw.op(IGAsm.dup);		// duplicate the pointer
							outputFunctionBody(m, child.m_children.get(0), pw);
							pw.op(IGAsm.call_vtable, "2", t.toString(), "push");
						}
					}
					break;
				}
				
				case IGNode.MAP_INITIALIZER: 
				{
					Type t = m_ctx.resolveType(node.m_children.get(0).m_type);
					pw.addFunctionDependency(t.toString(), "<set[]>");

					int storage_class = t.m_left.getStorageClass();
						
					outputFunctionBody(m, node.getChild(0), pw);	
						
					final IGNodeList children = node.getChild(1).m_children;
					int children_count = children.size();	
						
					//for (IGNode child : node.getChild(1).m_children)
					for (int child_idx = 0; child_idx < children_count; child_idx++)
					{	
						IGNode child = children.get(child_idx);
						if (child.m_node_type == IGNode.MAP_INITIALIZER_ITEM) 
						{
							int initializer_index = child.m_user_index;
							
							pw.op(IGAsm.dup);		// duplicate the pointer
							outputFunctionBody(m, child.m_children.get(0), pw);
							// node 1 is ignored
							outputFunctionBody(m, child.m_children.get(2), pw);
							pw.op(IGAsm.call_vtable,"3",t.toString(),"<set[]>");
						}
					}
					break;
				}
	
				//  : TYPE  following a function declaration
				case IGNode.FUNCTION_RETURN_TYPE				: {  throw new RuntimeException("fail"); }

	


	
				case IGNode.PACKAGE								: {  throw new RuntimeException("fail"); }
				case IGNode.IMPORT								: {  throw new RuntimeException("fail"); }
				case IGNode.SOURCE								: {  throw new RuntimeException("fail"); }
				case IGNode.CLASS								: {  throw new RuntimeException("fail"); }
				case IGNode.CLASS_VARIABLE						: {  throw new RuntimeException("fail"); }
				case IGNode.CLASS_STATIC_VARIABLE				: {  throw new RuntimeException("fail"); }
				case IGNode.CLASS_FUNCTION						: {  throw new RuntimeException("fail"); }
				case IGNode.CLASS_STATIC_FUNCTION				: {  throw new RuntimeException("fail"); }


				case IGNode.TRY: 
				{
					int catch_count = node.m_children.size() -1;
					String []labels= new String[catch_count];

					Type   []types  = new Type[catch_count];

					String lblTerm = getLabel();
					String lblTryStart = getLabel();
					
					
					
					//System.err.println("TRY DEBUG");
					
					// mark that all exceptions between this instruction
					// and the mentioned label, should redirect to the label in question
					pw.op_comment(IGAsm.try_push, lblTryStart, /*comment */ "meta instruction");
					
			
					for (int i = 0; i < labels.length; i++) {
						labels[i] = getLabel();
						
						
						IGNode catch_node = node.m_children.get(i+1);
						
						// okay so if its a var statement it doesn't technically have a return type
						// thus we WILL need to drill down lower in the heirarchy to determine typing
						types[i] = catch_node.m_children.get(0).m_type;
						
						//catch_node.m_children.get(0).debug();
					}
				

					outputFunctionBody(m, node.m_children.get(0), pw);
					
					pw.op(IGAsm.jmp, lblTerm);
					
					// the catch block disambiguator
					{
						pw.comment("catch block disambiguator");
						pw.label(lblTryStart);
						pw.op(IGAsm.catch_entry);
						 
						for (int i = 0; i < catch_count; i++) 
						{
							pw.op(IGAsm.dup);
							pw.op(IGAsm.instance_of, types[i].toString());
							pw.op(IGAsm.jn0_i32, labels[i]);
						}
						pw.op(IGAsm.__throw);		// rethrow the exception in the case we found nothing
					}
					
					// process each catch block
					for (int i = 0; i < catch_count; i++) 
					{
						pw.label(labels[i]);
						IGNode catch_unit =	node.m_children.get(i + 1);
						
						// get ready the variable for the try catch
						IGAsmOp [] catch_expr = outputFunctionBody(m, catch_unit.m_children.get(0), pw, 1);
						pw.asm(catch_expr[1]);
						pw.op(IGAsm.pop);
							
						// prepare the body of the try catch						
						outputFunctionBody(m, catch_unit.m_children.get(1), pw);
	
						// if its not the last unit then jump to the end of the catch segment					
						if (i != catch_count - 1) {
							pw.op(IGAsm.jmp, lblTerm);
						}
					}
					
					// try_pop pops all try blocks whose destination is prior to this 
					pw.label(lblTerm);
				
					break;
				}
				
				case IGNode.NODE_CATCH							: {  throw new RuntimeException("fail"); }

				case IGNode.NODE_FUNCTION_SIGNATURE				: { 
			 break;}

				case IGNode.FUNCTION_PARAMETERS             : {
					break;
				}

				case IGNode.NODE_RETURN							: 
				{ 
					 if ("<constructor>".equals(m.m_name.m_value) || m.m_return_type == Type.get("void")) {
					 	pw.op(IGAsm.ret0);
					 }
					 else {
						outputFunctionBody(m, node.m_children.get(0), pw);
						
						int storage_class = m_ctx.resolveType(m.m_return_type).getStorageClass();
						castTo(node.m_children.get(0).m_type, m.m_return_type, pw);
						
						if (storage_class == IGAsmStorageClass.OBJECT) {
							pw.op(IGAsm.ret_obj);
						}
						else if (storage_class ==  IGAsmStorageClass.FUNC_PTR) {
							pw.op(IGAsm.ret_funcptr);
						}
						else {
					 		pw.op(IGAsm.ret1);
					 	}
					 }
					 break; 
				}
				
				case IGNode.FLOW_CHANGE:
				{
					if (node.m_token.m_type == TOK_CONTINUE) {
						pw.op_comment(IGAsm.jmp, getContinueLabel(), /*comment */ "continue");
					}
					else if (node.m_token.m_type == TOK_BREAK) {
						pw.op_comment(IGAsm.jmp, getBreakLabel(), /*comment*/ "break");					
					}
					else
					{
						throw new RuntimeException("Unknown flow change event");
					}
					break;
				}

				case IGNode.NODE_ENUM_MEMBER					: { break;}
				case IGNode.NODE_ENUM							: { break;}
				case IGNode.NODE_INTERFACE						: {  break;}
				//case IGNode.SWITCH							    : { break; }
	
				case IGNode.SCOPE_CHAIN_START					: { 
					 break;
			 	}
 
 			default:
 				//this seems to just be for @cpp calls etc...
 				//node.m_token.printError("Where am I?");
 				System.err.println("Unknown: " + node.m_node_type);
 				throw new RuntimeException("ASM Failure");
 				//break;
		}
		
		return empty2();
	}


	//////////////////////////////////////////////////////
	// Main entry point
	/////////////////////////////////////////////


	public IGContext m_ctx = null;
	

	
	IGTemplateUtil m_template_util = new IGTemplateUtil();


	
	public void configContext(IGContext ctx)
	{
		//Vector<Type> templated_types = new Vector<Type>();
		//Type.getTemplatedTypes(templated_types);
		ctx.clearTypeReplace();
		ctx.setCollapseObjectTemplateTypes(true);
		
		/*
		for (Type t : templated_types) {
			
			Type left  = t.m_left;
			Type right = t.m_right;
			
			if (!t.containsTemplatingParameters())
			{
				if (left.isObjectInstance()) {
					left = Type.get("iglang.Object");
				}
				if (right != null && right.isObjectInstance()) {
					right = Type.get("iglang.Object");
				}
			
				Type alt = Type.get(t.m_primary, left, right);
				alt.m_scope = t.m_scope;
			
				ctx.addTypeReplace(t,alt );
			}
		}
		*/
		
	}
	


	private int m_asm_writer_mode = IGAsmDocument.MODE_WRITE_HEADERS;

	/**
		THIS IS THE MAIN ENTRY POINT
	*/

	public static class Config 
	{
		public String dst_extension;
	}
	
	public  Vector<IGAsmTarget> run(
		IGPackage root, 
		Vector<IGSource> sources, 
		int asm_writer_mode,
		Config config)
	{	
	
		m_asm_writer_mode = asm_writer_mode;
		
		// avoid weird load patterns
		Characterization.touch();
		
		try
		{
		
			Vector<IGAsmTarget> to_run = new Vector<IGAsmTarget>();
		
			// process all non templated classes
			for (IGSource s : sources)
			{
				if (s.isTemplatedClass()) 
				{
					continue;
				}
			



				m_ctx = new IGContext();
				configContext(m_ctx);
				
				try
				{
					to_run.add(processSource(s, config));
				}
				catch (RuntimeException ex) {
					System.out.println("Assembly compilation failed in: " + s);
					throw ex;
				}
			}
			
			
			// start working through templated variants
			while (m_template_util.augmentTemplateVariants())
			{		
				for (IGSource s : sources)
				{
					if (!s.isTemplatedClass()) 
					{
						continue;
					}
				
				
					boolean isTemplated = false;
				
				
					Type idt = s.getInstanceDataType();
				
					// there's literally no reason to ever output this source file
					//if (!template_variants.containsKey(idt.m_primary)) {
					//}
				
					IGTemplateUtil.IGTemplateVariant tv = m_template_util.getVariant(idt.m_primary);
					if (tv == null) { continue; }
					
					int template_count = tv.m_lefts.size();
				
					//System.err.println("Templated class output");
				
			
			
					for (int template_index = 0; template_index < template_count; template_index++)
					{
						m_ctx = new IGContext();
						configContext(m_ctx);
					
						Type l = tv.m_lefts.get(template_index);
						Type r = tv.m_rights.get(template_index);
				
						for (int k = 0; k < s.m_template_types.size(); k++) {
							if (k == 0) {
								m_ctx.addTypeReplace(s.m_template_types.get(0), l);
							}
							else if (k == 1)
							{
								m_ctx.addTypeReplace(s.m_template_types.get(1), r);
							}
						}
				
						
						to_run.add(processSource(s, config));
					}
					
					// clear out the processed list of lefts and rights
					tv.mark();
				}
			}
			
			////////////////////////////////////////////////////////////////
			// Convert asm files to binary asm files
			
			
			
			return to_run;
			
		}
		catch (IOException ex)
		{
			ex.printStackTrace();
			return null;
		}
	}
	
	private int fromHex(char c) {
		if ('0' <= c && c <= '9') {
			return c - '0';
		}
		else if ('a' <= c && c <= 'f') {
			return 10 + (c - 'a');
		}
		else if ('A' <= c && c <= 'F') {
			return 10 + (c - 'A');
		}
		return 0;
	}
	
	public void run2(Vector<IGAsmTarget> to_run) {
	
		try
		{
			convertASMToVM(to_run);
		}
		catch (IOException ex)
		{
			ex.printStackTrace();
		}
	}
	
	
	private static String [] s_int_to_string = new String[256];
	
	static {
		for (int i = 0; i < 256; i++) {
			s_int_to_string[i] = Integer.toString(i);
		}
	}
	
	private static final String intToString(int idx) 
	{
		return s_int_to_string[idx];
	}
	
	
	
	
	
	
	static Object s_threads_observer = new Object();
	static int    s_threads_active = 0;
	static int    s_threads_job_index = 0;
	static Vector<IGAsmTarget> s_threads_jobs = null;
	
	static class Runner extends Thread {
		
		//IGAsmTarget                    m_target;
		Hashtable<String, IGAsmTarget> m_targets;
					
		public Runner(Hashtable<String, IGAsmTarget> targets) {
			//m_target = target;
			m_targets = targets;
		
		}
		
		public void run() 
		{
			while (s_threads_job_index > 0)
			{
				IGAsmTarget target = null;
				synchronized(s_threads_jobs) 
				{
					if (s_threads_job_index <= 0) {
						break;
					}
					
					s_threads_job_index --;
					target = s_threads_jobs.get(s_threads_job_index);
					
				}
				
				
				
				try
				{
					//IGAsmTarget target = m_target;
					Hashtable<String, IGAsmTarget> targets = m_targets;
		
					target.writeAsmFile(targets);

					//TextReader src = Util.getDestFileAsTextReader(target.m_source, target.m_src_path);
					File dst = Util.getDestFile(target.m_source, target.m_dst_path);	
		
					//System.out.println("convert ASM to VM: " + dst.getAbsolutePath());
		
					IGAssembler.process(target.m_writer, target.m_src_path, dst);
				}
				catch (IOException ex) {
					ex.printStackTrace();
					System.out.println("Failed in thread");
				}
		

			}
			
			synchronized(s_threads_observer)
			{
				s_threads_active--;
				s_threads_observer.notifyAll();
			}
		}
	}
	
	private void convertASMToVM(Vector<IGAsmTarget> to_run) throws IOException
	{
		Hashtable<String, IGAsmTarget> targets = new Hashtable<String, IGAsmTarget>();
		for (IGAsmTarget target : to_run) {
			targets.put(target.m_full_path, target);
		}
	
		s_threads_active = 4;	//to_run.size();
		s_threads_jobs = to_run;
		s_threads_job_index = to_run.size();
		
		new Runner( targets).start();
		new Runner( targets).start();
		new Runner( targets).start();
		new Runner( targets).start();
		
		//for (IGAsmTarget target : to_run)
		{	
			
			//new Runner(target, targets).start();
			
			/*
		
			target.writeAsmFile(targets);

			//TextReader src = Util.getDestFileAsTextReader(target.m_source, target.m_src_path);
			File dst = Util.getDestFile(target.m_source, target.m_dst_path);	
			
			//System.out.println("convert ASM to VM: " + dst.getAbsolutePath());
			
			IGAssembler.process(target.m_writer, target.m_src_path, dst);
			*/
		}
		
		while (s_threads_active > 0) {
			try
			{
				synchronized(s_threads_observer) {
					s_threads_observer.wait();
				}
			}
			catch (InterruptedException ex) {
				
			}
		}
		
	}
	
	/*
	 * Process a source file.  The particulars of the type remapping will be set up prior to calling this
	 * 
	 */
	
	public IGAsmTarget processSource(IGSource s, Config config) throws IOException
	{
							
		resetLabelCounter();
	
		String extension = generateExtension(s);
	
	
		//System.out.println("#### processing: " + s.m_expected_component);	//+ "(" + template_index + ")");
	
		IGAsmDocument pw = new IGAsmDocument(m_asm_writer_mode);	
	
		if (s.m_type == IGScopeItem.SCOPE_ITEM_ENUM)
		{
			pw.metaType("enum", m_ctx.getType(s).toString());
		}
		else if (s.m_type == IGScopeItem.SCOPE_ITEM_INTERFACE)
		{
			pw.metaType("interface", m_ctx.getType(s).toString());
		}
		else if (s.m_type == IGScopeItem.SCOPE_ITEM_CLASS) 
		{
			pw.metaType("class", m_ctx.getType(s).toString());	
			
			{
				StringBuilder sb = new StringBuilder();
				//sb.append("full-extends ");
				IGSource s2 = s;
				boolean first = true;
				while (s2 != null && m_ctx.getType(s2) != Type.get("iglang.Object")) 
				{
					if (s2.m_class_extends != null) 
					{
						if (!first) {
							sb.append(",");
						}
						first = false;
						sb.append(s2.m_class_extends);
						
						s2 = (IGSource)s2.m_class_extends.m_scope;
					}
					else {
						break;
					}
				}
				
				pw.meta("full-extends", sb.toString());
			}
		
			if (m_ctx.getType(s) != Type.get("iglang.Object")) 
			{
				if (s.m_class_extends != null) 
				{
					pw.metaExtends("extends", s.m_class_extends.toString());
				}
			}
			
			for (Type t : s.m_class_implements) {
				pw.meta("implements", t.toString());
			}			
			
			{
				StringBuilder sb = new StringBuilder();
				//sb.append("full-implements ");
				IGSource s2 = s;
				boolean first = true;
				while (s2 != null) 
				{
					if (s2.m_class_extends != null) 
					{
						for (Type t : s2.m_class_implements) 
						{
							if (!first) {
								sb.append(",");
							}
							first = false;
							sb.append(t);
						}
						
						s2 = (IGSource)s2.m_class_extends.m_scope;
					}
					else {
						break;
					}
				}
				pw.meta("full-implements", sb.toString());
			}				
		}
	
		boolean found_constructor = false;
	
		for (int i = 0; i < 4; i++)
		{
			
			boolean static_variable  = i == 0;
			boolean variable_obj 	 = i == 1;
			boolean variable_pri     = i == 2;
			boolean static_function  = i == 3;
			boolean function 		 = i == 3;
	
			// TODO resolve this differently
			if (s.m_type == IGScopeItem.SCOPE_ITEM_INTERFACE) {
				function       = false;
				static_function = false;
			}
	
			for (IGMember m : s.m_members) 
			{
				if (m.hasModifier(IGMember.STUB)) {
					continue;
				}
		
		
				String permissions = "private";
				if (m.hasModifier(IGMember.PUBLIC)) {
					permissions = "public";
				}
		
				if (m.m_type == IGMember.TYPE_VAR) {
				

					String [] sb = null;
					int    storage_class = -1;
				
					// a value of 0 will default to whatever initialized means for a data type
					String initial_value = "0";

					if (m.hasModifier(IGMember.STATIC)) {
						if (!static_variable) { continue; }
						sb = new String[7];
						sb[0] = ("static-var");
						
						
						storage_class = m_ctx.resolveType(m.m_return_type).getStorageClass();

						IGValueRange vr = null;
						if (m.hasModifier(IGMember.ENUM_VALUE)) 
						{
							vr = m.m_return_value_range;
						}
						else
						{
							// see if this is an assignment statement
							IGNode node = m.m_node.findRightMostWithNodeAndTokenType(IGNode.NODE_BINARY, TOK_ASSIGN);
							if (node != null) {
								vr = node.m_value_range;
							}
						}

						if (vr != null && vr.isConcrete()) {
							
							if (vr.m_type == IGValueRange.TYPE_INT) {
							
								int val = vr.intValue();
								initial_value = Integer.toString(val);
							}
							else if (vr.m_type == IGValueRange.TYPE_DOUBLE) {
								double val = vr.doubleValue();
								initial_value = Double.toString(val);
							}	
						}
						else {
							initial_value = "null or not concrete: " + vr;
						}

						// okay weird... none of these are deemded to be concrete
						initial_value = "0";
					}
					else
					{	
						if (!variable_obj && !variable_pri) { 
							continue; 
						}
						
						storage_class = m_ctx.resolveType(m.m_return_type).getStorageClass();
						
						if ((storage_class == Type.STORAGE_OBJ || storage_class == Type.STORAGE_FUNC_PTR) && variable_pri) {
							continue;
						}
						
						if ((storage_class != Type.STORAGE_OBJ && storage_class != Type.STORAGE_FUNC_PTR) && variable_obj) {
							continue;
						}
						
						sb = new String[7];
						sb[0] = ("var") ;
					}
				
					// TEMPLATE WORK NEEDED
					sb[1] = (permissions);
					sb[2] = (m.m_name.m_value);
					sb[3] = (m_ctx.resolveTypeDoNotCollapse(m.m_return_type).toTypeString());
					sb[4] = (m_ctx.resolveType(m.m_return_type).toTypeString());
					sb[5] = (intToString(storage_class));
					sb[6] = initial_value;
					
					pw.header(sb);
				}
				else if (m.m_type == IGMember.TYPE_FUNCTION)
				{
					String [] sb = null;
				
					if (m.hasModifier(IGMember.STATIC)) {	
						if (!static_function) { continue; }
						sb = new String[7];
						sb[0] = ("static-function");					
						sb[6] = "";
					}
					else
					{
						if (!function) { continue; }
						sb = new String[7];
						
						if (s.getScopeType() == IGScopeItem.SCOPE_ITEM_ENUM) 
						{
							sb[0] = ("static-function");
							sb[6] = "FINAL";	
						}
						else {
							sb[0] = ("function");	
							sb[6] = "";	
						}
											
					}
				
					sb[1] = (permissions);
					sb[2] = (m.m_name.m_value);
					sb[3] = (m.getMinParameterCount() + ".." + m.getMaxParameterCount());
					sb[4] = (intToString(m.getConservativeLocalVariableCount()));
					
					if ("<constructor>".equals(m.m_name.m_value)) {
						sb[5] = "0";
					}
					else
					{
						sb[5] = (((m.m_return_type == Type.get("void")) ? "0" : "1"));
					}
					
					if (!m.hasModifier(IGMember.FINAL)) {	
						sb[6] = "FINAL";
					}
					
					pw.header(sb);
				
					int idx = 0;
					if (!m.hasModifier(IGMember.STATIC)) {	
						pw.op_comment(IGAsm.param_undef,"0", /*comment*/  "this");
						idx ++;
					}
				
					// set the default values for all parameters
					// all values have a default of binary 0 unless otherwise specified
					
					ArrayList<IGVariable> parameters = m.m_parameters;
					for (int parameter_idx = 0; parameter_idx < parameters.size(); parameter_idx++) {
						IGVariable p = parameters.get(parameter_idx);
					
						if (p.m_has_default)
						{
								IGValueRange vr = p.getDefaultValueRange();
								
								if (vr.m_type == IGValueRange.TYPE_INT) {
									pw.op_comment(IGAsm.param_i32, intToString(idx), ""+vr.intValue(),    /*comment*/ p.m_name.m_value + " ");	
								}
								else if (vr.m_type == IGValueRange.TYPE_DOUBLE) {
									pw.op_comment(IGAsm.param_f64, intToString(idx), ""+vr.doubleValue(), /*comment*/ p.m_name.m_value + " ");		
								}
								else if (vr.m_type == IGValueRange.TYPE_POINTER_NULL) {
									pw.op_comment(IGAsm.param_object, intToString(idx), "0",                 /*comment*/ p.m_name.m_value + " ");
								}
								else if (vr.m_type == IGValueRange.TYPE_DELEGATE_NULL) {
									pw.op_comment(IGAsm.param_func_ptr, intToString(idx), "0",                 /*comment*/ p.m_name.m_value + " ");
								}
								else if (vr.m_type == IGValueRange.TYPE_BOOL_FALSE) {
									pw.op_comment(IGAsm.param_i32, intToString(idx), "0",                 /*comment*/ p.m_name.m_value + " ");	
								}
								else if (vr.m_type == IGValueRange.TYPE_BOOL_TRUE) {
									pw.op_comment(IGAsm.param_i32, intToString(idx), "1",                 /*comment*/ p.m_name.m_value + " ");	
								}
								else {
									throw new RuntimeException("Invalid parameter value range");
								}
						}
						else
						{
								pw.op_comment(IGAsm.param_undef, intToString(idx),  "0", /*comment*/p.m_name.m_value);
						}
						idx ++;
					}							
					//pw.comment("###############################");
				
				
				
					IGNode body_node = m.m_node.getChild(1);
					
					if (m.m_name.m_value.equals("<constructor>")) 
					{
						found_constructor = true;
					
						// todo determine how this interplays with user self called constructors
						// ????
						if (s.m_class_extends != null && !m.m_super_constructor_called) 
						{
							// TODO this needs to be modified
							// the previous layer really NEEDS to error if there are more
							// than 1 parameters to a constructor and it isn't explicitly called
						
							String super_type = m_ctx.getType(s).toString();

							pw.addFunctionDependency(super_type, "<constructor>");

							pw.op_comment(IGAsm.local_load,"0", /*comment*/ "this");
							pw.op(IGAsm.call_super, "1", super_type, "<constructor>");
						}									
					
						boolean found_possible_state_disruption = false;
					
						// process all member variables
						for (IGMember source_member : s.m_members) 
						{
							if (source_member.m_type != IGMember.TYPE_VAR ||
								source_member.hasModifier(IGMember.STATIC)) {
							
								continue;	
							}
							
							
							// figure out if the member variable has an initial value
							IGNode n = source_member.m_node.findRightMostWithNodeAndTokenType(IGNode.NODE_BINARY, TOK_ASSIGN);
								
							// attempt to write out an abbreviated version
							// ie.   x = 0 
							// or    y = null  
							// DO NOT EVER NEED TO BE EXECUTED	
							if (n != null && !found_possible_state_disruption) 
							{
								
							
							}	
		
							if (n != null) 
							{
								Type t = n.m_children.get(1).m_type;	
								
								if (n.m_children.get(1).m_type.isFunctionWrapper()) {
									outputFunctionWrapperCast(source_member,n.m_children.get(1), source_member.m_return_type, pw);
								}		
								else
								{
									outputFunctionBody(source_member, n.m_children.get(1), pw);
									castTo(t, source_member.m_return_type, pw, true);
								}

								pw.addMemberDependency( m_ctx.getType(s).toString(),  source_member.m_name.m_value);
								
								pw.op(IGAsm.dup);
								pw.op(IGAsm.this_store,  m_ctx.getType(s).toString(),  source_member.m_name.m_value);
								pw.op(IGAsm.pop);
							}
						}		
					}
				
					//System.out.println("" + m.m_name);
					outputFunctionBody(m, body_node, pw);
				
					if (m.m_return_type == Type.get("void") || m.m_name.m_value.equals("<constructor>")) {
						pw.op(IGAsm.ret0);
					}
								 
					pw.op(IGAsm.END);
				}
			}
		}

		if (s.getScopeType() == IGScopeItem.SCOPE_ITEM_CLASS) 
		{
			if (!found_constructor) 
			{
				// okay so this actually seems to work correctly for when
				// there is no constructor found for a particular class
				
				IGMember base_constructor = s.getFirstConstructor();
		
				int parameter_count = base_constructor.getMaxParameterCount();
				int min_param_count = base_constructor.getMinParameterCount();
		
				ArrayList<String> sb = new ArrayList<String>();
				sb.add("function");
				sb.add("public");
				sb.add("<constructor>");
				sb.add(min_param_count + ".." + parameter_count);
				sb.add(intToString(parameter_count));
				sb.add("0");
				sb.add("");
				pw.header(sb);
		
				

				pw.op_comment(IGAsm.param_undef,"0", /*comment*/ "this");	
				
				/*		
				for (int i = 1; i < parameter_count; i++) {
					pw.op(IGAsm.param_undef, intToString(i));	
				}
				*/
				
				ArrayList<IGVariable> parameters = base_constructor.m_parameters;
				for (int parameter_idx = 0; parameter_idx < parameters.size(); parameter_idx++) {
				
					int idx = parameter_idx + 1;
					IGVariable p = parameters.get(parameter_idx);
				
					if (p.m_has_default)
					{
							IGValueRange vr = p.getDefaultValueRange();
							
							if (vr.m_type == IGValueRange.TYPE_INT) {
								pw.op_comment(IGAsm.param_i32, intToString(idx), ""+vr.intValue(),    /*comment*/ p.m_name.m_value + " ");	
							}
							else if (vr.m_type == IGValueRange.TYPE_DOUBLE) {
								pw.op_comment(IGAsm.param_f64, intToString(idx), ""+vr.doubleValue(), /*comment*/ p.m_name.m_value + " ");		
							}
							else if (vr.m_type == IGValueRange.TYPE_POINTER_NULL) {
								pw.op_comment(IGAsm.param_object, intToString(idx), "0",                 /*comment*/ p.m_name.m_value + " ");
							}
							else if (vr.m_type == IGValueRange.TYPE_DELEGATE_NULL) {
								pw.op_comment(IGAsm.param_func_ptr, intToString(idx), "0",                 /*comment*/ p.m_name.m_value + " ");
							}
							else if (vr.m_type == IGValueRange.TYPE_BOOL_FALSE) {
								pw.op_comment(IGAsm.param_i32, intToString(idx), "0",                 /*comment*/ p.m_name.m_value + " ");	
							}
							else if (vr.m_type == IGValueRange.TYPE_BOOL_TRUE) {
								pw.op_comment(IGAsm.param_i32, intToString(idx), "1",                 /*comment*/ p.m_name.m_value + " ");	
							}
							else {
								throw new RuntimeException("Invalid parameter value range");
							}
					}
					else
					{
							pw.op_comment(IGAsm.param_undef, intToString(idx),  "0", /*comment*/p.m_name.m_value);
					}
					//idx ++;
				}	
		
		
		
		
				if (s.m_class_extends != null) {
					pw.op_comment(IGAsm.local_load,"0", /*comment*/ "this");
					for (int i = 1; i < parameter_count; i++) {
						pw.op_comment(IGAsm.local_load,  intToString(i), /*comment*/ "param " + i);	
					}

					pw.addFunctionDependency(m_ctx.getType(s).toString(), "<constructor>");
					pw.op(IGAsm.call_super, intToString(parameter_count), m_ctx.getType(s).toString(), "<constructor>");
				}	
		
				
				for (IGMember source_member : s.m_members) 
				{
					if (source_member.m_type == IGMember.TYPE_VAR) {
						if (!source_member.hasModifier(IGMember.STATIC)) 
						{
							IGNode n = source_member.m_node.findRightMostWithNodeAndTokenType
								(IGNode.NODE_BINARY, TOK_ASSIGN);

							if (n != null) {

								Type t = n.m_children.get(1).m_type;	
								
								if (n.m_children.get(1).m_type.isFunctionWrapper()) {
									outputFunctionWrapperCast(source_member,n.m_children.get(1), source_member.m_return_type, pw);
								}		
								else
								{
									outputFunctionBody(source_member, n.m_children.get(1), pw);
									castTo(t, source_member.m_return_type, pw, true);
								}
								
								pw.op(IGAsm.dup);
								pw.op(IGAsm.this_store, m_ctx.getType(s).toString(),  source_member.m_name.m_value);
								pw.op(IGAsm.pop);
							}
							else {
								// by default all non set fields are initialized with 0
							}
						}
					}
				}						
	
				pw.op(IGAsm.ret0);
				pw.op(IGAsm.END);
			}
		}


		// print out the static initializers	
		/////////////////////////////////////////////			
		{
			ArrayList<String> sb = new ArrayList<String>();
			sb.add("static-function");
			sb.add("public");
			sb.add("<static>");
			sb.add("0..0");
			sb.add("1");
			sb.add("0");
			sb.add("");
			pw.header(sb);

			String module_type = m_ctx.getType(s).toString();

			for (IGMember m : s.m_members) 
			{
				boolean static_variable = true;
	
				if (m.m_type == IGMember.TYPE_VAR) 
				{
					if (m.hasModifier(IGMember.STATIC)) 
					{
						if (m.hasModifier(IGMember.ENUM_VALUE)) 
						{
							IGValueRange vr = m.m_return_value_range;
							
							if (vr.m_type == IGValueRange.TYPE_INT) {
								pw.op(IGAsm.push_i32, Integer.toString(vr.intValue()) );	
							}
							else if (vr.m_type == IGValueRange.TYPE_DOUBLE) {
								pw.op(IGAsm.push_f64, Double.toString(vr.doubleValue()) );		
							}
							else if (vr.m_type == IGValueRange.TYPE_POINTER_NULL) {
								pw.op(IGAsm.push_obj_null);	// does this make sense for generic asm?
							}
							else if (vr.m_type == IGValueRange.TYPE_DELEGATE_NULL) {
								pw.op(IGAsm.push_fp_null);	// does this make sense for generic asm?
							}
							else if (vr.m_type == IGValueRange.TYPE_BOOL_FALSE) {
								pw.op(IGAsm.push_i32, "0");	
							}
							else if (vr.m_type == IGValueRange.TYPE_BOOL_TRUE) {
								pw.op(IGAsm.push_i32, "1");	
							}
							else {
								throw new RuntimeException("Invalid parameter value range");
							}
	
							//pw.op(IGAsm.dup);
							pw.addMemberDependency(module_type, m.m_name.m_value);
							pw.op(IGAsm.static_store, module_type, m.m_name.m_value);
							//pw.op(IGAsm.pop);
						}
						else
						{
							IGNode n = m.m_node.findRightMostWithNodeAndTokenType(IGNode.NODE_BINARY, TOK_ASSIGN);
				
							if (n != null) {
								Type t = n.m_children.get(1).m_type;	
								
								if (n.m_children.get(1).m_type.isFunctionWrapper()) {
										outputFunctionWrapperCast(m,n.m_children.get(1), m.m_return_type, pw);
								}		
								else
								{
									outputFunctionBody(m, n.m_children.get(1), pw);
									castTo(t, m.m_return_type, pw, true);
								}
								
								pw.addMemberDependency(module_type, m.m_name.m_value);
								pw.op(IGAsm.static_store, module_type, m.m_name.m_value);
							}
						}
					}
				}
			}				
	
			pw.op(IGAsm.ret0);
			pw.op(IGAsm.END);
		}
		
		
		///////////////
		// Additional functionality for enums
		// 1) toString
		// 2) equals
		// 3) hashCode
	
		if (s.getScopeType() == IGScopeItem.SCOPE_ITEM_ENUM) 
		{
	
			// Print out the default enum toString
			////////////////
			
			if (!s.hasUserMemberFunction("toString"))
			{

				boolean has_enum_values = false;

				ArrayList<String> sb = new ArrayList<String>();
				{
					sb.add("static-function");
					sb.add("public");
					sb.add("toString");
					sb.add("1..1");
					sb.add("1");
					sb.add("1");
					sb.add("");
				}
				pw.header(sb);

				String lblEnd = getLabel();
		
				for (IGMember m : s.m_members) 
				{
					boolean static_variable = true;
	
					if (m.m_type == IGMember.TYPE_VAR && m.hasModifier(IGMember.STATIC)) 
					{
						if (m.hasModifier(IGMember.ENUM_VALUE)) {
				
							String next_label = getLabel();
					
							pw.op_comment(IGAsm.local_load, "0", /*comment*/ "this");
							IGValueRange vr = m.m_return_value_range;
							
							if (vr.m_type == IGValueRange.TYPE_INT) {
								pw.op(IGAsm.push_i32, Integer.toString(vr.intValue()) );	
								pw.op(IGAsm.cmp_eq_i32);	
							}
							else if (vr.m_type == IGValueRange.TYPE_DOUBLE) {
								pw.op(IGAsm.push_f64, Double.toString(vr.doubleValue()) );		
								pw.op(IGAsm.cmp_eq_f64);
							}
							else if (vr.m_type == IGValueRange.TYPE_POINTER_NULL) {
								pw.op(IGAsm.push_obj_null);
								pw.op(IGAsm.cmp_eq_obj);
							}
							else if (vr.m_type == IGValueRange.TYPE_DELEGATE_NULL) {
								pw.op(IGAsm.push_fp_null);
								pw.op(IGAsm.cmp_eq_fp);
							}
							else if (vr.m_type == IGValueRange.TYPE_BOOL_FALSE) {
								pw.op(IGAsm.push_i32,"0");
								pw.op(IGAsm.cmp_eq_i32);	
							}
							else if (vr.m_type == IGValueRange.TYPE_BOOL_TRUE) {
								pw.op(IGAsm.push_i32,"1");	
								pw.op(IGAsm.cmp_eq_i32);
							}
							else {
								throw new RuntimeException("Invalid parameter value range");
							}
							
							has_enum_values = true;
							pw.op(IGAsm.j0_i32, next_label);
							pw.op(IGAsm.push_string, m.m_name.m_value);
							pw.op(IGAsm.jmp, lblEnd);
							pw.label(next_label);
						}
					}
				}
				if (has_enum_values) {
					//pw.op(IGAsm.pop);
				}

				pw.op(IGAsm.push_string, "unknown");
				pw.label(lblEnd);
				pw.op(IGAsm.ret1);	
				pw.op(IGAsm.END);
			}
		
			// add a default equals
			if (!s.hasUserMemberFunction("equals")) 
			{
				ArrayList<String> sb = new ArrayList<String>();
				{
					sb.add("static-function");
					sb.add("public");
					sb.add("equals");
					sb.add("2..2");			// takes 2 parameters (this, value)
					sb.add("2");			// stack height of 2
					sb.add("1");			// returns a single value
					sb.add("FINAL");
				}
				pw.header(sb);
			
				pw.op(IGAsm.local_load,"0");
				pw.op(IGAsm.local_load,"1");
				pw.op(IGAsm.cmp_eq_i32);
				pw.op(IGAsm.ret1);
				pw.op(IGAsm.END);
			}
		
			// add a default hashCode
			if (!s.hasUserMemberFunction("hashCode")) 
			{
				ArrayList<String> sb = new ArrayList<String>();
				{
					sb.add("static-function");
					sb.add("public");
					sb.add("hashCode");
					sb.add("1..1");
					sb.add("1");
					sb.add("1");
					sb.add("FINAL");
				}
				pw.header(sb);
			
				pw.op(IGAsm.local_load,"0");
				pw.op(IGAsm.ret1);
				pw.op(IGAsm.END);				
			}
		}
	
	
		// no longer
		pw.finished();
	
		
		//////////
		// generate assembly	
		{
			String extension2 = generateExtension(s);
			return new IGAsmTarget(pw, s, extension2 + ".asm",  
										  extension2 + config.dst_extension, 
										  extension, generateFullPath(s));
		}
	}
	
	/*
	private String toBase64(String s) {
		String   bytesEncoded = DatatypeConverter.printBase64Binary(s.getBytes());
		return "base64:" + bytesEncoded;
	}
	*/
	
	
	public String generateFullPath(IGSource s) 
	{
		Type simple_type = s.getInstanceDataType();
		Type full_type = m_ctx.getType(s);
		
		String a = full_type.toString();
		//a = a.replace("<", "{");
		//a = a.replace(">", "}");
		return a;
	}
	
	public String generateExtension(IGSource s) 
	{
		Type simple_type = s.getInstanceDataType();
		Type full_type = m_ctx.getType(s);
		
		String a = full_type.toString().substring(simple_type.toString().length(), full_type.toString().length());
		a = a.replace("<", "{");
		a = a.replace(">", "}");
		return a;
	}
}

/*
 * Characterization
 * - this is used to determine HOW a named variable or function should be accessed
 */

final class  Characterization
{
	public static void touch() {
	}


	private String m_name = null;
	private Type   m_type = null;
	private IGNode m_to_process = null;
	private boolean m_needs_this_push = false;
	private boolean m_function_pointer = false;
	private IGMember  m_function = null;
	private boolean m_super_call = false;
	
	public IGNode getProcessingNode()
	{
		return m_to_process;
	}
	
	public boolean needsThisPush()
	{
		return m_needs_this_push;
	}
	
	
	public static final int FIELD = 0;
	public static final int CALL  = 1;
	
	public Characterization(IGContext ctx, int mode, Asm o, IGMember m, IGNode root)
	{
		// are we specifically accessing a field?
		if (mode == FIELD) 
		{
			switch (root.m_node_type) 
			{
				//case IGNode.NODE_LOCAL_GET:
				//case IGNode.NODE_LOCAL_SET:
				
				case IGNode.LOCAL_ACCESSOR:
				{
					m_name = root.m_token.m_value;
					
					IGMember the_actual_function = (IGMember)root.m_scope;
					// okay so this doesn't detect that the function in question is in fact static
					// this just detects the scope that the function (getter) was called within
					// TODO fix
					
					IGSource s = m.m_parent_source;
					//if (the_actual_function.hasModifier(IGMember.STATIC)) {
					//	m_type = s.getDataType(null);
					//}
					//else
					//{
						m_type = ctx.getType(s);
					//}
					
					m_function = the_actual_function;
					
					break;
				}
				case IGNode.STATIC_LOCAL_ACCESSOR:
				{
					m_name = root.m_token.m_value;
					
					IGMember the_actual_function = (IGMember)root.m_scope;
					// okay so this doesn't detect that the function in question is in fact static
					// this just detects the scope that the function (getter) was called within
					// TODO fix
					
					IGSource s = m.m_parent_source;
					//if (the_actual_function.hasModifier(IGMember.STATIC)) {
						m_type = s.getDataType(null);
					//}
					//else
					//{
					//	m_type = ctx.getType(s);
					//}
					
					m_function = the_actual_function;
					
					break;
				}
				
				//case IGNode.NODE_DOT_GET:
				//case IGNode.NODE_DOT_SET:
				
				case IGNode.STATIC_DOT_ACCESSOR:
				case IGNode.DOT_ACCESSOR:
					
					m_function = (IGMember)root.m_scope;
					
					// handle the case
					// super DOT getter-or-setter
					{
						IGNode left_node = root.m_children.get(0);
						if (left_node.m_node_type == IGNode.LOCAL_VARIABLE_ACCESS &&
							left_node.m_token.m_value.equals("super")) {
							m_super_call = true;		
						}
					}
					
					// fall through
				default:
					m_name = root.m_children.get(1).m_token.m_value;
					m_type = root.m_children.get(0).m_type;
					m_to_process = root.m_children.get(0);
		
					if (isStatic()) {
						m_to_process = null;
					}
			}
			
			
			m_type = ctx.resolveType(m_type);
			
			return;
		}
	
		// okay so we're accessing a function
	
		//if (root.m_children.size() != 2) {
		//	root.m_token.printError("what?");
		//	root.debug(1);
		//	throw new RuntimeException("How does a call node NOT have 2 children?");
		//	
		//}
	
		IGNode left = root; //.m_children.get(0);
		//IGNode right = root.m_children.get(1);	
	
		/////////////////////////////////////////
		// Call Delegate
		/////////////////////////////////////////
		if (!left.m_type.isFunctionWrapper()) {
		
		
			
			
			m_to_process = left;
			m_function_pointer = true;
			m_type = null;
			m_name = null;
			
			m_type = ctx.resolveType(m_type);
			
			//System.out.println("AAAA: " + left.m_type);
		}
		/////////////////////////////////////////
		// Call Function/Static-Function/
		/////////////////////////////////////////
		else
		{
			m_function_pointer = false;
			
			//System.out.println("BBBB: " + left.m_type);
			
			switch (root.m_node_type) {
				case IGNode.NODE_DOT:
				case IGNode.DOT_ACCESSOR:
				case IGNode.STATIC_DOT_ACCESSOR:
				
				//case IGNode.NODE_DOT_SET:
				//case IGNode.NODE_DOT_GET:
				
					m_function 	 = (IGMember)root.m_scope;
					m_name 		 = left.m_children.get(1).m_token.m_value;
					m_type 		 = left.m_children.get(0).m_type;
					m_to_process = left.m_children.get(0);
				
					if (m_to_process.m_node_type == IGNode.LOCAL_VARIABLE_ACCESS &&
						m_to_process.m_token.m_value.equals("super")) {
						m_super_call = true;		
					}
				
					// the function is viewed as being final
					// so we should set up the type so that it matches that of the member?
					
				
					//if (root.m_node_type == IGNode.NODE_DOT_GET) {
					//	System.out.println("" + root);
					//}
				
					if (isStatic()) {
						m_to_process = null;
					}
					break;
				
				// accessing a static function
				case IGNode.STATIC_FIELD_ACCESS:
				{
					m_function = (IGMember)root.m_scope;
					m_name = left.m_token.m_value;
					
					IGMember the_actual_function = (IGMember)root.m_scope;
					// okay so this doesn't detect that the function in question is in fact static
					// this just detects the scope that the function (getter) was called within
					// TODO fix
					
					IGSource s = m.m_parent_source;
					if (the_actual_function.hasModifier(IGMember.STATIC)) {
						m_type = the_actual_function.m_parent_source.getDataType(null);
					}
					else
					{
						m_type = the_actual_function.m_parent_source.getInstanceDataType();
					}
					
		
					m_to_process = null;
					break;
				}
			
				// accessing a member function
				case IGNode.FIELD_ACCESS:
					m_function = (IGMember)root.m_scope;
					m_name = left.m_token.m_value;
					
					
					m_type = m.m_parent_source.getPostValidatorConstructorType();	//getInstanceDataType();
					m_to_process = null;
					m_needs_this_push = true;
					break;
				
				case IGNode.LOCAL_VARIABLE_ACCESS:
					// more than likely a delegate
					throw new RuntimeException("Invalid");
					//break;
				
				default:
					System.err.println("Unknown characterization for: " + root.m_node_type);
			}
			
			Type prev_type = m_type;
			m_type = ctx.resolveType(m_type);	
			
			if (prev_type != m_type) 
			{
				String function_name = m_function.m_name.m_value;
				
				m_function = ((IGSource)m_type.m_scope).getMember(function_name);
				
				
			}
			
			///if (isFinal()) 
			//		{
						//System.out.println("fuck fuck fuck fuck fuck");
						//if (m_function.m_parent_source != (IGSource)m_type.m_scope)
						//{	
							//System.out.println("need drill down: " + 			m_type.m_scope + " " + 			m_function.m_parent_source);
						//}
			//		}
				
			// okay so the problem is 
		}
		
			
		
		//if (m_function != null && m_function.hasModifier(IGMember.FINAL)) {
		//	m_type 
		//}
		
		//System.err.println("Characterization");

		//System.err.println("name: " + m_name);
		//System.err.println("type: " + m_type);
		//System.err.println("root-type: " + root.m_node_type);
	
		//root.debug(1);
		/*
		if (n.m_type == IGNode.STATIC_FIELD_ACCESS) {
		
		}
		else if (n.m_type == IGNode.NODE_DOT) {
		
		}
		*/
	}
	
	public boolean isObject() {
		return !m_type.isNamespace();
	}
	
	public boolean isSuper() {
		//System.out.println("is super");
		return m_super_call;
	}
	
	public boolean isStatic() {
		return m_type.isNamespace();
	}
	
	// fordebugging
	public IGMember getFunction() {
		return m_function;
	}
	
	public boolean isFinal() {
	
		if (m_function_pointer) return false;
		
		if (m_function != null && m_function.hasModifier(IGMember.FINAL)) {
			return true;
		
		}	
		
		return false;
		/*
		IGScopeItem si = m_type.m_scope;
		if (si instanceof IGSource && m_name != null) {
			IGSource m2  = (IGSource)si;
			IGMember member = m2.getMember(m_name);
			
			if (member == null) {
				throw new RuntimeException("Couldn't find field named: " + m_name + " in: " + m_type);
			}
			
			if (member.hasModifier(IGMember.FINAL)) {
				return true;
			}		
		}
		*/
		// okay so we really need to look at the function in question
		// in order to determine if the scope is currently valid
		//return false;
	}
	
	public boolean isInterface() {
		if (isObject()) {
			IGScopeItem si = m_type.m_scope;
			if (si instanceof IGSource) {
				IGSource m2  = (IGSource)si;
				return m2.m_type == IGScopeItem.SCOPE_ITEM_INTERFACE;
			}
		}
		return false;
	}
	
	public boolean isEnum() {
		if (isObject()) {
			IGScopeItem si = m_type.m_scope;
			if (si instanceof IGSource) {
				IGSource m2  = (IGSource)si;
				return m2.m_type == IGScopeItem.SCOPE_ITEM_ENUM;
			}
		}
		return false;
	}
	
	public boolean isFunctionPointer() {
		return m_function_pointer;
	}
	
	
	public String  getName()
	{
		return m_name;
	}
	
	public void setType(Type t) {
		m_type = t;
	}
	
	public Type getObjectType() {
		return m_type;
	}
	
	public String getType()
	{
		// replacing of overridden scope types
		// or should this just mark that the type has been overridden... probably
		if (m_type == Type.INT) {
			return "iglang.util.igInteger";
		}
		else if (m_type == Type.DOUBLE) {
			return "iglang.util.igDouble";
		}
		else if (m_type == Type.FLOAT) {
			return "iglang.util.igFloat";
		}
		else if (m_type == Type.BOOL) {
			return "iglang.util.igBoolean";
		}
		else if (m_type == Type.SHORT) {
			return "iglang.util.igShort";
		}
		else if (m_type == Type.BYTE) {
			return "iglang.util.igByte";
		}
	
		String s =  m_type.toString();
		if (s.indexOf("source:") != -1) {
			return s.substring("source:".length());
		}
		else
		{
			return s;
		}
		
	} 
}
