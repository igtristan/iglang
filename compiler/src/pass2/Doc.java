/*
Copyright (c) 2014-2017 Incubator Games Ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

package pass2;

import java.util.*;
import util.*;
import pass1.*;

import java.io.*;


									/*
					
    @author (classes and interfaces only, required)
    @version (classes and interfaces only, required. See footnote 1)
    @param (methods and constructors only)
    @return (methods only)
    @exception (@throws is a synonym added in Javadoc 1.2)
    @see
    @since
    @serial (or @serialField or @serialData)
    @deprecated (see How and When To Deprecate APIs)
*/

public class Doc extends Processor
{

	public static int indexOfWhiteSpace( String s, int startOffset )
	{
        final int length = s.length();
        for ( int i = startOffset; i < length; i++ )
            {
            switch ( s.charAt( i ) )
                {
                case ' ':
                case '\n':
                case '\r':
                case '\t':
                    return i;
                default:
                    // keep looking
                }
            }// end for
        return -1;
	}

	private Hashtable<String, String> getInfo(IGMember m, Vector<String> doc_parameters) 
	{
		String doc = m.m_base_node == null ? "" : m.m_base_node.m_token.m_whitespace;
		

		Hashtable<String, String> text = new Hashtable<String,String>();
		String text_key = "@summary";
		text.put("@summary", "");
		text.put("@body", "");
		
		if (doc.lastIndexOf("*/") != -1) {
			int term = doc.lastIndexOf("*/");
			int prim = doc.lastIndexOf("/**", term);
			
			String sub = doc.substring(prim + 3, term);
			String [] lines = sub.split("\n");
		

		
		
		
			for (String line : lines) {
				line = line.trim();
				while (line.startsWith("*")) {
					line = line.substring(1).trim();
				}
				
				if (line.startsWith("@param")) {
					line = line.substring("@param".length()).trim();
					
					int idx = indexOfWhiteSpace(line, 0);
					
					doc_parameters.add(line.substring(0, idx));
					text_key    = "@param:" + line.substring(0, idx);
					line        = line.substring(idx+1);
					
					text.put(text_key, "");
				}
				else if (line.startsWith("@return")) {
					text_key = "@return";
					line = line.substring("@return".length());

					text.put(text_key, "");
				}
				
				if (!line.startsWith("@") && line.length() > 0) {
					text.put(text_key, text.get(text_key) + " " + line);
					
					if (text_key.equals("@summary")) {
						text_key = "@body";
						text.put(text_key, text.get(text_key) + line);
					}
				}
			}
		}
		return text;
	}

	public String typeLink(IGSource s, Type type) {
		if (type.isPrimitive()) {
			return "" + type;
		}
		
		String type_string = type.m_primary;
		
		
		
		String mypath = s.m_scope_path.toString();
		int count = mypath.length() - mypath.replace(".", "").length();
		
		String tmp = "<a href=\"";
		for (int i = 0; i < count; i++) {
			tmp += "../";
		}
		tmp += type.m_primary.replace(".","/");
		tmp += ".html\">" + type.toString()
			.replace("<", "&lt;").replace(">", "&gt;") + "</a>";
		return tmp;
		
	}
	
	
	public String packageLink(IGSource s, IGScopePath sp) {

		
		String mypath = s.m_scope_path.toString();
		int count = mypath.length() - mypath.replace(".", "").length();
		
		String tmp = "<a href=\"";
		for (int i = 0; i < count; i++) {
			tmp += "../";
		}
		tmp += "index.html#" + sp.toString();
		tmp += "\">" + sp.toString()
			.replace("<", "&lt;").replace(">", "&gt;") + "</a>";
		return tmp;
		
	}
	
	public String fieldName(IGSource s, String name) {
		if (name.equals("<constructor>")) {
			return s.m_expected_component;
		}
		if (!name.startsWith("__ig") && name.startsWith("__")) {
			name = name.substring(2);
		}
		if (name.endsWith("__get")) {
			return "get " + name.substring(0, name.length() - 5);
		}
		if ( name.endsWith("__set")) 
		{
			return "set " + name.substring(0, name.length() - 5);
		}
		else if (name.equals("<get[]>")) {
			return "get []";
		}
		else if (name.equals("<set[]>")) {
			return "set []";
		}
		else
		{
			return name;
		}
	}
	
	public String fieldLink(IGSource s, String name) {
		return "<a href=\"#" + name + "\">" + fieldName(s, name) + "</a>";
	}

	public String fieldAnchor(IGSource s, String name) {
		return "<a id=\"" + name + "\"></a>";
	}


	public void writeModifiers(IGMember m, PrintWriter pw, boolean all_modifiers)
	{			
		if (all_modifiers) {
			for (int i = 0; i < 16; i++) 
			{
				if ((m.m_modifiers & (1 << i)) != 0) {
					pw.print(IGMember.s_modifier_names[i] + " ");
				}
			}
		}
		else {
			if (m.hasModifier(IGMember.STATIC)) {
				pw.print("static ");
			}
			//if (m.hasModifier(IGMember.GET)) {
			//	pw.print("get ");
			//}
			//if (m.hasModifier(IGMember.SET)) {
			//	pw.print("set ");
			//}
		}
	}

	public  void run(IGPackage root, Vector<IGSource> sources)
	{	
		// create the header
		
		try
		{
			{
				Vector<IGScopePath> sp = new Vector<IGScopePath>();
				for (IGSource s : sources) {
					sp.add(s.m_scope_path);
				}
			
				Collections.sort(sp, new Comparator<IGScopePath>() {
					@Override
					public int compare(IGScopePath o1, IGScopePath o2) {
						return o1.compareTo(o2);
					}           
				});
			
				PrintWriter pw = Util.createOutputPrintWriter(root, "index.html");
				pw.println("<html><body><div style=\"margin: 30px\">");
				pw.println("<h1>iglang</h1>");
				for (IGScopePath p : sp) {
				
					
					String class_link = 
					
							"<a id=\"" + p.toString() +  "\" href=\"" + p.toString().replace(".","/") + ".html\">" +
							p + "</a>";
				
					pw.println("" + class_link + "<br />");
				}
				
				
				pw.println("</div></body></html>");
				pw.close();
			
			}
		
		
			for (IGSource s: sources)
			{
				if (pass1.IGSettings.s_verbose) {
					System.err.println("Php.run " + s.m_expected_component);
				}
				
				PrintWriter pw = Util.createOutputPrintWriter(s, ".html");
				
				
				String name = s.m_expected_component;
				
				
				// referencing
				// http://docs.oracle.com/javase/7/docs/api/java/lang/String.html
				pw.println("<html>");
				pw.println("<body>");
				pw.println("<div style=\"margin-left: 30px; margin-right: 30px\">");
				
				pw.println("<div class=\"header\">");
				pw.println("<p>package: " + packageLink(s, s.m_package) + "</p>");
				int scope_type = s.getScopeType();
				if (scope_type == IGScopeItem.SCOPE_ITEM_CLASS) {
					pw.println("class");
				}
				if (scope_type == IGScopeItem.SCOPE_ITEM_INTERFACE) {
					pw.println("interface");
				}
				if (scope_type == IGScopeItem.SCOPE_ITEM_ENUM) {
					pw.println("enum");
				}
				pw.println(s.m_expected_component + "<br />");
				
				if (scope_type == IGScopeItem.SCOPE_ITEM_ENUM) {
					pw.println("extends " + typeLink(s, s.m_enum_extends) + "<br />");
				}
				else {
					if (s.m_class_extends != null) {
						pw.println("extends " + typeLink(s, s.m_class_extends) + "<br />");
					}
				}
				
				/*
					public Type				 m_class_extends = null;
	public Vector<Type>      m_class_implements = new Vector<Type>();
	public Type				 m_enum_extends = null;
	*/
				
				if (scope_type == IGScopeItem.SCOPE_ITEM_CLASS) {
					pw.println("All Implemented Interfaces: <br />");
					
					for (Type t : s.m_class_implements) {
						pw.println("" + t + "<br />");
					}	
				}
				
				pw.println("<hr />");
				
				/*
				
				
				pw.println("<div class=\"subTitle\">" + "java.lang" + "</div>");
				pw.println("<h2 title=\"" + name + "\" class=\"title\">" + name + "</h2>");
				pw.println("</div>");
				
				pw.println("<hr />");
				pw.println("<div class=\"contentContainer\">");
				pw.println("<ul class=\"inheritance\">");
				pw.println("<li><a href=\"../../java/lang/Object.html\" title=\"class in java.lang\">java.lang.Object</a></li>");
				pw.println("<li>");
				pw.println("<ul class=\"inheritance\">");
				pw.println("<li>java.lang.String</li>");
				pw.println("</ul>");
				pw.println("</li>");
				pw.println("</ul>");
				//pw.println("<h1>" + s.m_expected_component + "</h1>");
				
				int scope_type = s.getScopeType();
				if (scope_type == IGScopeItem.SCOPE_ITEM_CLASS) {
					pw.println("<p>class</p>");
				}
				if (scope_type == IGScopeItem.SCOPE_ITEM_INTERFACE) {
					pw.println("<p>interface</p>");
				}
				if (scope_type == IGScopeItem.SCOPE_ITEM_ENUM) {
					pw.println("<p>enum</p>");
				}
				*/
				
				pw.println("<hr />");
				pw.println("<h2>Variable Summary</h2>");
				pw.println("<table>");
				for (IGMember m : s.m_members) {
					if (m.isVariable() && !m.isPrivate() && !m.isInternal()) {
						pw.print("<tr><td>");
						writeModifiers(m,pw,false);
						pw.println("var " + fieldLink(s, m.m_name.m_value));
						
						pw.println(" : " + typeLink(s, m.m_return_type) + "<br/>");
											
						Vector<String> doc_parameters = new Vector<String>();
						Hashtable<String, String> text = getInfo(m, doc_parameters);
						
						pw.println("<div style=\"margin-left: 30px\">");
						pw.println(text.get("@summary"));
						pw.println("</div>");
						
						
						pw.print("</td></tr>");
					}
				}
				pw.println("</table>");
				
				pw.println("<h2>Function Summary</h2>");
				pw.println("<table>");
				for (IGMember m : s.m_members) {
					if (m.isFunction() && !m.isPrivate()  && !m.isInternal()) {
						pw.print("<tr><td>");
						writeModifiers(m,pw,false);			
						pw.print("function " + fieldLink(s, m.m_name.m_value));
						pw.print(" (");
						
						boolean first = true;
						for (IGVariable v : m.m_parameters) {
							if (!first) {
								pw.print(", ");
							}
							first = false;
							
							pw.print(v.m_name.m_value + " : " + typeLink(s, v.m_type));
						}
						pw.print(") : ");
						pw.println(typeLink(s, m.m_return_type) + "<br/>");	
											
						Vector<String> doc_parameters = new Vector<String>();
						Hashtable<String, String> text = getInfo(m, doc_parameters);
						
						pw.println("<div style=\"margin-left: 30px\">");
						pw.println(text.get("@summary"));
						pw.println("</div>");
						
											
						pw.print("</td></tr>");
					}
				}
				pw.println("</table>");



				for (int kk = 0; kk < 2; kk++)
				{
					if (kk == 0) {
						pw.println("<h2>Detailed Variables</h2>");
					}
					else {
						pw.println("<h2>Detailed Functions</h2>");
					}
					String color = "#fff";
					for (IGMember m : s.m_members) 
					{
						if (m.isPrivate() || m.isInternal()) {
							continue;
						}
						
						if (kk == 0 && m.isFunction()) {
							continue;
						}
						if (kk == 1 && !m.isFunction()) {
							continue;
						}
				
						color = color.equals("#fff") ? "#eee" : "#fff";
					
					
						pw.println(fieldAnchor(s, m.m_name.m_value));
					
						pw.println("<div style=\"background-color: " + color + "; padding: 8px; padding-left:16px; margin: 0px;\">");
					
						Vector<String> doc_parameters = new Vector<String>();
						Hashtable<String, String> text = getInfo(m, doc_parameters);
				
						pw.println("<div>");
						pw.print("<h3>");					
						pw.println( fieldName(s, m.m_name.m_value) + "</h3>");
						pw.println("</div>");	
					
					
						writeModifiers(m, pw, true);					



						if (!m.isFunction()) {
							pw.print(" var " + fieldName(s, m.m_name.m_value) + " : ");
							pw.println(typeLink(s, m.m_return_type) + "<br/>");
							pw.println("<div style=\"margin-left: 20px;  margin-bottom: 10px;\">");
							pw.println("<p>" + text.get("@body") + "</p>");
							pw.println("</div>");
						}
						else if (m.isFunction()) {
						
							pw.print(" function " + fieldName(s, m.m_name.m_value) + " (");
						
							boolean first = true;
							for (IGVariable v : m.m_parameters) {
								if (!first) {
									pw.print(", ");
								}
								first = false;
							
								pw.print(v.m_name.m_value + " : " + typeLink(s, v.m_type));
							}
							pw.println(") : ");	
						
							pw.println(typeLink(s, m.m_return_type) + "<br/>");
					
						
						
						
							pw.println("<div style=\"margin-left: 20px;  margin-bottom: 10px;\">");
							pw.println("<p>" + text.get("@body") + "</p>");
						
							// display the doc text for each parameter
							if (doc_parameters.size() > 0) {
								pw.println("<strong>Parameters:</strong><br/>");
								pw.println("<div style=\"margin-left: 20px; margin-bottom: 10px;\">");
								for (String doc_param : doc_parameters) {
									pw.println(doc_param + " " + text.get("@param:" + doc_param) + "<br />");
								}
								pw.println("</div>");
							}
						
							if ( text.get("@return") != null)
							{
								pw.println("<strong>Returns:</strong><br/>");
								pw.println("<div style=\"margin-left: 20px;  margin-bottom: 10px;\">");
								pw.println( text.get("@return"));
								pw.println("</div>");
							}
							pw.println("</div>");
						}
						pw.println("</div>");
					}
				}
				pw.println("</div>");
				pw.println("</body></html>");
				pw.close();
			}
		}
		catch (IOException ex)
		{
			ex.printStackTrace();
		}
	}

}