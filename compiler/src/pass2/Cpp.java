/*
Copyright (c) 2014-2017 Incubator Games Ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

package pass2;

import util.*;
import pass1.*;

import java.util.Vector;
import java.util.Hashtable;
import java.util.ArrayList;
import java.io.*;

public class Cpp extends Processor
{
	final boolean DISPLAY_ANNOTATIONS = false;


	public int peekAnnotation(){
		peek();
		return 0;
	}


	public Object getAnnotation(Token t) {
		return null;
	}
	public  void commentToken(PrintWriter pw, Token t) throws IOException
	{
		pw.print(t.m_whitespace);
		
		if (DISPLAY_ANNOTATIONS) {
			pw.print("/*");
			pw.print(t.m_value);
			pw.print("*/");
		}
	}

	public  void commentUntil(PrintWriter pw, int type) throws IOException
	{
		while (peek().m_type != type) {
			commentToken(pw, read());
		}
	}
	
	public  void whitespaceUntil(PrintWriter pw, int type0, int type1) throws IOException
	{
		while (peek().m_type != type0 && peek().m_type != type1) {
			pw.print(read().m_whitespace);
		}
	}

	public void outputMemberBody(TokenWriter pw, Token [] list, IGMember m, IGSource s) throws IOException
	{
						
		Processor object_member = new Processor();
		object_member.setSource(list, m.m_first_body_token, m.m_last_body_token);
		
		
		pw.print("/*member*/");
	
		// if there is no body of this member there is no default value specified
		// thus we need to come up with a good default value
		////////////////////////////////////////////////
		if (object_member.peekTokenType() == TOK_EOF) 
		{
			Type type = m.m_return_type;
			if (type == Type.get("int") || type == Type.get("uint") || type == Type.get("double"))
			{
				pw.print(" = 0");
			}
			else if (type == Type.get("bool")) {
				pw.print(" = false");
			}
			else if (type.isEnum()) {
				pw.print(" = 0");
			}
			else {
				pw.print(" = NULL");
			}
		
		}
		else
		{
		
			pw.print("= ");
			
			pw.clearAppendBuffer();
			
			while (object_member.peekTokenType() != TOK_EOF) 
			{
				Token token = object_member.read();
				IGNode node = token.m_node;
			
				// this is SOO horrible
				if (node != null && node.m_node_type == IGNode.SCOPE_CHAIN_START) 
				{
					IGSource src = (IGSource)node.m_type.m_scope;
					pw.prepend(token, src.m_package.toStringWithSeperator("::","::",false) + "::");
				}
				else if (token.m_type == IGLexer.LEXER_STRING)
				{
					pw.prepend(token, "iglang::const_string(L\"");
					pw.append(token, "\"," + getStringIndex(token.m_value) +  ")");
				}
				else if (token.m_type == IGLexer.LEXER_CHAR)
				{
					pw.prepend(token, "'");
					pw.append(token, "'");
				}
				else if (token.m_type == TOK_NULL) {
					token.m_output = false;
					pw.append(token, "NULL");
				}
				else if (Util.hasNode(token, IGNode.NODE_NEW))
				{
					token.m_output = false;
					Type t = node.m_type;
					
					// disable the output for the tokens that compose the type
					IGNode type_node = node.getChild(0);
					for (int i = type_node.m_start_index; i < type_node.m_end_index; i++) 
					{
						// skip over the tokens alltogether 
						//read();
						
						object_member.read();
						
						Token token_i = object_member.peekAt(i);
						
						token_i.m_output = false;
						token_i.discard();
						
						pw.append(token, "/*" + token_i.m_value + "*/");
					}
					
					//node.getChild(1).disableTokenOutput();
					
					String result = type(s, t);
					if (result.endsWith("*")) {
						result = result.substring(0, result.length() - 1);
					}
					//pw.prepend(token, "/*NEW*/");
					pw.append(token, " (new " + result + ")->__ig_init" );
				
				}
				else if (Util.hasNode(token, IGNode.NODE_DOT))
				{
					IGNode left  = node.getChild(0);
					IGNode right = node.getChild(1);
					
					Type   left_type = left.m_type;
					
					pw.printWhitespace(token);
					if (left_type.isNamespace() || left.m_token.m_value.equals("super")) {
						pw.append(token, "::");
					}
					else
					{
						pw.append(token, "->");
					}
					
					token.m_output = false;
				}	
			}
			
			pw.printRange(list, m.m_first_body_token, m.m_last_body_token);
		}	
		
		
	}

	public void outputCppVariable(TokenWriter pw, IGSource s) throws IOException
	{
		pw.println();
		
		Token t = read();
		IGMember m = (IGMember)getAnnotation(t);	//.m_annotation_object;
		if (m.hasModifier(IGMember.STATIC) && !m.hasModifier(IGMember.FINAL)) 
		{
			commentUntil(pw, TOK_ID);
			pw.print(type(s, m.m_return_type));
			pw.print(" ");
			pw.print(s.m_expected_component);
			pw.print("::");
			pw.print(read().all());
			
			
			outputMemberBody(pw, s.m_token_list, m, s);
			
			pw.print(";");
			
			//pw.print(expect(TOK_SEMI, "Expecting semicolon following member body"));
			/*
			commentUntil(pw, TOK_ASSIGN);
			
			// this is technically incomplete
			while (peek().m_type != TOK_SEMI) {
				pw.print(read().all());
			}
			pw.print(read().all());
			*/
		}
		else
		{
			commentUntil(pw, TOK_SEMI);
			commentToken(pw, read());
		}
		
		pw.println();
	}

	public void outputCppFunction(TokenWriter pw, IGSource s) throws IOException
	{
		pw.clearAppendBuffer();
	
		Token t = read();
		IGMember m = (IGMember)getAnnotation(t);//.m_annotation_object;
		boolean constructor = m.m_name.m_value.equals("<constructor>");
		
		while (t.m_type != TOK_FUNCTION) {
			pw.print(t.m_whitespace);
			t = read();
		}
		
		if (!constructor) {
			pw.print(type(s, m.m_return_type));
			pw.print(" ");
		}
		else
		{
			pw.print(s.m_expected_component + "*");
		}
		
		pw.print(t.m_whitespace);
		// preface the function with the name of the class
		pw.print(s.m_expected_component);
		pw.print("::");
		
		// throw away any get or set tokens
		////////////////////////////////////
		if (peek().m_type == TOK_GET || peek().m_type == TOK_SET) {
			pw.printWhitespace(read());
		}

		// write out the name of the function
		if (constructor) {
			pw.print(read().m_whitespace + "__ig_init");
		}
		else {
			pw.print(read().all());
		}
		
		// write out the first bracket "("	
		//////////////////////////////////				
		pw.print(expect(TOK_LBRACKET));
		

		
		//pw.print("__iglang_object_defaults();/*should be non */");
		
		// print out the parameters to this function
		for (int i = 0; i < m.m_parameters.size(); i++) 
		{
			String whitespace = "";
		
			pw.printWhitespace(expect (TOK_ID));
			
			Token colon = expect (TOK_COLON);
			pw.printWhitespace(colon);
			
			IGNode id_type_pair = colon.m_node;
			
			// skip ahead to the end of the type id pair
			setTokenIndex(id_type_pair.m_end_index);
			
			// skip default parameters
			if (peek().m_type == TOK_ASSIGN) {
				pw.printWhitespace(read());
				pw.printWhitespace(read());
			}
			/*
			while (peek().m_type != TOK_RBRACKET && peek().m_type != TOK_COMMA) {
				whitespace += read().m_whitespace;
			}
			
			pw.print(whitespace);
			*/
			
			IGVariable v = m.m_parameters.get(i);
			
			//////////////
			// output
			// TYPE  ID
			
			pw.print(type(s, v.m_type));
			pw.print(" ");
			pw.printValue(v.m_name);
			
			if (peek().m_type == TOK_COMMA) {
				pw.print(expect(TOK_COMMA));
			}
		}
		
		
		
		
		// close bracket
		pw.print(expect(TOK_RBRACKET));
		
		if (!constructor) {
			commentToken(pw, expect(TOK_COLON));
					
			// throw away the following type info
			while (peek().m_type != TOK_LBRACE) {
				commentToken(pw, read());
			}
		}
		else
		{
			//pw.print("constructor body");
		}
		
		IGNode body_node = m.m_node.getChild(1);
		int start_token_index = body_node.m_start_index;
		int end_token_index   = body_node.m_end_index;
		
		
		//int start_token_index = getTokenIndex();
		
		if (constructor) { 
			pw.print("{super::__ig_init();");
		}
		
		markup(pw, s, body_node);
		
		pw.printRange(s.m_token_list, start_token_index, end_token_index);
		
		if (constructor) { 
			pw.print(" return this; }");
		}
		
		setTokenIndex(end_token_index);
	}
	
	/*
	 * Depth first traversal of all nodes
	 */
	
	private void markup(TokenWriter pw, IGSource s, IGNode node)
	{
		
		if (markupPre(pw, s, node)) {
		
			final IGNodeList children = node.m_children;
			int   children_count = children.size();
			//for (IGNode child : node.m_children) {
			
			for (int i = 0; i < children_count; i++) 
			{
				markup(pw, s, children.get(i));
			}
		}
		
		markup2(pw, s, node);
	}
	
	
	private boolean markupPre(TokenWriter pw, IGSource s, IGNode node)
	{
		return true;
	}
	
	private static boolean isAssignTokenType(int t) {
		return  t == TOK_ASSIGN ||
				t == TOK_ADD_ASSIGN ||
				t == TOK_SUB_ASSIGN;
	}
	
	
	
	private void functionWrapper(IGSource s, IGNode right, TokenWriter pw)
	{
		// get the function signature from the function wrapper if there is one
		Type right_type = right.m_type;
		if (right_type.isFunctionWrapper()) {
			right_type = right_type.unwrap();
		}
		
		
		// TODO just reuse the existing code
	
		IGSource parent = ((IGMember)(right.m_scope)).m_parent;
		
		String parent_type = type(s, parent.getInstanceDataType());
		String parent_type_no_pointer = parent_type.substring(0, parent_type.length() - 1);
		
		String pre = "Function<" + parent_type_no_pointer;
		// okay now assemble the parameters
		
		// the return type
		pre += "," + type(s, right_type.m_right);
		
		for (Type t :right_type.m_left.m_types) {
			pre += "," + type(s, t);
		}
		
		
		pre += ">(";
		
		
		if (right.m_children.size() == 0) {
			pre += "this";
			pre += ", &" + parent_type_no_pointer + "::";
		
			String post = ")";
		
			pw.prepend(right, pre);
		
			pw.append(right, post);
		}
		else
		{
			String pre2 = "";
			pre2 += "";
			pre2 += ", &" + parent_type_no_pointer + "::";
		
			String post = ")";
		
		
			pw.prepend(right, pre);
			pw.replace(right.m_token, pre2);
			pw.append(right, post);
		
		}
	}
	
	/*
	 * Marking up node
	 */
	
	private void markup2(TokenWriter pw, IGSource s, IGNode node)
	{
		/*
		 we should never be examining individual tokens ... but instead look at the node type
		*/
	
		Token token = node.m_token;
		
		
		if (node.m_node_type == IGNode.SCOPE_CHAIN_START) 
		{
			IGSource src = (IGSource)node.m_type.m_scope;
			pw.prepend(token, src.m_package.toStringWithSeperator("::","::",false) + "::");
		}
		else if (token.m_type == TOK_NULL && (node.m_node_type == IGNode.NODE_VALUE)) 
		{
			pw.replace(token, "NULL");
		}
		else if (token.m_type == IGLexer.LEXER_CHAR && (node.m_node_type == IGNode.NODE_VALUE))
		{
			pw.prepend(token, "'");
			pw.append(token, "'");
		}
		else if (token.m_type == IGLexer.LEXER_STRING  && (node.m_node_type == IGNode.NODE_VALUE))
		{
			pw.prepend(token, "iglang::const_string(L\"");
			pw.append(token, "\"," + getStringIndex(token.m_value) +  ")");
		}	
		else if ((node.m_node_type == IGNode.NODE_BINARY) &&
					 (token.m_type == TOK_LSL)) 
		{
			pw.replace(token, token.m_value.substring(1));	 
		}
		// all divisions actually return a float
		// we need to add in
		else if ((node.m_node_type == IGNode.NODE_BINARY) &&
					 (token.m_type == TOK_DIV)) 
		{
			//if (node.m_type == Type.get("double")) {
			//	pw.replace(token, "/(double)");	 
			//}
		}
		else if ((node.m_node_type == IGNode.NODE_NEW))
		{
			token.m_output = false;
			Type t = node.m_type;
			
			// disable the output for the tokens that compose the type
			IGNode type_node = node.getChild(0);
			for (int i = type_node.m_start_index; i < type_node.m_end_index; i++) {
				peekAt(i).m_output = false;
				
				// stop processing these tokens all together
				peekAt(i).discard();
			}
			
			// remove the "*" from the object type when creating a new instance
			String result = type(s, t);
			if (result.endsWith("*")) {
				result = result.substring(0, result.length() - 1);
			}
			pw.append(token, "(new " + result + ")->__ig_init");
		}
		else if ((node.m_node_type ==  IGNode.NODE_TYPE_CAST))
		{
			IGNode value = node.getChild(1);
			
			pw.disableOutput(node.getChild(0));
			//for (int i = token.m_token_index; i < value.m_start_index; i++) {
			//	peekAt(i).m_output = false;
			//}
			
			/*
			token.m_output = false;
			while (getTokenIndex() < value.m_start_index) {
				read().m_output = false;
			}
			*/
			
			// TODO catch this in the Validator
			String dest_type = type(s, node.m_type);
			if (dest_type.startsWith("source:")) {
				throw new RuntimeException("Borked the type on this cast");
				//dest_type = dest_type.substring(7) + "/*WTF IS THERE A SOURCE*/";
			}
			
			pw.append(token, "iglang::ig_cast< " + dest_type + "," + type(s, value.m_type) + " >");
		}
		// never print out var keywords
		else if (token.m_type == TOK_VAR)
		{
			pw.replace(token, "");
		}
		else if ((node.m_node_type == IGNode.NODE_INDEX))
		{
			pw.prepend(peekAt(node.m_start_index), "(*");
			pw.prepend(token, ")");
		}
		else if ((node.m_node_type ==  IGNode.ID_TYPE_PAIR))
		{
			// make the ':' no longer render
			pw.replace(token, "");
			
			pw.prepend(node.m_start_index, type(s, node.m_type) + " ");
			
			// hide the tokens that compose the type
			IGNode type_node = node.getChild(1);
			for (int i = type_node.m_start_index; i < type_node.m_end_index; i++) {
				peekAt(i).m_output = false;
			}
			
			
			if (!node.hasPredecessor(IGNode.NODE_CATCH))
			{
				// create a smart default if one is needed
				if (peekAt(node.m_end_index).m_type != TOK_ASSIGN && 
					peekAt(node.m_end_index).m_type != TOK_IN)
				{
					Type type = node.m_type;
					if (type == Type.get("int") || type == Type.get("uint") || type == Type.get("double"))
					{
						pw.prepend(node.m_end_index, " = 0");
					}
					else if (type == Type.get("bool")) {
						pw.prepend(node.m_end_index, " = false");
					}
					else if (type.isEnum()) {
						pw.prepend(node.m_end_index, " = 0");
					}
					else {
						pw.prepend(node.m_end_index, " = NULL");
					}
				}
			}
		}
		else if ((node.m_node_type == IGNode.NODE_DOT_GET)) 
		{
			IGNode left  = node.getChild(0);
			IGNode right = node.getChild(1);
			
			Type   left_type = left.m_type;
			if (left_type.isNamespace()) {
				pw.replace(token, "::");
			}
			else {
				pw.replace(token, "->");
			}
			
			// this is a much more complicated case
			pw.append(node.m_end_index - 1, "__get()");
		}
		else if (node.m_node_type == IGNode.NODE_LOCAL_GET)
		{
			pw.append(node.m_end_index - 1, "__get()");
		}
		else if ((node.m_node_type ==  IGNode.NODE_DOT_SET)) 
		{
			if (node.m_parent.m_token.m_type == TOK_ASSIGN) 
			{		
				IGNode left  = node.getChild(0);
				IGNode right = node.getChild(1);
			
				Type   left_type = left.m_type;
				if (left_type.isNamespace()) {
					pw.replace(token, "::");
				}
				else
				{
					pw.replace(token, "->");
				}
			}
			
		}
		else if (node.m_node_type == IGNode.NODE_PARAMETER_LIST)
		{
			/*
			for (int i = node.getChildCount() - 1; i >= 0; i--)
			{
				IGNode right = node.getChild(i);
				
				
				
				if (//(right.m_type.isFunctionPointer() 
					right.m_type.isFunctionWrapper() &&
					right.m_scope != null &&
					right.m_scope.getScopeType() == IGScopeItem.SCOPE_ITEM_FUNCTION ) {
				
					
					functionWrapper(s, right, pw);	
				}	
				
				if (//(right.m_type.isFunctionPointer() 
					right.m_type.isFunctionWrapper()&&
					right.m_scope != null &&
					right.m_scope.getScopeType() == IGScopeItem.SCOPE_ITEM_STATIC_FUNCTION ) {
					
					IGSource parent = ((IGMember)(right.m_scope)).m_parent;
					String parent_type = type(s, parent.getInstanceDataType());
					String parent_type_no_pointer = parent_type.substring(0, parent_type.length() - 1);

					String pre = "&";	// + parent_type_no_pointer + "::";		
					if (right.m_children.size() == 0) {		// HACK
						pre += parent_type_no_pointer + "::";
					}			
					pw.prepend(right.m_start_index, pre);
				}		
			}
			*/
		
		}
		else if (node.m_node_type == IGNode.PARAMETER_CAST)
		{
			IGNode right = node.getChild(0);
			
			if (//(right.m_type.isFunctionPointer() 
				right.m_type.isFunctionWrapper() &&
				right.m_scope != null &&
				right.m_scope.getScopeType() == IGScopeItem.SCOPE_ITEM_FUNCTION ) {
			
				
				functionWrapper(s, right, pw);	
			}	
			
			if (//(right.m_type.isFunctionPointer() 
				right.m_type.isFunctionWrapper()&&
				right.m_scope != null &&
				right.m_scope.getScopeType() == IGScopeItem.SCOPE_ITEM_STATIC_FUNCTION ) {
				
				IGSource parent = ((IGMember)(right.m_scope)).m_parent;
				String parent_type = type(s, parent.getInstanceDataType());
				String parent_type_no_pointer = parent_type.substring(0, parent_type.length() - 1);

				String pre = "&";	// + parent_type_no_pointer + "::";		
				if (right.m_children.size() == 0) {		// HACK
					pre += parent_type_no_pointer + "::";
				}			
				pw.prepend(right.m_start_index, pre);
			}				
		}
		else if (isAssignTokenType(token.m_type) && (node.m_node_type ==  IGNode.NODE_BINARY))
		{
			IGNode left  = node.getChild(0);
			IGNode right = node.getChild(1);
			
			if (token.m_type == TOK_ADD_ASSIGN)
			{
				if (left.m_type == Type.get("iglang.String"))
				{
					pw.prepend(left, "iglang::__StringL(&");
					pw.append(left, ")");
				}
			}
			
			/*
			if (node.m_type == Type.get("iglang.String")) 
			{
				if (token.m_type == TOK_ASSIGN) {
				
				}
				else if (token.m_type == TOK_ADD_ASSIGN)
				{
				
				token.m_output = false;
				pw.prepend(node.m_start_index, "iglang::String::__add(iglang::__toString(");
				pw.append(token, "),iglang::__toString(");
				
				//System.out.println("Testing __add");
				//peekAt(node.m_end_index - 1).printError("THis is the terminator");
				
				pw.append(node.m_end_index - 1, "))");
				
				}
			}	
			*/
			
			
			
			boolean simple_set = (token.m_type == TOK_ASSIGN);
			boolean is_setter  = Util.hasNode(left, IGNode.NODE_DOT_SET) || 
								 Util.hasNode(left, IGNode.NODE_LOCAL_SET);
	
			String tmp_variable_type = "xxx";
			String tmp_variable = "__ig_tmp" + token.m_token_index;
			String tmp_setter = "";
			
			if (!simple_set && is_setter) {
			
				if (Util.hasNode(left, IGNode.NODE_DOT_SET)) 
				{
					//    LEFT        +=   RIGHT
					//(  A   .   B)   +=   RIGHT
				
						
					IGNode setter_container = left.getChild(0);
					IGSource vs = (IGSource) setter_container.m_type.m_scope;
					
					tmp_variable_type = vs.m_package.toStringWithSeperator("::","::",false) + "::" +
										vs.m_expected_component + " * ";
										
					tmp_setter = left.getChild(1).m_token.m_value;
										
										
					pw.replace(left.m_token, "");					
					pw.replace(left.getChild(1).m_token, "; " + tmp_variable + "->" + tmp_setter + "__set("
												  + tmp_variable + "->" + tmp_setter + "__get() ");
												  
					// convert -= and +=  to  - and +							  
					pw.replace(token, token.m_value.substring(0,1));
					
				}
				else {
					throw new RuntimeException("Unsupported");
				}
				
				pw.prepend(node,  tmp_variable_type + " " + tmp_variable + " = ");

			}
			else if (is_setter) 
			{
				pw.append(left.m_end_index-1, "__set(");
			}
			
			
			if (right.m_scope != null && 
				! Util.hasNode(right, IGNode.NODE_DOT_GET) &&
				(right.m_scope.getScopeType() == IGScopeItem.SCOPE_ITEM_STATIC_FUNCTION ||
				 right.m_scope.getScopeType() == IGScopeItem.SCOPE_ITEM_FUNCTION))
			{
				// an explicit call is only required for functions
				if (right.m_scope.getScopeType() == IGScopeItem.SCOPE_ITEM_FUNCTION) {
				
					System.out.println("" + right.m_type);
					System.out.println("" + right.m_scope);
					
					functionWrapper(s, right, pw);

				}
			}
			
			if (is_setter) 
			{
				pw.append(node.m_end_index-1, ")");
				
				if (simple_set) {
					token.m_output = false;
				}
			}
		}
		else if ((node.m_node_type ==  IGNode.NODE_DOT))
		{
			Type   left_type = node.getChild(0).m_type;
			if (left_type.isNamespace() || node.getChild(0).m_token.m_value.equals("super")) {
				pw.replace(token, "::");
			}
			else
			{
				pw.replace(token, "->");
			}
		}
		else if ((node.m_node_type ==  IGNode.NODE_TYPE_FOREACH))
		{
			// convert
			// for (var s : String in map) {
			// }
			// =>
			// iglang::String * s;
			// iglang::Map<iglang::String *, iglang::String *> * __ig_tmp = map->__it_start();
			// while (__ig_tmp->__it_next(s)) {
			// }
			
			
			IGNode value 	= node.getChild(0);
			IGNode data_set = node.getChild(1);
		
			IGVariable v = null;
		
			if (value.m_node_type == IGNode.NODE_TYPE_LOCAL_VARIABLE_DEFINITION) 
			{
				v = (IGVariable)value.m_scope;
			}
			else if (value.m_node_type == IGNode.LOCAL_VARIABLE_ACCESS)
			{
				v = (IGVariable)value.m_scope;					
			}
			else
			{
				error(token, "Invalid value variable: " + value.m_type);
			}
		
		
			// ohh my god what to do in this case
			token.m_output = false;		// hide the for
			
			IGNode left_variable = node.getChild(0);
			IGNode right_in      = node.getChild(1);
			IGNode block         = node.getChild(2);
			
			pw.prepend(token, "{");
			peekAt(node.m_start_index + 1).m_output = false;	// hide the ( following the for
			peekAt(right_in.m_end_index)  .m_output = false; // hide the ) at the end of the for
			
			// this should really be at the end of the block  TODO TODO TOD TODO
			//pw.append(node.m_end_index - 1, "{");
			
			String tmp_variable = "__ig_tmp" + token.m_token_index;

			pw.append(left_variable.m_end_index - 1, "; "  + iteratorType(s, right_in.m_type) + tmp_variable + " = ");
			

			right_in.m_token.m_output = false;		// hide the in
			pw.append(right_in.m_end_index - 1, "->__iterator(); while(" + tmp_variable + ".next(" + v.m_name.m_value + "))");
			
			
			
			pw.append(block.m_end_index - 1, "}");
			
		}
		else if (token.m_type == IGLexer.LEXER_NATIVE_BLOCK) 
		{
			if (token.m_value.startsWith("@cpp")) {
				pw.append(token, token.m_value.substring(4));
			}
			token.m_output = false;
		}
		else if (token.m_type == TOK_ASR && node.m_node_type == IGNode.NODE_BINARY)
		{
			IGNode left = node.getChild(0);
			IGNode right = node.getChild(1);
			token.m_output = false;
			pw.append(token, ",");
			pw.prepend(node.m_start_index, "iglang::ig_asr(");
			pw.append(node.m_end_index - 1, ")");
		
		}
		else if (token.m_type == TOK_LSR && node.m_node_type == IGNode.NODE_BINARY)
		{
			IGNode left = node.getChild(0);
			IGNode right = node.getChild(1);
			token.m_output = false;
			pw.append(token, ",");
			pw.prepend(node.m_start_index, "iglang::ig_lsr(");
			pw.append(node.m_end_index - 1, ")");
		}
		else if (token.m_type == TOK_ADD && (node.m_node_type ==  IGNode.NODE_BINARY))
		{
			IGNode left = node.getChild(0);
			IGNode right = node.getChild(1);
			
			
			if (node.m_type == Type.get("iglang.String")) 
			{
				token.m_output = false;
				pw.prepend(node.m_start_index, "iglang::String::__add(iglang::__toString(");
				pw.append(token, "),iglang::__toString(");
				
				//System.out.println("Testing __add");
				//peekAt(node.m_end_index - 1).printError("THis is the terminator");
				
				pw.append(node.m_end_index - 1, "))");
			}	
		}
		else if (token.m_type == TOK_EQ && (node.m_node_type == IGNode.NODE_BINARY))
		{
			IGNode left = node.getChild(0);
			IGNode right = node.getChild(1);
			
			if (left.m_type  == Type.get("iglang.String") &&
				right.m_type == Type.get("iglang.String"))
			{
				token.m_output = false;
				pw.prepend(node.m_start_index, "iglang::__equals(");
				pw.append(token, ",");
			
				pw.append(node.m_end_index - 1, ")");
				
			}
		}
		else if (token.m_type == TOK_NE && (node.m_node_type == IGNode.NODE_BINARY))
		{
			IGNode left = node.getChild(0);
			IGNode right = node.getChild(1);
			
			if (left.m_type  == Type.get("iglang.String") &&
				right.m_type == Type.get("iglang.String"))
			{
				token.m_output = false;
				pw.prepend(node.m_start_index, "(!iglang::__equals(");
				pw.append(token, ",");

				pw.append(node.m_end_index - 1, "))");
				
			}
		}		
		else if (token.m_type == TOK_ADD_ASSIGN && (node.m_node_type ==  IGNode.NODE_BINARY))
		{
			IGNode left = node.getChild(0);
			IGNode right = node.getChild(1);
			
			
			//iglang::
			if (left.m_type == Type.get("iglang.String")) 
			{
				token.m_output = false;
				pw.prepend(node.m_start_index, "iglang::String::__append(");
				pw.append(token, ",");
				pw.append(node.m_end_index - 1, ")");
			}
		}
	}
	
	
	/*
	 * Output the contents of a class as a cpp file
	 */

	public  void outputClass(Vector<IGSource> sources, IGPackage root, IGSource s) throws IOException
	{
		setSource(s.m_token_list, 0, s.m_token_list.length);
		TokenWriter pw = Util.createOutputPrintWriter(s, ".cpp");
		startNameSpace(sources, pw, s, true);
		
		//Hashtable<Integer,String> to_append = new Hashtable<Integer,String>();
	
		while (peekTokenType() != TOK_EOF)
		{
			/////////////////////////////////////////////
			// seek to the start of the next function or class/member variable
			/////////////////////////////////////////////
			while (peekTokenType() != TOK_EOF) {
				if (peekAnnotation() == ANNOTATION_FUNCTION_START || 
					peekAnnotation() == ANNOTATION_VARIABLE_START) {
					break;
				}
				pw.print(read().m_whitespace);
			}
			
			if (peekAnnotation() == ANNOTATION_FUNCTION_START) 
			{
				outputCppFunction((TokenWriter)pw, s);
			}
			else if (peekAnnotation() == ANNOTATION_VARIABLE_START) 
			{
				outputCppVariable((TokenWriter)pw, s);
			}
			
		}
		
		pw.println();
		
		pw.println("void " + s.m_expected_component + "::__gc_mark_children(int pass)");
		pw.println("{");
		pw.println("\tsuper::__gc_mark_children(pass);");
		
		for (IGMember m : s.m_members) {
			if (m.m_type == IGMember.TYPE_VAR) {
				if (!m.m_return_type.isPrimitive() && !m.m_return_type.isEnum() && !m.hasModifier(IGMember.STATIC)) 
				{
					pw.println("\t\tiglang::__gc_mark_templated(" + m.m_name.m_value + ");");
					
					/*
					if (m.m_return_type.isFunctionPointer())
					{
						//pw.println("\tif (NULL != " + m.m_name.m_value + "){");
						pw.println("\t\t" + m.m_name.m_value + ".__gc_mark(pass);");
						//pw.println("\t}");
					}
					else
					{
						pw.println("\tif (NULL != " + m.m_name.m_value + "){");
						pw.println("\t\t ((Object *)" + m.m_name.m_value + ")->__gc_mark(pass);");
						pw.println("\t}");
						
						?? need to be able to determine if this is a pointer
						
						pw.println("\tif (NULL != " + m.m_name.m_value + "){");
						pw.println("\t\t dynamic_cast<iglang::Object *>(" + m.m_name.m_value + ")->__gc_mark(pass);");
						pw.println("\t}");
					}
					*/
				}
			}
		}
		
		pw.println("}");
		
		////////////////////////////////
		// mark s function for GC
		//////////////////////////////
		{
			pw.println("void " + s.m_expected_component + "::__gc_mark_statics(int pass)");
			pw.println("{");
			for (IGMember m : s.m_members) {
				if (m.m_type == IGMember.TYPE_VAR) {
					if (!m.m_return_type.isPrimitive() && 
						!m.m_return_type.isEnum() && m.hasModifier(IGMember.STATIC)) {
						
						pw.println("\t\tiglang::__gc_mark_templated(" + m.m_name.m_value + ");");
						
						/*
						if (m.m_return_type.isFunctionPointer())
						{
							//pw.println("\tif (NULL != " + m.m_name.m_value + "){");
							pw.println("\t\t" + m.m_name.m_value + ".__gc_mark(pass);");
							//pw.println("\t}");
						}
						else
						{
							pw.println("\tif (NULL != " + m.m_name.m_value + "){");
							pw.println("\t\t ((Object *)" + m.m_name.m_value + ")->__gc_mark(pass);");
							pw.println("\t}");
							
							pw.println("\tif (NULL != " + m.m_name.m_value + "){");
							pw.println("\t\t dynamic_cast<iglang::Object *>(" + m.m_name.m_value + ")->__gc_mark(pass);");
							pw.println("\t}");
						}
						*/
					}
				}
			}
			pw.println("}");
		}
		
		
		////////////////////////////////////////
		// default constructor if none exists
		////////////////////////////////////////
		if (!s.hasConstructor()) {
			pw.println(s.m_expected_component + " * " + s.m_expected_component + "::"+
					"__ig_init() { super::__ig_init(); return this; }");
		}
		
		////////////////////////////////////////
		// initialize object defaults
		// - called by root Object constructor
		////////////////////////////////////////
		{
			pw.println("void " + s.m_expected_component + "::__iglang_object_defaults()/* this is called by Object's constructor*/");
			pw.println("{");
			pw.println("\tsuper::__iglang_object_defaults();");
			for (IGMember m : s.m_members) {
				if (m.m_type == IGMember.TYPE_VAR) {
					if (!m.hasModifier(IGMember.STATIC)) {
						pw.print("\t" + m.m_name.m_value);
						
						outputMemberBody(pw, s.m_token_list, m, s);
											
						pw.println(";");
					}
					
					
				}
			}		
			pw.println("}");
		}
		
		
		
		
		endNameSpace(pw, s);
		
		pw.close();
	}



	private  String type2(IGSource source, Type type)
	{
		assert type != null : "Um this shouldn't ever happen";
		
		String kv = type.m_primary;
		return kv.replace(".", "::");
	}

	private String iteratorType(IGSource source, Type type)
	{
		String kv = null;
	
		if (type.m_primary.equals("iglang.Map")) {
			kv = "iglang::Map__Iterator<" + type(source, type.m_left) + "," +
					type(source, type.m_right) + ">";
		}
		else if (type.m_primary.equals("iglang.Array")) {
			kv = "iglang::Array__Iterator<" + type(source, type.m_left) + ">";
		}
		else if (type.m_primary.equals("iglang.Vector")) {
			kv = "iglang::Vector__Iterator<" + type(source, type.m_left) + ">";
		}
	
		return kv;
	}


	private  String type(IGSource source, Type type)
	{
		assert type != null : "Um this shouldn't ever happen";
		
		String kv = "";
		
		if (type.isEnum()) {
			kv = "iglang::ig_int";
		}
		else if (type.isPrimitive()) {
			kv = "iglang::ig_" + type.m_primary;
		}
		else if (type.m_primary.equals("StaticFunction")) {
			int arg_count = type.m_left.m_types.length;
			
			String value = "iglang::StaticFunction" + arg_count + "<" + 
					type(source, type.m_right);

			for (int i = 0; i < type.m_left.m_types.length; i++) 
			{
				value += ",";
				value += type(source, type.m_left.m_types[i]);
			}
			kv = value + ">";
		}
		else if (type.m_primary.equals("Function")) {
		
			int arg_count = type.m_left.m_types.length;
			
			String value = "iglang::Function" + arg_count + "<" + 
					type(source, type.m_right);

			for (int i = 0; i < type.m_left.m_types.length; i++) 
			{
				value += ",";
				value += type(source, type.m_left.m_types[i]);
			}
			kv = value + ">";
		}
		else if (type.m_primary.equals("iglang.Map")) {
			kv = "iglang::Map<" + type(source, type.m_left) + "," +
					type(source, type.m_right) + "> *";
		}
		else if (type.m_primary.equals("iglang.Array")) {
			kv = "iglang::Array<" + type(source, type.m_left) + "> *";
		}
		else if (type.m_primary.equals("iglang.Vector")) {
			kv = "iglang::Vector<" + type(source, type.m_left) + "> *";
		}
		
			
		else {
			kv = type.m_primary + " *";
		}
		
		return kv.replace(".", "::");
	}
	
	
	private  int s_current_protection = 0;
	
	private  void newHeaderSource()
	{
		s_current_protection = 0;
	}
	
	
	private  void protection(PrintWriter pw, IGMember m)
	{
		int protection = 0;
		if (m.hasModifier(IGMember.PUBLIC)) 
		{
			protection = IGMember.PUBLIC;
		}
		else if (m.hasModifier(IGMember.PROTECTED)) {
			protection = IGMember.PROTECTED;
		}
		else if (m.hasModifier(IGMember.INTERNAL)) {
			protection = IGMember.PUBLIC;
		}
		else if (m.hasModifier(IGMember.PRIVATE)) {
			protection = IGMember.PRIVATE;
		}
		else
		{
			protection = IGMember.PRIVATE;
		}
		
		if (s_current_protection != protection) {
			s_current_protection = protection;
			
			if (protection == IGMember.PRIVATE) {
				pw.println("\nprivate:");
			}
			else if (protection == IGMember.PUBLIC) {
				pw.println("\npublic:");
			}
			else if (protection == IGMember.PROTECTED) {
				pw.println("\nprotected:");
			}
		}
	}
	
	private void predeclare(PrintWriter pw, IGSource s)
	{
		pw.println();
		pw.println("#ifndef predeclare_" + s.m_guid);
		pw.println("#define predeclare_" + s.m_guid + " 1");
		
		{
			IGScopePath sp = s.m_package;
			for (Token t : sp.getTokens()) {
				pw.print("namespace " + t.m_value + "{ ");
			}
			//pw.println();
		}
		
		//s.m_package.toStringWithSeperator("::", "::", true) + 
		pw.print("class " + s.m_expected_component + ";");
		
		{
			IGScopePath sp = s.m_package;
			for (Token t : sp.getTokens()) {
				pw.print(" }");
			}
			pw.println();
		}
		
		
		pw.println("#endif"); 
	}
	
	/*
	 * startNameSpace
	 * @param add_using - actually means is it in a .cpp file
	 */
	
	
	private  void startNameSpace(Vector<IGSource> sources, PrintWriter pw, IGSource s, boolean add_using)
	{
	
		IGScopePath source_scope = s.m_package;
	
		String rel = source_scope.getRelativePathToRoot();
		
		if (!add_using) {
			for (IGSource s2 : sources) {
				//predeclare(pw, s2);
			}
		}
		
		
		// Default Includes
		/////////////////////
		
		if (add_using)
		{
			pw.println("#include \"" + rel + "__iglang__.h\"");
		}
		
	
		// build up the namespace heirarchy	
		{
			IGScopePath sp = s.m_package;
			for (Token t : sp.getTokens()) {
				pw.print("namespace " + t.m_value + "{ ");
			}
			pw.println();
		}
		
		if (add_using)
		{
			pw.println("using namespace iglang;");
		
		}
	}
	
	private  void endNameSpace(PrintWriter pw, IGSource s)
	{
		IGScopePath sp = s.m_package;
		for (Token t : sp.getTokens()) {
			pw.print("}");
		}
		pw.println();
	}
	
	private void addPackageIncludes(IGPackage root, Vector<IGSource> all_sources, PrintWriter pw) throws IOException
	{
	
		if (root.m_child_sources.size() > 0)
		{
			ArrayList<Token> tokens = root.getScopePath().getTokens();
			if (tokens.get(0).m_value.equals("iglang") ||
			    tokens.get(0).m_value.equals("platform")) {
			 	
			 	return;   
			}
			
			
			for (Token t : root.getScopePath().getTokens()) {
				pw.print("namespace " + t.m_value + "{ ");
			}
			pw.println();
			
			
			
			for (IGSource source : root.m_child_sources)
			{
				pw.println("class " +  source.m_expected_component + ";");
			}
			
			for (Token t :  root.getScopePath().getTokens()) {
				pw.print("} ");
			}
			pw.println();
			
			all_sources.addAll(root.m_child_sources);
		}
	
		for (IGPackage child : root.m_child_packages)
		{	
			addPackageIncludes(child, all_sources, pw);
		}
	
	}
	
	public  void createEntryPoint(IGPackage root, Vector<IGSource> has_statics) throws IOException
	{
		PrintWriter pw = Util.createOutputPrintWriter(root, "__iglang__.cpp");
		
		pw.println("#include \"__iglang__.h\"");
		
		
		pw.println("void __iglang__init() {");
		pw.println("}");
		pw.println("");
		
		
		pw.println("void __iglang__gc(iglang::ig_bool force, void (* cbk)() ) {");
		pw.println("\tif (!iglang::Object::__gc_begin(force)) { return; }");
		pw.println("\tcbk();");
		for (IGSource source : has_statics)
		{
			pw.print("\t");
			for (Token t : source.getScopePath().getTokens()) {
				pw.print(t.m_value + "::");
			}
			pw.println("__gc_mark_statics(0);");
		
		}
		pw.println("\tiglang::Object::__gc_end();");		
		pw.println("}");
		
		
		
		pw.close();
	} 

	public  void createPackageHeaders(IGPackage root, boolean primary) throws IOException
	{
	
		if (primary)
		{
			PrintWriter pw = Util.createOutputPrintWriter(root, "__iglang__.h");
			String sentinel = Util.getGUID();
			
			pw.println("#ifndef " + sentinel);
			pw.println("#define " + sentinel + " 1");
			pw.println();
			pw.println("//pre-declare every class");
			
			pw.println("#include \"iglang/__package__.h\""); 
			pw.println("#include \"platform/__package__.h\"");
		
		
			pw.println("void __iglang__gc(iglang::ig_bool force, void (*cbk)());");
		
		
			Vector<IGSource> all_sources = new Vector<IGSource>();
		
			addPackageIncludes(root, all_sources, pw);
		
		
		
		
			Hashtable<IGSource,Boolean> marks = new Hashtable<IGSource,Boolean>();
			Vector<IGSource> sources = all_sources;
			
			boolean has_pending = true;
			
			while (has_pending)
			{
				has_pending = false;
				
				for (IGSource source : sources)
				{
					if (!marks.containsKey(source)) {
						has_pending = true;
					}
					
					Vector<IGSource> to_check = new Vector<IGSource>();
					if (null != source.m_class_extends)
					{
						IGSource src = (IGSource)(source.m_class_extends.m_scope);
						if (sources.contains(src) && !marks.containsKey(src)) {
							to_check.add(src);
						}
					}
				
					for (Type type : source.m_class_implements) {
						IGSource src = (IGSource)type.m_scope;
						if (sources.contains(src) && !marks.containsKey(src)) { 
							to_check.add(src);
						}
					}
			
					if (to_check.size() == 0 && !marks.containsKey(source)) {
						marks.put(source, true);
						pw.println("#include \"" +  source.m_package.toStringWithSeperator("/","/",true) + "/" + 
									 source.m_expected_component + ".h\"");
					}	
				}
			}
		
		
		
			pw.println("#endif");
			pw.close();	
		}
	
	
		if (root.m_child_sources.size() > 0)
		{
			PrintWriter pw = Util.createOutputPrintWriter(root, "__package__.h");
			String sentinel = Util.getGUID();
			
			pw.println("#ifndef " + sentinel);
			pw.println("#define " + sentinel + " 1");
			
			for (Token t : root.getScopePath().getTokens()) {
				pw.print("namespace " + t.m_value + "{ ");
			}
			pw.println();
			
			
			
			for (IGSource source : root.m_child_sources)
			{
				pw.println("class " +  source.m_expected_component + ";");
			}
			
			for (Token t :  root.getScopePath().getTokens()) {
				pw.print("} ");
			}
			pw.println();
			
			
			/*
			
			Hashtable<IGSource,Boolean> marks = new Hashtable<IGSource,Boolean>();
			Vector<IGSource> sources = root.m_child_sources;
			
			boolean has_pending = true;
			
			while (has_pending)
			{
				has_pending = false;
				
				for (IGSource source : sources)
				{
					if (!marks.containsKey(source)) {
						has_pending = true;
					}
					
					Vector<IGSource> to_check = new Vector<IGSource>();
					if (null != source.m_class_extends)
					{
						IGSource src = (IGSource)(source.m_class_extends.m_scope);
						if (sources.contains(src) && !marks.containsKey(src)) {
							to_check.add(src);
						}
					}
				
					for (Type type : source.m_class_implements) {
						IGSource src = (IGSource)type.m_scope;
						if (sources.contains(src) && !marks.containsKey(src)) { 
							to_check.add(src);
						}
					}
			
					if (to_check.size() == 0 && !marks.containsKey(source)) {
						marks.put(source, true);
						pw.println("#include \"" +  source.m_expected_component + ".h\"");
					}	
				}
			}
			
			*/
			
			pw.println("#endif");
			pw.close();	
		}
		
		for (IGPackage child : root.m_child_packages)
		{	
			createPackageHeaders(child, false);
		}
	
	}



	private Vector<String> m_string_list            = new Vector<String>();
	private StringIntHashtable m_string_map = new StringIntHashtable();
	
	private int getStringIndex(String s) {
		if (!m_string_map.containsKey(s)) {
			m_string_map.put(s, m_string_list.size());
			m_string_list.add(s);
		}
		
		return m_string_map.get(s);
	}


	public  void run(IGPackage root, Vector<IGSource> sources)
	{	
	
		
	
	
		// create the header
		
		Vector<IGSource> has_statics = new Vector<IGSource>();
		
		
		try
		{
			createPackageHeaders(root, true);
			
			
			
			
			//////////////////////////////////////////////////
			// Create header files
			//////////////////////////////////////////////////
			
			for (IGSource s : sources)
			{
				System.out.println("creating header: " + s.m_expected_component);
				newHeaderSource();
			
				TokenWriter pw = Util.createOutputPrintWriter(s, ".h");
				String sentinel = s.m_guid;
			
			
				pw.println("#ifndef " + sentinel);
				pw.println("#define " + sentinel + " 1");
				
				startNameSpace(sources, pw, s, false);

				// cheap hack to remove static object constants
				for (IGMember m : s.m_members) 
				{
					if (m.m_type == IGMember.TYPE_VAR) 
					{	
						if (m.hasModifier(IGMember.FINAL)) {
							if (!m.m_return_type.isPrimitive()) {
								m.clearModifier(IGMember.FINAL);
							}
						}
					}
				}		
			
				

			
				if (s.m_type == IGScopeItem.SCOPE_ITEM_ENUM)
				{
					pw.println("class " + s.m_name.m_value);
					pw.println("{");
					pw.println("public:");
					
					boolean first = true;
					for (IGMember m : s.m_members) 
					{
						if (!first) {
						}
						first = false;
						
						String enum_type = "int";
						pw.println("\tstatic const " + enum_type + " " + m.m_name.m_value + " = " + m.m_return_value_range.toCPPString() + ";");
					}
					pw.println("\tstatic iglang::String * toString(int e); ");
					pw.println("};");

				}
				
				// todo force all enums to be public
				else if (s.m_type == IGScopeItem.SCOPE_ITEM_INTERFACE )
				{
					pw.println("class " + s.m_name.m_value + " : virtual public iglang::InterfaceObject");
					pw.println("{");
					pw.println("public:");
					for (IGMember m : s.m_members) 
					{
						//Log.log("Parsing IGMember: " + m.m_name.m_value);

					
						if (m.m_type == IGMember.TYPE_FUNCTION) 
						{
							pw.print("\t");
	
							pw.print("virtual ");
				
							pw.print(type(s, m.m_return_type));
							pw.print(" ");
							pw.print(m.m_name.m_value);
							pw.print("(");
							
							ArrayList<IGVariable> parameters = m.m_parameters;
							for (int i = 0; i < parameters.size(); i++) {
								if (i != 0) {
									pw.print(",");
								}
								IGVariable v = parameters.get(i);
								pw.print(type(s, v.m_type));
								pw.print(" ");
								pw.print(v.m_name.m_value);
							}
							
							
							pw.print(")");
							
							
							pw.println(" = 0;");
						}
					
					}
					
					
					// end it
					
					//pw.println("public:");
					//pw.println("\t//GC CODE ");
					//pw.println("\tvirtual void __gc_mark(int pass) = 0;");
					//pw.println("\tvirtual void __gc_mark_children(int pass) = 0;");
					
					pw.println("};");			
				}
				else if (s.m_type == IGScopeItem.SCOPE_ITEM_CLASS)
				{
					pw.print("class " + s.m_name.m_value);
					if (s.m_class_extends != null ||
						s.m_class_implements.size() > 0) {
						
						pw.print(":");	
						
						boolean first = false;
						if (s.m_class_extends != null) 
						{
							pw.print("public " + type2(s, s.m_class_extends));
							first = false;
						}
						
						for (Type t : s.m_class_implements) {
							if (!first) {
								pw.print(",");
							}
							first = false;
							
							pw.print("public " + type2(s, t));
						}
						
					}
					pw.println();
					
					pw.println("{");
					
					pw.println("private:");
					if (s.m_class_extends != null) 
					{
						pw.println("\ttypedef " + type2(s, s.m_class_extends) + " super;");
					}
					pw.println();

					for (IGMember m : s.m_members) 
					{
						Log.log("Parsing IGMember: " + m.m_name.m_value);
						protection(pw, m);
					
						if (m.m_type == IGMember.TYPE_VAR) 
						{	
							pw.print("\t");
							if (m.hasModifier(IGMember.STATIC)) {
								pw.print("static ");
							}
							if (m.hasModifier(IGMember.FINAL)) {
								pw.print("constexpr ");
							}
							pw.print (type(s, m.m_return_type));
							pw.print (" ");
							pw.print (m.m_name.m_value);
							
							if (m.hasModifier(IGMember.FINAL)) {
								outputMemberBody(pw, s.m_token_list, m, s);
							}
							
							pw.println(";"); 
						}
						else if (m.m_type == IGMember.TYPE_FUNCTION) 
						{
							boolean constructor = m.m_name.m_value.equals("<constructor>");
						
							pw.print("\t");
							if (m.hasModifier(IGMember.STATIC)) 
							{
								pw.print("static ");
								// technically c++ doesn't allow virtual s
								// in the same way that as3 does
							}
							else
							{
								//if (m.hasModifier(IGMember.VIRTUAL) ||
								//	m.hasModifier(IGMember.OVERRIDE)) {
								if (!constructor && m.hasModifier(IGMember.VIRTUAL) ) {
									pw.print("virtual ");
								}
								//}
							}
							
							if (!constructor) {
								pw.print(type(s, m.m_return_type));
							}			
							else
							{
								pw.print(s.m_expected_component + "*");
							}			
							
							pw.print(" ");
							
							if (constructor) {
								pw.print("__ig_init");	
							}	
							else
							{
								pw.print(m.m_name.m_value);
							}
							pw.print("(");
							
							ArrayList<IGVariable> parameters = m.m_parameters;
							for (int i = 0; i < parameters.size(); i++) {
								if (i != 0) {
									pw.print(",");
								}
								IGVariable v = parameters.get(i);
								pw.print(type(s, v.m_type));
								pw.print(" ");
								pw.print(v.m_name.m_value);
								
								if (v.m_has_default) 
								{
									pw.print("=");
									
									IGValueRange vr = v.getDefaultValueRange();
									if (vr.m_type == IGValueRange.TYPE_INT) {
										pw.print(vr.intValue());
									}
									else if (vr.m_type == IGValueRange.TYPE_DOUBLE) {
										pw.print(vr.doubleValue());
									}
									else if (vr.m_type == IGValueRange.TYPE_POINTER_NULL) {
										pw.print("NULL");
									}
									else if (vr.m_type == IGValueRange.TYPE_BOOL_FALSE) {
										pw.print("0");
									}
									else if (vr.m_type == IGValueRange.TYPE_BOOL_TRUE) {
										pw.print("1");
									}
									else {
										throw new RuntimeException("Invalid parameter value range");
									}
									
									//Token default_value = v.m_default_value;
									//if (default_value.m_type == TOK_NULL) {
									//	pw.printWhitespace(default_value);
									//	pw.print("NULL");
									//}
									//else
									//{
									//	pw.print(default_value);
									//}
								}
								// ????
							}
							
							
							pw.print(")");
							
							
							pw.println(";");
						}
					
					}
					
					pw.println("public:");
					
					////////////////////////////////////////
					// default constructor if none exists
					////////////////////////////////////////
					if (!s.hasConstructor()) {
							pw.println(s.m_expected_component + " * "+
					"__ig_init();");
						}		
					// end it
					
					
					pw.println("\t//GC CODE ");
					pw.println("\tvirtual void __gc_mark_children(int pass); ");
					pw.println("\tstatic void    __gc_mark_statics(int pass); ");
					pw.println("\t//INITIALIZER CODE");
					pw.println("\tvirtual void __iglang_object_defaults(); ");
					pw.println("};");
					
					has_statics.add(s);
					
					// output the body of the class
					// 
					
					System.out.println("creating source: " + s.m_expected_component);		
					outputClass(sources, root, s);
					
				}
				
				endNameSpace(pw, s);
				
				pw.println("#endif");
				pw.close();	
			}
			
			
			createEntryPoint(root, has_statics);
			
		}
		catch (IOException ex)
		{
			ex.printStackTrace();
		}
	
	}
}