/*
Copyright (c) 2014-2017 Incubator Games Ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

package pass2;

import pass1.*;
import java.io.*;

import util.TextReader;


public class Util
{
	/**
	 * Old school file copy.  Have been having issues with new File.copy 
	 * on Windows.
	 */

	public static void copyFile(String source, File dest) throws IOException {
	    InputStream is = null;
	    OutputStream os = null;
	    try {

	    	if (source.startsWith("jar:")) {
	    		is = Util.class.getClassLoader().getResourceAsStream(
	    			source.substring(4));	
	    	}
	    	else
	    	{		
	        	is = new FileInputStream(new File(source));
	     	}
	        os = new FileOutputStream(dest);
	        byte[] buffer = new byte[1024];
	        int length;
	        while ((length = is.read(buffer)) > 0) {
	            os.write(buffer, 0, length);
	        }
	    } 
	    catch (IOException ex) 
	    {
	    	ex.printStackTrace();
	    	throw ex;
	    }
	    finally {
	        if (is != null) is.close();
	        if (os != null) os.close();
	    }
	}

	public static TokenWriter createOutputPrintWriter(IGSource source, String extension) throws IOException
	{
		/*
		File pw_file = new File(source.m_dst_path + extension);
		try
		{
			new File(pw_file.getParent()).mkdirs();
		}
		catch (Exception ex) {
			// do nothing not really an error
		}
		*/
		TokenWriter pw = new TokenWriter(getDestFile(source, extension));
		return pw;
	}
	
	public static TokenWriter createCachedOutputPrintWriter(IGSource source, String extension) throws IOException
	{
		/*
		File pw_file = new File(source.m_dst_path + extension);
		try
		{
			new File(pw_file.getParent()).mkdirs();
		}
		catch (Exception ex) {
			// do nothing not really an error
		}
		*/
		TokenWriter pw = new TokenWriter(getDestFile(source, extension));
		return pw;
	}
	
	
	
	public static File getDestFile(IGSource source, String extension) throws IOException
	{
		File pw_file = new File(source.getDestinationPath() + extension);
		try
		{
			new File(pw_file.getParent()).mkdirs();
		}
		catch (Exception ex) {
			// do nothing not really an error
		}
		return pw_file;
	}
	
	
	public static TextReader getDestFileAsTextReader(IGSource source, String extension) throws IOException
	{
		File pw_file = new File(source.getDestinationPath() + extension);
		try
		{
			new File(pw_file.getParent()).mkdirs();
		}
		catch (Exception ex) {
			// do nothing not really an error
		}
		return new TextReader(new FileReader(pw_file));
	}
	
	/*
	public static PrintWriter createOutputPrintWriter(IGPackage source, Type resolved, String extension) throws IOException
	{
		File pw_file = new File(source.m_dst_path + extension);
		try
		{
			new File(pw_file.getParent()).mkdirs();
		}
		catch (Exception ex) {
			// do nothing not really an error
		}
		PrintWriter pw = new PrintWriter(pw_file);
		return pw;
	}
	*/
	
	public static PrintWriter createOutputPrintWriter(IGPackage source, String extension) throws IOException
	{
		File pw_file = new File(source.m_dst_path + extension);
		try
		{
			new File(pw_file.getParent()).mkdirs();
		}
		catch (Exception ex) {
			// do nothing not really an error
		}
		PrintWriter pw = new PrintWriter(pw_file);
		return pw;
	}
	
	public static boolean hasNode(Token t, int node_type)
	{
		return t.m_node != null && t.m_node.m_node_type == node_type;
	}
	
	public static boolean hasNode(IGNode t, int node_type)
	{
		return t != null && t.m_node_type == node_type;
	}
	
	public static String getGUID()
	{
		return "file_" + java.util.UUID.randomUUID().toString().replace("-","_");
	}

}