/*
Copyright (c) 2014-2017 Incubator Games Ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

package pass1;

public class IGTokenList
{
	public Token       m_token;
	public IGTokenList m_next;
	
	private static IGTokenList s_free = null;
	
	
	public IGTokenList(Token t) {
		m_token = t;
		m_next  = null;
	}

	public static IGTokenList alloc(Token t) {
		if (s_free == null) {
			return new IGTokenList(t);
		}
		else {
			IGTokenList item = s_free;
			s_free = item.m_next;
			
			item.m_token = t;
			item.m_next  = null;
			return item;
		}
	}
	
	public static void reclaim(IGTokenList list) {
		//System.out.println("reclaim A");
		while (list != null ) {
			IGTokenList tmp = list;
			list = list.m_next;
			
			tmp.m_next = s_free;
			s_free = tmp;
		
		}
		//System.out.println("reclaim B");		
	}
}