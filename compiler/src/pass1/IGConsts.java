/*
Copyright (c) 2014-2017 Incubator Games Ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

package pass1;

public class IGConsts
{
	public static final int	 TOK_ID			= IGLexer.LEXER_ID;
	
	
	public static final int	 IDX = 0;
	public static final int     TOK_TYPE         = 0;
	public static final int		TOK_PACKAGE 	 = 1;		//0
	public static final int		TOK_IMPORT 		 = 2;		//1
	public static final int		TOK_CLASS 		 = 3;		//2
	public static final int		TOK_FUNCTION 	 = 4;		//3
	public static final int		TOK_VAR 		 = 5;		//4
	public static final int		TOK_PUBLIC 		 = 6;		//5
	public static final int		TOK_INTERNAL 	 = 7;		//6
	public static final int		TOK_PRIVATE 	 = 8;		//7
	public static final int		TOK_PROTECTED 	 = 9;		//8
	public static final int		TOK_INT 		 = 10;		//9
	public static final int		TOK_UINT 		 = 11;		//10
	public static final int		TOK_INC			 = 12;		//11
	public static final int		TOK_DEC		 	 = 13;		//12
	public static final int		TOK_SHORT 		 = 14;		//13
	public static final int		TOK_IF 			 = 15;		//14
	public static final int		TOK_ELSE 		 = 16;		//15
	public static final int		TOK_FOR 		 = 17;		//16
	public static final int		TOK_WHILE 		 = 18;		//17
	public static final int		TOK_DO 			 = 19;		//18
	public static final int		TOK_VOID 		 = 20;		//19
	public static final int	    TOK_BYTE		 = 21;
	public static final int		TOK_DOT 		 = 22;
	public static final int		TOK_QUESTIONMARK = 23;
	public static final int		TOK_COLON 		 = 24;
	public static final int		TOK_SEMI 		 = 25;
	
	
	public static final int	 TOK_LSR			= 26;
	public static final int	 TOK_ASR			= 27;
	public static final int	 TOK_LSL           = 28;
	
	public static final int		TOK_ADD_ASSIGN 	= 29;
	public static final int		TOK_SUB_ASSIGN 	= 30;
	public static final int		TOK_MUL_ASSIGN 	= 31;
	public static final int		TOK_DIV_ASSIGN 	= 32;
	public static final int		TOK_MOD_ASSIGN 	= 33;
	public static final int	  TOK_XOR_ASSIGN = 34;
	
	public static final int	  TOK_EQ			= 35;
	public static final int	  TOK_NE			= 36;
	public static final int	  TOK_GE			= 37;
	public static final int	  TOK_LE			= 38;
	public static final int		TOK_ADD 		= 39;
	public static final int		TOK_SUB 		= 40;
	public static final int		TOK_MUL 		= 41;
	public static final int		TOK_DIV 		= 42;
	public static final int		TOK_MOD 		= 43;
	public static final int		TOK_LBRACE 		= 44;
	public static final int		TOK_RBRACE 		= 45;
	public static final int		TOK_LARRAY 		= 46;
	public static final int		TOK_RARRAY 		= 47;
	public static final int		TOK_LBRACKET 	= 48;
	public static final int		TOK_RBRACKET 	= 49;
	public static final int		TOK_LT 			= 50;
	public static final int		TOK_GT 			= 51;
	public static final int	 TOK_ASSIGN		= 52;
	public static final int	 TOK_COMMA		= 53;

	public static final int	 TOK_CONST		= 54;
	public static final int	 TOK_STATIC		= 55;
	public static final int	 TOK_EXTENDS		= 56;
	public static final int	 TOK_GET			= 57;
	public static final int	 TOK_SET			= 58;
	public static final int	 TOK_RETURN		= 59;
	public static final int	 TOK_NOT			= 60;
	public static final int	 TOK_AND_ASSIGN	= 61;
	public static final int	 TOK_OR_ASSIGN	= 62;
	public static final int	 TOK_AND			= 63;
	public static final int	 TOK_OR			= 64;
	
	public static final int	 TOK_BIT_AND		= 65;
	public static final int	 TOK_BIT_OR		= 66;
	public static final int	 TOK_BIT_NOT		= 67;
	public static final int	 TOK_BIT_XOR      = 68;
	
	public static final int	 TOK_NEW			= 69;
	public static final int		TOK_NUMBER 		= 70;
	public static final int		TOK_BOOLEAN 	= 71;
	public static final int	 TOK_SWITCH		= 72;
	public static final int	 TOK_CASE			= 73;
	public static final int	 TOK_DEFAULT		= 74;
	public static final int	 TOK_BREAK		= 75;
	public static final int	 TOK_NULL			= 76;
	public static final int	 TOK_CONTINUE		= 77;
    public static final int	 TOK_CLASS_TYPE    = 78;
	public static final int	 TOK_AS			= 79;
	public static final int	 TOK_THROW			= 80;
	//public static final int	 TOK_XOR_ASSIGN 		= ?;
	//public static final int	 TOK_XOR 			= ?;
	public static final int	 TOK_FUNCTION_TYPE = 81;
	public static final int	 TOK_STATIC_FUNCTION_TYPE = 82;
	public static final int	 TOK_INTERFACE = 83;
	public static final int	 TOK_IMPLEMENTS = 84;
	public static final int	 TOK_INLINE = 85;
	public static final int	 TOK_OVERRIDE = 86;
	public static final int	 TOK_VIRTUAL = 87;
	public static final int	 TOK_FINAL = 88;
	public static final int	 TOK_ENUM  = 89;
	public static final int	 TOK_TRY   = 90;
	public static final int	 TOK_CATCH = 91;
	public static final int	 TOK_IN    = 92;
	public static final int	 TOK_TRUE  = 93;
	public static final int	 TOK_FALSE = 94;
	
	public static final int	 TOK_FOR_EACH = 95;
	public static final int TOK_FLOAT = 96;
	public static final int TOK_FOR_EACH_IN_RANGE = 103;
	
	public static final int TOK_RANGE = 102;
	
	// distinction between regular brackets and those used to call functions
	//public static final int	 TOK_LFUNCTION = ?;
	//public static final int	 TOK_RFUNCTION = ?;
	
	/*
	// does this id refer to something on the stack
	public static final int	 TOK_ID_LOCAL_VARIABLE  = ?;
	public static final int	 TOK_ID_LOCAL_FUNCTION_PTR  = ?;
	public static final int	 TOK_ID_LOCAL_SFUNCTION_PTR = ?;
	
	// does this id refer to something in this
	public static final int	 TOK_ID_MEMBER_VARIABLE  = ?;
	public static final int	 TOK_ID_MEMBER_FUNCTION  = ?;
	public static final int	 TOK_ID_MEMBER_SFUNCTION = ?;
	*/
	
	// is the id just relative to the current scopes loaded
	public static final int	 TOK_ID_REL_SCOPE    = 96;
	// is the id relative to the stack
	public static final int	 TOK_ID_REL_STACK    = 97;
	// is the id relative to "this"
	public static final int	 TOK_ID_REL_THIS     = 98;
	// is the id relative to my class (static and otehrwise)
	public static final int	 TOK_ID_REL_THIS_CLASS = 99;
	
	
	// is the id relative to a class
	public static final int	 TOK_ID_REL_STATIC   = 100;
	// is the id relative to a member
	public static final int	 TOK_ID_REL_MEMBER   = 101;
	
	public static final int TOK_REF_COUNT = 102;		// #refcount
	
	public static final int TOK_ANON_FN    = 103;
	
	public static final int TOK_NATIVE     = 104;
	// Math.pi      TOK_ID_REL_SCOPE . TOK_ID_REL_STATIC
	// var x : int;   x = 5;     TOK_ID_REL_STACK
	// var y : Obj;   y.z = 22;   TOK_ID_REL_STACK . ID_REL_MEMBER

	public static final int TOK_SPECIAL = 105;
	
	public static final int	 TOK_EOF     		= IGLexer.LEXER_EOF;
	
	public static final int	 TOK_INLINE_C		= 255;
	
	
	
	
	
	
	public static final int ANNOTATION_NONE = 0;
	
	// if annotation & ANNOTATION_SCOPE_BIT != 0 then this is the start of a new scope
	//public static final int ANNOTATION_SCOPE_BIT       = 0x100;
	
	
	
	
	/// THESE ANNOTATIONS WERE HORRIBLE IDEAS
	/// TODO ... REMOVE ENTIRELY
	
	// 
	public static final byte ANNOTATION_PACKAGE			= 0;
	public static final byte ANNOTATION_IMPORT			= 1;
	public static final byte ANNOTATION_CLASS            = 2;
	public static final byte ANNOTATION_ENUM				= 3;
	public static final byte ANNOTATION_INTERFACE        = 4;
	
	// wrapeprs around the body of a class
	//public static final byte ANNOTATION_OBJECT_START     = 5;
	//public static final byte ANNOTATION_OBJECT_END       = 6;
	
	// high level (where a function or a variable in a class start)
	public static final byte ANNOTATION_FUNCTION_START  = 7;
	public static final byte ANNOTATION_VARIABLE_START  = 8;
	public static final byte ANNOTATION_ENUM_MEMBER       = 9;
	
	public static final byte ANNOTATION_BLOCK		     = 10;	
	//public static final byte ANNOTATION_FUNCTION_VARIABLE = 0x8000;		// parse var station
	//public static final byte ANNOTATION_EXPRESSION_START  = 0x8002;		// around an expression

	
	//public static final byte ANNOTATION_FOREACH           = 0x10001;
	//public static final byte ANNOTATION_FOR           	 = 0x10002;
	//public static final byte ANNOTATION_TRY				 = 0x10003;
	//public static final byte ANNOTATION_CATCH			 = 0x10004;
	//public static final byte ANNOTATION_WHILE			 = 11;
	//public static final byte ANNOTATION_DO				 = 12;
	//public static final byte ANNOTATION_RETURN			 = 0x10007;
	public static final byte ANNOTATION_THROW			 = 13;
	public static final byte ANNOTATION_IF 				 = 14;
	public static final byte ANNOTATION_ELSE_IF			 = 15;
	public static final byte ANNOTATION_ELSE				 = 16;
	public static final byte ANNOTATION_CONTINUE			 = 17;
	public static final byte ANNOTATION_BREAK			 = 18;
	public static final byte ANNOTATION_NATIVE_BLOCK		 = 19;
	public static final byte ANNOTATION_UNKNOWN = 127;
	
	// this does specify a godforsaken local variable
	public static final byte ANNOTATION_LOCAL_VARIABLE = 20;
}
