/*
Copyright (c) 2014-2017 Incubator Games Ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

package pass1;

import java.util.*;

public class IGScopeLookup
{
	public static final int RVALUE = 0;
	public static final int LVALUE = 1;

	boolean				m_member_function = true;
	int	 				m_value_type;
	int 				m_token_index;
	IGScopeItem         m_requestor;	// is this a static class or not?
										// also need to do checks for variable initializeation
										// as well as accessibility (public, private, protected, internal)
	private ArrayList<IGScopeItem> m_found = new ArrayList<IGScopeItem>();
	private ArrayList<String>      m_errors = new ArrayList<String>();

	public void init(int type, boolean member, int token_index, IGScopeItem requestor) {
		m_value_type      = type;
		m_member_function = member;
		m_token_index = token_index;
		m_requestor       = requestor;
		m_found.clear();
		m_errors.clear();
	}
	
	// TODO add an error code
	public void add(IGScopeItem item) {
		m_found.add(item);
		
		if (m_found.size() > 1) {
			addError("Ambiguous symbol found");
		}
	}
	
	public void addError(String error) {
		m_errors.add(error);
	}
	
	public String getError() {
		if (m_errors.size() ==  0) return null;
		return m_errors.get(0);
	}
	
	
	public boolean hasResults() {
		return m_found.size() > 0;
	}
	
	public IGScopeItem getResult() {
		return m_found.get(0);
	}
}