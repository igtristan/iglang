/*
Copyright (c) 2014-2017 Incubator Games Ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/


package pass1;
import java.io.*;
import java.util.Scanner;

public class IGTemplateParser
{
	public int m_curr_column 	= 1;
	public int m_curr_line 		= 1;
	public int m_position  =0 ;
	public String m_data = null;
	
	
	/*
	 * Initialize the LookaheadReader with the contents of an input stream.
	 * @param - stream - the stream to retrieve the code to compile
	 * @param - path   - where the stream was taken from, may be faked
	 */

	public final int init(String path, InputStream stream)
	{
		try 
		{
			// Use a scanner so all new lines are correctly converted
			Scanner scanner = new Scanner(stream);

			// read all of the lines of text in a os independent way
			StringBuilder builder = new StringBuilder();
			
			// do the initial check so that empty files don't cause exceptions
			if (scanner.hasNextLine()) 
			{
				builder.append(scanner.nextLine());
				while (scanner.hasNextLine()) {
					builder.append("\n");
       				builder.append(scanner.nextLine());
				}
			}
			
			m_data = builder.toString();


			m_position = 0;
			m_curr_column = 1;
			m_curr_line = 1;
		}
		catch (Exception ex) {
			ex.printStackTrace();
			return 0;
		}
		
		return 1;
	}
	
	// 0 = byte array
	public final void writeTo(PrintWriter pw, int write_mode, String param_name)
	{
	
		pw.print("\t" + param_name + ".writeUTFBytes(\"");
		
		int len = m_data.length();
		
		int mode = 0;
		for (int i = 0; i < len; i++) {
			char c0 = m_data.charAt(i + 0);
			char c1 = (i == len-1) ? '\0' : m_data.charAt(i + 1);
			
			
			if (mode == 0 && c0 == '$' && c1 == '{') {
				pw.println("\");");
				
				pw.print("\t" + param_name + ".writeUTFBytes(params[\"");
				mode = 1;
				i++;
				continue;
			}
			else if (mode == 1 && c0 == '}') {
				pw.println("\"]);");
				pw.print("\t" + param_name + ".writeUTFBytes(\"");
				mode = 0;
				continue;
			}
			
			if (mode == 0)
			{
				if (c0 == '\n')      { pw.print("\\n"); continue; }
				else if (c0 == '\r') { pw.print("\\r"); continue; }
				else if (c0 == '"')  { pw.print("\\\""); continue; }
				else if (c0 == '\\') { pw.print("\\\\"); continue; }
			}
			
			pw.print(c0);
		}
		
		if (mode == 0) {
			pw.println("\");");
		}
		else {
			pw.println(");");
		}
	
	}

	/*
	 * Peek at the next character in the stream
	 */ 
	
	public final char peek() 
	{
		if (m_position >= m_data.length()) { return '\0'; }
		return  m_data.charAt(m_position);
	}
	
		/* Read a single character from the file
	 * - update line # when we reach a newline
	 * - update column number
	 */

	public final char read()
	{
		if (m_position >= m_data.length()) { return '\0'; }
		char data = m_data.charAt(m_position);
		
		if (data == '\n') {
			m_curr_line ++;
			m_curr_column = 1;
		}
		else
		{
			m_curr_column ++;
		}
		m_position++;
		return data;
	}
}