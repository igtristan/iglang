/*
Copyright (c) 2014-2017 Incubator Games Ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

package pass1;
import java.io.*;
import java.util.Scanner;

public class IGLookaheadReader
{
	public int m_curr_column 	= 1;
	public int m_curr_line 		= 1;
	public int m_position  =0 ;
	public char [] m_data = null;

	
	
	private char [] m_whitespace      = new char[1024*1024];
	private int     m_whitespace_index = 0;
	
	
	/*
	 * Initialize the LookaheadReader with the contents of an input stream.
	 * @param - stream - the stream to retrieve the code to compile
	 * @param - path   - where the stream was taken from, may be faked
	 */
	
	public final int init(String path, InputStream stream)
	{
		try 
		{
			// Use a scanner so all new lines are correctly converted
			Scanner scanner = new Scanner(stream);

			// read all of the lines of text in a os independent way
			StringBuilder builder = new StringBuilder();
			
			// do the initial check so that empty files don't cause exceptions
			if (scanner.hasNextLine()) 
			{
				builder.append(scanner.nextLine());
				while (scanner.hasNextLine()) {
					builder.append("\n");
       				builder.append(scanner.nextLine());
				}
			}
			
			m_data = new char[builder.length()];
			builder.getChars(0, builder.length(), m_data, 0);
			//m_data     = builder.toString().toCharArray();
			m_position = 0;
			m_curr_column = 1;
			m_curr_line = 1;
			
			if (m_data.length > m_whitespace.length) {
				m_whitespace = new char[m_data.length];
			}
		}
		catch (Exception ex) {
			ex.printStackTrace();
			return 0;
		}
		
		return 1;
	}
	
	/**
	 * Discard a specific amount of characters.  This'll update the line and column numbers
	 * 
	 */ 
	public final void discard(int amount)
	{
		while (amount > 0) 
		{
			amount --;
			
			if (m_position >= m_data.length) { 
				break;
			}
			
			char data = m_data[m_position];
		
			if (data == '\n') {
				m_curr_line ++;
				m_curr_column = 1;
			}
			else
			{
				m_curr_column ++;
			}
			m_position++;
		}
	}
	
	/* Read a single character from the file
	 * - update line # when we reach a newline
	 * - update column number
	 */

	public final char read()
	{
		if (m_position >= m_data.length) { return '\0'; }
		char data = m_data[m_position];
		
		if (data == '\n') {
			m_curr_line ++;
			m_curr_column = 1;
		}
		else
		{
			m_curr_column ++;
		}
		m_position++;
		return data;
	}
	
	/*
	 * Peek at the next character in the stream
	 */ 
	
	public final char peek() 
	{
		if (m_position >= m_data.length) { return '\0'; }
		return  m_data[m_position];
	}
	
	/*
	 * Peek at the N'th character from the current cursor
	 */
	
	public final char peek(int distance)
	{	
		if (m_position + distance >= m_data.length) { return '\0'; }
		return  m_data[m_position + distance];
	}
	
	/*
	 * Peek at a series of characters
	 */
	
	public final void peekMultiple(char [] chars, int count)
	{
		int length = m_data.length;
		
		for (int i = count - 1; i >= 0; i--) {
			chars[i] = (m_position + i < length) ? m_data[m_position + i] : '\0';	
		}
	}
	
	/**
	 * Read a single character as whitespace
	 */
	
	public final char readWhitespace()
	{
		char c = read();
		m_whitespace[m_whitespace_index++] = c;
		
		return c;
	}
	
	/**
	 * Read characters into the whitespace buffer until a non whitespace character is found
	 */
	
	public final void eatWhitespace()
	{
		int p = m_position;
		int l = m_data.length;
		
		while (p < l)
		{
			char c = m_data[p];
			if (!(c == ' ' || c == '\t' || c == 10 || c == 13)) {
				break;
			}
			
			m_whitespace[m_whitespace_index++] = read();
			
			p++;
			
			
		}
		
		//m_position = p;
		
		
		/*
		char c = peek();
		while (	c == ' ' || 
				c == '\t' || 
				c == 10 || 
				c == 13) {
				
				
			char c2 = read();
			m_whitespace[m_whitespace_index++] = c2;
			
			c = peek();
		}
		
		*/
	}
	
	/**
	 * Read multiple characters and return them as a string
	 */
	 
	
	public final String readMultipleAsString(int count)
	{
		if (m_position + count >= m_data.length) {
			count = m_data.length - m_position;
			if (count <= 0) {
				return "";
			}
		}
		
		String tmp = new String(m_data, m_position, count);
		m_position += count;
		return tmp;
	}
	
	/**
	 * Get the contents of the whitespace buffer
	 * TODO - optimize
	 */
	
	public final String getWhitespace()
	{
		if (m_whitespace_index == 0) {
			return "";
		}
		
		String wsp = new String(m_whitespace, 0, m_whitespace_index);
		m_whitespace_index = 0;

		return wsp;
	}
	
}