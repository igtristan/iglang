/*
Copyright (c) 2014-2017 Incubator Games Ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

package pass1;

import util.*;

import java.util.*;

/**
	This class performs the initial validation on the source file
	Splitting tokens seems to be the only thing that'd ever make any clear sense

*/

public class IGFileReader extends IGConsts
{



	private static int          s_token_index = 0;
	private static IGTokenList  s_token_list_head = null;
	private static IGSource     s_source;


	// what was this for again>
	private final static void injectTokenListBefore(IGTokenList list)
	{
		IGTokenList head_list = s_token_list_head;
		

		
		
		IGTokenList terminator = IGTokenList.alloc(head_list.m_token);
		terminator.m_next = head_list.m_next;
		
		
		head_list.m_token = null;
		head_list.m_next  = list;
		
		// point the cursor at the injected list
		s_token_list_head = list;
		while (list.m_next != null) {
			list = list.m_next;
		}
		
		list.m_next = terminator;
		
		
	}

	/**
	 * Read the next token without any error checking
	 * @return the next token in the stream
	 */
	
	private final static Token read()
	{
		IGTokenList stok = s_token_list_head;	//s_token_list[s_token_index];
		s_token_list_head.m_token.m_token_index = s_token_index;
		s_token_index ++;
		s_token_list_head = s_token_list_head.m_next;
		return stok.m_token;	
	}

	/**
	 * Peek the next token without any error checking
	 * @return the next token in the stream
	 */ 
	
	private final static Token peekToken0()
	{
		IGTokenList stok = s_token_list_head;	
		return stok.m_token;	
	}	

	private final static Token peekToken1()
	{
		IGTokenList stok = s_token_list_head.m_next;	
		return stok.m_token;	
	}	

	private final static Token peekToken2()
	{
		IGTokenList stok = s_token_list_head.m_next.m_next;
		return stok.m_token;	
	}	


	private static final void merge3(int new_type, String new_value)
	{
		IGTokenList left = s_token_list_head;
		IGTokenList left1 = left.m_next;
		IGTokenList left2 = left1.m_next;
		
		left.m_token.m_type = new_type;
		left.m_token.m_value = new_value;
		left.m_token.m_whitespace += left1.m_token.m_whitespace + left2.m_token.m_whitespace;
		
		left.m_next = left2.m_next;
		
	}



	/**
	 * Prepend a token
	 */

	private static final Token prependToken(int type, String value)
	{
		Token left = s_token_list_head.m_token;

		Token right = new Token();
		right.copy(left);
		right.m_whitespace = "";
		
		left.m_type       = type;
		left.m_value      = value;
		
		IGTokenList list_left = s_token_list_head;
		IGTokenList list_right = new IGTokenList(right);
		
		list_right.m_next = list_left.m_next;
		list_left.m_next = list_right;
		
		return right;
	}
	

	/**
	 * Append a token after the current read head
	 */

	private static final Token appendToken(int type, String value)
	{
		Token left = s_token_list_head.m_token;

		Token right = new Token();
		right.copy(left);
		right.m_type       = type;
		right.m_value      = value;
		right.m_whitespace = "";
		
		
		IGTokenList list_left = s_token_list_head;
		IGTokenList list_right = new IGTokenList(right);
		
		list_right.m_next = list_left.m_next;
		list_left.m_next = list_right;
		
		return right;
	}
	
	/**
	 * Append a set of tokens to the Token under the read head.
	 * This token has not been read yet
	 */
	
	private static final void appendTokens(int [] types, String [] values)
	{
		Token left = s_token_list_head.m_token;
		IGTokenList left_list = s_token_list_head;
		int count = types.length;
		
		//Token     [] rights = new Token[count];
		IGTokenList [] rights_list = new IGTokenList[count];
		for (int i = 0; i < count; i++)
		{
			Token right = new Token();
			right.copy(left);
			right.m_type = types[i];
			right.m_value = values[i];
			right.m_whitespace = " ";
			
			//rights[i]   = right;
			rights_list[i] = new IGTokenList(right);
			
			// chain them together
			if (i != 0) {
				rights_list[i-1].m_next = rights_list[i];
			}
		}
		
		
		rights_list[count-1].m_next = left_list.m_next;
		left_list.m_next = rights_list[0];
	}


	/*
	 * Split the current token into 2 tokens
	 */

	private static final void splitToken(int left_type, int right_type)
	{
		Token left = s_token_list_head.m_token;
		
		if (left.m_value.length() != 2)  {
			throw new RuntimeException("splitToken only works on 2 character symbols");
		}
		
		left.m_type = left_type;
		
				
		Token right = new Token();
		right.copy(left);
		right.m_type = right_type;
		right.m_position ++;
		right.m_whitespace = "";
		//right.m_next = left.m_next;
		right.m_value = left.m_value.substring(1,2);

		//left.m_next = right;
		left.m_value = left.m_value.substring(0,1);
		
		
		
		IGTokenList list_left = s_token_list_head;
		IGTokenList list_right = new IGTokenList(right);
		
		list_right.m_next = list_left.m_next;
		list_left.m_next = list_right;
	}

	private static final int peekTokenType()
	{
		return s_token_list_head.m_token.m_type;
		//return s_token_list[s_token_index].m_type;
	}
	
	private static final boolean notPeekTokenTypeOrEOF(int t)
	{
		return 
			s_token_list_head.m_token.m_type != t && s_token_list_head.m_token.m_type != TOK_EOF;
	}
	
	
	private static final int peekTokenType0() {
		return s_token_list_head.m_token.m_type;
	}

	private static final int peekTokenType1() {
		return s_token_list_head.m_next.m_token.m_type;
	}
	
	private static final int peekTokenType2() {
		return s_token_list_head.m_next.m_next.m_token.m_type;
	}
	
	private static final int getTokenIndex() {
		return s_token_index;
	}
	

	/**
	 * This isn't a fatal error for this pass.  But it will kill the next one.
	 * Display the error then halt after this pass is completed.
	 *
	 * @param stok - The token that caused the error
	 * @param text - Error message to display
	 */

	private static final void softError(Token stok, String text)
	{
		stok.printError(text);
		new Throwable().printStackTrace();
		
		s_source.m_failed_pass0 = true;
	}
	
	
	private static final void softError(String text) {
		softError(peekToken0(), text);
//		softError(s_token_list[s_token_index], text);
	}
	
	/**
	 * An error occurred that is non recoverable.  Stop processing immediately.
	 * @param stok - The token that caused the error
	 * @param text - Error message to display
	 */

	private final static void error(Token stok, String text)
	{
		stok.printError(text);
		System.exit(1);
	}

	private final static void error(String text) {
		error(peekToken0(), text);			// why was this a soft error?
		//error(s_token_list[s_token_index], text);
	}

	/**
	 * Expect that the next token has the specified type.  If it doesn't match
	 * then this is a fatal error.
	 * @param token_type - the type of the next token we're expecting
	 * @param text - the error message to display if it isn't what we're expecting
	 * @return - the next token
	 */

	private final static Token expect(int token_type, String text)
	{
		Token stok = read();	//s_token_list_head;	//s_token_list[s_token_index];
		if (stok.m_type != token_type)  {	
			if (stok.m_type == TOK_EOF) {
				text += " Reached end of file while parsing.";
			}
			error(stok, text );
		}

		// commit to the token index
		//s_token_list_head.m_token_index = s_token_index;
		//s_token_index ++;
		//s_token_list_head = s_token_list_head.m_next;
		
		return stok;
	}
	
	private static Token expect(int token_type) {
		return expect(token_type, "");
	}
	

	
	/**
	 * Parse a scope path. A scope path is in the following two regex forms:
	 * 	i.  (ID\.)*ID
	 * 	ii. (ID\.)*\*
	 * @param allow_glob - can the scope path end with an asterix
	 */

	private final static IGScopePath parseScope(boolean allow_glob)
	{
		IGScopePath s = new IGScopePath();
		
		while (peekTokenType() != TOK_SEMI)
		{
			s.add(expect(TOK_ID, "Expecting scope path"));
			if (peekTokenType() != TOK_SEMI) {
				expect(TOK_DOT, "Expecting .");
				if (allow_glob) {
					if (peekTokenType() == TOK_MUL) {
						s.add(expect(TOK_MUL, "Expecting glob"));
						
						// double ** indicates that its a recursive import
						if (peekTokenType() == TOK_MUL) {
							expect(TOK_MUL, "Expecting glob");
							s.setRecursive(true);
						}
						break;
					}
				}
			}
		}
		
		return s;
	}
	
	private final static Type parseTypeAllowVoid()
	{
		if (peekTokenType() == TOK_VOID) {
			read();
			return Type.VOID;
		}
		else
		{
			return parseType();
		}
	}
	

	private final static Type parseType() {
		final int token_type = peekTokenType();

		//////////////////////////
		// This code is incompatible with the AS3 code
		if (token_type == IGLexer.LEXER_INT) {
			Token count_token = expect(IGLexer.LEXER_INT, "Expecting a count");
			int count = Token.getIntValue(count_token);	
		
			if (!(1 <= count && count <= 16)) {
				softError("Count for types should be in the range 1 to 16");
			}

			Type type = _parseType();

			ArrayList<Type> tuple_types = new ArrayList<Type>();
			for (int i = 0; i < count; i++) {
				tuple_types.add(type);
			}

			return Type.get("tuple", Type.get(tuple_types), Type.VOID);
		}

		return _parseType();
	}


	private final static Type _parseType()
	{
		//System.out.println("following token: " + peekToken0().m_value + " " + peekTokenType());
	
		final int token_type = peekTokenType();


		if (token_type == TOK_INT) {
			read();	return Type.INT;
		}
		else if (token_type == TOK_BOOLEAN) {
			read();	return Type.BOOL;
		}
		// TODO right out deprecate this type all together
		else if (token_type == TOK_UINT) {
			read();	return Type.UINT;
		}
		else if (token_type == TOK_NUMBER) {
			read();	return Type.DOUBLE;
		}
		else if (token_type == TOK_BYTE) {
			read(); return Type.BYTE;
		}
		else if (token_type == TOK_SHORT) {	
			read(); return Type.SHORT;
		}
		else if (token_type == TOK_FLOAT) {
			read(); return Type.FLOAT;
		}
		else if (token_type == TOK_FUNCTION_TYPE ||
				 token_type == TOK_STATIC_FUNCTION_TYPE)
		{
			int function_type = peekTokenType();
		
			expect(function_type);
			expect(TOK_LT, "Expecting function type definition");
			
			ArrayList<Type> parameter_types = Type.allocTempVector();
			expect(TOK_LBRACKET, "Expecting ( within function type.");
			while (peekTokenType() != TOK_RBRACKET)
			{
				parameter_types.add(parseType());
				if (peekTokenType() != TOK_RBRACKET) {
					expect(TOK_COMMA, "Expecting ,");
				}
			}
			
			expect(TOK_RBRACKET, "Expecting ) within function type.");
			expect(TOK_COLON,    "Expecting : within function type.");
			
			Type right = parseTypeAllowVoid();
			
			// avoid useless string allocation
			if (peekTokenType() != TOK_GT)  {
				expect(TOK_GT, "Expecting > end of function type definition. " + peekToken0().m_value + " " + peekToken0().m_type);
			}
			else {
				read();
			}
			
			Type result = Type.get(function_type == TOK_FUNCTION_TYPE ? 
				"Function" : "StaticFunction", Type.get(parameter_types), right);	
				
			Type.freeTempVector(parameter_types);	
				
			return result;
		}
		else
		{
			Token class_type = expect(TOK_ID, "Expecting class or enum name");
			
			if (peekTokenType() == TOK_LT)
			{
				expect(TOK_LT, "Expecting < for templated type");
			
				Type left = parseType();
				Type right = null;
				
				// disambiguate any  '>=' into '>' '='
				if (peekTokenType() == TOK_GE) { splitToken(TOK_GT, TOK_ASSIGN); }
				
				// if the next token isn't '>' then we expect to see another parameter
				if (peekTokenType() != TOK_GT ) {
					expect(TOK_COMMA, "Expecting comma separating templated types.");
					right = parseType();
				}
				
				// disambiguate any '>='  into '>' '='
				if (peekTokenType() == TOK_GE) { 
					splitToken(TOK_GT, TOK_ASSIGN); 
				}
					
				expect(TOK_GT, "Expecting > to terminate templated type");	
				
				return Type.get(class_type.m_value, left, right);
			}
			else {
				return Type.get(class_type.m_value);
			}
		} 
	}
	
	/**
	 * Stub in a function so that compilation can proceed even though the
	 * function never exists.
	 */
	private static boolean addFunctionStub(IGSource source,
					int modifiers, 
					String name, Type return_type,
					ArrayList<String> parameter_names, ArrayList<Type> parameter_types)
	{
		
	
		IGMember member = new IGMember(source);
		
		member.setType(IGMember.TYPE_FUNCTION);
		

		member.addModifier(modifiers | IGMember.STUB);
		member.setNameAndReturnType(Token.createId(name), return_type);
		
		int position = 0;
		member.beginScope(0, position);
		
		if (!member.hasModifier(IGMember.STATIC)) {
			//member.addVariable(0, position, Token.createId("super"), s_source.m_class_extends);
			// TODO probably the previous line is much more correct than the following one
			
			member.addVariable(0, position, Token.createId("super"), source.getPreValidatorConstructorType());
			member.addVariable(0, position, Token.createId("this"),  source.getPreValidatorConstructorType());
		}
		
		if (parameter_names.size() != parameter_types.size()) {
			throw new RuntimeException("Unbalanced arrays");
		}
		
		for (int i = 0; i < parameter_names.size(); i++) {
			IGVariable parameter = null;
			if (null == (parameter = member.addParameter(position, 
					Token.createId(parameter_names.get(i)), parameter_types.get(i)))) {
					
				throw new RuntimeException("Duplicate parameters");
				//softError(parameter_id, "A parameter already exists with this name");
			}
		}
		
		member.closeScope(0, position + 1);
		
		source.addMember(member);
		
		return true;
	}
	
	private static IGMember parseMember(IGSource source,int source_type)
	{	
	
		boolean is_interface = source_type == TOK_INTERFACE;
		boolean is_native = false;
			
		IGMember member = new IGMember(source);
		if (is_interface) {
			member.addModifier(IGMember.PUBLIC);
		}
		
		
		while (peekTokenType() ==  IGLexer.LEXER_NATIVE_BLOCK) {
			read();
		}	
		
		
		Token start = peekToken0();
		

		int peeked_type = peekTokenType();
		
		/////////////////////////////////////////
		// Process all of the modifiers
		/////////////////////////////////////////		
		
		while (peeked_type != TOK_VAR && 
			   peeked_type != TOK_FUNCTION && 
			   peeked_type != TOK_CONST )
		{
			if (peeked_type == TOK_STATIC) {
				if (is_interface) {  softError("Interfaces may not contain static functions"); }
				read();  member.addModifier(IGMember.STATIC);
			}
			else if (peeked_type == TOK_FINAL) {
				if (is_interface) {  softError("Interfaces may not contain final members"); }
				read();  member.addModifier(IGMember.FINAL);
			}
			else if (peeked_type == TOK_PUBLIC) {
				read();  member.addModifier(IGMember.PUBLIC);
			}
			else if (peeked_type == TOK_PRIVATE) {
				if (is_interface) {  softError("Interfaces may only contain public members"); }
				read();  member.addModifier(IGMember.PRIVATE);
			}
			else if (peeked_type == TOK_PROTECTED) {
				if (is_interface) {  softError("Interfaces may only contain public members"); }
				read();  member.addModifier(IGMember.PROTECTED);
			}
			else if (peeked_type == TOK_INTERNAL) {
				if (is_interface) {  softError("Interfaces may only contain public members"); }
				read();  member.addModifier(IGMember.INTERNAL);
			}
			else if (peeked_type == TOK_VIRTUAL) {
				read();  member.addModifier(IGMember.VIRTUAL);
			}
			else if (peeked_type == TOK_OVERRIDE) {
				read();  member.addModifier(IGMember.OVERRIDE);
			}
			else if (peeked_type == TOK_INLINE) {
				if (is_interface) {  softError("Interfaces may not be inlined"); }
				read();  member.addModifier(IGMember.INLINE);
			}
			else if (peeked_type == TOK_NATIVE) {
				if (is_interface) { softError("Interfaces may not have native members"); }
				read();  member.addModifier(IGMember.NOINLINE);
				is_native = true;
			}
			else
			{
				start.printError("IGMember starting at: ");
				error(peekToken0(), "Unknown member modifier found");
			}
			
			peeked_type = peekTokenType();
		}
		
		///////////////////////////////////////////////////
		// Process a variable
		////////////////////////////////////////////////////
		
		if (peekTokenType() == TOK_VAR || peekTokenType() == TOK_CONST) 
		{
		
			if (peekTokenType() == TOK_CONST) {
				member.addModifier(IGMember.FINAL);
			}
		
			source.addAnnotation(start, ANNOTATION_VARIABLE_START,  member);
			//start.m_annotation_object = member;
			//start.m_annotation = ANNOTATION_VARIABLE_START;
		
			if (is_interface) 
			{
				error("Variables aren't allowed in interfaces");
			}
			
			// if its a const just rename it to a var
			read().m_type = TOK_VAR;
			
			
			//member.m_parameter_start_index = getTokenIndex();
			Token id = expect(TOK_ID, "Expecting a name for the variable");
			expect(TOK_COLON, "Expecting a colon seperating the name from the type");
			
			
			
			// check that there isn't a name conflict
			/////////////////////////////////////////
			if (s_source.hasMemberWithName2(id.m_value, IGSource.MEMBER_DEFAULT))
			{
				softError(id, "IGMember already exists with this name");
			}
			
			
			Type type = parseType();
			member.m_parameter_end_index = getTokenIndex();
			
			member.setType(IGMember.TYPE_VAR);
			member.setNameAndReturnType(id, type);
			
			if (peekTokenType() != TOK_SEMI) 
			{
				expect(TOK_ASSIGN, "Expecting a value or a semicolon");
				
				member.m_first_body_token = getTokenIndex();
				matchExpression(member);
				member.m_last_body_token        = getTokenIndex();
				
				expect(TOK_SEMI);
			}
			else
			{
				// this "might" further break the cpp target
				//member.m_first_body_token 	   = -1;	getTokenIndex();
				//member.m_last_body_token       = getTokenIndex();
				
				expect(TOK_SEMI);
				
			}
		}
		///////////////////////////////////////////////////////
		// Process a function
		///////////////////////////////////////////////////////
		else 
		{
			member.setType(IGMember.TYPE_FUNCTION);		
		
			source.addAnnotation(start,ANNOTATION_FUNCTION_START,  member);
			//start.m_annotation_object = member;
			//start.m_annotation = ANNOTATION_FUNCTION_START;
		
			expect(TOK_FUNCTION, "Expecting a function or a variable.");
			
			if (peekTokenType() == TOK_SET)
			{	
				if (is_interface) {  
					softError("Interfaces may not contain accessors."); 
				}
				if (source_type == TOK_ENUM && !member.hasModifier(IGMember.STATIC)) {
					softError("Enums may not contain member setters."); 
				}
				
				if (peekTokenType1() == TOK_LARRAY && peekTokenType2() == TOK_RARRAY) {
					merge3(TOK_ID, "<set[]>");
				}
				else {
					expect(peekTokenType()); 
					member.addModifier(IGMember.SET);
				}
			}
			else if (peekTokenType() == TOK_GET) {
				if (is_interface) {  
					softError("Interfaces may not contain accessors."); 
				}
				
				if (peekTokenType1() == TOK_LARRAY && peekTokenType2() == TOK_RARRAY) 
				{
					merge3(TOK_ID, "<get[]>");				
				}
				else {
					expect(peekTokenType());  
					member.addModifier(IGMember.GET);
				}
			}
			
			
			Token id = expect(TOK_ID, "Expecting a name for the function");
			if (id.m_value.equals(s_source.m_expected_component)) {
				id.m_value = "<constructor>";
			}
			
			
			
			
			int member_type = IGSource.MEMBER_DEFAULT;
			if (member.hasModifier(IGMember.GET)) {
				member_type = IGSource.MEMBER_GET;
			}
			else if (member.hasModifier(IGMember.SET)) {
				member_type = IGSource.MEMBER_SET;
			}
			
			
			// check that there isn't a name conflict
			/////////////////////////////////////////
			if (s_source.hasMemberWithName2(id.m_value, member_type)) 
			{
				if (id.m_value.equals("<constructor>")) {
					softError(id, "Constructor already exists for this class.");
				}
				else {
					softError(id, "Field already exists with this name: " + id.m_value);	
				}
			}
			
			if (member.hasModifier(IGMember.GET)) {
				id.m_value += "__get";
			}
			else if (member.hasModifier(IGMember.SET)) {
				id.m_value += "__set";
			}
			
			
			//member.m_parameter_start_index = getTokenIndex();
			expect(TOK_LBRACKET, "Expecting a ( starting a function parameter list.");
			
			
			boolean default_parameters_started = false;
			
			
			
			int pos = getTokenIndex();
			member.beginScope(0, pos);
			
			/////////////////////////////////////////////////////////
			// add super and this variables to non static functions
			/////////////////////////////////////////////////////////

			if (!is_interface)  {
				if (!member.hasModifier(IGMember.STATIC)) 
				{
					// safety assert
					if (member.getVariableCount() != 0) {
						throw new RuntimeException("Horrible happenstance!");
					}
					
					int position = getTokenIndex();
					if ( s_source.m_class_extends != null) 
					{
						// okay so this IS actually tagged with the class that it extends
						member.addVariable(0, position, Token.createId("super"), s_source.m_class_extends);
					}
					
					
					member.addVariable(0, position, Token.createId("this"), source.getPreValidatorConstructorType());
				}
			}
			
			
			
			while (peekTokenType() != TOK_RBRACKET && peekTokenType() != TOK_EOF)
			{
				int position = getTokenIndex();
			
				Token parameter_id = expect(TOK_ID, "Expecting a name for a parameter");
				expect(TOK_COLON, "Expecting a colon seperating the name from the type of a parameter");
				

				
				


				Token parameter_type_token = peekToken0(); 
				Type  parameter_type       = parseType();
				
				int count = 0;
				if (parameter_type.isTuple()) {
					count = parameter_type.getTupleTypes().length;
				}


				if (count == 0)
				{

					IGVariable parameter = null;
					if (null == (parameter = member.addParameter(position, parameter_id, parameter_type, parameter_type_token))) {
						softError(parameter_id, "A parameter already exists with this name");
					}
					
					
					// DEFAULT PARAMETER
					if (peekTokenType() == TOK_ASSIGN || default_parameters_started) 
					{
						expect(TOK_ASSIGN, "All parameters following the first default parameters must have defaults");
						matchPair2(member, TOK_COMMA, TOK_RBRACKET);
						
						
						parameter.m_has_default = true;
						default_parameters_started = true;
					}
				}
				/////////////////////////////////////////////////////////////////////////
				// Tuples
				/////////////////////////////////////////////////////////////////////////w
				else 
				{

					// Add a root variable encompassing the tuple
					//////////////////////////////////////////////
					int root_arg_position = member.getParameterCount();
					

					IGVariable v = member.addVariable(member.getParameterCount(), 
									position, parameter_id, parameter_type);
					if (null == v) {
						softError(parameter_id, "A parameter already exists with this name");
					}
					v.setCounted(false);

					Type [] tuple_types = parameter_type.getTupleTypes();


					IGVariable []parameters = new IGVariable[tuple_types.length];
					
					// Add in each individual parameter
					//////////////////////////////////////
					for (int pid = 0; pid < tuple_types.length; pid++)
					{
						Token new_parameter_id = new Token();
						new_parameter_id.copy(parameter_id);
						new_parameter_id.m_value += "_" + pid;

						IGVariable parameter = null;
						if (null == (parameters[pid] = member.addParameter(position, new_parameter_id, tuple_types[pid], parameter_type_token))) {
							softError(new_parameter_id, "A parameter already exists with this name");
						}
					}
					
					
					// DEFAULT PARAMETER
					if (peekTokenType() == TOK_ASSIGN || default_parameters_started) 
					{
						expect(TOK_ASSIGN, "All parameters following the first default parameters must have defaults");
						matchPair2(member, TOK_COMMA, TOK_RBRACKET);
						
						// mark all of tehe following as having a default
						for (int pid = 0; pid < count; pid++) {
							parameters[pid].m_has_default = true;
						}
						default_parameters_started = true;
					}

					//List<IGVariable> vv = member.getParamters();
					//for (IGVariable vvv : vv) {
					//	System.out.println("" + vv);
					//}


				}
		
				if (peekTokenType() != TOK_RBRACKET) {
					expect(TOK_COMMA, "Expecting a comma separating function parameters.");
				}
			}
			
			




			Token close_bracket = expect(TOK_RBRACKET, "Expecting a )");
			
			
			
			
			Token return_type_token = null;
			Type return_type = null;
			
			// standard function
			if (!id.m_value.equals("<constructor>")) 
			{
				// make all functions by default void
				if (peekTokenType0() == TOK_LBRACE)
				{
					 peekToken0().m_type = TOK_COLON;
					 peekToken0().m_value = ":";
					 appendToken(TOK_LBRACE, "{");
					 appendToken(TOK_VOID,   "void");
				}
			
			
				expect(TOK_COLON, "Expecting a type following a method definition");
				
				return_type_token = peekToken0();
				return_type       = parseTypeAllowVoid();
			}
			// constructor
			else
			{
				if (member.hasModifier(IGMember.STATIC)) {
					softError(id, "Constructor may not be marked as static");
				}
				
				//id.m_value = "<constructor>";
				
				
				return_type = s_source.getPreValidatorConstructorType();
				
				//System.err.println("pre validator constructor type " +  return_type);
			}
			
			//member.m_parameter_end_index = getTokenIndex();
			
				
			member.setNameAndReturnType(id, return_type, return_type_token);
			
			
			if (is_interface) 
			{	
				expect(TOK_SEMI, "Expecting ; terminating interface funcion");
			}
			///////////////
			// Inline code to throw an exception if a native error occurs
			///////////////
			else if (is_native && peekTokenType() == TOK_SEMI)
			{
				boolean is_constructor = member.m_name.m_value.equals("<constructor>");
					
				member.m_first_body_token = getTokenIndex();
				
				peekToken0().m_type = TOK_LBRACE;
				peekToken0().m_value = "{";
				
				int [] token_types = {TOK_THROW, TOK_NEW, TOK_ID, TOK_LBRACKET, IGLexer.LEXER_STRING, TOK_RBRACKET, TOK_SEMI, TOK_RBRACE};
				String [] token_strings = {"throw","new","Error","(","Native Missing",")",";","}"};
				
				appendTokens(token_types, token_strings);
				
				
				parseBlock(member, 1, is_constructor);
				member.m_last_body_token = getTokenIndex();
			}
			else
			{	
				

			
				if (peekTokenType() == TOK_SEMI) {
					softError("Unless the file contains an interface all functions must have bodies");
				}
				else
				{	
				
					boolean is_constructor = member.m_name.m_value.equals("<constructor>");
					
					member.m_first_body_token = getTokenIndex();
					parseBlock(member, 1, is_constructor);
					member.m_last_body_token = getTokenIndex();
				}
				
			}
			
			// close the scope of the variables being used as parameters
			pos = getTokenIndex();
			member.closeScope(0, pos);
		}
	
		
		
		
		s_source.addMember(member);
		
		
		return member;
	}
	
	/*
	private static boolean isValidExpressionToken(int type) {
		if (type == TOK_FOR || type == TOK_WHILE 	|| type == TOK_DO || 
			type == TOK_VAR || type == TOK_RETURN 	|| type == TOK_CATCH || 
			type == TOK_TRY || type == TOK_IN 		|| type == TOK_THROW || 
			type == TOK_FUNCTION || type == TOK_SWITCH)
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	*/
	
	/*
	 * Parse an expression in the form
	 * ID : Type
	 */
	
	private static void parseHeadlessVarStatement(IGMember member, int scope)
	{
		int pos = getTokenIndex();

		
		Token id = expect(TOK_ID, "Expecting name of variable");
		
		expect(TOK_COLON, "Expecting ': TYPE' after name of variable");
		
		Token type_token = peekToken0();
		Type  type = parseType();
		
		if (type == null) {
			error(type_token, "Invalid type");
		}
		
		IGVariable v = null;
		if (null == (v = member.addVariable(scope, pos, id, type, type_token))) {
			softError(id, "A variable with this name already exists in the current scope");
		}
	}
	
	/*
	 * Parse an expression in the form
	 * var ID : Type = expression ...
	 * var ID : Type ;  ...
	 * var ID : Type in ...
	 */
	
	private static void parseVarStatement(IGMember member, int scope)
	{
		int pos = getTokenIndex();
		
		if (peekTokenType0() != TOK_VAR) 
		{
			// the whitespace is important for the AS3 build
			prependToken(TOK_VAR, "var ");
		}
		
		
		Token var_token = expect(TOK_VAR);
		
		Token id = expect(TOK_ID, "Expecting name of variable");
		expect(TOK_COLON, "Expecting ': TYPE' after name of variable");

		Token type_token = peekToken0();
		Type  type       = parseType();

		if (type == null) {
			error(type_token, "Invalid type");
		}
	
		IGVariable [] vs = null;
		IGVariable v = null;
		if (null == (v = member.addVariable(scope, pos, id, type, type_token))) {
			softError(id, "A variable with this name already exists in the current scope");
		}

		if (type.isTuple())
		{
			v.m_counted = false;

			Type [] tuple_types = type.getTupleTypes();
			final int tuple_count = tuple_types.length;



			vs = new IGVariable[tuple_count];
			for (int i = 0; i < tuple_count; i++) 
			{

				Token tuple_id =  new Token();
				tuple_id.copy(id);
				tuple_id.m_value += "_" + i;

				if (null == (vs[i] = member.addVariable(scope, pos, tuple_id, tuple_types[i], type_token))) {
					softError(id, "A variable with this name already exists in the current scope");
				}
			}
		}
		
		// annotate the 'var' token with the Variable object this represents
		// this is used within IGValidator
		s_source.addAnnotation(var_token, ANNOTATION_LOCAL_VARIABLE, v);
		
		int peeked_token = peekTokenType();
		if (peeked_token == TOK_SEMI) 
		{
			// this is used in the As3New target
			v.m_AS3_has_initial_value = false;
		}
		else if (peeked_token == TOK_ASSIGN)
		{
			//expect(TOK_ASSIGN, "Expecting assignment");
			read();		// <- read the '='
			matchExpression(member);
		}
		
		
		v.setAvailableIndex(getTokenIndex());
		if (vs != null) {
			for (int i = 0; i < vs.length; i++) {
				vs[i].setAvailableIndex(getTokenIndex());
			}
		}
		
	}
	

	private static void matchPair2(IGMember member, int escape, int escape2) {
		int peek_type = peekTokenType();
		while (peek_type != escape && peek_type != escape2 && peek_type != TOK_EOF) 
		{	
			if (peek_type == TOK_LBRACKET)    { matchBracketedExpression(member, ""); }
			else if (peek_type == TOK_LARRAY) { matchArrayedExpression(member, ""); }
			else if (peek_type == TOK_LBRACE) { matchBracedExpression(member, ""); }
			// seeing one of these would indicate a mismatch
			else if (peek_type == TOK_RARRAY) { break; }
			else if (peek_type == TOK_RBRACE) { break; }
			else if (peek_type == TOK_RBRACKET) { break; }
			
			// default case
			else {
				read();
			}
			 peek_type = peekTokenType();
		}
	}
	
	private static void matchArrayedExpression(IGMember member, String type)
	{
		if (peekTokenType() != TOK_LARRAY) {
			expect(TOK_LARRAY, "Expecting [ following " + type);
		} else { read(); }
		matchExpression(member);
		expect(TOK_RARRAY, "Missing matching ]");
	}
	
	private static void matchBracedExpression(IGMember member, String type)
	{
		if (peekTokenType() != TOK_LBRACE) {
			expect(TOK_LBRACE, "Expecting { following " + type);
		} else { read(); }
		matchExpression(member);
		expect(TOK_RBRACE, "Missing matching }");
	}
	
	private static void matchBracketedExpression(IGMember member, String type)
	{
		if (peekTokenType() != TOK_LBRACKET) {
			expect(TOK_LBRACKET, "Expecting ( following " + type);
		} else { read(); }
		
		matchExpression(member);

		expect(TOK_RBRACKET, "Missing matching )");
	}
	
	
	static class IGInternalCodeInject
	{
	
	}
	
	
	/**
	 * This is used for matching the contents of anonymous functions.
	 * It likely should be more robust to catch other bracketing issues.
	 */
	 
	private static IGTokenList matchSimpleBlock()
	{
		expect(TOK_LBRACE, "Expecting {");
		
		int peeked_type = peekToken0().m_type;
		boolean exit = false;
		while (!exit) 
		{
			if (peeked_type == TOK_LBRACE) {
				matchSimpleBlock();
			}
			else if (peeked_type == TOK_RBRACE) {
				exit = true;
			}
			else if (peeked_type == TOK_EOF)  {
				exit = true;
			}	
			else {
				read();
			}
		
			peeked_type = peekToken0().m_type;
		}
		
		IGTokenList last = s_token_list_head;
		expect(TOK_RBRACE, "Expecting }");
		return last;
	}
	
	
	private static int s_anon_sequence = 1;
	
	
	private static void matchExpression(IGMember member)
	{
		boolean exit = false;
		int peeked_type = peekToken0().m_type;
		while (!exit) 
		{
			switch (peeked_type)
			{
				case TOK_LBRACKET: { matchBracketedExpression(member, ""); break; }
				case TOK_LARRAY:   { matchArrayedExpression(member, ""); break; }
				case TOK_LBRACE:   { matchBracedExpression(member, ""); break; }	
				case TOK_RARRAY:   { exit = true; break; }	
				case TOK_RBRACE:   { exit = true; break; }
				case TOK_RBRACKET: { exit = true; break; }
				case TOK_EOF:      { exit = true; break; }
				case TOK_SEMI:     { exit = true; break; }
				
				
				// also anything that'd only be allowed in a statement should be thrown away
				case TOK_RETURN:   { exit = true; break; }
				case TOK_VAR:      { exit = true; break; }
				case TOK_IF:	   { exit = true; break; }
				case TOK_ELSE:     { exit = true; break; }
				case TOK_SWITCH:   { exit = true; break; }
				case TOK_CASE:     { exit = true; break; }
				case TOK_DEFAULT:  { exit = true; break; }
				case TOK_CONST:    { exit = true; break; }
				case TOK_DO:       { exit = true; break; }
				case TOK_WHILE:    { exit = true; break; }
				case TOK_FUNCTION: { exit = true; break; }
				case TOK_STATIC:   { exit = true; break; }
				case TOK_THROW:    { exit = true; break; }
				case TOK_CATCH:    { exit = true; break; }
				case TOK_TRY:      { exit = true; break; }
				case TOK_IN:       { exit = true; break; }
				case TOK_RANGE:    { exit = true; break; }
				
				
				case TOK_ANON_FN:  
				{ 
					IGTokenList root_token       = s_token_list_head;
					Token anon_fn = read();
					
					
					
					
					// record where the anonymous function starts
					int         head_token_index = getTokenIndex();
					
					int seq = s_anon_sequence;
					s_anon_sequence ++;
					
					IGTokenList head_token       = s_token_list_head;
					
					
					anon_fn.m_type  = TOK_ID;
					anon_fn.m_value = "__ig_anon_" + seq;
					//anon_fn.m_whitespace += " /*x*/ ";
					
					
					if (peekTokenType0() == TOK_STATIC) {
						read();
					}
					
					// safeguard any possible weird EOF issues
					if (peekTokenType0() == TOK_FUNCTION) {
						// append a token following the one at the top
						Token t = appendToken(TOK_ID, "__ig_anon_" + seq);
						t.m_whitespace += " ";
					}
					
					expect(TOK_FUNCTION, "Expecting function token for anonymous function");
					expect(TOK_ID,		 "Expecting name for anonymous function");
					expect(TOK_LBRACKET,   "Expecting function signature");
					while (peekTokenType0() != TOK_RBRACKET) {
						
						expect(TOK_ID,    "Expecting id for function parameter");
						expect(TOK_COLON, "Expecting colon separating id from type");
						parseType();
						
						if (peekTokenType0() != TOK_RBRACKET) {
							expect(TOK_COMMA, "Expecting comma separating anonymous function parameters");
						}	
					}
					
					
					expect(TOK_RBRACKET,   "Expecting end of function signature");
					expect(TOK_COLON);
					parseTypeAllowVoid();
					
					IGTokenList last = matchSimpleBlock();
					last.m_next = null;
					

					// splice out the code.					
					addMemberInject(head_token);
					root_token.m_next = s_token_list_head;
					s_token_index     = head_token_index;					
					
					
					// splice out the code from between the two regions
					
					//System.out.println("did this get here? " + peekToken0().m_value);
					
					



					//System.out.println("did this get here? " + peekToken0().m_value);					
					
					// revert back the read head
					//s_token_list_head = head_token;
					
				
					//exit = true; 
					break; 
				}
				
				
				
				//case TOK_COMMA:  { exit = true; break; }		// commas can appear legitimately in types
				default:           { read(); break; }
			}
					   
			peeked_type = peekToken0().m_type;
		}
	}
	
	
	/***
	 * This is only a coarse examination of the contents of a block of a function
	 * This is primarily used to extract variable names
	 */
	 
	 
	 /*
	private static void parseBlock(IGMember member, int scope)
	{
		parseBlock(member, scope, false);
	}
	*/

	private static void parseBlock(IGMember member, int scope, boolean is_constructor)
	{
	
		
		// parse the contents
		Token t = expect(TOK_LBRACE, "Expecting {");
		int pos = getTokenIndex();
		member.beginScope(scope, pos);
		
		
		//t.m_annotation = ANNOTATION_BLOCK;
		
		
		//boolean constructor_error = false;
		
		boolean constructor_error = (scope != 1) || !is_constructor;
									//!member.m_name.m_value.equals("<constructor>");
									
		if (scope == 1 && is_constructor) { //member.m_name.m_value.equals("<constructor>")) {
			if (peekTokenType0() == TOK_ID && peekToken0().m_value.equals("super") &&
					peekTokenType1() == TOK_DOT &&
					peekTokenType2() == TOK_ID && peekToken2().m_value.equals("<constructor>"))
			{
				member.m_super_constructor_called = true;
			}
		}
		
	
		while (peekTokenType() != TOK_RBRACE && peekTokenType() != TOK_EOF)
		{
			if (constructor_error) 
			{
				//System.err.println("<>");
				//System.err.println(			
				if (peekTokenType0() == TOK_ID && peekToken0().m_value.equals("super") &&
					peekTokenType1() == TOK_DOT &&
					peekTokenType2() == TOK_ID && peekToken2().m_value.equals("<constructor>"))
				{
					error(peekToken0(), "Invalid location of constructor call.");
			
				}
			}
			constructor_error = true;
			
			int peeked_type = peekTokenType();
			
			if (peeked_type == TOK_LBRACE) {
				parseBlock(member, scope + 1, is_constructor);
			}
			else if (peeked_type == TOK_VAR || (peekTokenType0() == TOK_ID && peekTokenType1() == TOK_COLON)) 
			{
				parseVarStatement(member, scope);
				expect(TOK_SEMI, "Expecting a semicolon following a variable declaration.");
			}
			else if (peeked_type == IGLexer.LEXER_NATIVE_BLOCK)
			{	
				// eat some native code
				Token nb_start = read();
			}
			// this is a scoped block
			else if (peeked_type == TOK_LBRACE)
			{
				parseBlock(member, scope + 1, is_constructor);
			}
			else if (peeked_type == TOK_TRY)
			{
				Token try_start = expect(TOK_TRY);
				
				parseBlock(member, scope + 1, is_constructor);
				
				while (peekTokenType() == TOK_CATCH)
				{
					Token catch_start = expect(TOK_CATCH);
					
					
					pos = getTokenIndex();
					member.beginScope(scope + 1, pos);
					
					expect(TOK_LBRACKET, "Expecting ( following catch definition");
					parseHeadlessVarStatement(member, scope + 1);
					expect(TOK_RBRACKET, "Expecting ) following catch definition");
						
					parseBlock(member, scope + 2, is_constructor);
					
					// close the scope for the headless var statement
					pos = getTokenIndex();
					member.closeScope(scope + 1, pos);
				}
			}
			else if (peeked_type == TOK_IF) 
			{
				Token if_start = read();		// <- read the if
				matchBracketedExpression(member, "if");
				parseBlock(member, scope + 1, is_constructor);
				
				while (peekTokenType() == TOK_ELSE)
				{
					Token else_start = read();  // <- read the else
					if (peekTokenType() == TOK_IF) 
					{
						read();	// <- read the if
						matchBracketedExpression(member, "else if");		
						parseBlock(member, scope + 1, is_constructor);
					}
					else
					{
						parseBlock(member, scope + 1, is_constructor);
						break;
					}
				}
			}
			else if (peeked_type == TOK_ELSE) {
				error("Unexpected else");
			}
			else if (peeked_type == TOK_WHILE) 
			{
				Token while_start = read();	//expect(TOK_WHILE);
				matchBracketedExpression(member, "while");
				parseBlock(member, scope + 1, is_constructor);
			}
			else if (peeked_type == TOK_DO) {
				Token do_start = read();	//expect(TOK_DO);

				parseBlock(member, scope + 1, is_constructor);
				
				expect(TOK_WHILE,  "Expecting while following do");
				matchBracketedExpression(member, "while of do/while loop");
				expect(TOK_SEMI,     "Expecting ; following while clause of do statement");
			}
			else if (peeked_type == TOK_SWITCH)
			{	
				int   switch_pos   = getTokenIndex();
				Token switch_start = read();	//expect(TOK_SWITCH);
				
				// ummm.. shit don't know what type this variable will be yet
				// but at this point we don't know what types are supported by the switch
				
				/*
				// not currently needed because we'll use the stack to store the value in question
				{
					Token iterator_name = Token.createUniqueVariableId(switch_start);
					IGVariable v = null;
					if (null == (v = member.addVariable(scope, switch_pos, iterator_name, Type.VOID))) {
						softError(switch_start, "A variable with this name already exists in the current scope");
					}
				}
				*/
				
				// switch (x) {
				//		case (22) {
				//		}
				//		case (33) {
				//		}
				//		default {
				//		}
				// }
				
				matchBracketedExpression(member, "switch");
				//expect(TOK_LBRACKET, "Expecting ( following switch");
				//parseExpression(peekToken0(), TOK_RBRACKET);
				//expect(TOK_RBRACKET, "Expecting ) terminating switch clause");				
				
				expect(TOK_LBRACE, "Expecting { starting switch terms");

				int peek_type = peekTokenType();
				while (peek_type != TOK_RBRACE && peek_type != TOK_EOF) {

					if (peekTokenType() == TOK_DEFAULT) {
						expect(TOK_DEFAULT);	
						parseBlock(member, scope + 1, is_constructor);					
					}
					else
					{
						expect(TOK_CASE,     "Expecting case signifying the start of a clause");
						matchBracketedExpression(member, "case");
						//expect(TOK_LBRACKET, "Expecting ( following a case");
						//parseExpression(peekToken0(), TOK_RBRACKET);
						//expect(TOK_RBRACKET, "Expecting ) terminating switch clause");		
					
						parseBlock(member, scope + 1, is_constructor);
					}   
					
					peek_type = peekTokenType();
				}

				expect(TOK_RBRACE, "Expecting { starting switch terms");
			}
			else if (peeked_type == TOK_CASE) {
				error("Unexpected case");
			}
			else if (peeked_type == TOK_DEFAULT) {
				error("Unexpected default");
			}
			else if (peeked_type == TOK_FOR)
			{

				int for_pos = getTokenIndex();
			
				Token for_start = expect(TOK_FOR);
				expect(TOK_LBRACKET, "Expecting ( following for");
				
				
				//for_start.m_annotation = ANNOTATION_FOR;			// really do I still use annotations?
				
				
				
				///////
				// UMMMM... this doesn't need to happen for for loops
				// this is really just needed to reserve a slot
				// any solutions that would allow us to not do this would be cool
				// but really for it to work we need to make this for each for loop
				// frig... sounds like a wasted slot every for loop
				
				IGVariable v = null;
				{
					Token iterator_name = Token.createUniqueVariableId(for_start);
					
					if (null == (v = member.addVariable(scope, for_pos, iterator_name, Type.get("Object")))) {
						softError(for_start, "A variable with this name already exists in the current scope");
					}
				}
					

				// need to resolve the foreach sort of construct that uses the in keyword
				if (peekTokenType() == TOK_VAR || (peekTokenType0() == TOK_ID && peekTokenType1() == TOK_COLON)) 
				{
					pos = getTokenIndex();
					member.beginScope(scope + 1, pos);
					
					// form (var v : type = value; expression; expression)
				
					//Log.log("Detected var");
					// put in special code to identify the type of variable and in what scope it started
				
					// a variable is defined requiring a new scope height				
					parseVarStatement(member, scope + 1);
					
					if (peekTokenType() == TOK_IN) 
					{
						//for_start.m_annotation = ANNOTATION_FOREACH;
						for_start.m_type = TOK_FOR_EACH;
						expect(TOK_IN);
						matchExpression(member);
						//parseExpression(peekToken0(), TOK_RBRACKET, TOK_RANGE);
						
						if (peekTokenType() == TOK_RANGE) {
							expect(TOK_RANGE);
							for_start.m_type = TOK_FOR_EACH_IN_RANGE;
							matchExpression(member);
							// this parse expression code needs to be neutered
							// it really shouldn't do anywhere near as much as it does
							//parseExpression(peekToken0(), TOK_RBRACKET);
						}
						
						expect(TOK_RBRACKET, "Expecting ) terminating for");
					}
					else
					{
						v.m_type = Type.INT;
						
						expect(TOK_SEMI, "Expecting semicolon following statement");
						//parseExpression(peekToken0(), TOK_SEMI);
						matchExpression(member);
						expect(TOK_SEMI, "Expecting ;");
						matchExpression(member);
						//parseExpression(peekToken0(), TOK_RBRACKET);
										
						expect(TOK_RBRACKET, "Expecting ) terminating for");					
					}
					
					parseBlock(member, scope + 2, is_constructor);
					
					// a variable is defined requiring a new scope height
					pos = getTokenIndex();
					member.closeScope(scope + 1, pos);
					
				}
				else
				{
					// form (v = value; expression; expression)
				
					//parseExpression(peekToken0(), TOK_SEMI, TOK_IN);
					matchExpression(member);
					if (peekTokenType() == TOK_IN) 
					{
						//for_start.m_annotation = ANNOTATION_FOREACH;
						for_start.m_type = TOK_FOR_EACH;
						expect(TOK_IN);
						matchExpression(member);
						//parseExpression(peekToken0(), TOK_RBRACKET, TOK_RANGE);
						
						if (peekTokenType() == TOK_RANGE) {
							expect(TOK_RANGE);
							for_start.m_type = TOK_FOR_EACH_IN_RANGE;
							matchExpression(member);
							//parseExpression(peekToken0(), TOK_RBRACKET);
						}
						
						expect(TOK_RBRACKET, "Expecting ) terminating for");
					}
					else
					{
						expect(TOK_SEMI, "Expecting ;");
						matchExpression(member);
						//parseExpression(peekToken0(), TOK_SEMI);
						expect(TOK_SEMI, "Expecting ;");
					
						matchExpression(member);
						//parseExpression(peekToken0(), TOK_RBRACKET);
						expect(TOK_RBRACKET, "Expecting ) terminating for");
					}
					
					
					parseBlock(member, scope + 1, is_constructor);
				}
			}
			else if (peeked_type == TOK_RETURN)
			{
				Token return_start = read();
				matchExpression(member);
				expect(TOK_SEMI, "Expecting semicolon following return.");
			}
			else if (peeked_type == TOK_CONTINUE)
			{
				Token continue_start = read();
				expect(TOK_SEMI, "Expecting semicolon following continue.");
			}
			else if (peeked_type == TOK_BREAK)
			{
				Token break_start = read();
				expect(TOK_SEMI, "Expecting semicolon following break.");
			}
			else if (peeked_type == TOK_THROW) 
			{
				Token throw_start = read();
				matchExpression(member);
				expect(TOK_SEMI, "Expecting semicolon following throw statement.");
			}
			else 
			{
				matchExpression(member);
				expect(TOK_SEMI, "Expecting semicolon following statement.");
			}
		}

		pos = getTokenIndex();
		member.closeScope(scope, pos);
		expect(TOK_RBRACE, "Expecting }");
		
	}
	
	
	
	static ArrayList<IGTokenList> s_member_inject = null;
	
	// add a member to inject before the last '}' of the class/enum/interface
	public static void addMemberInject(IGTokenList head)
	{
		s_member_inject.add(head);
	}

	
	private static void parseClass(IGSource source)
	{
		expect(TOK_LBRACE, "Expecting { starting class contents.");
		
		while (notPeekTokenTypeOrEOF(TOK_RBRACE))
		{
			//System.out.println("\tparseMember");
			int start = getTokenIndex();
			IGMember member = parseMember(source, TOK_CLASS);
			int end = getTokenIndex();
			
			member.setTokenRange(start, end);
		}

		// for each inject that was created
		// inject it into the end of the file
		while (s_member_inject.size() > 0) {
			IGTokenList head = s_member_inject.remove(0);
			
			injectTokenListBefore(head);
			//????? okay so how to we bully this code in place
			
			int start = getTokenIndex();
			IGMember member = parseMember(source, TOK_CLASS);
			int end = getTokenIndex();
			
			member.setTokenRange(start, end);
		}

		
		expect(TOK_RBRACE, "Expecting } following class contents.");
	}

	private static void parseInterface(IGSource source)
	{
		//System.out.println("parseInterface");
		expect(TOK_LBRACE, "Expecting { starting interface contents.");
		
		while (notPeekTokenTypeOrEOF(TOK_RBRACE))
		{
			int start = getTokenIndex();
			IGMember member = parseMember(source, TOK_INTERFACE);
			int end = getTokenIndex();
			
			member.setTokenRange(start, end);
		}
		
		expect(TOK_RBRACE, "Expecting } following interface contents.");
	}

	private static void parseEnum(IGSource source)
	{
			
	
		expect(TOK_LBRACE, "Expecting { starting enum contents.");
		
		int enum_value = 0;
		
		if (peekTokenType() != TOK_SEMI)
		{
			while (notPeekTokenTypeOrEOF(TOK_RBRACE))
			{
			
				int start = getTokenIndex();
			
			
				IGMember member = new IGMember(source);
		
				//member.m_parameter_start_index = getTokenIndex();
				Token name = expect(TOK_ID, "Expecting id");
				//member.m_parameter_end_index = getTokenIndex();

				member.addModifier(IGMember.PUBLIC);
				member.addModifier(IGMember.STATIC);
				member.addModifier(IGMember.FINAL);
				member.addModifier(IGMember.ENUM_VALUE);
				member.setType(IGMember.TYPE_VAR);
				member.setNameAndReturnType(name, Type.get(source.m_expected_component));
			
				if (source.hasMemberWithName2(name.m_value, IGSource.MEMBER_DEFAULT)) {
					softError(name, "Enum already contains entry for this value");
				}
			
				source.addAnnotation(name, ANNOTATION_ENUM_MEMBER, member);
				//name.m_annotation = ANNOTATION_ENUM_MEMBER;
				//name.m_annotation_object = member;
			
				source.addMember(member);
		
				if (peekTokenType() == TOK_ASSIGN)
				{
					expect(TOK_ASSIGN);
					
					
				
					member.m_first_body_token = getTokenIndex();
					matchPair2(member, TOK_COMMA, TOK_SEMI);
					
					
					//parseRoughExpression(true);
					
					/*
					if (peekTokenType() == IGLexer.LEXER_HEX)
					{
						Token value = expect(IGLexer.LEXER_HEX, "Expecting integer value to assign to enum");
						//member.m_value = new IGOptimizerValue(IGOptimizerValue.OTYPE_INT, value);
						//member.m_value_range = IGValueRange.createInt(IGLexer.LEXER_HEX, value.m_value);
						enum_value = Token.getIntValue(value) + 1;
					}
					else
					{
						Token value = expect(IGLexer.LEXER_INT, "Expecting integer value to assign to enum");
						//member.m_value = new IGOptimizerValue(IGOptimizerValue.OTYPE_INT, value);
						//member.m_value_range = IGValueRange.createInt(IGLexer.LEXER_INT, value.m_value);				
						enum_value = Token.getIntValue(value) + 1;
					}
					*/
					
					member.m_last_body_token = getTokenIndex();
				}
				else
				{
					//member.m_value = new IGOptimizerValue(IGOptimizerValue.OTYPE_INT, Token.createInteger(enum_value));
					//member.m_value_range = IGValueRange.createInt(enum_value);
					//member.addValueToken(Token.createInteger(enum_value));
					enum_value ++;
				}
				
				
				int end = getTokenIndex();
				member.setTokenRange(start, end);
			
				if (peekTokenType() == TOK_SEMI)
				{	
					break;
				}
			
				if (peekTokenType() != TOK_RBRACE)
				{
					expect(TOK_COMMA, "Expecting comma seperating enum types");
				}
			}
		}
		
		// a semi colon always follows a list of enum values
		/////////////////////////////////////////////////////////
		
		if (peekTokenType() == TOK_SEMI)
		{	
			expect(TOK_SEMI, "Expecting ; following list of enumerated values");
			
			while (peekTokenType() != TOK_RBRACE)
			{
				int start = getTokenIndex();
				IGMember member = parseMember(source, TOK_ENUM);
				member.addModifier(IGMember.FINAL);
				int end = getTokenIndex();
				
				member.setTokenRange(start, end);
			}
		}
		
		expect(TOK_RBRACE, "Expecting } following enum contents");//.m_annotation = ANNOTATION_OBJECT_END;
		
		//////////////////////////////////////////
		// This should only ever be done for enums
		
		// add the stub for toString
		if (!source.hasMemberWithName2("toString", IGSource.MEMBER_DEFAULT))
		{
			ArrayList<String> parameter_names = new ArrayList<String>();
			ArrayList<Type>   parameter_types = new ArrayList<Type>();
		
			addFunctionStub(source,
					IGMember.FINAL | IGMember.PUBLIC, 
					"toString", Type.get("String"),		//iglang.
					parameter_names,parameter_types);
		}
		
		// add the stub for equals
		if (!source.hasMemberWithName2("equals", IGSource.MEMBER_DEFAULT))
		{
			ArrayList<String> parameter_names = new ArrayList<String>();
			ArrayList<Type>   parameter_types = new ArrayList<Type>();
			
			parameter_names.add("other");
			parameter_types.add(Type.get(source.m_expected_component));
		
			addFunctionStub(source,
					IGMember.FINAL | IGMember.PUBLIC, 
					"equals", Type.get("bool"),
					parameter_names,parameter_types);
		}
		
		// add the stub for hashCode
		if (!source.hasMemberWithName2("hashCode", IGSource.MEMBER_DEFAULT))
		{
			ArrayList<String> parameter_names = new ArrayList<String>();
			ArrayList<Type>   parameter_types = new ArrayList<Type>();
		
			addFunctionStub(source,
					IGMember.FINAL | IGMember.PUBLIC, 
					"hashCode", Type.get("int"),
					parameter_names,parameter_types);
		}
	}


	//private static void DEBUG(String s)
	//{
		//System.out.println(s);
	//}
	
	
	
	public static void parseFile(IGSource source, IGTokenList token_list_head)
	{
		////DEBUG("parseFile");
		
		//System.out.println(source.m_expected_component);
	
		s_member_inject = new ArrayList<IGTokenList>();
			
		s_token_index = 0;
		s_source = source;
		s_token_list_head = token_list_head;
		
		boolean encountered_object = false;
		
		
		Token pkg = expect(TOK_PACKAGE, "Expecting package at top of file");
		//pkg.m_annotation = ANNOTATION_PACKAGE;
		
		
		IGScopePath sp = parseScope(false);
		if (!sp.isIdentical(source.m_file_based_package)) {
			
			
			String data = null;
			for (String d : source.m_file_based_package) {
				if (data == null) {
					data = d;
				}
				else {
					data = data + "." + d;
				}
			}
			
			if (data == null) {
				data = "package;";
			}
			else {
				data = "package " + data + ";";
			}
			
			softError(pkg, "Invalid package. Based on file path, expected: " + data);
		}
		
		s_source.setPackage(sp);
		
		
		
		
		expect(TOK_SEMI, "Expecting ; following package");
		
		////DEBUG("Parsed Scope: " + peekTokenType());
		
		boolean found_contents = false;
		
		while (peekTokenType() != TOK_EOF) 
		{
			if (peekTokenType() == TOK_IMPORT) 
			{
				if (encountered_object) 
				{
					error("All imports must precede any class, interface or enum definitions");
				}
			
				//DEBUG("Import");
				
				Token imp = expect(TOK_IMPORT, "Expecting import");
				//imp.m_annotation = ANNOTATION_IMPORT;
				
				s_source.addScope(parseScope(true));
				expect(TOK_SEMI, "Expecting ; following import");
			}
			else if (peekTokenType() == IGLexer.LEXER_NATIVE_BLOCK)
			{
				expect(peekTokenType());
			}
			else
			{
				Token first = peekToken0();
			
				// I guess right now we ignore all modifiers?	
				while (peekTokenType() == TOK_PUBLIC    ||
					   peekTokenType() == TOK_PRIVATE   ||
					   peekTokenType() == TOK_PROTECTED ||
					   peekTokenType() == TOK_INTERNAL  ||
					   peekTokenType() == TOK_FINAL     ||
					   peekTokenType() == TOK_REF_COUNT)
				{
					expect(peekTokenType());
				}

				
				if (peekTokenType() == TOK_CLASS) 
				{
					found_contents = true;
				
					expect(TOK_CLASS, "Expecting class");
				
					Token klass = expect(TOK_ID, "Expecting class name");
					if (!source.m_expected_component.equals(klass.m_value)) {
						error("Class name doesn't match name of file");
					}
					
					/////////////////////////////////////
					// Extract the templated parameters
					/////////////////////////////////////
					
					if (peekTokenType() == TOK_LT) 
					{
						expect(TOK_LT);	
						do
						{
							Token replacement = expect(TOK_ID, "Expecting identifier for templated parameter");
							source.addTemplateParameter(replacement);
							if (peekTokenType() != TOK_GT) {
								expect(TOK_COMMA, "Expecting a ',' to seperate templated types");
							}
						} 
						while (peekTokenType() != TOK_GT);
					
						expect(TOK_GT, "Expecting > to terminate templated type");
					}
					
					////////////////////////////////////////
					// Set the name and type of the source
					////////////////////////////////////////
					source.setNameAndType(klass, IGScopeItem.SCOPE_ITEM_CLASS);
				
	
					//////////////////////////////////////
					// See what class it extends if any.  If it doesn't extend anything
					// assume it extends Object
					//////////////////////////////////////
					
					if (peekTokenType() == TOK_EXTENDS) 
					{
						expect(TOK_EXTENDS);
						Type class_extends = parseType();
						source.setExtends(class_extends);
					}
				
					///////////////////////////////////////
					// See what interfaces it implements
					//////////////////////////////////////
				
					if (peekTokenType() == TOK_IMPLEMENTS) 
					{
						expect(TOK_IMPLEMENTS);
					
						while (peekTokenType() != TOK_LBRACE)
						{
							Type class_implements = parseType();
							source.addImplements(class_implements);
						
							if (peekTokenType() != TOK_LBRACE) {
								expect(TOK_COMMA, "Expecting comma separating implemented interfaces");
							}	
						}
					}
				
					parseClass(source);
					encountered_object = true;
				}
				else if (peekTokenType() == TOK_INTERFACE) 
				{
					found_contents = true;
					//first.m_annotation = ANNOTATION_INTERFACE;
					
					//DEBUG("Interface");
				
					expect(TOK_INTERFACE);
					Token klass = expect(TOK_ID, "Expecting an interface name");
					if (!source.m_expected_component.equals(klass.m_value)) 
					{
						error("Interface name doesn't match name of file");
					}
			
					source.setNameAndType(klass, IGScopeItem.SCOPE_ITEM_INTERFACE);	
				
					parseInterface(source);
				
					encountered_object = true;
				}
				else if (peekTokenType() == TOK_ENUM) 
				{
					found_contents = true;
					//first.m_annotation = ANNOTATION_ENUM;
					
					//DEBUG("Enum");
				
					expect(TOK_ENUM);
					Token klass = expect(TOK_ID, "Expecting an enum name");
					if (!source.m_expected_component.equals(klass.m_value)) {
						error("Enum name doesn't match name of file");
					}
				
					
					if (peekTokenType() == TOK_EXTENDS) 
					{
						expect(TOK_EXTENDS);
						Type enum_extends = parseType();
						source.m_enum_extends = enum_extends;
						//source.setExtends(class_extends);
						
						if (enum_extends != Type.INT &&
							enum_extends != Type.BOOL &&
							enum_extends != Type.DOUBLE) {
							error("Enums can only extend int,bool,double");	
						}
					}
					else
					{
						source.m_enum_extends = Type.get("int");
					}
				
				
				
					source.setNameAndType(klass, IGScopeItem.SCOPE_ITEM_ENUM);
					parseEnum(source);
				
					encountered_object = true;
				}
				else
				{
					System.out.println("TT: " + peekTokenType());
					expect(TOK_EOF, "Unexpected symbol. FR");
				}
			}
		}
		
		///////////////////////////
		// check for empty file
		//////////////////////////
		if (!found_contents) {
			softError("Expecting class, interface or enum in file.");
		}
		

		// create a formal array of tokens to assign to the source		
		{
			Token [] list = new Token[s_token_index];
			for (int i = 0; i < s_token_index; i++) {
			
				// skip over null entries
				// this can and will happen with anonymous functions
				while (token_list_head.m_token == null) {
					token_list_head = token_list_head.m_next;
				}
				
				list[i] = token_list_head.m_token;
				token_list_head = token_list_head.m_next;
			}
			source.setTokenList(list);
		}
	}

}