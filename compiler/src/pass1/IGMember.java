/*
Copyright (c) 2014-2017 Incubator Games Ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

package pass1;

import java.util.*;

public class IGMember extends IGScopeItem
{
	public static final int STATIC 		= 0x0001;
	public static final int FINAL  		= 0x0002;		// const also maps to this
	public static final int PUBLIC 		= 0x0004;
	public static final int PROTECTED 	= 0x0008;
	public static final int VIRTUAL 	= 0x0010;
	public static final int OVERRIDE    = 0x0020;
	public static final int PRIVATE     = 0x0040;
	public static final int INLINE      = 0x0080;
	public static final int INTERNAL    = 0x0100;
	public static final int SET         = 0x0200;
	public static final int GET			= 0x0400;
	public static final int ENUM_VALUE	= 0x0800;
	public static final int STUB        = 0x1000;
	public static final int REF_COUNTED = 0x2000;
	
	public static final int NOINLINE    = 0x4000;
	
	public int   m_inline_candidate = 0;
	public IGNode m_inline_candidate_node = null;
	
	public IGSource m_parent_source = null;
	public IGVariableScope m_variable_scope = null;
	
	
	public IGMember(IGSource parent) { //, int type) {
		m_parent_source = parent;
		//m_type = type;
	}
	
	
	public IGSource getSource() {
		return m_parent_source;
	}
	
	
	
	/**
	 * Determine if the function represented by this member
	 * can be inlined.  This is explicitly targetting the case
	 * where:
	 *
	 * public function get x() : int { return m_x; }
	 */
	
	public boolean isInlineFunctionCandidate() 
	{
		if ((m_modifiers & FINAL) == 0) { return false; }
		if ((m_modifiers & STUB)  != 0) { return false; }
		if (m_type != TYPE_FUNCTION)   { return false; }
		if (m_parameters.size() != 0)   { return false; }
		if (m_return_type == Type.VOID) { return false; }
		
		return m_inline_candidate != 0;
	}
	
	public static final String [] s_modifier_names = new String[]{
		"static",
		"final",
		"public",
		"protected",
		"virtual",
		"override",
		"private",
		"inline",
		"internal",
		"set",
		"get",
		"enum-value",
		"stub"
	};
	
	
	///////////////////////////////////
	// IGScopeItem
	///////////////////////////////////
	

	
	public  int       getScopeType() 
	{
		if (m_type == TYPE_VAR) 
		{
			// variable
			if ((m_modifiers & STATIC) != 0) return SCOPE_ITEM_STATIC_VARIABLE;
			else                             return SCOPE_ITEM_VARIABLE;
		}
		else
		{
			// function
			if ((m_modifiers & STATIC) != 0) return SCOPE_ITEM_STATIC_FUNCTION;
			else                             return SCOPE_ITEM_FUNCTION;
		}
	}
	
	public  IGScopePath getScopePath() {
		//return m_scope_path;
		return null;
	}
	
	public Type getDataType(Type relative) 
	{
		// if the relative type contains generics, we need to substitute in our current values for the 
		// generics into them
		// ie.   Map<String,String>
		// asks for its member    bool containsKey(T1)  => bool containsKey(String)
		// this should remap the datatype to 
	
	

	
		Type to_return = null;
		if (m_type  != TYPE_VAR)
		{
			if (null == m_function_type) 
			{
				// calculate the number of parameters required for the function
				// this doesn't include the implicit THIS parameter for
				// member functions.  That is designated by the type StaticFunction or Function
				
				int required_count = 0;
				ArrayList<Type> types = Type.allocTempVector();	//new Vector<Type>();
				
				int parameter_count= m_parameters.size();
				for (int i = 0; i < parameter_count; i++) {
				//for (IGVariable v : m_parameters) {
				
					IGVariable v = m_parameters.get(i);
					types.add(v.m_type);
					if (!v.m_has_default) {
						required_count ++;
					}
				}
				
				if (m_return_type == null) {
					throw new RuntimeException("Umm what just happened");
				}
				
				Type parent_type = Type.get("void");	//m_parent_source.getInstanceDataType();
				
				if ((m_modifiers & STATIC) != 0) {
					m_function_type = Type.get("<wrapper>", 
								parent_type, 
								Type.get("StaticFunction", Type.get(types, required_count), m_return_type));
				}
				else {
					m_function_type = Type.get("<wrapper>", 
								parent_type,
								Type.get("Function",       Type.get(types, required_count), m_return_type));
				}
				
				Type.freeTempVector(types);
			}		
		}
	
		if (m_type == TYPE_VAR) {
			to_return =  m_return_type;
		}
		else
		{
			to_return = m_function_type;
		}
		
		
		if (relative != null && relative.m_scope instanceof IGSource) {
			// it is entirely possible for this situation to occur
			// as we might be calling a method of a parent class
			
			// thus relative would identify with the child
			// but  the method/variable would identity with the parent
			
			IGSource relative_source = (IGSource)relative.m_scope;
			Vector<Type> template_parameters = relative_source.m_template_types;
			
			for (int i = 0; i < template_parameters.size(); i++)
			{
				Type pattern = template_parameters.get(i);	//Type.get(template_parameters.get(i).m_value);
				Type replace = i == 0 ? relative.m_left : relative.m_right;
	
				to_return = to_return.replace(pattern, replace);
			}
			//throw new RuntimeException("Umm.. this should have never hapened. " + relative + " " + m_parent.m_expected_component);
		}
		
		
		
		
		
		return to_return;
	}
	
	public IGSource m_parent = null;
	
	

	
		
	
	public void setType(int t)
	{
		m_type = t;
		if (t == TYPE_FUNCTION) {
			m_parameters = new ArrayList<IGVariable>();
			m_variables  = new ArrayList<IGVariable>();
		}
	}
	

	
	
	public static final int TYPE_VAR = 0;
	public static final int TYPE_FUNCTION = 1;
	
	
	public String       m_clean_name;		// Non mangled name
	public int          m_clean_type;		// IGSource.(GET,SET,DEFAULT)
	
	
	public int          m_type;
	public Token        m_name;
	public Type         m_return_type;
	public IGValueRange m_return_value_range;
	public Token        m_return_type_token;
	
	public Type  m_function_type;
	public int m_modifiers = PRIVATE;
	
	// the node representing this members ast
	public IGNode m_base_node               = null;			// NYI FOR VARIABLES?
	public IGNode m_node                   = null;			// NYI FOR VARIABLES?
	public ArrayList<IGVariable> m_parameters = null; //new ArrayList<IGVariable>();		// strict subset of m_variables
	private ArrayList<IGVariable> m_variables  = null; //new ArrayList<IGVariable>();
	
	
	
	// where the body of the function exists       function x() : void  **{ do something }**
	// or where the defaults of a variable exists  var x : int = **some value**;
	public  int             m_first_body_token = -1;
	public  int			    m_last_body_token = -1;
	
	// where the parameter list starts
	// ( ID : TYPE, ID : TYPE, ID : TYPE ) : TYPE
	// this should also include default values boy
	////////////////////////////////////
	public  int             m_parameter_start_index = -1;
	public  int             m_parameter_end_index = -1;
	
	
	// the range of tokens that this Source member was extracted from
	public int m_member_token_start = -1;
	public int m_member_token_end   = -1;
	
	
	
	// for constructors
	public boolean        m_super_constructor_called = false;
	
	
	public boolean isCorrespondingArraySetter(IGMember setter)
	{
		if (m_return_type == Type.VOID) { return false; }
		if (m_parameters.size() != 1)  return false;
		if (setter.m_return_type != Type.VOID) { return false; }
		if (setter.m_parameters.size() != 2) return false;
		
		if (m_return_type != setter.m_parameters.get(1).m_type) {
			return false;
		}	
		
		if (m_parameters.get(0).m_type != setter.m_parameters.get(0).m_type) {
			return false;
		}
		
		return true;
	}
	
	
	public int getParameterCount() {
		return m_parameters.size();
	}
		
		
	public int getVariableCount() {
		return m_variables.size();
	}

	public List<IGVariable> getVariables() {
		return m_variables;
	}

	/**
	 * Return the bitset of the modifiers that contribute to the protection of a given
	 * member.  (Public, Private, Protected, Internal)
	 */	
		
	public int getProtection()
	{
		return m_modifiers & (PUBLIC | PROTECTED | PRIVATE | INTERNAL);
	}	
	
	
	public void addModifier(int m) 
	{
		if (m == PUBLIC || m == PRIVATE || m == PROTECTED || m == INTERNAL) {
			m_modifiers &= ~(PUBLIC | PRIVATE | PROTECTED | INTERNAL);
		}
		m_modifiers |= m;
	}
	
	public void setModifier(int m) 
	{
		m_modifiers |= m;
	}
	
	
	public boolean hasModifier(int m) 
	{
		return (m_modifiers & m) == m;
	}
	
	
	public boolean isPrivate() {
		return hasModifier(PRIVATE);
	}
	
	public boolean isInternal() {
		return hasModifier(INTERNAL);
	}
	
	
	public void clearModifier(int m) {
		m_modifiers &= ~m;
	}
	
	
	public final String getMemberNameAsString() {
		return m_name.m_value;
	}
	
	public final String getSourceNameAsString() {
		return "" + m_parent_source.getInstanceDataType();
	}
	

	// TO REPLACE VALUE TOKENS
//	public IGOptimizerValue m_value = null;				// THIS IS BEING PHASED OUT
	//public IGValueRange     m_value_range = null;		// the value range emitted by this member
	
	
	
	/**
	 * Get the value range representing the known value of this member
	 * The native keyword should play into this.
	 * As items with "native" infront of the them should not be optimized away
	 */
	
	public IGValueRange getDefaultValueRange(IGValueRange use_as_default) 
	{
		if (m_declaration_node == null) {
			throw new RuntimeException("Missing data");
		}
		
		
		
		
		IGNode      n = m_declaration_node;
		
		/*
		if (n.m_type == IGNode.NODE_ENUM_MEMBER) 
		{
			{
				throw new RuntimeException("This is an enum... we need to evaluate this more specifically");
			}
		}
		*/
		
		
		IGNode assign = n.findRightMostWithNodeAndTokenType(IGNode.NODE_BINARY, IGConsts.TOK_ASSIGN);
		
		if (assign == null) {
			if (use_as_default == null) {
				// return the default based on the type
				// ie.  0 is the default for int
				// ie. 0.0 is the default for double
				// null is the default for Object
				return m_return_type.getDefaultValueRange();
			}
			else {
				return use_as_default;
			}
		}

		if (assign.m_children.get(1).m_value_range == null) {
			return null;
		}

		return IGValueRange.createImplicitCast(
				m_return_type,
				assign.m_children.get(1).m_type, 
				assign.m_children.get(1).m_value_range);	
	}

	

	
	/*
	 * Only works if this is a FUNCTION
	 * @return the minimum number of parameters required.  If this was a function
	 *         adds an additional parameter for "this" if non static
	 */
	
	public int getMinParameterCount() 
	{
		if (m_parameters == null) {
			throw new RuntimeException("This'd never work: " + m_name.m_value);
		}
		int i = 0;
		// break on the first parameter with a default value
		for (i = 0; i < m_parameters.size(); i++) {
			if (m_parameters.get(i).m_has_default) {
				break;
			}
		}
		int k = hasModifier(STATIC) ? 0 : 1;
		return k + i;
	}
	
	/*
	 * Only works if this is a FUNCTION
	 * @return the maximum number of parameters required. If this was a function.
	 *         adds an additional parameter for "this" if non static
	 */
	
	public int  getMaxParameterCount() {
		int k = hasModifier(STATIC) ? 0 : 1;
		return m_parameters.size() + k;
	}
	
	/*
	 * Only works if this is a FUNCTION
	 * @returns the conservative number of local variable slots needed for this function
	 *  
	 */
	
	public int getConservativeLocalVariableCount()
	{
		int k = hasModifier(STATIC) ? 0 : 1;
		return m_variables.size() + 1;
	}
	
	
	/**
	 * Does this IGMember represent a static of member function
	 */
	
	public boolean isFunction() {
		return m_type == TYPE_FUNCTION;
	}
	
	
	/**
	 * Does this IGMember represent a static or member variable
	 */
	
	public boolean isVariable() {
		return m_type == TYPE_VAR;
	}
	
	/*
	 * @return true if this member is a function that is a constructor
	 */
	
	public boolean isConstructor()
	{
		if ( m_name.m_value.equals("<constructor>") && 
			 m_type == TYPE_FUNCTION && !hasModifier(STATIC)) {
			return true;	 
		}
		
		return false;
	}
	

	
	
	public void setNameAndReturnType(Token name, Type type)
	{
		setNameAndReturnType(name, type, null);
	}
	
	
	/**
	 * Add a parameter to a function
	 */
	
	public IGVariable addParameter(int position, Token name, Type type)
	{	
		return addParameter(position, name, type, null);
	}
	
	
	public void setNameAndReturnType(Token name, Type type, Token type_token)
	{
		m_name = name;
		m_return_type = type;
		m_return_type_token = type_token;
	}
	
	/**
	 * Add a parameter to a function
	 */
	
	public IGVariable addParameter(int position, Token name, Type type, Token type_token)
	{	
		int scope = 0;
		/*
		final int variable_count = m_variables.size();
		for (int i = 0; i < variable_count; i++)
		{
			IGVariable v = m_variables.get(i);
			if (v.inConflict(0, position, name.m_value)) {
				return null;
			}
		}
	
		IGVariable v2 = new IGVariable(0, position, name, type);
		v2.m_type_token = type_token;
		m_parameters.add(v2);
		m_variables.add(v2);
		*/
		
		if (m_variable_scope.nameExists(name.m_value)) {
			return null;
		}
		
		
		//System.out.println("Add-variable: " + name.m_value + " occurrance-count: " + occurance_count);
	
		IGVariable v2 = new IGVariable(scope, position, name, type);
		v2.m_occurrance_count = 0;
		v2.m_type_token       = type_token;
		m_variables.add(v2);
		m_parameters.add(v2);
		
		m_variable_scope.add(v2);
		
		return v2;
	}
	
	/**
	 * Add a variable to a function
	 */
	
	public IGVariable addVariable(int scope, int position, Token name, Type type)
	{
		return addVariable(scope, position, name, type, null);
	}
	
	public IGVariable addVariable(int scope, int position, Token name, Type type, Token type_token)
	{
	
		
		int occurance_count = 0;
		final int variable_count = m_variables.size();
		for (int i = 0; i < variable_count; i++)
		{
			IGVariable v = m_variables.get(i);
			if (v.m_name.m_value.equals(name.m_value)) {
				occurance_count++;
			}
		}
		
		
		if (m_variable_scope.nameExists(name.m_value)) {
			return null;
		}
		
		
		//System.out.println("Add-variable: " + name.m_value + " occurrance-count: " + occurance_count);
	
		IGVariable v2 = new IGVariable(scope, position, name, type);
		v2.m_occurrance_count = occurance_count;
		v2.m_type_token = type_token;
		m_variables.add(v2);
		
		m_variable_scope.add(v2);
		
		return v2;
	}
	
	// To remove... please?
	//public void addValueToken(Token t) {
		//m_value_tokens.add(t);
	//}
	
	
	public void beginScope(int scope, int position) {
		m_variable_scope = new IGVariableScope(m_variable_scope);
	}
	
	public void closeScope(int scope, int position) {
		// set the end position on all variables
		
		/*
		final int variable_count = m_variables.size();
		for (int i = 0; i < variable_count; i++)
		{
			IGVariable v = m_variables.get(i);
			if (v.m_scope == scope && v.m_end_position == -1) {
			    v.m_end_position = position;
			} 
		}
		*/
		
		IGVariableScope old = m_variable_scope;
		
		for (int i = old.m_children.size() - 1; i >= 0; i--) {
			IGVariable v = old.m_children.get(i);
			v.m_end_position = position;
		}
		
		m_variable_scope = old.m_parent;
		
		if (scope == 0 && m_variable_scope != null) {
			throw new Error("Mismatched variable scope");
		}
	}
	
	public void debug()
	{
		System.out.print("\t");
		System.out.println(m_return_type);
		System.out.println(m_name.m_value);
	}
	
	

	
	final public int lookupLocalVariable(Token tok, boolean setter_context)
	{
		return lookupLocalVariable(tok.m_value,  tok.m_token_index, setter_context);	
	}
	
	
	/*
	 * Lookup a local variable within the function.  
	 * @param key         - the name of the variable
	 * @param token_index - the token index within the file thats referencing the local variable
	 * @param setter_context - whether the variable is being used as an lvalue (true) or an rvalue (false)
	 * @returns the index into the local variable table
	 */
	
	final public int lookupLocalVariable(String key, int token_index, boolean setter_context)	
	{
		//String key = tok.m_value;
		//int token_index =;
	
		int len = m_variables.size();
		int posn = 0;
		for (int i = 0; i < len; i++) 
		{
			// todo have separate indexes for when the variable is get able
			// vs setable
		
			IGVariable v = m_variables.get(i);

			//System.err.println("lookup: " + key + " vs " + v.m_name.m_value + " " + token_index + " in [" + v.m_start_position + "," + v.m_end_position + ")");
			if (v.m_start_position <= token_index && token_index < v.m_end_position &&
				    v.m_name.m_value.equals(key)) {
				    
				if (!setter_context) {
					if (v.m_available_position > token_index) {
						//return null;
					}
				}
			
				return posn;	    
			}

			if (v.m_counted) {
				posn++;
			}
		}
		
		return -1;	
	}
	
	
	/**
	 * Print out the local variables and parameters this function has
	 */
	
	public void debugVariables()
	{
		System.err.println("Local variables: ");
		int len = m_variables.size();
		for (int i = 0; i < len; i++) {
			IGVariable v = m_variables.get(i);
			System.err.println("\t" + i + ">\t" + v.m_name.m_value + "\t" + v.m_type + "\t" + v.m_start_position + " " + v.m_available_position);
		}
	
	}
	
	

	

	/*
	 * Attempt to resolve a local variable first, then ask the source we're a child of if they know of
	 * the field being accessed.
	 *
	 * Todo replace setter_context, with something atune to IGScopeLookup which includes the path
	 * of the object trying to access this field, and also allows for multiple items to be returned
	 */
	
	public IGScopeItem lookup(String key, int token_index, boolean setter_context) 
	{
		int len = m_variables.size();
		for (int i = len - 1; i >= 0; i--) 
		{
			// todo have separate indexes for when the variable is get able
			// vs setable
		
			IGVariable v = m_variables.get(i);

			//System.err.println("lookup: " + key + " vs " + v.m_name.m_value + " " + token_index + " in [" + v.m_start_position + "," + v.m_end_position + ")");
			if (v.m_start_position <= token_index && 
				    token_index < v.m_end_position &&
				    v.m_name.m_value.equals(key)) {
				    
				if (!setter_context) {
					if (v.m_available_position > token_index) {
						//return null;
					}
				}
			
				return v;	    
			}
		}
		
		return m_parent.lookup(key, setter_context);
	}
	
	
	
	/**
	 * This can be used with resolving scope for member variables... but is super weird
	 */
	
	public void lookup(String key, IGScopeLookup lk) 
	{
	
		if (m_type != TYPE_FUNCTION) {
			//throw new RuntimeException("What the hell? " + m_name.m_value);
		}
		
		final int value_type = lk.m_value_type;
		
		if (null != m_variables)
		{		
			final int len = m_variables.size();
			final int token_index = lk.m_token_index;
			
		
			IGVariable candidate = null;
		
			// The variables list is structured in such a way that if you find the first
			// occurrance from the bottom up it will be the highest scope.
		
			for (int i = len - 1; i >= 0; i--) 
			{
				// todo have separate indexes for when the variable is get able
				// vs setable
		
				IGVariable v = m_variables.get(i);

				// depending on if its a LVALUE or RVALUE
				// different conditions will apply
				// this should also apply for const variables ?
			
				//System.err.println("lookup: " + key + " vs " + v.m_name.m_value + " " + token_index + " in [" + v.m_start_position + "," + v.m_end_position + ")");
				if (v.m_start_position <= token_index && 
						token_index < v.m_end_position &&
						v.m_name.m_value.equals(key)) {
					
					
					if (value_type == IGScopeLookup.RVALUE) {
						if (v.m_available_position <= token_index) {
							//lk.addError("IGVariable is not yet initialized.");
							candidate = v;
							break;
						}
					}    
					else
					{
						candidate = v;
						break;
					}
			
				
					//return v;	    
				}
			}
		
			if (candidate != null) {
				lk.add(candidate);
			}
		}
		
		if (lk.hasResults()) {
			return;
		}
		
		IGScopeItem result = m_parent.lookup(key, value_type == IGScopeLookup.LVALUE);
		if (null != result) {
			lk.add(result);
		}
		// TODO ... also check contexts
		//return m_parent.lookup(key, setter_context);
	}
	
	

	
	public void setTokenRange(int start, int end) {
		m_member_token_start = start;
		m_member_token_end   = end;
	}
}
