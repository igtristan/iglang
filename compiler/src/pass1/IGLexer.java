/*
Copyright (c) 2014-2017 Incubator Games Ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

package pass1;

import java.io.InputStream;

class IGLexerEntry
{
	public String m_string = null;
	public int    m_id     = -1;
	
	public IGLexerEntry [] m_next = new IGLexerEntry[128];

	public IGLexerEntry(boolean is_root) 
	{
	
	}
	
	public IGLexerEntry(int symbol, String s) {
		m_id = symbol;
		m_string = s;
	}
	
	
	public void fill(String set_a, String set_b, IGLexerEntry repeat )
	{
		for (int i = set_a.length() - 1; i >= 0; i --) {
			int  c = set_a.charAt(i);
			
			if (m_next[c] == null) {
				m_next[c] = repeat;
			}
			else {
				m_next[c].fillRepeating(set_b, repeat);
			}
		}
	}
	
	public void fillRepeating(String set_b, IGLexerEntry repeat) {
		
		if (m_id == -1) {
			m_id     = repeat.m_id;
			m_string = repeat.m_string;
		}
	
		for (int i = set_b.length() - 1; i >= 0; i --) {
			int  c = set_b.charAt(i);
		
			if (m_next[c] == null) {
				m_next[c] = repeat;
			}
			else {
				m_next[c].fillRepeating(set_b, repeat);
			}	
		}
	}

	public void initRoot(String native_block) 
	{
		addSymbol("\'", 0, IGLexer.LEXER_CHAR);
		addSymbol("\"", 0, IGLexer.LEXER_STRING);
		//addSymbol("\0", 0, IGLexer.LEXER_EOF);

		// id remaps		
		{
			addSymbolWithReplace("constructor", 0, IGLexer.LEXER_ID, "<constructor>");
			addSymbolWithReplace("delete",      0, IGLexer.LEXER_ID, "__delete");
			addSymbolWithReplace("using",       0, IGLexer.LEXER_ID, "__using");
			addSymbolWithReplace("friend",      0, IGLexer.LEXER_ID, "__friend");
		
			if (native_block.equals("cpp")) {
				addSymbolWithReplace("LITTLE_ENDIAN", 0, IGLexer.LEXER_ID, "__LITTLE_ENDIAN");
				addSymbolWithReplace("BIG_ENDIAN",    0, IGLexer.LEXER_ID, "__BIG_ENDIAN");
			}
		
			if (native_block.equals("php")) {
			
			// PHP reserved keywords
			// http://php.net/manual/en/reserved.keywords.php
			
				addSymbolWithReplace("list", 0, IGLexer.LEXER_ID, "__list");
				addSymbolWithReplace("echo", 0, IGLexer.LEXER_ID, "__echo");
			}
		}
	
		// hex string
		{
			IGLexerEntry hex_start_a = addSymbol("0x", 0, -1);
			IGLexerEntry hex_start_b = addSymbol("0X", 0, -1);
		
			IGLexerEntry hex_repeat = new IGLexerEntry(IGLexer.LEXER_HEX, null);
			hex_repeat.set("0123456789ABCDEFabcdef", hex_repeat);	
		
			hex_start_a.set("0123456789ABCDEFabcdef", hex_repeat);
			hex_start_b.set("0123456789ABCDEFabcdef", hex_repeat);
		}
		
		// int string
		{
			// set up the int repeat to fold back on itself
			IGLexerEntry int_repeat = new IGLexerEntry(IGLexer.LEXER_INT, null);
			int_repeat.set("0123456789", int_repeat);
			
			IGLexerEntry int_zero = addSymbol("0", 0, IGLexer.LEXER_INT);
			this.set("123456789", int_repeat);
			
			// float string
			
			IGLexerEntry float_repeat = new IGLexerEntry(IGLexer.LEXER_FLOAT, null);
			float_repeat.set("0123456789", float_repeat);
			int_repeat.set(".", float_repeat);
			int_zero.set(".", float_repeat);
		}
		
		// id
		{
		
			String start_id_set = 	"abcdefghijklmnopqrstuvwxyz" +
									"ABCDEFGHIJKLMNOPQRSTUVWXYZ_";
		
			String id_set = "0123456789abcdefghijklmnopqrstuvwxyz" +
							"ABCDEFGHIJKLMNOPQRSTUVWXYZ_";
		
			IGLexerEntry id_repeat = new IGLexerEntry(IGLexer.LEXER_ID, null);
							
			id_repeat.set(id_set, id_repeat);
							
			this.fill(start_id_set, id_set, id_repeat);				
		}
		

	}
	
	public void set(String symbols, IGLexerEntry target) {
		for (int i = symbols.length() - 1; i >= 0; i --) {
			int  c = symbols.charAt(i);
			m_next[c] = target;
		}
	}
	
	public void addSymbolWithReplace(String symbol, int symbol_idx, int symbol_id, String replace)
	{
		addSymbol(symbol, symbol_idx, symbol_id).m_string = replace;
	}

	public IGLexerEntry addSymbol(String symbol, int symbol_idx, int symbol_id) {
		if (symbol_idx == symbol.length()) {
			if (symbol_id != -1) {
				this.m_string = symbol;
				this.m_id     = symbol_id;
			}
			
			return this;
		}
		else {
			int c = symbol.charAt(symbol_idx);
			if (m_next[c] == null) {
				m_next[c] = new IGLexerEntry(false);
			}
			
			return m_next[c].addSymbol(symbol, symbol_idx + 1, symbol_id);
		}
	}
}


public class IGLexer
{
	
	public static final int  LEXER_MAX_USER_DEFINED_SYMBOLS	=	128;
	public static final int  LEXER_BUILT_IN_SYMBOL_COUNT	=	12;
	public static final int  LEXER_MAX_SYMBOL_LENGTH		=	16;

	public static final int  LEXER_EOF			=			(LEXER_MAX_USER_DEFINED_SYMBOLS + 0);	
	public static final int  LEXER_STRING		=			(LEXER_MAX_USER_DEFINED_SYMBOLS + 1);
	public static final int  LEXER_ID			=			(LEXER_MAX_USER_DEFINED_SYMBOLS + 2);
	public static final int  LEXER_INT			=			(LEXER_MAX_USER_DEFINED_SYMBOLS + 3);
	public static final int  LEXER_FLOAT		=			(LEXER_MAX_USER_DEFINED_SYMBOLS + 4);
	public static final int  LEXER_HEX			=			(LEXER_MAX_USER_DEFINED_SYMBOLS + 5);
	public static final int  LEXER_INVALID		=			(LEXER_MAX_USER_DEFINED_SYMBOLS + 6);
	public static final int  LEXER_NATIVE_BLOCK	=			(LEXER_MAX_USER_DEFINED_SYMBOLS + 7);
	public static final int  LEXER_CHAR			=           (LEXER_MAX_USER_DEFINED_SYMBOLS + 8);
	public static final int  LEXER_CHAR_MULTILINE =           (LEXER_MAX_USER_DEFINED_SYMBOLS + 9);
	public static final int  LEXER_STRING_MULTILINE = 		  (LEXER_MAX_USER_DEFINED_SYMBOLS + 10);
	public static final int  LEXER_STRING_INVALID_ESCAPE = 		  (LEXER_MAX_USER_DEFINED_SYMBOLS + 11);
	
	
	String              m_native_block;
	String			[]	m_symbols = new String[LEXER_MAX_USER_DEFINED_SYMBOLS + LEXER_BUILT_IN_SYMBOL_COUNT];
	
	boolean				m_symbol_dfsa_dirty = true;
	IGLexerEntry		m_symbol_root = new IGLexerEntry(true);
	
	String					m_file;
	IGLookaheadReader		m_reader;			
	int						m_peek_active;		// is there currently a value peeked?
	Token					m_peek;		
	
	public IGTokenSource           m_token_source;
	
	public String typeToString(int s)
	{
		if (s < 0 || s >= m_symbols.length) {
			return "unknown";
		}
		return m_symbols[s];
	}
	
	public void clearSymbols()
	{
		int j;
		
		for (j = 0; j < (LEXER_MAX_USER_DEFINED_SYMBOLS + LEXER_BUILT_IN_SYMBOL_COUNT); j++) {
			this.m_symbols[j] = null;	
		}

		m_symbol_root = new IGLexerEntry(true);
		m_symbol_dfsa_dirty = true;
		
		this.m_symbols[LEXER_INT]		= ("int");
		this.m_symbols[LEXER_STRING]	= ("string");
		this.m_symbols[LEXER_ID]		= ("id");
		this.m_symbols[LEXER_EOF]		= ("<EOF>");
		

	}
	
	public void addSymbol( String symbol, int symbol_id)
	{
		assert 0 <= symbol_id && symbol_id < LEXER_MAX_USER_DEFINED_SYMBOLS : "Invalid symbol id";
		
		String str = (symbol);	
		this.m_symbols[symbol_id] = str;
		
		m_symbol_root.addSymbol(symbol, 0, symbol_id);
		m_symbol_dfsa_dirty = true;
	}
	
	public void buildTable(String native_block)
	{
		this.m_native_block = native_block;
		if (m_symbol_dfsa_dirty) {
			m_symbol_dfsa_dirty = false;
			m_symbol_root.initRoot(m_native_block);
		}
	}
	
	public int initWithFile( String path, InputStream file)
	{
		//System.err.println("err Opening file: " + path);
		//System.err.println("out Opening file: " + path);
	
		m_reader = new IGLookaheadReader();
		if (m_reader.init(path, file) == 0) {
			return 0;
		}
		
		
		this.m_file = (path);
		this.m_peek_active = 0;
		this.m_peek = null;
		
		
		
		this.m_token_source = new IGTokenSource(path, m_reader.m_data);
		
		return 1;
	}
	
	public int openFile( String path, InputStream file)
	{
		
		if (0 == m_reader.init(path, file))
		{
			return 0;
		}
		
		this.m_file 		= path;
		this.m_peek_active 	= 0;
		
		return 1;
	}
	
	public int peek( Token  tok)
	{
		if (0 == this.m_peek_active) {
			this.read(this.m_peek);
			this.m_peek_active = 1;
		}
		
		tok.copy(this.m_peek);

		return (tok.m_type == LEXER_EOF) ? 0 : 1;
	}
	
	private boolean stringsMatch(String symbol, char [] str)
	{
		for (int i = 0; i < symbol.length(); i++) {
			if (symbol.charAt(i) != str[i]) {
				return false;
			}
		}
		return true;
	}
	
	// pulled this out of the read function
	char [] m_tmp_string = new char[LEXER_MAX_SYMBOL_LENGTH + 1];
	
	public int read( Token tok)
	{
		//tok.m_file		= this.m_file;
		tok.m_token_source = m_token_source;
	
		IGLookaheadReader reader = this.m_reader;
		
		if (this.m_peek_active == 1) 
		{
			
			tok.copy(this.m_peek);
			this.m_peek_active = 0;
			
			return (tok.m_type == LEXER_EOF) ? 0 : 1;
		}	
		
		reader.eatWhitespace();
		
		char c = reader.peek(0);
		//System.out.println("peek '" + c + "'") ;
		if (c == 0)
		{
			//tok.m_file		= this.m_file;
			tok.m_value		= this.m_symbols[LEXER_EOF];
			tok.m_type		= LEXER_EOF;
			tok.m_whitespace = "";
			tok.m_line		= (short)reader.m_curr_line;
			tok.m_token_source = m_token_source;
			tok.m_position = reader.m_position - 1;
			//tok.m_column	= 0;
			return 0;
		}
		
		boolean has_comment = false;
		
		do
		{
			has_comment = false;
			// Handle single line c++ style comments
			
			c = reader.peek(0);
			if (c == '/' && '/' == reader.peek(1)) 
			{
				// throw away the first two backslashes
				reader.readWhitespace();	
				reader.readWhitespace();	
				
				// throw away characters until we reach a new line character
				char c2 = '/';
				while (c2 != '\n' && c2 != '\0')			// might need to change this for dos compatibility
				{
					c2 = reader.readWhitespace();			
				}
				
				reader.eatWhitespace();
				has_comment = true;
			}
			
			reader.eatWhitespace();
			
			c = reader.peek(0);
			if (c == '/' && '*' == reader.peek(1))
			{
				// throw away the first two backslashes
				reader.readWhitespace();	
				reader.readWhitespace();	
				
				while (!(reader.peek(0) == '*' && reader.peek(1) == '/'))
				{
					reader.readWhitespace();
				}
				reader.readWhitespace();
				reader.readWhitespace();
				
				has_comment = true;
			}
			
			reader.eatWhitespace();
			
			c = reader.peek(0);
			if (c == '@') 
			{
				int column 	= reader.m_curr_column;
				int line  	= reader.m_curr_line;
				int position = reader.m_position;
//				char [] data = reader.m_data;
			
				String s = "";
				
				// ie  @as3
				s += (char)reader.read();
				//s += (char)reader.read();
				//s += (char)reader.read();
				//s += (char)reader.read();
				
				boolean found_first_whitespace = false;
				
				// throw away characters until we reach a new line character
				while (((char)reader.peek()) != '\n' && 
						((char)reader.peek()) != '\0')			// might need to change this for dos compatibility
				{
				
					if (!found_first_whitespace) 
					{
						char peeked = (char)reader.peek();
						
						if (peeked == ' ' || peeked == '\t')
						{
							if (s.startsWith("@code:!")) {
								if (s.equals("@code:!" + m_native_block)) {
									// we match the current compilation target so we should not output
									// this code
								}
								else
								{
									break;
								}
							}
							else if (s.startsWith("@code:") && s.equals("@code:" + m_native_block)) {
								// abort consuming the rest of this token it's meant as inline code
								break;
							}
							
							found_first_whitespace = true;
						}
					}
				
					s += reader.read();		
				}
				
			//	System.out.println("@escape: " + s);
				
				//tok.m_file		= this.m_file;
				tok.m_value		= s;
				tok.m_type		= LEXER_NATIVE_BLOCK;
				tok.m_whitespace = reader.getWhitespace();
				tok.m_line		= (short)line;
				//tok.m_column	= column;
				
				tok.m_position = position;
				tok.m_token_source = m_token_source;
				//tok.m_data     = data;
				
				return 1;
			}
			
			reader.eatWhitespace();
		}
		while(has_comment);
		
		c = reader.peek(0);
		
		int column 	= reader.m_curr_column;
		int line  	= reader.m_curr_line;
		int type 	= LEXER_INVALID;
		int position = reader.m_position;
		//char [] data = reader.m_data;
		
		String type_string = null;
		
		
		//int p0 = reader.peek(0);
		
		if (c == 0) {
			type = LEXER_EOF;
		}
		else
		{
			// follow the lexer tree
			{
				int cc = 0;
				IGLexerEntry entry = m_symbol_root;		
				for (;;)
				{
					int p = reader.peek(cc);
					
					if (p >= entry.m_next.length) {
						break;
					}
					
					IGLexerEntry next = entry.m_next[p];
			
					if (next == null) {
						break;
					}
			
					cc++;
					entry = next;
				}
		
				// check that a valid node was found
				if (entry.m_id != -1) 
				{
					type  		= entry.m_id;
					type_string = entry.m_string;
				
					if (type_string != null) 
					{
						reader.discard(cc);
					}
					else {
						type_string = reader.readMultipleAsString(cc);  
					}
				}
			}
		
			if (type == LEXER_HEX) {
				type_string = type_string.toLowerCase();
			}
			else if (type == LEXER_STRING) 
			{
				int cc = 0;
				while (true)
				{	
					c = reader.peek(cc);
					if (c == '"') {
						break;
					}
				
			
					// invalid following character
					// expecting to see a " terminating the string
					if (c == '\n' ||  '\0' == c) {
						type = LEXER_STRING_MULTILINE;
						break;
					}
			
					cc++;
			
					// read the escaped character
					// if we found a \
					if (c == '\\') 
					{
						c = reader.peek(cc);
			
						// invalid following character
						// expecting to see a " terminating the string
						if (c == '\n' ||  '\0' == c) {
							type = LEXER_STRING_MULTILINE;
							break;
						}
						else if (c != 't' && c != 'b'  && c != 'n'  && c != 'r' &&
								 c != 'f' && c != '\'' && c != '\"' && c != '\\' && c != 'u')
						{
							// TODO validate unicode escapes
						
							//System.err.println("Bad escape character: " + (char)c);
							type = LEXER_STRING_INVALID_ESCAPE;
							break;
						
						}
					
						cc++;
					}
				}	
			
				type_string = reader.readMultipleAsString(cc);  	
				reader.read();
			}
			else if (type == LEXER_CHAR) 
			{
				// determine the length of the string
				c =reader.peek(0);
			
				int cc = 0;
				
				int prev = c;
				if ('\'' != c && '\0' != c) 
				{	
					cc++;
					c = reader.peek(cc);
					if (c == '\n') {
						type = LEXER_CHAR_MULTILINE;
					}
				}	
			
				if (prev == '\\' && '\'' != c && '\0' != c) 
				{	
					cc++;
					c = reader.peek(cc);
					if (c == '\n') {
						type = LEXER_CHAR_MULTILINE;
					}
				}	
			
				if (c != '\'') {
					type = LEXER_CHAR_MULTILINE;
				}
			
				type_string = reader.readMultipleAsString(cc);  	
				reader.read();
			}
		}
		
		
		// replace after this point
		
		/*
		reader.peekMultiple(m_tmp_string, LEXER_MAX_SYMBOL_LENGTH + 1);
		
		// see if it matches a user defined symbol
		int i, j;
		for (i = 0; i < LEXER_MAX_USER_DEFINED_SYMBOLS; i++)
		{
			if (null == this.m_symbols[i]) {
				continue;
			}
			
			String symbol = this.m_symbols[i];
			
			//fprintf(stderr, "comparing %s to '%s'\n", symbol, string); 
			
			
			// this is super bad... create a SYMBOL type .. or do a prescan
			// for strings that could be misinterpreted with IDs
			int no_misinterpretation = ('a' <= symbol.charAt(0) && symbol.charAt(0) <= 'z') ? 0 : 1;
			
			
			int symbol_length = symbol.length();
			
			char next_char  = m_tmp_string[symbol_length];
			
			// TODO
			if (stringsMatch(symbol, m_tmp_string)
				
				&& 
				
				//1
				(!(('0' <= next_char && next_char <= '9') || 
				   ('a' <= next_char && next_char <= 'z') || 
				   ('A' <= next_char && next_char <= 'Z') ||
				   '_' == next_char) || (1 == no_misinterpretation))
				) 
			{
				type  = i;
				type_string = this.m_symbols[i];
				
				reader.discard(symbol.length());
				
				
				// get rid of those characters from the stream
				//for (j = symbol.length(); j > 0; j--) 
				//{
				//	reader.read();
				//}
				
				// we found our match
				break;
			}
		}
		
		
		// could not match against a user defined symbol
		// determine if its a INT, STRING or ID
		if (i == LEXER_MAX_USER_DEFINED_SYMBOLS)
		{
			
			c = m_tmp_string[0];
			
			int toss_out = 0;
			int character_count = 0;
			
			if ('0' == c)
			{
				// special case since in the future we may want to add support for octal  012345 or hexadecimal 0x324345AF
				character_count = 1;
				type = LEXER_INT;
				
				if ('.' == reader.peek(character_count))
				{
					character_count++;
					c = reader.peek(character_count);
					
					type = LEXER_FLOAT;
					while ('0' <= c && c <= '9') 
					{
						character_count++;
						c = reader.peek(character_count);
					}
				}
				// OKAY WOW it seems that the IGFileReader is doing a real bad job with invalid input
				else if ('x' == reader.peek(character_count) ||
						 'X' == reader.peek(character_count))
				{
					character_count++;
					c = reader.peek(character_count);
					
					type = LEXER_HEX;
					while (('0' <= c && c <= '9') || 
						   ('a' <= c && c <= 'f') || 
						   ('A' <= c && c <= 'F')) 
					{
						character_count++;
						c = reader.peek(character_count);
					}
				}
			}
			else if ('1' <= c && c <= '9') {
				// numeric
				
				type = LEXER_INT;
				
				//int allow_period = 0;
				
				character_count= 0;
				while ('0' <= c && c <= '9') {
					character_count++;
					c = reader.peek(character_count);
				}
				
				if ('.' == reader.peek(character_count))
				{
					character_count++;
					c = reader.peek(character_count);
					
					type = LEXER_FLOAT;
					while ('0' <= c && c <= '9') 
					{
						character_count++;
						c = reader.peek(character_count);
					}
				}
				
				
			}	
			/////////////////////////////////
			// LEXER_STRING
			/////////////////////////////////
			else if ('"' == c) {
				// start of a string
				// eat the first quote
				reader.read();
				
				character_count = 0;
				
				type = LEXER_STRING;
				
				// determine the length of the string
				//c =reader.peek(0);
				
				
				
				// check that it isn't an empty string
				//if (c != '"')
				//{
				// this handles all cases except for double slahses
				// this is meant to keep the string entirely intact
				
				while (true)
				{	
					c = reader.peek(character_count);
					if (c == '"') {
						break;
					}
					
				
					// invalid following character
					// expecting to see a " terminating the string
					if (c == '\n' ||  '\0' == c) {
						type = LEXER_STRING_MULTILINE;
						break;
					}
				
					character_count++;
				
					// read the escaped character
					// if we found a \
					if (c == '\\') 
					{
						
						c = reader.peek(character_count);
				
						// invalid following character
						// expecting to see a " terminating the string
						if (c == '\n' ||  '\0' == c) {
							type = LEXER_STRING_MULTILINE;
							break;
						}
						
						character_count++;
					}
				}	
				//}
				
				
				toss_out = 1;
			}
			else if ('\'' == c) {
				// start of a string
				// eat the first quote
				reader.read();
				
				type = LEXER_CHAR;
				character_count = 0;
				
				// determine the length of the string
				c =reader.peek(0);
				
				int prev = c;
				if ('\'' != c && '\0' != c) 
				{	
					character_count++;
					c = reader.peek(character_count);
					if (c == '\n') {
						type = LEXER_CHAR_MULTILINE;
					}
				}	
				
				if (prev == '\\' && '\'' != c && '\0' != c) 
				{	
					character_count++;
					c = reader.peek(character_count);
					if (c == '\n') {
						type = LEXER_CHAR_MULTILINE;
					}
				}	
				
				if (c != '\'') {
					type = LEXER_CHAR_MULTILINE;
				}
				
				toss_out = 1;
			}
			else if ('_' == c || ('a' <= c && c <= 'z') || ('A' <= c && c <= 'Z'))
			{
				// start of an id
				
				character_count = 0;
				while ('_' == c || ('a' <= c && c <= 'z') || ('A' <= c && c <= 'Z') || ('0' <= c && c <= '9'))
				{
					character_count++;
					c = reader.peek(character_count);
				}
				
				type = LEXER_ID;
			}
			else if ('\0' == c) {
				// found and end of file
				character_count = 0;
				type = LEXER_EOF;
			}
			else {
				// WOAH NOW
				assert false  : ("IGLexer.Unexpected character: " + c);
			}
			
			
			type_string = reader.readMultipleAsString(character_count);
			
			if (type == LEXER_HEX) {
				type_string = type_string.toLowerCase();
			}
			
			/////////
			// Remapping of symbols
			//////////
			
			if (type == LEXER_ID)
			{
				if (type_string.indexOf("__") != -1) {
					// TODO
					// throw an exception stating that variables may not have "__" 
					// or maybe this should just be a warning, as ig code will
					// use that pattern as an escape
				
				}
			

			}
			
			
			while (toss_out > 0) {
				reader.read();
				toss_out--;
			}
		}
		*/
		//if (type == LEXER_STRING) {
		//	System.out.println("string: " + type_string);
		//}
		
		tok.init(type, type_string, m_token_source, (short)line);
		tok.m_whitespace = reader.getWhitespace();
		tok.m_position = position;
//		tok.m_data     = data;
//		tok.m_token_source = m_token_source;
		reader.eatWhitespace();
		
		//System.out.println("read token: " + tok.m_value + " " + tok.m_type);
		return (tok.m_type == LEXER_EOF) ? 0 : 1;
	}
	
	public void close()
	{
	/*
		try
		{
			m_reader.close();
		}
		catch (Exception ex) {
			ex.printStackTrace();
		}
	*/		
	}
	

}

