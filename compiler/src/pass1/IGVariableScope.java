/*
Copyright (c) 2014-2017 Incubator Games Ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

package pass1;

import java.util.ArrayList;

public class IGVariableScope
{
	public IGVariableScope       m_parent = null;
	public ArrayList<IGVariable> m_children = new ArrayList<IGVariable>();


	public IGVariableScope(IGVariableScope parent) {
		m_parent = parent;
	}

	public boolean nameExists(String n) {
		for (int i = m_children.size() - 1; i >= 0; i--) {
			if (m_children.get(i).m_name.m_value.equals(n)) {
				return true;
			}
		}
		
		if (m_parent != null) {
			return m_parent.nameExists(n);
		}
		return false;
	}
	
	public void add(IGVariable v) {
		m_children.add(v);
	}
}