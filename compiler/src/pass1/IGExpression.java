/*
Copyright (c) 2014-2017 Incubator Games Ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

package pass1;

public class IGExpression
{
	public Type        m_type = null;		// available after promotion to an L or R value

	public IGNode      m_node   = null;
	public IGScopeItem m_lvalue = null;
	public IGScopeItem m_rvalue = null;
	
	
	public static final int RVALUE = 2;
	public static final int LVALUE = 1;
	
	public boolean m_resolved = false;
	public int     m_class    = 0;
	
	public IGExpression(Type t) {
		m_type = t;
	}
	
	public static IGExpression createRValue(Type t) 
	{
		IGExpression e = new IGExpression(t);
		e.m_class = RVALUE;	
		e.m_resolved = true;
		
		return e;
	}
	
	// unless its in a var statement
	// all values assigned to will be thought to have both setters/getters
	
	public IGExpression toRValue() 
	{
		if (m_resolved && (m_class & RVALUE) != RVALUE) {
			throw new Error("Not an rvalue");
		}
		
		m_resolved = true;
		m_class    = RVALUE;
		
		return this;
	}	
	
	/**
	 * For use in most assignments ie.   x = 5 or x += 5
	 * Since they can always be chained.  ie.  y = (x = 5) or y = (x += 5)
	 */
	
	public IGExpression toLRValue() {

		if (m_resolved && (m_class & (RVALUE | LVALUE)) != (RVALUE | LVALUE)) {
			throw new Error("Not an rlvalue");
		}
		
		m_resolved = true;
		m_class    = RVALUE | LVALUE;

		return this;
	}
	
	/**
	 * only for use in
	 * var x : int = 
	 * statements
	 */
	public IGExpression toLValue() 
	{
	
		if (m_resolved && (m_class & LVALUE) != LVALUE) {
			throw new Error("Not an lvalue");
		}
		
		m_resolved = true;
		m_class    = RVALUE;
	
		return this;
	}
}