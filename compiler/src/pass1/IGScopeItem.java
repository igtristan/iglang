/*
Copyright (c) 2014-2017 Incubator Games Ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

package pass1;

import java.util.Vector;

public  class IGScopeItem
{
	// IGPackage
	public static final int SCOPE_ITEM_PACKAGE 		= 0;
	
	// IGSource
	public static final int SCOPE_ITEM_CLASS   		= 0x10;
	public static final int SCOPE_ITEM_INTERFACE 	= 0x11;
	public static final int SCOPE_ITEM_ENUM			= 0x12;
	
	// IGMember
	public static final int SCOPE_ITEM_STATIC_FUNCTION 	= 0x20;
	public static final int SCOPE_ITEM_FUNCTION        	= 0x21;	
	public static final int SCOPE_ITEM_MEMBER_FUNCTION  = 0x21;
	
	public static final int SCOPE_ITEM_MEMBER_VARIABLE  = 0x22;
	public static final int SCOPE_ITEM_VARIABLE		 	= 0x22;
	public static final int SCOPE_ITEM_STATIC_VARIABLE  = 0x23;
	
	// Variable
	public static final int SCOPE_ITEM_LOCAL_VARIABLE        = 0x30;
	public static final int SCOPE_ITEM_PRIMITIVE             = 0x40;
	
	// Null
	
	public  int       getScopeType() { return SCOPE_ITEM_PRIMITIVE; };
	public  IGScopePath getScopePath() { return null;  }
	public String getScopePathAsString() { return getScopePath().toString(); }
	public  Type               getDataType(Type relative) { return null; }
	
	
	public static final IGScopeItem PRIMITIVE = new IGScopeItem();
	
	// used for disambiguating between function pointers and non function pointers
	public boolean    isScopeTypeVariable() {
		int st = getScopeType();
		return st == SCOPE_ITEM_VARIABLE || st == SCOPE_ITEM_STATIC_VARIABLE || st == SCOPE_ITEM_LOCAL_VARIABLE;
	}
	
	public String toString() {
		return super.toString() + "=> " + describeScopeType();
	}
	
	public String             describeScopeType()
	{
		int scope_type = getScopeType();
		
		if (scope_type == SCOPE_ITEM_PACKAGE)         return "package";
		if (scope_type == SCOPE_ITEM_CLASS)           return "class";
		if (scope_type == SCOPE_ITEM_INTERFACE)       return "interface";
		if (scope_type == SCOPE_ITEM_ENUM)            return "enum";
		if (scope_type == SCOPE_ITEM_STATIC_FUNCTION) return "static function";
		if (scope_type == SCOPE_ITEM_FUNCTION )       return "function";
		if (scope_type == SCOPE_ITEM_VARIABLE)        return "variable";
		if (scope_type == SCOPE_ITEM_STATIC_VARIABLE) return "static variable";
		if (scope_type == SCOPE_ITEM_PRIMITIVE)       return "primitive";
		
		return "unknown";
	}
	
	/*
	 * This should be overrided by a variable or an intermediate value
	 */
	
	public boolean isScopeCallable() 
	{
		int scope_type = getScopeType();
	
		if (scope_type != IGScopeItem.SCOPE_ITEM_STATIC_FUNCTION ||
			scope_type != IGScopeItem.SCOPE_ITEM_FUNCTION) {
			return false;	
		}
		else
		{
			return true;
		}
	}
	
	public IGScopeItem getScopeChild(String name, boolean setter_context) {
		return null;
	}
	
	public IGNode m_declaration_node;	// the node representing the AST of this variable declaration
}