/*
Copyright (c) 2014-2017 Incubator Games Ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

package pass1;

//import java.util.Vector;
import java.util.*;
/*

TODO	
	- todo check that signatures from overrides or interfaces match in terms of types and defaults
	- verify that import signatures are correct (this currently isn't the case)
	- verify that you can't call a static function on an object (DONE)
	- verify that you can't call a member function as if it was static (DONE)
	- verify that fully specified paths don't work (AS3 build has issues with this)
	- verify import paths,  use in combination with resolver to display something usefull

AS3 operator precedence for reference
http://help.adobe.com/en_US/ActionScript/3.0_ProgrammingAS3/WS5b3ccc516d4fbf351e63e3d118a9b90204-7fd1.html


1. Primary			[] {x:y} () f(x) new x.y x[y] <></> @ :: ..
2. Postfix			x++ x--
3. Unary			++x --x + - ~ ! delete typeof void
4. Multiplicative	* / %
5. Additive		+ -
6. Bitwise shift	#<< #>> #>>>
7. Relational		< > <= >= as in instanceof is
8. Equality		== != === !==
9. Bitwise AND		&
A. Bitwise XOR		^
B. Bitwise OR		|
C. Logical AND		&&
D. Logical OR		||
E. Conditional		?:
F. Assignment		= *= /= %= += -= <<= >>= >>>= &= ^= |=
G. Comma			,



*/

public final class IGValidator extends IGConsts
{

	public static final int TOKEN_UNARY       = 0x3;
	public static final int TOKEN_RELATIONAL  = 0x7;
	public static final int TOKEN_EQUALITY    = 0x8;
	public static final int TOKEN_ASSIGNMENT  = 0xF;
	public static final int TOKEN_OR	      = 0xD;	
	public static final int TOKEN_AND         = 0xC;   
	public static int isBinaryOrTernary(int op)
	{
		// 5 * 6 + 7
		//    *  (4)
		// 	 /  \
		// 5     \
		//         +  (5)
		//        /  \
		//      6      7
		// if op > super op
	
		// TODO tag unary varients
		
		switch (op)
		{
			// 4. Multiplicative	* / %
			case TOK_MUL: 
			case TOK_DIV:
			case TOK_MOD:
			return 0x4;
		
		// 5. Additive		+ -
			case TOK_ADD: 
			case TOK_SUB:
			return 0x5;
		
		// 6. Bitwise shift	<< >> >>>
			case TOK_LSL: 
			case TOK_ASR: 
			case TOK_LSR:
			return 0x6;
		
		// 7. Relational < > <= >= as in instanceof is
			case TOK_LE: 
			case TOK_GE: 
			case TOK_LT: 
			case TOK_GT:
			return TOKEN_RELATIONAL;
		
		// 8. Equality		== != === !==
			case TOK_EQ: 
			case TOK_NE:
			return TOKEN_EQUALITY;

		// 9. Bitwise AND		&
			case TOK_BIT_AND:
			return 0x9;
		
		// A. Bitwise XOR		^
			case TOK_BIT_XOR:
			return 0xA;
		
		// B. Bitwise OR		|
			case TOK_BIT_OR:
			return 0xB;
		
		// C. Logical AND		&&
			case TOK_AND:
			return TOKEN_AND;

		// D. Logical OR		||
			case TOK_OR:
			return TOKEN_OR;
			
		// E. Conditional		?:
			case TOK_QUESTIONMARK:
			return 0xE;

		// F. Assignment		= *= /= %= += -=  &= ^= |=    (<<= >>= >>>= NYI)
			case TOK_ASSIGN: 
			case TOK_ADD_ASSIGN: 
			case TOK_SUB_ASSIGN: 
			case TOK_MUL_ASSIGN: 
			case TOK_DIV_ASSIGN: 
			case TOK_MOD_ASSIGN: 
			case TOK_XOR_ASSIGN: 
			case TOK_OR_ASSIGN: 
			case TOK_AND_ASSIGN:
			return TOKEN_ASSIGNMENT;
		}
		
		return 0;
	}
	
	private static Token [] s_token_list = null;
	private static int           s_token_index = 0;
	private static int           s_token_index_end = 0;
	private static IGMember        s_member = null;
	private static IGResolver    s_resolver = null;
	private static IGSource      s_source = null;
	
	
	public static String describeAccess(IGMember member) 
	{
		if (member.hasModifier(IGMember.PUBLIC)) {
			return "public in " + member.m_parent_source.getScopePath().toString();
		}
		else if (member.hasModifier(IGMember.PRIVATE)) {
			return "private in " + member.m_parent_source.getScopePath().toString();
		}
		else if (member.hasModifier(IGMember.INTERNAL)) {
			return "internal in " + member.m_parent_source.getScopePath().toString();
		}
		else if (member.hasModifier(IGMember.PROTECTED)) {
			return "protected in " + member.m_parent_source.getScopePath().toString();
		}
		
		return "unknown";
	}
	// this is all relative to the current source file
	public static boolean canAccessScope(IGMember member) {
		if (member.hasModifier(IGMember.PUBLIC)) {
			return true;
		}
		
		if (member.m_parent_source == s_source) {
			return true;
		}
		
		if (member.hasModifier(IGMember.PROTECTED)) {
			if (s_source.inherits(member.m_parent_source)) {
				return true;
			}
		}
		
		if (member.hasModifier(IGMember.INTERNAL)) {
			// TODO unimplemented
			if (s_source.samePackage(member.m_parent_source)) {
				return true;		
			}
		}
		
		return false;
	}
	
	public static void setTokenList(Token [] list, int start, int end, IGMember m, IGResolver r, IGSource s)
	{
		s_token_list = list;
		s_token_index = start;
		s_token_index_end = end;
		s_member = m;
		s_resolver = r;
		s_source  =s;
	}
	
	
	public static void skipToAnnotation(IGSource s, IGMember m)
	{
		int new_idx = s_token_index;
		Token [] tokens = s_token_list;
		
		
		Token to_find =s.getTokenForAnnotationObject(m);
	
		s_token_index = to_find.m_token_index;
		
		/*
		
		while (new_idx != s_token_index_end)
		{
			if (tokens[new_idx] == to_find) { //.m_annotation_object == m) {
				break;
			}
			new_idx++;
		}
		s_token_index = new_idx;
		*/
	}
	
	public static int peekTokenType()
	{
		if (s_token_index == s_token_index_end) return TOK_EOF;
	
		return s_token_list[s_token_index].m_type;
	}
	
	public static int peekTokenType(int offset)
	{
		if (s_token_index + offset >= s_token_index_end) return TOK_EOF;
	
		return s_token_list[s_token_index + offset].m_type;
	}
	
	
	public static final Token read()
	{	
		if (s_token_index == s_token_index_end) return null;

		Token token = s_token_list[s_token_index];
		s_token_index ++;
		return token;
	}
	
	public static final Token peek()
	{	
		if (s_token_index == s_token_index_end) return null;

		Token token = s_token_list[s_token_index];
		return token;
	}
	
	public static final void softError(Token t, String text) {
		t.printError(text);
		s_source.m_failed_pass0 = true;
	}
	
	public static final void error(String text)
	{
		error(read(), text);
		
	}
	
	public static final void error(Token t, String text)
	{
		t.printError(text);
		throw new RuntimeException("Failure: " + text);
	}
	
	
	public static final Token dontExpect(int type, String text)
	{
		Token t = peek();
		if (t.m_type == type) {
			t.printError(text);
			throw new RuntimeException("Failure: " + text);
		}
		return t;
	}
	
	
	public static final Token expect(int type, String text)
	{
		Token t = read();
		if (t.m_type != type) {
			t.printError(text);
			throw new RuntimeException("Failure: " + text);
		}
		return t;
	}
	
	public static final Token expect(int type)
	{
		return expect(type, "<no-message>");
	}
	
	public static Type readTypeAllowVoid()
	{
		if (peekTokenType() == TOK_VOID) {
			expect(TOK_VOID);
			return Type.VOID;
		}
		
		return readType();
	}
	
	public static Type readType()
	{
		int peek_token_type = peekTokenType();
		

		// Handle repeated tupples
		///////////////////////////////////////
		if (peek_token_type == IGLexer.LEXER_INT) {
			// this is a tuple

			Token tupple_count = read();
			Type repeated_type = readType();


			int count = Token.getIntValue(tupple_count);
			ArrayList<Type> parameter_types = Type.allocTempVector();
			for (int i = 0; i < count; i++) {
				parameter_types.add(repeated_type);
			}


			return Type.get("tuple", Type.get(parameter_types), Type.VOID);	
		}

		// primitives
		/////////////////////
		
		if (peek_token_type == TOK_INT) {
			read();
			return Type.INT;
		}
		else if (peek_token_type == TOK_BOOLEAN) {
			read();
			return Type.BOOL;
		}
		else if (peek_token_type== TOK_UINT) 
		{
			read();
			return Type.UINT;
		}
		else if (peek_token_type == TOK_NUMBER) 
		{
			read();
			return Type.DOUBLE;
		}
		else if (peek_token_type == TOK_SHORT)
		{
			read();
			return Type.SHORT;
		}
		else if (peek_token_type == TOK_BYTE)
		{
			read();
			return Type.BYTE;
		}
		else if (peek_token_type == TOK_FLOAT) {
			read();
			return Type.FLOAT;
		}
		// function wrappers
		//////////////////////
		else if (peek_token_type == TOK_FUNCTION_TYPE ||
				 peek_token_type == TOK_STATIC_FUNCTION_TYPE)
		{
			int function_type = peek_token_type;
		
			expect(function_type);
			expect(TOK_LT, "Expecting function type definition");
			
			ArrayList<Type> parameter_types = Type.allocTempVector();
			expect(TOK_LBRACKET, "Expecting (");
			while (peekTokenType() != TOK_RBRACKET)
			{
				parameter_types.add(readType());
				if (peekTokenType() != TOK_RBRACKET) {
					expect(TOK_COMMA, "Expecting ,");
				}
			}
			
			expect(TOK_RBRACKET, "Expecting )");
			expect(TOK_COLON,  "Expecting :");
			
			Type right = readTypeAllowVoid();
			
			expect(TOK_GT, "Expecting end of function type definition");
			
			Type result = Type.get(function_type == TOK_FUNCTION_TYPE ? 
				"Function" : "StaticFunction", Type.get(parameter_types), right);	
				
			Type.freeTempVector(parameter_types);
				
			return result;
		}
		// standard objects
		//////////////////////
		else
		{
			Token class_type = expect(TOK_ID);
			if (peekTokenType() == TOK_LT) 
			{
				expect(TOK_LT, "Expecting template types");
				
				
				Type left = null;
				Type right = null;
				
				do
				{
					if (left == null) left = readType();
					else right = readType();
					
					if (peekTokenType() != TOK_GT) {
						expect(TOK_COMMA, "Expecting comma seperating templated arguments");
					}
				}
				while (peekTokenType() != TOK_GT);
			
			
				expect(TOK_GT, "Expecting template type terminator");	
				
				return Type.get(class_type.m_value, left, right);
			}
			else
			{
				return Type.get(class_type.m_value);
			}
		}
	}
	
	public static Type peekType()
	{
		Type type = null;
		int start = s_token_index;
		try
		{
			type = readType();	
		}
		catch (Exception ex) {
			return null;
		}
		
		s_token_index = start;
		return type;
	}
	

	/**
	 * From the context of the current file, function and line number
	 * evaluate
	 */

	private static IGScopeItem readScopeItem(boolean setter_context)
	{
		Token t = expect(TOK_ID, "Expecting id");
		return readScopeItem(setter_context, t);
	}
	
	
	private static IGScopeLookup s_lookup = new IGScopeLookup();
	
	private static IGScopeItem readScopeItem(boolean setter_context, Token t)
	{
		//Token t = expect(TOK_ID, "Expecting id");
		
		IGScopeLookup lookup = s_lookup;
		
		lookup.init(    setter_context ? IGScopeLookup.LVALUE : IGScopeLookup.RVALUE,
						s_member.hasModifier(IGMember.STATIC),
						s_token_index - 1,
						s_member);
		// TODO				
		
		String error_message = null;
		s_member.lookup(t.m_value, lookup);
		
		error_message = lookup.getError();
		if (error_message != null) {
			 error(t, error_message);
		}
		
		if (lookup.hasResults()) {
			return lookup.getResult();
		}
		
		// not found is this a class or package we've included
		return s_resolver.findScopeItem(t.m_value, -1);
	}
	
	
	public static int       s_node_stack_size = 0;
	public static IGNode [] s_node_stack = new IGNode[256];

	// Start of node stack manipulators
	//////////////////////////////////
	
	public static int getNodeStackSize() {
		return s_node_stack_size;	//s_node_stack.size();
	}
	
	public static void setNodeStackSize(int sz)
	{
		s_node_stack_size = sz;
		//while (s_node_stack.size() > sz) {
		//	s_node_stack.remove(s_node_stack.size() -1);
		//}
//		s_node_stack.removeRange(sz, s_node_stack.size());
//      previously used setSize
	
	}
	
	public static void nodeStackDebug() {
		for (int i = 0; i < s_node_stack_size; i++) {
			IGNode node = s_node_stack[i];
			System.err.println("" + i + "> " + node.m_node_type + " " + node.m_token.m_value + " " + node.m_start_index + " " + node.m_end_index);
			node.m_token.printError("tracing node stack");
		
		}
	
	}
	
	
	public static IGNode startMemberNode(IGMember m)
	{
		IGNode parent = (s_node_stack_size == 0) ? null : s_node_stack[s_node_stack_size-1];	//s_node_stack.size() == 0 ? null : s_node_stack.get(s_node_stack.size() - 1);
		
		Token following = peek();
		
		//if (following.m_node != null) error(following, "This token already has a node assigned");
		IGMemberNode node = new IGMemberNode(s_token_index, following, m);
		s_node_stack[s_node_stack_size] = node;
		s_node_stack_size ++;
		
		if (parent != null) {
			parent.addChild(node);
		}
		
		return node;
	}
	
	public static IGNode startNode()
	{
		//IGNode parent = s_node_stack.size() == 0 ? null : s_node_stack.get(s_node_stack.size() - 1);
		IGNode parent = (s_node_stack_size == 0) ? null : s_node_stack[s_node_stack_size-1];	//s_node_stack.size() == 0 ? null : s_node_stack.get(s_node_stack.size() - 1);
	
		Token following = peek();
		
		//if (following.m_node != null) error(following, "This token already has a node assigned");
		IGNode node = new IGNode(s_token_index, following);
		s_node_stack[s_node_stack_size] = node;
		s_node_stack_size ++;
		
		if (parent != null) {
			parent.addChild(node);
		}
		
		return node;
	}
	
	public static IGNode cloneNode()
	{
		IGNode parent = (s_node_stack_size == 0) ? null : s_node_stack[s_node_stack_size-1];
		//IGNode parent = s_node_stack.size() == 0 ? null :  s_node_stack.get(s_node_stack.size() - 1);
		
		//if (following.m_node != null) error(following, "This token already has a node assigned");
		IGNode node = new IGNode(parent);
		//s_node_stack.add(node);
		s_node_stack[s_node_stack_size] = node;
		s_node_stack_size ++;
		
		if (parent != null) {
			parent.addChild(node);
		}
		
		return node;
	
	}
	
	public static IGNode startNode(int start_index, Token following)
	{	
		IGNode parent = (s_node_stack_size == 0) ? null : s_node_stack[s_node_stack_size-1];
		//IGNode parent = s_node_stack.size() == 0 ? null : s_node_stack.get(s_node_stack.size() - 1);
		
		//if (following.m_node != null) error(following, "This token already has a node assigned");
		IGNode node = new IGNode(start_index, following);
		//s_node_stack.add(node);
		s_node_stack[s_node_stack_size] = node;
		s_node_stack_size ++;
		
		
		
		if (parent != null) {
			parent.addChild(node);
		}
		
		return node;	
	}
	
	public static void addNodeChild(IGNode node) {
		s_node_stack[s_node_stack_size-1].addChild(node);
		//s_node_stack.get(s_node_stack.size() - 1).addChild(node);
	}
	
	
	
	public static void setNodeType(short type) {
		setNodeType(type, null);
	}
	
	public static void setNodeType(short type, IGScopeItem si) {
		IGNode node = s_node_stack[s_node_stack_size-1];	//s_node_stack.get(s_node_stack.size() - 1);
		
		node.m_node_type = type;
		node.m_scope     = si;
	}
	
	public static IGNode endNode(Type t)
	{
		s_node_stack_size--;
		IGNode node = s_node_stack[s_node_stack_size];
		
		//IGNode node = s_node_stack.remove(s_node_stack.size() - 1);
		node.complete(s_token_index, t);
		//s_node_stack.remove(s_node_stack.size() - 1);
		
		return node;
	}
	
	public static IGNode endNode()
	{
		//IGNode node = s_node_stack.remove(s_node_stack.size() - 1);
		
		s_node_stack_size--;
		IGNode node = s_node_stack[s_node_stack_size];
		
		node.complete(s_token_index, Type.VOID);
		//s_node_stack.remove(s_node_stack.size() - 1);
		
		return node;
	}
	
	/////////////////////////////////////////
	// end of node stack manipulators
	
	public static IGExpression parseExpression()
	{
		return parseExpression(ParseExpression_MODE_DEFAULT);
	}
	
	
	/*
	 * Parse a function call expression.
	 * @param the signature of the function to be called, and verified against
	 * Generates a IGNode.NODE_PARAMETER_LIST node, typed with the return type of the function
	 */


	public static Type [] m_function_call_types = new Type[256];
	
	public static IGExpression parseFunctionCallExpression(Type function_type)
	{	
		if (function_type.isFunctionWrapper()) {
			function_type = function_type.m_right;
		}
	
		startNode();
		{
			Token left;
			{
				left = expect(TOK_LBRACKET, "Expecting ( preceeding argument list");
				
				Type parameter_list = function_type.m_left;
		
				assert function_type.m_left != null : "Missing parameter list";
		
				int minimum_parameters = parameter_list.getMinimumParameters();
				int expected_type_count = parameter_list.getMaximumParameters();
				
				int parameter_count = 0;
			
				for (int i = 0; true; i++)
				{
					
					if (peekTokenType() == TOK_EOF)
					{
						error(peek(), "Unexpected EOF");
					}
					
					if (peekTokenType() == TOK_RBRACKET) 
					{
						if (parameter_count >= minimum_parameters) 
						{
							break;
						}
				
						// the expected min count == the expected max count
						if (minimum_parameters == expected_type_count) {
							softError(peek(), "Too few arguments. " + minimum_parameters + " expected. Found: " + parameter_count + " \n" +  
											  "Signature: " + function_type.prettyFunctionSignature());
						}
						else
						{
							softError(peek(), "Too few arguments. " + minimum_parameters + ".." + expected_type_count + " expected. Found: " + parameter_count + "  \n" +  
											  "Signature: " + function_type.prettyFunctionSignature());
						}
						
						break;
					}
			
					if (i != 0) {
						expect(TOK_COMMA, "Expecting comma separating arguments to function");
					}
		
				
					// these will already be wrapped in their own nodes
					{
					
					
						Token param_start = null;

						
						Type  node_type               =  null;//expected_parameter_type;						
						

							startNode();
							{
								param_start = peek();

								//if (i == 0) {
								IGExpression parameter_expression = parseExpression(ParseExpression_MODE_DEFAULT);
								//}

								//System.out.println("parameter expression type: " + parameter_expression.m_type);
									

								int countem = 1;
								if (parameter_expression.m_type.isTuple()) 
								{
									//System.out.println("parameter expression type: " + parameter_expression.m_type);
									Type [] types = parameter_expression.m_type.getTupleTypes();
									countem = types.length;
									parameter_expression.m_type = types[0];
								}
								//	parameter_expression.m_type = types[0];
								//	iteration_count = types.length; 

								//}

								int parameter_start = parameter_count;
								while (countem-- > 0)
								{
									Type  expected_parameter_type = 
										(parameter_count< parameter_list.m_types.length) ? 
											parameter_list.m_types[parameter_count] : null;
							
									m_function_call_types[parameter_count ] = expected_parameter_type;
									if (null == m_function_call_types[parameter_count ]) {
										softError(param_start, "Too many parameters to function.");
										break;
									}

									parameter_count ++;
									if (expected_parameter_type != null)
									{
										Type parameter_type = parameter_expression.m_type;
										if (parameter_type.workRequiredToCastTo(expected_parameter_type, false) == Type.WORK_INCOMPATIBLE) 
										{
											//System.err.println("" + function_type);
											softError(param_start, "Can't assign a value of that type to parameter " + (parameter_count+1) + ".\n" +  
													  "Expecting: " + expected_parameter_type.toPrettyString() + ". Found: " + parameter_type.toPrettyString());
										}

										node_type = expected_parameter_type;
									}
									else 
									{
										// when its an unexpected parameter just use the type of the parameter
										Type parameter_type = parameter_expression.m_type;
										node_type           = parameter_type;
									}
								}
								if (parameter_count != parameter_start + 1) {
									node_type = Type.get("tuple", Type.get(m_function_call_types, parameter_start, parameter_count), Type.VOID);
								}
								
								setNodeType(IGNode.PARAMETER_CAST);
							}
							endNode(node_type);

							// error on the first observed excess parameter
							if (parameter_count == expected_type_count + 1) 
							{
								if (minimum_parameters == expected_type_count) {
									softError(param_start, "Too many arguments to function. " + minimum_parameters + " expected.\n" +  
												  "Signature: " + function_type);
								}
								else
								{
									softError(param_start, "Too many arguments to function. " + minimum_parameters + ".." + expected_type_count + " expected.\n" +  
												  "Signature: " + function_type);
								}	
							}
						
					}
				}
		
				/*
				// hrm... need to better diagnose invalid calls
				if (peekTokenType() == TOK_COMMA) {
				
					if (minimum_parameters == expected_type_count) {
						error(peek(), "Too many parameters. " + minimum_parameters + " expected. Signature: " + function_type);
					}
					else
					{
						error(peek(), "Too many parameters. " + minimum_parameters + ".." + expected_type_count + " expected. Signature: " + function_type);
					}
				}
				*/
		
				Token right = expect(TOK_RBRACKET, "Expecting ) following argument list");
				//right.m_type = TOK_RFUNCTION;
			}
		
			if (function_type.m_right == null) {
				error(left, "Function has invalid return type: " + function_type);
			}

			setNodeType(IGNode.NODE_PARAMETER_LIST);
		}
		endNode(function_type.m_right);
		
		return IGExpression.createRValue(function_type.m_right);
	}
	
	private static final int ParseExpression_MODE_DEFAULT					= 100;	
	private static final int ParseExpression_MODE_NORMAL					= 100;
	private static final int ParseExpression_VARIABLE_DECLARATION			= 101;
	private static final int ParseExpression_MEMBER_VARIABLE_DECLARATION 	= 102;
	
	
	/*
	 * parse statements in the form:
	 * ID ':' TYPE  ( '=' EXPRESSION ) 
	 * This is used for:
	 *  - local variable declarations
	 *  - parameters to functions
	 * @param - root_node (can be null) the node to use as the scope declaration root
	 */
	
	private static Type parseVarExpression(int mode) {
		return parseVarExpression(mode, null);
	}
	
	
	// grammer:
	//
	// NODE_TYPE_LOCAL_VARIABLE_DEFINITION = 'var' ASSIGN
	//                                     = 'var' ID_TYPE_PAIR
	// ASSIGN                              = ID_TYPE_PAIR '=' EXPRESSION
	// ID_TYPE_PAIR                        = NODE_ID ':' NODE_TYPE_TYPE	
	
	private static Type parseVarExpression(int mode, IGNode root_node)
	{
		boolean allow_id_type = mode == ParseExpression_VARIABLE_DECLARATION ||
								mode == ParseExpression_MEMBER_VARIABLE_DECLARATION;
	
		
	
		int start_index = s_token_index;
	
		int peek_type0 =  peekTokenType();
	
	

		IGNode      left_node = null;
		Type        type = null;
		
		
		Token id   = peek();
	
		startNode();
		//{
			// resolve what this ID pertains to
			IGScopeItem scope_item = readScopeItem(true);		// its a LVALUE (aka setter)
		
			// all signs say this is a cast
			if (scope_item == null) 
			{
				error(id, "Unknown token.  Expecting ID : Type");
			}

			int scope_type = scope_item.getScopeType();


			if (mode == ParseExpression_VARIABLE_DECLARATION)
			{
				if (scope_type != IGScopeItem.SCOPE_ITEM_LOCAL_VARIABLE) {
					error(id, "Expecting variable name following var keyword");
				}
		
				setNodeType(IGNode.NODE_TYPE_LOCAL_VARIABLE_NAME, scope_item);
			}
			else if (mode == ParseExpression_MEMBER_VARIABLE_DECLARATION)
			{
				if (scope_type != IGScopeItem.SCOPE_ITEM_STATIC_VARIABLE &&
					scope_type != IGScopeItem.SCOPE_ITEM_MEMBER_VARIABLE) 
				{
					error(id, "Expecting static or non static member variable.");
				}
		
				setNodeType(scope_type != IGScopeItem.SCOPE_ITEM_MEMBER_VARIABLE 
						? IGNode.NODE_TYPE_STATIC_VARIABLE_NAME :
						  IGNode.NODE_TYPE_MEMBER_VARIABLE_NAME, scope_item);
			
			}
			else
			{
				error(id, "Unexpected mode all together");
			}
			
			
			left_node = endNode(scope_item.getDataType(null));
		
	
			Token colon = expect(TOK_COLON);
			
			IGNode node_type_id_type_pair = startNode(start_index, colon);			
				addNodeChild(left_node);		
				startNode();
				{
					type = fullyQualify(s_resolver, readType(), id);
					setNodeType(IGNode.NODE_TYPE_TYPE);
				}
				endNode(type);
			setNodeType(IGNode.ID_TYPE_PAIR, scope_item);	
			

		//}	
		left_node = endNode(type);
		
		
		
		
		type = scope_item.getDataType(null);
		
		// User has specified a default value
		// var x : int = 5;
		if (peekTokenType() == TOK_ASSIGN)  
		{
			Type default_type = null;
			
			IGNode node_assign = startNode(start_index, peek());
			{
				Token tok_assign = expect(TOK_ASSIGN, "Expecting a default value for a parameter/ variable");
				setNodeType(IGNode.NODE_BINARY);
				addNodeChild(left_node);
			
				IGExpression assign_expression = parseExpression(ParseExpression_MODE_DEFAULT).toRValue();
				default_type = assign_expression.m_type;
				
				// can I just collapse all this additional logic that follows?
				if (default_type != type) 
				{	
					Type left = type;
					Type right = default_type;
					
					if (right.workRequiredToCastTo(left, false) == Type.WORK_INCOMPATIBLE) {
						error(tok_assign, "Expression doesn't match field being assigned to: " +
								 left.toPrettyString() + " <= " + right.toPrettyString());
					}
				}
			}
			endNode(type);
			
			
			
			// before I was returning the type of the default parameter. 
			// But really in the end that didn't seem to matter
			// Because nothing was ever checking the return value
			//return default_type;
			
			if (root_node == null)  {
				root_node = node_assign;
			}
		
		}
		// User has not specified a default value
		// var x : int;
		else {
			
			if (root_node == null)  {
				root_node = node_type_id_type_pair;
			}
		}
		
		scope_item.m_declaration_node = root_node;
		
		return type;
	}
	
	
	public static void validateDotAccessibility(Type previous_type, Token id, IGScopeItem scope_new)
	{
		int scope_type =  scope_new.getScopeType();
		if (previous_type.isNamespace())
		{
			if (scope_type == IGScopeItem.SCOPE_ITEM_MEMBER_FUNCTION) 
			{
				softError(id, "Attempting to access a member function from a static context");		
			}
			else if (scope_type == IGScopeItem.SCOPE_ITEM_MEMBER_VARIABLE) 
			{
				softError(id, "Attempting to access a member variable from a static context");		
			}
			else if (scope_type == IGScopeItem.SCOPE_ITEM_STATIC_FUNCTION)
			{
				if (!canAccessScope((IGMember)scope_new)) {
					softError(id, "Can't access function due to it being marked: " + describeAccess((IGMember)scope_new));
				}
			}
			else if (scope_type == IGScopeItem.SCOPE_ITEM_STATIC_VARIABLE)
			{
				if (!canAccessScope((IGMember)scope_new)) {
					softError(id, "Can't access static variable due to it being marked: " + describeAccess((IGMember)scope_new));
				}
			}
		}
		else if (!previous_type.isNamespace())
		{
			if (scope_type == IGScopeItem.SCOPE_ITEM_STATIC_FUNCTION)
			{
				softError(id, "Attempting to access a static function from an object context");							
			}
			else if (scope_type == IGScopeItem.SCOPE_ITEM_STATIC_VARIABLE)
			{
				softError(id, "Attempting to access a static variable from an object context");		
			}		
			else if (scope_type == IGScopeItem.SCOPE_ITEM_MEMBER_FUNCTION)
			{
				if (!canAccessScope((IGMember)scope_new)) {
					softError(id, "Can't access function due to it being marked: " + describeAccess((IGMember)scope_new));
				}
			}
			else if (scope_type == IGScopeItem.SCOPE_ITEM_MEMBER_VARIABLE)
			{
				if (!canAccessScope((IGMember)scope_new)) {
					softError(id, "Can't access  variable due to it being marked: " + describeAccess((IGMember)scope_new));
				}
			}			
		}
	}
	
	
	public static IGExpression parseExpression(int mode )
	{
		boolean allow_id_type = mode == ParseExpression_VARIABLE_DECLARATION ||
								mode == ParseExpression_MEMBER_VARIABLE_DECLARATION;
	
		int start_index = s_token_index;
	
		int peek_type0 =  peekTokenType();
	
		IGExpression left_expression = null;
		IGNode       left_node = null;
		Type         type = null;
		
		// bracketed expressions
		// for coping with parameters to functions and their defaults
		// also for coping with variable declaraction
		//
		// ParseExpression_VARIABLE_DECLARATION
		// (x : int, y : int = 7)
		//
		// ParseExpression_VARIABLE_DECLARATION
		// var x : int;    /// OR
		// var y : int = EXPRESSION
		
		if (allow_id_type)
		{
			throw new RuntimeException("Just call parseVarExpression on its own");
			//return parseVarExpression(mode);
		}
		///////////////////////////
		// BRACKETED EXPRESSION
		//////////////////////////
		else if (peek_type0 == TOK_LBRACKET)
		{
			IGNode start_expression = startNode();
			{
				expect(TOK_LBRACKET, "Expecting (");

				IGExpression bracket_expression = parseExpression();


				////////////////////////////////////////////
				// Promotion to a tuple
				////////////////////////////////////////////
				if (peekTokenType() != TOK_RBRACKET) {
				
					ArrayList<Type> types = new ArrayList<Type>();
					types.add(bracket_expression.m_type);

					while (peekTokenType() != TOK_RBRACKET) {
						expect(TOK_COMMA, "Expecting comma separating types");

						bracket_expression = parseExpression();
						types.add(bracket_expression.m_type);

					}
					expect(TOK_RBRACKET, "Expecting )");
				

					type = Type.get("tuple", Type.get(types), Type.VOID);
					//System.out.println("tuple created: " + type);


					// 
					setNodeType(IGNode.NODE_TYPE_BRACKETED_EXPRESSION, null);
					
				}
				else
				{
					type = bracket_expression.m_type;
					expect(TOK_RBRACKET, "Expecting )");
				
					// basically we're inheriting everything about this object
					// in this case we don't want to force the expression to be an RVALUE on exit
					IGNode child = start_expression.getChild(0);		
					start_expression.m_value_type = child.m_value_type;

					setNodeType(IGNode.NODE_TYPE_BRACKETED_EXPRESSION, child.m_scope);
					left_expression = bracket_expression;
				}
			}	
			left_node = endNode(type);
		}
		/////////////////////
		// Unary operators
		////////////////////
		else if (peek_type0 == TOK_SUB ||
				 peek_type0 == TOK_ADD ||
				 peek_type0 == TOK_NOT ||
				 peek_type0 == TOK_BIT_NOT) 
		{
			startNode();
			{
				Token        unary_token      = read();	
				IGExpression unary_expression = parseExpression(TOKEN_UNARY);
				type = unary_expression.m_type;
				
				if (peek_type0 == TOK_BIT_NOT && (type != Type.INT && type != Type.UINT )) {
				 	softError(unary_token, "Can only bitwise negate ints");
				}

				if (peek_type0 == TOK_ADD && (type != Type.INT && type != Type.UINT && type != Type.DOUBLE )) {
				 	softError(unary_token, "Can only effect ints and doubles with a unary +");
				}

				if (peek_type0 == TOK_SUB && (type != Type.INT && type != Type.UINT && type != Type.DOUBLE )) {
				 	softError(unary_token, "Can only effect ints and doubles with negation -");
				}
				
				if (peek_type0 == TOK_NOT && type != Type.BOOL) {
					softError(unary_token, "Can only logically negate bools");
				}
				
				setNodeType(IGNode.NODE_UNARY);
				
				left_expression = unary_expression;
			}
			left_node = endNode(type);			// added this
		}
		else if (peek_type0 == TOK_ANON_FN) {
			throw new RuntimeException("TOK_ANON_FN not flushed out");
		
		}
		// resolve between casting and not casting
		else if (peek_type0 == TOK_INC ||
		         peek_type0 == TOK_DEC) {
		 	  
		 	startNode();
		 	{
			 	Token        inc_token      = read();
			 	IGExpression inc_expression = parseExpression().toLRValue();
			 	type = inc_expression.m_type;
			 	
			 	if (!type.isArithmetic()) {
			 		if (peek_type0 == TOK_INC) {
				 		softError(inc_token, "Can only increment ints or doubles"); 
				 	}
				 	else {
				 		softError(inc_token, "Can only decrement ints or doubles"); 
				 	}
			 	}
			 	
			 	setNodeType(IGNode.INCREMENT);
			}
			endNode(type);
			
			// not allowed to do anything with this value
		 	return IGExpression.createRValue(Type.VOID);     
		}
		else if (peek_type0 == TOK_INT     ||
				 peek_type0 == TOK_UINT    ||
				 peek_type0 == TOK_BOOLEAN ||
				 peek_type0 == TOK_NUMBER) 
		{
			// cast an expression to an int, uint, boolean or number
			// int(expression)
		
			startNode();
			{
				Token cast_start = peek();
				Type dst_type, src_type;
				
				startNode();
				{
					dst_type = readType();
					setNodeType(IGNode.NODE_TYPE_TYPE);
				}
				endNode(dst_type);
							
			
				IGNode start_expression = startNode();
				{
					expect(TOK_LBRACKET, "Expecting ( for cast");
					IGExpression cast_expression = parseExpression().toRValue();
					src_type = cast_expression.m_type;
					expect(TOK_RBRACKET, "Expecting ) for cast");
					setNodeType(IGNode.NODE_TYPE_BRACKETED_EXPRESSION, start_expression.getChild(0).m_scope);
				}
				endNode(src_type);
				
				
				// validate that the explicit cast is possible
				int work = src_type.workRequiredToCastTo(dst_type, true);
				if (work == Type.WORK_INCOMPATIBLE) {
					softError(cast_start, "Impossible cast");
				}
				
				type = dst_type;
				
				setNodeType(IGNode.NODE_TYPE_CAST, null);
			}
			left_node = endNode(type);
			
			left_expression = IGExpression.createRValue(type);
		}
		else if (peek_type0 == TOK_NULL)
		{
			IGNode node = startNode();
			{
				read();
				type = Type.NULL;
				
				node.m_value_range = IGValueRange.createNull();
				setNodeType(IGNode.NODE_VALUE, null);
			}
			left_node = endNode(type);

			left_expression = IGExpression.createRValue(Type.NULL);
		}
		//////////////////
		// Establish the point of reference for a scope
		// all further "dot"s will be in relation to this initial id
		else if (peek_type0 == TOK_ID)
		{
			Token start_id = peek();
			
			IGNode id_node = startNode();
			{
				// this'd be where we'd want to start eating each of the
				// tokens for a forced cast situtation ??
			
				Token id   = peek();
				int peek_token_type = peekTokenType(1);
				Token scope_token = read();
				
				
				final boolean is_compound_assignment = peek_token_type == TOK_ADD_ASSIGN ||
													   peek_token_type == TOK_SUB_ASSIGN ||
													   peek_token_type == TOK_MUL_ASSIGN ||
													   peek_token_type == TOK_DIV_ASSIGN ||
													   peek_token_type == TOK_MOD_ASSIGN || 
														peek_token_type == TOK_XOR_ASSIGN || 
														peek_token_type == TOK_OR_ASSIGN || 
														peek_token_type == TOK_AND_ASSIGN;
														
				final boolean possible_setter = (peek_token_type == TOK_ASSIGN) || is_compound_assignment;
				
				
				
				//int         scope_item_start = getTokenIndex();
				
				// new logic
				left_expression = new IGExpression(null);
				left_expression.m_lvalue = readScopeItem(true, scope_token);
				left_expression.m_rvalue = readScopeItem(false, scope_token);
				left_expression.m_node   = id_node;
				
				// old logic
				IGScopeItem scope_item = readScopeItem(possible_setter, scope_token);
				
	
				if (scope_item == null) 
				{
					if (possible_setter) {
						IGScopeItem get_scope_item = readScopeItem(false, id);
						if (null != get_scope_item) {
							error(id, "Found getter but not setter for: " + id.m_value);
						}
						else {
							error(id, "Unknown symbol: " + id.m_value);						
						}
					}
					else
					{
						error(id, "Unknown symbol: " + id.m_value);
					}
				}
				
				int scope_type = scope_item.getScopeType();
				
				
				
				if (is_compound_assignment && 
					(scope_type == IGScopeItem.SCOPE_ITEM_FUNCTION || 
					 scope_type == IGScopeItem.SCOPE_ITEM_STATIC_FUNCTION))
				{
					IGScopeItem get_scope_item = readScopeItem(false, id);
					if (null == get_scope_item) {
						softError(id, "Found setter but not getter for: " + id.m_value);
					}
				}
	
			
				boolean can_cast = false;
				boolean is_static = false;
				boolean scope_chain_start = false;
				
				switch (scope_type)
				{
					// IGPackage
					case IGScopeItem.SCOPE_ITEM_PACKAGE: 
						scope_chain_start = true;
						break;

					// IGSource
					case IGScopeItem.SCOPE_ITEM_CLASS: 					
						can_cast = true; 
						scope_chain_start = true;
						break;
					case IGScopeItem.SCOPE_ITEM_INTERFACE: 				
						can_cast = true; 	
						scope_chain_start = true;
						break;
					case IGScopeItem.SCOPE_ITEM_ENUM: 					
						can_cast = true;	
						scope_chain_start = true;
						break;

					// IGMember
					case IGScopeItem.SCOPE_ITEM_STATIC_FUNCTION: 		
						if (!canAccessScope((IGMember)scope_item)) {
							error(start_id, "Cannot access static function. It's marked as: " + describeAccess((IGMember)scope_item));
						}
						is_static = true; 
						break;
					case IGScopeItem.SCOPE_ITEM_STATIC_VARIABLE:  		
						if (!canAccessScope((IGMember)scope_item)) {
							error(start_id, "Cannot access static variable. It's marked as: " + describeAccess((IGMember)scope_item));
						}		
						is_static = true; 
						break;
					case IGScopeItem.SCOPE_ITEM_FUNCTION:  		
						if (!canAccessScope((IGMember)scope_item)) {
							error(start_id, "Cannot access member function. It's marked as: " + describeAccess((IGMember)scope_item));
						}				
						break;
					case IGScopeItem.SCOPE_ITEM_VARIABLE: 		
						if (!canAccessScope((IGMember)scope_item)) {
							error(start_id, "Cannot access member variable. It's marked as: " + describeAccess((IGMember)scope_item));
						} 				
						break;

					// IGVariable
					case IGScopeItem.SCOPE_ITEM_LOCAL_VARIABLE: 		break;
						
					default:
						error(id, "Unknown scope item type");
						break;
				}
				
				Type cast_dst_type = null;
			
			
				///////////////////////////////////
				// Is it a type that can be cast to?
				// - ie does the format  Vector<String>(some_pointer)  make sense
				//////////////////////////////////////
				if (can_cast)
				{
					IGSource scope_source = ((IGSource)scope_item);
				
					///////////////////////////////////
					// handle templated types
					///////////////////////////////////
				
					// retrieve the type we'd expect an instance of this class to have
					cast_dst_type = ((IGSource)scope_item).getInstanceDataType();
				
					// if it is a templated class (Vector,Map,Array) resolve the templated parameters
					int tc = scope_source.m_template_parameters.size();
					if (tc > 0) 
					{
						expect(TOK_LT, "Expecting templated parameter list");
						Type left  = s_resolver.makeAbsoluteType(readType());
						Type right = null;
						if (tc > 1) {
							expect(TOK_COMMA, "Expecting second template parameter");
							right = s_resolver.makeAbsoluteType(readType());
						}
				
						cast_dst_type = Type.get(cast_dst_type.m_primary, left, right);
				
						expect(TOK_GT, "Expecting end of templated parameter list");
					}	
				}
			
				// If a '(' follows then we consider it a cast
				// attempt to cast to a particular type
				/////////////////////////////////////////////
				if (peekTokenType() == TOK_LBRACKET && can_cast)
				{
					// wrap the previous tokens in a cast
					cloneNode();
						setNodeType(IGNode.NODE_TYPE_TYPE);
					endNode(cast_dst_type);
				
					setNodeType(IGNode.NODE_TYPE_CAST, scope_item);
				
				
					if (scope_type != IGScopeItem.SCOPE_ITEM_CLASS      &&
						scope_type != IGScopeItem.SCOPE_ITEM_INTERFACE  &&
						scope_type != IGScopeItem.SCOPE_ITEM_ENUM) {
						
						error(start_id, "You can only cast an object to a type of class or interface");	
					}
				
					// this is effectively a bracketed expression
					startNode();
						expect(TOK_LBRACKET, "Expecting ( for cast");
						IGExpression cast_expression = parseExpression().toRValue();
						Type cast_src_type =  cast_expression.m_type;
						expect(TOK_RBRACKET, "Expecting ) for cast");	
						
						setNodeType(IGNode.NODE_TYPE_BRACKETED_EXPRESSION, null);				
					endNode(cast_src_type);
					
					
					// validate that the explicit cast is possible
					int work = cast_src_type.workRequiredToCastTo(cast_dst_type, true);
					if (work == Type.WORK_INCOMPATIBLE) {
						softError(start_id, "Impossible cast");
					}

					type = cast_dst_type;
					
				}
				// push a given scope onto the stack
				else
				{
				
					boolean type_overridden = false;
				
					if (scope_type == IGScopeItem.SCOPE_ITEM_STATIC_FUNCTION || 
						scope_type == IGScopeItem.SCOPE_ITEM_FUNCTION) {
					
						IGMember member = (IGMember) scope_item;
						if (member.hasModifier(IGMember.GET) ||
							member.hasModifier(IGMember.SET)) 
						{
							if (member.hasModifier(IGMember.GET)) 
							{
								type = scope_item.getDataType(null).unwrap().m_right;	
							}
							else {
								type = scope_item.getDataType(null).unwrap().m_left.m_types[0];	
							}
							
							if (scope_type == IGScopeItem.SCOPE_ITEM_STATIC_FUNCTION) {
								setNodeType(IGNode.STATIC_LOCAL_ACCESSOR, scope_item);
							}
							else {
								setNodeType(IGNode.LOCAL_ACCESSOR, scope_item);
							}
							type_overridden = true;
						}
						else
						{
							type = scope_item.getDataType(null);
						}
					}
					else {
						type = scope_item.getDataType(null);
					}
					
					
					// if its not an accessor apply one of the following types
					if (!type_overridden) 
					{
						if (scope_chain_start)
						{
							setNodeType(IGNode.SCOPE_CHAIN_START, scope_item);
						}
						else if (scope_type == IGScopeItem.SCOPE_ITEM_LOCAL_VARIABLE) 
						{
							setNodeType(IGNode.LOCAL_VARIABLE_ACCESS, scope_item);
						}
						else if (is_static) 
						{
							setNodeType(IGNode.STATIC_FIELD_ACCESS, scope_item);
						}
						else 
						{
							if (s_member.hasModifier(IGMember.STATIC)) {
								softError(id, "Can't access function or variable from a static context");
							}
						
							if (scope_item == null) {
								id.printError("How does this not have a scope item associated?");
							}
							setNodeType(IGNode.FIELD_ACCESS, scope_item);
						}
					}
					
				}
			}
			
			left_node = endNode(type);
		}
		else if (peek_type0 == IGLexer.LEXER_INT ||
				 peek_type0 == IGLexer.LEXER_HEX) 
		{
			IGNode node = startNode();
			{
				type = Type.INT;
				Token  int_value = read();
				
				if (peek_type0 == IGLexer.LEXER_INT) 
				{
					if (int_value.m_value.length() > "4294967295".length()) 
					{
						softError(int_value, "32 bit integer value must be between 0 and 4294967295");
					}
					else {
						long long_value = Long.parseLong(int_value.m_value);
						if (long_value < 0 || 
							long_value > 4294967295L)
						{
							softError(int_value, "32 bit integer value must be between 0 and 4294967295");
						}
					}
				}
				else if (peek_type0 == IGLexer.LEXER_HEX) {
					if (int_value.m_value.length() > "0xff00ff00".length()) {
						softError(int_value, "Hex value must be between 0x00000000 and 0xffffffff");
					}
				}
				
				node.m_value_range = IGValueRange.createInt(peek_type0, int_value.m_value);
				setNodeType(IGNode.NODE_VALUE, null);	
			}			
			left_node = endNode(type);
		}
		else if (peek_type0 == IGLexer.LEXER_CHAR) {
			IGNode node = startNode();
				type = Type.INT;
				Token char_value = read();
				
				node.m_value_range = IGValueRange.createInt(peek_type0, char_value.m_value);
				setNodeType(IGNode.NODE_VALUE, null);				
			left_node = endNode(type);
		}
		else if (peek_type0 == IGLexer.LEXER_FLOAT) {
			IGNode node = startNode();
				type = Type.DOUBLE;
				Token double_value = read();
				
				node.m_value_range = IGValueRange.createDouble(peek_type0, double_value.m_value);
				setNodeType(IGNode.NODE_VALUE, null);				
			left_node = endNode(type);
		}
		else if (peek_type0 == IGLexer.LEXER_STRING) {
			startNode();
				type = Type.STRING;
				read();
				setNodeType(IGNode.NODE_VALUE, null);				
			left_node = endNode(type);
		}
		else if (peek_type0 == TOK_TRUE ||
				 peek_type0 == TOK_FALSE) 
		{
			IGNode node = startNode();
			{
				type = Type.BOOL;
				Token token = read();
				node.m_value_range = IGValueRange.createBool(token.m_value);
				setNodeType(IGNode.NODE_VALUE, null);				
			}
			left_node = endNode(type);
		}
		//////////////////
		// new Object(func call)
		// new Object<int, barf>(func call)
		else if (peek_type0 == TOK_NEW) 
		{
		
			int new_start_index = s_token_index;
			
			// START NEW
			startNode();
			{
				Token n = read(); // <- read the 'new' token		//expect(TOK_NEW);
			
				IGScopeItem scope_item = null;
				
				// <child0> The Type used for the NEW operator
				startNode();
				{
					Token id = peek();
					scope_item = readScopeItem(false);
					
					// all signs say this is a cast
					if (scope_item == null) 
					{
						error(id, "Can't find type with name");
					}
					else if (scope_item.getScopeType() != IGScopeItem.SCOPE_ITEM_CLASS)
					{
						error(id, "Couldn't find named class. Found a " + scope_item.describeScopeType() + " with the same name");
					}
		
					IGSource scope_source = ((IGSource)scope_item);
		
					{
						IGMember con = scope_source.getConstructor();
					
						if (con != null)
						{
							if (!canAccessScope((IGMember)con)) {
								softError(n, "You can't contruct this object from the current scope");
							}
						}
					}
		
		
					// this isn't really correct
					type = ((IGSource)scope_item).getInstanceDataType();
				
					///////////////////////////////////
					// handle templated types
					// 
					///////////////////////////////////
				
					int tc = scope_source.m_template_parameters.size();
			
					if (tc > 0) 
					{
						expect(TOK_LT, "Expecting templated parameter list");
						Type left  = s_resolver.makeAbsoluteType(readType());
						Type right = null;
						if (tc > 1) {
							expect(TOK_COMMA, "Expecting second template parameter");
							right = s_resolver.makeAbsoluteType(readType());
						}
				
						// this was a custom templating.. so we couldn't guarantee that
						// the scope was correctly set on the type at this point
						type = Type.get(type.m_primary, left, right);
						type.m_scope = scope_item;
						
						expect(TOK_GT, "Expecting end of templated parameter list");
					}
					else
					{
						
					}
					
					setNodeType(IGNode.NODE_TYPE_TYPE, scope_item);
				}
				endNode(type);
			
				IGMember constructor = ((IGSource)scope_item).getFirstConstructor();
			
				// <child1> The constructor call for the new operator
				startNode();
				{
					
					
					Type constructor_type = (null == constructor) ? null :  constructor.getDataType(type);	//.unwrap();
					
					//System.out.println("constructor-type: " + constructor_type + " pre-type: " + type);
					//System.out.println("\tconstructor-type-scope: " + constructor_type.m_scope);
					
					//Type constructor_type = (null == constructor) ? null : constructor.getDataType(null);	
				
					if (null == constructor)
					{		
						error("Null constructor.. what gives?");	
						//expect(TOK_LBRACKET, "Expecting start of constructor call parameters");
						//expect(TOK_RBRACKET, "Expecting end of constructor call");
					}
					
					//else
					//{
					
					if (peekTokenType() != TOK_LBRACKET) {
						expect(TOK_LBRACKET, "Expecting start of constructor call parameters");
					}
						
					
					parseFunctionCallExpression(constructor_type);
					
					setNodeType(IGNode.NEW_CALL);	
				}
				endNode(type);
				
				//System.out.println("new: " + type);
				
				setNodeType(IGNode.NODE_NEW);
			}
			left_node = endNode(type);
			// END NEW
			
			
			// start INITIALIZER list
			// [ value, value, ... value] or
			// [ key : value, key : value, ... key : value]
			if (peekTokenType() == TOK_LARRAY)
			{
				short group_node_type = -200;
			
				//IGNode.INITIALIZER (2 children, the new and the list)
				startNode(new_start_index, peek());
				{
					// (child 1) add the NEW op as the first child
					addNodeChild(left_node);
					
					
					
					//(child 2) IGNode.INITIALIZER_LIST
					startNode();
					{
						Token ini_list_start = expect(TOK_LARRAY);
					
						Type ini_left_type  = type.m_left;
						Type ini_right_type = type.m_right;
			
						if (!(type.m_primary.equals("iglang.Array")
							||
							type.m_primary.equals("iglang.Vector") 
							||
							 type.m_primary.equals("iglang.Map"))
							) {
				
							softError(ini_list_start, "Can only use initialization list with Arrays, Vectors or Maps. Found: " +
								type);
						}
				
						// get the value type
						Type key_type = ini_left_type;
						Type value_type = ini_right_type;
						if (value_type == null) {
							value_type = ini_left_type;
							key_type = null;
						}
			
						int initializer_count = 0;
					
						short list_node_type = IGNode.ARRAY_INITIALIZER_LIST;
						short item_node_type = IGNode.ARRAY_INITIALIZER_ITEM;
						group_node_type = IGNode.ARRAY_INITIALIZER;
					
						if (type.m_primary.equals("iglang.Vector")) 
						{
							list_node_type  = IGNode.VECTOR_INITIALIZER_LIST;
							item_node_type  = IGNode.VECTOR_INITIALIZER_ITEM;	
							group_node_type = IGNode.VECTOR_INITIALIZER;				
						}
						else if (type.m_primary.equals("iglang.Map")) 
						{
							list_node_type  = IGNode.MAP_INITIALIZER_LIST;
							item_node_type  = IGNode.MAP_INITIALIZER_ITEM;	
							group_node_type = IGNode.MAP_INITIALIZER;				
						}
					
					
					
						while (peekTokenType() != TOK_RARRAY) 
						{
							// wrap each item in a node
							IGNode initialize_item = startNode();
						
							{
								initialize_item.m_user_index = initializer_count;
								initializer_count ++;
						
								// do any soft casting required
								Type key_or_value_type = value_type;
								if (item_node_type == IGNode.MAP_INITIALIZER_ITEM) {
									key_or_value_type = key_type;
								}
								startNode();
								{
									Token param_start = peek();
									IGExpression parameter_expression = parseExpression(ParseExpression_MODE_DEFAULT).toRValue();
									Type         parameter_type       = parameter_expression.m_type;
			
								
								
									if (parameter_type.workRequiredToCastTo(key_or_value_type, false) 
											== Type.WORK_INCOMPATIBLE) 
									{
										//System.err.println("" + function_type);
										softError(param_start, "B. Can't assign a variable of that type to this parameter. Expecting " 
											+ key_or_value_type + " found " + parameter_type);
									}
						
									setNodeType(IGNode.PARAMETER_CAST);
								}
								endNode(key_or_value_type);
							
								if (item_node_type == IGNode.MAP_INITIALIZER_ITEM) 
								{
									startNode();
									{
										expect(TOK_COLON, "Expecting ':' separating initializer item pairs");
										setNodeType(IGNode.MAP_INITIALIZER_SEPARATOR);
									}
									endNode();
									
									startNode();
									{
										Token param_start = peek();
										IGExpression parameter_expression = parseExpression(ParseExpression_MODE_DEFAULT).toRValue();
										Type parameter_type = parameter_expression.m_type;
		
										if (parameter_type.workRequiredToCastTo(value_type, false) == Type.WORK_INCOMPATIBLE) 
										{
											//System.err.println("" + function_type);
											softError(param_start, "C. Can't assign a variable of that type to this parameter. Expecting " 
												+ value_type + " found " + parameter_type);
										}
					
										setNodeType(IGNode.PARAMETER_CAST);
									}
									endNode(value_type);
								
								}
							
								setNodeType(item_node_type);
							}
							endNode(null);
				
				
							if (peekTokenType() != TOK_RARRAY) {
								expect(TOK_COMMA, "Expecting ',' separating initializer list items");
							}
						}
			
				
						Token ini_list_end = expect(TOK_RARRAY, "Missing ']' at end of initializer list");
						setNodeType(list_node_type);
					}
					endNode(type);
					
					
					setNodeType(group_node_type);
				}
				left_node = endNode(type);
			}
			// end INITIALIZER
		}
		else {
			expect(TOK_ID, "Unexpected token in expression or statement");
		}
		
		
		if (!allow_id_type)
		{	
			if (type == null) {
				error("Expecting type before action");
			}
		
			while (peekTokenType() == TOK_LBRACKET ||
				   peekTokenType() == TOK_LARRAY   ||
				   peekTokenType() == TOK_DOT)
			{
				
			
			
				startNode(start_index, peek());
				{
				
				
				
				
					addNodeChild(left_node);
					
					peek_type0 = peekTokenType();
			
			
					///////////////
					// type(function call)
					///////////////
					if (peek_type0 == TOK_LBRACKET)
					{
						
						if (!type.isFunctionWrapper() && !(type.m_primary.equals("Function") || type.m_primary.equals("StaticFunction"))) 
						{
							if (type.m_scope == null || !type.m_scope.isScopeCallable()) 
							{
								error("Expecting function to call: " + type + " " + type.m_scope.describeScopeType());
							}
						}
			
						// parseFunctionCallExpression handles its own unwrapping
						
						
						IGExpression function_call_expression = parseFunctionCallExpression(type);
						type = function_call_expression.m_type;
						
						setNodeType(IGNode.NODE_CALL, type.m_scope);
					}
					////////////////
					// type[index]
					///////////////
					else if (peek_type0 == TOK_LARRAY)
					{
						//hasMemberWithName
						if (!(type.m_scope instanceof IGSource)) {
							error(peek(), type + " doesn't implement get[] and set[]");
						}
						
						IGSource array_source = (IGSource)type.m_scope;
						// 1. verify that there is an array<get>
						// 2. verify that there is an array<set>
						
						IGMember getter = array_source.m_member_map.get("<get[]>");
						IGMember setter = array_source.m_member_map.get("<set[]>");
						
						if (getter == null || setter == null 
							|| getter.getScopeType() != IGScopeItem.SCOPE_ITEM_FUNCTION
							|| setter.getScopeType() != IGScopeItem.SCOPE_ITEM_FUNCTION
							|| !getter.isCorrespondingArraySetter(setter)) 
						{
							// won't know if this is a getter or a setter until we detect
							// an assignment. For now we'll just require that both are present 
							error(peek(), type + " doesn't implement get[] and set[]");
						}
					
						Token index_start = expect(TOK_LARRAY, "Expecting [");
						
						
						// Guarantee that the parameters are of the right format
						//
						Type required_index_type = null;
						startNode();
						{
							IGExpression index_expression = parseExpression().toRValue();
							Type index_type               = index_expression.m_type;
							
					
	
							Type crud = getter.getDataType(type).unwrap();
						
							required_index_type = crud.m_left.m_types[0];
							type                     = crud.m_right;
						
							//System.out.println("" + index_type + " vs " + required_index_type);
						
						
							// verify that the index type is definitely compatible						
							try
							{
								if (index_type.workRequiredToCastTo(required_index_type, false) == Type.WORK_INCOMPATIBLE) 
								{
									softError(index_start, "Invalid index type. Found: " +
										index_type + " required: " + required_index_type);
								}
							}
							catch (RuntimeException ex) {
								error(index_start, "Something went wrong.");
							}
							setNodeType(IGNode.PARAMETER_CAST);
						}
						endNode(required_index_type);
						
						
								
						expect(TOK_RARRAY, "Expecting ]");		
								
						setNodeType(IGNode.NODE_INDEX, type.m_scope);
					}
					/////////////////
					// type.ID
					/////////////////
					else if (peek_type0 == TOK_DOT) 
					{
						Token dot_token = read();
						
						
						IGScopeItem scope_new = null;
						Type previous_type = type;
						
						// create a new node around the token being dereferenced
						startNode();
						{
							Token id = expect(TOK_ID, "Expecting id following '.'");
			
							int peek_token_type = peekTokenType();
							
							boolean is_compound_assignment = peek_token_type == TOK_ADD_ASSIGN ||
													   peek_token_type == TOK_SUB_ASSIGN ||
													   peek_token_type == TOK_MUL_ASSIGN ||
													   peek_token_type == TOK_DIV_ASSIGN ||
													   peek_token_type == TOK_MOD_ASSIGN || 
														peek_token_type == TOK_XOR_ASSIGN || 
														peek_token_type == TOK_OR_ASSIGN || 
														peek_token_type == TOK_AND_ASSIGN;
							boolean possible_setter = (peek_token_type == TOK_ASSIGN ||
													   is_compound_assignment);
														
														
							//System.err.println("dot type: " + type);
			
							scope_new = type.m_scope.getScopeChild(id.m_value, possible_setter);
							
							
							// the error case
							if (scope_new == null) 
							{
								if (peek().m_type == TOK_LBRACKET) 
								{
									error(id, "Couldn't find function with this name: " + id.m_value + " in " + type.m_scope);
								}
								else
								{
									if (possible_setter) {
										IGScopeItem get_scope_new = type.m_scope.getScopeChild(id.m_value, false);
										if (null != get_scope_new) {
											error(id, "Found getter but no setter: " + id.m_value);
										}
										else {
											error(id, "Couldn't find variable with this name: " + id.m_value + " in " + type.m_scope);
										}
									}
									else
									{
										error(id, "Couldn't find variable with this name: " + id.m_value + " in " + type.m_scope);
									}
								}
								
								error(id, "Catch all error");
							}
							// TODO we aint checking privacy are we
							else 
							{
								int scope_type =  scope_new.getScopeType();
								
								if (is_compound_assignment && 
									(scope_type == IGScopeItem.SCOPE_ITEM_FUNCTION || 
									 scope_type == IGScopeItem.SCOPE_ITEM_STATIC_FUNCTION))
								{
									IGScopeItem get_scope_item = type.m_scope.getScopeChild(id.m_value, false);
									if (null == get_scope_item) {
										softError(id, "Found setter but not getter for: " + id.m_value);
									}
								}		
								
								validateDotAccessibility(previous_type, id, scope_new);
							}
						}
						// seems like this is incorrect
						endNode(scope_new.getDataType(previous_type));
				
				
						/*
						 * Okay we need to do some work here resolving out generics
						 */
				
						if (scope_new.getScopeType() == IGScopeItem.SCOPE_ITEM_STATIC_FUNCTION ||
							scope_new.getScopeType() == IGScopeItem.SCOPE_ITEM_FUNCTION) 
						{
							IGMember member = (IGMember)scope_new;
							if (member.hasModifier(IGMember.GET) ||
								member.hasModifier(IGMember.SET)) 
							{
									
								if (scope_new.getScopeType() == IGScopeItem.SCOPE_ITEM_STATIC_FUNCTION) {
									setNodeType(IGNode.STATIC_DOT_ACCESSOR, scope_new);
								}
								else if (scope_new.getScopeType() == IGScopeItem.SCOPE_ITEM_FUNCTION) {
									setNodeType(IGNode.DOT_ACCESSOR, scope_new);
								}
								else {
									throw new RuntimeException("Invalid case");
								}
							
								// determine the type.
								// either the return value of a getter, or the argument of a setter		
								if (member.hasModifier(IGMember.GET)) {
									type = scope_new.getDataType(previous_type).unwrap().m_right;
								}
								else {
									type = scope_new.getDataType(previous_type).unwrap().m_left.m_types[0];					
								}
							}
							else
							{
								setNodeType(IGNode.NODE_DOT, scope_new);
								type = scope_new.getDataType(previous_type);
							}
						}
						else
						{
							setNodeType(IGNode.NODE_DOT, scope_new);
							type = scope_new.getDataType(previous_type);
						}	
				
						
					}
					else
					{
						error("Unexpected token in expression or statement");
					}
				}
				left_node = endNode(type);
			}
		
		
			///////////////////////////
			// Unary postfix
			///////////////////////////		
			{
				if (peekTokenType() == TOK_INC || 
					peekTokenType() == TOK_DEC) 
				{
					Token unary_postfix = peek();
					
					startNode(start_index, unary_postfix);
					{
						addNodeChild(left_node);
						read();		// <- read the postfix token '++' or '--'
						//expect(unary_postfix.m_type, "Expecting postfix type ++ or --");
					
						setNodeType(IGNode.INCREMENT);
					}
					left_node = endNode(type);
					
					if (!type.isArithmetic()) {
						softError(unary_postfix, "Can only increment or decrement ints or doubles");
					}
					
					
					type = Type.VOID;
				}
			}
		}
		
		int op_importance = 101;
		while (0 != (op_importance = isBinaryOrTernary(peekTokenType()))) 
		{
			// enforce a right to left grouping for OR and AND
			if (op_importance == TOKEN_OR || op_importance == TOKEN_AND) 
			{
				if (op_importance > mode) 
				{
					// we need to let the previous op steal our token
					return new IGExpression(type);
				}
			}
			else
			{
				// >= enforces the correct concatenation order
				if (op_importance >= mode) {
					// we need to let the previous op steal our token
					return new IGExpression(type);
				}
			}
			
			// peek at the token to decide what action to perform
			Token op_token = peek();
		
			startNode(start_index, peek());
			{
				addNodeChild(left_node);
				
				Token op   = read();
				IGExpression right_expression = parseExpression(op_importance).toRValue();
				Type right = right_expression.m_type;
		
				// slip in the ternary operator here
				if (op.m_type == TOK_QUESTIONMARK) 
				{
					Type left = right;
					Token colon = expect(TOK_COLON);
				
					IGExpression new_right = parseExpression(op_importance).toRValue(); 	
					right = new_right.m_type;
					setNodeType(IGNode.NODE_TRINARY);
					
					
					if (type != Type.BOOL) {
						softError(op, "Conditional in trinary operator must be of type bool. Found " + type + ".");
					}
					
					if (left == Type.VOID || right == Type.VOID) {
						error(colon, "Neither option of a ternary operator can be void");
					}
					
					if (left != right) {
						
						if (left == Type.NULL  && right.isObjectInstance()) {
							type = right;	
						}
						else if (right == Type.NULL && left.isObjectInstance()) {
							type = left;
						}
						else
						{
							type = right;
							softError(colon, "Each option of the ternary operator must have identical types.");
						}
					}
					else {
						// TODO merge left and right types together
						type = right;
					}
					
				}
				//////////////////////////////////////////
				// BINARY OPERATORS
				//////////////////////////////////////////
				else
				{
					short node_type = IGNode.NODE_BINARY;

					if (type == Type.VOID || right == Type.VOID) {
						error(op, "Neither side of a binary operator can be void");
					}
				
					if (op_importance == TOKEN_RELATIONAL) 
					{
						if (op.m_type == TOK_LT ||
						    op.m_type == TOK_GT ||
						    op.m_type == TOK_LE ||
						    op.m_type == TOK_GE) {
						 
						 	if (!type.isArithmetic() || !right.isArithmetic()) {
								softError(op, "Incompatible arguments for comparison. " + type + " vs " + right);
							}   
						}
						
						type = Type.BOOL;
					}
					else if (op.m_type == TOK_AND && op.m_type == TOK_OR)
					{
						if (type != Type.BOOL || 
						   right != Type.BOOL) {
						
							softError(op, "Left and right sides must be bools");
						
							type = Type.BOOL;
						}
					}					
					else if (op_importance == TOKEN_EQUALITY) 
					{
						if (type == Type.NULL) 
						{
							if (!right.isNullComparable()) {
								softError(op, "Right side cannot be compared to null.");
							}
						}
						else if (right == Type.NULL) 
						{
							if (!type.isNullComparable()) {
								softError(op, "Left side cannot be compared to null.");
							}
						}
						// this fails on null != type statements
						else if (right.workRequiredToCastTo(type, false) == Type.WORK_INCOMPATIBLE) {
							softError(op, "Left side can't be converted to the right side. " + type + " " +
									op.m_value + " " + right);
						}
						
						type = Type.BOOL; 
					}
					else if (op_importance == TOKEN_ASSIGNMENT)
					{
						// this'll eventually be handled by the lvalue nonsense
						if (!left_node.canBeTargetOfAssignment()) {
							softError(op, "Left side of assignment is invalid");
						}
						
						if (right.workRequiredToCastTo(type, false) == Type.WORK_INCOMPATIBLE) 
						{
							softError(op, "Right side of the assignment can't be implicitly coerced into left side. " 
								+ type.fancyToString() + " " +
									op.m_value + " " + right.fancyToString());
						}
						
						//type = Type.VOID;
					}

					else if (op.m_type == TOK_ADD && (right == Type.STRING || type  == Type.STRING)) 
					{
						Type non_string_type = (right != Type.STRING) ? right : type;
						if (non_string_type != Type.STRING)
						{
							if (non_string_type.workRequiredToCastTo(Type.STRING, true) == Type.WORK_INCOMPATIBLE) {
								error(op, "Cannot concatenate type: " + non_string_type.fancyToString() + " with a String");
							}
						}
						type = Type.STRING;			
					}
					else if (op.m_type == TOK_DIV) 
					{
						
						if (type.isIntish() && 
							right.isIntish()) {
						
							if (type == Type.UINT && right == Type.UINT) {
								type = Type.UINT;
							}
							else {
								type = Type.INT;
							}
						}
						else
						{
							type = Type.DOUBLE;
						}		
					}
					else if (op.m_type == TOK_MUL) 
					{
						if (type.isTuple())
						{
							int right_count = right.getTupleCount();

							// tuple multiplied by a single value
							if (type.getTupleCount() > 1 &&
								type.isTupleDoubles() && right == Type.DOUBLE) 
							{
								node_type = IGNode.NODE_TUPLE_BINARY_SINGLE;
							}
							// tuple multiplie by itself
							else if (type.getTupleCount() == right_count &&
								type.isTupleDoubles() && right.isTupleDoubles()) 
							{
								node_type = IGNode.NODE_TUPLE_BINARY;
							}
							else
							{
								softError(op, "Incompatible arguments. " + type + " vs " + right);
							}

						
						}
						else
						{
							int work = right.workRequiredToCastTo(type, false);
							if (work != Type.WORK_NONE && work != Type.WORK_PRIMITIVE_CAST) {
								softError(op, "Incompatible arguments. " + type + " vs " + right);
							}
							else if (!type.isArithmetic() || !right.isArithmetic()) {
								softError(op, "Incompatible arguments. " + type + " vs " + right);
							}
							else
							{
								// if either side is a double the result is a double
								if (type == Type.DOUBLE || right == Type.DOUBLE) {
									type = Type.DOUBLE;
								}
							}
						}
					}
					else if (op.m_type == TOK_ADD || 
							 op.m_type == TOK_SUB || 
							 op.m_type == TOK_MUL) 
					{

						if (type.isTuple())
						{
							if (type.getTupleCount() == right.getTupleCount() &&
								type.isTupleDoubles() && right.isTupleDoubles()) 
							{
								node_type = IGNode.NODE_TUPLE_BINARY;
							}
							else
							{
								softError(op, "Incompatible arguments. " + type + " vs " + right);
							}

						
						}
						else
						{
							int work = right.workRequiredToCastTo(type, false);
							if (work != Type.WORK_NONE && work != Type.WORK_PRIMITIVE_CAST) {
								softError(op, "Incompatible arguments. " + type + " vs " + right);
							}
							else if (!type.isArithmetic() || !right.isArithmetic()) {
								softError(op, "Incompatible arguments. " + type + " vs " + right);
							}
							else
							{
								// if either side is a double the result is a double
								if (type == Type.DOUBLE || right == Type.DOUBLE) {
									type = Type.DOUBLE;
								}
							}
						}
					}
					else if (op.m_type == TOK_MOD) 
					{

						int work = right.workRequiredToCastTo(type, false);
						if (work != Type.WORK_NONE && work != Type.WORK_PRIMITIVE_CAST) {
							softError(op, "Incompatible arguments. " + type + " vs " + right);
						}
						else if (!type.isArithmetic() || !right.isArithmetic()) {
							softError(op, "Incompatible arguments. " + type + " vs " + right);
						}
						else
						{
							// if either side is a double the result is a double
							if (type == Type.DOUBLE || right == Type.DOUBLE) {
								type = Type.DOUBLE;
							}
						}
					}
					else if (op.m_type == TOK_BIT_AND || 
							 op.m_type == TOK_BIT_OR  || 
							 op.m_type == TOK_BIT_NOT || 
							 op.m_type == TOK_BIT_XOR ||
							 op.m_type == TOK_LSR     ||
							 op.m_type == TOK_ASR     ||
							 op.m_type == TOK_LSL)
					{
						if (!type.isIntishOrIntishEnum() || !right.isIntishOrIntishEnum()) {
							softError(op, "Both sides of a bitwise operator must be ints or enums that extend ints. " + type + " vs " + right);
						}	
						
						type = Type.INT;
					}

					setNodeType(node_type);
				}
			}
			
			left_node = endNode(type);
			//type = right;
		}
		
		return new IGExpression(type);
	}
	


	public static IGNode resolveForEach(int block_state)
	{
		startNode();
		// child0 = var statement
		// child1 = in  statement
		//		child0 = container
		//		child1 = RANGE or iteratotr
		// child2 = block
		{
		
			int     peek_type        = peekTokenType();
			boolean classic_for_each = (peek_type == TOK_FOR_EACH);
			
			
			Token for_token = expect(peek_type);
			expect(TOK_LBRACKET);
			
			
			Token iterator_value_token = peek();
			
			Type iterator_type = null;
		
			if (peekTokenType() == TOK_VAR) {
				iterator_type = parseVariable();
			}
			else 
			{
				if (classic_for_each) {
					iterator_type = parseStatement();
				}
				else {
					IGExpression interator_type_expression = parseExpression().toLRValue();
					iterator_type = interator_type_expression.m_type;
				}
			}
		
			Type expression_result  = null;
			startNode();
			{
				expect(TOK_IN);

				// parse the expression following the 'in'
				IGExpression expression_collection = parseExpression().toRValue();
				expression_result = expression_collection.m_type;
			
				// ranged for each
				// for (var i : int in 0 => 9)
				if (!classic_for_each)
				{
					Type max_expression_result = null;
					startNode();
					{
						expect(TOK_RANGE);
						
						IGExpression max_expression_result2 = parseExpression(); 
						max_expression_result = max_expression_result2.m_type;
						
						// TODO allow for enums that use int as their primitives
						if (!expression_result.isIntishOrIntishEnum() ||
							!max_expression_result.isIntishOrIntishEnum()) {
							
							softError(for_token, "Ranged foreach's only work on ints or enums that extend ints. " + expression_result + " => " + max_expression_result);
						}
						
						if (!iterator_type.isIntishOrIntishEnum()) {
							softError(iterator_value_token, "Iterators for ranged foreach's only work on ints or enums that extend ints. Found: " + iterator_type);
						}
						
						setNodeType(IGNode.FOREACH_IN_RANGE, null);
					}	
					endNode(max_expression_result);
				}
				// classic for each
				// for (var i : X in COLLECTION)
				else
				{
					IGScopeItem scope_new = null;
					Type previous_type = expression_result;
					
					// create a new node around the token being dereferenced
					startNode();
					{
						scope_new = expression_result.m_scope.getScopeChild("iterator", false);
					}
					
					if (scope_new                == null || 
						scope_new.getScopeType() != IGScopeItem.SCOPE_ITEM_MEMBER_FUNCTION) 
					{
						error(for_token, "Iteratable objects must have a function iterator");
					}
					
					// we now have a wrapper surrounding the function to get the iterator
					Type iterator_function_type = scope_new.getDataType(previous_type);
					{
						// remove the <wrapper> type
						Type unwrapped_iterator_function_type = iterator_function_type.unwrap();
					
						// verify that the iterator function takes 0 parameters
						if (unwrapped_iterator_function_type.m_left.getMaximumParameters() != 0) {
							error(for_token, "Iteratable objects must have a function iterator, that takes 0 parameters");
						}
					
						// get the return type of the function... eg. iglang.util.VectorIterator<int>
						Type       iterator_object_type = unwrapped_iterator_function_type.m_right;
					
						// verify that this object has a "next" function... 
						IGScopeItem scope_iterator_next = iterator_object_type.m_scope.getScopeChild("next", false);
						if (scope_iterator_next == null ||
							scope_iterator_next.getScopeType() != IGScopeItem.SCOPE_ITEM_MEMBER_FUNCTION) {
						
							error(for_token, "Iterators must have a next function.");
						}
						
						{
							IGScopeItem scope_iterator_hasnext = iterator_object_type.m_scope.getScopeChild("has_next", false);
							if (scope_iterator_hasnext == null ||
								scope_iterator_hasnext.getScopeType() != IGScopeItem.SCOPE_ITEM_MEMBER_VARIABLE) {
						
								error(for_token, "Iterators must have a has_next variable");
							}
							
							Type         scope_iterator_hasnext_type = scope_iterator_hasnext.getDataType(iterator_object_type);
							if (Type.BOOL != scope_iterator_hasnext_type) {
								error(for_token, "Iterators must have a has_next variable of type bool");
							}
						}
						
						// get a fully qualified reference to that next function
						Type           scope_iterator_next_fn_type = scope_iterator_next.getDataType(iterator_object_type);		// umm does this make sense <===
						Type unwrapped_scope_iterator_next_fn_type = scope_iterator_next_fn_type.unwrap();
					
						// verify that the iterator function takes 0 parameters
						if (unwrapped_scope_iterator_next_fn_type.m_left.getMaximumParameters() != 0) {
							error(for_token, iterator_object_type +" Iterator next function must take 0 parameters.");
						}
					
						//System.out.println("it check: " + previous_type + " " + scope_new);
						//System.out.println("iterator type: " + iterator_type);
						//System.out.println("previous type: " + previous_type);
						//System.out.println("check: " + unwrapped_iterator_function_type);
						//System.out.println("iterator_object_type: " + iterator_object_type);
						//System.out.println("scope iterator next: " + scope_iterator_next);
						//System.out.println("next fn type: " + unwrapped_scope_iterator_next_fn_type);
						
						if (iterator_type != unwrapped_scope_iterator_next_fn_type.m_right) {
							softError(for_token, "Not compatible with collection: " + iterator_type + " vs " + unwrapped_scope_iterator_next_fn_type.m_right + " in " + expression_result);
						}
						// seems like this is incorrect
					}
					
					
					endNode(iterator_function_type);		
							
				}
			
				/*
				if (expression_result.m_primary.equals("iglang.Map")) {
			
				}
				else if (expression_result.m_primary.equals("iglang.Vector") ||
						 expression_result.m_primary.equals("iglang.Array")) {
			
				}
				else
				{
					error(for_token, "For can only iterator over iglang Map, Vector and Array collections");
				}
				*/

				setNodeType(IGNode.NODE_TYPE_FOREACH_IN, null);
			}
			endNode(expression_result);					
			expect(TOK_RBRACKET);
		
			// The body of the for each
			IGNode foreach_body_block = resolveBlock(block_state | BLOCK_STATE_LOOP);
			//if (foreach_body_block.m_control_flow != 0) {
			//	System.out.println("cf: " + foreach_body_block.m_control_flow);
			//	foreach_body_block.debug();
			//	//System.out.println("" + foreach_body_block);
			//}
		
			setNodeType(IGNode.NODE_TYPE_FOREACH, null);
		}
		return endNode(Type.VOID);
	}
	
	
	
	public static IGNode resolveFor(int block_state)
	{
		startNode();
		{
			expect(TOK_FOR);
			
			// child 0
			expect(TOK_LBRACKET);
		
			if (peekTokenType() == TOK_VAR) {
				parseVariable();
			}
			else {
				parseStatement();
			}
			expect(TOK_SEMI);
			
			// child 1
			
			Token  tok = peek();
			IGExpression conditional_expression = parseExpression().toRValue();
			if (conditional_expression.m_type != Type.BOOL) {
				softError(tok, "Expecting boolean in condition of for loop");
			}
			expect(TOK_SEMI);
			
			// child 2
			//parseExpression();
			parseStatement();
			expect(TOK_RBRACKET);

			// child 3
			// The body of the for 
			IGNode for_body = resolveBlock(block_state | BLOCK_STATE_LOOP);
			//System.out.println("for_bod cf: " + for_body.m_control_flow);
			//for_body.debug();
		
			setNodeType(IGNode.NODE_FOR, null);
		}
		return endNode(Type.VOID);	
	}
	
	
	
	public static final int BLOCK_STATE_LOOP = 1;

	public static IGNode resolveBlock(int block_state)
	{			
		IGMember m = s_member;
		boolean return_encountered = false;
		boolean unreachable_raised = false;
		
		
		IGNode last_block_node = null;
		
		IGNode block_node = startNode();
		{
			expect(TOK_LBRACE);
			
			while (peek() != null && peekTokenType() != TOK_RBRACE)
			{
				
				int peek_token_type = peekTokenType();
				
				if (peek_token_type != IGLexer.LEXER_NATIVE_BLOCK) {
					last_block_node = null;
				}
				
				
				
				
				
				if (return_encountered && 
					!unreachable_raised && 
					peek_token_type != IGLexer.LEXER_NATIVE_BLOCK) {
					softError(peek(), "Unreachable statement");
					unreachable_raised = true;
				}
			
				// scoped blocks
				if (peek_token_type == TOK_LBRACE) {
					last_block_node = resolveBlock(block_state);
					if (last_block_node.m_control_flow != 0) {
						return_encountered = true;
					}
					
				}
				else if (peek_token_type == IGLexer.LEXER_NATIVE_BLOCK)
				{
					startNode();
						expect(peek_token_type);
						
						setNodeType(IGNode.NATIVE_BLOCK);
					endNode();
				}
				else if (peek_token_type == TOK_CONTINUE ||
						 peek_token_type == TOK_BREAK) {
					

					
					IGNode node = startNode();
					{
						node.m_control_flow |= IGNode.CF_CONTINUE_OR_BREAK;
						return_encountered = true;
						last_block_node = node;
						
						
						Token flow_change = expect(peek_token_type);

						if ((block_state & BLOCK_STATE_LOOP) == 0) {
							if (peek_token_type == TOK_CONTINUE) {
								softError(flow_change, "continue outside of loop");
							}
							else {
								softError(flow_change, "break outside of loop");
							}
						}

						expect(TOK_SEMI);
						
						
						setNodeType(IGNode.FLOW_CHANGE);
					}
					endNode();		 
				}
				else if (peek_token_type == TOK_RETURN) 
				{
					IGNode return_node = startNode();
					{
					
						return_node.m_control_flow |= IGNode.CF_RETURN;
						
						// mark that this node 
						//return_node.m_returns = true;
						last_block_node = return_node;
						return_encountered = true;
						
						Token return_token = expect(peek_token_type);
						
						Type return_type = Type.VOID;
						
						if(!(m.m_return_type == Type.VOID || "<constructor>".equals(m.m_name.m_value))  )
						{					
							if (peekTokenType() != TOK_SEMI) {
								// yes a void expression is possible
								IGExpression return_expression = parseExpression().toRValue();
								return_type = return_expression.m_type;
							}		
						
							if ("<constructor>".equals(m.m_name.m_value)) {
								if (return_type != Type.VOID)
								{
									softError(return_token, "Can't return values from a constructor: " + return_type);
								}
							}
							else
							{						
								if (return_type.workRequiredToCastTo(m.m_return_type, false) == Type.WORK_INCOMPATIBLE) {
								//if (!return_type.assignable(m.m_return_type)) {
									softError(return_token, 
										"Can't cast expression (" + return_type + ") to function return type: " + m.m_return_type);
								}
							}
						}
						
						expect(TOK_SEMI, "Expecting a semicolon following a return statement");
						
						setNodeType(IGNode.NODE_RETURN);
					}
					endNode();
				}
				else if (peek_token_type == TOK_TRY)
				{
					int try_return_counts = 0;
					int try_counts = 0;
					
					byte overall_control_flow = (byte)0xff;
				
					IGNode try_catch_container = startNode();
						expect(peek_token_type);
						IGNode try_block = resolveBlock(block_state);
						
						overall_control_flow &= try_block.m_control_flow;
						
						//try_return_counts += try_block.m_returns ? 1 : 0;
						try_counts ++;
						
						while (peekTokenType() == TOK_CATCH) 
						{
							startNode();
								expect(TOK_CATCH);
								
								expect(TOK_LBRACKET);
								parseVarExpression(ParseExpression_VARIABLE_DECLARATION);
								expect(TOK_RBRACKET);
									
								IGNode catch_block = resolveBlock(block_state);
								
								overall_control_flow &= catch_block.m_control_flow;
								
								//try_return_counts += catch_block.m_returns ? 1 : 0;
								try_counts ++;
								
								
								setNodeType(IGNode.NODE_CATCH);
							endNode();	
						}
						
						//if (try_counts == try_return_counts || 
						if (overall_control_flow != 0) {
						
							try_catch_container.m_control_flow = overall_control_flow;
							last_block_node = try_catch_container;
							return_encountered = true;
						}
						
						
						setNodeType(IGNode.TRY);
					endNode();
				
				}
				else if (peek_token_type == TOK_THROW) 
				{
					IGNode throw_node = startNode();
					{
						throw_node.m_control_flow = IGNode.CF_RETURN;
						//throw_node.m_returns = true;
						last_block_node = throw_node;
						return_encountered = true;
						
						
						Token throw_token = expect(peek_token_type);
						IGExpression throw_expression = parseExpression().toRValue();
						Type   throw_type = throw_expression.m_type;
						if (!throw_type.assignable(Type.get("iglang.Error"))) {
							softError(throw_token, "Thrown object must extend iglang.Error");
						}
						
						expect(TOK_SEMI);
						
						setNodeType(IGNode.THROW);
					}
					endNode();
				}
				else if (peek_token_type == TOK_WHILE) 
				{
					startNode();
						expect(peek_token_type);
						expect(TOK_LBRACKET);
						Token tok = peek();
						IGExpression while_condition_expression = parseExpression().toRValue();			// <child 0>
						if (while_condition_expression.m_type != Type.BOOL) {
							softError(tok, "Expecting a boolean condition for a while loop");
						}
						expect(TOK_RBRACKET);
					
						resolveBlock(block_state | BLOCK_STATE_LOOP);				// <child 1>
						
						setNodeType(IGNode.WHILE);
					endNode();			
				}
				else if (peek_token_type == TOK_DO)
				{
					startNode();
						expect(TOK_DO);
						resolveBlock(block_state | BLOCK_STATE_LOOP);			// <child0>
						expect(TOK_WHILE);
						expect(TOK_LBRACKET);
						Token tok = peek();
						IGExpression do_while_condition_expression = parseExpression();		// <child1>
						if (do_while_condition_expression.m_type != Type.BOOL) {
							softError(tok, "Expecting a boolean condition for a do/while loop");
						}
						expect(TOK_RBRACKET);
						expect(TOK_SEMI);
						
						setNodeType(IGNode.DO);
					endNode();	
				}
				else if (peek_token_type == TOK_FOR_EACH || peek_token_type == TOK_FOR_EACH_IN_RANGE)
				{
					resolveForEach(block_state);
				}
				else if (peek_token_type == TOK_FOR) {
					resolveFor(block_state);
				}
				else if (peek_token_type == TOK_SWITCH) {
					//resolveSwitch();
					
					Type switch_type = null;
					IGNode switch_node = startNode();
					{	
						Token switch_start = expect(TOK_SWITCH);
						
						expect(TOK_LBRACKET);
						IGExpression switch_conditional_expression = parseExpression().toRValue();
						switch_type = switch_conditional_expression.m_type;
						expect(TOK_RBRACKET);
						
						byte control_flow_mask = (byte) 0xff;
						
						
						Type cmp_type = switch_type;
					
						if (cmp_type.isEnum()) {
							cmp_type = cmp_type.getEnumPrimitive();
						}
						
						if (cmp_type != Type.BOOL && cmp_type != Type.INT && cmp_type != Type.DOUBLE) {
							error(switch_start, "Unsupported switch type: " + switch_type);
						}

						
						expect(TOK_LBRACE, "Expecting { following switch");
						{
							do 
							{
								Type case_type;
								startNode();
								{
									Token case_token = expect(TOK_CASE, "Expecting case in switch");
									
									expect(TOK_LBRACKET);
									IGExpression case_expression = parseExpression().toRValue();
									case_type = case_expression.m_type;
									
									if (case_type.workRequiredToCastTo(switch_type, false) == Type.WORK_INCOMPATIBLE) 
									{
										softError(case_token, 
											"Can't cast case expression (" + case_type + ") to switch type (" + switch_type + ")");
									}
									
									
									expect(TOK_RBRACKET);
								
									IGNode node = resolveBlock(block_state);
									control_flow_mask &= node.m_control_flow;
								
									setNodeType(IGNode.SWITCH_CASE);
								}
								endNode(case_type);
							}
							while (peekTokenType() == TOK_CASE);
						
							boolean found_default_case = false;
						
							if (peekTokenType() == TOK_DEFAULT) {
								startNode();
								{
									expect(TOK_DEFAULT);
									IGNode node = resolveBlock(block_state);
									control_flow_mask &= node.m_control_flow;
									
									setNodeType(IGNode.SWITCH_DEFAULT);
								}
								endNode(Type.VOID);
								
								found_default_case = true;
							}
							
							dontExpect(TOK_DEFAULT, "Extra default");
							dontExpect(TOK_CASE   , "Additional case following default");
							
							
							if (found_default_case)
							{
								switch_node.m_control_flow = control_flow_mask;
								if (control_flow_mask != 0 ) {
									return_encountered = true;
									last_block_node = switch_node;
								}
							}
						}
						expect(TOK_RBRACE, "Expecting } to terminate switch");
						
						setNodeType(IGNode.SWITCH);
					}
					endNode(switch_type);
				}
				else if (peek_token_type == TOK_IF) 
				{
					byte control_flow_mask = (byte) 0xff;
					IGNode if_statement_node = startNode();
					{
						setNodeType(IGNode.NODE_IF);
						expect(TOK_IF);
						
						// <child 0>
						Token if_expr = expect(TOK_LBRACKET);
						IGExpression if_expression = parseExpression().toRValue();
						Type  if_type  = if_expression.m_type;
						if (if_type != Type.BOOL) {
							error(if_expr, "Expecting a true or false value in an if statement. Found: " + if_type.m_primary);
						}
						expect(TOK_RBRACKET);
						
						boolean if_else_clause_found = false;
						int if_return_count = 0;
						int if_block_count = 1;
						
						
						
						// <child 1>
						IGNode first_if = resolveBlock(block_state);
						control_flow_mask &= first_if.m_control_flow;
						
						// <optional: child 2...N>
						while (peekTokenType() == TOK_ELSE) 
						{
							if_block_count ++;
							
							startNode();
							{
								expect(TOK_ELSE);
								if (peekTokenType() == TOK_IF) 
								{
									expect(TOK_IF);
									setNodeType(IGNode.NODE_ELSE_IF);
								
									// if (a == true)
									//    ^^^^^^^^^^^	
									if_expr = expect(TOK_LBRACKET);
									if_expression = parseExpression();
									if (Type.BOOL != (if_type = if_expression.m_type)) {
										error(if_expr, "Expecting a true or false value in an else if statement. Found: " + if_type.m_primary);
									}				
									expect(TOK_RBRACKET);
						
									IGNode else_if_block = resolveBlock(block_state);
									control_flow_mask &= else_if_block.m_control_flow;
								}	
								else
								{
									if_else_clause_found = true;
									
									setNodeType(IGNode.NODE_ELSE); 		
									IGNode else_block = resolveBlock(block_state);
									control_flow_mask &= else_block.m_control_flow;
								}
							}
							endNode();	
						}
						
						if (if_else_clause_found)
						{
							if_statement_node.m_control_flow = control_flow_mask;
							if (control_flow_mask != 0 ) {
								return_encountered = true;
								last_block_node = if_statement_node;
							}
						}
						
						
					}
					endNode();
				}
				else if (peek_token_type == TOK_VAR)
				{
					parseVariable();
					expect(TOK_SEMI, "Expecting; following variable declaration");
				}
				// just a statement
				else
				{
					
					parseStatement();
					expect(TOK_SEMI, "Expecting ; following a statement");
				}
			}
			
			expect(TOK_RBRACE);
		}
		setNodeType(IGNode.NODE_BLOCK, null);
		
		
		if (last_block_node != null)
		{
			block_node.m_control_flow = last_block_node.m_control_flow;
			//block_node.m_returns = (block_node.m_control_flow & IGNode.CF_RETURN) != 0;
		}
		////if (last_block_node != null && last_block_node.m_returns) {
		//	block_node.m_returns = true;
		//}
		
		return endNode();
	}

	public static Type parseStatement()
	{	
		Type expression_type;
		startNode();
		{
			IGExpression expression = parseExpression();
			expression_type = expression.m_type;
			setNodeType(IGNode.STATEMENT);
		}
		endNode(expression_type);
		return expression_type;
	}
	
	private static Type parseVariable()
	{
				
		IGVariable variable = null;
	
		IGNode var_node = startNode();
		{
			Token var = expect(TOK_VAR);
			variable = (IGVariable)s_source.getAnnotationObject(var);	//var.m_annotation_object;
		
			// parse the expression expect that the first token in it will be
			// ID : TYPE
			parseVarExpression(ParseExpression_VARIABLE_DECLARATION, var_node);
		
			// this type isn't used anywhere
			setNodeType(IGNode.NODE_TYPE_LOCAL_VARIABLE_DEFINITION, variable);
		}
		endNode(variable.m_type);	
		
		return variable.m_type;
	}

	public static boolean isMemberModifier(int type) {
		if (type == TOK_PUBLIC || type == TOK_PRIVATE || type == TOK_PROTECTED || type == TOK_INTERNAL ||
			type == TOK_STATIC || type == TOK_OVERRIDE || type == TOK_FINAL || type == TOK_NATIVE) {
			return true;	
		}
		return false;
	}



	// grammer:
	// MEMBER-NODE           = MEMBER_PREAMBLE MEMBER-NAME
	// MEMBER-NAME           = 'function' 'get' ID | function' 'set' ID | 'var' ID | 'const' ID | 'function' ID
	// MEMBER-PREAMBLE       = MEMBER-PREAMBLE-TOKEN *
	// MEMBER-PREAMBLE-TOKEN = 'public' | 'static' | 'private' | 'protected'

	public static void parseMember(IGSource s, IGMember m, IGResolver r)
	{
	
		boolean is_enum_value = false;
		
		if (m.hasModifier(IGMember.ENUM_VALUE)) {
			is_enum_value = true;
		}
	
		m.m_base_node = startMemberNode(m);
		{
			//int member_start_index = m.m_parameter_start_index;

			//public function get name
			//public static override final function set name
			//ENUM,
			//

			// skip over all unnecessary preamble for the moment
			// public/private/static/final function/var
			startNode();
			{
				if (!is_enum_value) 
				{
					while (isMemberModifier(peek().m_type)) 
					{
						startNode();
						{
							read();
							setNodeType(IGNode.MEMBER_PREAMBLE_TOKEN, m);
						}
						endNode();
					}	
				}
				
				setNodeType(IGNode.MEMBER_PREAMBLE, m);
			}
			IGNode prefix = endNode();
		
			startNode();
			{
				if (!is_enum_value) 
				{
					if (peek().m_type == TOK_FUNCTION) {
						expect(TOK_FUNCTION);
						
						if      (peek().m_type == TOK_GET) { expect(TOK_GET); }
						else if (peek().m_type == TOK_SET) { expect(TOK_SET); }
						
						expect(TOK_ID, "Expecting a name");
					}
					else if (
						peek().m_type == TOK_CONST ||
						peek().m_type == TOK_VAR) 
					{
						read();
					}
					else {
						error("Expecting function, const or var.");
					}
				}
				
				setNodeType(IGNode.MEMBER_NAME, m);
			}
			IGNode member_name  = endNode();
		
			//member_name.debug();
		
			// record the node stack size so that if there is an exception
			// we can reset the stack size so that there are no errors
			int stack_height = getNodeStackSize();
		
			boolean is_enum = false;
		

			try
			{		
				////////////////////////////////////////////////////////
				// ENUM MEMBER
				if ( //peek().m_annotation == ANNOTATION_ENUM_MEMBER
					s_source.getAnnotationType(peek()) == ANNOTATION_ENUM_MEMBER
					 && m.hasModifier(IGMember.ENUM_VALUE))
				{	
					is_enum = true;
					IGNode enum_member_node = startNode();
					{
						parseExpression();
						setNodeType(IGNode.NODE_ENUM_MEMBER, m);
					}
					m.m_declaration_node = enum_member_node;
					m.m_node = endNode();
				}
				///////////////////////////////////////////////////////
				// Static or IGMember IGVariable
				else if (m.m_type == IGMember.TYPE_VAR)
				{
					// notice how the member parameter is set to null
			
					// Creates a node to wrap a var
					IGNode variable_node = startNode();
					{
						parseVarExpression(ParseExpression_MEMBER_VARIABLE_DECLARATION);
					
						//if (m.hasModifier(IGMember.STATIC)) {
						//	setNodeType(IGNode.STATIC_VARIABLE, m);
						//}
						//else
						//{
						//	setNodeType(IGNode.MEMBER_VARIABLE, m);
						//}
					}
					m.m_declaration_node = variable_node;
					m.m_node =  endNode();
				}
				/////////////////////////////////////////////
				// Static or member function
				else
				{
		
					startNode();
					{
						//////////////////////////////////////////////////////////
						// NODE-TYPE:  NODE_FUNCTION_SIGNATURE  (child 0)
						// parse the parameter list
						// ( ID : TYPE, IG : TYPE .....) <optional : TYPE>
						//////////////////////////////////////////////////////////
				
						Type function_return_type = Type.VOID;
				
						startNode();
						{
							expect(TOK_LBRACKET);
						
							// the parameters to the function in question
							startNode();
							{
								while (peek() != null)
								{
									if (peekTokenType() != TOK_RBRACKET)
									{
										parseVarExpression(ParseExpression_VARIABLE_DECLARATION);
									}
			
			
									if (peekTokenType() == TOK_RBRACKET) {
										break;
									}

									expect(TOK_COMMA, "Expecting comma");
								}
								setNodeType(IGNode.FUNCTION_PARAMETERS, m);
							}
							endNode();
				
							expect(TOK_RBRACKET);

							// there'll always be a return type unless we're dealing with
							// a constructor				
							if (!m.isConstructor()) 
							{
								Type type = null;
								startNode();
								{
									expect(TOK_COLON);
					
									startNode();
									{					
										type = fullyQualify(s_resolver, readTypeAllowVoid(), peek());
										function_return_type = type;
										setNodeType(IGNode.NODE_TYPE_TYPE, null);	
									}
									endNode(type);
						
									setNodeType(IGNode.FUNCTION_RETURN_TYPE, null);
								}
								endNode(type);
							}
						
							setNodeType(IGNode.NODE_FUNCTION_SIGNATURE);
						}
						endNode();

					
						///////////////////////////////////////////////////////////
						// parse the body of the function (child 1)
						///////////////////////////////////////////////////////////

						if (peek() != null && peekTokenType() != TOK_SEMI) 
						{
							//setTokenList(s.m_token_list, m.m_first_body_token, m.m_last_body_token, m, r, s);
							IGNode block_node = resolveBlock(0);
						
							
						
						
							if (function_return_type != Type.VOID)
							{
								
							
								if ((block_node.m_control_flow & IGNode.CF_RETURN) == 0)	
								{
									softError(m.m_name, "Function doesn't return a value on all paths");	
								}
								else 
								{
									
								
									if (block_node.m_children.size() == 1 && block_node.getChild(0).m_node_type == IGNode.NODE_RETURN)
									{
										IGNode should_be_return = block_node.getChild(0);
										
										//System.out.println("Valid return 2: " + block_node.m_children.size());
										//block_node.debug();
										
										if (should_be_return.m_children.size() == 1) {
											IGNode should_be_field = should_be_return.getChild(0);
											if (should_be_field.m_node_type == IGNode.FIELD_ACCESS) {
												
												m.m_inline_candidate = 1;
												m.m_inline_candidate_node = should_be_field;									
											}
											else if (should_be_field.m_node_type == IGNode.STATIC_FIELD_ACCESS) {
												
												m.m_inline_candidate = 2;
												m.m_inline_candidate_node = should_be_field;										
											}
											

										}
									}
								}
							}
						}
					}
				
				
					m.m_node = endNode();
				}

			}
			catch (RuntimeException ex)
			{
				ex.printStackTrace();
				s.m_failed_pass0 = true;
				setNodeStackSize(stack_height);
			}	
		
			// apply the tag to the outer node
			int scope_type = m.getScopeType();
			if (!is_enum)
			{
				if (scope_type == IGScopeItem.SCOPE_ITEM_STATIC_VARIABLE) 	{ setNodeType(IGNode.CLASS_STATIC_VARIABLE); }
				if (scope_type == IGScopeItem.SCOPE_ITEM_VARIABLE) 			{ setNodeType(IGNode.CLASS_VARIABLE);        }
				if (scope_type == IGScopeItem.SCOPE_ITEM_STATIC_FUNCTION) 	{ setNodeType(IGNode.CLASS_STATIC_FUNCTION); }
				if (scope_type == IGScopeItem.SCOPE_ITEM_FUNCTION) 			{ setNodeType(IGNode.CLASS_FUNCTION);        }	
			}
		}		
		endNode();
		
		int scope_type = m.getScopeType();
		if (scope_type == IGScopeItem.SCOPE_ITEM_STATIC_VARIABLE ||
			scope_type == IGScopeItem.SCOPE_ITEM_VARIABLE) {
			
			if (m.hasModifier(IGMember.ENUM_VALUE)) {
				if (peek().m_type == TOK_COMMA) {
					expect(TOK_COMMA,"");
				}
				else if (peek().m_type == TOK_RBRACE) {
					// likely the end of the list?
				}
				else {
					// this should actually depend on the type of enum..
					// there will most certainly be cases where it wont work
					expect(TOK_SEMI, "Expecting ',' or ';' terminating enum value: " + m.m_name.m_value);
				}	
			}
			else 
			{
				expect(TOK_SEMI, "Expecting ';' terminating variable: " + m.m_name.m_value);
			}
		}
		else if (s.getScopeType() == IGScopeItem.SCOPE_ITEM_INTERFACE) {
			expect(TOK_SEMI, "Expecting ';' terminating interface function: " + m.m_name.m_value);
		}
	}
	
	public static Type fullyQualify(IGResolver r, Type type, Token viscinity)
	{
		if (null == type) return null;
		
		Type result  = null;
		try
		{
			 result = r.makeAbsoluteType(type);
		}
		catch (IGResolverException ex) 
		{
			ex.printStackTrace();
			error(viscinity, "Could't qualify type: " + type);
		}

		return result;
	}
	
	
	/**
	 * Convert an intermediate type into a fully qualified one.
	 */
	
	public static Type fullyQualify(IGResolver r, Type type, Token viscinity, String error_message)
	{
		if (null == type) return null;
		
		Type result  = null;
		try
		{
			 result = r.makeAbsoluteType(type);
		}
		catch (IGResolverException ex) 
		{
			ex.printStackTrace();
			
			if (ex.m_type != null) {
				error(viscinity, "Couldn't resolve type: " + ex.m_type);
			}
			else {
				error(viscinity, error_message + type);
			}
		}

		return result;
	}

	public static Type fullyQualifyFunctionSignature(IGResolver r, IGMember m)
	{
		// validate each token separately
		// then move on to the whole sig
		for (IGVariable v : m.m_parameters) 
		{
			try
			{
				r.makeAbsoluteType(v.m_type);
			}
			catch (IGResolverException ex) 
			{
			//	ex.printStackTrace();
				error(v.m_type_token, "Couldn't resolve parameter type: " + v.m_type);
			}
		}
		
		// validate the return type
		try
		{
			r.makeAbsoluteType(m.m_return_type);
		}
		catch (IGResolverException ex) 
		{
			//ex.printStackTrace();
			error(m.m_return_type_token, "Couldn't resolve return type: " + m.m_return_type);
		}
	
		// validate the whole shebang (also includes minimum parameters)
		Type type = m.getDataType(null);
		
		if (null == type) return null;
		
		Type result  = null;
		try
		{
			 result = r.makeAbsoluteType(type);
		}
		catch (IGResolverException ex) 
		{
			ex.printStackTrace();
			
			if (ex.m_type != null) {
				error(m.m_name, "Couldn't resolve type: " + ex.m_type);
			}
			else {
				error(m.m_name, "Failed to resolve function signature 2: " + type);
			}
		}

		return result;
	}

	public static void run(IGPackage root, Vector<IGSource> sources)
	{
		///////////////////////////////////////////////
		// Map the scopes of primitives to a concrete enum
		// this is to facilitate the generation of generics
		// as it gives us a way to know what "functions" can be
		// called on a primitive value
		////////////////////////////////////////////////////
		
		for (IGSource s : sources)
		{	
			try
			{
				String instance_data_type = s.getInstanceDataType().toString();
				
				if (instance_data_type.equals("iglang.util.igShort")) {
					Type.SHORT.m_scope = s;
				}
				if (instance_data_type.equals("iglang.util.igByte")) {
					Type.BYTE.m_scope = s;
				}
				if (instance_data_type.equals("iglang.util.igInteger")) {
					Type.INT.m_scope = s;
					Type.UINT.m_scope = s;
				}
				else if (instance_data_type.equals("iglang.util.igDouble")) {
					Type.DOUBLE.m_scope = s;
				}
				else if (instance_data_type.equals("iglang.util.igBoolean")) {
					Type.BOOL.m_scope = s;
				}
				else if (instance_data_type.equals("iglang.util.igFloat")) {
					Type.FLOAT.m_scope = s;
				}
			}
			catch (RuntimeException ex) {
				util.Log.logError(s.m_expected_component, "", 0, 0);
				throw ex;
			}
		}
		
		/////////////////////////////////////////////////
		// Validate 
		// - extends    (does the class being extended exists)
		// - interfaces (does the class honor the interfaces mentioned)
		// - function types (do the types of each of the functions match something that exists)
		//
		// THESE all produce soft errors 		
		
		for (IGSource s : sources)
		{
			IGResolver r = new IGResolver();
			
			// failed to resolve the packages included by this source file
			if (!r.init(root, s)) {
				continue;
			}
		
			int scope_type = s.getScopeType();
			if (scope_type == IGScopeItem.SCOPE_ITEM_CLASS ||
				scope_type == IGScopeItem.SCOPE_ITEM_INTERFACE ||
				scope_type == IGScopeItem.SCOPE_ITEM_ENUM) 
			{
			
				//////////////////////////////////////////////////////
				// Fully qualify all of the types used... yes
				// and tag the appropriate scope items for each of them
				{	
					if (scope_type != IGScopeItem.SCOPE_ITEM_ENUM) 
					{
						s.m_class_extends = fullyQualify(r, s.m_class_extends, s.m_name, "Failed to find class extended: ");
					}
					
					if (scope_type == IGScopeItem.SCOPE_ITEM_ENUM) 
					{
						if (s.m_enum_extends == null) {
							s.m_enum_extends = Type.INT;
						}
						s.m_enum_extends = 
							fullyQualify(r, s.m_enum_extends, s.m_name, "Failed to find primitive to extend: ");
					}
					
					if (scope_type == IGScopeItem.SCOPE_ITEM_CLASS) 
					{
						Vector<Type> implements_list = s.m_class_implements;
						for (int i = 0; i < implements_list.size(); i++) {
							implements_list.set(i, 
								fullyQualify(r, implements_list.get(i), s.m_name,
								"Failed to find interface implemented: "));
						}
						
						Vector<Token> template_tokens = s.m_template_parameters;
						Vector<Type>  template_types  = s.m_template_types;
						
						for (int i = 0; i < template_tokens.size(); i++) {
							template_types.add(i, 
								fullyQualify(r, Type.get(template_tokens.get(i).m_value), s.m_name));
						}
					}
				
					//////////////
					// Resolve the types of all variables/functions
					for (IGMember m : s.m_members) 
					{
						if (m.m_type == IGMember.TYPE_FUNCTION) 
						{
							// What exactly does this do?
							
							m.m_function_type = fullyQualifyFunctionSignature(r,  m);	
							m.m_return_type   = fullyQualify(r, m.m_return_type, m.m_name, "Failed to resolve return type: ");
						}
						else
						{
							m.m_return_type   = fullyQualify(r, m.m_return_type, m.m_name, "Failed to resolve variable type: ");
						}
						
						if (m.m_type == IGMember.TYPE_FUNCTION) 
						{

							List<IGVariable> vlist = m.getVariables();
							// this check is most likely redundant as we recheck all types as we go
							for (int i = 0; i < vlist.size(); i++) 
							{
								IGVariable v = vlist.get(i);
								Token blame = (v.m_type_token != null) ? v.m_type_token : v.m_name;
							
								//System.err.println("IGVariable : " + v.m_name + " " + v.m_type);
								vlist.get(i).m_type = fullyQualify(r, v.m_type, blame, "Failed to resolve variable type: ");
							}
						}	
					}
				}
			}
		}
		
		///////////////////////
		// Now we know that the externally faceing endpoints for a source are valid
		// So we can now start building up the AST for each of them
		///////////////////////
		
		for (IGSource s : sources)
		{
			IGResolver r = new IGResolver();
			
			// failed to resolve the packages included by this source file
			if (!r.init(root, s)) {
				continue;
			}
			
			s_source = s;	 // <-- for soft errors ?
			
			
			if (s.getScopeType() == IGScopeItem.SCOPE_ITEM_CLASS ||
				s.getScopeType() == IGScopeItem.SCOPE_ITEM_INTERFACE) 
			{
				//////////////////////////////////////////////////
				// Validating the implements list and hierarchy
				// - TODO make objects fail who don't implement all the functions
				//   that they should
				
			
				
				///////////////////////
				// Validate meta data rules
				
				for (IGMember m : s.m_members) 
				{	
					String member_name = m.m_name.m_value;
					
					if (m.m_name.m_value.equals("<constructor>") && !m.m_super_constructor_called) 
					{
						// we need to verify that the previous constructor takes 0 parameters
						
						
						Type extends_type = s.m_class_extends;
						while (extends_type != null)
						{
							//System.out.println("extends_type-type: " + extends_type);
						
							IGSource class_extends = (IGSource)extends_type.m_scope;
						
							boolean found_super_class_constructor = false;
							
							for (IGMember m2 : class_extends.m_members) 
							{
								String extends_member_name = m2.m_name.m_value;
						
								if (m2.m_type != IGMember.TYPE_FUNCTION || !extends_member_name.equals("<constructor>")) {
									continue;
								}
								
								if (m2.getMinParameterCount() >  1) {
									softError(m.m_name,
										 "Constructor must explicitly call super constructor in " +
										extends_type + " since it requires additional arguments");
								}
								
								
								// exit out of this search loop
								extends_type = null;
								found_super_class_constructor = true;
								break;
							}
							
							// its possible that this doesn't even have a constructor
							if (!found_super_class_constructor)
							{
								extends_type = class_extends.m_class_extends;
							}
						}
					
						continue;
					}
					else if (m.m_name.m_value.equals("<constructor>"))
					{
						continue;
					}
					
					
					//////////////////////////////////////////////////
					// Validating the extends list
					
					boolean found_potential_override = false;
					Type extends_type = s.m_class_extends;
					while (extends_type != null)
					{
						IGSource class_extends = (IGSource)extends_type.m_scope;
						
						for (IGMember m2 : class_extends.m_members) 
						{
							String extends_name = m2.m_name.m_value;
						
							if (!member_name.equals(extends_name)) {
								continue;
							}
						
							if (m.getScopeType()  == IGScopeItem.SCOPE_ITEM_FUNCTION &&
								m2.getScopeType() == IGScopeItem.SCOPE_ITEM_FUNCTION) {
						
								if (m2.hasModifier(IGMember.FINAL)) {
									softError(m.m_name, "Function " + member_name +  " attempts to override final method.");
								}
								else if (!m.hasModifier(IGMember.OVERRIDE)) {
									softError(m.m_name, "Function " + member_name +  " must be marked as override");
								}
								else if (m.hasModifier(IGMember.OVERRIDE)) {
									found_potential_override = true;
								}
								
								if (m.getProtection() != m2.getProtection()) {
									softError(m.m_name, "Protection of function " + member_name +  " differs from that being overriden");
								}
								// TODO check asses
								
								if (m.m_return_type != m2.m_return_type) {
									error(m.m_name, "Overriding function must have the same return type");
								}
								if (m.m_parameters.size() != m2.m_parameters.size()) {
									error(m.m_name, "Overriding function must have the same number of parameters");
								}
								for (int i = 0; i < m.m_parameters.size(); i++) {
									IGVariable v0 = m.m_parameters.get(i);
									IGVariable v1 = m2.m_parameters.get(i);
									
									if (v0.m_type != v1.m_type) {
										error(v0.m_type_token, "Overriding function must have the same type of parameters");
									}
								}
								
								m.setModifier(IGMember.VIRTUAL);
								m2.setModifier(IGMember.VIRTUAL);
							}
						}
						
						extends_type = class_extends.m_class_extends;
					}	
					
					if (m.hasModifier(IGMember.OVERRIDE) && !found_potential_override) {
						softError(m.m_name, "Function " + member_name +  " marked override but doesn't override anything");
					}
				}	
				
				// Interfaces
				// Note. The extends chain has been validated at this point
				// validate implements rules
				/////////////////////////////////
				
				Vector<Type> implements_list = s.m_class_implements;
				for (int i = 0; i < implements_list.size(); i++) 
				{
				
					Type implements_type = implements_list.get(i);
					
					while (implements_type != null) 
					{
						
						IGSource mod_implements = (IGSource)implements_type.m_scope;
						
						
						for (IGMember interface_member : mod_implements.m_members) 
						{
							String mod_implements_name = interface_member.m_name.m_value;						
							boolean found = false;
							
											
							for (IGMember m : s.m_members) 
							{	
								String member_name = m.m_name.m_value;

					
								if (!member_name.equals(mod_implements_name)) {
									continue;
								}
					
								if (                m.getScopeType() == IGScopeItem.SCOPE_ITEM_MEMBER_FUNCTION &&
									interface_member.getScopeType() == IGScopeItem.SCOPE_ITEM_MEMBER_FUNCTION) 
								{
									found = true;
									
									//if (m2.hasModifier(IGMember.FINAL)) {
									//	softError(m.m_name, "Interface implementation " + member_name +  " cannot be final.");
									//}
									
									if (!m.hasModifier(IGMember.PUBLIC)) {
										softError(m.m_name, "Interface implementation " + member_name +  " must be public.");
									}
									/*
									else if (!m.hasModifier(IGMember.OVERRIDE)) {
										error(m.m_name, "Function " + member_name +  " must be marked as override");
									}
									*/
							
									m .setModifier(IGMember.VIRTUAL);
									interface_member.setModifier(IGMember.VIRTUAL);
								}
							}
							
							if (!found) { // && (m.m_class_extends == null || s.getExhasFunctionNamed() {
								softError(s.m_name, "Class does not implement function: " + mod_implements_name);
							}
						}
						implements_type = mod_implements.m_class_extends;
					}
				}
				
				if (s.getScopeType() == IGScopeItem.SCOPE_ITEM_INTERFACE) 
				{
					s.m_class_extends = fullyQualify(r, s.m_class_extends, s.m_name);
				}
			}
			else if (s.getScopeType() == IGScopeItem.SCOPE_ITEM_ENUM) 
			{
			
			}
		}
		
		
		for (IGSource s : sources)
		{
			if (IGSettings.s_verbose) {
				System.out.println("Parsing file: " + s.m_src_path);
			}
			
			IGResolver r = new IGResolver();
			
			// failed to resolve the packages included by this source file
			if (!r.init(root, s)) {
				continue;
			}
			
			
			Token [] token_array = s.m_token_list;
			
			// This is perfect
			setTokenList(token_array, 0, s.m_token_list.length, null, r, s);
			
			startNode();
			{
				resolvePackage();
				
				while (true)
				{
					if (peek().m_type == TOK_IMPORT)
					{
						resolveImport(sources);
					}
					else if (peek().m_type == IGLexer.LEXER_NATIVE_BLOCK)
					{
						resolveNative();
					}
					else
					{
						break;
					}
				}
				
				////////////////////////////////
				// CLASS, ENUM, INTERFACE
				////////////////////////////////
				IGNode start_node = startNode();
				{
				
					///////////
					// SOURCE_PREAMBLE
					// [access-modifiers] [type name] [extends] [implements] 
					/////
					IGNode preamble = startNode();
					{
						// access modifiers (public, private, protected, internal, final)
						startNode();
						{
							while (peek().m_type != TOK_CLASS &&
								   peek().m_type != TOK_ENUM && 
								   peek().m_type != TOK_INTERFACE) 
							{
								int peek_type = peek().m_type;
								if (peek_type != TOK_PUBLIC &&
									peek_type != TOK_PRIVATE &&
									peek_type != TOK_PROTECTED &&
									peek_type != TOK_INTERNAL &&
									peek_type != TOK_FINAL    &&
									peek_type != TOK_REF_COUNT) {
								
									error(peek(), "Unknown class descriptor.");	
								}
							
								Token pre_class = read();
								
								if (peek_type == TOK_REF_COUNT) {
									s.addModifier(IGSource.REF_COUNTED);
								}
								
							}
							setNodeType(IGNode.SOURCE_ACCESS_MODIFIERS, s);
						}
						endNode();
					
						// name with template parameters
						IGNode source_name = startNode();
						{					
							Token source_start = read();
							if (source_start.m_type != TOK_ENUM && 
								source_start.m_type != TOK_CLASS && 
								source_start.m_type != TOK_INTERFACE) {
				 
								error(source_start, "Expecting class, enum or interface.");   
							}
							
							if (source_start.m_type != TOK_ENUM && (s.m_modifiers & IGSource.REF_COUNTED) != 0) {
								softError(source_start, "Only enums may be ref counted.");
							}
							
							
							read();
							
							if (peek().m_type == TOK_LT) 
							{
								// Parse templateing parameters
								IGNode source_templates = startNode();
								{
									expect(TOK_LT, "");
									
									while (peek().m_type != TOK_GT) {
									
										Token tok = expect(TOK_ID, "Expecting template parameter id");
										
										if (peek().m_type != TOK_GT) {
											expect(TOK_COMMA, "Expecting ',' separating template arguments");
										}
									}
									
									expect(TOK_GT, "Expecting > terminating template list.");
								
								
									setNodeType(IGNode.SOURCE_TEMPLATES, null);
								}
								endNode();
							}
							
							
							
							// UMM... I'm not eating templated types right here
							setNodeType(IGNode.SOURCE_NAME, s);
						}
						endNode();
						
						// extends
						if (peek().m_type == TOK_EXTENDS) 
						{
							startNode();
								expect(TOK_EXTENDS, "");
					
								Type type = null;
								startNode();
									type = fullyQualify(r, readType(), s.m_name);
									setNodeType(IGNode.NODE_TYPE_TYPE);
								endNode(type);
								
								
							
								setNodeType(IGNode.EXTENDS, s);
							endNode();
						}
				
						// implements
						if (peek().m_type == TOK_IMPLEMENTS) {
							startNode();
								expect(TOK_IMPLEMENTS, "");
					
								while (peek().m_type != TOK_LBRACE) {
									Type type = null;
									startNode();
										type = fullyQualify(r, readType(), s.m_name);
										setNodeType(IGNode.NODE_TYPE_TYPE);
									endNode(type);
									
									if (peek().m_type != TOK_LBRACE) {
										expect(TOK_COMMA);
									}
								}
								setNodeType(IGNode.IMPLEMENTS, s);
							endNode();
						}
					
						setNodeType(IGNode.SOURCE_PREAMBLE, s);
					}
					if (preamble != endNode()) {
						preamble.debug();
						throw new RuntimeException("Mismatch!");
					}
					// end source preamble
					
					
					// this fucks up templating
					expect(TOK_LBRACE, "Expecting {");
					
				
				
				
					for (IGMember m : s.m_members) 
					{
						// stubs imply that it's auto generated under the hood
						if (m.hasModifier(IGMember.STUB)) {
							continue;
						}
					
						//System.err.println("processing: " + m.m_name.m_value);
					
						try
						{
							int node_stack_size = getNodeStackSize();
						
							// reprime the token list iterator
							setTokenList(token_array, 0, s.m_token_list.length, m, r, s);
		
							// fast forward to the start of the member
							skipToAnnotation(s, m);
							//while (peek().m_type != TOK_EOF)
							//{
							//	if (peek().m_annotation_object == m) {
							//		break;
							//	}
							//	read();
							//}
							
							
							parseMember(s, m, r);
							
							if (node_stack_size != getNodeStackSize()) 
							{
								System.err.println
									("unclosed node.  expecting: " + node_stack_size + " found: " + getNodeStackSize());
									
								nodeStackDebug();	
									
								throw new RuntimeException("node stack failure");			
							}
						}
						catch (java.lang.NullPointerException ex)
						{
							System.out.println("what just happened");
							peek().printError("huh");
							error("Something messed up around this position");
							ex.printStackTrace();
							s.m_failed_pass0 = true;
						}
					}
					
					if (s.getScopeType() == IGScopeItem.SCOPE_ITEM_ENUM) {
						if (peek().m_type == TOK_SEMI) {
							expect(TOK_SEMI, "Expecting ;");
						}
					}
					
					expect(TOK_RBRACE, "Expecting } terminating class, enum or interface");
					
					if (s.getScopeType() == IGScopeItem.SCOPE_ITEM_CLASS ) {
						setNodeType(IGNode.CLASS, s);
					}
					else if (s.getScopeType() == IGScopeItem.SCOPE_ITEM_ENUM ) {
						setNodeType(IGNode.NODE_ENUM, s);
					}
					else if (s.getScopeType() == IGScopeItem.SCOPE_ITEM_INTERFACE ) {
						setNodeType(IGNode.NODE_INTERFACE, s);
					}
					else 
					{
						throw new RuntimeException("Invalid Source Type");
					}
					
					// read to the end of the file boy	
					// THIS IS SUCH A HACK
					// SHOULD REALLY JUST ITERATE THROUGH MEMBERS AGAIN
					s_token_index_end = s.m_token_list.length;		
					//while (peek() != null) {
					//	read();
					//}
				}
				IGNode node = endNode();
				
				
				if (node != start_node) {
					node.debug();
					throw new RuntimeException("huh how did this happen: " + node.m_start_index + " vs " + 
							start_node.m_start_index);
				}
			
				
				setNodeType(IGNode.SOURCE, s);
				
				
			}
			
			// this node represents the end of the file (aka the source)
			s.m_root_node = endNode();
		}
		
		if (IGSettings.s_verbose) {
			System.err.println("validation finished");
		}
	}
	
	/*
	 * Resolve a package in the form
	 *   package iglang.something;
	 */
	
	final static void resolvePackage()
	{
		startNode();
		{
			expect(TOK_PACKAGE, "Expecting package");
			while (peekTokenType() != TOK_SEMI) {
				Token t = read();
				//if (t.m_type == IGConsts.TOK_INTERNAL) {
				//	t.m_type = IGConsts.TOK_ID;
				//}
			}
			expect(TOK_SEMI, "Expecting ; following import");
			
			
			setNodeType(IGNode.PACKAGE);
		}
		endNode();
	}
	
	/*
	 * Resolve a statement in the form.
	 *   import iglang.Object;
	 *   import iglang.*;
	 */
	
	final static void resolveNative()
	{
		startNode();
		{
			expect(IGLexer.LEXER_NATIVE_BLOCK, "Expecting native block");
			setNodeType(IGNode.NATIVE_BLOCK);
		}
		endNode();
	}
	
	
	final static void resolveImport(Vector<IGSource> sources)
	{
		// TODO verify that the import signature is actually correct
	
		IGScopePath sp = new IGScopePath();
	
		startNode();
			Token import_token = expect(TOK_IMPORT, "Expecting package");
			
			while (peekTokenType() != TOK_SEMI) 
			{
				Token tok = read();
				
				if (tok.m_type == TOK_MUL) {
				
				
					boolean found = false;
					
					// recursive glob import
					if (peekTokenType() == TOK_MUL) {
						// throw away the extra '*'
						read();
						
						for (IGSource source : sources) {
							if (source.m_package.startsWith(sp)) {
								found = true;
							}
						}
					}
					// standard glob import
					else {
						for (IGSource source : sources) {
							if (source.m_package.equals(sp)) {
								found = true;
							}
						}
					}
					
					if (!found) {
						softError(import_token, "Package doesn't exist");
					}
				}
				
				sp.add(tok);
				
				if (peekTokenType() == TOK_SEMI && tok.m_type != TOK_MUL) {
					boolean found = false;
					for (IGSource source : sources) {
						if (source.m_scope_path.equals(sp)) {
							found = true;
						}
					}
					
					if (!found) {
						softError(import_token, "Class/Interface/Enum doesn't exist");
					}					
				}
				
				
//				sp = new IGScopePath(sp, tok);

				// if its a glob or it actually points to a source
				if (tok.m_type == TOK_MUL) {
					break;
				}		
						
				if (peekTokenType() != TOK_SEMI) {
					expect(TOK_DOT, "Expecting separator");
				}
			}
			expect(TOK_SEMI, "Expecting ; following import");
		
			setNodeType(IGNode.IMPORT);
		endNode();
		
		
		// TODO fill out the rest of this logic
		// should check that either the imports exactly match
		//for (IGSource s : sources) {
		//
		//}
		//
		//softError(import_token, "Import path doesn't exist");
	}
}