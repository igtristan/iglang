/*
Copyright (c) 2014-2017 Incubator Games Ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

package pass1;

import java.util.*;

public final class Type
{
	public static final int STORAGE_8 		 = 1;
	public static final int STORAGE_16 		 = 2;
	public static final int STORAGE_32 		 = 4;
	public static final int STORAGE_32F      = 5;
	public static final int STORAGE_64 		 = 8;
	public static final int STORAGE_OBJ 	 = 16;
	public static final int STORAGE_FUNC_PTR = 17;
	public static final int STORAGE_32_REF   = 18;
	
	private static IGTypeDatabaseEntry s_database = new IGTypeDatabaseEntry();
	private static ArrayList<Type> s_all_types = new ArrayList<Type>();
	// this must appear before the types are shown
	//private static Hashtable<String, Type> s_types = new Hashtable<String, Type>();
	
	// constants for primitives and common types
	public static final Type  INT            = Type.get("int");
	public static final Type UINT            = Type.get("uint");
	public static final Type VOID            = Type.get("void");
	public static final Type DOUBLE          = Type.get("double");
	public static final Type BOOL            = Type.get("bool");
	public static final Type SHORT			 = Type.get("short");
	public static final Type FLOAT           = Type.get("float");
	public static final Type BYTE            = Type.get("byte");
	public static final Type NULL            = Type.get("iglang.null");
	public static final Type DELEGATE_NULL   = Type.get("iglang.null@delegate");
	public static final Type STRING          = Type.get("iglang.String");
	public static final Type OBJECT          = Type.get("iglang.Object");
	public static final Type FUNCTION        = Type.get("iglang.Function");
	public static final Type STATIC_FUNCTION = Type.get("iglang.StaticFunction");
	public static final Type MEMBER_FUNCTION = Type.get("iglang.MemberFunction");	
	
	
	static String [] s_int_255 = new String[256];
	static {
		for (int i = 0; i <= 255; i++) {
			s_int_255[i] = "" + i;
		}
	}
	
	public final IGValueRange getDefaultValueRange() {
		if (this == INT || this == UINT) { return IGValueRange.INT_0;      }
		if (this == BYTE || this == SHORT) { return IGValueRange.INT_0;    }
		if (this == DOUBLE)              { return IGValueRange.DOUBLE_0;   }
		if (this == FLOAT)                { return IGValueRange.DOUBLE_0;  }
		if (this == BOOL)				 { return IGValueRange.BOOL_FALSE; }
		if (isEnum()) {
			return getEnumPrimitive().getDefaultValueRange();
		}
		return IGValueRange.POINTER_NULL;
	}

	public final IGValueRange getAnyValueRange() {
		if (this == INT || this == UINT) { return IGValueRange.INT_ANY;      }
		if (this == BYTE || this == SHORT) { return IGValueRange.INT_ANY;   }
		if (this == DOUBLE)              { return IGValueRange.DOUBLE_ANY;   }
		if (this == FLOAT)               { return IGValueRange.DOUBLE_ANY;   }
		if (this == BOOL)				 { return IGValueRange.BOOL_ANY; }
		if (isEnum()) {
			return getEnumPrimitive().getAnyValueRange();
		}
		return IGValueRange.POINTER_ANY;
	}


	/**
	 * used by asm target to determine how a variable with this
	 * type should be stored
	 */


	public final int getStorageClass() {
		// crap it looks like the key is primed by the toString method
		//this.toString();
	
		// special hacks
		//if (m_primary.equals("iglang.util.igByte")) { return STORAGE_8; }
		//if (m_primary.equals("iglang.util.igShort")) { return STORAGE_16; }
		if (this == BYTE)  { return STORAGE_8; }
		if (this == SHORT) { return STORAGE_16; }
		if (m_primary.equals("iglang.util.igDelegate")) { return STORAGE_FUNC_PTR; }
		if (this == INT || this == UINT) { return STORAGE_32; }
		if (this == BOOL) { return STORAGE_8; }
		if (this == DOUBLE) { return STORAGE_64; }
		if (this == FLOAT)  { return STORAGE_32F; }
		if (m_primary.equals("Function") || m_primary.equals("StaticFunction")) {
			return STORAGE_FUNC_PTR;
		}
		if (isEnum()) 
		{
			IGSource s = (IGSource)m_scope;
			if ((s.m_modifiers & IGSource.REF_COUNTED) != 0) {
				return STORAGE_32_REF;
			}
			else {
				Type prim = getEnumPrimitive();
				return prim.getStorageClass();
			}
		}
		
		return STORAGE_OBJ;
	}



	// the key used to place this into the map
	public String       m_key = null;
	private byte        m_required;			// how many of the parameters are required
	public String 		m_primary;
	public Type   		m_left;
	public Type   		m_right;
	public Type []      m_types;
	
	// this is updated before final code generation
	
	public boolean      m_scope_overridden = false;			// not used
	public IGScopeItem  m_scope = IGScopeItem.PRIMITIVE;
	
	
	/**
	 * Create an array of all types that have a source that has templating parameters.
	 */
	
	
	public final static void getTemplatedTypes(ArrayList<Type> types)
	{
		for (Type value : s_all_types)
		{
			if (value.m_types != null) {
				continue;
			}
		
			if (value.m_primary.equals("<wrapper>")) {
				continue;
			}
			
			if (value.m_scope instanceof IGSource) {
				IGSource s = (IGSource)value.m_scope;
				
				if (value.m_left != null && s.isTemplatedClass()) {
					types.add(value);
				}
			}	
		}
	
	
	}
	
	private static ArrayList<Object> s_type_vectors = new ArrayList<Object>();
	
	// not thread safe
	public static final ArrayList<Type> allocTempVector() {
	
		int sz = s_type_vectors.size();
		if (sz == 0) {
			return new ArrayList<Type>();
		}
		else {
			return (ArrayList<Type>) s_type_vectors.remove(sz - 1);
		}
	}
	
	// not thread safe
	public static final void freeTempVector(ArrayList<Type> type) {
		type.clear();
		s_type_vectors.add(type);
	}
	
	/**
	 * Return a new type that is represented by
	 * all instances of the type "pattern" being replaced with "repl"
	 * THIS IS FOR TEMPLATED TYPE RESOLVING
	 * iglang.Array<iglang.T1>    with iglang.T1 and iglang.String yields
	 *   iglang.Array<iglang.String>
	 */
	public final Type replace(Type pattern, Type repl)
	{
		if (this == pattern) return repl;
		
		Type new_type = null;
		
		
		if (m_left != null) {
			new_type = Type.get(m_primary, 
				m_left == null ? null : m_left.replace(pattern, repl),
				m_right == null ? null : m_right.replace(pattern, repl)
			);
									
		}
		else if (m_types != null) 
		{
			int type_count = m_types.length;
			ArrayList<Type> list = Type.allocTempVector();	//new Vector<Type>();
			for (int i = 0; i < type_count; i ++) {
				list.add(m_types[i].replace(pattern, repl));
			}
			
			new_type = Type.get(list, getMinimumParameters());
			Type.freeTempVector(list);
		}
		else
		{
			
			new_type = this;
		}
		
		new_type.m_scope = this.m_scope;
		return new_type;
	}
	
	
	/**
	 * Does this type represent an enum
	 */
	public final boolean isEnum()
	{
		if (m_scope instanceof IGSource) {
			if (  ((IGSource)m_scope).m_type == IGScopeItem.SCOPE_ITEM_ENUM)
			{
				return true;
			}
		}
		
		return false;
	}
	
	/**
	 * Is this type an int or an unsigned int
	 */ 
	
	public final boolean isIntish() {
		return this == Type.INT ||
			   this == Type.UINT ||
			   this == Type.SHORT ||
			   this == Type.BYTE;
	}
	
	
	/*
	 * Is this type intish (int or unit).  Or does it correspond to an enum
	 * that extends int
	 */
	
	public final boolean isIntishOrIntishEnum() {
		if (isIntish()) {
			return true;
		}
		
		if (m_scope instanceof IGSource) 
		{
			IGSource source = (IGSource)m_scope;
			if (source.m_type == IGScopeItem.SCOPE_ITEM_ENUM && source.m_enum_extends.isIntish())
			{
				return true;
			}
		}
		
		return false;
	}
	
	public final boolean isIntishEnum() 
	{
		if (m_scope instanceof IGSource) 
		{
			IGSource source = (IGSource)m_scope;
			if (source.m_type == IGScopeItem.SCOPE_ITEM_ENUM && source.m_enum_extends.isIntish())
			{
				return true;
			}
		}
		
		return false;
	}
	
	
	public final boolean isFunctionWrapper() {
		return m_primary.equals("<wrapper>");
	}
	
	public final Type unwrap() {
		if (!isFunctionWrapper()) {
			throw new RuntimeException("Not a wrapped function");
		}
		
		return m_right;
	}
	
	public final boolean isNamespace() {
		if (m_primary.startsWith("package:") ||
			m_primary.startsWith("source:")) {
			return true;	
		}
		else
		{
			return false;
		}
	
	}

	public final static Type get(String base)
	{
		IGTypeDatabaseEntry result = s_database.poke(base);
		if (result.m_value != null) {
			return result.m_value;
		}	
		
		Type type = new Type();
		type.m_primary = base;
		type.m_left    = null;
		type.m_right   = null;
		type.m_types   = null;
		type.m_required = 0;

		result.m_value = type;
		s_all_types.add(type);		
		//s_types.put(base, type);
		
		return type;
	}
	
	public final static Type get(String base, Type left, Type right)
	{
		/*
		String key;
		{
			StringBuilder sb = new StringBuilder();
			sb.append(base).append("<").append(left);
			if (right != null) {
				sb.append(",").append(right);		
			}
			sb.append(">");
		
			key = sb.toString();
		}
		*/
		
		
		IGTypeDatabaseEntry result = s_database.poke(base).poke('<').poke(left);
		if (right != null) {
			result = result.poke(',').poke(right);
		}	
		result = result.poke('>');
		
		if (result.m_value != null) {
			return result.m_value;
		}
		
		
		
		
		//if (s_types.containsKey(key)) return s_types.get(key);
		
		Type type = new Type();
		type.m_primary = base;
		type.m_left    = left;
		type.m_right   = right;
		type.m_types   = null;
		type.m_required = 0;
		
		result.m_value = type;
		s_all_types.add(type);		
		//s_types.put(key, type);
		return type;
	}
	
	public final static Type get(ArrayList<Type> types) {
		return get(types, types.size());
	}
	
	
	public static final int parsePositiveSubstringInt(String s, int start_index, int end_index)
	{
		int buffer = 0;
		while (start_index < end_index) {
			buffer = (s.charAt(start_index) - '0') + 10 * buffer;
			start_index ++;
		}
		return buffer;
	}
	
	public final  int getMinimumParameters() {
		// throw away the @ sign and grab the number after it
		return m_required;	//parsePositiveSubstringInt(m_primary, 1, m_primary.length());	//Integer.parseInt(m_primary.substring(1));
	}
	
	public final  int getMaximumParameters() {
		return m_types.length;
	}

	public final static Type get(Type [] array, int start, int end) {
		ArrayList<Type> tmp = new ArrayList<Type>();
		for (int i = start; i < end; i++) {
			tmp.add(array[i]);
		}
		return get(tmp, tmp.size());
	}
	
	
	public final static Type get(ArrayList<Type> types, int required_count) 
	{
		final int types_sz = types.size();
		
		// drill into the type database
		IGTypeDatabaseEntry result = s_database.poke('@').poke(Integer.toString(required_count)).poke('<');
		for (int i = 0; i < types_sz; i++) 
		{
			if (i != 0) {
				result = result.poke(',');
			}
			Type resolved = types.get(i);
			if (resolved == null) {
				throw new RuntimeException("Type in list is null");
			}
			result = result.poke(resolved.toString());
		}	
		result = result.poke('>');
		
		if (result.m_value != null) {
			return result.m_value;
		}
		
		Type type = new Type();
		type.m_primary = "@"; // + required_count;
		type.m_required = (byte)required_count;
		type.m_left    = null;
		type.m_right   = null;
		type.m_types   = new Type[types_sz];
		types.toArray(type.m_types);
		result.m_value = type;
		s_all_types.add(type);	
		//s_types.put(key, type);
		return type;
	}
	
	
	public final boolean isVoid() {
		return this == VOID;
		//return this == Type.VOID;
	}
	
	/*
	 * Return true if math can be performed with the type in question.
	 * Allowed arithmetic types are: int, uint and double
	 */
	
	public final boolean isArithmetic() 
	{
		return this == INT || this == UINT || this == DOUBLE;
	/*
		if (m_left != null || m_right != null) 
			return false;
			
		if (m_primary.equals("int")  ||
			m_primary.equals("uint") ||
			m_primary.equals("double")) {
		
			return true;	
		}
		
		return false;
*/
	}
	
	
	/*
	 * Return true if the type represents something that is copy by value
	 * and can be contained in a register.
	 * Allowed primitive types are: bool, int, uint, double and void
	 */
	
	public final boolean isPrimitive()
	{
		return  this == BOOL   || 
				this == INT    || 
				this == UINT   || 
				this == DOUBLE ||
				this == FLOAT  || 
				this == VOID   || 
				this == BYTE   || 
				this == SHORT;
		/*
		if (m_left != null || m_right != null) 
			return false;
			
		if (m_primary.equals("bool") ||
			m_primary.equals("int")  ||
			m_primary.equals("uint") ||
			m_primary.equals("double") ||
			m_primary.equals("void")) {
		
			return true;	
		}
		
		return false;
		*/
	}
	
	public final boolean isFunctionPointer() {
		if (m_primary.equals("Function") ||
			m_primary.equals("StaticFunction") )
		{
			
			return true;
		}
		
		return false;
	}
	
	/* Is any logical checking require to see if we can cast one type to another
	 *	-  double => double is fine
	 *   
	 */
	
	public  static final int WORK_INCOMPATIBLE        = -1;
	public  static final int WORK_NONE                = 0;
	public  static final int WORK_FUNCTION_UNWRAP     = 1;
	
	public  static final int WORK_OBJECT_CAST         = 3;
	
	public  static final int WORK_PRIM_CAST           = 4;
	public  static final int WORK_PRIMITIVE_CAST      = 4;
	
	// casting an object to a string
	public  static final int WORK_STRING_CAST         = 5;
	
	// converting an enum to a value
	public  static final int WORK_ENUM_TO_STRING      = 6;
	
	// casting a string to an (int, double, bool)
	public  static final int WORK_STRING_TO_PRIMITIVE = 7;
	public  static final int WORK_STRING_TO_PRIM      = 7;
	
	// casting a primitive (int, double, bool) to a string
	public  static final int WORK_PRIM_TO_STRING      = 8;
	public  static final int WORK_PRIMITIVE_TO_STRING = 8;
	
	// casting an enum of a same storage class to another
	public static final  int WORK_ENUM_CAST           = 9;
	public static final  int WORK_NULL_TO_DELEGATE_NULL = 10;
	
//	public final static final int 
	
	private static final int [] s_type_switch_explicit = 
	{ 			
		/*                                 SOURCE                                                     */
		/*dst       bool                 int                  uint                 double               string   */
		/*bool*/	WORK_NONE,           WORK_PRIM_CAST,      WORK_PRIM_CAST,      WORK_PRIM_CAST, 	    WORK_STRING_TO_PRIM,
		/*int*/		WORK_NONE, 			 WORK_NONE,           WORK_NONE,    	   WORK_PRIM_CAST,      WORK_STRING_TO_PRIM,
		/*uint*/	WORK_NONE,      	 WORK_NONE,			  WORK_NONE,           WORK_PRIM_CAST,      WORK_STRING_TO_PRIM,
		/*double*/	WORK_PRIM_CAST,      WORK_PRIM_CAST,      WORK_PRIM_CAST, 	   WORK_NONE,           WORK_STRING_TO_PRIM,
		/*String*/	WORK_PRIM_TO_STRING, WORK_PRIM_TO_STRING, WORK_PRIM_TO_STRING, WORK_PRIM_TO_STRING, WORK_NONE
	};
	
	private static final int [] s_type_switch_implicit = 
	{ 			
		/*                                 SOURCE                                                     */
		/*dst       bool                 int                  uint                 double               string   */
		/*bool*/	WORK_NONE,           WORK_INCOMPATIBLE,   WORK_INCOMPATIBLE,   WORK_INCOMPATIBLE, 	WORK_INCOMPATIBLE,
		/*int*/		WORK_INCOMPATIBLE, 	 WORK_NONE,           WORK_NONE,    	   WORK_PRIM_CAST,      WORK_INCOMPATIBLE,
		/*uint*/	WORK_INCOMPATIBLE,   WORK_NONE,			  WORK_NONE,           WORK_PRIM_CAST,      WORK_INCOMPATIBLE,
		/*double*/	WORK_INCOMPATIBLE,   WORK_PRIM_CAST,      WORK_PRIM_CAST, 	   WORK_NONE,           WORK_INCOMPATIBLE,
		/*String*/	WORK_INCOMPATIBLE,   WORK_INCOMPATIBLE  , WORK_INCOMPATIBLE,   WORK_INCOMPATIBLE,   WORK_NONE
	};

	
	// todo add the notion of explicit
	// var x : String = "1";
	// ie    int(x)
	// vs
	// var y : int = x;		// implicit casts should be MUCH more restrictive... ie casting back to an enum SHOULDN'T WORK
	public final int workRequiredToCastTo(Type other, boolean explicit)
	{
		// wouldn't comparison actually be a bidirectional application of these rules?
		
	
		Type right = this;
		Type left = other;
		
		if (right == BYTE || right == SHORT) { right = INT; }
		if (left == BYTE || left == SHORT)   { left = INT; }
		if (right == FLOAT) { right = DOUBLE; }
		if (left  == FLOAT) { left = DOUBLE; }
		
		// same type, thus we don't actually need to do anything
		if (left == right) {
			return WORK_NONE;
		}
		
		// is the left and the right side (bool, int, uint, double, void)
		final boolean left_p  = left.isPrimitive();
		final boolean right_p = right.isPrimitive();
		
		final boolean left_enum  = !left.isPrimitive()  && left.isEnum();
		final boolean right_enum = !right.isPrimitive() && right.isEnum();
		
		
		//System.err.println("left vs right: " + left + " vs " + right);
		
		
		final Type type_int = Type.INT;
		final Type type_uint = Type.UINT;
		final Type type_double = Type.DOUBLE;
		final Type type_bool   = Type.BOOL;
		final Type type_string = Type.STRING;
		
		// tag both as being objects for the time being
		int left_switch = 0x1000;
		int right_switch = 0x1000;
		

		
		
		if      (left == type_bool)   { left_switch = 0; }
		else if (left == type_int)    { left_switch = 1; }
		else if (left == type_uint)   { left_switch = 2; }
		else if (left == type_double) { left_switch = 3; }
		else if (left == type_string) { left_switch = 4; }
		else if (left.isEnum() && left.getEnumPrimitive() == type_bool)   { left_switch = 0x10; }  
		else if (left.isEnum() && left.getEnumPrimitive() == type_int)    { left_switch = 0x11; }  
		else if (left.isEnum() && left.getEnumPrimitive() == type_uint)   { left_switch = 0x12; }  
		else if (left.isEnum() && left.getEnumPrimitive() == type_double) { left_switch = 0x13; }  
				
		if      (right == type_bool)   { right_switch = 0; }
		else if (right == type_int)    { right_switch = 1; }
		else if (right == type_uint)   { right_switch = 2; }
		else if (right == type_double) { right_switch = 3; }
		else if (right == type_string) { right_switch = 4; }
		else if (right.isEnum() && right.getEnumPrimitive() == type_bool)   { right_switch = 0x10; }  
		else if (right.isEnum() && right.getEnumPrimitive() == type_int)    { right_switch = 0x11; }  
		else if (right.isEnum() && right.getEnumPrimitive() == type_uint)   { right_switch = 0x12; }  
		else if (right.isEnum() && right.getEnumPrimitive() == type_double) { right_switch = 0x13; }  
		
		// both left and right sides are enums 
		if ((left_switch & 0x10) != 0 && (right_switch & 0x10) != 0) {
		
			// obviously not the same enum but they have the same storage class
			if (left_switch == right_switch) {
				return explicit ? WORK_ENUM_CAST : WORK_INCOMPATIBLE;
			}
			return WORK_INCOMPATIBLE;
		}
		// left side is an enum and right side is a string
		else if ((left_switch & 0x10) != 0 && right == type_string) {
			return WORK_INCOMPATIBLE;
		}
		// right side is an enum and left side is a string
		// - this will result in an enum toString
		else if ((right_switch & 0x10) != 0 && left == type_string) {
			return explicit ? WORK_ENUM_TO_STRING : WORK_INCOMPATIBLE;
		}
		
		// toss out the enum tags
		left_switch  &= ~0x10;
		right_switch &= ~0x10;
		
		// if this cast doesn't involve objects on either side then just return the value from
		// the table
		if (left_switch != 0x1000 && right_switch != 0x1000) {
			int work = explicit ?
						s_type_switch_explicit[left_switch *5 + right_switch] : 
						s_type_switch_implicit[left_switch *5 + right_switch];

			return work;
		}
		
		if (right.isFunctionWrapper()) {
			//System.err.println("fw left vs right: " + left + " vs " + right);
			right = right.unwrap();
			
			if (left == right) {
				return WORK_FUNCTION_UNWRAP;
			}
			
			// make sure that both are Function or StaticFunction as their primary values
			if (!left.m_primary.equals("Function") &&!left.m_primary.equals(right.m_primary)) {
				return WORK_INCOMPATIBLE;
			}

			// verify that the return types are the same		
			if (right.m_right != left.m_right) {
				return WORK_INCOMPATIBLE;
			}
			//if (right.m_right.workRequiredToCastTo(left.m_right, explicit) != WORK_NONE) {
			//	return WORK_INCOMPATIBLE;
			//}
			
			Type [] right_fn_types = right.m_left.m_types;
			Type [] left_fn_types  = left.m_left.m_types;

			// verify that the number of parameters are identical
			if (right_fn_types.length != left_fn_types.length) {
				return WORK_INCOMPATIBLE;
			}
			
			// verify that 
			for (int i = 0; i < left_fn_types.length; i++) {
				if (left_fn_types[i] != right_fn_types[i]) {
					return WORK_INCOMPATIBLE;
				}	
				//if (right_fn_types[i].workRequiredToCastTo(left_fn_types[i], explicit) != WORK_NONE) {
				//	return WORK_INCOMPATIBLE;
				//}
			}
			
			return WORK_FUNCTION_UNWRAP;
		}
		
		
		if (right == NULL) {
			if (left.isClassInstance() || left.isInterfaceInstance()) {
				return WORK_NONE;
			}
			else if (left.isFunctionPointer()) {
				return WORK_NULL_TO_DELEGATE_NULL;
			}
			else
			{
				return WORK_INCOMPATIBLE;
			}
		}
		else if  (right == DELEGATE_NULL) {
			if (left.isFunctionPointer()) {
				return WORK_NONE;
			}
			else
			{
				return WORK_INCOMPATIBLE;
			}
		}
		
		
		//WORK_NULL_TO_DELEGATE_NULL
		
		// what about ints etc
		
		if ((left.isClassInstance() || left.isInterfaceInstance()) &&
		    (right.isClassInstance() || right.isInterfaceInstance()))
		{
			if (m_scope instanceof IGSource) 
			{
				IGSource source = (IGSource)m_scope;
				
				
				
				
			
				if (source == null) {
					//System.err.println("huh? " + this + " " + other);
					throw new RuntimeException("work failure");
				}
			
				// casese for Vector<Enum extends int>  explicit casting 
				// to         Vector<int>
				
				if (explicit && other.m_primary.equals(right.m_primary)) 
				{
					if (other.isTemplatedClassInstance()) 
					{
						if (
						(right.m_left == null ||
						 right.m_left.workRequiredToCastTo(other.m_left, false) == WORK_NONE)
						 &&
						(right.m_right == null ||
						 right.m_right.workRequiredToCastTo(other.m_right, false) == WORK_NONE)
						)
						{
							return WORK_NONE;
						}
					}
				}
			
				if (source.assignable(right, other)) {
					return WORK_NONE;
				}
				else
				{
					return explicit ? WORK_OBJECT_CAST : WORK_INCOMPATIBLE;
				}
			}
		}
		
		//System.err.println("huh2? " + this + " " + other);
		return WORK_INCOMPATIBLE;
	}
	
	/**
	 * Returns true if this type contains any templating parameters
	 */
	
	public final boolean containsTemplatingParameters() {
		if (m_primary.equals("iglang.T1") || m_primary.equals("iglang.T2")) {
			return true;
		}
	

		if (m_left != null && m_left.containsTemplatingParameters()) {
			return true;
		}
	
		if (m_right != null && m_right.containsTemplatingParameters()) {
			return true;
		}
		
		if (m_types != null) {
			for (Type t : m_types) {
				if (t.containsTemplatingParameters()) {
					return true;
				}
			}
		}
		
		return false;
	
	}
	
	/**
	 * If this type is a class instance, a interface instance, a function pointer or itself is null
	 * then you can compare it to null.
	 */
	
	public final boolean isNullComparable() 
	{
		if (isClassInstance() || isInterfaceInstance() || isFunctionPointer() || this == Type.NULL) {
			return true;
		}
		else {
			return false;
		}
	}
	
	/**
	 * Does this type represent an instance of a class
	 */
	
	public final boolean isClassInstance() {
		if (m_scope != null && 
			m_scope instanceof IGSource && 
			m_scope.getScopeType() == IGScopeItem.SCOPE_ITEM_CLASS)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	/**
	 * Does this type represent an instance of a templated class
	 * ie Map<String,String>
	 */
	
	public final boolean isTemplatedClassInstance() {
		if (m_scope != null && 
			m_scope instanceof IGSource && 
			m_scope.getScopeType() == IGScopeItem.SCOPE_ITEM_CLASS)
		{
		
			IGSource  s = (IGSource)m_scope;
			if (s.getTemplatedParameterCount() > 0) {
				return true;
			}
		}
		
		return false;
	}
	
	/*
	 * Does this type represent an enum instance.
	 */
	
	public final boolean isEnumInstance() {
		if (m_scope != null && 
			m_scope instanceof IGSource && 
			m_scope.getScopeType() == IGScopeItem.SCOPE_ITEM_ENUM)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	/*
	 * Get the primitive type that the enum instance uses for storage
	 */
	
	public final Type getEnumPrimitive() 
	{
		if (m_scope != null && 
			m_scope instanceof IGSource && 
			m_scope.getScopeType() == IGScopeItem.SCOPE_ITEM_ENUM)
		{
			Type t = ((IGSource)m_scope).m_enum_extends;
			return t;
		}
		else
		{
			return null;
		}	
	}
	
	/**
	 * Return true if this type represents an instance of an Interface
	 */
	
	public final boolean isInterfaceInstance() {
		if (m_scope != null && 
			m_scope instanceof IGSource && 
			m_scope.getScopeType() == IGScopeItem.SCOPE_ITEM_INTERFACE)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	/**
	 * Return true if this type represents an object (interface or class)
	 */
	
	public final boolean isObjectInstance()
	{
		if (m_scope != null && 
			m_scope instanceof IGSource && 
			(m_scope.getScopeType() == IGScopeItem.SCOPE_ITEM_INTERFACE ||
			 m_scope.getScopeType() == IGScopeItem.SCOPE_ITEM_CLASS))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
		
	/*
	 * Can this type be coerced into the other type.
	 * This determines if the cast can be made in terms of ig language rules.
	 * @param other - they type we want to coerce this type into, represents the lvalue in most cases
	 * 	thus we look for numeric compatibility, or check if we're a subtype of the other type
	 */ 
	
	// THIS NEEDS TO BE PHASED OUT
	// THIS IS STILL USED IN 2  separate areas
	public final boolean assignable(Type other)
	{
		// can this type be coerced into the other type
	
	
		// The type checking is working besides the cases of enums
		// where they expect a specific type of value
		//return true;
	
	

		
		Type right = this;
		Type left = other;
		
		// perfectly acceptable to unwrap functions being assigned
		if (right.isFunctionWrapper()) {
			right = right.unwrap();
		}
		
		if (right == left) return true;
				
		// enums are just treated as ints for now
		if (left.isEnumInstance() && right.isEnumInstance()) {
			// these enums are not of the same type
			return false;
		}
		else if 
		(
			(left == Type.INT  || left == Type.DOUBLE || left == Type.UINT   || left.isEnum()) &&
			(right == Type.INT || right == Type.DOUBLE || right == Type.UINT || right.isEnum())
		)
		{
			// valid internal type conversions
			// although these would be things java would complain about
			return true;
		}
		else if (right == Type.NULL   && !left.isPrimitive())
		{
			// assigning NULL to an object or a function pointer
			return true;
		}
		else
		{
			if (!(m_scope instanceof IGSource)) {
				// this only seem to be triggering on the templated parameter
				//System.out.println(this + " is not a scope.");
				return false;
			}
			
			IGSource source = (IGSource)m_scope;
			return source.assignable(right, other);
		}
	
	} 
	
	public final boolean isTuple() {
		return m_primary.equals("tuple");
	}


	public Type [] getTupleTypes() {	
		if (! m_primary.equals("tuple")) {
			Type [] lonely = new Type[1];
			lonely[0] = this;
			return lonely;
		}
		return m_left.m_types;
	}

	public int getTupleCount() {
		if (! m_primary.equals("tuple")) {
			return 1;
		}
		return m_left.m_types.length;
	}

	public boolean isTupleDoubles() {
		Type [] types = getTupleTypes();
		for (int i = types.length - 1; i >= 0; i--) {
			if (types[i] != Type.DOUBLE) {
				return false;
			}
		}
		return true;
	}


	public final boolean isBuiltIn()
	{
		if (m_primary.equals("Function") ||
			m_primary.equals("StaticFunction") ||
			m_primary.equals("tuple"))
		{
			
			return true;
		}
		
		return false;
	}
	
	public  String fancyToString()
	{	
		if (m_primary.equals("<wrapper>")) {
			return m_right.toTypeString();
		}
		
		return toTypeString();
	}
	
	
	/**
	 * toString used by Assembly target, so variable types
	 * this just has different behaviour for function types
	 * usage: infrequent
	 */
	 
	 
	public final String toTypeString()
	{
		if (m_left != null)  
		{
			StringBuilder sb = new StringBuilder();
			toTypeString(sb);
			return sb.toString();
		}
		else 
		{
			return m_primary;
		}	
	} 
	
	public final void toTypeString(StringBuilder sb)
	{
		if (m_primary.equals("Function") ||
			m_primary.equals("StaticFunction"))  {
		
			sb.append(m_primary);
			sb.append(m_required);
			sb.append("<(");
			
			Type left = m_left;
			int  left_types_sz = left.m_types.length;
			for (int i = 0; i < left_types_sz; i++) {
				if (i != 0) {
					sb.append(",");
				}
				
				left.m_types[i].toTypeString(sb);
			}
			
			sb.append("):");
			m_right.toTypeString(sb);
			sb.append(">");
		}
		else if (m_left != null && m_right != null) {
			sb.append(m_primary).append("<");
				m_left.toTypeString(sb);
			sb.append(",");
				m_right.toTypeString(sb);
			sb.append(">");
		}
		else if (m_left != null) {
			sb.append(m_primary).append("<");
				m_left.toTypeString(sb);
			sb.append(">"); 
		}
		else {
			sb.append(m_primary);
		}	

	}
	
	//Function<@0<>,void>
	//<wrapper><void,StaticFunction<@0<>,void>>
	
	
	public final String prettyFunctionSignature()
	{
		if (m_left != null && m_right != null) {
			return "(" +  m_left.toPrettyString() +  "):" + m_right.toPrettyString();
		}
		else {
			return "" + this;
		}
	}
	
	public final String toPrettyString()
	{
		if (m_primary.equals("Function")) {
		 	return "Function<(" +  m_left.toPrettyString() +  "):" + m_right.toPrettyString() + ">" ;
		}
		else if (m_primary.equals("StaticFunction")) {
		 	return "StaticFunction<(" +  m_left.toPrettyString() +  "):" + m_right.toPrettyString() + ">"; 
		}
		else if (m_primary.equals("<wrapper>")) {
		
			String pri = m_right.m_primary;
			Type   left = m_right.m_left;
			Type   right = m_right.m_right;
		
			if (pri.equals("Function")) {
		 		return "function (" +  left.toPrettyString() +  "):" + right.toPrettyString() + "" ;
			}
			else if (pri.equals("StaticFunction")) {
		 		return "static function (" +  left.toPrettyString() +  "):" + right.toPrettyString() + ""; 
			}
			else {
				throw new RuntimeException("Invalid wrapper");
			}
		}
		else if (m_types != null) 
		{
			StringBuilder sb = new StringBuilder();
			//sb.append(m_primary)
			//sb.append("<");
			for (int i = 0; i < m_types.length; i++) {
				if (i != 0) {
					sb.append(",");
				}
				sb.append(m_types[i].toPrettyString());
			}	
			//sb.append(">");
			
			return sb.toString();
		}
		else if (m_left != null && m_right != null) {
			return  m_primary + "<" + m_left.toPrettyString() + "," + m_right.toPrettyString() + ">";
		}
		else if (m_left != null) {
			return  m_primary + "<" + m_left.toPrettyString() + ">"; 
		}
		else {
			return  m_primary;
		}

	}
	
	@Override public final String toString() 
	{
		// ATTEMPT TO CACHE THE STRING
		if (m_key == null)
		{
			if (m_types != null) 
			{
				StringBuilder sb = new StringBuilder();
				sb.append(m_primary).append(m_required).append("<");
				for (int i = 0; i < m_types.length; i++) {
					if (i != 0) {
						sb.append(",");
					}
					sb.append(m_types[i].toString());
				}	
				sb.append(">");
				
				m_key = sb.toString();
			}
			else if (m_left != null && m_right != null) {
				m_key =  m_primary + "<" + m_left + "," + m_right + ">";
			}
			else if (m_left != null) {
				m_key =  m_primary + "<" + m_left + ">"; 
			}
			else {
				m_key = m_primary;
			}
		}
		
		return m_key;
	}
	
	public final Type getReturnType() {
		if (m_right != null) return m_right;
		return m_left;
	}
}
