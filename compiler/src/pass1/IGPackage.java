/*
Copyright (c) 2014-2017 Incubator Games Ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

package pass1;

import util.*;
import java.util.ArrayList;

/*
 * This class serves to maintain a hierarchy of the packages used in the current compilation unit
 
 */



public class IGPackage extends IGScopeItem
{
	

	public IGScopePath m_scope_path = new IGScopePath();
	
	
	
	///////////////////////////////////
	// IGScopeItem
	///////////////////////////////////
	
	public  int       getScopeType() {
		return SCOPE_ITEM_PACKAGE;
	}
	
	public  IGScopePath getScopePath() {
		return m_scope_path;
	}
	
	public IGScopeItem getScopeChild(String name, boolean setter_context)
	{
		// Actually we do really need to check for ambiguous situations
		// aka make an IGAmbiguousScopeItem
		
		final int child_package_count = m_child_packages.size();
		final int child_source_count = m_child_sources.size();
		
		for (int i = 0; i < child_package_count;  i++) 
		{
			if (m_child_packages.get(i).getScopePath().endsWith(name)) {
				return m_child_packages.get(i);
			}
		}
		
		for (int i = 0; i < child_source_count;  i++) 
		{
			if (m_child_sources.get(i).getScopePath().endsWith(name)) {
				return m_child_sources.get(i);
			}
		}
		
		return null;
	}
	
	private String m_get_data_type_cache = null;
	
	public Type getDataType(Type relative) { 
		if (m_get_data_type_cache == null) {
			m_get_data_type_cache = "package:" + getScopePath().toString();
		}
		Type t = Type.get(m_get_data_type_cache);
		t.m_scope = this;
		return t;
	}
	
	////////////////////////////////////
	
	// the path including the trailing slash that the package should be based in
	public String              m_dst_path = null;
	
	// all sub packages
	public ArrayList<IGPackage>   m_child_packages = new ArrayList<IGPackage>();
	
	// all classes, interfaces and enums that are a part of this package
	public ArrayList<IGSource>   m_child_sources  = new ArrayList<IGSource>();

	// who the parent package is.  If its the root this'll be null
	private IGPackage          m_parent = null;

	public IGPackage(String dst_path)
	{
		m_dst_path = dst_path;

	}
	
	/*
	 * Get the root package.
	 * aka the package whose path contains NO tokens
	 **/
	
	public IGPackage getRoot() {
		if (null == m_parent) return this;
		else return m_parent.getRoot(); 
	}	
	
	/*
	 * Inject a source into the package heirarchy.  This'll create additional IGPackage nodes
	 * in order to place a particular source.  This should only be called on the root node
	 */ 
	
	
	public void inject(IGSource source) 
	{
		// check if the source file should be added to this package
		if (m_scope_path.sameScope(source.m_package)) {
			m_child_sources.add(source);
			return;
		}
		
		// check if the source file should be added to an existing child package
		for (IGPackage p : m_child_packages)
		{
			int len = p.m_scope_path.size();
			if (p.m_scope_path.sameScope(source.m_package, len)) {
				p.inject(source);
				
				return;
			}
		}
		
		// no existing child package... so create one		
		{
			IGScopePath child_scope = new IGScopePath(source.m_package, m_scope_path.size() + 1);
			String    sub_package_path = getRoot().m_dst_path + child_scope.toStringWithSeperator("/","/",false) + "/";

			IGPackage p = new IGPackage(sub_package_path);
			p.m_parent     = this;
			p.m_scope_path = child_scope;
		
			m_child_packages.add(p);
			p.inject(source);
		}
	}
	
	
	/*
	 * Lookup an item in the package hierarchy
	 */
	
	public void resolve(ArrayList<IGScopeItem> items, IGScopePath sp) 
	{	
		for (IGPackage  p : m_child_packages) 
		{
			p.resolve(items, sp);

		}
		
		for (IGSource s : m_child_sources) 
		{
			if (s.getScopePath().matches(sp)) {
				if (!items.contains(s)) {
					items.add(s);
				}
			}
		}
	}


}	