/*
Copyright (c) 2014-2017 Incubator Games Ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

package pass1;

public class IGVariable extends IGScopeItem
{
	public int   m_start_position;		// what token # the variable is valid as an LVALUE
	public int   m_available_position;	// what token # the variable is valid as an RVALUE
	public int   m_end_position = -1;	// what token # the variable is valid to

	public Token  m_name;				// the token representing the name of the variable
	
	public Type  m_type;				// the type of the variable

	public Token m_type_token;			// the token representing the first token of the type
										// this is used to display errors about the type
										
	public int   m_scope;				// how deep in scope the variable is

	// does this variable take up a spot in the vtable? (re: tupples)
	public boolean m_counted = true;
	
	////////////////
	// Valid for Parameters to functions
	///////////////
	//public Token   m_default_value = null;
	public boolean m_has_default = false;
	

	// how many times has a variable with this name, been used in the function
	public int  m_occurrance_count = 0;	
		
	// this is for local variables
	public boolean m_AS3_has_initial_value = true;
	
	private IGScopePath m_scope_path = null;


	public void setCounted(boolean c) {
		m_counted = c;
	}

	public IGValueRange getDefaultValueRange() 
	{
		if (m_declaration_node == null) {
			throw new RuntimeException("Missing data");
		}
		
		IGNode n = m_declaration_node;
		IGNode assign = n.findRightMostWithNodeAndTokenType(IGNode.NODE_BINARY, IGConsts.TOK_ASSIGN);
		
		if (assign == null) {
			return m_type.getDefaultValueRange();
		}

		if (assign.m_children.get(1).m_value_range == null) {
			return null;
		}

		return IGValueRange.createImplicitCast(
				assign.m_children.get(0).m_type,
				assign.m_children.get(1).m_type, 
				assign.m_children.get(1).m_value_range);	
	}
	

		
	public  int       getScopeType()
	{
		return SCOPE_ITEM_LOCAL_VARIABLE;
	}
	
	public  IGScopePath getScopePath()
	{
		if (m_scope_path == null) {
			m_scope_path = new IGScopePath(m_name);
		}
		return m_scope_path;
	}
	
	// the only way a variable contains a function is 
	// if it has a delegate within it
	
	public boolean isScopeCallable() 
	{
		if (m_type.m_primary.equals("Function") || 
			m_type.m_primary.equals("StaticFunction")) 
		{
			return true;
		}
		
		return false;
	}
	
	public IGScopeItem getScopeChild(String name, boolean setter_context) {
		if (m_type.m_scope == null) return null;
		return m_type.m_scope.getScopeChild(name, setter_context);
	}
	
	////////////////////////////////////////
	
	public Type getDataType(Type relative) {
		return m_type;
	}
	
	
	
	public IGVariable(int scope, int start_position, Token name, Type type)
	{
		m_scope = scope;
		m_start_position = start_position;
		m_available_position = start_position;
		m_name = name;
		m_type = type;
	}
	
	public void setAvailableIndex(int position)
	{
		m_available_position = position;
	}
	
	/*
	public boolean inConflict(int scope, int start_position, String name)
	{
		if (m_scope == scope && 
		    m_start_position < start_position && 
		    (m_end_position == -1 || start_position < m_end_position) &&
		    name.equals(m_name.m_value)) {

		 	return true;   
		}
		
		return false;	
	}
	*/
}