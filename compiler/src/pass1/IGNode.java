/*
Copyright (c) 2014-2017 Incubator Games Ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

package pass1;

import java.util.Vector;
import java.util.ArrayList;
/*
 This class encapsulates the contents of a part of an expression
 
*/


/*
	LOCAL_VARIABLE_ACCESS = on stack variable
	FIELD_ACCESS, STATIC_FIELD_ACCESS, LOCAL_ACCESSOR, STATIC_LOCAL_ACCESSOR
	DOT    
	DOT_ACCESSOR  
	STATIC_DOT_ACCESSOR




*/




public class IGNode
{
	public int 		m_start_index;		// the inclusive index of the first token contained by this node
	public int 		m_end_index;		// the exclusive index of the first token contained by this node
	
	
	
	public static final byte CF_CONTINUE_OR_BREAK = 1;		// something that could term a function component eg. loop
	public static final byte CF_RETURN            = 2;		// something that could term a function
	
	public static final byte VT_LEFT  = 1;
	public static final byte VT_RIGHT = 2;
	
	public byte     m_value_type   = VT_LEFT;
	public byte     m_control_flow = 0;
	
	
	//public boolean  m_returns = false;
	public boolean  label = false;
	public short 	m_node_type = 0;	
	
	public IGNode   m_parent;			// the parent of this node
	public Token 	m_token;			// the token that represents this node (may or may not be valid)
	
	

	//public IGOptimizerValue m_value = null;
	
	
	
	public Type 		m_type;					// the type contained by this node
	public IGScopeItem 	m_scope = null;			
	
	
	public IGNodeList        m_children    = new IGNodeList();
	public IGValueRange      m_value_range = null;		// the value contained by this ndoe
	
	public int m_user_index = 0;				// used only by initializer lists
	public int m_min_line_number = -1;
	
	public boolean isEmpty() {
		return m_children.size() == 0;
	}
	
	/*
	public final IGNode clone(IGNode parent)
	{
		IGNode node = new IGNode();
		node.m_start_index = m_start_index;
		node.m_end_index   = m_end_index;
		node.m_returns     = m_returns;
		node.m_token       = m_token;
		node.m_node_type   = m_node_type;
		//node.m_value       = m_value;
	
		node.m_scope = m_scope;
		node.m_type = m_type;
		//node.m_value = m_value;
		node.m_value_range = m_value_range;
	
		for (int i  = 0; i < m_children.size(); i++) {
			node.m_children.add(m_children.get(i).clone(node));
		}
	
		return node;
	}
	*/
	
	
	
	public final int getBinaryType()
	{
		if (m_node_type != NODE_BINARY) {
			return -1;
		}
		
		Token op = m_token;
		if (op.m_type == IGConsts.TOK_ASSIGN) 
		{
			return 1;
		}
		else if (op.m_type == IGConsts.TOK_ADD_ASSIGN ||
				 op.m_type == IGConsts.TOK_SUB_ASSIGN ||
				 op.m_type == IGConsts.TOK_MUL_ASSIGN ||
				 op.m_type == IGConsts.TOK_DIV_ASSIGN ||
				 op.m_type == IGConsts.TOK_MOD_ASSIGN || 
				 op.m_type == IGConsts.TOK_XOR_ASSIGN || 
				 op.m_type == IGConsts.TOK_OR_ASSIGN || 
				 op.m_type == IGConsts.TOK_AND_ASSIGN) 
		{
			return 2;		 
		}
		else {
			// +, -, *, etc
			return 0;
		}
	}
	
	/** 
	 * Return whether this value is being used as an LVALUE or not
	 * It'll be an LValue iff
	 * This is only valid once IGValidator is run
	 */
	
	public final boolean isLValue() {
		if (m_parent != null && m_parent.m_node_type == NODE_TYPE_BRACKETED_EXPRESSION) {
			return m_parent.isLValue();
		}
		else if (m_parent != null && m_parent.getBinaryType() > 0 && m_parent.m_children.get(0) == this) 
		{
			return true;
		}
		else {
			return false;
		}
	}
	
	public final boolean isLRValue() {
		if (m_parent != null && m_parent.m_node_type == NODE_TYPE_BRACKETED_EXPRESSION) {
			return m_parent.isLRValue();
		}
		else if (m_parent != null && m_parent.getBinaryType() > 1) 
		{
			return true;
		}
		else {
			return false;
		}
	}
	
	/**
	 * Perhaps this should be called promote to assignment
	 */
	
	public final boolean canBeTargetOfAssignment()
	{
		if (m_node_type == STATIC_LOCAL_ACCESSOR      ||
			m_node_type == LOCAL_ACCESSOR      ||  // <-- this isn't always true.  Need to verify that it CAN be an LVALUE
			m_node_type == DOT_ACCESSOR        ||  // <-- this isn't always true.  Need to verify that it CAN be an LVALUE
			m_node_type == STATIC_DOT_ACCESSOR ||  // <-- this isn't always true.  Need to verify that it CAN be an LVALUE
			m_node_type == NODE_INDEX          ||
			m_node_type == FIELD_ACCESS        ||
			m_node_type == LOCAL_VARIABLE_ACCESS ||
			m_node_type == STATIC_FIELD_ACCESS ||
			m_node_type == NODE_DOT		// <-- gross
			
			) 		
		{
			
			return true;	
		}
	
		// switch to using m_value_type
		// when that is all implemented
		else if (m_node_type == NODE_TYPE_BRACKETED_EXPRESSION) {
			return m_children.get(0).canBeTargetOfAssignment();
		}
		else if (m_node_type == NODE_TRINARY) {		// this should be ternary not trinary
			return false;
		}
		else if (m_node_type == NODE_UNARY || m_node_type == NODE_VALUE || // eg.  2 = 2;  or -2 = 2;
				 m_node_type == NODE_NEW												   // eg.  new Object() = "string";
				
				) 
				{
			return false;
		}
		
		//System.out.println("bad type: " + m_node_type);
		return false;
		//return true;
	}
	
	public final void replaceTypes(Type pattern, Type repl)
	{
		if (m_type == pattern) {
			m_type = repl;
		}
		
		final int children_count = m_children.size();
		for (int i = 0; i < children_count; i++)
		{
			m_children.get(i).replaceTypes(pattern, repl);
		}
	}
	
	
	public void debug()
	{
		debug(0);
	}
	
	
	
	
	public final int calcMinLineNumber()
	{	
		if (m_min_line_number == -1)
		{
			// assign m to be the maximum value of a 32 bit integer
			int m = 0x7fffffff;
		
			if (null != m_token && m_token.m_line < m) {
				m = m_token.m_line;
			}	
			
			IGNodeList children = m_children;
			int children_count = m_children.size();
			//for (IGNode n : m_children) {
			for (int i = 0; i < children_count; i++) {
				int m2 = children.get(i).calcMinLineNumber();
				if (m2 < m) {
					m = m2;
				}
			}
			m_min_line_number = m;
		}
		return m_min_line_number;
	}
	
	public void debug(int indent)
	{
		if (indent == 0) {
			 m_token.printDebug("Debug");
		}
	
	
		for (int i = 0; i < indent; i++) System.out.print("\t");
		
		System.out.println(m_token.m_value + "@" + m_token.m_token_index + " [" + m_start_index + "," + m_end_index + ") node_type: " + m_node_type + " type: " + m_type);
		for (int i = 0; i < m_children.size(); i++) {
			m_children.get(i).debug(indent+1);
		}	
	}
	
	
	public IGNode findLeftMostWithType(int node_type)
	{
		if (m_node_type == node_type) {
			return this;
		}
		
		if ( m_children.size() > 0) {
			return m_children.get(0).findLeftMostWithType(node_type);
		}
		
		return null;
	}
	
	public IGNode findRightMostWithNodeAndTokenType(int node_type, int token_type)
	{
		if (m_node_type == node_type && m_token.m_type == token_type) {
			return this;
		}
		
		if ( m_children.size() > 0) {
			return m_children.get(m_children.size() - 1).findRightMostWithNodeAndTokenType(node_type, token_type);
		}
		
		return null;
	}
	
	public boolean hasPredecessor(int node_type) {
	
		if (m_node_type == node_type) {
			return true;
		}
	
		if (m_parent == null) {
			return false;
		}
		
		return m_parent.hasPredecessor(node_type);
		
	}
	
	
	public void disableTokenOutput()
	{	
		m_token.m_output = false;
		
		final IGNodeList children = m_children;
		final int children_count  = children.size();
	
		for (int i = 0; i < children_count; i++) {
			children.get(i).disableTokenOutput();
		}
		//for (IGNode n : m_children) {
		//	n.disableTokenOutput();
		//}
	}
	
	private IGNode()
	{
		
	}
	
	
	public IGNode(IGNode other) {
		m_start_index = other.m_start_index;
		m_token = other.m_token;
		m_token.m_node = this;
	}
	
	public IGNode(int idx, Token t)
	{
		m_start_index = idx;
		m_token = t;
		m_token.m_node = this;
	}
	
	public void addChild(IGNode n) {
		if (null != n.m_parent) {
			n.m_parent.removeChild(n);
		}
		
		n.m_parent = this;
		m_children.add(n);
	}
	
	private void removeChild(IGNode n) {
		if (n.m_parent != this) return;
		n.m_parent = null;
		
		m_children.remove(n);
		
		//int idx = m_children.indexOf(n);
		//m_children.remove(idx);
	}
	
	public void complete(int idx, Type t)
	{
		m_type = t;
		m_end_index = idx;
		
		
	}	

	public IGNode getChild(int idx) {
		return m_children.get(idx);
	}
	
	public int getChildCount() {
		return m_children.size();
	}
	
	
	
	/////////////////////////////////////////////////////////////////
	// Node type constants
	/////////////////////////////////////////////////////////////////
	
	
	public static final short NODE_TYPE_DEFAULT = 0;
	
	public static final short NODE_TYPE_LOCAL_VARIABLE_NAME 		= 2;
	public static final short NODE_TYPE_LOCAL_VARIABLE_DEFINITION = 3;
	
	
	
	
	// ID : TYPE
	public static final short ID_TYPE_PAIR              = 4;
	
	public static final short NODE_TYPE_TYPE						= 91;

	
	public static final short NODE_TYPE_VAR						= 6;
	
	public static final short FIELD_ACCESS              = 7;
	
	public static final short LOCAL_VARIABLE_ACCESS     = 1;
	
	
	// TYPE (<child0>)
	public static final short NODE_TYPE_CAST                      = 5;
	// XXXXX<child0> . ID<child1>
	public static final short NODE_DOT							= 8;
	public static final short DOT_ACCESSOR                      = 81;
	public static final short STATIC_DOT_ACCESSOR               = 82;
	//public static final short NODE_DOT_SET						= 81;  // a '=' follows
	//public static final short NODE_DOT_GET						= 82;	// this is used as an rvalue
	
	// XXXXX '[' index ']'
	public static final short NODE_INDEX							= 9;
	// XXXXX '(' params ')'
	public static final short NODE_CALL							= 10;
	// '(' EXPRESSION ')'
	public static final short NODE_TYPE_BRACKETED_EXPRESSION		= 90;
	
	public static final short NODE_NEW							= 92;
	
	
	// The following 3 are used when there is no specific 
	// operator mentioned
	/////////////////////////////////////////////////////////////
	
	public static final short NODE_UNARY						= 17;
	public static final short NODE_BINARY						= 18;
	public static final short NODE_TRINARY					= 19;
	
	
	public static final short NODE_TYPE_STATIC_VARIABLE_NAME      = 11;
	public static final short NODE_TYPE_VARIABLE_NAME             = 12; // TODO delete this
	public static final short NODE_TYPE_MEMBER_VARIABLE_NAME      = 12;
	
	
	
	// for ( <child-0 VAR or EXPRESSION>  <child-1 NODE_TYPE_FOREACH_IN> )
	public static final short NODE_TYPE_FOREACH                   = 13;
	
	// in <child-0 EXPRESSION> 
	public static final short NODE_TYPE_FOREACH_IN                = 14;
		
	//public static final short NODE_TYPE_RETURN_ID                 = 15;	
	
	public static final short NODE_BLOCK						=	16;
	
	
	public static final short NODE_IF								= 100;
	public static final short NODE_ELSE_IF						= 101;
	public static final short NODE_ELSE							= 102;
	
	public static final short NODE_FOR							= 103;
	
	
	// wraps inline shorts, Strings, booleans, nulls
	public static final short NODE_VALUE							= 104;
	
	public static final short NODE_PARAMETER_LIST					= 105;
	
	//public static final short NODE_LOCAL_GET						= 106;
	//public static final short NODE_LOCAL_SET						= 107;
	
	public static final short LOCAL_ACCESSOR                        = 106;
	public static final short STATIC_LOCAL_ACCESSOR                 = 107;	
	//  : TYPE  following a function declaration
	public static final short FUNCTION_RETURN_TYPE				= 108;
	
	public static final short STATIC_FIELD_ACCESS					= 109;
	
	public static final short PACKAGE								= 110;
	public static final short IMPORT								= 111;
	public static final short SOURCE								= 112;
	public static final short CLASS								= 113;
	
	public static final short CLASS_VARIABLE						= 120;
	public static final short CLASS_STATIC_VARIABLE				= 121;
	public static final short CLASS_FUNCTION						= 122;
	public static final short CLASS_STATIC_FUNCTION				= 123;
	public static final short NODE_CATCH							= 124;
	public static final short NODE_FUNCTION_SIGNATURE				= 125;
	public static final short NODE_RETURN							= 126;
	public static final short NODE_ENUM_MEMBER					= 127;
	public static final short NODE_ENUM							= 128;
	public static final short NODE_INTERFACE						= 129;
	
	public static final short SCOPE_CHAIN_START					= 130;
	public static final short FLOW_CHANGE							= 131;
	public static final short NEW_CALL							= 134;
	public static final short THROW								= 135;			
	public static final short WHILE								= 136;			
	public static final short DO									= 137;			
	public static final short INCREMENT							= 138;
	public static final short NATIVE_BLOCK						= 139;
	public static final short STATEMENT							= 140;
	public static final short TRY									= 141;
	public static final short PARAMETER_CAST						= 142;
	
	// token:'EXTENDS'  child:(node: TYPE)
	public static final short EXTENDS								= 145;
	// token:'IMPLEMENTS' child0..N(node: TYPE)
	public static final short IMPLEMENTS							= 146;
	public static final short SOURCE_ACCESS_MODIFIERS             = 147;
	public static final short SOURCE_NAME							= 148;
	public static final short SOURCE_PREAMBLE						= 149;
	public static final short FUNCTION_PARAMETERS                 = 150;
	public static final short MEMBER_NAME                         = 151;
	
	public static final short ARRAY_INITIALIZER_LIST              = 152;
	public static final short ARRAY_INITIALIZER_ITEM              = 153;
	
	public static final short VECTOR_INITIALIZER_LIST             = 154;
	public static final short VECTOR_INITIALIZER_ITEM             = 155;
	
	public static final short MAP_INITIALIZER_LIST                = 156;
	public static final short MAP_INITIALIZER_ITEM                = 157;
	
	
	public static final short ARRAY_INITIALIZER             = 158;
	public static final short VECTOR_INITIALIZER             = 159;
	public static final short MAP_INITIALIZER                = 160;
	public static final short MAP_INITIALIZER_SEPARATOR      = 161;
	public static final short SOURCE_TEMPLATES				= 162;
	
	public static final short FOREACH_IN_RANGE				= 163;
	
	public static final short SWITCH						= 164;
	public static final short SWITCH_CASE					= 165;
	public static final short SWITCH_DEFAULT				= 167;
	
	public static final short MEMBER_PREAMBLE               = 168;
	public static final short MEMBER_PREAMBLE_TOKEN			= 169;

	public static final short NODE_TUPLE_BINARY = 170;
	public static final short NODE_TYPE_TUPLE_CREATION = 171;
	public static final short NODE_TUPLE_BINARY_SINGLE = 172;

}