/*
Copyright (c) 2014-2017 Incubator Games Ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

package pass1;

import java.util.*;

/**
 * This code is used for optimizing value calculations throughout the system.  This is used
 * for propagating static constants as well as enum constants.
 *
 * A value can be the following
 *	BOOL     - TRUE, FALSE,  UNKNOWN
 *  INT      - VALUE, RANGE
 *  DOUBLE   - VALUE, RANGE
 *  POINTER  - NULL, NOT-NULL, UNKNOWN
 *
 * If a value is known it is considered concrete.  ie you can make further value determinations base
 * on it.
 *
 *
 * TODO.  Extend the concept of concretes to functions as well.
 *
 */
public class IGValueRange extends IGConsts
{

	
	public static final int CONCRETE_BIT = 0x1000;
	
	public static final int TYPE_INT        = 0 | CONCRETE_BIT;
	public static final int TYPE_DOUBLE     = 1 | CONCRETE_BIT;
	public static final int TYPE_INT_ANY    = 0;
	public static final int TYPE_DOUBLE_ANY = 1;
	
	public static final int TYPE_POINTER_ANY   = 2;
	public static final int TYPE_POINTER_VALID = 3 ;
	public static final int TYPE_POINTER_NULL  = 4  | CONCRETE_BIT;
	
	public static final int TYPE_BOOL_ANY   = 8;
	public static final int TYPE_BOOL_FALSE = 9  | CONCRETE_BIT;
	public static final int TYPE_BOOL_TRUE  = 10 | CONCRETE_BIT;

	public static final int TYPE_STRING_ANY = 11;
	public static final int TYPE_STRING_NULL = 12 | CONCRETE_BIT;
	public static final int TYPE_STRING_VALID = 13 | CONCRETE_BIT;

	public static final int TYPE_DELEGATE_ANY   = 14;
	public static final int TYPE_DELEGATE_VALID = 15 ;
	public static final int TYPE_DELEGATE_NULL  = 16  | CONCRETE_BIT;

	// Used for default values (bitwise representation is all 0s)
	public static final IGValueRange INT_0 = new IGValueRange(TYPE_INT).setIntValue(0);
	public static final IGValueRange INT_1 = new IGValueRange(TYPE_INT).setIntValue(1);
	public static final IGValueRange DOUBLE_0 = new IGValueRange(TYPE_DOUBLE).setDoubleValue(1);
	public static final IGValueRange DOUBLE_1 = new IGValueRange(TYPE_DOUBLE).setDoubleValue(1);
	public static final IGValueRange BOOL_FALSE = new IGValueRange(TYPE_BOOL_FALSE);
	public static final IGValueRange BOOL_TRUE = new IGValueRange(TYPE_BOOL_TRUE);
	public static final IGValueRange POINTER_NULL = new IGValueRange(TYPE_POINTER_NULL);
	
	// Used for unknown values
	public static final IGValueRange POINTER_ANY = new IGValueRange(TYPE_POINTER_ANY);
	public static final IGValueRange BOOL_ANY    = new IGValueRange(TYPE_BOOL_ANY);
	public static final IGValueRange INT_ANY     = new IGValueRange(TYPE_INT_ANY);
	public static final IGValueRange DOUBLE_ANY  = new IGValueRange(TYPE_DOUBLE_ANY);
	public static final IGValueRange DELEGATE_ANY = new IGValueRange(TYPE_DELEGATE_ANY);
	public static final IGValueRange DELEGATE_NULL = new IGValueRange(TYPE_DELEGATE_NULL);
	
	
	
	public int getIntValue() {
		if (m_long_min != m_long_max) { 
			throw new RuntimeException("Failed getIntValue");
		}
		return (int)m_long_min;
	}
	
	private IGValueRange setIntValue(int i) {
		m_long_min = i;
		m_long_max = i;
		return this;
	}	
	
	private IGValueRange setDoubleValue(double i) {
		m_double_min = i;
		m_double_max = i;
		return this;
	}
	
	

	public int    m_type = 0;	
	long	      m_long_min = 0;
	long 	      m_long_max = 0;
	double        m_double_min = 0;
	double        m_double_max = 0;
	String        m_string = null;

	public @Override String toString() {
		return "(" + m_type + "," + m_long_min + "=>" + m_long_max + " " + 
									m_double_min + "=>" + m_double_max + ", " + m_string + ")";

	}

	public static int s_created = 0;
	
	public IGValueRange(int type) {
		m_type = type;
		s_created ++;
		
		if (type == TYPE_INT_ANY) {
			m_long_max = Integer.MAX_VALUE;
			m_long_min = Integer.MIN_VALUE;
		}
		else if (type == TYPE_DOUBLE_ANY) {
			m_double_max = Double.MAX_VALUE;
			m_double_min = Double.MIN_VALUE;
		}
	}
	
	
	
	
	public final void debug() {
		System.out.println("IGValueRange " + m_type + " " + m_long_min + " " + m_long_max + " " +
			m_double_min + " " + m_double_max);
	}
	
	/**
	 * Is the value of this range 100% known
	 */
	
	public final boolean isConcrete() {
		return ((m_type & CONCRETE_BIT) != 0);
	}	
	
	/**
	 * Get the value of this range as a double
	 */
	
	public final double doubleValue() {
		if (m_type == TYPE_DOUBLE) { return m_double_min; }
		if (m_type == TYPE_INT)  { return (double)(int)m_long_min; }
		throw new RuntimeException("What gives?");
	}
	
	/**
	 * Get the value of this range as an int
	 */
	
	public final int intValue() {
		if (m_type == TYPE_INT) { return (int)m_long_min; }
		throw new RuntimeException("What gives?");
	}
	

	
	public static final IGValueRange createNull() {
		return POINTER_NULL;	// new IGValueRange(TYPE_POINTER_NULL);
	}
	
	/**
	 * Called from the Validator code
	 */
	
	public static final IGValueRange createBool(String token_value) {
		if (token_value.equals("true")) {
			return BOOL_TRUE;	//new IGValueRange(TYPE_BOOL_TRUE);	
		}
		else {
			return BOOL_FALSE;	//new IGValueRange(TYPE_BOOL_FALSE);
		}
	}
	
	/**
	 * Create a new value range with a concrete double
	 */
	
	public static final IGValueRange createDouble(double value) {
		IGValueRange vr = new IGValueRange(TYPE_DOUBLE);
		vr.m_double_min = value;
		vr.m_double_max = value;
		return vr;
	}
	
	/** 
	 * Create a new value range with a concrete int
	 */
	
	public static final IGValueRange createInt(int value) {
		IGValueRange vr = new IGValueRange(TYPE_INT);
		vr.m_long_min = value;
		vr.m_long_max = value;
		return vr;
	}
	
	public static final IGValueRange createDouble(int token_type, String token_value) 
	{
		if (token_type == IGLexer.LEXER_FLOAT) {
			return new IGValueRange(TYPE_DOUBLE).setDoubleValue(Double.parseDouble(token_value));
		}
		else {
			throw new RuntimeException("Invalid double type.");
		}
	}
	
	public static final IGValueRange createInt(int token_type, String token_value) 
	{
		IGValueRange vr = new IGValueRange(TYPE_INT);
		
		if (token_type == IGLexer.LEXER_INT) 
		{
			long value = Long.parseLong(token_value);
			vr.m_long_min = value;
			vr.m_long_max = value;
		}
		else if (token_type == IGLexer.LEXER_HEX) {
			String v = token_value.substring(2);
			long value = Long.parseLong(v, 16);
			vr.m_long_min = value;
			vr.m_long_max = value;			
		}
		else if (token_type == IGLexer.LEXER_CHAR) {
			// this is SOO heavy weight compared to what it is used for
			int value = util.Decoder.escapedCharToInt(token_value);
			vr.m_long_min = value;
			vr.m_long_max = value;
		}
		else {
			throw new RuntimeException("Invalid integer type");
		}
		
		return vr;
	}
	
	public static final IGValueRange createIncrement(IGValueRange other) {
		IGValueRange vr = createClone(other);
		if (vr.m_type == TYPE_INT) {
			vr.m_long_min++;
			vr.m_long_max++;
		}
		else if (vr.m_type == TYPE_DOUBLE) {
			vr.m_double_min++;
			vr.m_double_max++;
		}
		else {
			throw new RuntimeException("Can't create increment");
		}
		return vr;
	}
	
	public static final IGValueRange createClone(IGValueRange other) {
		IGValueRange vr = new IGValueRange(other.m_type);
		vr.m_long_min = other.m_long_min;
		vr.m_double_min = other.m_double_min;
		vr.m_long_max = other.m_long_max;
		vr.m_double_max = other.m_double_max;
		return vr;
	}
	
	public static final IGValueRange createNegation(IGValueRange other) {
		IGValueRange vr = new IGValueRange(other.m_type);
		vr.m_long_min = -other.m_long_max;
		vr.m_long_max = -other.m_long_min;
		vr.m_double_min = -other.m_double_max;
		vr.m_double_max = -other.m_double_min;
		return vr;
	}
	
	public static final IGValueRange createLogicalNot(IGValueRange other) {
		if (other.m_type == TYPE_BOOL_FALSE) {
			return BOOL_TRUE;
		}
		else if (other.m_type == TYPE_BOOL_TRUE) {
			return BOOL_FALSE;	
		}
		else {
			return BOOL_ANY;
		}
	}
	
	public static final IGValueRange createBitwiseNot(IGValueRange other) {
		IGValueRange vr = new IGValueRange(other.m_type);	
		vr.m_long_min = ~other.m_long_min;
		vr.m_long_max = ~other.m_long_max;
		if (vr.m_long_max > vr.m_long_min) {
			vr.m_long_max = ~other.m_long_min;
			vr.m_long_min = ~other.m_long_max;
		}
		return vr;
	}
	
	public static final IGValueRange createImplicitCast(Type dst_type, Type src_type, IGValueRange other_range)
	{
		int work = src_type.workRequiredToCastTo(dst_type, false);
		
		IGValueRange default_dst_range = dst_type.getAnyValueRange();
		
		switch (work) 
		{
			case Type.WORK_INCOMPATIBLE: {
				throw new RuntimeException("Internal Failure");
			}
			case Type.WORK_NONE: 
			{
				// user probably typed it in for safety.  Nothing to really do
				return createClone(other_range);
			}
			
			case Type.WORK_FUNCTION_UNWRAP:
			{
				// should never be specifying a known value for a unwrapped function
				throw new RuntimeException("Internal Failure");
			}
			
			case Type.WORK_NULL_TO_DELEGATE_NULL:
			{
				return DELEGATE_NULL;
			}
			
			
			case Type.WORK_OBJECT_CAST: 
			{
				// pointer value remains unchanged
				return createClone(other_range);
			}
	
			case Type.WORK_PRIM_CAST: {
			
				// consider  (bool, int, uint and double)
				// also consider enums that take on that particular type
			
				Type type_bool = Type.BOOL;
				Type type_int  = Type.INT;
				Type type_uint = Type.UINT;
				Type type_double = Type.DOUBLE;
			
				int left_switch = 0;
				int right_switch = 0;
			
				if      (dst_type == type_bool)   { left_switch = 0; }
				else if (dst_type == type_int)    { left_switch = 1; }
				else if (dst_type == type_uint)   { left_switch = 2; }
				else if (dst_type == type_double) { left_switch = 3; }
				else if (dst_type.isEnum() && dst_type.getEnumPrimitive() == type_bool)   { left_switch = 0x0; }  
				else if (dst_type.isEnum() && dst_type.getEnumPrimitive() == type_int)    { left_switch = 0x1; }  
				else if (dst_type.isEnum() && dst_type.getEnumPrimitive() == type_uint)   { left_switch = 0x2; }  
				else if (dst_type.isEnum() && dst_type.getEnumPrimitive() == type_double) { left_switch = 0x3; }  
	
				if      (src_type == type_bool)   { right_switch = 0; }
				else if (src_type == type_int)    { right_switch = 1; }
				else if (src_type == type_uint)   { right_switch = 2; }
				else if (src_type == type_double) { right_switch = 3; }
				else if (src_type.isEnum() && src_type.getEnumPrimitive() == type_bool)   { right_switch = 0x0; }  
				else if (src_type.isEnum() && src_type.getEnumPrimitive() == type_int)    { right_switch = 0x1; }  
				else if (src_type.isEnum() && src_type.getEnumPrimitive() == type_uint)   { right_switch = 0x2; }  
				else if (src_type.isEnum() && src_type.getEnumPrimitive() == type_double) { right_switch = 0x3; }  
				
				if ((left_switch == 1 || left_switch == 2) && right_switch == 3) {
					if (!other_range.isConcrete()) {
						return default_dst_range;
					}
					
					IGValueRange v = createClone(default_dst_range);
					v.m_type     = IGValueRange.TYPE_INT;
					v.m_long_min = (long)other_range.m_double_min;
					v.m_long_max = (long)other_range.m_double_max;
					return v;
				}
				else if ((right_switch == 1 || right_switch == 2) && left_switch == 3) {
					if (!other_range.isConcrete()) {
						return default_dst_range;
					}
					
					IGValueRange v = createClone(default_dst_range);
					v.m_type     = IGValueRange.TYPE_DOUBLE;
					v.m_double_min = (double)other_range.m_long_min;
					v.m_double_max = (double)other_range.m_long_max;
					return v;
				}
				else if ((right_switch == 1 || right_switch == 2) && 
						 (left_switch == 1 || left_switch == 2)) {
					return createClone(other_range);
				}
				// bool <= double
				else if (left_switch == 0 && right_switch == 3) {
					if (!other_range.isConcrete()) {
						return IGValueRange.BOOL_ANY;
					}
					
					if (other_range.m_double_min == 0) {
						return IGValueRange.BOOL_FALSE;
					}
					else {
						return IGValueRange.BOOL_TRUE;
					}
				}
				// bool <= int, uint
				else if (left_switch == 0 && (right_switch == 1 || right_switch == 2)) {
					if (!other_range.isConcrete()) {
						return IGValueRange.BOOL_ANY;
					}
					
					if (other_range.m_long_min == 0) {
						return IGValueRange.BOOL_FALSE;
					}
					else {
						return IGValueRange.BOOL_TRUE;
					}
				}
				else if (left_switch == 3 && right_switch == 0) {
					if (!other_range.isConcrete()) {
						return IGValueRange.DOUBLE_ANY;
					}
					
					if (other_range.m_type == IGValueRange.TYPE_BOOL_FALSE) {
						return IGValueRange.DOUBLE_0;
					}
					else {
						return IGValueRange.DOUBLE_1;
					}
				}
				else if ((left_switch == 1 || left_switch == 2) && right_switch == 0) {
					if (!other_range.isConcrete()) {
						return IGValueRange.INT_ANY;
					}
					
					if (other_range.m_type == IGValueRange.TYPE_BOOL_FALSE) {
						return IGValueRange.INT_0;
					}
					else {
						return IGValueRange.INT_1;
					}
				}
			}
				
			// casting an object to a string
			case Type.WORK_STRING_CAST : { break;
				// will be an ANY string by default
				// check that the object isn't null?
			}
				
			// converting an enum to a value
			case Type.WORK_ENUM_TO_STRING: { break;
				// will be an ANY string by default
			}
				
			// casting a string to an (int, double, bool)
			case Type.WORK_STRING_TO_PRIM : { break;
				// will be an ANY primitive by default
			}
				
			// casting a primitive (int, double, bool) to a string
			case Type.WORK_PRIM_TO_STRING : { break;
				// will be an ANY pointer by default
			}
				
			// casting an enum of a same storage class to another
			case Type.WORK_ENUM_CAST : {
				return createClone(other_range);
			}
		}
		
		return default_dst_range;
	}
	
	
	
	
	
	private static void optimize(IGSource s, IGNode _node)
	{	
		// children are optimized before parents
		
	
	
		ArrayList<IGNode> to_process = new ArrayList<IGNode>();
		
		// explicit stack?
		to_process.add(_node);
		_node.label = false;
		
		int sz = 1;
		
	
		while (sz > 0) 
		{
			IGNode node = to_process.get(sz - 1);
			if (node.label == false) 
			{
				int children_count = node.m_children.size();
				for (int i = 0; i < children_count; i++) 
				{
					IGNode c = node.m_children.get(i);
					c.label = false;
					to_process.add(c);
				}
				sz += children_count;
				node.label = true;
			}
			else {
				sz --;
				to_process.remove(sz);
				optimizePost(s, node);		
			}
		}
		
		
		/*
		//if (optimizePre(s, node))
		//{
			int sz = node.m_children.size();
			for (int i = 0; i < sz; i++) {
				optimize(s, node.m_children.get(i));
			}
		//}
		
		optimizePost(s, node);		
		*/
	}
	
//	public static boolean optimizePre(IGSource s, IGNode node) {
//		return true;
//	}
	
	public static void optimizePost(IGSource s, IGNode node) 
	{
		switch (node.m_node_type)
		{
			
			/*
			case IGNode.NODE_RETURN:
			{
				if (node.m_type != Type.VOID) {
					if (node.m_children.get(0).m_value_range != null) {
						node.m_value_range = node.m_children.get(0).m_value_range;
					}
				}
				break;
			}
			*/
		
			case IGNode.STATIC_FIELD_ACCESS:
			{
				IGMember member = (IGMember)node.m_scope;
				if (member.hasModifier(IGMember.STATIC | IGMember.FINAL)) 
				{
					node.m_value_range = member.m_return_value_range;	
				}
				
				break;
			}
			
			case IGNode.NODE_DOT:
			{
				if (node.m_scope instanceof IGMember) 
				{
					IGMember member = (IGMember)node.m_scope;
					if (member.hasModifier(IGMember.STATIC | IGMember.FINAL)) 
					{
						node.m_value_range = member.m_return_value_range;	
					}
				}
				break;
			}
			
			case IGNode.NODE_TYPE_BRACKETED_EXPRESSION:
			{
				if (node.m_children.get(0).m_value_range != null) {
					node.m_value_range = node.m_children.get(0).m_value_range;
				}
				
				break;
			}
			
			case IGNode.NODE_UNARY:
			{
				if (node.m_children.get(0).m_value_range != null) 
				{
					int token_type = node.m_token.m_type;
					if (token_type == TOK_SUB) {
						node.m_value_range = IGValueRange.createNegation(node.m_children.get(0).m_value_range);
					}
					else if (token_type == TOK_ADD) {
						node.m_value_range = node.m_children.get(0).m_value_range;
					}
					else if (token_type == TOK_NOT) {
						node.m_value_range = IGValueRange.createLogicalNot(node.m_children.get(0).m_value_range);
					}
					else if (token_type == TOK_BIT_NOT) {
						node.m_value_range = IGValueRange.createBitwiseNot(node.m_children.get(0).m_value_range);
					}
				}	
				
				break;
			}
			
			
			case IGNode.NODE_TYPE_CAST: 
			{ 
				Type dst_type = node.m_type;
				Type src_type = node.m_children.get(1).m_type;
					
				//outputFunctionBody(m, node.m_children.get(1), pw);
				//castTo(src_type, dst_type, pw, true);
				
				if (dst_type == Type.INT)
				{
					IGNode src_node = node.m_children.get(1);
					if (src_node.m_value_range != null)
					{
						IGValueRange vr = src_node.m_value_range;
						if (vr.m_type == TYPE_INT) {
							node.m_value_range = vr;
						}
						else if (vr.m_type == TYPE_BOOL_FALSE) {
							node.m_value_range = INT_0;
						}
						else if (vr.m_type == TYPE_BOOL_TRUE) {
							node.m_value_range = INT_1;
						}	
						else if (vr.m_type == TYPE_DOUBLE) {
							node.m_value_range = createInt((int)vr.m_double_min);
						}
					}
				}
				else if (dst_type == Type.DOUBLE) 
				{
				
				}
				
			 	break;
			 }
						
			case IGNode.NODE_BINARY:
			{
				IGNode left = node.m_children.get(0);
				IGNode right = node.m_children.get(1);
				Type   type  = node.m_type;
				
			
				//if (token_type == TOK_ASSIGN) {
				//	if (right.m_value_range != null && right.m_value_range.isConcrete())
				//	{
				//		node.m_value_range = IGValueRange.createImplicitCast(left.m_type, right.m_type, right.m_value_range);
				//	}
				//}
				//else 
			
				if (left.m_value_range != null && 
					right.m_value_range != null && 
					left.m_value_range.isConcrete() && 
					right.m_value_range.isConcrete()) {
				
				
					int token_type = node.m_token.m_type;
				
					if (left.m_type.isArithmetic() && 
					    right.m_type.isArithmetic())
					{
						if (type == Type.DOUBLE) 
						{
							double left_value = left.m_value_range.doubleValue();
							double right_value = right.m_value_range.doubleValue();
					
							if (token_type == TOK_ADD) {
								node.m_value_range = IGValueRange.createDouble(left_value + right_value);
							}
							else if (token_type == TOK_SUB) {
								node.m_value_range = IGValueRange.createDouble(left_value - right_value);
							}
							else if (token_type == TOK_MUL) {
								node.m_value_range = IGValueRange.createDouble(left_value * right_value);
							}
							else if (token_type == TOK_DIV) {
								//if (right_value == 0) 
								//{
								//	node.m_token.printError("Divide by zero");
								//	System.exit(1);
								//}
								
								
								// divide by zero for doubles is perfectly acceptable
								// 1.0 / 0.0 = Infinity
								// -1.0 / 0.0 = -Infinity
								// 0.0 / 0.0 = NaN
								node.m_value_range = IGValueRange.createDouble(left_value / right_value);
							}
						}
						else if (type == Type.INT) 
						{
							int left_value = left.m_value_range.intValue();
							int right_value = right.m_value_range.intValue();
					
							switch(token_type)
							{
								case TOK_ADD:
									node.m_value_range = IGValueRange.createInt(left_value + right_value);
									break;
								case TOK_SUB:
									node.m_value_range = IGValueRange.createInt(left_value - right_value);
									break;
								case TOK_MUL:
									node.m_value_range = IGValueRange.createInt(left_value * right_value);
									break;
								case TOK_LSL:
									node.m_value_range = IGValueRange.createInt(left_value << right_value);
									break;
								case TOK_LSR:
									node.m_value_range = IGValueRange.createInt(left_value >>> right_value);
									break;
								case TOK_ASR:
									node.m_value_range = IGValueRange.createInt(left_value >> right_value);
									break;
								case TOK_BIT_OR:
									node.m_value_range = IGValueRange.createInt(left_value | right_value);
									break;
								case TOK_BIT_AND:
									node.m_value_range = IGValueRange.createInt(left_value & right_value);
									break;
								case TOK_DIV:
									if (right_value == 0) 
									{
										node.m_token.printError("Divide by zero");
										System.exit(1);
									}
									node.m_value_range = IGValueRange.createInt(left_value / right_value);
									break;
							
							}
						}
					}
				}
				else if (type == Type.DOUBLE)  {
					node.m_value_range = DOUBLE_ANY;
				}
				else if (type == Type.INT) {
					node.m_value_range = INT_ANY;
				}
			
				break;
			}
		}
	}
	
	
	public static  int run(IGPackage root, Vector<IGSource> sources)
	{	
		int errors = 0;
		
		///////////
		// Do a first pass of optimizations
		// iterate through each source file and attempt to optimize it
		
		for (IGSource s : sources)
		{
			IGNode node = s.m_root_node;
			if (node != null)
			{		
				// first major flame graph hump
				optimize(s, node);
			}
			else {
				System.out.println("Skipping node: " + s.m_expected_component);
			}
		}
		
		//System.err.println("PART2");		
		///////////
		// Calculate enum values, and static constants
		
		for (IGSource s : sources) 
		{
			IGValueRange pre_vr = null;		// <-- previous value range
			boolean      default_enum_value_used = false;
			
			for (IGMember m : s.m_members) 
			{
				if (m.m_type == IGMember.TYPE_VAR && m.hasModifier(IGMember.STATIC | IGMember.ENUM_VALUE)) 
				{
					if (pre_vr == null) {
						pre_vr = m.m_return_type.getDefaultValueRange();
					}

					IGValueRange vr = m.getDefaultValueRange(pre_vr);
					if (vr == null || !vr.isConcrete()) {
						m.m_name.printError("Could not evaluate enum value at compile time.");
						throw new Error("Fatal Exception");
					}
					else if (pre_vr == vr) {
						if (vr.m_type != IGValueRange.TYPE_INT && 
						    vr.m_type != IGValueRange.TYPE_DOUBLE) {
							m.m_name.printError("Must explicitly set type for these types of enum fields");
							throw new Error("Fatal Exception");
						}
						
						// why isn't this calculated? up front
						// don't increment the value the first round through
						if (default_enum_value_used) {
							vr = IGValueRange.createIncrement(vr);
						}
					}
					
					default_enum_value_used = true;
					m.m_return_value_range = vr;
					pre_vr = vr;
				}
			}
		}
		
		
		//System.err.println("PART3");
		///////////
		// Calculate enum values, and static constants
		
		for (IGSource s : sources) 
		{
			IGValueRange pre_vr = null;
			
			for (IGMember m : s.m_members) 
			{
				if (m.m_type == IGMember.TYPE_VAR && 
					m.hasModifier(IGMember.STATIC | IGMember.FINAL) &&
					!m.hasModifier(IGMember.ENUM_VALUE)) 
				{
				
					
					IGValueRange vr = m.getDefaultValueRange(null);
					if (vr == null || !vr.isConcrete()) {
					}
					else {
						// we found a concrete valu for the static final
						m.m_return_value_range = vr;
					}
					
					//System.err.println("Matches criteria:  " + m.m_name.toString() + " " + m.m_return_value_range);	
					//m.m_return_value_range.debug();
				}
			}
		}
		//System.err.println("PART4");		
		// re-run the optimizer to propagate enum defaults
		for (IGSource s : sources)
		{
			IGNode node = s.m_root_node;
			if (node != null)
			{	
				// second major flame graph hump
				optimize(s, node);
			}
			else {
				System.out.println("Skipping node: " + s.m_expected_component);
			}
		}
		
		///////////////
		// Process default parameters
		///////////////
		
		for (IGSource s : sources) {
			for (IGMember m : s.m_members) {
				if (m.getScopeType() != IGScopeItem.SCOPE_ITEM_MEMBER_FUNCTION &&
					m.getScopeType() != IGScopeItem.SCOPE_ITEM_STATIC_FUNCTION) {
					
					continue;	
				}
				
				for (IGVariable v : m.m_parameters) {
					if (!v.m_has_default) {
						continue;
					}
					
					IGValueRange vr = v.getDefaultValueRange();
					if (vr == null || !vr.isConcrete()) {
						v.m_name.printError("Could not evaluate default value.");
						vr.debug();
						errors ++;						
					}
				}
			
			}
		}
		
		//System.out.println("Value Ranges created: " + IGValueRange.s_created);
		
		return errors;
	}
	
	// toString utils for various targets
	///////////////
	
	public String toAS3String()
	{
		IGValueRange vr = this;
								
		if (vr.m_type == IGValueRange.TYPE_INT) {
			return "" +  vr.intValue();	
		}
		else if (vr.m_type == IGValueRange.TYPE_DOUBLE) {
			return "" + vr.doubleValue();
		}
		else if (vr.m_type == IGValueRange.TYPE_POINTER_NULL) {
			return "null";
		}
		else if (vr.m_type == IGValueRange.TYPE_DELEGATE_NULL) {
			return "null";
		}
		else if (vr.m_type == IGValueRange.TYPE_BOOL_FALSE) {
			return "false";	
		}
		else if (vr.m_type == IGValueRange.TYPE_BOOL_TRUE) {
			return "true";	
		}
		else {
			throw new RuntimeException("Invalid parameter value range");
		}
	}
	
	public String toPHPString()
	{
		IGValueRange vr = this;
								
		if (vr.m_type == IGValueRange.TYPE_INT) {
			return Integer.toString(vr.intValue());	
		}
		else if (vr.m_type == IGValueRange.TYPE_DOUBLE) {
			return "" + vr.doubleValue();
		}
		else if (vr.m_type == IGValueRange.TYPE_POINTER_NULL) {
			return "NULL";
		}
		else if (vr.m_type == IGValueRange.TYPE_DELEGATE_NULL) {
			return "NULL";
		}
		else if (vr.m_type == IGValueRange.TYPE_BOOL_FALSE) {
			return "false";	
		}
		else if (vr.m_type == IGValueRange.TYPE_BOOL_TRUE) {
			return "true";	
		}
		else {
			throw new RuntimeException("Invalid parameter value range");
		}
	}
	
	public String toCPPString()
	{
		IGValueRange vr = this;
								
		if (vr.m_type == IGValueRange.TYPE_INT) {
			return Integer.toString(vr.intValue());	
		}
		else if (vr.m_type == IGValueRange.TYPE_DOUBLE) {
			return "" + vr.doubleValue();
		}
		else if (vr.m_type == IGValueRange.TYPE_POINTER_NULL) {
			return "NULL";
		}
		else if (vr.m_type == IGValueRange.TYPE_DELEGATE_NULL) {
			return "NULL";
		}
		else if (vr.m_type == IGValueRange.TYPE_BOOL_FALSE) {
			return "false";	
		}
		else if (vr.m_type == IGValueRange.TYPE_BOOL_TRUE) {
			return "true";	
		}
		else {
			throw new RuntimeException("Invalid parameter value range");
		}
	}
}