/*
Copyright (c) 2014-2017 Incubator Games Ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

package pass1;


import java.util.*;

public class IGContext
{

	HashMap<Type,Type> m_replace = new HashMap<Type,Type>();
	private boolean m_has_replace = false;
	private boolean m_cot = false;
	
	
	public void clearTypeReplace() {
		m_replace = new HashMap<Type,Type>();
		m_has_replace = false;
	}
	
	public void setCollapseObjectTemplateTypes(boolean v )
	{
		m_cot = v;
	}	
	
	public Type getType(IGSource s) {
	
		if (s.m_template_types.size() == 1) {
			return resolveType(Type.get(s.getInstanceDataType().m_primary, s.m_template_types.get(0), null));
		}
		else if (s.m_template_types.size() == 2) {
			return resolveType(
				Type.get(s.getInstanceDataType().m_primary, 
					s.m_template_types.get(0),  
					s.m_template_types.get(1)
				)
			);		
		}
		else
		{
			return resolveType(s.getInstanceDataType());
		}
	}
	
	public void addTypeReplace(Type pattern, Type replace)
	{
		m_replace.put(pattern, replace);
		m_has_replace = true;
	}
	
	public Type resolveType(Type type) {
		return _resolveType(type, false);
	}

	public Type resolveTypeDoNotCollapse(Type type)
	{
		return _resolveType(type, true);
	}
	
	private Type _resolveType(Type type, boolean do_not_collapse)
	{
		if (type == null) {
			return null;
		}
	
	
		if (!m_cot && (!m_has_replace || type == null)) {
			return type;
		}
		
		
		if (m_replace.containsKey(type)) {
			return m_replace.get(type);
		}	
				
		// no need to molest int, uint, double, void, bool
		if (type.isPrimitive())
		{
			// don't blow away the scope.. as it may be using auxillary functionality
			//type.m_scope = IGScopeItem.PRIMITIVE;
		}
		else if (type.m_primary.equals("<wrapper>")) 
		{
			Type left  = _resolveType(type.m_left, do_not_collapse);
			Type right = _resolveType(type.m_right, do_not_collapse);
				
			type = Type.get("<wrapper>", left, right);
		}
		else if (type.isBuiltIn()) 
		{
			if (type.m_primary.equals("Function") ||
				type.m_primary.equals("StaticFunction") ||
				type.m_primary.equals("MemberFunction") ) 
			{
			
				Type right = _resolveType(type.m_right,do_not_collapse);
				
				
				int count = type.m_left.getMinimumParameters();	//Integer.parseInt(type.m_left.m_primary.substring(1));
				
				
				ArrayList<Type> list = Type.allocTempVector();	//new Vector<Type>();
				for (Type t : type.m_left.m_types) {
					list.add(_resolveType(t,do_not_collapse));
				}
				
				type = Type.get(type.m_primary, Type.get(list, count), right);
				Type.freeTempVector(list);
			}
		}
		else
		{	
			IGScopeItem old_scope = type.m_scope;
		
			if (type.m_left != null) 
			{
				// handle Object, Map, Array, Vector
				Type left  = _resolveType(type.m_left,do_not_collapse);
				Type right = _resolveType(type.m_right,do_not_collapse);
				
				if (m_cot) {
				
					if (!do_not_collapse) 
					{
						if (left != null && left.isObjectInstance()) {
							left = Type.OBJECT;	
						}
						if (right != null && right.isObjectInstance()) {
							right = Type.OBJECT;
						}
					}
					
					if (left != null &&  (left.m_primary.equals("Function") || left.m_primary.equals("StaticFunction"))) {
						left = Type.get("iglang.util.igDelegate");
					}
					
					if (right != null &&  (right.m_primary.equals("Function") || right.m_primary.equals("StaticFunction"))) {
						right = Type.get("iglang.util.igDelegate");
					}
				}
			
				type = Type.get(type.m_primary, left, right);
			}
			
			type.m_scope = old_scope;
		}
		
		return type;
	}
	
	/*
	
	//As far as I can tell.. this was the same as the above except that it didn't substitute iglang.Object's 
	//for everything that extended object
	
	public Type resolveTypeDoNotCollapse(Type type)
	{
		if (type == null) {
			return null;
		}
	
	
		if (!m_cot && (!m_has_replace || type == null)) {
			return type;
		}
		
		
		if (m_replace.containsKey(type)) {
			return m_replace.get(type);
		}
				
		// no need to molest int, uint, double, void, bool
		if (type.isPrimitive())
		{
			// don't blow away the scope.. as it may be using auxillary functionality
			//type.m_scope = IGScopeItem.PRIMITIVE;
		}
		else if (type.m_primary.equals("<wrapper>")) 
		{
			Type left  = resolveType(type.m_left);
			Type right = resolveType(type.m_right);
				
			type = Type.get("<wrapper>", left, right);
			//type.m_scope = IGScopeItem.PRIMITIVE;
		}
		else if (type.isBuiltIn()) 
		{
			if (type.m_primary.equals("Function") ||
				type.m_primary.equals("StaticFunction")) 
			{
			
				Type right = resolveType(type.m_right);
				int count = Integer.parseInt(type.m_left.m_primary.substring(1));
				
				
				ArrayList<Type> list = Type.allocTempVector();	//new Vector<Type>();
				for (Type t : type.m_left.m_types) {
					list.add(resolveType(t));
				}
				
				type = Type.get(type.m_primary, Type.get(list, count), right);
				Type.freeTempVector(list);
				//type.m_scope = IGScopeItem.PRIMITIVE;
			}
		}
		else
		{	
			IGScopeItem old_scope = type.m_scope;
		
			if (type.m_left != null) 
			{
				// handle Object, Map, Array, Vector
				Type left  = resolveTypeDoNotCollapse(type.m_left);
				Type right = resolveTypeDoNotCollapse(type.m_right);
				
				if (m_cot) 
				{
					//if (left != null && left.isObjectInstance()) {
					//	left = Type.get("iglang.Object");
					//}
					//if (right != null && right.isObjectInstance()) {
					//	right = Type.get("iglang.Object");
					//}
					
					if (left != null &&  (left.m_primary.equals("Function") || left.m_primary.equals("StaticFunction"))) {
						left = Type.get("iglang.Delegate");
					}
					
					if (right != null &&  (right.m_primary.equals("Function") || right.m_primary.equals("StaticFunction"))) {
						right = Type.get("iglang.Delegate");
					}
				}
			
				type = Type.get(type.m_primary, left, right);
			}
			//else
			//{
			//	// fully qualify the path to the type
			//	type = Type.get(type.m_primary);
			//}
			
			type.m_scope = old_scope;
		}
		
		return type;
	}
	
	*/
}