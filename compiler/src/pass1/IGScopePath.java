/*
Copyright (c) 2014-2017 Incubator Games Ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

package pass1;

import java.util.Vector;
import java.util.ArrayList;

public class IGScopePath
{
	private ArrayList<Token> m_scope_tokens = new ArrayList<Token>();
	public  String        m_to_string_cache = null;
	public  boolean m_recursive = false;
	
	public void setRecursive(boolean v) {
		m_recursive = v;
	}
	
	public void add(Token t) {
		m_scope_tokens.add(t);
		m_to_string_cache = null;
	}
	
	public ArrayList<Token> getTokens() {
		return m_scope_tokens;
	}
	
	
	public boolean startsWith(ArrayList<String> toks) 
	{
		if (toks.size() > m_scope_tokens.size()) { return false; }
		
		int sz = toks.size();
		for (int i = 0; i < sz; i++) {
			if (!toks.get(i).equals(m_scope_tokens.get(i).m_value)) {
				return false;
			}
		}
		
		return true;
	}
	
	public boolean isIdentical(ArrayList<String> toks) {
		if (toks.size() != m_scope_tokens.size()) { return false; }
		
		int sz = toks.size();
		for (int i = 0; i < sz; i++) {
			if (!toks.get(i).equals(m_scope_tokens.get(i).m_value)) {
				return false;
			}
		}
		
		return true;
	}
	
	public static IGScopePath BUILTINS = null;
	static
	{
		BUILTINS = new IGScopePath();
		BUILTINS.add(Token.createId("iglang"));
		BUILTINS.add(new Token(IGConsts.TOK_MUL, "*"));
	}
	
	public String getRelativePathToRoot()
	{
		String rel = "";
		for (int i = 0; i < m_scope_tokens.size(); i++) {
			rel += "../";
		}
		
		return rel;
	}
	
	public int compareTo(IGScopePath other) {
		
		int this_length = m_scope_tokens.size();
		int other_length = m_scope_tokens.size();
		
		for (int i = 0; i < this_length && i < other_length; i++) {
			int comp = m_scope_tokens.get(i).m_value.compareTo(other.m_scope_tokens.get(i).m_value);
			if (comp != 0) {
				return comp;
			}
		}
		
		return this_length - other_length;
	}
	
	
	
	public IGScopePath() {}
	
	public IGScopePath(Token t) {
		add(t);
	}
	
	public IGScopePath(IGScopePath other, int max_tokens) {
		for (int i = 0; i < max_tokens; i++) {
			m_scope_tokens.add(other.m_scope_tokens.get(i));
		}
	}
	
	public IGScopePath(IGScopePath other, Token token)
	{ 
		for (int i = 0; i < other.m_scope_tokens.size(); i++) {
		m_scope_tokens.add(other.m_scope_tokens.get(i));
		}
		m_scope_tokens.add(token);
	}
	
	public boolean endsWith(String s) {
		return m_scope_tokens.get(m_scope_tokens.size() - 1).m_value.equals(s);
	}
	
	public boolean matches(IGScopePath other)
	{
		final int this_size = this.m_scope_tokens.size();
		final int other_size = other.m_scope_tokens.size();
	
		// Recursive glob behaviour
		if (other.m_recursive) 
		{
			//  a.b.c
			// matches a.*   RECURSIVE
			// matches a.b.* RECURSIVE OR NOT
			if (other_size > this_size) {
				return false;
			}
		}
		else {
			// Simple glob or non recursive behaviour
			if (other_size != this_size) {
				return false;
			}
		}
		
		for (int i = 0; i < other_size; i++) 
		{
			Token left = m_scope_tokens.get(i);
			Token right = other.m_scope_tokens.get(i);
			
			if (right.m_value.equals("*")) {
				// allow for the glob operator
				// yes this'll allow matching a.b.c with a.*.c
			}
			else if (!left.m_value.equals(right.m_value)) {
				return false;
			}
		}
		
		return true;
	
	}
	
	
	public boolean sameScope(IGScopePath other)
	{
		if (other.m_scope_tokens.size() != this.m_scope_tokens.size()) return false;
		for (int i = 0; i < m_scope_tokens.size(); i++) {
			Token left = m_scope_tokens.get(i);
			Token right = other.m_scope_tokens.get(i);
			if (!left.m_value.equals(right.m_value)) {
				return false;
			}
		}
		return true;
	}
	

	
	public boolean sameScope(IGScopePath other, int max_tokens)
	{
		if (other.m_scope_tokens.size() < max_tokens ||
			 this.m_scope_tokens.size() < max_tokens) 
			return false;
			
		for (int i = 0; i < max_tokens; i++) {
			Token left = m_scope_tokens.get(i);
			Token right = other.m_scope_tokens.get(i);
			if (!left.m_value.equals(right.m_value)) {
				return false;
			}
		}
		return true;
	}
	
	public int size() {
		return m_scope_tokens.size();
	}
	
	public void debug()
	{
		System.out.print("\t");
		for (Token t : m_scope_tokens) {
			System.out.print(t.m_value);
			System.out.print(".");
		}
		System.out.println();
	}
	
	
	public boolean startsWith(IGScopePath sp)
	{
		if (m_scope_tokens.size() < sp.m_scope_tokens.size()) {
			return false;
		}
		
		for (int i = 0; i < sp.m_scope_tokens.size(); i++) {
			Token left = m_scope_tokens.get(i);
			Token right = sp.m_scope_tokens.get(i);
			if (!left.m_value.equals(right.m_value)) {
				return false;
			}
		}
		
		return true;
	}
	
	@Override public boolean equals(Object other) {
		IGScopePath sp = (IGScopePath) other;
		
		int sz = m_scope_tokens.size();
		if (sz != sp.m_scope_tokens.size()) {
			return false;
		}
		
		for (int i = 0; i < sz; i++) {
			if (!m_scope_tokens.get(i).m_value.equals(sp.m_scope_tokens.get(i).m_value)) {
				return false;
			}
		}
		
		return true;
	}
	
	@Override public String toString() 
	{
		if (m_to_string_cache == null)
		{
			StringBuilder sb = new StringBuilder();
			
			final int tok_length = m_scope_tokens.size();
			for (int i = 0; i < tok_length; i++)
			{	
				Token t = m_scope_tokens.get(i);
				if (i != 0) {
					sb.append(".");
				}
				sb.append(t.m_value);		
			}
			m_to_string_cache = sb.toString();
		}
		
		return m_to_string_cache;
	}

	public String toStringWithSeperator(String sep, String last_sep, boolean exclude_glob)
	{
		StringBuilder result = new StringBuilder();
		
		int len = m_scope_tokens.size();
		if (isGlob() && exclude_glob) len -= 1;
		
		for (int i = 0; i < len; i++) {
			if (i != 0 && i == len -1) {
				result.append(last_sep);
			}
			else if (i != 0) {
				result.append(sep);
			}
			result.append(m_scope_tokens.get(i).m_value);
		}
		return result.toString();
	}
	
	public boolean isGlob()
	{
		return m_scope_tokens.get(m_scope_tokens.size() -1).m_type == IGConsts.TOK_MUL;
	}
}

