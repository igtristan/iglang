/*
Copyright (c) 2014-2017 Incubator Games Ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

package util;

import java.util.ArrayList;

class Entry
{
	String key;
	int    hash;
	int    value;
	Entry  next = null;
	
	public void init(String k, int h, int v, Entry n) {
		key = k;
		hash = h;
		value = v;
		next = n;
	}
}

// modified from
//http://grepcode.com/file/repository.grepcode.com/java/root/jdk/openjdk/6-b14/java/util/Hashtable.java

public final class StringIntHashtable
{
	private Entry [] table = null;
	private static ArrayList<Entry> s_free = new ArrayList<Entry>();
	
	public StringIntHashtable()
	{
		table = new Entry[31];
	}
	
	public StringIntHashtable(int sz)
	{
		table = new Entry[sz];
	}
	
	public void clear() {
		for (int i = 0; i < table.length; i++) {
			if (table[i] != null) 
			{
				Entry tmp = table[i];
				while (tmp != null) {
					Entry rem = tmp;
					s_free.add(rem);
					tmp = tmp.next;
				}
				table[i] = null;
			}
		}
	}
	
	public static Entry allocEntry(String k, int h, int v, Entry n) {
		Entry e = (s_free.size() == 0) ? 
						new Entry() : 
						s_free.remove(s_free.size() - 1);
		e.init(k,h,v,n);
		return e;
	}
	
	
	
	public boolean containsKey(String key) 
	{
		 Entry []  tab = table;
         int hash = key.hashCode();
         int index = (hash & 0x7FFFFFFF) % tab.length;
         for (Entry e = tab[index] ; e != null ; e = e.next) {
             if ((e.hash == hash) && e.key.equals(key)) {
                 return true;
             }
         }
         return false;
	}
	
	public void put(String key, int value) {
		Entry tab[] = table;
         int hash = key.hashCode();
         int index = (hash & 0x7FFFFFFF) % tab.length;
         for (Entry e = tab[index] ; e != null ; e = e.next) {
             if ((e.hash == hash) && e.key.equals(key)) {
                 e.value = value;
				 return;
             }
         }
         
         tab[index]  = allocEntry(key, hash, value, tab[index]);
	}
	
	/**
	 * get the existing value, if not found set the value specified and return that value
	 */
	
	public int getput(String key, int value) {
		Entry tab[] = table;
         int hash = key.hashCode();
         int index = (hash & 0x7FFFFFFF) % tab.length;
         for (Entry e = tab[index] ; e != null ; e = e.next) {
             if ((e.hash == hash) && e.key.equals(key)) {
                	return e.value;
             }
         }
         
         tab[index]  = allocEntry(key, hash, value, tab[index]);
         return value;
	}
	
	
	public int get(String key) {
		 Entry []  tab = table;
         int hash = key.hashCode();
         int index = (hash & 0x7FFFFFFF) % tab.length;
         for (Entry e = tab[index] ; e != null ; e = e.next) {
             if ((e.hash == hash) && e.key.equals(key)) {
                	return e.value;
             }
         }
         
         throw new RuntimeException("StringIntHashtable value not found");
	}

}