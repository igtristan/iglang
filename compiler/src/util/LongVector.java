/*
Copyright (c) 2014-2017 Incubator Games Ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

package util;

public final class LongVector
{
	int m_index = 0;
	long [] m_data = new long[16];
	
	public int size() {
		return m_index;
	}
	
	public void clear() {
		m_index = 0;
	}
	
	public void add(long value) 
	{
		if (m_index == m_data.length) 
		{
			int new_cap = (m_data.length * 4) / 3;
			long [] old_data = m_data;
			m_data = new long[new_cap];
			
			System.arraycopy(old_data, 0, m_data, 0, m_index);
		}
		m_data[m_index] = value;
		m_index++;
	}
	
	public int indexOf(long value) {
		long [] d = m_data;
		for (int i = m_index - 1; i >= 0; i--) {
			if (d[i] == value) {
				return i;
			}
		}
		return -1;
	}
	
	public long get(int index) {
		return m_data[index];
	}

}