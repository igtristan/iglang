/*
Copyright (c) 2014-2017 Incubator Games Ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

package util;

import java.io.*;

public class LEDataOutputStream extends FilterOutputStream implements DataOutput
{
	/////////////////////////////////////////////////////////////////////////
	// Construction
	/////////////////////////////////////////////////////////////////////////
    public LEDataOutputStream(OutputStream out)
    {
        super(out);
    }

	/////////////////////////////////////////////////////////////////////////
	// DataOutput Implementation
	/////////////////////////////////////////////////////////////////////////
	/**
	 * Writes the eight low- order bits of the argument v.
	 **/
	public void	writeByte(int b) throws IOException
	{
        this.write(b);
	}

	/**
	 * Writes two bytes to the output stream to represent the value of the
	 * argument.
	 **/
	public void	writeShort(int i) throws IOException
	{
        this.write(i & 0xff);
        this.write((i >> 8) & 0xff);
	}

	/**
	 * Writes an int value, which is comprised of four bytes, to the output
	 * stream.
	 **/
	public void	writeInt(int i) throws IOException
	{
    	this.write((i >> 0)  & 0xff);
    	this.write((i >> 8)  & 0xff);
    	this.write((i >> 16) & 0xff);
    	this.write((i >> 24) & 0xff);
	}

	/**
	 * Writes a long value, which is comprised of eight bytes, to the output
	 * stream.
	 **/
	public void	writeLong(long l) throws IOException
	{
        writeInt((int) (l & 0xffffffff));
        writeInt((int) ((l >> 32) & 0xffffffff));
	}

	/**
	 * Writes a float value, which is comprised of four bytes, to the
	 * output stream.
	 **/
	public void	writeFloat(float f) throws IOException
	{
        int i = Float.floatToIntBits(f);
        this.write((i >> 0)  & 0xff);
    	this.write((i >> 8)  & 0xff);
    	this.write((i >> 16) & 0xff);
    	this.write((i >> 24) & 0xff);
	}

	/**
	 * Writes a double value, which is comprised of eight bytes, to the
	 * output stream.
	 **/
	public void	writeDouble(double d) throws IOException
	{
        long l = Double.doubleToLongBits(d);
        writeInt((int) (l & 0xffffffff));
        writeInt((int) ((l >> 32) & 0xffffffff));
	}

	/**
	 * Writes a boolean value to this output stream.
	 * This writes one byte to the stream - 1 for true, 0 for false.
	 **/
	public void	writeBoolean(boolean b) throws IOException
	{
		if(b) {
			this.write(1);
		} else {
			this.write(0);
		}
	}


	/**
	 * Writes a char value, which is comprised of two bytes, to the output
	 * stream.
	 **/
	public void	writeChar(int v) throws IOException
	{
		this.writeShort(v);
	}

	/**
	 * Writes every character in the string s, to the output stream, in
	 * order, two bytes per character.
	 **/
	public void	writeChars(String s) throws IOException
	{
		for(int i=0; i<s.length(); ++i)
		{
			short unicode_id = (short)s.charAt(i);
			this.writeShort(unicode_id);
		}
	}

	/**
	 * Writes two bytes of length information to the output stream, followed
	 * by the modified UTF-8 representation of every character in the string
	 * s.
	 **/
	public void	writeUTF(String s) throws IOException
	{
		byte[] b = s.getBytes("UTF-8");
		this.writeShort(b.length);
		this.write     (b);
	}

	/**
	 * Writes a string to the output stream.
	 * This writes the string as ASCII.  The upper eight bytes of each char
	 * are discared.
	 **/
	public void	writeBytes(String s) throws IOException
	{
		for(int i=0; i<s.length(); ++i)
		{
			byte  val        = (byte)s.charAt(i);
			this.writeByte(val);
		}
	}

	/**
	 * Writes the eight low-order bits of the argument b.
	 **/
	public void	write(int b) throws IOException
	{
        out.write(b);
	}

	/**
	 * Writes to the output stream all the bytes in array b.
	 **/
	public void write(byte[] b) throws IOException
	{
		this.write(b, 0, b.length);
	}

	/**
	 * Writes len bytes from array b, in order, to the output stream.
	 **/
	public void	write(byte[] b, int off, int len) throws IOException
	{
		int sidx = off;
		int eidx = off + len;
		for(int i=sidx; i<eidx; ++i) {
			this.write(b[i]);
		}
	}
	
	
	public void writeCString(String s, int len) throws IOException{
		
		int max_len = s.length();
		if (len < max_len) {
			max_len = len;
		}
		int i = 0;
		for (; i < max_len; i++) {
			this.write((byte)s.charAt(i));
		}
		
		for (; i < len; i++) {
			this.write(0);
		}
	}
}


