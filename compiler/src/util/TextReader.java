/*
Copyright (c) 2014-2017 Incubator Games Ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

package util;

import java.io.*;
	
public class TextReader extends BufferedReader
{
	public TextReader(FileReader fr) {
		super(fr);
	}


	private String m_current_line = null;
	private int    m_line_no = 0;
	
	
	public String getCurrentLine() {
		return m_current_line;
	}
	
	public int getLineNumber() {
		return m_line_no;
	}
	
	public final String readLine() throws IOException {
		m_current_line = super.readLine();
		m_line_no ++;
		return m_current_line;
	}
	
	
	public final int readASMLineToks(String [] dst) throws IOException
	{
		int start_position = 0;
		int end_position = 0;
		
		String line = null;
		
		do
		{
			m_current_line = super.readLine();
			m_line_no ++;
			line = m_current_line;
			
			if (line == null) {
				return 0;
			}
			int line_len = line.length();
			
			start_position = 0;
			end_position   = line_len;
		
			
			for (int i = 0; i < line_len; i++) {
				if (line.charAt(i) == '#') {
					end_position = i;
					break;
				}
			}
		
			for (int i = end_position-1; i >= 0; i--) {
				char c = line.charAt(i);
				if (c != '\t' && c != ' ') {
					break;
				}
				else {
					end_position --;
				}
			}
		
			for (int i = 0; i < end_position; i++) {
				char c = line.charAt(i);
				if (c != '\t' && c != ' ') {
					break;
				}
				else {
					start_position++;
				}
			}
			
		}
		while (end_position - start_position <= 0);
		
		// null out all unneeded entries
		for (int i = dst.length - 1; i >= 0; i--) {
			dst[i] = null;
		}
		
		// todo parcel out each individual token
		// currently we have the range that the tokens can exist within
		// RANGE = [start_position end_position)
		// this range is guaranteed to be trimmed
		
		int dst_index = 0;
		int last_start = start_position;
		boolean last_was_whitespace = true;
		
		for (int i = start_position; i < end_position; i++) 
		{
			char c = line.charAt(i);
			boolean is_whitespace = (c == ' ' || c == '\t');
			
			if (is_whitespace && !last_was_whitespace) 
			{
				dst[dst_index] = line.substring(last_start, i);
				dst_index++;
				
				last_was_whitespace = true;
			}
			else if (!is_whitespace && last_was_whitespace)
			{
				last_start = i;
				last_was_whitespace = false;
			}
		}
		
		dst[dst_index] = line.substring(last_start, end_position);
		dst_index++;
		
		return dst_index;
	}
	
	public final int readASMLineToks(int [] dst) throws IOException
	{
		int start_position = 0;
		int end_position = 0;
		
		String line = null;
		
		do
		{
			m_current_line = super.readLine();
			m_line_no ++;
			line = m_current_line;
			
			if (line == null) {
				return 0;
			}
			int line_len = line.length();
			
			start_position = 0;
			end_position   = line_len;
		
			
			for (int i = 0; i < line_len; i++) {
				if (line.charAt(i) == '#') {
					end_position = i;
					break;
				}
			}
		
			for (int i = end_position-1; i >= 0; i--) {
				char c = line.charAt(i);
				if (c != '\t' && c != ' ') {
					break;
				}
				else {
					end_position --;
				}
			}
		
			for (int i = 0; i < end_position; i++) {
				char c = line.charAt(i);
				if (c != '\t' && c != ' ') {
					break;
				}
				else {
					start_position++;
				}
			}
			
		}
		while (end_position - start_position <= 0);
		
		// null out all unneeded entries
		for (int i = dst.length - 1; i >= 0; i--) {
			dst[i] = 0;
		}
		
		// todo parcel out each individual token
		// currently we have the range that the tokens can exist within
		// RANGE = [start_position end_position)
		// this range is guaranteed to be trimmed
		
		int dst_index = 0;
		int last_start = start_position;
		boolean last_was_whitespace = true;
		
		for (int i = start_position; i < end_position; i++) 
		{
			char c = line.charAt(i);
			boolean is_whitespace = (c == ' ' || c == '\t');
			
			if (is_whitespace && !last_was_whitespace) 
			{
				dst[dst_index] = (last_start << 16) | i;
				dst_index++;
				
				last_was_whitespace = true;
			}
			else if (!is_whitespace && last_was_whitespace)
			{
				last_start = i;
				last_was_whitespace = false;
			}
		}
		
		dst[dst_index] = (last_start << 16) | end_position;
		dst_index++;
		
		return dst_index;
	}
	
	
	
	
	public String readASMLine() throws IOException 
	{
		int start_position = 0;
		int end_position = 0;
		String line = null;
		
		
		do
		{
			line = readLine();
			if (line == null) {
				return null;
			}
			int line_len = line.length();
			
			start_position = 0;
			end_position   = line_len;
		
			
			for (int i = 0; i < line_len; i++) {
				if (line.charAt(i) == '#') {
					end_position = i;
					break;
				}
			}
		
			for (int i = end_position-1; i >= 0; i--) {
				char c = line.charAt(i);
				if (c != '\t' && c != ' ') {
					break;
				}
				else {
					end_position --;
				}
			}
		
			for (int i = 0; i < end_position; i++) {
				char c = line.charAt(i);
				if (c != '\t' && c != ' ') {
					break;
				}
				else {
					start_position++;
				}
			}
			
		}
		while (end_position - start_position <= 0);
		
		////System.out.println(" " + start_position + " ==> " + end_position);
		//System.out.println("readASMLine src: '" + line + "'");

		String result =  line.substring(start_position, end_position);
		//System.out.println("readASMLine result: '" + result + "'");

		return result;
		
		/*
		while (
		
		
				line = line.trim();
				
				// skip comments, and blank lines
				if (line.startsWith("#") || line.length() == 0) {
					line = br.readASMLine();
					continue;
				}
				
				
				processed_line = line;
				
				// throw away terminating comments
				if (line.indexOf("#") != -1) {
					line = line.substring(0, line.indexOf("#"));
				}
				
				String [] toks = IGAssembler.WHITESPACE_SPLIT.split(line.trim());	// need a better whitespace regex
		*/	
	}
}