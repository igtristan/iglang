/*
Copyright (c) 2014-2017 Incubator Games Ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

package util;

public class ByteArray extends java.io.OutputStream
{
	// OUTPUT STREAM CODE
	@Override public void close() {}
	@Override public void flush() {}
	
	@Override public void write(int b) 
	{ 
		ensureCapacity(position + 1);
		data[position] = (byte)b;
		position ++;
		if (position > length) {
			length = position;
		}
	}

	@Override public void write(byte [] b) {
		ensureCapacity(position + b.length);
		for (int i = 0; i < b.length; i++) {
			write(b[i]);
		}
	}

	@Override public void write(byte [] b, int off, int len) {
		ensureCapacity(position + len);
		for (int i = off; i < off + len; i++) {
			write(b[i]);
		}
	}
	
	
	public void writeTo(java.io.OutputStream os) throws java.io.IOException {
		os.write(data, 0, length);
	}
	/////////////////////



	private int position = 0;
	private int length = 0;
	private int capacity = 0;
	private byte [] data = null;
	private boolean  m_big_endian = true;
	
	
	public ByteArray()
	{
		capacity = 256;
		data = new byte[256];
	}
	
	public ByteArray(java.io.File file) throws java.io.IOException
	{
		byte[] fileData = new byte[(int) file.length()];
    	java.io.DataInputStream dis = new java.io. DataInputStream(new java.io.FileInputStream(file));
	    dis.readFully(fileData);
	    dis.close();
	    data = fileData;
	    position = 0;
	    capacity = data.length;
	    length = data.length;
	}
	
	public void setBigEndian(boolean be) {
		m_big_endian = be;
	}
	

	private void ensureCapacity(int len) {
		if (len > capacity) 
		{
			int new_cap = (capacity * 4) / 3;
			if (new_cap < len + 8) {
				new_cap = len + 8;
			}
		
			// need to realloc a new temporary buffer
			byte [] old_data = data;
			data = new byte[new_cap];
			capacity = data.length;
			
			
			System.arraycopy(old_data, 0, data, 0, length);
			//for (int i = 0; i < length; i++) {
			//	data[i] = old_data[i];
			//}
		}
	}

	public void writeBytes(ByteArray other, int other_position, int other_length)
	{
		ensureCapacity(length + other_length);
		
		for (int i = 0; i < other_length; i++) {
			data[position] = other.data[other_position + i];
			position++;
		}
		
		if (length < position) {
			length = position;
		}
	}
	
	public int getBytesAvailable() {
		return length - position;
	}

	public void setPosition(int value) {
		position = value;
	}
	
	public int  getPosition() {
		return position;
	}
	
	public int getLength() {
		return length;
	}

	public String readUTFBytes(int count) {
		try
		{
			String s = new String(data, position, count, "UTF-8");
			position += count;
			if (position >= length) {
				throw new RuntimeException("Invalid readUTFBytes");
			}
			
			return s;
		}
		catch (java.io.UnsupportedEncodingException ex) {
			throw new RuntimeException("Invalid readUTFBytes");
		}
		
	}
	
	public String readUTF() {
		int count = readUnsignedShort();
		try
		{
			String s = new String(data, position, count, "UTF-8");
			position += count;
			if (position >= length) {
				throw new RuntimeException("Invalid readUTFBytes");
			}
			
			return s;
		}
		catch (java.io.UnsupportedEncodingException ex) {
			throw new RuntimeException("Invalid readUTFBytes");
		}
	}
	
	public String writeUTF(String s) {
		
		try
		{
			byte [] data = s.getBytes("UTF-8");
			ensureCapacity(position + 2 + data.length);
		
			writeShort(data.length);
			for (int i = 0; i < data.length; i++) {
				writeByte(data[i]);
			}
		}
		catch (java.io.UnsupportedEncodingException ex) {
			ex.printStackTrace();
			throw new RuntimeException("Invalid writeUTF");
		}

		return s;	
	}
	
	//////////////////////////////////////////////////////////

	public int readByte()
	{
		if (position == length) {
			throw new RuntimeException("Exceeded bounds of the byte array");
		}
		
		int val = data[position];
		position ++;
		return val;
	}
	
	public int readUnsignedByte()
	{
		if (position == length) {
			throw new RuntimeException("Exceeded bounds of the byte array");
		}
		int val = data[position];
		position ++;
		return val & 0xff;
	}
	
	public int writeByte(int b) 
	{
		ensureCapacity(position + 1);
		data[position] = (byte)b;
		position ++;
		if (position > length) {
			length = position;
		}
		
		return b;
	}

	//////////////////////////////////////////////////////////

	public int readShort()
	{
		if (m_big_endian) {
			return (readByte() << 8) | readUnsignedByte();
		}
		else
		{
			return readUnsignedByte() | (readByte() << 8);
		}
	}
	
	public int readUnsignedShort()
	{
		if (m_big_endian) {
			return (readUnsignedByte() << 8) | readUnsignedByte();
		}
		else
		{
			return readUnsignedByte() | (readUnsignedByte() << 8);
		}
	}
	
	public int writeShort(int b) 
	{
		ensureCapacity(position + 2);
		if (m_big_endian) {
			writeByte(b >> 8);
			writeByte(b);
		}
		else
		{
			writeByte(b);
			writeByte(b >> 8);
		}
		return b;
	}
	
	////////////////////////////////////////////////////////
	

	public int readUnsignedInt()
	{
		if (m_big_endian) {
			return  (readUnsignedByte() << 24) |  (readUnsignedByte() << 16) | 
					(readUnsignedByte() << 8) | (readUnsignedByte() << 0);
		}
		else
		{
			return  (readUnsignedByte() << 0) |  (readUnsignedByte() << 8) | 
					(readUnsignedByte() << 16) | (readUnsignedByte() << 24);
		}
	}
	
	public int readInt()
	{
		if (m_big_endian) {
			return  (readByte() << 24) |  (readUnsignedByte() << 16) | 
					(readUnsignedByte() << 8) | (readUnsignedByte() << 0);
		}
		else
		{
			return  (readUnsignedByte() << 0) |  (readUnsignedByte() << 8) | 
					(readUnsignedByte() << 16) | (readByte() << 24);
		}
	}
	
	public int writeInt(int b) 
	{
		ensureCapacity(position + 4);
		if (m_big_endian) {
			writeByte(b >> 24);
			writeByte(b >> 16);
			writeByte(b >> 8);
			writeByte(b >> 0);
		}
		else
		{
			writeByte(b >> 0);
			writeByte(b >> 8);
			writeByte(b >> 16);
			writeByte(b >> 24);
		}
		return b;
	}
	
	public void writeLong(long l)
	{
		ensureCapacity(position + 8);
		
		if (m_big_endian) {
			writeInt((int)((l >> 32) & 0xffffffff));
			writeInt((int)(l         & 0xffffffff));
		}
		else
		{
			writeInt((int)(l         & 0xffffffff));
			writeInt((int)((l >> 32) & 0xffffffff));
		}
	}
	
	
	/////////////////////////////////////////////////////////
	
	public float readFloat()
	{
		int v = readInt();
		return Float.intBitsToFloat(v);
	}
	
	
	public float writeFloat(float f) 
	{
		writeInt(Float.floatToIntBits(f));
		return f;
	}
}