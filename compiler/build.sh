#!/bin/sh

compile_jar()
{
    javac  -source 1.6 -target 1.6 -g -classpath ./src -d ./bin ./src/$1.java || exit 1
    rsync -av --exclude=".*" data/ bin/data
    
    
    cd bin
	jar cvfe ../jar/$1.jar $1 .
	#cd ..
    #cp ./jar/$1.jar ../$1.jar
}



main()
{
   [ -d bin ] || mkdir bin
   [ -d jar ] || mkdir jar
    compile_jar 		igc || exit 1
}


main;
