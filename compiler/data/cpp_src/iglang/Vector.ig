/*NO-OUTPUT*/
package iglang;

import iglang.unsafe.*;

public class Vector<T1> extends Object
{
	public function iterator(): VectorIterator<T1> { throw new Error("Incomplete"); }


	public function constructor(num_elements : int=8) {}
	
	public function get length(): uint { return 0; }
	public function set length(len : uint): void {}
	
	public function pop(): T1 { throw new Error("Incomplete"); }
	public function push(arg: T1): uint { return 0;}
	public function shift(): T1 { throw new Error("Incomplete"); }
	public function unshift(arg: T1) : uint {return 0;}
	
	/**
	 * Searches for an item in the Vector and returns the index position of 
	 * the item. The item is compared to the Vector elements using strict 
	 * equality (===). 
	 *
	 * @param searchElement The item to find in the Vector.
	 *
	 * @param fromIndex     The location in the Vector from which to start 
	 *                      searching for the item. If this parameter is 
	 *                      negative, it is treated as length + fromIndex, 
	 *                      meaning the search starts -fromIndex items from 
	 *                      the end and searches from that position forward 
	 *                      to the end of the Vector.
	 *
	 * @return              A zero-based index position of the item in the 
	 *                      Vector. If the searchElement argument is not 
	 *                      found, the return value is -1. 	 
	 **/
	public function indexOf(searchElement:T1, fromIndex:int=0) : int { return -1;}
	
	/**
	 * This maps to splice: http://help.adobe.com/en_US/FlashPlatform/reference/actionscript/3/Vector.html#splice%28%29
	 * @param startIndex  An integer that specifies the index of the element in the Vector where the insertion or deletion begins. You can use a negative integer to specify a position relative to the end of the Vector (for example, -1 for the last element of the Vector).
	 * @param deleteCount An integer that specifies the number of elements to be deleted. This number includes the element specified in the startIndex parameter. If the value is 0, no elements are deleted.
	 **/
	public function delete(startIndex:int, deleteCount:uint=1) : void {}

	/**
	 * The way you insert objects into a vector in AS3 requires passing them to splice using variadics.
	 * Tritsan says BOO on those things so we say YAY on special thingy bingy irky worky.
	 **/
	public function insert(index:int, obj:T1) : void {}
	
	
	/**
	 * Maps to "vector.IndexOf(obj) != -1".
	 * It's just cleaner code with this explicit function...
	 **/
	public function contains(obj:T1) : bool { return false;}
	
	/**
	 *
	 * Calls toString on each element and concatenates the values with a seperator
	 **/
	 
	public function join(sep : String) : String { return null; }
	
	
		/*
	 * Sort the vector
	 */
	public function sort(sort_function : StaticFunction<(T1,T1):int>) : void {  }
	
	public function get[](idx : int) : T1 {
		throw new Error("NO OUTPUT");
	}
	
	public function set[](idx : int, value : T1) : void {
		throw new Error("NO OUTPUT");
	}	
}
