/*NO-OUTPUT*/
package iglang;

import iglang.unsafe.*;

public class Array<T1> extends Object
{
	public function iterator(): ArrayIterator<T1> { throw new Error("Incomplete"); }
	
	public function Array(num_elements : int) {}
	
	// make it so you can't resize an array
	public function get length(): int {return 0;}
	
	public function get[](idx : int) : T1 {
		throw new Error("NO OUTPUT");
	}
	
	public function set[](idx : int, value : T1) : void {
		throw new Error("NO OUTPUT");
	}
}