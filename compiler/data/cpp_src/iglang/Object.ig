/*NO-OUTPUT*/
package iglang;

public class Object
{
	public function Object() {}
	public static function trace(v : String): void {}
	
	// required for c++ and java implementations
	public function hashCode(): uint {return 0;}
	public function equals(v : Object): bool {return false;}
	
	//public function toString() : String {}
}
