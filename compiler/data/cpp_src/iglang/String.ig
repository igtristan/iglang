/*NO-OUTPUT*/
package iglang;

public class String extends Object
{
	public function get length(): uint { return 0;}
	
	public function indexOf(val:String, startIndex:int=0):int {return 0;}
	public function charCodeAt(index:int=0):int {return 0;}
	public function charAt(index:int=0):String {return null;}
	
	public function split(delimiter:String, limit:double = 0x7fffffff): Array<String> {return null;}
	
	
	public function lastIndexOf(val:String, startIndex:double = 0x7FFFFFFF) : int {return 0;}
	
	public function substr(startIndex:double = 0, len:double = 0x7fffffff) : String {return null;}
	
	public function replace(pattern:String, repl:String) : String {return null;}
	
	public function toLowerCase() : String {return null;}
	
	public function localeCompare(other : String) : int {return 0;}

	/**
	 * [static] Returns a string comprising the characters represented by the Unicode character codes in the parameters.
	 **/
	public static function fromCharCode(val:uint) : String {return null;}
	
	
}
