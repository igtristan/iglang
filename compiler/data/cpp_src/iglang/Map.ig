/*NO-OUTPUT*/
package iglang;

import iglang.unsafe.*;

public class Map<T1,T2> extends Object
{
	public function iterator() : MapIterator<T1,T2> { throw new Error("Incomplete"); }
	public function Map() {}
	public function containsKey(key : T1 ) : bool { return false;}
	public function deleteKey(key : T1 ) : void {}
	
	public function get[](idx : T1) : T2 {
		throw new Error("NO OUTPUT");
	}

	public function set[](idx : T1, value : T2) : void {
		throw new Error("NO OUTPUT");
	}
}