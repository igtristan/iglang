<?php




mb_internal_encoding('UTF-8');

// HRM this is what I previously used for block star battle
//	date_default_timezone_set('UTC');

date_default_timezone_set('GMT');

// iglang auto loader
/////////////////////

function __ig_autoload($name) {
    
//    if ($name == 'web\AmazonDynamoDB') return false;
    
	$name = str_replace("\\", "/", $name) . ".php";
	include($name);
}

spl_autoload_register("__ig_autoload");


function __ig_newArray($size, $default)
{
	if ($size == 0) {
		return array();
	}
	else
	{
		return array_fill(0, $size, $default);
	}
}

function __ig_containsKey(&$array, $key)
{
	// an array '==' null if it is empty or equals null
	if ($array == NULL) {
		return false;
	}
	$result =  array_key_exists($key, $array);
	return $result;
}

function __ig_indexOf(&$array, $value)
{
	$key = array_search($value, $array, false); // ouch
	if ($key === false) return -1;
	return $key;
}

function __ig_stringIndexOf($string, $value, $start_at = 0)
{
	$pos = mb_strpos($string, $value, $start_at);
	if ($pos === false) return -1;
	return $pos;
}

function __ig_deleteKey(&$array, $key)
{
	unset($array[$key]);
}

function __ig_delete(&$array, $index, $delete_count = 1)
{
	array_splice($array, $index, $delete_count);
}

function __ig_setArrayLength(&$array, $length, $defaux)
{
	$cnt = count($array);
	$to_pop = $cnt - $length;
	$to_push = $length - $cnt;
	while ($to_pop > 0) {
		array_pop($array);
		$to_pop --;
	}

	while ($to_push > 0) {
		array_push($array, $defaux);
		$to_push --;
	}	
}

function __ig_split($string, $delim) 
{
	return explode($delim, $string);
}
/*
try {
	

	$data = str_split('some data');
	
	$ba = new platform\iglang\ByteArray();
	$ba->__ig_init($data, count($data));

    $ba->writeShort(-32);
    $ba->writeUTF('this is a string © 12345678');

    
    $ba->position__set(0);
    echo  '' . $ba->readShort() . "\n";
    echo  '' . $ba->readUTF() . "\n";

} catch (Exception $e) {
    echo $e->getMessage(), "\n";
}
*/
