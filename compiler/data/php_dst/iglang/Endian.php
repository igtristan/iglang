<?php
namespace iglang;

class Endian
{
	const BIG_ENDIAN    = 0;
	const LITTLE_ENDIAN = 1;
}

