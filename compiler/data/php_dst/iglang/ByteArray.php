<?php

namespace iglang;

class ByteArray implements \ArrayAccess
{
	private $m_capacity = 0;
	private $m_length   = 0;
	private $m_index    = 0;
	private $m_data     = NULL;			// now a SPL fixed array
	private $m_endian   = 0;		// 0 = BE,  1 = LE


	function __construct($data = NULL, $length = 0)
	{
		if (is_null($data)) 
		{
			$length = 256;
			$this->m_data 		= str_repeat("\0", $length);	//new \SplFixedArray($length);	//(array_fill(0, $length, 0);
			$this->m_length 	= 0;
			$this->m_capacity 	= $length;
		}
		else
		{
			// arrays are 
			$this->m_data           = str_repeat("\0", $length);
			for ($i = 0; $i < $length; $i++) {
				$this->m_data[$i] = chr($data[$i]);
			}
			//$this->m_data 		= implode($data);	//\SplFixedArray::fromArray($data);
			$this->m_length 	= $length;
			$this->m_capacity 	= $length;
		}
		
		
		$this->m_index 	= 0;
		$this->m_endian    = 0;
		return $this;
	}

	function debug()
	{
		echo 'capacity: ' . $this->m_capacity . "\n";
		echo 'length: ' . $this->m_length . "\n";
		echo 'index: ' . $this->m_index . "\n";
		echo 'endian: ' . $this->m_endian . "\n";
	
		for ($i = 0; $i < $this->m_length; $i++) {
			echo '' . $i . '> ' . $this->m_data[$i] . "\n";
		}
	}

	function ensureCapacity($cap)
	{
		if ($this->m_capacity >= $cap) return;
	
		$old_capacity = $this->m_capacity;
		$this->m_capacity = $cap + 8;
		
		$to_append = $this->m_capacity - $old_capacity;
		
		// grow the size of the underlying string ??
		$this->m_data = $this->m_data . str_repeat("\0", $to_append);
	
		//$this->m_data->setSize($this->m_capacity);
	}


	
	///////
	// php arrayaccess
	/////
	
	public function offsetSet($offset, $value) {
	 	$this->ensureCapacity($offset + 1);
        $this->m_data[$offset] = chr($value);
    }
    
    public function offsetExists($offset) {
        return 0 <= $offset && $offset < $this->m_length;
    }
    public function offsetUnset($offset) {
       
    }
    public function offsetGet($offset) {
    	if ($offset >= $this->m_length) {
    		throw new Exception("Exceeded bounds of byte array");
    	}
		return ord($this->m_data[$offset]);
    }
	///////
	// end php arrayaccess
	/////
	

		
	function writeBytes(/*ByteArray*/ $src, $offset, $length)
	{
		$this->ensureCapacity($this->m_index + $length);
		
		for ($i = 0; $i < $length; $i++) {
			$this->m_data[$this->m_index + $i] = $src->m_data[$offset + $i];
		}
		
		$this->m_index  += $length;
		if ($this->m_index > $this->m_length) {
			$this->m_length = $this->m_index;
		}
	}
	
	function writeUTF(/*iglang::String*/ $string)
	{
		$length = strlen($string);
		$this->ensureCapacity($this->m_index + $length + 3);
	
		$this->writeShort($length);
		for ($i = 0; $i < $length; $i++) {
			// ord doesn't seem to mangle UTF-8 strings?
			$this->m_data[$this->m_index + $i] = $string[$i];
		}
		
		$this->m_index  += $length;
		if ($this->m_index > $this->m_length) {
			$this->m_length = $this->m_index;
		}
	}
	
	
	function writeUTFBytes(/*iglang::String*/ $string)
	{
		$length = strlen($string);
		$this->ensureCapacity($this->m_index + $length);
		
		for ($i = 0; $i < $length; $i++) {
			$this->m_data[$this->m_index + $i] = $string[$i];
		}
		
		$this->m_index  += $length;
		if ($this->m_index > $this->m_length) {
			$this->m_length = $this->m_index;
		}
	}
	
	function writeByte($val)
	{
		$length = 1;
		$this->ensureCapacity($this->m_index + $length);
		
		$this->m_data[$this->m_index] = chr($val);
		
		$this->m_index  += $length;
		if ($this->m_index > $this->m_length) {
			$this->m_length = $this->m_index;
		}
	}

	function readByte()
	{
		$b = ord($this->m_data[$this->m_index]);
		if (0 != ($b & 0x80))        $b = $b - 0xff - 1;
		$this->m_index ++;
		return $b;
	}

	function readUnsignedByte()
	{
		$b = ord($this->m_data[$this->m_index]);
		$this->m_index ++;
		return $b & 0xFF;
	}

	///////////////////////////////
	// These functions don't directly access the byte stream

	function position__set($pos)
	{
		$this->m_index = $pos;
	}
	
	function position__get()
	{
		return $this->m_index;
	}
	
	function endian__set($endian)
	{
		$this->m_endian = $endian;
	}
	
	function length__set($length)
	{
		$this->ensureCapacity($length);
		$this->m_length = $length;
	}
	
	function length__get()
	{
		return $this->m_length;
	}

	function writeInt($val)
	{
		$this->ensureCapacity($this->m_index + 4);
		if ($this->m_endian)
		{
			// little endian
			$this->writeByte($val >> 0);
			$this->writeByte($val >> 8);
			$this->writeByte($val >> 16);
			$this->writeByte($val >> 24);
		
		}
		else
		{
			// big endian
			$this->writeByte($val >> 24);
			$this->writeByte($val >> 16);
			$this->writeByte($val >> 8);
			$this->writeByte($val >> 0);
		
		}
	}
	
	
	function writeUnsignedInt($val)
	{
		$this->ensureCapacity($this->m_index + 4);
		if ($this->m_endian)
		{
			// little endian
			$this->writeByte($val >> 0);
			$this->writeByte($val >> 8);
			$this->writeByte($val >> 16);
			$this->writeByte($val >> 24);
		
		}
		else
		{
			// big endian
			$this->writeByte($val >> 24);
			$this->writeByte($val >> 16);
			$this->writeByte($val >> 8);
			$this->writeByte($val >> 0);
		
		}
	}
	
	
	function writeShort($val)
	{
		$this->ensureCapacity($this->m_index + 2);
		if ($this->m_endian)
		{
			// little endian
			$this->writeByte($val >> 0);
			$this->writeByte($val >> 8);
		}
		else
		{
			// big endian
			$this->writeByte($val >> 8);
			$this->writeByte($val >> 0);
		}
	}
	
	function writeUnsignedShort($val)
	{
		$this->ensureCapacity($this->m_index + 2);
		if ($this->m_endian)
		{
			// little endian
			$this->writeByte($val >> 0);
			$this->writeByte($val >> 8);
		}
		else
		{
			// big endian
			$this->writeByte($val >> 8);
			$this->writeByte($val >> 0);
		}
	}	
			
	
	function readInt()
	{
		$v0 = $this->readUnsignedByte();
		$v1 = $this->readUnsignedByte();
		$v2 = $this->readUnsignedByte();
		$v3 = $this->readUnsignedByte();
		
		// this might not be correct???
		// little endian == 1
		$b = ($this->m_endian) ? 
				(($v3 << 24) | ($v2 << 16) | ($v1 << 8) | ($v0 << 0))  :
				(($v0 << 24) | ($v1 << 16) | ($v2 << 8) | ($v3 << 0));
	
		if (0 != ($b & 0x80000000))  $b = $b - 0xffffffff - 1;

		return $b;
	}

	function readUnsignedInt()
	{
		$v0 = $this->readUnsignedByte();
		$v1 = $this->readUnsignedByte();
		$v2 = $this->readUnsignedByte();
		$v3 = $this->readUnsignedByte();
		
		// little endian == 1
		return ($this->m_endian) ? 
				(($v3 << 24) | ($v2 << 16) | ($v1 << 8) | ($v0 << 0))  :
				(($v0 << 24) | ($v1 << 16) | ($v2 << 8) | ($v3 << 0));
	}



	function readShort()
	{
		$v2 = $this->readUnsignedByte();
		$v3 = $this->readUnsignedByte();
		$b = ($this->m_endian) ?  (($v3 << 8) | ($v2 << 0)) :  (($v2 << 8) | ($v3 << 0));
		if (0 != ($b & 0x8000))      $b = $b - 0xffff - 1;		
		return $b;
	}

	function readUnsignedShort()
	{
		$v2 = $this->readUnsignedByte();
		$v3 = $this->readUnsignedByte();
		$tmp = ($this->m_endian) ?  (($v3 << 8) | ($v2 << 0)) :  (($v2 << 8) | ($v3 << 0));
		return $tmp;
	}
	

	function readFloat()
	{
		// see http://php.net/manual/en/language.types.float.php
		$v = $this->readInt();
		
		// TODO verify that this is actually correct
		// convert 32 bit integer to a php float
		$x = ($v & ((1 << 23) - 1)) + (1 << 23) * ($v >> 31 | 1);
  		$exp = ($v >> 23 & 0xFF) - 127;
    	return $x * pow(2, $exp - 23);
	}
	
	function readUTF()
	{
		$character_count = $this->readUnsignedShort();
		return $this->readUTFBytes($character_count);
	}
	
	function readUTFBytes($character_count)
	{
		// set up a statically sized array to fill in
		//$string = array_fill(0, $character_count, 0);
		$string = str_repeat(' ', $character_count);
		
		// note the INTERNAL format for our php scripts is UTF-8 thus we don't need to encode
		// or decode values
		for ($i = 0; $i < $character_count; $i++) 
		{
			// chr converts an int to a string
			// HACK (need to fully support UTF8 strings)
			//$string[$i] = chr($this->readUnsignedByte());	// had "chr("
			$string[$i] = chr($this->readUnsignedByte());	// had "chr("
		}
		
		//return pack("c*", $string);
		//return implode($string);
		
		return $string;
	}
}



