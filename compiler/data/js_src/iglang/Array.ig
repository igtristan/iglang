package iglang;

import iglang.util.*;

/**
 * This is implemented semi-natively.  Do not add any fields to this or Object.
 */

public class Array<T1> extends Object implements ArrayIndexable
{

	/**
	 * Create a new Array with a specified length
	 */

	public function constructor(num_elements : int) {
		throw new Error("Unimplemented");
	}
	
	/**
	 * Get the length of the array
	 */
	 
	public final function get length(): int {		
		throw new Error("Unimplemented");
	}
	
	
	public override function toString(): String 
	{
		var first : bool = true;
		var sb : StringBuilder = new StringBuilder();
		sb.appendChar('[');
		
		for (var i : int in 0 => this.length)
		{
			if (!first) {
				sb.appendChar(',');
			}
			first = false;
			
			sb.appendString(String(this[i]));
		}
	
		sb.appendChar(']');
		return sb.toString();
	}
	
	/**
	 * Attempts to resize the underlying array.
	 * It may or may not return the same array handle, any 
	 * unoccupied slots will be filled with 0, null, etc
	 */
	
	public final function resize(new_size : int) : Array<T1>
	{
		return clone(new_size);
	}
	
	/**
	 * Creates a copy of the array.
	 * Any unoccupied slots will be filled with 0, null, etc
	 */
	
	public final function clone(new_size : int) : Array<T1>
	{
		var tmp : Array<T1> = new Array<T1>(new_size);
		tmp.copy(this);
		return tmp;
	}
	
	public final function copy(src : Array<T1>) : void
	{
		var src_start : int = 0;
		var len : int = -1;
		var dst_start : int = 0;
	
		if (len == -1) {
			len = this.length;
			if (src.length < this.length) {
				len = src.length;
			}
		}
		for (var i : int in 0 => len) {
			this[i + dst_start] = src[i + src_start];
		}
	}
	
	
	/**
	 * This can only really be overridden by the VM for int and string types
	 * perhaps we should just be overriding the String classes method?
	 */
	
	public final function contentsEqual(other  : Array<T1>) : bool
	{
		if (other.length != this.length) {
			return false;
		}
		
		for (var i : int = this.length - 1; i >= 0; i--) 
		{
			if (this[i] != other[i]) {
				return false;
			}
		}
		
		return true;
	}
	
	
	
	/**
	 * Fills the array with a value.
	 *
	 */
	
	public final function fill(value : T1) : void
	{
		for (var i : int in 0 => this.length) {
			this[i] = value;
		}
	}
	
	public function iterator() : ArrayIterator<T1>
	{
		return new ArrayIterator<T1>(this);
	}
	
	public function get [](key : int) : T1
	{
		throw new Error("Unimplemented");
	}
	
	public function set [](key : int, value : T1) : void
	{
		throw new Error("Unimplemented");
	}
	
	
	/**
	 * Searches for an item in the Vector and returns the index position of 
	 * the item. The item is compared to the Vector elements using strict 
	 * equality (===). 
	 *
	 * @param searchElement The item to find in the Vector.
	 *
	 * @param fromIndex     The location in the Vector from which to start 
	 *                      searching for the item. If this parameter is 
	 *                      negative, it is treated as length + fromIndex, 
	 *                      meaning the search starts -fromIndex items from 
	 *                      the end and searches from that position forward 
	 *                      to the end of the Vector.
	 *
	 * @return              A zero-based index position of the item in the 
	 *                      Vector. If the searchElement argument is not 
	 *                      found, the return value is -1. 	 
	 **/
	public final function indexOf(searchElement:T1, fromIndex:int=0) : int { 
	
		// TODO probably add in the concept of an Object.isNull?
	
		var len : int = this.length;
		for (var i : int = fromIndex; i < len; i++) {
			if (searchElement.equals(this[i])) {
				return i;
			}
		}
		return -1;
	}
	
	/**
	 * Maps to "vector.IndexOf(obj) != -1".
	 * It's just cleaner code with this explicit function...
	 **/
	public final function contains(obj:T1) : bool { 
	
		return indexOf(obj,0) != -1;
	}
	
	
		 
	public final function join(sep : String) : String { 
		if (this.length == 0) {
			return "";
		}
	
		var sb : StringBuilder = new StringBuilder();
		sb.appendString(this[0].toString());
		for (var i : int = 1; i < this.length; i++) {
			sb.appendString(sep);
			sb.appendString(this[i].toString());
		}
		
		return sb.toString();
	
	}
	
	
}