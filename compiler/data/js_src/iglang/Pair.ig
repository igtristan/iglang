package iglang;

public class Pair<T1, T2>
{
	public var left : T1;
	public var right :T2;
}