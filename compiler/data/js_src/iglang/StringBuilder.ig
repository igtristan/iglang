package iglang;

public class StringBuilder
{
	static private var s_native_cast_buffer : Array<byte> = new Array<byte>(64);
	private var m_data : Array<short> ;
	private var m_length : int;
	
	
	public function constructor(cap : int = 0) {
		if (cap > 0) {
			m_data = new Array<short>(cap);
		}	
	}
	
	/**
	 * Return the number of characters in the StringBuilder
	 */
	 
	public function get length() : int {
		return m_length;
	}
	
	/**
	 * Set the length of the StringBuilder
	 */
	
	public function set length(v : int) : void {
		m_length = v;
	}
	
	
	private function ensureCapacity(sz : int) : void
	{
		sz += m_length;
	
		if (m_data == null) {
			m_data = new Array<short>(sz * 2);
		}
		else if (sz > m_data.length)
		{
			var new_cap : int = (m_data.length * 4) / 3;
			if (new_cap < sz + 8) {
				new_cap = sz + 8;
			}

			m_data = m_data.resize(new_cap);
		}
	}
	
	/**
	 * Appends the string "null" if the pointer is null, and String(obj) otherwise
	 */
	
	public function appendObject(obj : Object) : StringBuilder
	{
		if (obj == null) {
			return appendString("null");
		}
		
		return appendString(obj.toString());
	}
	
	public function appendString(other : String) : StringBuilder
	{
		var other_length : int = other.length;
		ensureCapacity(other_length);
		
		for (var i : int = 0; i < other_length; i++) {
			var char_value : int = other.charCodeAt(i);
			m_data[m_length + i] = char_value;
		}
		m_length += other_length;
		return this;
	}
	
	public function appendChar(other : int) : StringBuilder
	{
		ensureCapacity(1);
		m_data[m_length] = other;
		m_length ++;
		return this;
	}
	
	public function appendBool(other : bool) : StringBuilder
	{
		if (other) {
			appendString("true");
		}
		else {
			appendString("false");
		}
		return this;
	}
	
	public function appendDouble(value : double) : StringBuilder
	{
		var cnt : int =  doubleToByteArray(value, s_native_cast_buffer, 64);
		ensureCapacity(cnt + 1);
		for (var i : int = 0; i < cnt; i++) {
			m_data[m_length + i] = int(s_native_cast_buffer[i]);
		}
		m_length += cnt;
		return this;
	}
	
	public function appendInt(value : int) : StringBuilder
	{
		var cnt : int =  intToByteArray(value, s_native_cast_buffer, 64);
		ensureCapacity(cnt + 1);
		for (var i : int = 0; i < cnt; i++) {
			m_data[m_length + i] = int(s_native_cast_buffer[i]);
		}
		m_length += cnt;
		return this;
	}
	
	public override function toString() : String {
		return String.__ig_shortArrayRangeToString(m_data, 0, m_length);
	}
	
	
	////////////
	// Native methods that require implementation
	///////////
	
	private static function doubleToByteArray(value : double, buffer : Array<byte>, len : int) : int
	{
		throw new Error("MissingNative");
	}
	
	private static function  intToByteArray(value : int, buffer : Array<byte>, len : int) : int
	{
		throw new Error("MissingNative");
	}
}