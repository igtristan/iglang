package iglang;

public class Integer
{
	public static const MAX_VALUE : int =  2147483647;
	public static const MIN_VALUE : int = -2147483648;		// TODO verify that this value gets set correctly
	public var value : int;
	
	public function constructor(v : int) {
		value = v;
	}
}