package iglang;

public class Math
{	
	public static const PI         :double = 
		3.14159265358979323846;
	public static const DEG_TO_RAD :double = 			3.14159265358979323846/ 180.0;
	public static const RAD_TO_DEG :double = 180.0 / 3.14159265358979323846;
	
	public static function sign (v :double) :double {
		return (v < 0) ? 
			-1 : 
			((v > 0) ? 1 : 0);
	}

	public static function sin   (v   :double            ) : double { throw new Error("Native Missing"); } // native
	public static function cos   (v   :double            ) : double { throw new Error("Native Missing"); } // native
	public static function tan   (v   :double            ) : double { throw new Error("Native Missing"); } // native
	public static function asin  (v   :double            ) : double { throw new Error("Native Missing"); } // native
	public static function acos  (v   :double            ) : double { throw new Error("Native Missing"); } // native
	public static function atan2 (y   :double, x : double) : double { throw new Error("Native Missing"); } // native
	public static function atan  (v   :double            ) : double { throw new Error("Native Missing"); } // native
	public static function pow   (base:double, pow:double) : double { throw new Error("Native Missing"); } // native
	public static function sqrt  (v   :double            ) : double { throw new Error("Native Missing"); } // native
	public static function random()                        : double { throw new Error("Native Missing"); } // native
	public static function abs   (v   :double)             : double { throw new Error("Native Missing"); } // native
	public static function floor (v   :double)             : double { throw new Error("Native Missing"); } // native
	public static function ceil  (v   :double)             : double { throw new Error("Native Missing"); } // native
	public static function round (v   :double)             : double { throw new Error("Native Missing"); } // native
	public static function min   (a   :double, b:double)   : double { throw new Error("Native Missing"); } // native
	public static function max   (a   :double, b:double)   : double { throw new Error("Native Missing"); } // native





	//////////////////////////////////////////////////////////////////
	// Experimental section
	//////////////////////////////////////////////////////////////////

	public static function square(a :double): double {
		return a*a;
	}

	public static function length(a :double, b :double, c:double) : double {
		return Math.sqrt(a*a+b*b+c*c);
	}

	public static function length3(a :double, b :double, c:double) : double {
		return Math.sqrt(a*a+b*b+c*c);
	}

	public static function clamp(value :double, min_value :double, max_value :double): double {
		if (value < min_value) { return min_value; }
		if (value > max_value) { return max_value; }
		return value;
	}
}
