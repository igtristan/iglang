package iglang;

import iglang.util.*;

public class ByteArray extends Object
{
	// TODO make this private again
	public  var m_data : Array<byte> = new Array<byte>(0);
	private var m_position     : int;
	private var m_capacity     : int;	// the capacity of the byte buffer
	private var m_length       : int;
	private var m_big_endian   : bool;			// IN AS3 THIS IS THE COMPLETE OPPOSITE
	

	public function get backing_array() :Array<byte> {
		return m_data;
	}


	public static final function __ig_createWithData(d :Array<byte>) :ByteArray
	{
		var b : ByteArray = new ByteArray();
		b.m_data = d;
		b.m_position = 0;
		b.m_length = d.length;
		b.m_capacity = d.length;
		b.m_big_endian = false;
		return b;
	}

	public static final function __ig_createWithCapacity(cap : int) : ByteArray
	{
		var d : Array<byte> = new Array<byte>(cap);
		var b : ByteArray = new ByteArray();
		b.m_data = d;
		b.m_position = 0;
		b.m_length = d.length;
		b.m_capacity = d.length;
		b.m_big_endian = false;
		return b;
	}
	
	
	private final function ensureCapacity(cap : int): void
	{
		if (m_capacity >= cap) { return; }
		
		var old_data : Array<byte> = m_data;
		
		var old_cap : int = m_capacity;
		var increase : int = old_cap / 2;
		if (old_cap + increase < cap + 8) {
			m_capacity = cap + 8;
		}
		else
		{
			m_capacity += increase;
		}
		
		m_data = old_data.resize(m_capacity);
	}
	
	
	
	
	/**
	 * Get/Set the position of the cursor within the ByteArray
	 */

	public final function get position(): int {return m_position;}
	public final function set position(val:int): void { m_position = val;}
	
	/**
	 * Get/Set the number of bytes of data in the byte array.
	 */
	
	public final function get length():int {return m_length;}
	public final function set length(val :int): void {
		ensureCapacity(val);
		m_length = val;
	}
	
	/**
	 * set which endian mode the byte array operates in
	 */
	
	public final function set endian(val : Endian) : void {
		m_big_endian = val == Endian.BIG_ENDIAN;
	}
	
	public final function get endian() : Endian {
		if (m_big_endian) {
			return Endian.BIG_ENDIAN;
		}	
		else {
			return Endian.LITTLE_ENDIAN;
		}
	}
	
	/**
	 * Implementation of array type accessors
	 */
	
	public final function get [](key : int) : int
	{
		if (key >= m_length) {
			throw new Error("Exceeded limit of ByteArray");
		}
		
		return m_data[key];
	}
	
	public final function set [](idx : int, value : int) : void
	{
		ensureCapacity(idx + 1);
		m_data[idx] = value;
		if (idx >= length) {
			length = idx + 1;
		}
	}

	private final function throwExceededBounds(): void {
		throw new Error("Exceeded bounds: " + m_position + " of " + m_length);
	}

	/**
	 * Reads a signed byte from the byte stream.
	 **/
	public final function readByte() : int {
		if (m_position >= m_length) {
			throwExceededBounds();	
		}
		
		var tmp : int = m_data[m_position];
		m_position++;
		return tmp;
	}

	/**
	 * Peek the next byte in the stream if no byte is found
	   return -1 to signify EOF;
	 */

	public final function peek(offset :int = 0) :int {
		if ((m_position + offset) >= m_length) {
			return -1;
		}
		
		return m_data[m_position + offset];	
	}


	/**
	 * Reads a signed byte from the byte stream.
	 **/
	public final function readUnsignedByte() : int {
		if (m_position + 1 > m_length) {
			throwExceededBounds();
		}
		
		var tmp : int = m_data[m_position];
		m_position++;
		return tmp & 0xff;
	}

	/**
	 * Reads a signed 16-bit integer from the byte stream.
	 **/
	public final function readShort() : int {
		if (m_position + 2 > m_length) {
			throwExceededBounds();
		}
		
		var t0 : int = m_data[m_position+0];
		var t1 : int = m_data[m_position+1];
		m_position += 2;
		
		if (m_big_endian) {
			return (t0 #<< 8) | (t1 & 0x00ff);
		}
		else
		{
			return (t1 #<< 8) | (t0 & 0x00ff);
		}
	}

	public final function readUnsignedShort() : int {
		if (m_position + 2 > m_length) {
			throwExceededBounds();
		}
		
		var t0 : int = m_data[m_position+0]  & 0x00ff;
		var t1 : int = m_data[m_position+1]  & 0x00ff;
		m_position += 2;
		
		if (m_big_endian) {
			return (t0 #<< 8) | t1 ;
		}
		else
		{
			return (t1 #<< 8) | t0;
		}
	}


	/**
	 * Reads a signed 32-bit integer from the byte stream.
	 **/
	public final function readInt() : int {
		if (m_position + 4 > m_length) {
			throwExceededBounds();
		}
		
		var t0 : int = m_data[m_position+0] & 0xff;
		var t1 : int = m_data[m_position+1] & 0xff;
		var t2 : int = m_data[m_position+2] & 0xff;
		var t3 : int = m_data[m_position+3] & 0xff;
		m_position += 4;
		
		if (m_big_endian) {
			return (t0 #<< 24) | (t1 #<< 16) | (t2 #<< 8) | t3;
		}
		else
		{
			return (t3 #<< 24) | (t2 #<< 16) | (t1 #<< 8) | t0;
		}
	}
	
	public final function readUnsignedInt() : int {
		if (m_position + 4 > m_length) {
			throwExceededBounds();
		}
		
		var t0 : int = m_data[m_position+0] & 0xff;
		var t1 : int = m_data[m_position+1] & 0xff;
		var t2 : int = m_data[m_position+2] & 0xff;
		var t3 : int = m_data[m_position+3] & 0xff;
		m_position += 4;
		
		if (m_big_endian) {
			return (t0 #<< 24) | (t1 #<< 16) | (t2 #<< 8) | t3;
		}
		else
		{
			return (t3 #<< 24) | (t2 #<< 16) | (t1 #<< 8) | t0;
		}
	}
	
	/**
	 * Reads an IEEE 754 single-precision (32-bit) floating-point number from the byte stream.
	 **/
	public final function readFloat() : double {
		var v : int = readInt();
		return Double.intBitsToDouble(v);
	}
	
	/**
	 * Reads an IEEE 754 single-precision (64-bit) floating-point number from the byte stream.
	 **/	
	public final function readDouble() : double {
		var v0 : int = readInt();
		var v1 : int = readInt();
		
		if (m_big_endian) {
			return Double.intPairToDouble(v1, v0);
		}
		else
		{
			return Double.intPairToDouble(v0, v1);
		}
	}
	
	/**
	 * Reads a UTF-8 string from the byte stream.
	 **/
	public final function readUTF() : String {
		var byte_length : int = readShort();
		return readUTFBytes(byte_length);
	}
	
	
	/**
	 * This does not exist in AS3
	 **/
	 
	 /*
	public final function readUTFCharacter() : int {
		var b0 : int = readUnsignedByte(); i++;
		//trace("read: " + b0);
		
		// this is a non ASCII sequence
		if ((b0 & 0x80) != 0) 
		{
			//trace("non ASCII");
			// 2 bytes =   110xxxxx  10xxxxxx
			// 3 bytes =   1110xxxx  10xxxxxx  10xxxxxx
		
			var n : int = (~b0) & 0xff;		// eg.     001XXXXX  (nlz tells us the number of 0's, negating gives us the # of 1s)
			//trace("post invert: " + n + " " + (~b0) + " " + b0);
			
			var nlz : int = 0;
			
			// this'll have at least 2 leading zeroes
			if ((n & 0xF0) == 0) { nlz += 4; n = n #<< 4; }
			if ((n & 0xC0) == 0) { nlz += 2; n = n #<< 2; }
			if ((n & 0x80) == 0) { nlz += 1;             }			
			
			//trace("NLZ found: " + nlz + " ########### ");	
			
			// mask out number of leading 1's + 1
			// +1 isn't necessary because the following binary digit is always a 0
			var b0_mask : int = 0x00ff #>> nlz;   
			
			b0                  = b0 & b0_mask;
			var following : int = nlz - 1;					// number of bytes following is leading 1's - 1
		
			while (following > 0) {
				var bN : int = readUnsignedByte();  i++;
				//trace("read: " + bN);
				b0 = (b0 #<< 6) | (bN & 0x3f);
				following --;
			}
		}
		return b0;
	}
	*/
	
	/**
	 * Reads a sequence of UTF-8 bytes specified by the length parameter from the byte stream and returns a string.
	 **/
	public final function readUTFBytes(length: int):String {
		
		if (m_position + length > m_length) {
			throwExceededBounds();
		}
		
		var character_count : int = 0;
		for (var i : int = 0; i < length; i++) {
			// missing the inner brackets will not cause compilation to fail
			// character_count += int(m_data[m_position + i] & 0xC0 != 0xC0);
			character_count += int((m_data[m_position + i] & 0xC0) != 0x80);
		}
		
		var start_posn : int = m_position;
		
		var string : Array<short> = new Array<short>(character_count);
		
		var string_idx : int = 0;
		var i : int = 0;
		while (i < length) 
		{
			var b0 : int = readUnsignedByte(); i++;
			//trace("read: " + b0);
			
			// this is a non ASCII sequence
			if ((b0 & 0x80) != 0) 
			{
				//trace("non ASCII");
				// 2 bytes =   110xxxxx  10xxxxxx
				// 3 bytes =   1110xxxx  10xxxxxx  10xxxxxx
			
				var n : int = (~b0) & 0xff;		// eg.     001XXXXX  (nlz tells us the number of 0's, negating gives us the # of 1s)
				//trace("post invert: " + n + " " + (~b0) + " " + b0);
				
				var nlz : int = 0;
				
				// this'll have at least 2 leading zeroes
				if ((n & 0xF0) == 0) { nlz += 4; n = n #<< 4; }
				if ((n & 0xC0) == 0) { nlz += 2; n = n #<< 2; }
				if ((n & 0x80) == 0) { nlz += 1;             }			
				
				//trace("NLZ found: " + nlz + " ########### ");	
				
				// mask out number of leading 1's + 1
				// +1 isn't necessary because the following binary digit is always a 0
				var b0_mask : int = 0x00ff #>> nlz;   
				
				b0                  = b0 & b0_mask;
				var following : int = nlz - 1;					// number of bytes following is leading 1's - 1
			
				while (following > 0) {
					var bN : int = readUnsignedByte();  i++;
					//trace("read: " + bN);
					b0 = (b0 #<< 6) | (bN & 0x3f);
					following --;
				}
			}
			
			/*
			if (string_idx == character_count) {
				trace("bad news idx: " + string_idx + " " + b0 + " '" + String.fromCharCode(b0) + "' len: " + length);
				
				m_position = start_posn;
				for (var k : int = 0; k < character_count; k++) {
					trace ("\t" + k + ") char: " + readUnsignedByte());
				}
			}
			*/
			string[string_idx] = b0;
			string_idx ++;
		}
				
		return String.__ig_shortArrayToStringNoCopy(string);
	}



	/**
	 * Writes bytes from another byte array
	 **/
	 
	 public final function writeBytes(bytes : ByteArray, offset : int, length : int) : void 
	 {
	 	ensureCapacity(m_position + length);
	 	
	 	for (var i : int = length - 1; i >= 0; i--) {
	 		m_data[m_position + i] = bytes[i + offset];
	 	}
	 	
	 	m_position += length;
	 	if (m_position > m_length) {
	 		m_length = m_position;
	 	}
	 }



	 public final function writeUTFCharacter(c : int) : void
	 {
	 	var dst_index : int = m_position;
	 	
		if      (c < 0x0000080) // 0 => 0x7f,  25 leading zeros
		{
			ensureCapacity(dst_index + 1);
			m_data[dst_index] = c;   dst_index ++;
		}
		else if (c < 0x0000800)
		{
			//dst_index += 2;
	
			//var nlz : int = #0  12345
	
			// 110xxxxx 	10xxxxxx
			ensureCapacity(dst_index + 2);				
			m_data[dst_index] = 0xC0 | c.extract(6, 0x1f);	dst_index ++;  // ((c #>> 6) & 0x1f);  
			m_data[dst_index] = 0x80 | c.extract(0, 0x3f);	dst_index ++;   //((c #>> 0) & 0x3f);  
		}
		else if (c < 0x0010000)
		{
			// 1110xxxx 	10xxxxxx 	10xxxxxx
			ensureCapacity(dst_index + 3);
			m_data[dst_index] = 0xE0 | ((c #>> 12) & 0x0f);  dst_index ++;
			m_data[dst_index] = 0x80 | ((c #>>  6) & 0x3f);  dst_index ++;
			m_data[dst_index] = 0x80 | ((c #>>  0) & 0x3f);  dst_index ++;
		}
		else if (c < 0x0200000)
		{
			// 11110xxx 	10xxxxxx 	10xxxxxx 	10xxxxxx
			ensureCapacity(dst_index + 4);
			m_data[dst_index] = 0xF0 | ((c #>> 18) & 0x07);  dst_index ++;
			m_data[dst_index] = 0x80 | ((c #>> 12) & 0x3f);  dst_index ++;
			m_data[dst_index] = 0x80 | ((c #>>  6) & 0x3f);  dst_index ++;
			m_data[dst_index] = 0x80 | ((c #>>  0) & 0x3f);  dst_index ++;
		}
		else if (c < 0x4000000)
		{
			// 111110xx 	10xxxxxx 	10xxxxxx 	10xxxxxx 	10xxxxxx
			ensureCapacity(dst_index + 5);
			m_data[dst_index] = 0xF8 | ((c #>>  24) & 0x03);  dst_index ++;
			m_data[dst_index] = 0x80 | ((c #>>  18) & 0x3f);  dst_index ++;
			m_data[dst_index] = 0x80 | ((c #>>  12) & 0x3f);  dst_index ++;
			m_data[dst_index] = 0x80 | ((c #>>   6) & 0x3f);  dst_index ++;
			m_data[dst_index] = 0x80 | ((c #>>   0) & 0x3f);  dst_index ++;
		}
		else
		{
			// 1111110x 	10xxxxxx 	10xxxxxx 	10xxxxxx 	10xxxxxx 	10xxxxxx
			ensureCapacity(dst_index + 6);
			m_data[dst_index] = 0xFC | ((c #>>  30) & 0x01);  dst_index ++;
			m_data[dst_index] = 0x80 | ((c #>>  24) & 0x3f);  dst_index ++;
			m_data[dst_index] = 0x80 | ((c #>>  18) & 0x3f);  dst_index ++;
			m_data[dst_index] = 0x80 | ((c #>>  12) & 0x3f);  dst_index ++;
			m_data[dst_index] = 0x80 | ((c #>>   6) & 0x3f);  dst_index ++;
			m_data[dst_index] = 0x80 | ((c #>>   0) & 0x3f);  dst_index ++;
		}
			
			
		m_position = dst_index;
		if (m_position > m_length) {
			m_length = m_position;
		}
	 }
	
	 /**
	 * Writes the string as UTF-8 without a length header
	 **/
	 
	 public final function writeUTFBytes(value : String): void {
	 	var len2 : int = value.__ig_strlenUTF8();
	 	ensureCapacity(m_position + len2);
	 	
	 	//throw new Error("Unimplemented");
	 	
	 	var dst_index : int = m_position;
		
		var len : int = value.length;
		for (var i : int = 0; i < len; i++)
		{
			var c : int = value.charCodeAt(i);	//m_data[i];

			//if (!(c < 0x0000080) || (c < 0)) {
			//	trace ("WRITING CC: " + c);
			//}
			if      (c < 0x0000080) // 0 => 0x7f,  25 leading zeros
			{
				m_data[dst_index] = c;   dst_index ++;
			}
			else if (c < 0x0000800)
			{
				//dst_index += 2;
				
				//var nlz : int = #0  12345
				
				// 11 bits to encode
				// 110xxxxx 	10xxxxxx
				m_data[dst_index] = 0xC0 | ((c #>> 6) & 0x1f);  dst_index ++;	// 5 bits
				m_data[dst_index] = 0x80 | ((c #>> 0) & 0x3f);  dst_index ++;	// 6 bits
			}
			else if (c < 0x0010000)
			{
				// 16 bits to encode 
				// 1110xxxx 	10xxxxxx 	10xxxxxx
				m_data[dst_index] = 0xE0 | ((c #>> 12) & 0x0f);  dst_index ++;
				m_data[dst_index] = 0x80 | ((c #>>  6) & 0x3f);  dst_index ++;
				m_data[dst_index] = 0x80 | ((c #>>  0) & 0x3f);  dst_index ++;
			}
			else if (c < 0x0200000)
			{
				// 11110xxx 	10xxxxxx 	10xxxxxx 	10xxxxxx
				m_data[dst_index] = 0xF0 | ((c #>> 18) & 0x07);  dst_index ++;
				m_data[dst_index] = 0x80 | ((c #>> 12) & 0x3f);  dst_index ++;
				m_data[dst_index] = 0x80 | ((c #>>  6) & 0x3f);  dst_index ++;
				m_data[dst_index] = 0x80 | ((c #>>  0) & 0x3f);  dst_index ++;
			}
			else if (c < 0x4000000)
			{
				// 111110xx 	10xxxxxx 	10xxxxxx 	10xxxxxx 	10xxxxxx
				m_data[dst_index] = 0xF8 | ((c #>>  24) & 0x03);  dst_index ++;
				m_data[dst_index] = 0x80 | ((c #>>  18) & 0x3f);  dst_index ++;
				m_data[dst_index] = 0x80 | ((c #>>  12) & 0x3f);  dst_index ++;
				m_data[dst_index] = 0x80 | ((c #>>   6) & 0x3f);  dst_index ++;
				m_data[dst_index] = 0x80 | ((c #>>   0) & 0x3f);  dst_index ++;
			}
			else
			{
				// 1111110x 	10xxxxxx 	10xxxxxx 	10xxxxxx 	10xxxxxx 	10xxxxxx
				m_data[dst_index] = 0xFC | ((c #>>  30) & 0x01);  dst_index ++;
				m_data[dst_index] = 0x80 | ((c #>>  24) & 0x3f);  dst_index ++;
				m_data[dst_index] = 0x80 | ((c #>>  18) & 0x3f);  dst_index ++;
				m_data[dst_index] = 0x80 | ((c #>>  12) & 0x3f);  dst_index ++;
				m_data[dst_index] = 0x80 | ((c #>>   6) & 0x3f);  dst_index ++;
				m_data[dst_index] = 0x80 | ((c #>>   0) & 0x3f);  dst_index ++;
			}
		}
		
		m_position = dst_index;
		if (m_position > m_length) {
			m_length = m_position;
		}
	 }
	 
	 /**
	 * Writes the string as UTF-8 prepended with a short specifying the length
	 **/
	 
	 public final function writeUTF(value : String): void {
	 	var len : int = value.__ig_strlenUTF8();
	 	writeShort(len);
	 	
	 	writeUTFBytes(value);
	 }
	 
	/**
	 * Writes a single byte
	 **/
	 
	 public final function writeByte(value : int) : void {
	 	ensureCapacity(m_position + 1);
	 	
	 	m_data[m_position] = value;
	 	m_position++;
	 	if (m_position > m_length) {
	 		m_length = m_position;
	 	}
	 }
	 
	 /**
	  * Writes a 2 byte short
	  */
	 
	 public final function writeShort(value : int) : void {

	 	var pos : int = m_position;
	 	ensureCapacity(pos + 2);
	 	
	 	if (m_big_endian) {
		 	m_data[pos+0] = value #>> 8;
	 		m_data[pos+1] = value & 0xff;
	 	}
	 	else
	 	{
	 		m_data[pos+1] = value #>> 8;
	 		m_data[pos+0] = value & 0xff;
	 	}
	 	
	 	pos += 2;
	 	m_position = pos;
	 	if (pos > m_length) {
	 		m_length = m_position;
	 	}
	 }	 
	 

	 
	 /**
	  * Writes a 4 byte, 32 bit integer
	  */
	 
	  public final function writeInt(value : int) : void {
	 	var pos : int = m_position;
	 	ensureCapacity(pos + 4);
	 	
	 	if (m_big_endian) {
		 	m_data[pos+0] = value #>> 24;
	 		m_data[pos+1] = value #>> 16;
	 		m_data[pos+2] = value #>> 8;
	 		m_data[pos+3] = value & 0xff;
	 	}
	 	else
	 	{
			m_data[pos+3] = value #>> 24;
	 		m_data[pos+2] = value #>> 16;
	 		m_data[pos+1] = value #>> 8;
	 		m_data[pos+0] = value & 0xff;
	 	}
	 	
	 	pos += 4;
	 	m_position = pos;
	 	if (pos > m_length) {
	 		m_length = m_position;
	 	}
	 }
	 
	 public final function writeUnsignedInt(value : int) : void
	 {
	 	writeInt(value);
	 }
	 
	 /**
	  * Writes an IEE 32bit floating point number
	  */
	 
	 public final function writeFloat(value : double): void
	 {
	 	writeInt(Double.doubleToIntBits(value));
	 }	 
	 
	 public final function writeDouble(value : double) : void
	 {
	 	if (m_big_endian) {
	 		writeInt(Double.doubleToIntBitsHi(value));
	 		writeInt(Double.doubleToIntBitsLo(value));
	 	}
	 	else {
	 		writeInt(Double.doubleToIntBitsLo(value));
	 		writeInt(Double.doubleToIntBitsHi(value));
	 	}
	 }

	 public final function toHexString(upper_case :bool = false) :String {
	 	var sb :StringBuilder = new StringBuilder(m_length * 2);
	 	

	 	if (upper_case) {
		 	for (var i :int in 0 => m_length) {
		 		var v0 :int  = int(m_data[i]).extract(4, 0xf);
		 		var v1 :int  = int(m_data[i]).extract(0, 0xf);
		 		sb.appendChar((v0 < 10) ? ('0' + v0) : (v0 - 10 + 'A'));
		 		sb.appendChar((v1 < 10) ? ('0' + v1) : (v1 - 10 + 'A'));
		 	}
	 	}
	 	else 
	 	{
		 	for (var i :int in 0 => m_length) {
		 		var v0 :int  = int(m_data[i]).extract(4, 0xf);
		 		var v1 :int  = int(m_data[i]).extract(0, 0xf);
		 		sb.appendChar((v0 < 10) ? ('0' + v0) : (v0 - 10 + 'a'));
		 		sb.appendChar((v1 < 10) ? ('0' + v1) : (v1 - 10 + 'a'));
		 	}
	 	}
	 	return sb.toString();
	 }

}
