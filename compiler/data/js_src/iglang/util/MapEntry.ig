package iglang.util;

public class MapEntry<T1,T2>
{
	public var m_next : MapEntry<T1,T2>;
	public var m_key : T1;
	public var m_value : T2;
	
	public function constructor(next : MapEntry<T1,T2>, key : T1)
	{
		m_next = next;
		m_key = key;
	}
}