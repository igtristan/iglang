package iglang.util;

enum igDouble extends double
{		
	;
	
	public function toString() : String {
		return String(double(this));
	}
	
	public function hashCode() : int {
		var d : double = this;
		return int(d);
	}
	
	public function equals(other : igDouble): bool
	{
		return this == other;
	}

	
	public function clamp(min_value :double, max_value :double) : int {
		if (double(this) < min_value) {
			return min_value;
		}
		if (double(this) > max_value) {
			return max_value;
		}
		return double(this);
	}

	public function min(other :double) :double {
		return (double(this) < other) ? double(this) : other;
	}


	public function max(other :double) :double {
		return (double(this) > other) ? double(this) : other;
	}
}