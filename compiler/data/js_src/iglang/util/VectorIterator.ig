package iglang.util;

public class VectorIterator<T1>
{
	private var m_index : int = 0;
	private var m_array : Array<T1>;
	private var m_length : int = 0;
	
	public function constructor(vector : Vector<T1>, data : Array<T1>) {
	
		m_length = vector.length;
		has_next = m_length > 0;
		m_index = 0;
		m_array = data;
	}
	
	public var has_next : bool = false;
	
	public final function next(): T1
	{
		m_index ++;
		has_next = (m_index < m_length);
		return m_array[m_index - 1];
	}

}