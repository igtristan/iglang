package iglang.util;

enum igFloat extends double
{		
	;
	
	public function toString() : String {
		return String(double(this));
	}
	
	public function hashCode() : int {
		var d : double = this;
		return int(d);
	}
	
	public function equals(other : igFloat): bool
	{
		return this == other;
	}

}