package iglang;

public class Double
{
	public var value : double;
	
	public function constructor(v : double) {
		value = v;
	}
	
	internal static function doubleToIntBits(value : double) : int {
		throw new Error("Unimplemented");
	}
	
	internal static function doubleToIntBitsLo(value : double) : int {
		throw new Error("Unimplemented");
	}

	internal static function doubleToIntBitsHi(value : double) : int {
		throw new Error("Unimplemented");
	}
	
	internal static function intBitsToDouble(value : int) : double {
		throw new Error("Unimplemented");
	}
	
	internal static function intPairToDouble(value0 : int, value1 : int) : double {
		throw new Error("intPairToDouble Missing Native");
	}
}