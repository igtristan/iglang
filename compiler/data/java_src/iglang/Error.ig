/*NO-OUTPUT*/
package iglang;

public class Error extends Object
{
	/**
	 * Creates a new Error object. If message is specified, its value is 
	 * assigned to the object's Error.message property.
	 **/
	public function Error(message:String)
	{
	}

	/**
	 * Contains the message associated with the Error object. By default, 
	 * the value of this property is "Error". You can specify a message 
	 * property when you create an Error object by passing the error string 
	 * to the Error constructor function.
	 **/
	public function getMessage() : String
	{
		return null;
	}
	
	/**
	 * Returns the call stack for an error at the time of the error's 
	 * construction as a string. As shown in the following example, the first
	 * line of the return value is the string representation of the exception 
	 * object, followed by the stack trace elements.
	 **/
	public function getStackTrace() : String
	{
		return null;
	}
}
