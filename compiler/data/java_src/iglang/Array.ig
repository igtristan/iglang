/*NO-OUTPUT*/
package iglang;

public class Array<T1> extends Object
{
	public function iterator(): Object { throw new Error("Incomplete"); }
	
	public function constructor(num_elements : int) {}
	
	// make it so you can't resize an array
	public function get length(): int {return 0;}
	
	public function copy(new_size : int) : Array<T1>
	{
		//trace("Array.copy");
		//trace("Array.copy " + new_size);
	
		var tmp : Array<T1> = new Array<T1>(new_size);
		for (var i : int = this.length - 1; i >= 0; i--) {
			tmp[i] = this[i];
		}
		return tmp;
	}
	
	public function get [](idx : int) : int {
		throw new Error("Not Implemented");
	}
	
	public function set [](idx : int , value : T1) : void {
		throw new Error("Not Implemented");
	}
}