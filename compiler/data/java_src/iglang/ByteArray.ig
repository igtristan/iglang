/*NO-OUTPUT*/
package iglang;

public class ByteArray extends Object
{
	public function get position(): uint {return 0;}
	public function set position(val:uint): void {}
	
	public function get length():uint {return 0;}
	public function set length(val :uint): void {}
	
	public function set endian(val : int) : void {}

	public function get [](idx : int) : int { return 0; }
	public function set [](idx : int, value : int) : void {}


	/**
	 * Reads a signed byte from the byte stream.
	 **/
	public function readByte() : int {return 0;}

	/**
	 * Reads a signed 16-bit integer from the byte stream.
	 **/
	public function readShort() : int {return 0;}

	/**
	 * Reads a signed 32-bit integer from the byte stream.
	 **/
	public function readInt() : int {return 0;}
	
	/**
	 * Reads an unsigned byte from the byte stream.
	 **/
	public function readUnsignedByte() : uint {return 0;}

	/**
	 * Reads an unsigned 16-bit integer from the byte stream.
	 **/
	public function readUnsignedShort() : uint {return 0;}

	/**
	 * Reads an unsigned 32-bit integer from the byte stream.
	 **/
	public function readUnsignedInt() :uint {return 0;}
	
	
	/**
	 * Reads an IEEE 754 single-precision (32-bit) floating-point number from the byte stream.
	 **/
	public function readFloat() : int {return 0;}
	
	
	/**
	 * Reads a UTF-8 string from the byte stream.
	 **/
	public function readUTF() : String {return null;}
	
	/**
	 * Reads a sequence of UTF-8 bytes specified by the length parameter from the byte stream and returns a string.
	 **/
	public function readUTFBytes(length:uint):String {return null;}


	/**
	 * Writes a 32-bit signed integer to the byte stream.
	 **/
	public function writeInt(value:int):void {}


	/**
	 * Writes bytes from another byte array
	 **/
	 
	 public function writeBytes(bytes : ByteArray, offset : uint, length : uint) : void {}

	/**
	 * Writes the string as UTF-8 without a length header
	 **/
	 
	 public function writeUTFBytes(value : String): void {}
	 
	 /**
	 * Writes the string as UTF-8 prepended with a short specifying the length
	 **/
	 
	 public function writeUTF(value : String): void {}
	 
	/**
	 * Writes a single byte
	 **/
	 
	 public function writeByte(value : int) : void {}
	 
	 /**
	  * Writes a 32 bit unsigned integer
	  **/
	  
	 public function writeUnsignedInt(value : uint) : void {}
	 
	 /**
	  * Writes a 16 bit integer 
	  */
	  
	 public function writeUnsignedShort(value : uint): void {}
	 public function writeShort(value : int) : void {}
	 

/*	
clear():void
Clears the contents of the byte array and resets the length and position properties to 0.
ByteArray
 	 	
compress(algorithm:String):void
Compresses the byte array.
ByteArray
 	 	
deflate():void
Compresses the byte array using the deflate compression algorithm.
ByteArray
 	 	
inflate():void
Decompresses the byte array using the deflate compression algorithm.
ByteArray
 	 	
readBoolean():Boolean
Reads a Boolean value from the byte stream.
ByteArray
 	 	
 	 	
readBytes(bytes:ByteArray, offset:uint = 0, length:uint = 0):void
Reads the number of data bytes, specified by the length parameter, from the byte stream.
ByteArray
 	 	
readDouble():Number
Reads an IEEE 754 double-precision (64-bit) floating-point number from the byte stream.
ByteArray
 	 	
readMultiByte(length:uint, charSet:String):String
Reads a multibyte string of specified length from the byte stream using the specified character set.
ByteArray
 	 	
readObject():*
Reads an object from the byte array, encoded in AMF serialized format.
ByteArray
 	 	
toJSON(k:String):*
Provides an overridable method for customizing the JSON encoding of values in an ByteArray object.
ByteArray
 	 	
toString():String
Converts the byte array to a string.
ByteArray
 	 	
uncompress(algorithm:String):void
Decompresses the byte array.
ByteArray
 	 	
writeBoolean(value:Boolean):void
Writes a Boolean value.
ByteArray
 	 	
writeByte(value:int):void
Writes a byte to the byte stream.
ByteArray
 	 	
writeBytes(bytes:ByteArray, offset:uint = 0, length:uint = 0):void
Writes a sequence of length bytes from the specified byte array, bytes, starting offset(zero-based index) bytes into the byte stream.
ByteArray
 	 	
writeDouble(value:Number):void
Writes an IEEE 754 double-precision (64-bit) floating-point number to the byte stream.
ByteArray
 	 	
writeFloat(value:Number):void
Writes an IEEE 754 single-precision (32-bit) floating-point number to the byte stream.
ByteArray
 	 	
writeMultiByte(value:String, charSet:String):void
Writes a multibyte string to the byte stream using the specified character set.
ByteArray
 	 	
writeObject(object:*):void
Writes an object into the byte array in AMF serialized format.
ByteArray
 	 	
writeShort(value:int):void
Writes a 16-bit integer to the byte stream.
ByteArray
 	 	
writeUnsignedInt(value:uint):void
Writes a 32-bit unsigned integer to the byte stream.
ByteArray
 	 	
writeUTF(value:String):void
Writes a UTF-8 string to the byte stream.
ByteArray
 	 	
writeUTFBytes(value:String):void
Writes a UTF-8 string to the byte stream.	
*/
}
