package iglang;

//import iglang.util.*;

public class Vector<T1> extends Object
{
	private var m_data : Array<T1> = null;
	private var m_length : int = 0;
	private var m_capacity : int = 0;

	public function constructor() { //initial_length : int=0) {
	
		m_capacity = 8;							//(initial_length == 0) ? 8 : initial_length;
		m_data = new Array<T1>(m_capacity);
		m_length = 0;	//initial_length;
		
	}
	
	public final function reverse() : void 
	{
		var tmp :T1;
		var sidx:int = 0;
		var eidx:int = m_length-1;
		while(sidx<eidx)
		{
			tmp          = m_data[sidx];
			m_data[sidx] = m_data[eidx];
			m_data[eidx] = tmp;
			sidx++;
			eidx--;
		}	
	}	
	
	public function __ig_internalArray() : Array<T1> {
		return m_data;
	}
	
//	public function iterator() : VectorIterator<T1>
//	{
//		return new VectorIterator<T1>(this);
//	}
	
	public function get [](idx : int) : T1
	{
		if (idx < 0 || idx >= m_length) {
			throw new Error("Out of bounds");
		}
		return m_data[idx];
	}
	
	public function set [](idx : int, v : T1) : void
	{
		ensureCapacity(idx + 1);
		m_data[idx] = v;
		if (idx >= m_length) {
			m_length = idx + 1;
		}
	}
	
	
	// HRM... WTF... this is being called as a plain old vector
	private function ensureCapacity(new_cap : int) : void
	{
		if (m_capacity >= new_cap) {
			return;
		}
		
		var old_data:Array<T1> = m_data;
		
		if (m_capacity * 1.5 > new_cap) {
			new_cap = m_capacity * 1.5;
		}	
		
		m_data = old_data.copy(new_cap);
		
		
		/*
		m_data = new Array<T1>(new_cap);
		
		for (var i : int = m_length - 1; i >= 0; i--) {
			m_data[i] = old_data[i];
		}
		*/
		
		m_capacity = new_cap;
	}
	
	public final function get length(): int { 
		return m_length; 
	}
	
	public final function set length(len : int): void 
	{  
		if (len < 0) {
			throw new Error("Invalid length: " + len);
		}
		// if this is omitted then huh
		ensureCapacity(len);
		m_length = len;
	}
	
	public final function pop(): T1 { 
		m_length --;
		var ele : T1 = m_data[m_length];
		return ele;
	}
	
	public final function push(arg: T1): void 
	{
		ensureCapacity(m_length + 1);
		m_data[m_length] = arg;
		m_length ++; 
	}
	
	public final function shift(): T1 { 
	
		if (m_length == 0) {
			throw new Error("Vector empty");
		}
		
		var result : T1 = m_data[0];
		
		m_length --;
		for (var i : int = 0; i < m_length; i++) {
			m_data[i] = m_data[i+1];
		}
		
		return result;
	}
	
	public final function unshift(arg: T1) : void {
		
		ensureCapacity(m_length + 1);
		for (var i : int = m_length; i > 0; i--) {
			m_data[i] = m_data[i-1];
		}
		m_data[0] = arg;
		m_length ++;
	}
	
	/**
	 * Searches for an item in the Vector and returns the index position of 
	 * the item. The item is compared to the Vector elements using strict 
	 * equality (===). 
	 *
	 * @param searchElement The item to find in the Vector.
	 *
	 * @param fromIndex     The location in the Vector from which to start 
	 *                      searching for the item. If this parameter is 
	 *                      negative, it is treated as length + fromIndex, 
	 *                      meaning the search starts -fromIndex items from 
	 *                      the end and searches from that position forward 
	 *                      to the end of the Vector.
	 *
	 * @return              A zero-based index position of the item in the 
	 *                      Vector. If the searchElement argument is not 
	 *                      found, the return value is -1. 	 
	 **/
	public final function indexOf(searchElement:T1, fromIndex:int=0) : int { 
	
		// TODO probably add in the concept of an Object.isNull?
	
		for (var i:int = fromIndex; i < m_length; i++) {
			if (searchElement.equals(m_data[i])) {
				return i;
			}
		}
		return -1;
	}
	
	/**
	 * This maps to splice: http://help.adobe.com/en_US/FlashPlatform/reference/actionscript/3/Vector.html#splice%28%29
	 * @param startIndex  An integer that specifies the index of the element in the Vector where the insertion or deletion begins. You can use a negative integer to specify a position relative to the end of the Vector (for example, -1 for the last element of the Vector).
	 * @param deleteCount An integer that specifies the number of elements to be deleted. This number includes the element specified in the startIndex parameter. If the value is 0, no elements are deleted.
	 **/
	public final function delete(startIndex:int, deleteCount:int=1) : void 
	{
		if (startIndex < 0 || startIndex >= m_length) {
			throw new Error("Invalid start index: " + startIndex + " of " + m_length);
		}
		if (deleteCount < 0) {
			throw new Error("Invalid delete count");
		}
		
		if (startIndex + deleteCount >= m_length) {
			m_length = startIndex;
		}
		else {
			// a shuffle is required
			// this is in the middle of the array
		
			var shuffle_count : int = m_length - (startIndex + deleteCount);
			for (var i : int = 0; i < shuffle_count; i++) {
				m_data[startIndex + i] = m_data[startIndex + i + deleteCount];
			}
		
			m_length -= deleteCount;
		}
	}

	/**
	 * The way you insert objects into a vector in AS3 requires passing them to splice using variadics.
	 * Tritsan says BOO on those things so we say YAY on special thingy bingy irky worky.
	 **/
	public final function insert(index:int, obj:T1) : void {
		ensureCapacity(m_length + 1);
		
		if (index >= m_length) {
			m_data[index] = obj;
			m_length = index + 1;
		}
		else {
			for (var i : int = m_length; i > index; i--) {
				m_data[i] = m_data[i-1];
			}
			m_data[index] = obj;
			m_length++;
		}
	}
	
	
	/**
	 * Maps to "vector.IndexOf(obj) != -1".
	 * It's just cleaner code with this explicit function...
	 **/
	public final function contains(obj:T1) : bool { 
	
		return indexOf(obj,0) != -1;
	}
	
	/**
	 *
	 * Calls toString on each element and concatenates the values with a seperator
	 **/
	 
	public final function join(sep : String) : String { 
		if (m_length == 0) {
			return "";
		}
	
		// this could be greatly improved by creating a string builder of some sort
		var acc : String = m_data[0].toString();
		for (var i : int = 1; i < m_length; i++) {
			acc = acc + sep + m_data[i].toString();
		}
		
		return acc;
	
	}
	
	
	public final function first() : T1 
	{
		if (m_length == 0) {
			throw new Error("Vector empty");
		}
		return m_data[0];
	}

	public final function last() : T1 
	{
		if (m_length == 0) {
			throw new Error("Vector empty");
		}
		return m_data[m_length-1];
	}
	
	
	
     /*
	 * Sort the vector
	 */
	public final function sort(sort_function : StaticFunction<(T1,T1):int>) : void 
	{  
		var num : Array<T1> = m_data;
		var num_length : int = m_length;

		// is THIS THE WRONG ORDER??
		// values from sort function
		// a negative number, if x should appear before y in the sorted sequence
    	// 0, if x equals y
    	// a positive number, if x should appear after y in the sorted sequence

		for (var i : int = 1; i < num_length; i++)
		{
			var x : T1 = num[i];
			
			//trace("Vector.sort i: " + i + " value: " + x);
			
			var j : int = i;
			while (j > 0 && sort_function(num[ j - 1 ], x) > 0 )
			{
				num[j] = num[j-1];
				j--;
			}
			num[j] = x;
			
			//trace("Vector state: " );
			//for (var k : int = 0; k < num_length; k++) {
			//	trace("\t" + num[k]);
			//}
		}

	/*
		// what sort function is this?
		for (var j : int = 1; j < num_length; j++)    // Start with 1 (not 0)
		{
			var key : T1 = num[ j ];
			var i : int = 0;
			for(i = j - 1; (i >= 0) && sort_function(num[ i ],key) < 0; i--)   // Smaller values are moving up
			{
				 num[ i+1 ] = num[ i ];
			}
			num[ i+1 ] = key;    // Put the key in its proper location
		}
	*/
	}
}
