/*NO-OUTPUT*/
package iglang;

public class Math
{
/*
	public static function sin(v : double): double {return 0;}
	public static function cos(v : double): double {return 0;}
	public static function atan2(y : double, x : double) : double {return 0;}
	
	public static function pow(base:double, pow:double):double {return 0;}
	public static const PI : double = 3.14159265359;
	
	
	public static function sqrt(v:double) : double {return 0;}
	
	public static function random() : double {return 0;}
	
	public static function abs(v:double) : double {return 0;}
	
*/
	public static const PI : double = 3.14159265359;
	public static const DEG_TO_RAD : double = 3.14159265359 / 180.0;
	public static const RAD_TO_DEG : double = 180.0 / 3.14159265359;
	
	public static function sin   (v   :double            ) : double { throw new Error("Native Missing"); } // native
	public static function cos   (v   :double            ) : double { throw new Error("Native Missing"); } // native
	public static function tan   (v   :double            ) : double { throw new Error("Native Missing"); } // native
	public static function asin  (v   :double            ) : double { throw new Error("Native Missing"); } // native
	public static function acos  (v   :double            ) : double { throw new Error("Native Missing"); } // native
	public static function atan2 (y   :double, x : double) : double { throw new Error("Native Missing"); } // native
	public static function pow   (base:double, pow:double) : double { throw new Error("Native Missing"); } // native
	public static function sqrt  (v   :double            ) : double { throw new Error("Native Missing"); } // native
	public static function random()                        : double { throw new Error("Native Missing"); } // native
	public static function abs   (v   :double)             : double { throw new Error("Native Missing"); } // native
	public static function floor (v   :double)             : double { throw new Error("Native Missing"); } // native
	public static function ceil  (v   :double)             : double { throw new Error("Native Missing"); } // native
	public static function min   (a   :double, b:double)   : double { throw new Error("Native Missing"); } // native
	public static function max   (a   :double, b:double)   : double { throw new Error("Native Missing"); } // native
	
}
