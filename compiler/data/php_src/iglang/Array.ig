/*NO-OUTPUT*/
package iglang;

public class Array<T1> extends Object
{
	function Array(num_elements : int) {}
	
	// make it so you can't resize an array
	public function get length(): uint {throw new Error("Unimplemented");}
	
	// arrays have a fixed size and cannot be mutated
	//public function pop(): T1 {}
	//public function push(arg: T1): uint {}
	//public function shift(): T1 {}
	//public function unshift(arg: T1) : uint {}
	
	public function iterator(): Object { throw new Error("Unimplemented"); }
}