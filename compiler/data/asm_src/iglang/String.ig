package iglang;

public class String extends Object
{
	private var m_data     : Array<short>;
	private var m_hash     : int;
	private var m_has_hash : bool;		// is the hash code generated?

	internal static final function __ig_shortArrayToStringNoCopy(data : Array<short>) : String {
		var s : String = new String();
		s.m_data = data;
		//s.generateHash();
		return s;
	}
	
	internal static final function __ig_shortArrayRangeToString(data : Array<short>, start : int, len : int) : String
	{
		var new_data : Array<short> = new Array<short>(len);
		for (var i : int = 0; i < len; i++) {
			new_data[i] = data[start + i];
		}
	
	
		var s : String = new String();
		s.m_data = new_data;
		//s.generateHash();
		return s;
	}
	
	internal static final function __ig_emptyString(len : int) : String
	{
		var new_data : Array<short> = new Array<short>(len);
		//for (var i : int = 0; i < len; i++) {
		//	new_data[i] = data[start + i];
		//}
	
	
		var s : String = new String();
		s.m_data = new_data;
		//s.generateHash();
		return s;
	}
	
	
	public final function trim() : String {
		var start_index : int = 0;
		var end_index : int = m_data.length;
		var len : int = m_data.length;
		
	
		while (start_index < len) 
		{
			var c : int = m_data[start_index];
			if (!(' ' == c || '\t' == c || '\n' == c || '\r' == c)) {
				break;
			}
			start_index++;
		}
		
		while (end_index > 0) 
		{
			var c : int = m_data[end_index - 1];
			if (!(' ' == c || '\t' == c || '\n' == c || '\r' == c)) {
				break;
			}
			end_index --;
		}
	
		if (start_index == 0 && end_index == m_data.length) {
			return this;
		}
		else {
			return __ig_shortArrayRangeToString(m_data, start_index, end_index - start_index);
		}
	}
	
	internal final function __ig_strlenUTF8(): int
	{
		
		var dst_index : int= 0;
		
		
		for (var i : int = m_data.length - 1; i >= 0; i--)
		{
			var c : int = m_data[i] & 0xffff;

			if      (c < 0x0000080) 		// 24
			{
				dst_index += 1;
			}
			else if (c < 0x0000800)			// 16
			{
				dst_index += 2;
				// 110xxxxx 	10xxxxxx
			}
			else if (c < 0x0010000)			// 8
			{
				// 1110xxxx 	10xxxxxx 	10xxxxxx
				dst_index += 3;
			}
			else if (c < 0x0200000)			// 6
			{
				// 11110xxx 	10xxxxxx 	10xxxxxx 	10xxxxxx
				dst_index += 4;
			}
			else if (c < 0x4000000)			// 1
			{
				// 111110xx 	10xxxxxx 	10xxxxxx 	10xxxxxx 	10xxxxxx
				dst_index += 5;
			}
			else
			{
				// 1111110x 	10xxxxxx 	10xxxxxx 	10xxxxxx 	10xxxxxx 	10xxxxxx
				dst_index += 6;
			}
		}
		
		return dst_index;
	}
	
	
	
	private final function generateHash() : void
	{
		var  hval : int = 0;
		/*
		 * FNV-1a hash each octet in the buffer
		 */
		 
		for (var i : int = m_data.length - 1; i >= 0; i--)
		{
			hval = (hval ^ int(m_data[i])) *  0x01000193;  // FNV_32_PRIME
		}


		m_has_hash = true;
		m_hash = hval;
	}


	public function get length(): int { return m_data.length; }
	

	public function startsWith(val : String) : bool
	{
		return indexOf(val, 0) == 0;
	}
	
	public function endsWith(val : String) : bool
	{	
		var si : int = m_data.length - val.m_data.length;
		if (si < 0) {
			return false;
		}
		return indexOf(val, si) != -1;
	}

	
	/**
	 * Get the first index of a substring in a string.
	 * Second line of some text.
	 * @param val the pattern to search form
	 * @param start_index the index to start the search
	 * @return -1 if not found else the index the string was found at
	 **/
	
	
	

	
	public function indexOf(val:String, startIndex:int=0):int 
	{
		var pattern_data   : Array<short> = val.m_data;
		var pattern_length_m1 : int = pattern_data.length - 1;

		var data : Array<short> = m_data;		
		var len : int = m_data.length - pattern_length_m1; 
		
		// if this has a shorter length than the pattern
		// this loop will never be executed
		for (var i : int = startIndex; i < len; i++) 
		{
			// start matching the last character first
			for (var j : int = pattern_length_m1; j >= 0; j--) 
			{
				// what happens if the pattern is bigger than the string
				if (data[i + j] != pattern_data[j]) {
					break;
				}
				else if (j == 0) {
					return i;
				}
			}
		}
	
		return -1;
	}
	
	public function lastIndexOf(val:String, startIndex:int = 0x7fffffff) : int 
	{
		if (startIndex > m_data.length) {
			startIndex = m_data.length;
		}
		
		startIndex = startIndex - val.length;
		
		var pattern_data   : Array<short> = val.m_data;
		var pattern_length_m1 : int = pattern_data.length - 1;

		var data : Array<short> = m_data;		
		var len : int = m_data.length;

		
		// this doesn't seem terribly correct
		for (var i : int = startIndex; i >= 0; i--) 
		{
			for (var j : int = 0; j <= pattern_length_m1; j++) {
				if (data[i + j] != pattern_data[j]) {
					break;
				}
				else if (j == pattern_length_m1) {
					return i;
				}
			}
		}
	
		return -1;
	}

	
	
	public function charCodeAt(index:int=0):int {
		//underlying array will do range check
		//if (index < 0 || index >= m_data.length) {
		//	throw new Error("Exceeded bounds");
		//}
		return 0xffff & m_data[index];
	}
	
	public final function get [](key : int) : int
	{
		return 0xffff & m_data[key];
	}
	
	public final function set[](key : int, val : int) : void 
	{
		// this really shouldn't need to be here
		throw new Error("Ha.  Strings are immutable.  What are you doing?");
	}
	
	
	public function charAt(index:int=0):String {
		//if (index < 0 || index >= m_data.length) {
		//	throw new Error("Exceeded bounds");
		//}
		return String.__ig_shortArrayRangeToString(m_data, index, 1);
	}
	
	public function split(delimiter: String, limit: int = 0x7fffffff): Array<String> 
	{
		if (m_data.length < delimiter.length) {
			var tmp : Array<String> = new Array<String>(1);
			tmp[0] = this;
			return tmp;
		}
	
	
		var total_splits : int = 0;
		
		
		var pattern_data   : Array<short> = delimiter.m_data;
		var pattern_length_m1 : int = pattern_data.length - 1;

		var data : Array<short> = m_data;		
		var len : int = m_data.length - pattern_length_m1;
		
		// count the number of splits required
		for (var i : int = 0; i < len; i++) {
			for (var j : int = 0; j <= pattern_length_m1; j++) {
				if (data[i + j] != pattern_data[j]) {
					break;
				}
				else if (j == pattern_length_m1) {
					total_splits ++;
					i += pattern_length_m1;
					break;
				}
			}
		}
	
		var split_array : Array<String> = new Array<String>(total_splits + 1);
		
		{
			// count the number of splits required
		
			var split_index : int = 0;
			var last_split : int = 0;
		
			var i : int = 0;
			for (i= 0; i < len; i++) {
				for (var j : int = 0; j <= pattern_length_m1; j++) {
					if (data[i + j] != pattern_data[j]) {
						break;
					}
					else if (j == pattern_length_m1) {
					
					
						split_array[split_index] = substr(last_split, i - last_split);
						split_index ++;
						
					
						i += pattern_length_m1;
						last_split = i + 1;
						break;
					}
				}
			}
			
			split_array[split_index] = substr(last_split, data.length - last_split);
			split_index ++;
		}
		
	
		return split_array;
	}
	
	public function replace(pattern:String, repl:String) : String {
		if (m_data.length < pattern.length) {
			return this;
		}
	
		var toks : Array<String> = split(pattern);
		var result : String = toks[0];
		
	
		for (var i : int = 1; i < toks.length; i++) {
			result = result + repl + toks[i];
		}
	
		return result;
	}
	
	
	public function substring(start_index :int = 0, end_index :int = 0x7fffffff) : String {
		return substr(start_index, end_index - start_index);
	}
	
	public function substr(startIndex:int = 0, len:int = 0x7fffffff) : String 
	{
		if (len == 0) {
			return "";
		}
		
		// constrain the length to avoid exceeding the bounds of the string
		if (len > m_data.length - startIndex) {
			len = m_data.length - startIndex;
		}
		
		var endIndex : int = startIndex + len;
		if (endIndex > m_data.length) {
			endIndex = m_data.length;
		}
		if (startIndex < 0) {
			startIndex = 0;
		}
		
		var buffer_length : int = endIndex - startIndex;
		var result : Array<short> = new Array<short>(buffer_length);
		
		for (var i : int = buffer_length - 1; i >= 0; i--) {
			result[i] = m_data[startIndex + i];
		}
	
		return String.__ig_shortArrayToStringNoCopy(result);
	}
	
	
	
	public function compareTo(other : String) : int
	{
		var a : String = this;
		var b : String = other;
	
		for (var i : int = 0; i < a.length && i < b.length; i++) {
			var diff : int = a.charCodeAt(i) - b.charCodeAt(i);
			if (diff != 0) {
				return diff;
			}
		}	
		
		if (a.length == b.length) {
			return 0;	
		}
		else if (a.length < 0) {
			return -1;
		}
		else {
			return 1;
		}
	}
	
	
	//http://www.unicode.org/Public/5.2.0/ucd/
	// look at this
	
	public function toLowerCase() : String 
	{
		// first check that an actual uppercase character exists first
		var found : int = -1;	
		for (var i : int = m_data.length - 1; i >= 0 ; i--) 
		{
			var c : int = m_data[i];
			if ('A' <= c && c <= 'Z') {
				found = i;
				break;
			}
		}
		
		if (found == -1) {
			return this;
		}
		
		var tmp : Array<short> = new Array<short>(m_data.length);
		
		for (var i : int = m_data.length - 1; i >= 0 ; i--) 
		{
			var c : int = m_data[i];
			if ('A' <= c && c <= 'Z') {
				tmp[i] = c - 'A' + 'a';
			}
			else {
				tmp[i] = c;
			}
		}
		
		return String.__ig_shortArrayToStringNoCopy(tmp);
	}
	
	public function toUpperCase() : String 
	{
		// first check that an actual uppercase character exists first
		var found : int = -1;	
		for (var i : int = m_data.length - 1; i >= 0 ; i--) 
		{
			var c : int = m_data[i];
			if ('a' <= c && c <= 'z') {
				found = i;
				break;
			}
		}
		
		if (found == -1) {
			return this;
		}
		
		var tmp : Array<short> = new Array<short>(m_data.length);
		
		for (var i : int = m_data.length - 1; i >= 0 ; i--) 
		{
			var c : int = m_data[i];
			if ('a' <= c && c <= 'z') {
				tmp[i] = c - 'a' + 'A';
			}
			else {
				tmp[i] = c;
			}
		}
		
		return String.__ig_shortArrayToStringNoCopy(tmp);
	}
	
	
	
	public function localeCompare(other : String) : int 
	{
		var this_length : int = m_data.length;
		var other_length : int = (other == null) ? 0 : other.m_data.length;


		if (this_length < other_length) {
			return -1;
		}
		else if (this_length > other_length) {
			return 1;
		}
			
		for (var i : int in 0 => this_length) {
			if (int(m_data[i]) < int(other.m_data[i])) {
				return -1;
			}
			else if (int(m_data[i]) > int(other.m_data[i])) {
				return 1;
			}
		}	
		
		return 0;
	}

	/**
	 * [static] Returns a string comprising the characters represented by the Unicode character codes in the parameters.
	 **/
	public static function fromCharCode(val:int) : String {
		var str : Array<short> = new Array<short>(1);
		str[0] = val;
		return String.__ig_shortArrayToStringNoCopy(str);
	}
	
	
	public final function intValue() : int
	{
		var d : double = doubleValue();
		
		// clamp the values to the mins and maxs of integers
		if (d < -2147483648.0) {
			return -2147483648;
		}
		else if (d > 2147483647.0) {
			return 2147483647;
		}
		
		return int(d);
	}
	
	public final function boolValue() : bool
	{
		return (this.equals("true") || this.equals("1")) ;
	}
	
	public final function doubleValue() : double
	{
		if (m_data.length == 0) {
			return 0;
		}
		var i : int = 0;
		var neg : double = 1.0;
		
		if (m_data[i] == '-') {
			neg = -1.0;
			i ++;
		}
		else if (m_data[i] == '+') {
			i++;
		}
		
		var acc : double = 0;
		while (i < m_data.length)
		{
			if (m_data[i] == '.') {
				break;
			}
		
			var c : int = int(m_data[i]) - '0';
			
			if (c < 0 || c > 9) {
				return 0;
			}
			
			acc = acc * 10 + c;
			
			i++;
		}	
	
		if (i < m_data.length && m_data[i] == '.')
		{
			// skip the .
			i++;
			
			
			var mult : double = 0.1;	
			while (i < m_data.length)
			{
				var c : int = int(m_data[i]) - '0';
				if (c < 0 || c > 9) {
					return 0;
				}
				
				acc += mult * c;
				mult *= 0.1;
				i++;
			}
		}

		return acc * neg;
	}
	
	
	private static var s_to_string_tmp : Array<short> = new Array<short>(256);
	
	public final static function stringFromInt(v : int) : String
	{
		if (v == 0) {
			return "0";
		}
		else if (v == -2147483648) {
			// this is to avoid any screw up with the negate
			return "-2147483648";
		}
		var tmp : Array<short> = s_to_string_tmp;
		var neg : bool = false;

		if (v < 0) {
			neg = true;
			v   = -v;
		}

		var i : int = 0;
		do 
		{
			i ++;
			tmp[tmp.length - i] = '0' + v%10;
			v = v / 10;
		}
		while (v > 0);
		
		if (neg) {
			i++;
			tmp[tmp.length - i] = '-';
		}
		
		return String.__ig_shortArrayRangeToString(tmp,tmp.length - i,i);
	}
	
	public final static function stringFromBool(v : bool) : String
	{
		return v ? "true" : "false";
	}
	
	public final static function stringFromDouble(v : double) : String
	{
		// this hack'll clamp the value to having 4 decimal places
		// it won't be super correct however
		var frac : double = (v - double(int(v))) * 10000.0 + 10000.5;
		
		
	//	var remainder : String = "0";
		
		return stringFromInt(v) + "." + stringFromInt(frac).substr(1);
	}
	
	public final static function stringFromObject(object : Object) : String
	{
		if (object == null) { return "null"; }
		return object.toString();
	}
	
	
	public final static function append(a : String, b : String) : String
	{
		if (b == null) {
			b = "null";
		}
		
		var result : Array<short> = new Array<short>(a.m_data.length + b.m_data.length);
		for (var i : int = 0; i < a.m_data.length; i++) {
			result[i] = a.m_data[i];
		}
		
		for (var i : int = 0; i < b.m_data.length; i++) {
			result[a.m_data.length + i] = b.m_data[i];
		}
	
		return String.__ig_shortArrayToStringNoCopy(result);
	}
	
	/**
	 * Internal.  Called whenever we need to check if a string is equal to another
	 * ie   "x" == "y"
	 */
	
	internal final static  function __ig_equals(a : String, b : String) : bool
	{
		if (a == null) {	
			return b == null;
		}
		else {
			return a.equals(b);
		}
	}
	
	// umm had same signature but didn't match..
	// the signature of the one below it?
	public final override function equals(_other : Object) : bool
	{
		if (_other == null) { return false; }
		
		var other : String = String(_other);		
		return m_data.contentsEqual(other.m_data);
		

		/*
	
		// verify that the other string is not null
		if (other == null) {
			return false;
		}
		
		// verify that the hashes are idential
		//if (m_hash != other.m_hash) {
		//	return false;
		//}
	
		// verify that the length of the two strings are equal
		if (m_data.length != other.m_data.length) {
			return false;
		}
		
		for (var i : int = m_data.length - 1; i >= 0; i--) {
			if (m_data[i] != other.m_data[i]) {
				return false;
			}
		}
		
		return true;
		*/
	}
	
	public final override function toString(): String
	{
		return this;
	}
	
	public final override function hashCode(): int
	{
		if (!m_has_hash) {
			generateHash();
		}
		return m_hash;
	}
	
}
