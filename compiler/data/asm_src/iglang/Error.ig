package iglang;

public class Error extends Object
{
	private var m_message : String = null;
	private var m_stack_trace : String = null;
	

	
	/**
	 * Creates a new Error object. If message is specified, its value is 
	 * assigned to the object's Error.message property.
	 **/
	public function constructor(message:String = null)
	{
		if (message == null) {
			message = "";
		}
		m_stack_trace = Object.stackTrace();
		m_message = message;
	}

	/**
	 * Contains the message associated with the Error object. By default, 
	 * the value of this property is "Error". You can specify a message 
	 * property when you create an Error object by passing the error string 
	 * to the Error constructor function.
	 **/
	public function get message() : String
	{
		return m_message;
	}
	
	/**
	 * Returns the call stack for an error at the time of the error's 
	 * construction as a string. As shown in the following example, the first
	 * line of the return value is the string representation of the exception 
	 * object, followed by the stack trace elements.
	 **/
	public function getStackTrace() : String
	{
		return m_stack_trace;
	}
	
	public override function toString(): String
	{
		return m_message + "\n" + m_stack_trace;
	}
	
	/*
	 * Stub used to throw an error 
	 */ 
	private static function __ig_nativeError(message : String = null): void
	{
		if (message == null) {
			message = "Native Error";
		}
		throw new Error(message);
	}
	
	private static function __ig_nullPointerError() : void
	{
		throw new Error("Null Pointer Error");
	}
	
	private static function __ig_typeCastError() : void
	{
		throw new Error("Type Cast Error");
	}
	
	private static function __ig_arrayBoundsError(idx: int, max : int) : void
	{
		throw new Error("Array Bounds Error index: " + idx + " length: " + max);
	}
	
	private static function __ig_arraySizeError(sz: int) : void
	{
		throw new Error("Array Size Error: " + sz);
	}
	
	private static function __ig_divideByZeroError() : void
	{
		throw new Error("Divide by zero Error");
	}
	
	private static function __ig_debugObject(o : Object) : void
	{
		throw new Error("NYI");
	}
	
	
	/**
	 * For internal debugging.  Note this WILL be moved.
	 **/
	
	public static function __ig_debugConfig(v : int) : int
	{
		throw new Error("NYI");
	}
}
