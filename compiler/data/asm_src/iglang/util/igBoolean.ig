package iglang.util;

enum igBoolean extends bool
{
	;


	public  function toString(): String
	{
		var value : bool = this;
		return value ? "true" : "false";
	}
	
	public  function hashCode(): int
	{	
		var value : bool = this;
		return int(value);
	}
	
	public  function equals(other : igBoolean): bool
	{
		return other == this;
	}

}