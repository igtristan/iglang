package iglang.util;


enum igInteger extends int
{
	;


	public static const MAX_VALUE : int =  2147483647;
	public static const MIN_VALUE : int = -2147483648;		// TODO verify that this value gets set correctly

	public  function toString(): String
	{
		return String(int(this));
	}
	
	public  function hashCode(): int
	{
		return this;
	}
	
	public  function equals(other : igInteger): bool
	{
		return other == this;
	}
	
	
	/*
	public function countLeadingZeros() : int {
		throw new Error("Missing Native");
	}
	*/
	
	// doesn't appear to be overloaded
	public function shiftRightAndMask(shift_right : int, mask : int) : int {
		var value : int = this;
		return (value #>> shift_right) & mask;
	}
	


}