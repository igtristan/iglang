package iglang;

public class DateTime
{

	public static const UTC : String   = "UTC";
	public static const LOCAL : String = "LOCAL";


	private var m_operating_timezone : String;
	private var m_time     : double;
	
	// split returned
	private var m_fields : Array<int> = new Array<int>(11);
	private var m_timezone : String;

	public function constructor(tz : String = null, time_in_utc : double = 0) 
	{
		init(tz, time_in_utc);
	}
	
	public function init(tz : String = null, time_in_utc : double = 0) : void
	{
		if (tz == null ) {
			tz = UTC;
		}
		m_operating_timezone = tz;

		
		if (time_in_utc == 0) {
			time_in_utc = nativeGetTime();
		}	
		m_time      = time_in_utc;
		
		
		m_timezone = nativeExtractTime(m_time, m_operating_timezone, m_fields);
	}
	
	
	/**
	 * Return the current time in UTC
	 */
	public static function get current_time() : double {
		return nativeGetTime();
	}
	
	
	public static function createUTCTime(y : int, m : int, d : int, h : int, min : int, sec : int) : double
	{
		var t : double = nativeCreateTime(y,m,d,h,min,sec);
		return t;
	}
	
	public static function createUTC(y : int, m : int, d : int, h : int, min : int, sec : int) : DateTime
	{
		var t : double = nativeCreateTime(y,m,d,h,min,sec);
		return new DateTime(UTC, t);
	}

	
	
	public function addSeconds(seconds : double) : DateTime 
	{
		m_time += seconds;
		m_timezone = nativeExtractTime(m_time, m_operating_timezone, m_fields);
		return this;
	}
	
	public function addMinutes(minutes : double) : DateTime 
	{
		m_time += minutes * 60;
		m_timezone = nativeExtractTime(m_time, m_operating_timezone, m_fields);
		return this;
	}

	public function addHours(hours : double) : DateTime 
	{
		m_time += hours * 60 * 60;
		m_timezone = nativeExtractTime(m_time, m_operating_timezone, m_fields);
		return this;
	}
	
	public function asTimeZone(tz : String) : DateTime {
		return new DateTime(tz, m_time);
	}
	
	/**
	 * Time is ALWAYS returned as an UTC value in seconds
	 */
	
	public function get time() : double {
		return m_time;
	}
	
	
	
	public function get seconds() : int {
		return m_fields[0];
	}

	public function get minutes() : int {
		return m_fields[1];
	}

	public function get hours() : int {
		return m_fields[2];
	}
	
	public function get day() : int {
		return m_fields[3];
	}
	
	public function get month() : int {
		return m_fields[4];
	}
	
	public function get year() : int {
		return m_fields[5];
	}
	
	
	public function get weekday() : int {
		return m_fields[6];
	}
	
	public function get yearday() : int {
		return m_fields[7];
	}
	
	/*
	public function get timezone() : String {
		return m_timezone;
	}
	*/

	public function get timezone_offset_from_UTC_in_minutes() : int {
		return m_fields[8];
	}

	/**
	  * see strftime for format string options
	  */

	public function format(format : String) : String 
	{
		return nativeFormatTime(m_time, m_operating_timezone, format);
	}
	
	
  
	public function formatISO8601() : String
	{
		return nativeFormatTime(m_time, UTC, "%Y-%m-%dT%H:%M:%SZ");
	}


	/**
	  * Parse an iso8601 time and return the results in UTC
	  * YYYY-MM-DDTHH:MM:SSZ
	  * YYYY-MM-DDTHH:MM:SS+0400
	  */
	
	
	public static function parse(date_format : String, date_time : String) :DateTime
	{
		return new DateTime(UTC, nativeParseTime(date_format, date_time));
	}
	
	public static function parseISO8601(date_time : String) : DateTime
	{
		return new DateTime(UTC, nativeParseTime("%Y-%m-%dT%H:%M:%S%z", date_time));
	}
	
	
	/**
	 * Get the number of days in a given month.  Month is 1 indexed.  ie. January = 1
	 */
	
	public static function getNumberOfDaysInMonth(year : int, month : int) : int
	{
		var numberOfDays : int = 0;  
		if (month == 4 || month == 6 || month == 9 || month == 11) {
	  		numberOfDays = 30;  
	  	}
		else if (month == 2)  
		{ 
			var isLeapYear : bool = (year % 4 == 0 && year % 100 != 0) || (year % 400 == 0);  
	  		if (isLeapYear)  
			{ numberOfDays = 29;  }
	  		else  
			{ numberOfDays = 28;  }
		}  
		else  {
		  numberOfDays = 31;  
		}
		
		return numberOfDays;
	}
	
	/**
	 * Get the difference in seconds between two dates
	 */
	
	
	public function diff(other : DateTime) : int
	{
		return other.m_time - this.m_time;
		/*
		double difftime ( time_t time2, time_t time1 );
		This function calculates the difference in seconds between time1 and time2.
		*/
	}
	
	/*

	
	*/
		
	/**
	 * this should wrap
	 *   time(0)
	 */
	private static function nativeGetTime() : double {
		throw new Error("Missing Native");
	}
	
	/**
	 * if tz == UTC  gmtime should be used
	 * if tz == LOCAL localtime should be used
	 */
	private static function nativeExtractTime(t : double, tz : String, fields : Array<int>) : String {
		throw new Error("Missing Native");
	}
	
	/*
		char isobasictime[256];

		time_t t = time(NULL);
		struct tm * gmt = gmtime(&t);		// not sure if this is necessary

		// yes this appears to check out
		strftime(isobasictime, 256, "%Y%m%dT%H%M%SZ", gmt);
	*/

	private static function nativeFormatTime(t : double, tz : String, format : String) : String {
		throw new Error("Missing Native");
	}
	
	/**
	 * directly map results to strptime
	 */ 
	
	
	private static function nativeParseTime(format : String, data : String) : double {
		throw new Error("Missing Native");
	}
	
	private static function nativeCreateTime(year : int, month : int, day : int, hours : int, minutes : int, seconds : int) : double
	{
		throw new Error("Missint Native");
	}

}