package iglang;

import iglang.util.*;

public class Vector<T1> extends Object
{
	private var m_data     : Array<T1> = null;
	private var m_length   : int = 0;
	private var m_capacity : int = 0;

	public function constructor(default_length : int = 0) {
	
		if (default_length > 8)
		{
			m_capacity = default_length;							
			m_data = new Array<T1>(default_length);
		}
		else {
			m_capacity = 8;						
			m_data = new Array<T1>(m_capacity);	
		}
		
		// default length for vectors needs to be improved on as3
		
		m_length = (default_length > 0) ? default_length : 0;
	}
	
	
	/**
	 * Return a new Vector containing a portion of this Vector
	 * @param startIndex - the start index within this Vector
	 * @param endIndex   - the end index within this Vector (default 0xFFFFFF)
	 **/
	
	public function slice(startIndex:int = 0, endIndex:int = 0xFFFFFF) : Vector<T1> 
	{
		if (endIndex > m_length) {
			endIndex = m_length;
		}
		
		if (startIndex < 0) {
			startIndex = 0;
		}
		
		if (startIndex >= endIndex) { 
			return new Vector<T1>(); 
		}
		
		var tmp : Vector<T1> = new Vector<T1>(endIndex - startIndex);
		var tmp_idx : int = 0;
		
		for (var i : int in startIndex => endIndex) {
			tmp[tmp_idx] = m_data[i];
			tmp_idx ++;
		}
		
		return tmp;
	}
		
	/**
	 * Return a String representation of this Vector.
	 * THIS IS NOT CONSISTENT ACROSS PLATFORMS
	 */
		
	public override function toString(): String 
	{
		var first : bool = true;
		var sb : StringBuilder = new StringBuilder();
		sb.appendChar('[');
		
		for (var i : int in 0 => m_length)
		{
			if (!first) {
				sb.appendChar(',');
			}
			first = false;
			
			sb.appendString(String(m_data[i]));
		}
	
		sb.appendChar(']');
		return sb.toString();
	}
	
	/**
	 * Reverse the order of the elements in the Vector.
	 */
	
	public final function reverse() : void 
	{
		var tmp :T1;
		var sidx:int = 0;
		var eidx:int = m_length-1;
		while(sidx<eidx)
		{
			tmp          = m_data[sidx];
			m_data[sidx] = m_data[eidx];
			m_data[eidx] = tmp;
			sidx++;
			eidx--;
		}	
	}	
	
	
	
	public function iterator() : VectorIterator<T1>
	{
		return new VectorIterator<T1>(this, m_data);
	}


	/**
	 * Get an item from the Vector at a specific index. 
	 * If the idx is < 0 or > length an Error is thrown.
	 * @param idx - the index to retrieve
	 */
	 
	public final function get [] (idx : int) : T1
	{
		if (idx < 0 || idx >= m_length) {
			throw new Error("Out of bounds");
		}
		return m_data[idx];
	}
	
	/**
	 * Set an item from the Vector
	 * If the idx is < 0 an Error is thrown, otherwise the vector is resized
	 * @param idx - the index to set
	 * @param v   - the value 
	 */
	
	public final function set [] (idx : int, v : T1) : void
	{
		ensureCapacity(idx + 1);
		m_data[idx] = v;
		if (idx >= m_length) {
			m_length = idx + 1;
		}
	}
	
	private function ensureCapacity(new_cap : int) : void
	{
		if (m_capacity >= new_cap) {
			return;
		}
		
		var old_data:Array<T1> = m_data;
		
		if ((m_capacity * 4)/3 > new_cap) {
			new_cap = (m_capacity * 4)/3;
		}	
		
		m_data = old_data.resize(new_cap);
		
		m_capacity = new_cap;
	}
	
	/**
	 * Return the length of the vector
	 */
	
	public final function get length(): int { 
		return m_length; 
	}
	
	public final function set length(len : int): void 
	{  
		if (len < 0) {
			throw new Error("Invalid length: " + len);
		}
		// if this is omitted then huh
		ensureCapacity(len);
		m_length = len;
	}
	
	/**
	 * Pop an item off the end of the Vector
	 */
	
	public final function pop(): T1 { 
		m_length --;
		var ele : T1 = m_data[m_length];
		return ele;
	}
	
	/**
	 * Push an item onto the end of the Vector.
	 * If the Vector is Empty an Error is thrown
	 * @param value - the item to push
	 */
	
	public final function push(value: T1): void 
	{
		ensureCapacity(m_length + 1);
		m_data[m_length] = value;
		m_length ++; 
	}
	
	/**
	 * Remove an item from the front of the Vector.
	 * If the Vector is Empty an Error is thrown
	 */
	
	public final function shift(): T1 { 
	
		if (m_length == 0) {
			throw new Error("Vector empty");
		}
		
		var result : T1 = m_data[0];
		
		m_length --;
		for (var i : int = 0; i < m_length; i++) {
			m_data[i] = m_data[i+1];
		}
		
		return result;
	}
	
	/**
	 * Place an item onto the beginning of the Vector.  All other elements are shifted
	 * up by one position;
	 * @param value - the item to shift
	 */
	
	public final function unshift(value: T1) : void {
		
		ensureCapacity(m_length + 1);
		for (var i : int = m_length; i > 0; i--) {
			m_data[i] = m_data[i-1];
		}
		m_data[0] = value;
		m_length ++;
	}
	
	/**
	 * Searches for an item in the Vector and returns the index position of 
	 * the item. The item is compared to the Vector elements using strict 
	 * equality (===). 
	 *
	 * @param searchElement The item to find in the Vector.
	 * @param fromIndex     The location in the Vector from which to start 
	 *                      searching for the item. If this parameter is 
	 *                      negative, it is treated as length + fromIndex, 
	 *                      meaning the search starts -fromIndex items from 
	 *                      the end and searches from that position forward 
	 *                      to the end of the Vector.
	 * @return              A zero-based index position of the item in the 
	 *                      Vector. If the searchElement argument is not 
	 *                      found, the return value is -1. 	 
	 **/
	 
	public final function indexOf(searchElement:T1, fromIndex:int=0) : int { 
	
		// TODO probably add in the concept of an Object.isNull?
		var len : int = m_length;
		for (var i:int = fromIndex; i < len; i++) {
			if (searchElement.equals(m_data[i])) {
				return i;
			}
		}
		return -1;
	}
	
	/**
	 * This maps to splice: http://help.adobe.com/en_US/FlashPlatform/reference/actionscript/3/Vector.html#splice%28%29
	 * @param startIndex  An integer that specifies the index of the element in the Vector where the insertion or deletion begins. You can use a negative integer to specify a position relative to the end of the Vector (for example, -1 for the last element of the Vector).
	 * @param deleteCount An integer that specifies the number of elements to be deleted. This number includes the element specified in the startIndex parameter. If the value is 0, no elements are deleted.
	 **/
	public final function delete(startIndex:int, deleteCount:int=1) : void 
	{
		if (deleteCount == 0) {
			return;
		}
		if (startIndex < 0 || startIndex >= m_length) {
			throw new Error("Invalid start index: " + startIndex + " of " + m_length);
		}
		if (deleteCount < 0) {
			throw new Error("Invalid delete count");
		}
		
		// case where we're removing elements off of the back of the vector
		if (startIndex + deleteCount >= m_length) {
			m_length = startIndex;
		}
		else {
			// a shuffle is required
			// this is in the middle of the array
		
			var shuffle_count : int = m_length - (startIndex + deleteCount);
			for (var i : int = 0; i < shuffle_count; i++) {
				m_data[startIndex + i] = m_data[startIndex + i + deleteCount];
			}
		
			m_length -= deleteCount;
		}
	}
	
	
	

	/**
	 * The way you insert objects into a vector in AS3 requires passing them to splice using variadics.
	 * Tritsan says BOO on those things so we say YAY on special thingy bingy irky worky.
	 **/
	public final function insert(index:int, obj:T1) : void {
		ensureCapacity(m_length + 1);
		
		if (index >= m_length) {
			m_data[index] = obj;
			m_length = index + 1;
		}
		else {
			for (var i : int = m_length; i > index; i--) {
				m_data[i] = m_data[i-1];
			}
			m_data[index] = obj;
			m_length++;
		}
	}
	
	
	/**
	 * Maps to "vector.IndexOf(obj) != -1".
	 * It's just cleaner code with this explicit function...
	 **/
	public final function contains(obj:T1) : bool { 
	
		return indexOf(obj,0) != -1;
	}
	
	/**
	 *
	 * Calls toString on each element and concatenates the values with a seperator
	 **/
	 
	public final function join(sep : String) : String { 
		if (m_length == 0) {
			return "";
		}
	
		var sb : StringBuilder = new StringBuilder();
		sb.appendString(m_data[0].toString());
		// this could be greatly improved by creating a string builder of some sort
		//var acc : String = m_data[0].toString();
		for (var i : int = 1; i < m_length; i++) {
			sb.appendString(sep);
			sb.appendString(m_data[i].toString());
			//acc = acc + sep + m_data[i].toString();
		}
		
		return sb.toString();
	
	}
	
	/**
	 * Returns the element at index 0 in the Vector
	 * If the Vector is empty an error is thrown
	 */
	
	public final function first() : T1 
	{
		if (m_length == 0) {
			throw new Error("Vector empty");
		}
		return m_data[0];
	}

	/**
	 * Returns the element at index length-1 in the Vector
	 * If the Vector is empty an error is thrown
	 */

	public final function last() : T1 
	{
		if (m_length == 0) {
			throw new Error("Vector empty");
		}
		return m_data[m_length-1];
	}
	
	
	
    /**
	 * Sort the vector
	 */
	public final function sort(sort_function : Function<(T1,T1):int>) : void 
	{  
		var num : Array<T1> = m_data;
		var num_length : int = m_length;

		// is THIS THE WRONG ORDER??
		// values from sort function
		// a negative number, if x should appear before y in the sorted sequence
    	// 0, if x equals y
    	// a positive number, if x should appear after y in the sorted sequence

		for (var i : int = 1; i < num_length; i++)
		{
			var x : T1 = num[i];
			
			//trace("Vector.sort i: " + i + " value: " + x);
			
			var j : int = i;
			while (j > 0 && sort_function(num[ j - 1 ], x) > 0 )
			{
				num[j] = num[j-1];
				j--;
			}
			num[j] = x;
		}
	}
}
