package iglang;


/**
 *  Do not add any fields to this or you WILL break the vm.
 */


public class Object
{
	public function constructor()
	{
	}	
	
	public static function trace(v : String): void { throw new Error("Native Missing");  }
	public static function stackTrace(): String    { throw new Error("Native Missing");  }
	
	public function hashCode(): int                {  throw new Error("Native Missing"); }
	public function equals(v2 : Object): bool      {  return v2 == this;                 }
	public function toString() : String            {  return "";                         }
}
