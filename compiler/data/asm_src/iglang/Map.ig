package iglang;

import iglang.util.*;

public class Map<T1,T2> extends Object implements ArrayIndexable
{

	private var m_hash : Array<MapEntry<T1,T2>>;
	private var m_size : int;
	private var m_count : int;
	
	
	public function Map() {
		m_size = 31;
		m_hash = new Array<MapEntry<T1,T2>>(m_size);
	}
	
	/**
	 * Return the number of items contained by this map
	 */
	
	public function get length() : int {
		return m_count;
	}
	
	/**
	 * Return a String representation of this Map.
	 * THIS IS NOT CONSISTENT ACROSS PLATFORMS
	 */
	
	public override function toString(): String 
	{
		var first : bool = true;
		var sb : StringBuilder = new StringBuilder();
		sb.appendChar('{');
		
		for (var i : int in 0 => m_hash.length)
		{
			var ent : MapEntry<T1,T2> = m_hash[i];
			while (ent != null) 
			{
				if (!first) {
					sb.appendChar(',');
				}
				first = false;
				
				sb.appendString(String(ent.m_key));
				sb.appendChar(':');
				sb.appendString(String(ent.m_value));
				ent = ent.m_next;
			}
		}
	
		sb.appendChar('}');
		return sb.toString();
	}
	
	/**
	 * Perform the bulk of the work of the Map
	 * 0 = lookup
	 * 1 = insert/update
	 * 2 = delete
	 */
	
	private final function lookup(key : T1, mode : int) : MapEntry<T1,T2>
	{
		var idx : int = (key.hashCode() & 0x7fffffff)  % m_size;
		
		var prev  : MapEntry<T1,T2> = null;
		var entry : MapEntry<T1,T2> = m_hash[idx];
		while (entry != null) 
		{
			if (key.equals(entry.m_key)) 
			{
				//trace("Map.lookup " + key + " " + key.hashCode() + 
				//	" equals " + entry.m_key + " " + entry.m_key.hashCode());
				if (mode == 2) 
				{
					m_count --;
					if (prev == null) {
						m_hash[idx] = entry.m_next;
					}
					else
					{
						prev.m_next = entry.m_next;
					}
				}
				
				return entry;	
			}
			
			prev  = entry;
			entry = entry.m_next;
		}
		
		// return null if nothing was found if its not an addition
		if (mode != 1) {
			return null;
		}
		
		// create a new entry
		m_count ++;
		m_hash[idx] = new MapEntry<T1,T2>(m_hash[idx], key);
		return m_hash[idx];
	}
	
	public final function iterator() : MapIterator<T1,T2> {
		return new MapIterator<T1,T2>(m_hash, m_size);
	}
	
	
	/**
	 * Get an element with a specified key.
	 * @param key - the key to look up
	 * @param defaux - the value to return if the element is not found
	 */
	
	public final function getWithDefault(key : T1, defaux : T2) : T2
	{
		var me : MapEntry<T1,T2> = lookup(key, 0);
		if (me == null) { return defaux; }
		return me.m_value;
	}
	
	/**
	 * Get an element with a specified key.
	 * If the element is not found and Error is thrown.
	 * @param key - the key to look up
	 */
	
	public final function get [](key : T1) : T2
	{
		try
		{
			return lookup(key, 0).m_value;
		}
		catch (e : Error) {
			throw new Error("Key '" + String(key) + "' not found in map.");
		}
	}
	
	/**
	 * Set an element with a specified key.
	 * If an element with that key is already part of the Map, it will be overridden.
	 * @param key - the key to associate the value with
	 * @param value - the value
	 */
	
	
	public final function set [](key : T1, value : T2) : void
	{
		lookup(key, 1).m_value = value;
	}
	
	
	/**
	 * Returns true if an element with the specified key exists in the Map
	 * @param key - the key
	 */
	
	public final function containsKey(key : T1 ) : bool { 
		return (lookup(key, 0)) != null;
	}
	
	/**
	 * Delete an element with a specified key, if that key exists in the Map.
	 * If it is not found, no Error is thrown.
	 * @param key - the key
	 */
	
	public final function deleteKey(key : T1 ) : void {
		lookup(key, 2);
	}
}