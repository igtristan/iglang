
/*
public String getSafeMemberName(String member_name) {
		// TODO
		if ("<constructor>".equals(member_name)) {
			return "$constructor";
		}
		else if ("<static>".equals(member_name)) {
			return "$static";
		}
		else if ("<get[]>".equals(member_name)) {
			return "$aget";
		}
		else if ("<set[]>".equals(member_name)) {
			return "$aset";
		}
		return member_name;
	}

	public String getSafeModulePath(String module_name) {
		return "IGVM.MODULE." + getSafeModuleName(module_name);
	}

	public String getSafeModuleName(String module_name) {
		// TODO
		StringBuilder tmp = new StringBuilder();
		int len = module_name.length();
		for (int i = 0; i < len; i++) {
			final char c = module_name.charAt(i);
			switch(c) {
				case '.': { tmp.append("$$"); break; }
				case '<': { tmp.append("$B"); break; }
				case '>': { tmp.append("$E"); break; }
				case ',': { tmp.append("$_"); break; }
				
				default:
					tmp.append(c);
			}
		}
		return tmp.toString();
	}
	*/
var IGVM = (function()
{
	"use strict";

	var STRINGS = {};
	var UNSAFE  = 1;

	var m_scribble = new ArrayBuffer(8);
	
	// create views on the data buffer
	var m_scribble_f64 = new Float64Array(m_scribble);
	var m_scribble_f32 = new Float32Array(m_scribble);
	var m_scribble_i32 = new Int32Array(m_scribble);

	function getSafeModuleName(n) {
		var tmp = [];
		var len = n.length;
		for (var i = 0; i < len; i++) {
			var c = n.charAt(i);
			switch(c) {
				case '.': { tmp.push("$$"); break; }
				case '<': { tmp.push("$B"); break; }
				case '>': { tmp.push("$E"); break; }
				case ',': { tmp.push("$_"); break; }
				default: {
					tmp.push(c);
				}
			}
		}
		return tmp.join("");
	}

	function getSafeMemberName(member_name) {
		if ("<constructor>" == member_name) {
			return "$constructor";
		}
		else if ("<static>" == member_name) {
			return "$static";
		}
		else if ("<get[]>" == member_name) {
			return "$aget";
		}
		else if ("<set[]>" == member_name) {
			return "$aset";
		}
		return member_name;
	}

	function fn_trace(str) {
		var n = public_interface.stringToNative(str);
		console.log(n);
	}

	function fn_intPairToDouble(v0, v1) {
		m_scribble_i32[0] = v0;
		m_scribble_i32[1] = v1;

		//console.log("fn_intPairToDouble " + v0 + " " + v1 + " " +
		//	m_scribble_f64[0]);
		return m_scribble_f64[0];
	}

	function fn_doubleToIntBitsLo(v0) {
		m_scribble_f64[0] = v0;
		return m_scribble_i32[0];
	}

	function fn_doubleToIntBitsHi(v0) {
		m_scribble_f64[0] = v0;
		return m_scribble_i32[1];
	}

	function fn_doubleToIntBits(v0) {
		m_scribble_f32[0] = v0;
		return m_scribble_i32[0];
	}

	function fn_intBitsToDouble(v0) {
		m_scribble_i32[0] = v0;
		return m_scribble_f32[0];
	}

	function fn_nativeGetTime() { 
		return Date.now();
	}
	function fn_nativeExtractTime(t, tz, fields_array) {
		return "";
	}
	//// yes this appears to check out
	//		strftime(isobasictime, 256, "%Y%m%dT%H%M%SZ", gmt);
	function fn_nativeFormatTime(t, tz, format) {
		return "";
	}
	function fn_nativeParseTime(format,data) {
		return 0.0;
	}
	function fn_nativeCreateTime(Y,M,D,h,m,s) {
		return 0,0;
	}
		


	function fn_stackTrace() {
//		throw "Trying to error from this point";
// wish there was an assert

		var stack = new Error().stack;
		console.log("stack trace: ");
		console.log(stack);
		return public_interface.nativeToString(JSON.stringify(stack));
	}

	function fn_intToByteArray(value, buffer, length) {
		return fn_doubleToByteArray(value, buffer, length);
	}

	function fn_doubleToByteArray(value, buffer, length) {
		var val = '' + value;
		var i = 0;
		for (; i < val.length && i < length; i++) {
			buffer[i] = val.charCodeAt(i);
		}
		//console.log("double to byte array: " + val + " " + value + " " + i);
		return i;
	}

	var public_interface = 
	{
		// object prototypesa
		OBJECT: {},

		// module prototypes
		MODULE: {},

		// CONSTANT - for dealing with ArrayBuffers in an unsafe but more performant way
		UNSAFE: 1,

		OBJECT_NULL: null,

		newInt8Array: function (len_or_array) { 
			const a = new Int8Array(len_or_array);
			a.$vtable = IGVM.MODULE.iglang$$Array$Bbyte$E.$vtable;
			return a;
		},


		newInt16Array: function (len_or_array) { 
			const a = new Int16Array(len_or_array);
			a.$vtable = IGVM.MODULE.iglang$$Array$Bshort$E.$vtable;
			return a;
		},

		newInt32Array: function (len_or_array) { 
			const a = new Int32Array(len_or_array);
			a.$vtable = IGVM.MODULE.iglang$$Array$Bint$E.$vtable;
			return a;
		},

		newFloat32Array: function (len_or_array) { 
			const a = new Float32Array(len_or_array);
			a.$vtable = IGVM.MODULE.iglang$$Array$Bfloat$E.$vtable;
			return a;
		},

		newFloat64Array: function (len_or_array) { 
			const a = new Float64Array(len_or_array);
			a.$vtable = IGVM.MODULE.iglang$$Array$Bdouble$E.$vtable;
			return a;
		},

		newObjectArray: function(len) {
			const a = new Array(len).fill(null);
			a.$vtable = IGVM.MODULE[getSafeModuleName('iglang.Array<iglang.Object>')].$vtable;
			return a;
		},


		newFuncPtrArray: function(len) {
			const a = new Array(len).fill(null);
			a.$vtable = IGVM.MODULE[getSafeModuleName('iglang.Array<iglang.util.igDelegate>')].$vtable;
			return a;
		},


		dupInt8Array: function (len_or_array) { 
			const a = new Int8Array(len_or_array);
			a.$vtable = IGVM.MODULE.iglang$$Array$Bbyte$E.$vtable;
			return a;
		},


		dupInt16Array: function (len_or_array) { 
			const a = new Int16Array(len_or_array);
			a.$vtable = IGVM.MODULE.iglang$$Array$Bshort$E.$vtable;
			return a;
		},

		dupInt32Array: function (len_or_array) { 
			const a = new Int32Array(len_or_array);
			a.$vtable = IGVM.MODULE.iglang$$Array$Bint$E.$vtable;
			return a;
		},

		dupFloat32Array: function (len_or_array) { 
			const a = new Float32Array(len_or_array);
			a.$vtable = IGVM.MODULE.iglang$$Array$Bfloat$E.$vtable;
			return a;
		},

		dupFloat64Array: function (len_or_array) { 
			const a = new Float64Array(len_or_array);
			a.$vtable = IGVM.MODULE.iglang$$Array$Bdouble$E.$vtable;
			return a;
		},

		dupObjectArray: function(arr) {
			const a = arr.slice();
			a.$vtable = IGVM.MODULE[getSafeModuleName('iglang.Array<iglang.Object>')].$vtable;
			return a;
		},


		dupFuncPtrArray: function(len) {
			const a = arr.slice();
			a.$vtable = IGVM.MODULE[getSafeModuleName('iglang.Array<iglang.util.igDelegate>')].$vtable;
			return a;
		},


		isObjectInstanceOf: function(obj, native_string_name) {
			if (obj == null) {
				return true;
			}

			var mkey = getSafeModuleName(native_string_name);
			if (!IGVM.MODULE.hasOwnProperty(mkey)) {
				throw "[getDirectDescendentsOf] Module does not exist";
			}
			var vt = obj.$vtable;
			var target_vt = IGVM.MODULE[mkey].$vtable;
			while (vt != null) {
				if (vt === target_vt) {
					return true;
				}
				vt = vt.$parent;
			}
			return false;
		},

		getDirectDescendentsOf: function (native_string_name)
		{
			var mkey = getSafeModuleName(native_string_name);
			if (!IGVM.MODULE.hasOwnProperty(mkey)) {
				throw "[getDirectDescendentsOf] Module does not exist";
			}
			var vt = IGVM.MODULE[mkey].$vtable;
			const map = IGVM.MODULE;
			const result = [];
			for (var key in map) {
				if (map.hasOwnProperty(key)) {
					var module_vt = map[key].$vtable;
					if (module_vt != null &&
						module_vt.$parent === vt) {
						result.push(map[key].$name);
					}
				}
			}
			return result;
		},


		// convert a javascript string to a igvm string
		nativeToString: function (native_string) 
		{
			var len  = native_string.length;
			var buff = this.newInt16Array(len);	
			for (var i = 0; i < len; i++){
				buff[i] = native_string.charCodeAt(i);
			}

			var b = this.MODULE.iglang$$String.__ig_shortArrayToStringNoCopy(buff);
			return b;
		},

		objectNull:   null,
		delegateNull: null,

		isNull: function(igvm_object_or_fn) {
			// this coudl change in the future
			return igvm_object_or_fn === null;
		},

		// convert to igvm string to anative string
		stringToNative: function (igvm_string) 
		{
			if (igvm_string === null) {
				return null;
			}
			// this works perfectly since the internal representation
			// is just an array of shorts
			const array = igvm_string.m_data;
			return String.fromCharCode.apply(null, array);
		},

		cachedNativeToString: function (native_string) {
			if (STRINGS.hasOwnProperty(native_string)) {
				return STRINGS[native_string];
			}

			var tmp = nativeToString(native_string);
			STRINGS[native_string] = tmp;
			return tmp;
		},

		getField: function(vm_object, field_name) {
			if(null == vm_object) {
				throw "Object is null";
			}
			if (!vm_object.hasOwnProperty(field_name)) {
				throw "Field with name does not exist: " + field_name;
			}
			return vm_object[field_name];
		},

		_resizzle: function(new_array, old_array) {
			for (var i = 0; i < new_array.length && i < old_array.length; i++) {
				new_array[i] = old_array[i];
			}
			return new_array;
		},


		_invokeDynamic: function(func_ptr) 
		{
			var obj    = func_ptr.o;
			var func   = func_ptr.f; // this is already safe
			var module = func_ptr.m; // this is already safe

  			var args = (obj == null) ? [] : [obj];
			for(var i = 1; i < arguments.length; i++) {
   				args.push(arguments[i]);
  			}

  			if (!this.MODULE.hasOwnProperty(module)) {
  				throw "Module name does not exists.";
  			}

  			if (!this.MODULE[module].hasOwnProperty(func)) {
  				throw "Function name does not exist.";
  			}

  			return this.MODULE[module][func].apply(null,args);
		},

		throwException: function(text) {
			if (text == null) {
				throw "Null text passed";
			}
			text = this.nativeToString(text);


			throw this.newObject('iglang.Error', text);
		},

		arrayToNativeArrayBuffer: function(igvm_array) {
			return igvm_array.buffer;
		},


		arrayToInt32Array: function (igvm_array) {
			return new Int32Array(igvm_array.buffer);
		},

		/**
		 * Create a view on the array buffer to populate this data buffer
		 */

		populateByteArray: function(byte_array, array_buffer, offset, length) {
			if (arguments.length < 3) { offset = 0; }
			if (arguments.length < 4) { length = array_buffer.byteLength; }
		
			if (isNaN(length) || isNaN(offset)) {
				throw "length and offset must be numbers";
			}

			var data = new Int8Array(array_buffer, offset, length);


			data.$vtable = IGVM.MODULE[getSafeModuleName('iglang.Array<byte>')].$vtable;
			byte_array.m_data = data;
			byte_array.m_position = 0;
			byte_array.m_length = length;
		},
		/**
		 * Create a new ByteArray from the contest of an ArrayBuffer
		 **/

		nativeArrayBufferToByteArray: function(array_buffer, offset, length, mode) 
		{
			// TODO check that array buffer is a Buffer

			if (arguments.length < 2) { offset = 0; }
			if (arguments.length < 3) { length = array_buffer.byteLength; }
			if (arguments.length < 4) { mode   = 0;  }
		
			if (isNaN(length) || isNaN(offset) || isNaN(mode)) {
				throw "length, offset and mode must be numbers";
			}

			// I think? we can specify offset and legth
			var data = null;

			if (mode == UNSAFE) {
				// make the storage directly reference the array buffer passed in
				data = new Int8Array(array_buffer, offset, length);
			}
			else {
				// create a new array buffer and copy the data from the exiting one
				var dst = new ArrayBuffer(length);
    			data = new Int8Array(dst);
    			data.set(new Int8Array(array_buffer,offset,length));
			}



			data.$vtable = IGVM.MODULE[getSafeModuleName('iglang.Array<byte>')].$vtable;
			var ret = this.call(
				'iglang.ByteArray', '__ig_createWithData', data);

			return ret;
		},

		byteArrayToNativeArrayBuffer: function(byte_array, mode) {
			if (arguments.length < 2) {
				mode = 0;
			}

			// extract fields from the raw byte array
			const src = byte_array.m_data;
			const len = byte_array.m_length;


			// return the underlying arrayBuffer iff unsafe mode is set
			// and the array occupies the entirety of that buffer
			if (mode === UNSAFE && src.byteOffset === 0 &&  src.byteLength === len) {
				return src.buffer;
			}
			else {
				//console.log("len as reported by byte array: " + len);
				const dst = new ArrayBuffer(len);
    			new Uint8Array(dst).set(src.slice(0, len));
    			return dst;
			}
		},

		/**
		 * Create a new instance of a named module
		 * arguments to the constructor are passed as optional parameters
		 */ 

		newObject: function(module /* ,arg0,arg1,....,argN */) 
		{

			module = getSafeModuleName(module);
			const func   = getSafeMemberName('<constructor>');

			if (!this.MODULE.hasOwnProperty(module)) {
  				throw "[newObject] Module name does not exists.";
  			}

  			if (!this.OBJECT.hasOwnProperty(module)) {
  				throw "[newObject] Module of this type cannot be instantiated.";
  			}

  			if (!this.MODULE[module].hasOwnProperty(func)) {
  				throw "[newObject] Constructor does not exist in this module.";
  			}

  			const obj  = new this.OBJECT[module];
  			const args = [obj];
			for(var i = 1; i < arguments.length; i++) {
				if (typeof arguments[i] === 'undefined') {
					throw ("[newObject] Undefined value passed at: " + i + " of " + arguments.length);
				}
   				args.push(arguments[i]);
  			}

  			// todo add this $name bs to the vtable
  			this.MODULE[module][func].apply(null,args);
  			obj['$name'] = module;
			return obj;
		},


		/**
		 * Invoke a named function on a vm object
		 *
		 */

		callNamed: function(fn_name, obj  /* ,arg0,arg1,....,argN */)
		{
			// TODO verify that the first argument is a string
			if (obj === null) {
				throw "[callNamed] Null object passed";
			}

			var args = [];
			for(var i = 1; i < arguments.length; i++) {
				if (typeof arguments[i] === 'undefined') {
					throw "[callNamed] Undefined value passed: " + i;
				}
   				args.push(arguments[i]);
  			}

  			if (!obj.hasOwnProperty('$vtable') ||
  				!obj.$vtable.hasOwnProperty(fn_name)) {
  				throw "[callNamed] Object does not contain function: " + fn_name + " '" + obj['$name'] + "'";
  			}

  			return obj.$vtable[fn_name].apply(null, args);
		},

		/**
		 * Invoke a concrete function from a specific module
		 * arguments to the function are passed as optional parameters
		 */

		call: function(module, func  /* ,arg0,arg1,....,argN */) 
		{
			var args = [];
			for(var i = 2; i < arguments.length; i++) {
				if (typeof arguments[i] === 'undefined') {
					throw "Undefined value passed: " + i;
				}
   				args.push(arguments[i]);
  			}

  			module = getSafeModuleName(module);
  			func   = getSafeMemberName(func);

  			if (!this.MODULE.hasOwnProperty(module)) {
  				throw "[call] Module name does not exists: " + module;
  			}

  			if (!this.MODULE[module].hasOwnProperty(func)) {
  				throw "[call] Function name does not exist: " + func + " in module: " + module;
  			}

  			return this.MODULE[module][func].apply(null,args);
		},

		setNative: function(module, func, new_func) {

			module = getSafeModuleName(module);
  			func   = getSafeMemberName(func);

  			if (!this.MODULE.hasOwnProperty(module)) {
  				throw "[setNative] Module name does not exists: " + module;
  			}

  			if (!this.MODULE[module].hasOwnProperty(func)) {
  				throw "[setNative] Function name does not exist: " + func;
  			}


  			// update the static reference
			var existing_func = this.MODULE[module][func];
			this.MODULE[module][func] = new_func;

			// updte the re
			for (var obj_key in this.MODULE) {
				var module = this.MODULE[obj_key];
				var vt     = module["$vtable"];
				if (null !== vt && vt.hasOwnProperty(func) && 
					vt[func] === existing_func) {
					vt[func] = new_func;
				}
			}

			return 0;
		},

		init: function()
		{
			this.setNative("iglang.Math", "sin",   Math.sin);
			this.setNative("iglang.Math", "cos",   Math.cos);
			this.setNative("iglang.Math", "tan",   Math.tan);
			this.setNative("iglang.Math", "asin",  Math.asin); //
			this.setNative("iglang.Math", "acos",  Math.acos); //
			this.setNative("iglang.Math", "atan2", Math.atan2); //
			this.setNative("iglang.Math", "atan",  Math.atan); //
			this.setNative("iglang.Math", "pow",   Math.pow);
			this.setNative("iglang.Math", "sqrt",  Math.sqrt);
			this.setNative("iglang.Math", "random", Math.random);
			this.setNative("iglang.Math", "abs",    Math.abs);
			this.setNative("iglang.Math", "floor",  Math.floor);
			this.setNative("iglang.Math", "ceil",   Math.ceil);
			this.setNative("iglang.Math", "round",  Math.round);
			this.setNative("iglang.Math", "min",    Math.min);
			this.setNative("iglang.Math", "max",    Math.max);
			this.setNative("iglang.Double", "intPairToDouble", fn_intPairToDouble);
			this.setNative("iglang.Double", "doubleToIntBitsLo", fn_doubleToIntBitsLo);
			this.setNative("iglang.Double", "doubleToIntBitsHi", fn_doubleToIntBitsHi);
			this.setNative("iglang.Double", "doubleToIntBits", fn_doubleToIntBits);
			this.setNative("iglang.Double", "intBitsToDouble", fn_intBitsToDouble);
			this.setNative("iglang.Object", "trace", fn_trace);
			this.setNative("iglang.Object", "stackTrace", fn_stackTrace);
			this.setNative("iglang.StringBuilder", "intToByteArray", fn_intToByteArray);
			this.setNative("iglang.StringBuilder", "doubleToByteArray", fn_doubleToByteArray);


			this.setNative("iglang.DateTime", "nativeGetTime", fn_nativeGetTime);
			
			// TODO uncomment when these are implemented
			//this.setNative("iglang.DateTime", "nativeExtractTime", fn_nativeExtractTime);
			//this.setNative("iglang.DateTime", "nativeFormatTime", fn_nativeFormatTime);
			//this.setNative("iglang.DateTime", "nativeParseTime", fn_nativeParseTime);
			//this.setNative("iglang.DateTime", "nativeCreateTime", fn_nativeCreateTime);

		}

	};

	return public_interface;
})();
