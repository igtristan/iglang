package iglang.util;

public enum igShort extends int
{
	;
	
	public  function toString(): String
	{
		return String(int(this));
	}
	
	public  function hashCode(): int
	{
		return int(this);
	}
	
	public  function equals(other : igShort): bool
	{
		return int(other) == int(this);
	}
	
}