/*NO-OUTPUT*/
package iglang;

public class String extends Object
{
	public function get length(): int { return 0;}
	
	/**
	 * Return true if this string starts with the specified string
	 */ 
	
	public function startsWith(val : String) : bool { return false; }
	
	/**
	 * Return true if the string ends with the specified string
	 */
	
	public function endsWith(val : String) : bool { return false; }
	
	/**
	 * Get the first index of a substring in a string.
	 * Second line of some text.
	 * @param val the pattern to search form
	 * @param start_index the index to start the search
	 * @return -1 if not found else the index the string was found at
	 **/
	
	public function indexOf(val:String, start_index:int=0):int {return 0;}
	
	/**
	 * Get the unicode character code at specified index
	 * @param index the index to get the character
	 * @return the value at that index
	 **/
	
	public function charCodeAt(index:int=0):int {return 0;}
	
	/**
	 * Return the character as a string from the specific index of the string
	 */
	
	public function charAt(index:int=0):String {return null;}
	
	public function split(delimiter:String, limit:int = 0x7FFFFFFF): Array<String> {return null;}
	
	
	public function lastIndexOf(val:String, startIndex:int = 0x7FFFFFFF) : int {return 0;}
	
	public function substr(startIndex:double = 0, len:int = 0x7FFFFFFF) : String {return null;}
	
	public function substring(start_index :int = 0, end_index :int = 0x7fffffff) : String { return null; }
	
	public function replace(pattern:String, repl:String) : String {return null;}
	
	public function toLowerCase() : String {return null; }
	
	public function toUpperCase() : String { return null; }
	
	public function localeCompare(other : String) : int {return 0;}

	/**
	 * [static x] Returns a string comprising the characters represented by the Unicode character codes in the parameters.
	 **/
	public static function fromCharCode(val:int) : String {return null;}
	
	public function compareTo(other : String) : int { return 0; }
	
	public final function trim() : String {
		throw new Error("NO OUTPUT");
	}
}
