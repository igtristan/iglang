/*NO-OUTPUT*/
package iglang;

public class ByteArray extends Object
{
	public function get position(): uint {return 0;}
	public function set position(val:uint): void {}
	
	public function get length():uint {return 0;}
	public function set length(val :uint): void {}
	
	public function set endian(val : Endian) : void {}
	public function get endian() : Endian {
		throw new Error("NO OUTPUT");
	}

	/**
	 * Reads a signed byte from the byte stream.
	 **/
	public function readByte() : int {return 0;}

	/**
	 * Reads a signed 16-bit integer from the byte stream.
	 **/
	public function readShort() : int {return 0;}

	/**
	 * Reads a signed 32-bit integer from the byte stream.
	 **/
	public function readInt() : int {return 0;}
	
	/**
	 * Reads an unsigned byte from the byte stream.
	 **/
	public function readUnsignedByte() : int {return 0;}

	/**
	 * Reads an unsigned 16-bit integer from the byte stream.
	 **/
	public function readUnsignedShort() : int {return 0;}
	
	
	/**
	 * Reads an IEEE 754 single-precision (32-bit) floating-point number from the byte stream.
	 **/
	public function readFloat() : double {return 0;}
	
	/**
	 * Reads an IEEE 754 double-precision (64-bit) floating-point number from the byte stream.
	 */
	 
	public function readDouble() :  double {
		throw new Error("NO OUTPUT");
	}
	
	/**
	 * Reads a UTF-8 string from the byte stream.
	 **/
	public function readUTF() : String {return null;}
	
	/**
	 * Reads a sequence of UTF-8 bytes specified by the length parameter from the byte stream and returns a string.
	 **/
	public function readUTFBytes(length:uint):String {return null;}


	/**
	 * Writes a 32-bit signed integer to the byte stream.
	 **/
	public function writeInt(value:int):void {}


	/**
	 * Writes bytes from another byte array
	 **/
	 
	public function writeBytes(bytes : ByteArray, offset : uint, length : uint) : void {}

	/**
	 * Writes the string as UTF-8 without a length header
	 **/
	 
	public function writeUTFBytes(value : String): void {}
	 
	 /**
	 * Writes the string as UTF-8 prepended with a short specifying the length
	 **/
	 
	public function writeUTF(value : String): void {}
	
	/**
	 * Writes a single character in UTF-8 encoding
	 */
	 
	public final function writeUTFCharacter(c : int) : void {
		throw new Error("NO OUTPUT"); 
	}
	 
	/**
	 * Writes a single byte
	 **/
	 
	public function writeByte(value : int) : void {}
	 
	 /**
	  * Writes a 32 bit unsigned integer
	  **/
	  
	//public function writeUnsignedInt(value : uint) : void {}
	 
	 /**
	  * Writes a 16 bit integer 
	  */
	  
	//public function writeUnsignedShort(value : uint): void {}
	public function writeShort(value : int) : void {}
	 
	public function writeFloat(value : double) : void {
	} 
	
	public function writeDouble(value : double) : void {
	
	}
	 
	public function get[](idx : int) : T1 {
		throw new Error("NO OUTPUT");
	}
	
	public function set[](idx : int, value : T1) : void {
		throw new Error("NO OUTPUT");
	}
	 
	 
}
