package iglang;

public class DateTime
{

	public static const UTC : String   = "UTC";
	public static const LOCAL : String = "LOCAL";


	private var m_operating_timezone : String = null;
	private var m_time     : double = 0;
	
	// split returned
	private var m_fields : Array<int> = new Array<int>(11);
	private var m_timezone : String = null;

	public function constructor(tz : String = null, time_in_utc : double = 0) {
		init(tz, time_in_utc);
	}
	
	public function init(tz : String = null, time_in_utc : double = 0) : void{	
		if (tz == null ) {
			tz = UTC;
		}
		m_operating_timezone = tz;

		if (time_in_utc == 0) {
			time_in_utc = nativeGetTime();
		}	
		m_time      = time_in_utc;
		
		m_timezone = nativeExtractTime(m_time, m_operating_timezone, m_fields);
	}
	
	public static function get current_time() : double {
		return nativeGetTime();
	}
	
	
	public static function createUTC(y : int, m : int, d : int, h : int, min : int, sec : int) : DateTime
	{
		var t : double = nativeCreateTime(y,m,d,h,min,sec);
		return new DateTime(UTC, t);
	}
	
	public static function createUTCTime(y : int, m : int, d : int, h : int, min : int, sec : int) : double
	{
		var t : double = nativeCreateTime(y,m,d,h,min,sec);
		return t;
	}
	
	public function addSeconds(seconds : double) : DateTime 
	{
		m_time += seconds;
		m_timezone = nativeExtractTime(m_time, m_operating_timezone, m_fields);
		return this;
	}
	
	public function addMinutes(minutes : double) : DateTime 
	{
		m_time += minutes * 60;
		m_timezone = nativeExtractTime(m_time, m_operating_timezone, m_fields);
		return this;
	}

	public function addHours(hours : double) : DateTime 
	{
		m_time += hours * 60 * 60;
		m_timezone = nativeExtractTime(m_time, m_operating_timezone, m_fields);
		return this;
	}
	
	public function asTimeZone(tz : String) : DateTime {
		return new DateTime(tz, m_time);
	}
	
	/*
	public function get local() : DateTime
	{
		return new DateTime(LOCAL, m_time);
	}
	
	public function get in_UTC() : DateTime {
		return new DateTime(UTC, m_time);	
	}
	*/
	
	/**
	 * Time is ALWAYS returned as an UTC value
	 */
	
	public function get time() : double {
		return m_time;
	}
	
	public function get seconds() : int {
		return m_fields[0];
	}

	public function get minutes() : int {
		return m_fields[1];
	}

	public function get hours() : int {
		return m_fields[2];
	}
	
	public function get day() : int {
		return m_fields[3];
	}
	
	public function get month() : int {
		return m_fields[4];
	}
	
	public function get year() : int {
		return m_fields[5];
	}
	
	
	public function get weekday() : int {
		return m_fields[6];
	}
	
	public function get yearday() : int {
	
		var d : int = day;
		for (var i : int in 1 => this.month) {
			d += getNumberOfDaysInMonth(this.year, i);
		}
		return d;
	}
	
	/*
	public function get timezone() : String {
		return m_timezone;
	}
	*/

	public function get timezone_offset_from_UTC_in_minutes() : int {
		return m_fields[8];
	}

	/**
	  * see strftime for format string options
	  */

	public function format(format : String) : String 
	{
		return nativeFormatTime(m_time, m_operating_timezone, format);
	}
	
	
  
	public function formatISO8601() : String
	{
		return nativeFormatTime(m_time, UTC, "%Y-%m-%dT%H:%M:%SZ");
	}


	/**
	  * Parse an iso8601 time and return the results in UTC
	  * YYYY-MM-DDTHH:MM:SSZ
	  * YYYY-MM-DDTHH:MM:SS+0400
	  */
	
	
	public static function parse(date_format : String, date_time : String) :DateTime
	{
		return new DateTime(UTC, nativeParseTime(date_format, date_time));
	}
	
	public static function parseISO8601(date_time : String) : DateTime
	{
		return new DateTime(UTC, nativeParseTime("%Y-%m-%dT%H:%M:%S%z", date_time));
	}
	
	
	/**
	 * Get the number of days in a given month.  Month is 1 indexed.  ie. January = 1
	 */
	
	public static function getNumberOfDaysInMonth(year : int, month : int) : int
	{
		var numberOfDays : int = 0;  
		if (month == 4 || month == 6 || month == 9 || month == 11) {
	  		numberOfDays = 30;  
	  	}
		else if (month == 2)  
		{ 
			var isLeapYear : bool = (year % 4 == 0 && year % 100 != 0) || (year % 400 == 0);  
	  		if (isLeapYear)  
			{ numberOfDays = 29;  }
	  		else  
			{ numberOfDays = 28;  }
		}  
		else  {
		  numberOfDays = 31;  
		}
		
		return numberOfDays;
	}
	
	/**
	 * Get the difference in seconds between two dates
	 */
	
	
	public function diff(other : DateTime) : int
	{
		return other.m_time - this.m_time;
		/*
		double difftime ( time_t time2, time_t time1 );
		This function calculates the difference in seconds between time1 and time2.
		*/
	}
	
	
		
	/**
	 * this should wrap
	 *   time(0)
	 */
	private static function nativeGetTime() : double {
@as3	return new Date().time / 1000.0;
		return 0;
	}
	
	/**
	 * if tz == UTC  gmtime should be used
	 * if tz == LOCAL localtime should be used
	 */
	private static function nativeExtractTime(t : double, tz : String, fields : Array<int>) : String {
@as3    var d : Date = new Date(t$0 * 1000);
@as3    if (tz$0 == "UTC") {
@as3		fields$0[0] = d.secondsUTC;
@as3		fields$0[1] = d.minutesUTC;
@as3		fields$0[2] = d.hoursUTC;
@as3		fields$0[3] = d.dateUTC;
@as3		fields$0[4] = d.monthUTC + 1;
@as3		fields$0[5] = d.fullYearUTC;
@as3		fields$0[6] = d.dayUTC;	 // weekday
@as3		fields$0[7] = 0;	         // yearday (calculated dynamically)
@as3		fields$0[8] = 0;	         // timezone offset in minutes
@as3	}
@as3    else if (tz$0 == "LOCAL") {
@as3		fields$0[0] = d.seconds;
@as3		fields$0[1] = d.minutes;
@as3		fields$0[2] = d.hours;
@as3		fields$0[3] = d.date;
@as3		fields$0[4] = d.month + 1;
@as3		fields$0[5] = d.fullYear;
@as3		fields$0[6] = d.day;	            // weekday
@as3		fields$0[7] = 0;	                // yearday (calculated dynamically)
@as3		fields$0[8] = -d.timezoneOffset;	// timezone offset in minutes.  Minutes needed to add to local time to get UTC
@as3	} else { throw new Error("Invalid Timezone"); }

		// do we ever even use this returned value?
		return null;
	}
	
	/*
		char isobasictime[256];

		time_t t = time(NULL);
		struct tm * gmt = gmtime(&t);		// not sure if this is necessary

		// yes this appears to check out
		strftime(isobasictime, 256, "%Y%m%dT%H%M%SZ", gmt);
	*/


	private static function twoDigit(n : int) : String {
		return "" + ((n/10) % 10) + (n % 10);
	}

	private static function fourDigit(n : int) : String {
		return "" + ((n/1000) % 10) + ((n/100) % 10) + ((n/10) % 10) + (n % 10);
	}
	
	public static function readTwoDigit(v : String, offset : int) : int {
		var c1 : int = (v.charCodeAt(offset + 0) - '0') * 10;
		var c0 : int = (v.charCodeAt(offset + 1) - '0') * 1;	
		return c1 + c0;
	}

	public static function readFourDigit(v : String, offset : int) : int {
		var c3 : int = (v.charCodeAt(offset + 0) - '0') * 1000;
		var c2 : int = (v.charCodeAt(offset + 1) - '0') * 100;	
		var c1 : int = (v.charCodeAt(offset + 2) - '0') * 10;
		var c0 : int = (v.charCodeAt(offset + 3) - '0') * 1;	
		return c3 + c2 + c1 + c0;
	}
	
	public static function readFourDigitTZOffset(v : String, offset : int) : int {
		var c3 : int = (v.charCodeAt(offset + 0) - '0') * 600;
		var c2 : int = (v.charCodeAt(offset + 1) - '0') * 60;	
		var c1 : int = (v.charCodeAt(offset + 2) - '0') * 10;
		var c0 : int = (v.charCodeAt(offset + 3) - '0') * 1;	
		return c3 + c2 + c1 + c0;
	}
	private static function nativeFormatTime(t : double, tz : String, format : String) : String 
	{
		var new_fields : Array<int> = new Array<int>(10);
		nativeExtractTime(t, tz, new_fields);
	
		var result : String = "";
		for (var i : int = 0; i < format.length; i++) 
		{
			var c : int = format.charCodeAt(i);
			if (c == '%') {
				i++;
				c = format.charCodeAt(i);
				switch (c) {
					case ('Y') { // year	
						result += fourDigit(new_fields[5]);
					}
					case ('m') { // month
						result += twoDigit(new_fields[4]);
					}
					case ('d') { // day
						result += twoDigit(new_fields[3]);
					}
					case ('H') { // hour
						result += twoDigit(new_fields[2]);
					}
					case ('M') { // min
						result += twoDigit(new_fields[1]);					
					}
					case ('S') { // sec
						result += twoDigit(new_fields[0]);					
					}
					case ('z') {
						if (new_fields[8] == 0) {
							result += "Z";
						}
						else 
						{
							// adjust the value so that it can be properly formatted
							var v : int = new_fields[8];
							if (v < 0) { v = -v; }
							var top : int = v / 60;
							v = (v % 60) + top * 100;
							
							if (new_fields[8] < 0) {
								result += "-";
								result += fourDigit(v);
							}
							else if (new_fields[8] > 0) {
								result += "+";
								result += fourDigit(v);
							}
						}
					}
					default {
						throw new Error("Invalid format symbol");
					}
				}
			}
			else {
				result += format.charAt(i);
			}
		}

		return result;
	}
	
	/**
	 * directly map results to strptime
	 */ 
	
	
	private static function nativeParseTime(format : String, data : String) : double 
	{
		var y   : int = 0;
		var m   : int = 0;
		var d   : int = 0;
		var h   : int = 0;
		var min : int = 0;
		var sec : int = 0;
		var tz  : int = 0;
		
		
		var i_data : int = 0;
		for (var i : int = 0; i < format.length; i++) 
		{
			var c : int = format.charCodeAt(i);
			if (c == '%') {
				i++;
				c = format.charCodeAt(i);
				switch (c) {
					case ('Y') { // year	
						y = readFourDigit(data, i_data); i_data += 4;
					}
					case ('m') { // month
						m = readTwoDigit(data, i_data); i_data += 2;
					}
					case ('d') { // day
						d = readTwoDigit(data, i_data); i_data += 2;
					}
					case ('H') { // hour
						h = readTwoDigit(data, i_data); i_data += 2;
					}
					case ('M') { // minutes	
						min = readTwoDigit(data, i_data); i_data += 2;
					}
					case ('S') { // seconds				
						sec = readTwoDigit(data, i_data); i_data += 2;
					}
					case ('z') { // UTC timezone offset
					
						var c0 : int = data.charCodeAt(i_data); i_data++;
						if (c0 == 'Z') {
							
						}
						else if (c0 == '-') {
							tz = readFourDigitTZOffset(data, i_data); i_data += 4;
						}
						else if (c0 == '+') {
							tz = readFourDigitTZOffset(data, i_data); i_data += 4;
						}		
						
						if (c0 == '+') {
							tz = -tz;
						}				
					}
					default {
						throw new Error("Invalid format symbol");
					}
				}
			}
			else
			{
				i_data ++;
				if (c != data.charCodeAt(i_data)) {
				 	throw new Error("Invalid Time");
				}
			}
		}

		return nativeCreateTime(y,m,d,h,min,sec) + tz * 60;
	}
	
	/**
	 * AS3 expects month to be in the range 0..11
	 */
	
	private static function nativeCreateTime(year : int, month : int, day : int, hours : int, minutes : int, seconds : int) : double
	{
@as3	return Date.UTC(year$0, month$0 - 1, day$0, hours$0, minutes$0, seconds$0) / 1000.0;
		throw new Error("Missint Native");
	}

}