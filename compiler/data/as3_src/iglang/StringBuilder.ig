package iglang;



public class StringBuilder
{
	private var m_length : int = 0;
	private var m_data : Vector<String> = new Vector<String>();

	public function constructor(cap : int = 0) {
	}
	
	
	public function get length() : int {
		return m_length;
	}
	
	public function set length(v : int) : void {
		m_length = v;
	}

	public function appendObject(other : Object) : StringBuilder
	{
		if (other == null) {
			return appendString("null");
		}
		
		return appendString(other.toString());
	}
	
	public function appendString(other : String) : StringBuilder
	{
		m_data.push(other);
		m_length += other.length;
		return this;
	}
	
	public function appendChar(other : int) : StringBuilder
	{
		m_data.push(String.fromCharCode(other));
		m_length ++;
		return this;
	}
	
	public function appendBool(other : bool) : StringBuilder
	{
		if (other) {
			appendString("true");
		}
		else {
			appendString("false");
		}
		return this;
	}
	
	

	
	public function appendDouble(value : double) : StringBuilder
	{
		appendString(String(value));
		return this;
	}
	
	public function appendInt(value : int) : StringBuilder
	{
		appendString(String(value));
		return this;
	}
	
	public override function toString() : String {
		return m_data.join("");	
	}
}