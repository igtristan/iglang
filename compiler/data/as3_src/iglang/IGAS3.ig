package iglang;


@as3 import mx.utils.StringUtil;
@as3 import flash.utils.ByteArray;
@as3 import flash.utils.Endian;

public class IGAS3
{

	 public static function __garbage__() : void
	 {

@as3 }



@as3 public static function stringStartsWith(s0 : String, s1 : String) : Boolean {
@as3	return s0.indexOf(s1) === 0;
@as3 }


@as3 public static function stringEndsWith(s0 : String, s1 : String) : Boolean {
@as3    var si : int = s0.length - s1.length;
@as3    if (si < 0) { return false; }
@as3	return s0.indexOf(s1, si) != -1;
@as3 }

@as3 public static function newByteArray() : ByteArray
@as3 {
@as3     var ba : ByteArray = new ByteArray();
@as3     ba.endian = Endian.LITTLE_ENDIAN;
@as3     return ba;
@as3 }

// TODO implement
@as3 public static function newVectorDouble(sz : int = 0) : Vector.<Number> {
@as3      var tmp : Vector.<Number> = new Vector.<Number>(sz);
@as3      vectorFill(tmp, 0.0);
@as3      return tmp;
@as3 }

// TODO implement
@as3 public static function newArrayDouble(sz : int = 0) : Vector.<Number> {
@as3      var tmp : Vector.<Number> = new Vector.<Number>(sz, true);
@as3      vectorFill(tmp, 0.0);
@as3      return tmp;
@as3 }

@as3 public static function vectorFill(target : *,  value : *) : void
@as3 {
@as3      for (var i : int = target.length - 1; i >= 0; i--) { target[i] = value; }    
@as3 }

@as3 public static function vectorCopy(target : *,  value : *) : void
@as3 {
@as3      for (var i : int = target.length - 1; i >= 0; i--) { target[i] = value[i]; }    
@as3 }

@as3	public static function containsKey(map : Object, key : *): Boolean {
@as3		return map[key] !== undefined;
@as3	}

@as3	public static function deleteKey(map : Object, key : *): void
@as3	{
@as3		delete map[key];
@as3	}
	
@as3	public static function containsKey2(map : Object, key : *): Boolean {
@as3		return map[key] !== undefined;
@as3	}

@as3	public static function mapGetWithDefault(map : Object, key : *, defaux : *): * {
@as3		if (map[key] !== undefined) { return map[key]; }
@as3		return defaux;
@as3	}


@as3	public static function createMapObjectFromArray(data : Array): Object
@as3	{	// objects can be keyed by ints and strings natively
@as3		var obj : Object = new Object();
@as3        for (var i : int = 0; i < data.length; i+= 2) {
@as3			obj[data[i]] = data[i+1];
@as3		}
@as3		return obj;
@as3	}	
	
@as3	public static function createMapDictionaryFromArray(data : Array): Dictionary
@as3	{	// objects can be keyed by ints and strings natively
@as3		var obj : Dictionary = new Dictionary();
@as3        for (var i : int = 0; i < data.length; i+= 2) {
@as3			obj[data[i]] = data[i+1];
@as3		}
@as3		return obj;
@as3	}	
	
	
		
@as3	public static function trim(s : String) : String {
@as3		return mx.utils.StringUtil.trim(s);
@as3	}

@as3    public static function objectLength(map :Object):int {
@as3     var cnt:int=0;
@as3     for (var s:* in map) { cnt++; }
@as3     return cnt;
@as3    }

@as3 public static function __term__() : void {
	}
	



}
