/*NO-OUTPUT*/
package iglang;

import iglang.unsafe.*;

public class Array<T1> extends Object
{
	public function iterator(): ArrayIterator<T1> { throw new Error("Incomplete"); }
	
	public function Array(num_elements : int) {}
	
	// make it so you can't resize an array
	public function get length(): int {return 0;}
	
	public function get[](idx : int) : T1 {
		throw new Error("NO OUTPUT");
	}
	
	public function set[](idx : int, value : T1) : void {
		throw new Error("NO OUTPUT");
	}
	
	/**
	 * Maps to "vector.IndexOf(obj) != -1".
	 * It's just cleaner code with this explicit function...
	 **/
	public function contains(obj:T1) : bool { return false;}
	
	
	/**
	 *
	 * Calls toString on each element and concatenates the values with a seperator
	 **/
	 
	public function join(sep : String) : String { return null; }
	
	
	public function indexOf(searchElement:T1, fromIndex:int=0) : int { return -1;}	
	
	public function fill(value : T1) : void {
	
	}
	
	public function copy(other : Array<T1>) : void {
	
	}
}