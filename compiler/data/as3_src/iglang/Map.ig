/*NO-OUTPUT*/
package iglang;

import iglang.unsafe.*;

public class Map<T1,T2> extends Object
{
	public function iterator(): MapIterator<T1,T2> { throw new Error("Incomplete"); }
	public function Map() {}
	public function containsKey(key : T1 ) : bool { return false;}
	public function deleteKey(key : T1 ) : void {}
	
	public function get[](idx : T1) : T2 {
		throw new Error("Not Implemented");
	}
	
	public function getWithDefault(idx : T1,defaux: T2) : T2 {
		throw new Error("Not Implemented");
	}
	
	public function set[](idx : T1, value : T2) : void {
		throw new Error("Not Implemented");
	}
	
	// need to implement in as3
	public function get length() : int {
		throw new Error("Not Implemented");
	}
}